#include <app.h>
#include <string.h>

#include "osrm/osrm.hpp"

#include "osrm/match_parameters.hpp"
#include "osrm/nearest_parameters.hpp"
#include "osrm/route_parameters.hpp"
#include "osrm/table_parameters.hpp"
#include "osrm/trip_parameters.hpp"

#include "osrm/coordinate.hpp"
#include "osrm/engine_config.hpp"
#include "osrm/json_container.hpp"

#include "osrm/osrm.hpp"
#include "osrm/status.hpp"

#include <exception>
#include <iostream>
#include <string>
#include <utility>

#include <cstdlib>


using namespace osrm;

OSRM *machine;

matrix_result_t Cache::get(int fx, int fy,int tx, int ty)
{
	matrix_result_t res={0};
	vector<matrix_result_t>::iterator it;
	if (this->curx != fx || this->cury != fy) {
		this->curres.clear();
		this->curres = get(fx,fy);
	} 

	for (it = this->curres.begin(); it != this->curres.end(); it++) {
		res = (matrix_result_t) *it;
		if (res.tx == tx && res.ty == ty) return res;
	} 

	res.meters = -1; // not-found condition

	this->curx=fx;
	this->cury=fy;
		
	return res;
}

vector<matrix_result_t>Cache::get(int fx, int fy)
{
	return k.get_matrixrow(fx,fy);
}

bool Cache::set(int fx, int fy,int tx, int ty,route_result_t r)
{
	k.set_matrixrow(fx,fy,tx,ty,r);
	return true;
}

Cache::Cache(string db, string collection)
{
	this->db=db;
	this->collection=collection;
}

Cache::Cache()
{
	this->db="network";
	this->collection="matrix";
}

Cache::~Cache()
{
	// close mongo
}

prox_t OsrmNearest(coord_t find, int *dist)
{
	prox_t ret={0};

    NearestParameters rp;
    rp.coordinates.push_back({util:FloatLongitude{find.x}, util::FloatLatitude{find.y}});

    json::Object result;

    const auto status = machine->Nearest(rp,result);

    if (status == Status::Ok) { 
        auto &waypoints = result.values["waypoints"].get<json::Array>();
        auto &waypoint = waypoints.values.at(0).get<json::Object>();
        auto &coords = waypoint.values["location"].get<json::Array>();
        auto &distance = waypoint.values["distance"].get<json::Number>();

        ret.x = coords.values.at(0).get<json::Number>().value;
        ret.y = coords.values.at(1).get<json::Number>().value;
        //ret.name = waypoint.values["name"].get<json::String>().value;

        //ret.x = phantom_node_vector.at(0).location.lon/1000000.0;
        //ret.y = phantom_node_vector.at(0).location.lat/1000000.0;
        //temp_string = facade->get_name_for_id(phantom_node_vector.front().name_id);
        //ret.name = strdup(temp_string.c_str());
    } 

	return ret;
}


route_result_t *OsrmRoute(Cache *cache, int nlocs, coord_t *locs)
{
	route_result_t *rr = (route_result_t *)calloc(sizeof(route_result_t),nlocs);
    RouteParameters rp;
    int dist;

    if (nlocs < 2) return rr;

    rp.overview = rp.OverviewType::Full;
    rp.steps = true;
    rp.geometries = rp.GeometriesType::Polyline6;
    //rp.annotations = RouteParameters.AnnotationsType::true;

    for (int i = 0; i < nlocs; ++i)
    {
        rp.coordinates.push_back({util::FloatLongitude{locs[i].x},util::FloatLatitude{locs[i].y}});
    }

    json::Object result;
    const auto status = machine->Route(rp,result);

    if (status != Status::Ok) { 
        // return an all 0 array 
        return rr;
    } 

    for (int i = 0; i < nlocs-1; ++i) { 
        auto &routes = result.values["routes"].get<json::Array>();
        auto &route = routes.values.at(0).get<json::Object>();

        const auto distance = route.values["distance"].get<json::Number>().value;
        const auto duration = route.values["duration"].get<json::Number>().value;
        const auto geometry = route.values["geometry"].get<json::String>().value;

		rr[i].meters=distance;
		rr[i].seconds=duration;
		rr[i].path=strdup(geometry.c_str());
    } 

	return rr;
}

coord_t *OsrmRandomNodes(int n)
{
	coord_t *list = (coord_t *)calloc(sizeof(coord_t),n);

	return list;
}

// the old version, calculates ever cells seperately
int *OsrmMatrixPerCell(int ndeps, coord_t *deps, int narrs, coord_t *arrs)
{
	int *rrd=(int*)calloc(sizeof(int),ndeps*narrs);
	return rrd;
}

int *OsrmMatrix(int nlocs, coord_t *locs)
{
	int *rrd=(int*)calloc(sizeof(int),nlocs*nlocs);

    TableParameters rp;
    json::Object result;

    int dist;

    if (nlocs < 2) return rrd;
    
    for (int i = 0; i < nlocs; ++i)
    {
        rp.coordinates.push_back({util::FloatLongitude{locs[i].x}, util::FloatLatitude{locs[i].y}});
        rp.sources.push_back(i);
        rp.destinations.push_back(i);
    }

    const auto status = machine->Table(rp,result);
    auto &durations = result.values["durations"].get<json::Array>();

    if (status != Status::Ok) 
        return rrd;

    for (int row=0; row < nlocs; row++) { 
        auto &dur_row = durations.values.at(row).get<json::Array>();
        for (int col=0; col < nlocs; col++) { 
            const auto dur_cell = dur_row.values.at(col).get<json::Number>().value;
            rrd[row*nlocs+col] = dur_cell;
        }
    } 

	return rrd;
}

Mat<int32_t> *OsrmDistanceMatrix(int ndeps, coord_t *deps, int narrs, coord_t *arrs)
{
	//double result;
	int x,y;
	int d;
	Mat<int32_t> *mp = new Mat<int32_t>(ndeps,narrs);

	int *res = OsrmMatrix(ndeps,deps);

	for (y=0; y< ndeps; y++) {
		for(x=0; x< narrs; x++) {
			int dist = res[y*narrs+x];
			mp->Set(y,x,dist);
            printf("%d ", dist);
			//delete res[y*narrs+x];
		} 
        printf("\n");
	} 

    printf("nu %d\n", mp->Get(0,0));

	free(res);
    printf("nu %d\n", mp->Get(0,0));
	return mp;
}


prox_t OsrmGeocode(char *country, char *city, char *street, char *num, char *zipcode)
{
	prox_t p={0};

	p.x = 1.1;
	p.y = 2.2;

	return p;
}

addr_t OsrmEdocoeg(double x,double lng)
{
	addr_t p={0};

	return p;
}

void OsrmExit(void)
{
    //delete nearest;
    //delete facade;
    delete machine;

    printf("--- deleted \n");
}

void OsrmInit(const char *base)
{
    //ServerPaths server_paths;
    const std::string base_string = base;

    EngineConfig config;
    config.storage_config = {base_string};
    config.use_shared_memory = false;

    machine = new OSRM{config};

#ifdef LK
	if (init_address(addrbuf) < 0) {
		printf("could not load %s\n", addrbuf);
	}
#endif


}
