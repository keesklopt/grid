/* mainly because fastci and mongo db bite eachother
	but also for convenience, and for smoorther debugging
	tomcat(&) was tried, but gave to often an out of memory error
	and of course the main code is C 
	A standalone server version
 */
#include <vector>
#include <app.h>
#include <fcntl.h>

#include <execinfo.h>
#include <signal.h>

#include <syslog.h>

#define MAXHOSTNAMELEN 64
//#define NETWORK "/data/projects/osrm/net1.osrm"
//#define NETWORK "/data/projects/matrix/netherlands-latest.osrm"
//#define NETWORK "/data/projects/matrix/europe-latest.osrm"
//#define NETWORK "/projects/klopt/3pty/osrmlib/europe-latest.osrm"
//#define NETWORK "/projects/klopt/klopt/3pty/osrmlib/netherlands-latest.osrm"
//#define NETWORK "/projects/klopt/klopt/3pty/netherlands-latest.osrm"
//#define NETWORK "/home/kees/netherlands-latest.osrm"
//#define NETWORK "/home/kees/projects/3pty/osrm-backend/build/netherlands-latest.osrm"
#define NETWORK "/bigsafe/kees/projects/klopt/3pty/osrm-backend/europe-latest.osrm"

using namespace std;

KloptServer::KloptServer(int backlog)
{
	backlog=backlog;
}

void KloptServer::Add(Service *s) {
	services.push_back(s);
	s->setServer(this);
} 

Client *KloptServer::Wait()
{
	int t,u, res,max=0;
    fd_set read_fd;
  	fd_set err_fd;
   	Client *cpp;
   	Client *nc;
   	Service *spp;
   	Socket *ns;
   	int sock;

	FD_ZERO(&err_fd);
   	FD_ZERO(&read_fd);

	// count highest socket fd (for select)
	for (vector<Service *>::iterator it = services.begin();
		it != services.end(); it++) {
		spp = (Service *)*it;
		ns = spp->getSocket();
		sock = ns->getfd();
		if (sock > max) max = sock;
		FD_SET(sock, &err_fd);
       	FD_SET(sock, &read_fd);
		//printf("Clients : %ld\n", spp->clts.size());
		for (vector<Client *>::iterator cit = spp->clts.begin();
			cit != spp->clts.end(); cit++) {
			cpp = (Client *) *cit;
			ns = cpp->getSocket();
			sock = ns->getfd();
			if (sock > max) max = sock;
           	FD_SET(sock, &err_fd);
           	FD_SET(sock, &read_fd);
		} 
	} 

again: // here comes a loop
	//printf("waiting %d\n", max);
	res = select(max + 1, &read_fd, NULL, &err_fd, NULL);
	//printf("waited %d %d %d\n", res, FD_ISSET(sock, &read_fd), FD_ISSET(sock, &err_fd));

	if (res < 0) {
		//printf("retrying with res is %d, err_fd = %d\n", res, err_fd);
		// this is probably one of the child signals, just retry
		goto again;
	}

	for (vector<Service *>::iterator it = services.begin();
		it != services.end(); it++) {
		spp = (Service *)*it;
		ns = spp->getSocket();
		sock = ns->getfd();
		if  (FD_ISSET(sock, &err_fd)) {
			printf("heel jammer\n");
		}
		if  (FD_ISSET(sock, &read_fd)) {
			ns = ns->Accept();
			if (!ns) {
				perror("accept");
				return NULL;
			} else {
				int c=0;
				nc =  new Client(ns);
				nc->setState(CLT_INIT);
				//if (spp->sock->type == SOCK_STREAM)
				c = spp->Add(nc);
				//printf("a: now %d clients \n", c);
				nc->setService(spp);
				return nc;
			}
		}

		for (vector<Client *>::iterator cit = spp->clts.begin();
			cit != spp->clts.end(); cit++) {
			cpp = (Client *) *cit;
			cpp->setState(CLT_RUNNING);
			ns = cpp->getSocket();
			sock = ns->getfd();
			if  (FD_ISSET(sock, &read_fd)) {
				return cpp;
			}
			if  (FD_ISSET(sock, &err_fd)) {
				printf("jammer");
			}
		}
	}
	printf("fell through!\n");
	return NULL;
}

int KloptServer::Handle(Client *c)
{
	intptr_t ret;
	int count;

	ret = c->Handle();

	if (ret == CS_CONT) return 0;

	count = c->Del();
	delete c;

	if (ret == CS_DONE) {
		//printf("now %d clients left\n", count);
        //printf("[RTE]: ------ waiting for input\n");
		return 0;
	}

	return 1; // CS_STOP remains : signal server stop 
}

int KloptServer::Loop(void)
{
	int stop=0;
	while(!stop) { 
		Client *c = Wait();
		stop = Handle(c);
		//stop = c->Handle();
	} 

	return stop;
}

class JsonRteService : public JsonService
{
	vector<prox_t> result;
public:
	int id;
	json_object * handle_ping(JsonRpc *message,Client *);
	//string handle_status(JsonRpc *message,Client *);
	//string handle_geocode(JsonRpc *message,Client *);
	//string handle_revgeocode(JsonRpc *message,Client *);
	JsonRteService(int port,int type);
	//string FGetS(Client *c);
	//intptr_t Handle(Client *c);

	json_object *handle_route(JsonRpc *message,Client *);
	json_object *handle_nearest(JsonRpc *message,Client *);
	json_object *handle_matrix(JsonRpc *message,Client *);
    json_object *handle_randomnodes(JsonRpc *message,Client *clt);

	// these could go into their own service 
	// but then a binary matrix should be communicated from RTE 
	json_object *handle_tsp(JsonRpc *message,Client *clt);
	json_object *handle_vrp(JsonRpc *message,Client *clt);
	//string csv_report_setorders(vector<Location *>olist);
	//string csv_report_getdistances(vector<Location *>olist);
	//string csv_report_getmatrixrow(vector<Location *>olist,int row);
	//string json_report(void);
};

json_object *get_route_array(route_result_t *mp,int count, Options options)
{
	int r;
	int c;
	char workbuf[1024]={0};
	
	json_object *jo = json_object_new_object();
	json_object *routes = json_object_new_array();

	for (r=0; r< count; r++) {

		json_object *elm = json_object_new_object();


		if (options.meters) {
			json_object *mtr = json_object_new_int(mp[r].meters);
			json_object_object_add(elm,"mtr",mtr);
		} 
		if (options.minutes) {
			json_object *sec = json_object_new_int(mp[r].seconds);
			json_object_object_add(elm,"sec",sec);
		} 
		if (options.paths) {
			char *path=(char *)"\"\"";
			if (mp[r].path) 
				path=mp[r].path;

			json_object *pth = json_object_new_string(path);
			json_object_object_add(elm,"path",pth);
		} 

		json_object_array_add(routes, elm);
	}

	json_object_object_add(jo, "routes", routes);
	return jo;
}

JsonRteService::JsonRteService(int port,int type) : JsonService(port,type) 
{
	this->id=0;
} 

json_object *JsonRteService::handle_ping(JsonRpc *message,Client *clt)
{
	int len;
    //json_object *base = json_object_new_object();
    json_object *result = json_object_new_object();
    json_object *pong   = json_object_new_string("pong");

    json_object_object_add(result, "method", pong);
    //json_object_object_add(base, "result", result);

	k.count("ping",1,1);
    // note for the originator addres (ip) we need to pass it 
    // along in ws.wsgi or it will be localhost, later
	k.log("ping",1,clt->sock->getPeerIp());

	return result;
} 

json_object *JsonRteService::handle_route(JsonRpc *message,Client *clt)
{
	json_object *jo;
	micro_t starttime;
	micro_t endtime;
	int i;
    int ncoords;
    route_result_t *m;
	coord_t *deps;
    Options o(message->get_param("options"));

	json_object *coord=NULL;
	json_object *pc=NULL;
	json_object *jx=NULL;
	json_object *jy=NULL;
	json_object *crd;
    json_object *locs = message->get_param("locations");

	starttime = micro_now();
    if (!locs) { 
        json_object *errsmsg = create_error(error_bad_param, "missing parameter locations");
		jo = errsmsg;
        goto cleanup;
    } 

    if (json_object_get_type(locs) != json_type_array) { 
        json_object *errsmsg = create_error(error_type, "locations member should be array");
		jo = errsmsg;
        goto cleanup;
    } 

    ncoords = json_object_array_length(locs);

	deps=(coord_t *)calloc(sizeof(coord_t),ncoords);
	for (i=0;i<ncoords; i++) {
		coord = json_object_array_get_idx(locs, i);

		if (json_object_get_type(coord) == json_type_object) {
			json_object_object_get_ex(coord, "x", &jx);
			json_object_object_get_ex(coord, "y", &jy);
		} else if (json_object_get_type(coord) == json_type_array) { 
			jx = json_object_array_get_idx(coord, 0);
			jy = json_object_array_get_idx(coord, 1);
		} else { 
        	json_object *errsmsg = create_error(error_type, "coordinate member should be an array or object");
			jo = errsmsg;
            goto cleanup;
    	} 

		if (!jx || !jy) { 
        	json_object *errsmsg = create_error(error_type, "no x and y members found in coordinate");
			jo = errsmsg;
            goto cleanup;
		} 
		double sx=json_object_get_double(jx);
		double sy=json_object_get_double(jy);
		deps[i].x = sx;
		deps[i].y = sy;
    }

	m = OsrmRoute(NULL, ncoords, deps);

	endtime = micro_now();

	jo = get_route_array(m,ncoords-1, o);

	if (o.timings) {
		json_object *timings=json_object_new_object();
		json_object *route = json_object_new_int(endtime-starttime);
		
		json_object_object_add(jo, "timings", timings);
		json_object_object_add(timings, "route", route);
	} 

	k.count("routes",1,ncoords);
	k.log("routes",ncoords,clt->sock->getPeerIp());

cleanup:

    for (int r=0; r < ncoords; r++) 
	    free(m[r].path);
	if (m) free(m);
    if (deps) free(deps);

	return jo;
} 


json_object *JsonRteService::handle_matrix(JsonRpc *message,Client *clt)
{
	json_object *jo;
	micro_t starttime;
	micro_t endtime;
	int i;

	json_object *jx=NULL;
	json_object *jy=NULL;
	json_object *coord;
	json_object *options;
    json_object *locs = message->get_param("locations");

    Options o(message->get_param("options"));

    if (!locs) { 
        json_object *errsmsg = create_error(error_bad_param, "missing parameter locations");
		return errsmsg;
    } 

    if (json_object_get_type(locs) != json_type_array) { 
        json_object *errsmsg = create_error(error_type, "locations member should be array");
		return errsmsg;
    } 

	starttime = micro_now();

	int ncoords = json_object_array_length(locs);
	coord_t *deps=(coord_t *)calloc(sizeof(coord_t),ncoords);
	for (i=0;i<ncoords; i++) {
		coord = json_object_array_get_idx(locs, i);

		if (json_object_get_type(coord) == json_type_object) {
			json_object_object_get_ex(coord, "x", &jx);
			json_object_object_get_ex(coord, "y", &jy);
		} else if (json_object_get_type(coord) == json_type_array) { 
			jx = json_object_array_get_idx(coord, 0);
			jy = json_object_array_get_idx(coord, 1);
		} else { 
        	json_object *errsmsg = create_error(error_type, "coordinate member should be an array or object");
            if (deps) free(deps);
			return errsmsg;
    	} 

		if (!jx || !jy) { 
        	json_object *errsmsg = create_error(error_type, "no x and y members found in coordinate");
			return errsmsg;
		} 
		double sx=json_object_get_double(jx);
		double sy=json_object_get_double(jy);
		deps[i].x = sx;
		deps[i].y = sy;
    }

	int *m = OsrmMatrix(ncoords, deps);

	jo = json_object_new_object();

	endtime = micro_now();

	//json_object *result = json_object_new_object();
	json_object *rows = json_object_new_array();
	json_object_object_add(jo, "times", rows);

	for (int y=0; y< ncoords; y++) {
		
		json_object *cols = json_object_new_array();
		for (int x=0; x< ncoords; x++) {
			// to match the exact number from osrm-routed do /10 +1 (seconds)
			int val = m[y*ncoords+x];
			if (val > 0) val = val/10+1;
			json_object *item = json_object_new_int(val);
    		json_object_array_add(cols,item);
		}
    	json_object_array_add(rows,cols);
	} 

	if (o.timings) {
		json_object *timings=json_object_new_object();
		json_object *matrix = json_object_new_int(endtime-starttime);
		
		json_object_object_add(jo, "timings", timings);
		json_object_object_add(timings, "matrix", matrix);
	} 

	if (m) free(m);
    if (deps) free(deps);

	k.count("matrix",1,ncoords);
	k.log("matrix",ncoords,clt->sock->getPeerIp());

	return jo;
} 

json_object *JsonRteService::handle_randomnodes(JsonRpc *message,Client *clt)
{
	json_object *jo;
	micro_t starttime;
	micro_t endtime;
	int i;

	json_object *crd;
    json_object *num = message->get_param("number");

	int n=1;
	if (num) n = json_object_get_int(num);

	starttime = micro_now();

	coord_t *coords = OsrmRandomNodes(n);

	jo = json_object_new_object();

	for (i = 0; i< n; i++) { 
		json_object *elm= json_object_new_array();
		json_object *x= json_object_new_int(coords[i].x);
		json_object *y= json_object_new_int(coords[i].y);
		json_object_object_add(elm, "x", x);
		json_object_object_add(elm, "y", y);
		json_object_array_add(jo, elm);
	} 
	

	k.count("randomnodes",1,n);
	k.log("randomnodes",n,clt->sock->getPeerIp());

	return jo;
} 

json_object *JsonRteService::handle_nearest(JsonRpc *message,Client *clt)
{
	//stringstream ss;
	json_object *jo;
    int b, c, ncoords=0;
	micro_t starttime;
	micro_t endtime;

	starttime = micro_now();
	json_object *x, *y;
	bool timings=false;
	bool membernames=false;
	bool meters=false;
	bool matchname=false;
	json_object *coord;
	json_object *options;
	json_object *coords = message->get_param("locations");

    if (!coords) { 
        json_object *errsmsg = create_error(error_bad_param, "missing parameter locations");
		return errsmsg;
    } 

    if (json_object_get_type(coords) != json_type_array) { 
        json_object *errsmsg = create_error(error_type, "locations member should be array");
		return errsmsg;
    } 

    ncoords = json_object_array_length(coords);
	options = message->get_param("options");
	if (options) { 
		json_object *opt;
		json_object_object_get_ex(options, "timings", &opt); 
        b = json_object_get_boolean(opt);

		if (opt && b) 
			timings = true;
		json_object_object_get_ex(options, "membernames", &opt); 
        b = json_object_get_boolean(opt);
		if (opt && b) 
			membernames = true;
		json_object_object_get_ex(options, "meters", &opt); 
        b = json_object_get_boolean(opt);
		if (opt &&  b) 
			meters = true;
		json_object_object_get_ex(options, "matchname", &opt); 
        b = json_object_get_boolean(opt);
		if (opt && b) 
			matchname = true;
	} 

	jo = json_object_new_object();
	json_object *locations = json_object_new_array();

	json_object_object_add(jo, "locations", locations);

	for (c=0; c< ncoords; c++) { 
		coord = json_object_array_get_idx(coords, c);

		if (json_object_get_type(coord) == json_type_object) {
			json_object_object_get_ex(coord, "x", &x);
			json_object_object_get_ex(coord, "y", &y);
		} else if (json_object_get_type(coord) == json_type_array) { 
			x = json_object_array_get_idx(coord, 0);
			y = json_object_array_get_idx(coord, 1);
		} else { 
        	json_object *errsmsg = create_error(error_type, "coordinate member should be an array or object");
			return errsmsg;
    	} 

		coord_t match;
		int len;
		match.x=json_object_get_double(x);
		match.y=json_object_get_double(y);
		prox_t result = OsrmNearest(match,&len);
		if (result.name == NULL) {
			result.name = (char *)strdup("null");
		} 


		json_object *row;
		json_object *x = json_object_new_double(result.x);
		json_object *y = json_object_new_double(result.y);

		if (membernames) {
			row = json_object_new_object();
			json_object_object_add(row, "x", x);
			json_object_object_add(row, "y", y);
		} else { 
			row = json_object_new_array();
			json_object_array_add(row, x);
			json_object_array_add(row, y);
		} 

		if (meters) { 
            json_object *mtrs = json_object_new_int(len/1000);
			if (membernames)
                json_object_object_add(row,"meters",mtrs);
            else
                json_object_array_add(row,mtrs);
		} 
		if (matchname) { 
            json_object *mtrs = json_object_new_string(result.name);
            free(result.name);
			if (membernames)
                json_object_object_add(row,"matchname",mtrs);
            else
                json_object_array_add(row,mtrs);
		} 
		json_object_array_add(locations, row);
	} 
	endtime = micro_now();
	if (timings) {
        json_object *timings = json_object_new_object();
        json_object *nearest = json_object_new_int(endtime-starttime);
        json_object_object_add(jo, "timings", timings);
        json_object_object_add(timings, "nearest", nearest);
    } 
	k.count("nearest",1,ncoords);
	k.log("nearest",ncoords,clt->sock->getPeerIp());
	return jo;
} 

json_object *JsonRteService::handle_tsp(JsonRpc *message,Client *clt)
{
	json_object *jo;
	micro_t starttime;
	micro_t endtime;
	int i;

	json_object *jx=NULL;
	json_object *jy=NULL;
	json_object *coord;
    json_object *locs = message->get_param("locations");

    Options o(message->get_param("options"));

    if (!locs) { 
        json_object *errsmsg = create_error(error_bad_param, "missing parameter locations");
		return errsmsg;
    } 

    if (json_object_get_type(locs) != json_type_array) { 
        json_object *errsmsg = create_error(error_type, "locations member should be array");
		return errsmsg;
    } 
	starttime = micro_now();

	int ncoords = json_object_array_length(locs);
	coord_t *deps=(coord_t *)calloc(sizeof(coord_t),ncoords);
	for (i=0;i<ncoords; i++) {
		coord = json_object_array_get_idx(locs, i);

		if (json_object_get_type(coord) == json_type_object) {
			json_object_object_get_ex(coord, "x", &jx);
			json_object_object_get_ex(coord, "y", &jy);
		} else if (json_object_get_type(coord) == json_type_array) { 
			jx = json_object_array_get_idx(coord, 0);
			jy = json_object_array_get_idx(coord, 1);
		} else { 
        	json_object *errsmsg = create_error(error_type, "coordinate member should be an array or object");
			return errsmsg;
    	} 

		if (!jx || !jy) { 
        	json_object *errsmsg = create_error(error_type, "no x and y members found in coordinate");
			return errsmsg;
		} 
		double sx=json_object_get_double(jx);
		double sy=json_object_get_double(jy);
		deps[i].x = sx;
		deps[i].y = sy;
    }

    tsp_result_t *m;
    if (ncoords < 3) { 
        m = new tsp_result_t[2];
        m[0].index = 0;
        m[1].index = 1;
    } else { 
        m = TspLk(ncoords, deps, &deps[0],&deps[ncoords-1],0,0);
    } 

	free(deps);

	jo = json_object_new_object();

	endtime = micro_now();
	char workbuf[1024]={0};

    json_object *order = json_object_new_array();
    json_object *times = json_object_new_array();

	for (int y=0; y< ncoords; y++) {
		int val = m[y].dist;
		int idx = m[y].index;

		if (val > 0) val = val/10+1;

        json_object *jval = json_object_new_int(val/10+1);
        json_object *jseq = json_object_new_int(idx);

        json_object_array_add(times, jval);
        json_object_array_add(order, jseq);

	} 

	free(m);

    json_object_object_add(jo, "times", times);
    json_object_object_add(jo, "order", order);

	if (o.timings) {
		json_object *timings=json_object_new_object();
		json_object *route = json_object_new_int(endtime-starttime);
		
		json_object_object_add(timings, "tsp", route);
		json_object_object_add(jo, "timings", timings);
	} 


	k.count("tsp",1,ncoords);
	k.log("tsp",ncoords,clt->sock->getPeerIp());

	return jo;
}

json_object *JsonRteService::handle_vrp(JsonRpc *message,Client *clt)
{
	json_object *jo;
	micro_t starttime;
	micro_t endtime;
	int i;
    int vcap=0;

	json_object *jx=NULL;
	json_object *jy=NULL;
	json_object *order;
	json_object *coord;
	json_object *amount;
    json_object *ords = message->get_param("orders");
    json_object *unusable = json_object_new_array();

    Options o(message->get_param("options"));

    if (!ords) { 
        json_object *errsmsg = create_error(error_bad_param, "missing parameter orders");
		return errsmsg;
    } 

    if (json_object_get_type(ords) != json_type_array) { 
        json_object *errsmsg = create_error(error_type, "orders member should be array");
		return errsmsg;
    } 
	starttime = micro_now();

	int norders = json_object_array_length(ords);
	order_t *orders=(order_t *)calloc(sizeof(order_t),norders+1);
    int use=0;
    int unuse=0;
	char workbuf[1024]={0};
	for (i=0;i<norders; i++) {
		order = json_object_array_get_idx(ords, i);

		if (json_object_get_type(order) != json_type_object) {
            json_object *errsmsg = create_error(error_type, "order member should be an object");
    		return errsmsg;
        } 

		json_bool tamount = json_object_object_get_ex(order, "amount", &amount);
        if (!tamount) { 
            json_object *errsmsg = create_error(error_type, "order element has no amount member");
    		return errsmsg;
        } 

		json_bool tcoord = json_object_object_get_ex(order, "location", &coord);
        if (!tcoord) { 
            json_object *errsmsg = create_error(error_type, "order element has no coord member");
    		return errsmsg;
        } 

		if (json_object_get_type(coord) == json_type_object) {
			json_object_object_get_ex(coord, "x", &jx);
			json_object_object_get_ex(coord, "y", &jy);
		} else if (json_object_get_type(coord) == json_type_array) { 
			jx = json_object_array_get_idx(coord, 0);
			jy = json_object_array_get_idx(coord, 1);
		} else { 
        	json_object *errsmsg = create_error(error_type, "coordinate member should be an array or object");
			return errsmsg;
    	} 

		if (!jx || !jy) { 
        	json_object *errsmsg = create_error(error_type, "no x and y members found in coordinate");
			return errsmsg;
		} 
		double sx=json_object_get_double(jx);
		double sy=json_object_get_double(jy);
        int a = json_object_get_int(amount);

        if (i > 0 && a > orders[0].amount) { 
		    orders[norders-unuse-1].id = i;
            json_object *unu = json_object_new_int(i);
            json_object_array_add(unusable, unu);
            unuse++;
        } else { 
		    orders[use].x = sx;
		    orders[use].y = sy;
		    orders[use].id = i;
            orders[use].amount = json_object_get_int(amount);
            use++;
        } 
    }
	orders[use].id = i;

    printf("NO %d U %d UN %d\n", norders, use, unuse);
    vrp_result_t *m;
    if (use < 3) { 
        //m = new vrp_result_t[2];
        m = (vrp_result_t *)calloc(sizeof(vrp_result_t),2);
        m[0].index = 1;
        m[1].index = 2;
        m[0].trip = 1;
        m[1].trip = 1;
        printf("INIT\n");
    } else { 
        printf("OTHER\n");
        m = VrpSa(use, orders, orders[0].amount, o.algos, 0, 0);
    } 

	endtime = micro_now();

    json_object *trips=json_object_new_array();
    //json_object *times=json_object_new_array();

    int nt=0;
    int subtotal=0;
    int totaltime=0;
    order=NULL;
    json_object *times=NULL;
    json_object *total=NULL;

	for (int y=0; y< use-1; y++) {
		int val = m[y].time;
		int trp = m[y].trip;
		int idx = m[y].index;

        if (nt < trp) { // new trip
            json_object *elm = json_object_new_object();

            order = json_object_new_array();
            times = json_object_new_array();
            total = json_object_new_int(subtotal);

            json_object_object_add(elm, "order", order);
            json_object_object_add(elm, "times", times);
            json_object_object_add(elm, "total", total);

            json_object_array_add(trips, elm);
            
            subtotal=0;

            nt=trp;
        } 
		if (val > 0) val = val/10+1;
        json_object *jid = json_object_new_int(orders[idx].id);
        json_object *jval = json_object_new_int(val);

        json_object_array_add(order, jid);
        json_object_array_add(times, jval);

        subtotal += val;
        totaltime += val;
	} 
	int val = m[use-1].time;
	if (val > 0) val = val/10+1;
    subtotal += val;
    totaltime += val;

	if (orders) free(orders);
    
	jo = json_object_new_object();
	total = json_object_new_int(totaltime);
    json_object_object_add(jo, "trips", trips);
    json_object_object_add(jo, "total", total);
    json_object_object_add(jo, "unsusable", unusable);

	if (o.timings) {
		json_object *timings=json_object_new_object();
		json_object *vrp = json_object_new_int(endtime-starttime);
		
		json_object_object_add(timings, "vrp", vrp);
		json_object_object_add(jo, "timings", timings);
	} 

	//sprintf(workbuf, "], \"total\": %d, \"unusable\":[%s],\"timings\":{\"matrix\":%ld} }", totaltime, unusable.c_str(), endtime-starttime);
	//ss += workbuf;

	//printf("result: %s\n", ss.c_str());

	free(m);

	k.count("vrp",1,norders);
	k.log("vrp",norders,clt->sock->getPeerIp());

	return jo;
}


class RteServer : public KloptServer
{
public:
	RteServer(int backlog) : KloptServer(backlog) {
	}
};

class HelloService : public Service
{
public:
	HelloService(int port, int type) : Service(port, type)
	{
	} 

	intptr_t Handle(Client * c)
	{
		printf("HelloService\n");
		return 0;
	}
};

int main(int argc, char *argv[])
{
	RteServer ts(5);
    // later !
    //Yaml y(YAML_FILE); 
    //string network = y.getStringProperty((char *)"network");

    Properties p(PROP_FILE);
    string network = p.getStringProperty((char *)"network");
    //printf("Got %s for network source\n", network.c_str());

    openlog("klopt.RTE", 0,LOG_NOTICE);

    syslog(LOG_NOTICE, "Loading network: \"%s\"", network.c_str());
	Network n(network.c_str());
    syslog(LOG_NOTICE, "Service started");

	//CommandService s(8811,SOCK_STREAM);
	JsonRteService j(SOCK_RTE,SOCK_STREAM);

	//ts.Add(&s);
	ts.Add(&j);
	ts.Loop();

	return 0;
}
