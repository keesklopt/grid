#ifdef USE_GOSMORE
#include <libgosm.h>
#endif

#define USE_MONGO 1
#define PROP_FILE "/etc/klopt/properties.json"
#define YAML_FILE "properties.yml"
// end osrm includes
//#include <net.h>
using namespace std;

#include <json-c/json.h>
#include <yaml.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <map>
#include <vector>
#ifdef USE_MONGO
#include "mongo/client/dbclient.h"
#endif
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>

#include <sys/param.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <dirent.h>

#ifndef GENERATING_PROTO
#include <boost/date_time/posix_time/posix_time.hpp>
#include "boost/date_time/posix_time/posix_time_types.hpp"
#include "boost/locale.hpp"
#include "boost/algorithm/string.hpp"
#endif

#define REQ_TIMEOUT 30
#define READ_LEN 1024

#define SOCK_SRV 6660
#define SOCK_GEO 6661
#define SOCK_RTE 6662
#define SOCK_TSP 6663
#define SOCK_VRP 6664
#define SOCK_LOC 6665
#define SOCK_TEL 6666
#define SOCK_SLV 6667

// this should match errors in json.cpp
enum { 
    error_no_error=0,
    error_bad_param,
    error_type,

    error_last_error,
};

using namespace boost::posix_time;

typedef int64_t micro_t;
micro_t micro_now(void);

enum clt_states {
    /*! just after connection, use when server has to msg first */
    CLT_INIT,
    /*! normal running, client should msg now */
    CLT_RUNNING,
    /*! serverside message, not a reaction to client message */
    CLT_PUSH,
    /*! serverside stopping of client, for instance when server stops */
    CLT_EXIT,
};

extern json_object *create_error(int err, std::string data);
//extern net_t *np;

typedef int (* perm_fnc_t)(int, int *,void *);

template <class T> class Mat
{
public:
    int rows;
    int cols;
    T *data;

    Mat (int rows,int cols) {
        this->cols=cols;
        this->rows=rows;  
        this->data = new T[this->cols*this->rows];
    } 

    void Set(int y, int x, T val)
    {   
        if (x > this->cols) return;
        if (y > this->rows) return;
        this->data[y*this->cols+x]=val;
    }

    T Get(int y, int x) {
        if (!data) return -1;
        if (x > this->cols) return -1;
        if (y > this->rows) return -1;
        return this->data[y*this->cols+x];
    } 

    void Dmp(void) {   
        int x;
        int y;
    
        for (y=0; y< this->rows; y++) {
            for (x=0; x< this->cols; x++) {
                printf("%d/%d,", this->data[y*this->cols+x],Get(y,x));
            }
            printf("\n");
        }
    } 
};

class Options
{
    public:

    bool timings;
    bool matchname;
    bool meters;
    bool minutes;
    bool membernames;
    bool paths;
    std::map<std::string,bool> algos;

    void parse_algorithms(json_object *algorithms) 
    { 
	    if (algorithms) { 
		    json_object *sol=NULL;
		    json_object_object_get_ex(algorithms, "haversine", &sol); 
		    if (sol) this->algos["haversine"] = true;
		    json_object_object_get_ex(algorithms, "osrm", &sol); 
		    if (sol) this->algos["osrm"] = true;
		    json_object_object_get_ex(algorithms, "one_pt_move", &sol); 
		    if (sol) this->algos["one_pt_move"] = true;
		    json_object_object_get_ex(algorithms, "two_pt_move", &sol); 
		    if (sol) this->algos["two_pt_move"] = true;
		    json_object_object_get_ex(algorithms, "two_opt", &sol); 
		    if (sol) this->algos["two_opt"] = true;
		    json_object_object_get_ex(algorithms, "or_opt", &sol); 
		    if (sol) this->algos["or_opt"] = true;
		    json_object_object_get_ex(algorithms, "three_opt", &sol); 
		    if (sol) this->algos["three_opt"] = true;
		    json_object_object_get_ex(algorithms, "cross_exchange", &sol); 
		    if (sol) this->algos["cross_exchange"] = true;
		    json_object_object_get_ex(algorithms, "three_pt_move", &sol); 
		    if (sol) this->algos["three_pt_move"] = true;
        } 
    } 

    void Parse(json_object *options) 
    { 
	    if (options) { 
		    json_object *opt;
            json_bool b;
		    json_object_object_get_ex(options, "timings", &opt); 
            b = json_object_get_boolean(opt);
		    if (opt && b) 
		        this->timings = true;
		    json_object_object_get_ex(options, "membernames", &opt); 
            b = json_object_get_boolean(opt);
		    if (opt && b) 
		        this->membernames = true;
		    json_object_object_get_ex(options, "meters", &opt); 
            b = json_object_get_boolean(opt);
		    if (opt &&  b) 
		        this->meters = true;
		    json_object_object_get_ex(options, "matchname", &opt); 
            b = json_object_get_boolean(opt);
		    if (opt && b) 
		        this->matchname = true;
		    json_object_object_get_ex(options, "algorithms", &opt); 
                parse_algorithms(opt); // just allways add

		    json_object_object_get_ex(options, "paths", &opt); 
            b = json_object_get_boolean(opt);
            //std::cout << opt << std::endl;
		    if (opt && b) 
		        this->paths = true;
        } 
    } 

    Options(json_object *options) {
        if ( !options) return ;
        this->timings = false;
        this->matchname = false;
        this->membernames = false;
        this->meters = true;
        this->minutes = true;
        this->paths = false;

        this->Parse(options);
    } 

};

typedef struct coord_tag
{   
    double x;
    double y; 
} coord_t;

class Properties
{
    struct json_object *root;

public:
    Properties(std::string fname);
	~Properties(void);
    std::string getStringProperty(char *key);
    int getIntProperty(char *key);
    int readFile(std::string name);
};

class Yaml
{
	yaml_parser_t parser;
	FILE *fh;
	
public:
    Yaml(std::string fname);
	~Yaml(void);
    yaml_token_t getProperty(char *key);
    std::string getStringProperty(char *key);
    int getIntProperty(char *key);
    int readFile(std::string name);
};

class Permute
{
	public:
	int num;
	int *index;

	Permute(int n);
	void go(perm_fnc_t fnc, void *);
	void go_deep(int x, perm_fnc_t fnc, void *) ;
	void dmp(void);
};

typedef struct vrp_result_tag
{
    int trip;
	int index;
	double time;
	int64_t matrixtime;
	int64_t tsptime;
	// double time;
} vrp_result_t;

typedef struct tsp_result_tag
{
	int index;
	double dist;
	int64_t matrixtime;
	int64_t tsptime;
	// double time;
} tsp_result_t;

class Tsp
{
	private:
	coord_t *locs;
	int nlocs;
	public:
	Mat<int32_t> *mat;
	class Permute *prm;
	tsp_result_t *solution;
	int bestdist;
	int besttime;

	Tsp() { }
	Tsp(int,coord_t *,Mat<int32_t> *);
	int add(double,double);
	int add(int nlocs,coord_t *locs);

	void run(void);
};

typedef struct order_tag
{
    double x;
    double y;
    int id;
    int amount;
} order_t;

typedef struct prox_tag
{
	double x;
	double y;
	double dist;
	char *name;
} prox_t;

typedef struct poly_tag
{
    int npoints;
    string points;
} poly_t;

typedef struct addr_tag
{
	string name;
	string street;
	string city;
	string country;
	string zipcode;
	string number;
} addr_t;

typedef struct route_result_tag
{
	int meters;
	int seconds;
	char *path;
} route_result_t;

typedef struct matrix_result_tag
{
	int fx;
	int fy;
	int tx;
	int ty;
	int meters;
	int seconds;
} matrix_result_t;

class Klopt
{
#ifdef USE_MONGO
	mongo::DBClientConnection conn;
#endif
public:
	Klopt();
	~Klopt();

    boost::posix_time::ptime dawn;  // start of project
    boost::posix_time::ptime fault;  // last crash time
    boost::posix_time::ptime up;  // last start time

    // gulicus
    
    char *get_zipcode(const char *city, const char *street, const char *house);
	prox_t klopt_get_coord(const char *id,const char *cc, const char *city, const char *street, const char *house,const char *pc);
	addr_t klopt_get_address(double x, double y,int, micro_t *, poly_t *);
    poly_t klopt_get_pand(string pandid);
	prox_t KloptGeocode(const char *country, const char *city, const char *street, const char *num, const char *zipcode);
    addr_t KloptRevGeocode(double x, double y,int max,micro_t *timings,poly_t *pand);
	void set_matrixrow(int fx, int fy,int tx, int ty,route_result_t r);
    void get_street(string id,addr_t &);
    void count(string id,int req,int num);
    string counts(void);
    void log(string id,int num, string ip);
    void initstats(string str);
    string get_street_id(string city_id, string street);
    string get_city(string id);
    string get_city_id(string name);
	route_result_t get_matrixrowcol(int fx, int fy,int tx, int ty,bool dummy);
	vector<matrix_result_t>get_matrixrow(int fx, int fy);
};

class Location
{
public:
	string id;
	string postcode;
	string nr;
	double x;
	double y;
	bool found;

	Location()
	{
	}

	Location(string id)
	{
		this->id=id;
	}
};

class OrderSet
{
public:
	string name;
	vector <Location *> locations;

	OrderSet(string name)
	{
		this->name=name;
	}
};

class Context
{
public:
	string id;
	map <string,OrderSet *> ordersets;

	Context(string name);
	OrderSet *addorderset(string osname);
	OrderSet *getorderset(string oset);
	void clearorderset(string osname);
};
#define CS_CONT 0 /*!< continue */
#define CS_DONE 1 /*!< done with message */
#define CS_STOP 2 /*!< done with server ! */

class Service;
class Client;
class Socket;

class KloptServer
{
public:
	int fd, sockfd;
	int backlog;
	vector<Service *> services;

	KloptServer(int backlog);
	void Add(Service *s);
	Client *Wait();
	int Loop(void);
	int Handle(Client *c);
};

class Service
{
public:
	int port;
	int type;
	Socket *sock;
	KloptServer *srv;
	vector<Client *> clts;
	map<string,Context *>contexts;

	Service(int port, int type);
	~Service();

	Socket *getSocket(void);
	vector<Client *> getClients(void);
	void setServer(KloptServer *srv);
	int Add(Client *c);
	int Del(Client *c);
	virtual intptr_t Handle(Client *c) = 0;
};

class Client
{
public:
	Socket *sock;
	Service *service;
    string msg;
	int state;
	time_t stamp;
	void *udata;

	Client (Socket *sock);
	~Client();
	Socket *getSocket(void);
	void setState(int state);
	void setService(Service *srvc);
	int Handle(void);
	int Del(void);
};

class Socket
{
public:
	int port;
	int fd;
	int type;
	int family;
	union {
		struct sockaddr sa;
		struct sockaddr_in sa4;
		struct sockaddr_in6 sa6;
	} addr;
	union {
		struct sockaddr sa;
		struct sockaddr_in sa4;
		struct sockaddr_in6 sa6;
	} peer;

	Socket (int type);
	Socket (int type,int fd);
	~Socket();
	
    std::string getPeerIp(void);
    std::string getAddrIp(void);
    std::string getIp(struct sockaddr_in);
	int setHostByName(const char *name);
	int reUse(void);
	int Listen(void);
	int Bind(short port);
	Socket *Dup(int fd);
	Socket *Accept(void);
    int Connect(struct sockaddr *);
	int getfd(void);
    bool Send(string s);
    std::string Recv(void);
};

class Cache
{
	Klopt k;
	int curx;
	int cury;
	vector<matrix_result_t>curres;
	string db;
	string collection;
public:
	Cache();
	~Cache();
	Cache(string db,string cl);
	matrix_result_t get(int fx, int fy,int,int);
	vector<matrix_result_t>get(int fx, int fy);
	bool set(int fx, int fy,int tx, int ty,route_result_t r);
};

class Network
{
public:
	Network(const char *loc);
	~Network();
};

class CommandService : public Service
{
	Klopt k;
	vector<prox_t> result;
public:
	int id;
	bool handle_geocode(json_object *content);
	vector<Location *> handle_setorders(string context, string orderset, string data);
	string handle_getdistances(string context, string orderset, string data);
	string handle_getmatrixrow(string context, string orderset, string data);
	CommandService(int port,int type);
	string FGetS(Client *c);
	intptr_t Handle(Client *c);

	string csv_report_setorders(vector<Location *>olist);
	string csv_report_getdistances(vector<Location *>olist);
	string csv_report_getmatrixrow(vector<Location *>olist,int row);
	string json_report(void);
};

class Statistics
{
    long nearest;
    long geo;
    long revgeo;
    long time;
    long dist;
    long route;
    long matrix;
    long matrixtime;
};

class JsonRpc
{
    Statistics nstats;   // number of runs
    Statistics tstats;   // time spent
public:
    string s; // the complete text
	int id;
	string method;
	json_object *content; 
	json_object *pars;
    bool fwd; // forwarded ?

	JsonRpc(string content);
	JsonRpc(int id, string command);
	~JsonRpc();

	JsonRpc(JsonRpc &orig);

	string get_method();
	json_object *get_param(string name);
	string serialize(string);
    string serialize(json_object *result);
	void error(string,string s);
    static json_object *err_ist(int errornumber, const char *errmsg);
    static json_object *rcv_ist(json_object *id, json_object *result, json_object *error);

};

class JsonService : public Service
{
	vector<prox_t> result;
public:
	Klopt k;
	int id;
	vector<Location *> handle_setorders(string context, string orderset, string data);
	//virtual string handle_ping(JsonRpc *message,Client *);
	virtual json_object *handle_ping(JsonRpc *message,Client *);
	virtual string handle_exit(JsonRpc *message,Client *clt);
	virtual string handle_status(JsonRpc *message,Client *);

	virtual json_object *handle_geocode(JsonRpc *message,Client *);
	virtual json_object *handle_revgeocode(JsonRpc *message,Client *);
	virtual string handle_getdistances(string context, string orderset, string data);
	virtual string handle_getmatrixrow(string context, string orderset, string data);
	virtual json_object *handle_route(JsonRpc *message,Client *);
	virtual json_object *handle_randomnodes(JsonRpc *message,Client *);
    virtual json_object *handle_tsp(JsonRpc *message,Client *);
    virtual json_object *handle_vrp(JsonRpc *message,Client *);
	JsonService(int port,int type);
	string FGetS(Client *c);
	intptr_t Handle(Client *c);
    json_object *Forward(JsonRpc *message, string host, int port);
    string StripJsonRpc(string in);

	virtual json_object *handle_nearest(JsonRpc *message,Client *);
	virtual json_object *handle_matrix(JsonRpc *message,Client *);

	virtual string csv_report_setorders(vector<Location *>olist);
	virtual string csv_report_getdistances(vector<Location *>olist);
	virtual string csv_report_getmatrixrow(vector<Location *>olist,int row);
	string json_report(void);
};

struct GeoResult 
{
	double x;
	double y;
};

class BagService : public Service
{
	vector<prox_t> result;
public:
	Klopt k;
	int id;
	bool handle_geocode(json_object *content);
	BagService(int port,int type);
	string FGetS(Client *c);
	intptr_t Handle(Client *c);

	string json_report(void);
	string csv_report(void);
};

// options, or'ed together:.. so keep them under #16 ;)
// include timings in the results
#define OPT_TIMINGS 	0x01
// return distances in results
#define OPT_METERS  	0x02
// return times in results
#define OPT_MINUTES 	0x04
// return path information in results
#define OPT_PATHS   	0x08
// do not used names (output) parameters: positions are important
#define OPT_POSITIONAL  0x10

void web_error(int id, int num, const char *msg);

/* socket.cpp */
/* network.cpp */
/* service.cpp */
/* haversine.cpp */
Mat<int32_t> *HaversineMatrix(int ndeps, coord_t *deps, int narrs, coord_t *arrs);
/* google.cpp */
prox_t GoogleGeocode(char *country, char *city, char *street, char *num, char *zipcode);
addr_t GoogleEdocoeg(double x, double y);
/* klopt.cpp */
/* address.cpp */
int init_address(char *name);
/* osrm.cpp */
prox_t OsrmNearest(coord_t find, int *dist);
coord_t *OsrmRandomNodes(int n);
route_result_t *OsrmRoute(Cache *cache, int nlocs, coord_t *locs);
int *OsrmMatrix(int nlocs, coord_t *locs);
void OsrmInit(const char *base);
void OsrmExit(void);
Mat<int32_t> *OsrmDistanceMatrix(int ndeps, coord_t *deps, int narrs, coord_t *arrs);
/* tsp.cpp */
tsp_result_t *TspLp(int nlocs, coord_t *locs, coord_t *start,coord_t *end,int timeout, int loopout);
tsp_result_t *TspLk(int nlocs, coord_t *locs, coord_t *start,coord_t *end,int timeout, int loopout);
/* vrp.cpp */
vrp_result_t *VrpSa(int norders, order_t *orders, int vcap, map<std::string,bool> heur,int timeout, int loopout);
/* bag.cpp */
/* context.cpp */
/* command.cpp */
/* json.cpp */
/* time.cpp */
/* properties.cpp */

//#ifndef GENERATING_PROTO
//#include "grid_proto.h"
//#endif
