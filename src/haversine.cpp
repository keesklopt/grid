//#include "fcgi_stdio.h"

#include <app.h>
//#include <grid.h>
#include <stdio.h>
#include <math.h>

class Haversine
{
	public:
	// Convert our passed value to Radians
	double ToRad( double nVal )
	{
    	return nVal * (M_PI/180);
	}

	double distance( double nLat1, double nLon1, double nLat2, double nLon2 )
	{       
    	double nRadius = 6371000.0; // Earth's radius in Kilometers
    	// Get the difference between our two points
    	// then convert the difference into radians
    	//printf("%f,%f,%f,%f\n", nLat1, nLon1, nLat2, nLon2);
	
    	double nDLat = ToRad(nLat2 - nLat1);
    	double nDLon = ToRad(nLon2 - nLon1);
       	 
    	// Here is the new line
    	nLat1 =  ToRad(nLat1);
    	nLat2 =  ToRad(nLat2);
       	 
    	double nA = pow ( sin(nDLat/2), 2 ) + cos(nLat1) * cos(nLat2) * pow ( sin(nDLon/2), 2 );
	
	
    	double nC = 2 * atan2( sqrt(nA), sqrt( 1 - nA ));
    	double nD = nRadius * nC;
	
    	//printf(" = %f\n", nD);
	
    	return nD; // Return our calculated distance in METERS !!
	}

	double distance( int32_t nLat1, int32_t nLon1, int32_t nLat2, int32_t nLon2 )
	{
		double x1 = (double)nLon1 / 10000000.0;
		double y1 = (double)nLat1 / 10000000.0;
		double x2 = (double)nLon2 / 10000000.0;
		double y2 = (double)nLat2 / 10000000.0;
			//printf("Trying %d (%d,%d)\n", n1->id,n1->x,n1->y);
		return distance(y1,x1,y2,x2);
	}

};


Mat<int32_t> *HaversineMatrix(int ndeps, coord_t *deps, int narrs, coord_t *arrs)
{
	double result;
	int x,y;

	Haversine haversine;
	Mat<int32_t> *mp = new Mat<int32_t>(ndeps,narrs);

	for(y=0; y< ndeps; y++) {
		for(x=0; x< narrs; x++) {
			result = haversine.distance(deps[y].y,deps[y].x,arrs[x].y,arrs[x].x);
			mp->Set(y,x,(int32_t)round(result));
		}
	}

	return mp;
}

Mat<int32_t> *HaversineRoute(int nlocs, coord_t *locs)
{
	double result;
	int l;

	Haversine haversine;
	Mat<int32_t> *mp = new Mat<int32_t>(1,nlocs);

	if (!locs || nlocs< 2) return mp;

	double fx = 100000.* locs[0].x;
	double fy = 100000.* locs[0].y;
	
	for(l=1; l< nlocs; l++) {
		result = haversine.distance(fy, fx, locs[l].y,locs[l].x);
		mp->Set(1,l,(int32_t)round(result));
		fy = locs[l].y;
		fx = locs[l].x;
	}

	return mp;
}

