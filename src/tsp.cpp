#include <app.h>
#include <glpk.h>

#include <lkconfig.h>
#include <memory.h>
#include <resource.h>
#undef INFINITY
#include <nn.h>
#include <length.h>
#include <read.h>
#include <decluster.h>
#include <kdtree.h>
#include <array.h>
#include <prng.h>
#include <jbmr.h>
#include <construct.h>
#include <lk.h>

#include <execinfo.h>
#include <signal.h>

#include <syslog.h>

extern void init_lk_main(void);

extern void(*sort)(void*a,size_t n,size_t es,int(*cmp)(const void*,const void*));

extern int candidate_expr,cand_nn_k,cand_nq_k,cand_del_d;

void foo_bar(glp_tree *tree, void *info)
{
	switch (glp_ios_reason(tree))
	{ 
		case GLP_IBRANCH:
		break;
		case GLP_IBINGO:
			printf("%d\n ", glp_ios_reason(tree));
		break;
		case GLP_ISELECT:
		break;
		default:
		/* ignore call for other reasons */
		break;
	}
	return;
}

int Tsp::add(int nlocs,coord_t *locs)
{
	this->locs = locs;
	this->nlocs=nlocs;

	return nlocs;
}

int print(int num, int *index, void *data)
{
	int y;
	for (int y=0; y< num; y++) {
		printf("%d,", index[y]);
	}
	printf("\n");
	return 0;
}

Permute::Permute(int n) 
{
	num = n;
	index = new int[num];
}

void Permute::go_deep(int curr, perm_fnc_t fnc, void *data) 
{
	//int x = fnc(NULL);
	if (curr == num-1) {
		//for (int y=0; y< num; y++) {
			//printf("%d,", index[y]);
		//}
		//printf("\n");
		fnc(num, index, data);
	} 

	for (int y=curr; y< num; y++) {
		int tmp = index[curr];
		index[curr] = index[y];
		index[y] = tmp;
		go_deep(curr+1,fnc,data);
		tmp = index[curr];
		index[curr] = index[y];
		index[y] = tmp;
	}
}

void Permute::go(perm_fnc_t fnc, void *data) 
{
	//int x = fnc(NULL);
	int x;

	for (x=0; x< this->num; x++) {
		this->index[x]=x;
	}

	go_deep(0,fnc,data);
}

void Permute::dmp(void) {
	go(print,NULL);
}

class MatTest
{
	public:
	int num;

	/* matrices with the center cut out, so numbered like this :
			1	2	3	4
		1   x   1   2   3
		2	4	x	5	6
		3	7	8	x	9
		4	10	11	12	x
		this class eases finding a row/col sequence number
		*/
	MatTest(int n)
	{
		num=n;
	}

	int dmp()
	{
		int x, y;

		for (y=1; y<= num; y++) {
			for (x=1; x<= num; x++) {
				printf("%d\t", get(x,y));
			}
			printf("\n");
		}
		return 0;
	}
	
	int total(void)
	{
		int n = num-1;
		return n*n;
	}

	int get(int x, int y)
	{
		x--;
		y--;
		int n=(y*num) + x;
		if(n%(num+1) == 0) return -1;
		return (n - (n/(num+1)) -1  ) +1;
	}
};

int Tsp::add(double x,double y)
{
	int nlocs = this->nlocs;

	this->nlocs++;
	if (!this->locs) {
		this->locs = (coord_t *)calloc(sizeof(coord_t),1);
	} else {
		this->locs = (coord_t *)realloc(this->locs,sizeof(coord_t)*this->nlocs);
	}
	this->locs[nlocs].x = (double)x;
	this->locs[nlocs].y = (double)y;

	return this->nlocs;
}

Tsp::Tsp(int nlocs, coord_t *locs, Mat<int32_t> *td)
{
	this->nlocs=nlocs;
	this->locs = locs;
	this->solution = (tsp_result_t *)calloc(sizeof(tsp_result_t),nlocs);

	this->prm = new class Permute(nlocs);
	//this->prm->dmp();

	init_lk_main();

	this->mat=td;
}

int calc(int n, int *index, void *data)
{
	int t;
	int total=0;
	int roundtrip=1; // for now, make round trip
	tsp_result_t tmp[n];
	Tsp *tsp = (Tsp *) data;

	for (t=0; t < n; t++) {
		int here=index[t];
		int there=index[t+1];
		if (t==n-1) {
			if (roundtrip) { 
				there = index[0]; // back to first location
			} else { 
				break;
			} 
		} 
		//printf("%d to %d\n", here, there);
		int dist = tsp->mat->Get(here,there);
		tmp[t].index=here;
		tmp[t].dist = dist;
		//printf("Adding %d to %d\n", dist, total);
		total += dist;
	}
	if (total < tsp->bestdist) {
		for (t=0; t < n; t++) {
			printf("Setting %d to %d\n", t, index[t]);
			int here=index[t];
			int there=index[t+1];
			tsp->solution[t]= tmp[t];
		}
		tsp->bestdist=total;
		printf("Best so far : %d\n", tsp->bestdist);
	}
	return 0;
}

void Tsp::run(void)
{
	int initial=0;
	int y;

	for(y=0; y< this->nlocs; y++) {
		//mat_set(this->solution,y,0,y);
		//prm_add(this->prm, (void *)&y);
	}

	this->bestdist = INT_MAX;
	this->prm->go(calc, (void *)this);
}

tsp_result_t *TspExhaustive(int nlocs, coord_t *locs, coord_t *start,coord_t *end,int timeout, int loopout)
{
	double result;
	int x,y;
	int d;
	
	//Mat<int32_t> *mp = mat_ist(nlocs,1);
	Mat<int32_t> *dist = OsrmDistanceMatrix(nlocs, locs, nlocs, locs);

	dist->Dmp();

	Tsp *tsp = new Tsp(nlocs,locs,dist);
	tsp->run();

	//return tsp->solution;
	return tsp->solution;
}

extern int *tour;
/*
extern length_t(*cost)(const int,const int);
length_t cost_from_matrix(const int i,const int j);
tsp_instance_t *tsp_instance;
length_t incumbent_len;
long random_seed;
int representation=REP_ARRAY,construction_algorithm = CONSTRUCT_GREEDY_RANDOM;


int verbose,iterations,should_show_tour,should_show_version;
long start_heuristic_param;
char*PostScript_filename,*lower_bound_name,*upper_bound_name;
int noround;
double upper_bound_value,lower_bound_value;
int extra_backtrack;
*/


length_t
matrix_cost(const int i,const int j)
{
	return(length_t)tsp_instance->edge_weights[i][j];
}

/*
int(*tour_next)(int) = array_next;
int(*tour_prev)(int);
int(*tour_between)(int,int,int);
void(*tour_flip)(int,int,int,int);
void(*tour_set)(int const*);
void(*tour_setup)(int n);
void(*tour_cleanup)(void);


int*original_city_num= NULL;
nt begin_data_structures_mark;
int max_generic_flips;
*/

extern int last_resource_mark;

tsp_result_t *TspLk(int nlocs, coord_t *locs, coord_t *start,coord_t *end,int timeout, int loopout)
{
	micro_t timer;
	micro_t starttime;
	micro_t endtime;
	length_t validate_len;

	// just does not work well with low number, return optimum with LP
	if (nlocs<6) {
		return TspLp(nlocs,locs,start,end,timeout,loopout);
	}

	init_lk_main();

	tsp_result_t *solution = (tsp_result_t *)calloc(sizeof(tsp_result_t),nlocs+1);
	verbose=0;
	double double_validate_len,ordered_double_len,raw_len;

	last_resource_mark= resource_mark("Reading the instance");

	tsp_instance = (tsp_instance_t *)calloc(sizeof(tsp_instance_t),1);
	candidate_expr= CAND_NN;
	cand_nn_k= 2;
	cand_nq_k= 5;
	cand_del_d= 3;

	tour_next= array_next;
	tour_prev= array_prev;
	tour_between= array_between;
	tour_flip= array_flip;
	tour_set= array_set;
	tour_setup= array_setup;
	tour_cleanup= array_cleanup;

	jbmr_setup(nlocs);

	mem_usage_reset();  /* Start memory counter at zero. */
	resource_setup(50);

	tour_setup(nlocs);

	starttime = micro_now();
	//Mat<int32_t> *orig = HaversineMatrix(nlocs, locs, nlocs, locs);
	//Mat<int32_t> *orig = OsrmDistanceMatrix(nlocs, locs, nlocs, locs);
	int *m = OsrmMatrix(nlocs, locs);
	endtime= micro_now();

    for (int x=0; x< nlocs; x++) { 
        for (int y=0; y< nlocs; y++) { 
            printf("[%d,%d] => %d\n", y,x, m[y*nlocs+x]);
        }
    }

	timer = endtime - starttime;

	// now time tsp 
	starttime= micro_now();

	tsp_instance->name=(char *)"lk";
	tsp_instance->input_n = tsp_instance->n= nlocs;
	tsp_instance->edge_weight_type=EXPLICIT;
	//tsp_instance->edge_weight_format=LOWER_DIAG_ROW;
	tsp_instance->edge_weight_format=FULL_MATRIX; // would expect !!
	tsp_instance->edge_weights= new_arr_of(length_t*,tsp_instance->input_n);

	cost = matrix_cost;

	int row;
	for(row= 0;row<tsp_instance->input_n;row++){
		tsp_instance->edge_weights[row]= new_arr_of(length_t,tsp_instance->input_n);
	}
	
	// turn it into a symmetric problem 
	// lk can't do asymmetric,
	int col;
	for(row= 0;row<tsp_instance->input_n;row++){
		for(col= 0;col<tsp_instance->input_n;col++){
            int dist = m[row*nlocs+col];
			tsp_instance->edge_weights[row][col] = dist;
			tsp_instance->edge_weights[col][row] = dist;
		}
	}

	//E2_create(tsp_instance);

	decluster_tree_t *mst;
	mst = decluster_setup(nlocs);
	length_t mst_len= decluster_mst(tsp_instance,mst);


/*
{
int i;
const int m= mst->n;
for(i= 0;i<m;i++) 
printf("e %d %d "length_t_native_spec"\n",
st->edge[i].city[0]+1,
mst->edge[i].city[1]+1,
length_t_native_pcast(mst->edge[i].cost));
}
*/

	decluster_preprocess(mst);


	nn_build(
	(candidate_expr&CAND_NN)?cand_nn_k:0,
	(candidate_expr&CAND_NQ)?cand_nq_k:0,
	(candidate_expr&CAND_DEL)?cand_del_d:0);

	tour= new_arr_of(int,nlocs);
incumbent_len=
construct(nlocs,tour,construction_algorithm,start_heuristic_param,random_seed);
//printf("Initial tour length: "length_t_spec"\n",length_t_pcast(incumbent_len));

	int i;
	for(i= 0;i<nlocs;i++){
		const int city= tour[i],next_city= tour[(i+1)%nlocs];
		//fprintf(stdout,"%f %f %f %f ue\n",
		//tsp_instance->coord[city].x[0],
		//tsp_instance->coord[city].x[1],
		//tsp_instance->coord[next_city].x[0],
		//tsp_instance->coord[next_city].x[1]);
	}

	tour_set(tour);

	prng_t*prng= prng_new(PRNG_DEFAULT,1998^random_seed);

	last_resource_mark= resource_mark("Lin-Kernighan");

	jbmr_run(2000,prng);

	int c;
printf("Tour:\n");
for(i= 0,c= 0;i<nlocs;i++,c= tour_next(c)){
	solution[i].index = (original_city_num?original_city_num[c]:c);
printf("%d ",(original_city_num?original_city_num[c]:c)+1);

//if((i%10)==9||i==nlocs-1)printf("\n");
}
	endtime= micro_now();
	solution->matrixtime=timer;
	solution->tsptime=endtime-starttime;

	return solution;
}

int SplitProblem(Mat<int32_t> *original, Mat<int32_t> **usablemat, Mat<int32_t> **errormat, int **translate)
{
	int nlocs = original->cols;
	int ulocs = 0;
	int elocs = 0;
	int x,y;

	int usability[nlocs];

	for (x = 0; x< nlocs; x++) 
		usability[x] = 0;
	for (x = 0; x< nlocs; x++) { 
		for (y = 0; y< nlocs; y++) { 
			if (x==y) continue;
			double dist = original->Get(x,y);
			if (dist>=0) { 
				usability[x]++;
				usability[y]++;
			} 
		} 
	}
	for (x = 0; x< nlocs; x++) { 
		printf("[%d] %d\n", x, usability[x]);
		if (usability[x] > 0) 
			ulocs++;
	}
	*translate= (int *)calloc(sizeof(int),ulocs);
	elocs = nlocs-ulocs;
	*usablemat = new Mat<int32_t>(ulocs,ulocs);
	*errormat  = new Mat<int32_t>(1,elocs);

	int uy=0;
	int ey=0;
	for (y = 0; y< nlocs; y++) { 
		if (usability[y] <= 0) {
			(*errormat)->Set(0,ey,y);
			ey++;
			continue;
		} 
		(*translate)[uy] = y;
		int ux=0; 
		for (x = 0; x< nlocs; x++) { 
			if (usability[x] > 0) {
				double dist = original->Get(x,y);
				(*usablemat)->Set(ux,uy,dist);
				ux++;
			} 
		} 
		uy++;
	}

	return ulocs;
}

tsp_result_t *TspLp(int nlocs, coord_t *locs, coord_t *start,coord_t *end,int timeout, int loopout)
{
	double result;
	int x,y;
	int d;
	micro_t timer;
	micro_t starttime;
	micro_t endtime;

	for (d=0; d< nlocs; d++) {
		printf("%f,%f\n", locs[d].x, locs[d].y);
	}
	starttime= micro_now();
	Mat<int32_t> *orig = OsrmDistanceMatrix(nlocs, locs,nlocs, locs);
	endtime= micro_now();

	timer = endtime - starttime;

	Mat<int32_t> *mp;
	Mat<int32_t> *err;
	int *trans;

	int nusable = SplitProblem(orig,&mp,&err,&trans);
	tsp_result_t *solution = (tsp_result_t *)calloc(sizeof(tsp_result_t),nlocs+1);
	if (nlocs<1) return solution;

	printf("original : \n");
	orig->Dmp();
	printf("smaller : \n");
	mp->Dmp();
	printf("error : \n");
	err->Dmp();

	for (int t=0; t< nusable; t++)
		printf("trans %d -> %d\n", t, trans[t]);

	starttime= micro_now();

    glp_prob *lp; 
	glp_tran *tran= NULL;
	int ret=0, r, R;
	glp_smcp params;
	glp_iocp iocp;
	glp_bfcp bfcp;
	glp_iptcp iptcp;

    lp = glp_create_prob();
	glp_get_bfcp(lp,&bfcp);
	glp_init_smcp(&params);
	glp_init_iocp(&iocp);
	glp_init_iptcp(&iptcp);

	bfcp.type = GLP_BF_FT;

	params.presolve=GLP_ON;
	params.meth=GLP_PRIMAL;
	params.r_test=GLP_RT_HAR;
	params.pricing=GLP_PT_PSE;

	iptcp.ord_alg = GLP_ORD_AMD;

	iocp.br_tech = GLP_BR_DTH;
	iocp.binarize = GLP_ON;
	iocp.presolve = GLP_ON;
	//iocp.tm_lim=10000;
	iocp.cb_func=foo_bar;
	if (timeout>0) iocp.tm_lim=timeout;
	//if (loopout>0) iocp.it_lim=loopout;

#define MANUAL 1
#ifdef MANUAL

	if (nusable<2) return solution;

	int nperm = nusable*(nusable-1);
	int ncols = 6 * nperm;
	int nrows = nperm+nusable*3;

	int ia[ncols+1], ja[ncols+1];
	double ar[ncols+1], z, x1, x2, x3;

	MatTest m(nusable);
	m.dmp(); 
	glp_set_prob_name(lp, "tsp");
	glp_set_obj_dir(lp, GLP_MIN);
	glp_add_rows(lp, nrows);
	//glp_set_row_name(lp, 1, "total");
	glp_set_obj_name(lp, "total");
	glp_set_row_bnds(lp, 1, GLP_FR, 0.0, 0.0);

	static char name[30];

	glp_add_cols(lp,nperm * 2);

	int count=1;
	for (r=1; r<= nusable; r++) {
		for (int r2=1; r2<= nusable; r2++) {
			if (r == r2) continue;
			name[0]='\0';
			snprintf(name,30,"x(%d,%d)", r, r2);
			glp_set_col_name(lp,count,name);
			glp_set_col_kind(lp,count,GLP_BV);
			glp_set_col_bnds(lp,count,GLP_DB, 0.0,1.0);

			double dist = mp->Get(r-1,r2-1);
		//	printf("Dist %f\n", dist);
			glp_set_obj_coef(lp,count,dist);
			count++;
		}
	}
	int nc=nperm + 1;
	for (r=1; r<= nusable; r++) {
		for (int r2=1; r2<= nusable; r2++) {
			if (r == r2) continue;
			name[0]='\0';
			snprintf(name,30,"y(%d,%d)", r, r2);
			glp_set_col_name(lp,nc,name);
			glp_set_col_bnds(lp,nc,GLP_DB,0.0,(nusable-1)*1.0);
			nc++;
		}
	}

	count=1;
	for (r=1; r<= nusable; r++) {
		snprintf(name,30,"leave(%d)", r);
		glp_set_row_name(lp,count,name);
		glp_set_row_bnds(lp,count,GLP_FX,1.0,1.0);
		count ++;
	}
	for (r=1; r<= nusable; r++) {
		snprintf(name,30,"enter(%d)", r);
		glp_set_row_name(lp,count,name);
		glp_set_row_bnds(lp,count,GLP_FX,1.0,1.0);
		count ++;
	}

	for (r=1; r<= nusable; r++) {
		for (int r2=1; r2<= nusable; r2++) {
			if (r==r2) continue;
			snprintf(name,30,"cap(%d,%d)", r,r2);
			glp_set_row_name(lp,count,name);
			glp_set_row_bnds(lp,count,GLP_UP,-0.0,-0.0);
			count++;
		}
	}

	for (r=1; r<= nusable; r++) {
		snprintf(name,30,"node(%d)", r);
		glp_set_row_name(lp,count,name);
		if (r==1)
			glp_set_row_bnds(lp,count,GLP_FX,-1.0*(nusable-1),-1.0*(nusable-1));
		else 
			glp_set_row_bnds(lp,count,GLP_FX,1.0,1.0);
		count++;
	}

	// reset  (once, for the cols this time)
	count=1;
	for (r=1; r<= nusable; r++) {
		for (int r2=1; r2<= nusable; r2++) {
			if (r==r2) continue;
			ia[count] = r;
			ja[count] = m.get(r2,r);
			ar[count] = 1;
			count++;
		}
	}


	for (r=1; r<= nusable; r++) {
		for (int r2=1; r2<= nusable; r2++) {
			if (r==r2) continue;
			ia[count] = r+nusable;
			ja[count] = m.get(r,r2);
			ar[count] = 1;
			//printf("Setting %d %d\n", r, ja[count]);
			count++;
		}
	}

	int row=(nusable*2)+1;
	for (r=1; r<= nusable; r++) {
		for (int r2=1; r2<= nusable; r2++) {
			if (r==r2) continue;
			ia[count] = row;
			ja[count] = m.get(r2,r);
			ar[count] = -1.0*(nusable-1);
			ia[count+nperm] = row;
			ja[count+nperm] = m.get(r2,r)+nperm;
			ar[count+nperm] = 1.0;
			//printf("Setting %d %d\n", count, ja[count]);
			count++;
			row++;
		}
	}

	count+= nperm;

	for (r=1; r<= nusable; r++) {
		for (int r2=1; r2<= nusable; r2++) {
			if (r==r2) continue;
			ia[count] = row;
			ja[count] = m.get(r2,r)+nperm;
			ar[count] = -1.0;
			ia[count+nperm] = row;
			ja[count+nperm] = m.get(r,r2)+nperm;
			ar[count+nperm] = 1.0;
			//printf("Setting %d %d\n", count, ja[count]);
			count++;
		}
		row++;
	}
	count+= nperm;
	//printf("Now : %d %d\n", count, ncols);

	//printf("C : %d\n", count);
	glp_load_matrix(lp,count-1,ia,ja,ar);

	//glp_write_lp(lp,NULL, "freemps");
#else

	glp_std_basis(lp);
	tran = glp_mpl_alloc_wksp();
	ret  = glp_mpl_read_model(tran, "tsp.mod", 1);
	if (ret != 0)
	{ 
		fprintf(stderr, "Error on translating model\n");
		goto skip;
	}
	ret  = glp_mpl_read_data(tran, "data4.dat");
	if (ret != 0)
	{ 
		fprintf(stderr, "Error on translating data\n");
		goto skip;
	}
	ret = glp_mpl_generate(tran, NULL);
	if (ret != 0)
	{ 
		fprintf(stderr, "Error on generating model\n");
		goto skip;
	}
	glp_mpl_build_prob(tran, lp);
	ret = lpx_print_mip(lp, "egypt.mps");

	//glp_write_lp(lp,NULL, "freemps");

	R = glp_get_num_rows(lp);
	//for (r=0; r< R; r++) {
		//glp_get_row(lp, r);
		//printf("Solutions is %f\n", glp_mip_row_val(lp,r));
	//}
	//glp_write_mps(lp,GLP_MPS_FILE, NULL, "freemps");
#endif
	glp_sort_matrix(lp);
	glp_scale_prob(lp,GLP_SF_GM | GLP_SF_EQ | GLP_SF_2N);
	glp_create_index(lp);
	glp_intopt(lp, &iocp);
	//glp_mpl_postsolve(tran,lp,GLP_SOL);
	if (ret != 0)
		fprintf(stderr, "Error on writing MPS file\n");

	R = glp_get_num_rows(lp);
	//printf("R is %d\n", R);

	r= glp_find_row(lp, "total");
	printf("Solution is %f\n", glp_mip_obj_val(lp));

	//lpx_print_mip(lp,"/var/tmp/result");
	//lpx_print_sol(lp,"/var/tmp/result");
	//lpx_print_sens_bnds(lp,"resultbnd");

	int total = glp_mip_obj_val(lp);
	total = 0;
	solution[0].index = trans[0];
	int s=1;
	r=1;
	do {
		for (int r2=1; r2<= nusable; r2++) {
			if (r==r2) continue;
			count = m.get(r2,r);
			//printf("[%d][%d] is %f\n", r,r2,glp_mip_col_val(lp,count));
			if (glp_mip_col_val(lp,count) > 0.0) {
				solution[s].index = trans[r2-1];
				int dist = mp->Get(r-1,r2-1);
				solution[s].dist = dist;
				total += dist;
				r=r2;
				s++;
				break;
			}
		}
	} while (r!=1);
	//for (r=1; r<= nusable; r++) {
		//for (int r2=1; r2<= nusable; r2++) {
			//if (r==r2) continue;
			//count = m.get(r2,r);
			//printf("[%d][%d] is %f\n", r,r2,glp_mip_col_val(lp,count));
			//if (glp_mip_col_val(lp,count) > 0.0) {
				//solution[trans[r-1]].index = r2-1;
				//int dist = mat_get(mp,r-1,r2-1);
				//solution[trans[r-1]].dist = dist;
				//total += dist;
			//}
		//}
	//}
	for (r=0; r< err->cols; r++) {
		int e = err->Get(0,r);
		solution[e].index = -1;
		solution[e].dist = -1;
	}
	//printf("Setting %d to %d\n", nlocs, total);
	solution[nlocs].index=0;
	solution[nlocs].dist=total;

	//glp_mpl_free_wksp(tran);
	glp_delete_prob(lp);

	endtime= micro_now();
	solution->matrixtime=timer;
	solution->tsptime=endtime-starttime;

	// maybe later ? create the problem manually
    //glp_set_prob_name(lp, "tsp");
    //glp_set_obj_dir(lp,GLP_MIN); // minimize cost
	//glp_add_rows(lp,3);
	//glp_set_row_name(lp, 1, "n"); 	// num nodes
	//glp_set_row_bnds(lp, 1, GLP_UP, 0.0, 100.0);

	return solution;
}
