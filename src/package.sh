#!/bin/bash

rm -rf package
mkdir -p package/usr/local/bin
mkdir -p package/usr/local/etc
mkdir -p package/usr/share/klopt
cp ./klopt_rte package/usr/local/bin
cp ./klopt_geo package/usr/local/bin
cp ./klopt_srv package/usr/local/bin
pushd package
fpm -s dir -t deb -n "klopt-bin" -v 1.0 .
popd
