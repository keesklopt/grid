/* mainly because fastci and mongo db bite eachother
	but also for convenience, and for smoorther debugging
	tomcat(&) was tried, but gave to often an out of memory error
	and of course the main code is C 
	A standalone server version
 */
#include <vector>
#include <app.h>
#include <fcntl.h>

#include <execinfo.h>
#include <signal.h>

#include <syslog.h>

#include <unicode/uclean.h>

#define MAXHOSTNAMELEN 64

using namespace std;

class JsonGeoService : public JsonService
{
	vector<prox_t> result;
public:
	int id;
	json_object *handle_ping(JsonRpc *message,Client *);
	//string handle_status(JsonRpc *message,Client *);
	json_object *handle_geocode(JsonRpc *message,Client *);
	json_object *handle_revgeocode(JsonRpc *message,Client *);

	JsonGeoService(int port,int type);
	//string FGetS(Client *c);
	//intptr_t Handle(Client *c);

	//string handle_nearest(JsonRpc *message,Client *);
	//string handle_matrix(JsonRpc *message,Client *);

	//string csv_report_setorders(vector<Location *>olist);
	//string csv_report_getdistances(vector<Location *>olist);
	//string csv_report_getmatrixrow(vector<Location *>olist,int row);
	//string json_report(void);
};

JsonGeoService::JsonGeoService(int port,int type) : JsonService(port,type) 
{
	this->id=0;
} 

json_object *JsonGeoService::handle_ping(JsonRpc *message,Client *clt)
{
	int len;
    json_object *result = json_object_new_object();
    json_object *pong   = json_object_new_string("pong");

    json_object_object_add(result, "method", pong);
    //json_object_object_add(base, "result", result);

	k.count("ping",1,1);
    // note for the originator addres (ip) we need to pass it 
    // along in ws.wsgi or it will be localhost, later
	k.log("ping",1,clt->sock->getPeerIp());

	return result;
} 

/*
string JsonGeoService::handle_ping(JsonRpc *message,Client *clt)
{
	string ss;
    string subsrv;
    
	int len;
	char workbuf[1024];

	workbuf[0] = '\0';
	sprintf(workbuf,"\"result\":\"pong\"");
	ss = workbuf;

	k.count("ping",1,1);
    // note for the originator addres (ip) we need to pass it 
    // along in ws.wsgi or it will be localhost, later
	k.log("ping",1,clt->sock->getPeerIp());

	return ss;
} 
*/


/* reverse geocode : locations => address */
json_object *JsonGeoService::handle_revgeocode(JsonRpc *message,Client *clt)
{
	static char erbuf[2048];
	static char buf[40960];
	json_object *options=NULL;
	json_object *coord;
	json_object *locations=NULL;
    micro_t timings[3];
	bool membernames=false;
	int meters=0;  // default : don't use
	bool matchname=false;
	bool dotimes=false; 
    bool getpand=false;
    poly_t *pand=NULL;

	json_object *pc=NULL;
	json_object *jx=NULL;
	json_object *jy=NULL;
    int opt, b;
	int x, y;
	this->id=666;

	options = message->get_param("options");
	if (options) { 
		json_object *opt;
		json_bool testopt;
		testopt = json_object_object_get_ex(options, "timings",&opt); 
        b = json_object_get_boolean(opt);

		if (testopt && b) 
			dotimes = true;
		testopt = json_object_object_get_ex(options, "membernames",&opt); 
        b = json_object_get_boolean(opt);
		if (testopt && b) 
			membernames = true;
		testopt = json_object_object_get_ex(options, "maxmeters",&opt); 
        b = json_object_get_boolean(opt);
		if (testopt) 
			meters = json_object_get_int(opt);
		testopt = json_object_object_get_ex(options, "matchname",&opt); 
        b = json_object_get_boolean(opt);
		if (testopt && b) 
			matchname = true;
		testopt = json_object_object_get_ex(options, "getpand",&opt); 
        b = json_object_get_boolean(opt);
		if (testopt && b) {
			getpand = true;
            pand = new poly_t();
        } 
	} 

    locations = message->get_param("locations");
	if (!locations) {
        json_object *errsmsg = create_error(error_bad_param, "reverse geocode function needs locations");
		return errsmsg;
    } 

	int rows = json_object_array_length(locations);
	int len=0;

	json_object *jo = json_object_new_object();
	json_object *addresses = json_object_new_array();

	json_object_object_add(jo, "addresses", addresses);

	for (x=0; x< rows; x++) {

		coord = json_object_array_get_idx(locations, x);

		if (json_object_get_type(coord) == json_type_object) {
			json_object_object_get_ex(coord, "x",&jx);
			json_object_object_get_ex(coord, "y",&jy);
		} else if (json_object_get_type(coord) == json_type_array) { 
			jx = json_object_array_get_idx(coord, 0);
			jy = json_object_array_get_idx(coord, 1);
		} else { 
        	json_object *errsmsg = create_error(error_type, "coordinate member should be an array or object");
			return errsmsg;
    	} 

		if (!jx || !jy) { 
        	json_object *errsmsg = create_error(error_type, "no x and y members found in coordinate");
			return errsmsg;
		} 
		double sx=json_object_get_double(jx);
		double sy=json_object_get_double(jy);

		addr_t res= k.KloptRevGeocode (sx,sy,meters,&timings[0],pand);

		json_object *elm = json_object_new_object();
		json_object *street = json_object_new_string(res.street.c_str());
		json_object *city = json_object_new_string(res.city.c_str());
		json_object *country = json_object_new_string(res.country.c_str());
		json_object *zipcode = json_object_new_string(res.zipcode.c_str());
		json_object *number = json_object_new_string(res.number.c_str());

		json_object_object_add(elm, "street", street);
		json_object_object_add(elm, "city", city);
		json_object_object_add(elm, "country", country);
		json_object_object_add(elm, "zipcode", zipcode);
		json_object_object_add(elm, "number", number);
        if (getpand) { 
		    json_object *npts = json_object_new_int(pand->npoints);
		    json_object *pts = json_object_new_string(pand->points.c_str());
		    json_object_object_add(elm, "npoints", npts);
		    json_object_object_add(elm, "points", pts);
        } 

		json_object_array_add(addresses, elm);
	}
	//ss << "]"; // end locations array

    if (dotimes) { 

        json_object *tim = json_object_new_object();

        json_object *locate = json_object_new_int(timings[0]);
        json_object *address = json_object_new_int(timings[1]);
        json_object *street = json_object_new_int(timings[2]);
        json_object *total = json_object_new_int(timings[0] + timings[1] + timings[2]);

        json_object_object_add(jo, "timings", tim);
        json_object_object_add(tim, "locate", locate);
        json_object_object_add(tim, "address", address);
        json_object_object_add(tim, "street", street);
        json_object_object_add(tim, "total", total);
    } 


	k.count("addresses",1,rows);
	k.log("addresses",rows,clt->sock->getPeerIp());

    //syslog(LOG_INFO, "geo returns : %s\n", ss.str().c_str());
	return jo;
}

/* geocode : address => geo locations */
json_object *JsonGeoService::handle_geocode(JsonRpc *message,Client *clt)
{
	static char erbuf[2048];
	static char buf[40960];
	json_object *options=NULL;
	json_object *addresses=NULL;

	json_object *pc=NULL;
	json_object *postcode=NULL;
	json_object *nummer=NULL;
	json_object *stad=NULL;
	json_object *straat=NULL;
	json_object *land=NULL;
	bool membernames=false;
	bool matchname=false;
	bool dotimes=false; 
    bool b;
	int x, y;
	this->id=666;
	micro_t starttime;
	micro_t endtime;

	options = message->get_param("options");
	if (options) { 
		json_object *opt;
		json_bool testopt;
		testopt = json_object_object_get_ex(options, "timings",&opt); 
        b = json_object_get_boolean(opt);

		if (testopt && b) 
			dotimes = true;
		testopt = json_object_object_get_ex(options, "membernames",&opt); 
        b = json_object_get_boolean(opt);
		if (testopt && b) 
			membernames = true;

		testopt = json_object_object_get_ex(options, "matchname",&opt); 
        b = json_object_get_boolean(opt);
		if (testopt && b) 
			matchname = true;
	} 

    addresses = message->get_param("addresses");
	if (!addresses) {
        json_object *errsmsg = create_error(error_bad_param, "geocode function needs addresses");
		return errsmsg;
	}

	int rows = json_object_array_length(addresses);
	int len=0;

	starttime = micro_now();

	json_object *jo= json_object_new_object();
	json_object *locs= json_object_new_array();

    json_object_object_add(jo, "locations", locs);

	for (x=0; x< rows; x++) {
		pc = json_object_array_get_idx(addresses,x);
		json_object_object_get_ex(pc, "zipcode",&postcode); 
		json_object_object_get_ex(pc, "number",&nummer);
		json_object_object_get_ex(pc, "city",&stad);
		json_object_object_get_ex(pc, "street",&straat);
		json_object_object_get_ex(pc, "country",&land);
		const char *zip=NULL;
		const char *house=NULL;
		const char *street ="";
		const char *city ="";
		const char *country ="NL";

		if (postcode) zip = json_object_get_string(postcode);
		if (nummer) house = json_object_get_string(nummer);
		if (straat) street = json_object_get_string(straat);
		if (stad) city = json_object_get_string(stad);

		prox_t res= k.KloptGeocode
			(country,city,street,house,zip);

        json_object *elm = json_object_new_object();
        json_object *x = json_object_new_double(res.x);
        json_object *y = json_object_new_double(res.y);

        json_object_array_add(locs, elm);
        json_object_object_add(elm, "x", x);
        json_object_object_add(elm, "y", y);

        // add pand geometry
	}

	endtime = micro_now();

    if (dotimes) { 
        json_object *tim = json_object_new_object();

        json_object *locations = json_object_new_int(endtime-starttime);

        json_object_object_add(jo, "timings", tim);
        json_object_object_add(tim, "locations", locations);
    } 

	//ss << "}"; // end result

	k.count("locations",1,rows);
	k.log("locations",rows,clt->sock->getPeerIp());

    //syslog(LOG_INFO, "revgeo returns : %s\n", ss.str().c_str());
	return jo;
}


KloptServer::KloptServer(int backlog)
{
	backlog=backlog;
}

void KloptServer::Add(Service *s) {
	services.push_back(s);
	s->setServer(this);
} 

Client *KloptServer::Wait()
{
	int t,u, res,max=0;
    fd_set read_fd;
  	fd_set err_fd;
   	Client *cpp;
   	Client *nc;
   	Service *spp;
   	Socket *ns;
   	int sock;

	FD_ZERO(&err_fd);
   	FD_ZERO(&read_fd);

	// count highest socket fd (for select)
	for (vector<Service *>::iterator it = services.begin();
		it != services.end(); it++) {
		spp = (Service *)*it;
		ns = spp->getSocket();
		sock = ns->getfd();
		if (sock > max) max = sock;
		FD_SET(sock, &err_fd);
       	FD_SET(sock, &read_fd);
		//printf("Clients : %ld\n", spp->clts.size());
		for (vector<Client *>::iterator cit = spp->clts.begin();
			cit != spp->clts.end(); cit++) {
			cpp = (Client *) *cit;
			ns = cpp->getSocket();
			sock = ns->getfd();
			if (sock > max) max = sock;
           	FD_SET(sock, &err_fd);
           	FD_SET(sock, &read_fd);
		} 
	} 

again: // here comes a loop
	//printf("waiting %d\n", max);
	res = select(max + 1, &read_fd, NULL, &err_fd, NULL);
	//printf("waited %d %d %d\n", res, FD_ISSET(sock, &read_fd), FD_ISSET(sock, &err_fd));

	if (res < 0) {
		//printf("retrying with res is %d, err_fd = %d\n", res, err_fd);
		// this is probably one of the child signals, just retry
		goto again;
	}

	for (vector<Service *>::iterator it = services.begin();
		it != services.end(); it++) {
		spp = (Service *)*it;
		ns = spp->getSocket();
		sock = ns->getfd();
		if  (FD_ISSET(sock, &err_fd)) {
			printf("heel jammer\n");
		}
		if  (FD_ISSET(sock, &read_fd)) {
			ns = ns->Accept();
			if (!ns) {
				perror("accept");
				return NULL;
			} else {
				int c=0;
				nc =  new Client(ns);
				nc->setState(CLT_INIT);
				//if (spp->sock->type == SOCK_STREAM)
				c = spp->Add(nc);
				//printf("a: now %d clients \n", c);
				nc->setService(spp);
				return nc;
			}
		}

		for (vector<Client *>::iterator cit = spp->clts.begin();
			cit != spp->clts.end(); cit++) {
			cpp = (Client *) *cit;
			cpp->setState(CLT_RUNNING);
			ns = cpp->getSocket();
			sock = ns->getfd();
			if  (FD_ISSET(sock, &read_fd)) {
				return cpp;
			}
			if  (FD_ISSET(sock, &err_fd)) {
				printf("jammer");
			}
		}
	}
	printf("fell through!\n");
	return NULL;
}

int KloptServer::Handle(Client *c)
{
	intptr_t ret;
	int count;

	ret = c->Handle();

	if (ret == CS_CONT) return 0;

	count = c->Del();
	delete c;

	if (ret == CS_DONE) {
		//printf("del : now %d clients left\n", count);
        printf("[GEO]: ------ waiting for input\n");
		return 0;
	}

	return 1; // CS_STOP remains : signal server stop 
}

int KloptServer::Loop(void)
{
	int stop=0;
	while(!stop) { 
		Client *c = Wait();
		stop = Handle(c);
		//stop = c->Handle();
	} 

	return stop;
}

class TestServer : public KloptServer
{
public:
	TestServer(int backlog) : KloptServer(backlog) {
	}
};

class HelloService : public Service
{
public:
	HelloService(int port, int type) : Service(port, type)
	{
	} 

	intptr_t Handle(Client * c)
	{
		printf("HelloService\n");
		return 0;
	}
};

int main(int argc, char *argv[])
{
	TestServer ts(5);

    openlog("klopt.GEO", 0,LOG_NOTICE);
    syslog(LOG_NOTICE, "Service started");

	//CommandService s(6670,SOCK_STREAM);
	JsonGeoService j(SOCK_GEO,SOCK_STREAM);

	//ts.Add(&s);
	ts.Add(&j);
	ts.Loop();

    u_cleanup();

	return 0;
}
