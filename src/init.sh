#!/bin/bash

mongo bag --eval 'db.statistics.drop()';
mongo bag --eval 'db.statistics.insert({"operation":"ping","count":0})';
mongo bag --eval 'db.statistics.insert({"operation":"nearest","count":0})';
mongo bag --eval 'db.statistics.insert({"operation":"geo","count":0})';
mongo bag --eval 'db.statistics.insert({"operation":"revgeo","count":0})';
mongo bag --eval 'db.statistics.insert({"operation":"route","count":0})';
mongo bag --eval 'db.statistics.insert({"operation":"matrix","count":0})';
mongo bag --eval 'db.statistics.insert({"operation":"tsp","count":0})';
