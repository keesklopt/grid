<IfModule mod_fastcgi.c>
    AddHandler fastcgi-script .fcgi .fcg
    FastCgiServer /var/www/grid/mat_nl_car -idle-timeout 1000 -processes 1
    ScriptAlias /nl_car "/var/www/grid/mat_nl_car"
</IfModule>
