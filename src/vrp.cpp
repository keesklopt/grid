#include <iostream>
#include <stdio.h>

#include <VRPH.h>
#include <app.h>

#include <execinfo.h>
#include <signal.h>

#include <syslog.h>

using namespace std;

vrp_result_t *VrpSa(int norders, order_t *orders, int vcap, map<std::string,bool> heur,int timeout, int loopout)
{
    printf("Run vrp sa variant\n");
    vrp_result_t *res= (vrp_result_t *)calloc(sizeof(vrp_result_t),norders);
    coord_t *locs = (coord_t *)calloc(sizeof(coord_t),norders);

	int dimension=norders-1;
	int numdays=1; // can we make timeslots (2 hour/ hour / less ?)

	int *my_sol_buff;
	int *final_sol;
	int heuristics=0; // default

    if (heur["one_pt_move"] == true) 
        heuristics |= ONE_POINT_MOVE;
    if (heur["two_pt_move"] == true) 
        heuristics |= TWO_POINT_MOVE;
    if (heur["two_opt"] == true) 
        heuristics |= TWO_OPT;
    if (heur["or_opt"] == true) 
        heuristics |= OR_OPT;
    if (heur["three_opt"] == true) 
        heuristics |= THREE_OPT;
    if (heur["cross_exchange"] == true) 
        heuristics |= CROSS_EXCHANGE;
    if (heur["three_pt_move"] == true) 
        heuristics |= THREE_POINT_MOVE;

    if (heuristics == 0) heuristics = ONE_POINT_MOVE; // default

	double **matrix = new double*[norders+2]();
	matrix[0] = new double[(norders+2)*(norders+2)]();

	VRPH_version();
	VRP v(dimension,numdays);

	// make this a multi-dimensonal array by reassigning 
	for (int i=1; i<norders+2; i++) 
		matrix[i] = matrix[i-1] + (norders+2);

    for (int i=0; i< norders; i++) {
        locs[i].x = orders[i].x;
        locs[i].y = orders[i].y;
        v.nodes[i+1].id=i;
        v.nodes[i+1].demand=orders[i+1].amount;
    } 

    my_sol_buff=new int[dimension+2]();
    final_sol=new int[dimension+2]();

	v.set_max_veh_capacity(vcap);

	//Mat<int32_t> *orig = 
        //OsrmDistanceMatrix(norders, locs,norders, locs);

	Mat<int32_t> *orig = 
        HaversineMatrix(norders, locs,norders, locs);

    for (int y=0; y< norders; y++) { 
        for (int x=0; x< norders; x++) { 
            matrix[y][x] = (double) orig->Get(y,x);
        }
    }
    
	//endtime= micro_now();
	v.set_matrix(matrix);

    v.max_theta= -VRP_INFINITY;
    v.min_theta=  VRP_INFINITY;


     // but we have the largest possible here...
    v.create_neighbor_lists(VRPH_MIN(MAX_NEIGHBORLIST_SIZE,dimension));
    v.reset();
    ClarkeWright CW(dimension);

    double best_obj=VRP_INFINITY;
    double this_obj, start_obj;
    int *best_sol=new int[norders+2]();
    int num_loops=200;
    int num_lambdas=3;
    int iters_per_loop=2;
    int nlist_size=40;
    double lambda_vals[VRPH_MAX_NUM_LAMBDAS];

    lambda_vals[0]=.6; 
    lambda_vals[1]=1.4; 
    lambda_vals[2]=1.6; 
    int i;

    double starting_temperature = 2;
    double cooling_ratio = .99;

    int verbose=false;

    for (i=0; i<num_lambdas;i++) {
        CW.Construct(&v,lambda_vals[i], false);

        if(verbose)
            printf("CW[%f] solution: %f\n",lambda_vals[i], v.get_total_route_length()); 

        this_obj = v.SA_solve(heuristics, starting_temperature, cooling_ratio, iters_per_loop,
            num_loops, nlist_size, verbose);

        if(v.get_best_total_route_length()<best_obj)
        {
            best_obj=v.get_best_total_route_length();
            v.export_canonical_solution_buff(best_sol);
        }

        if(verbose)
            printf("Improved solution: %f\n",this_obj);

    }

    printf("----------- sol ------------\n");
    printf("N routes : %d\n", v.get_total_number_of_routes());
    printf("total route length: %f\n", v.get_total_route_length());

    int off=0;
    int lastnode=0; // depot is always 0
    for (i=1; i<= v.get_total_number_of_routes(); i++) {
        printf("Route [%d] -> %f long, load %d, [%d-%d] %d stops\n", i,
            v.route[i].length, v.route[i].load, 
            v.route[i].start, v.route[i].end, 
            v.route[i].num_customers);
        int s=v.route[i].start;
        while (s>0) { 
            printf("[%d]", s);
            res[off].trip=i;
            res[off].index=s;
            res[off].time = matrix[lastnode][s];
            lastnode=s;
            s = v.next_array[s];
            off++;
        }
        printf("\n");
    }
    res[off].time = matrix[lastnode][0]; // back to depot again

    v.show_routes();

    return res;
}
