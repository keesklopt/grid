#include <app.h>
#include <stdio.h>

//#include "typedefs.h"
//#include "data_structures/phantom_node.hpp"

#include <syslog.h>

//#include "Plugins/RawRouteData.h"

//#define NETWORK "/data/projects/osrm/netherlands-latest.osrm"

// KK SEP 26 2013 : changed input parameter postcode to address
// since it can contain street, city, postcode etc.
// also when used as a return value it contains locations as well
// in that case i changed the param name to 'location' so :
// 
// address : pc,num,street,city,country
// coordinate : x,y
// location : coordinate,address

// it is tempting to make proper JsonRpc objects and serialize them
// but it is more error prone and SLOWER, so just do not bother

// this should match the enum errorcodes above
std::string errors[] = { 
    "no error",
    "bad parameter",
    "type error",

    "last error"
};

void web_error(int id, int num, const char *msg)
{
	const char *str;
	//json_object *out = json_object();
	//json_object *cod = json_object_new_int(num);
	//json_object *mes = json_string(msg);

	json_object *out;
	json_object *err;

	//json_object_add(res, "code", cod);
	//json_object_add(res, "message", mes);
	//json_object_add(out, "error", res);
	//json_object_add(out, "result", json_null);
	//json_object_add(out, "id", json_null);

	err = JsonRpc::err_ist(num, msg);
	out = JsonRpc::rcv_ist(json_object_new_int(id), NULL, err);

	str = json_object_to_json_string(out);
	printf("%s", str);

	if (out) json_object_put(out);
}

json_object *create_error(int err, string data)
{
	json_object *jo = json_object_new_object();
	json_object *content = json_object_new_object();
	json_object *error = json_object_new_int(err);
	json_object *msg = json_object_new_string(data.c_str());

	json_object_object_add(content, "code", error);
	json_object_object_add(content, "message", msg);
	json_object_object_add(jo, "error", content);

    return jo;
}

JsonService::JsonService(int port,int type) : Service(port,type) 
{
	this->id=0;
} 

string JsonService::FGetS(Client *clt)
{
	string s="";
	int total=0;
	int ret=0;
	char c[READ_LEN+1];

	Socket *sock = clt->getSocket();

    struct timeval tv;

    tv.tv_sec = REQ_TIMEOUT;  /* 3 Secs Timeout */
    tv.tv_usec = 0;  // Not init'ing this can cause strange errors
   
    // just NEVER hang the server, rather loose a request
    // this will happen if the input is not closed with '\0'
    // try to provide vis ws.wsgi to make sure it does
    setsockopt(sock->fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));
	// read all
	do { 
        memset(c, '\0', READ_LEN+1);
		ret = read(sock->fd,&c,READ_LEN);
		if (ret==0) return s; // disconnect, try to send whatever you read
    //printf("START : %d for %s %d %d\n", ret,c, c[0],c[1]);
		s += c;
	} while (ret==READ_LEN && c[ret-1] != '\0');
    //printf("REST : %d\n", ret);

	total+=ret;

    //printf("Got %s with len %d\n", s.c_str(), total);

	return s;
}

string JsonService::csv_report_getdistances(vector<Location *>olist)
{
	stringstream ss;
	vector<Location *>::iterator it;
	double lastx=-180.0;
	double lasty=-180.0;
	double meters=88;
	Location *base;
	it=olist.begin();
	base = *it;

	int nlocs=0;
	for (; it!= olist.end(); it++) nlocs++;

	Cache cache;
	coord_t *locs = (coord_t *)calloc(nlocs+1, sizeof(coord_t));

	int lineno=0;
	for (it=olist.begin(); it!= olist.end(); it++) {
		Location *p = (Location *) *it;

		locs[lineno].x = p->x;
		locs[lineno].y = p->y;
		lineno++;
	} 
	// and route back
	locs[lineno].x = base->x;
	locs[lineno].y = base->y;

	route_result_t * rr = OsrmRoute(&cache, nlocs+1, locs);

	for (int l=0; l< nlocs; l++) {
		if (l) ss<< ";"; 
		ss << rr[l].meters;
	} 
	ss << endl;
	
	if (rr->path) free(rr->path);
	if (rr) free(rr);
	free(locs);
	return ss.str();
}

std::string JsonService::handle_getmatrixrow(string context, string orderset, string data)
{
	static char erbuf[2048];
	static char buf[40960];
	int x, y;
	OrderSet *os;

	string result;

	Context *ctx = contexts[context];
	if (!ctx) { 
		ctx = new Context(context);
		contexts[context] = ctx;
	} 

	os  = ctx->getorderset(orderset);
	if (!os) {
		string result = "fail;orderset " + orderset + " not found";
		return result;
	} 

	int num = atoi(data.c_str());
	result = csv_report_getmatrixrow(os->locations,num);

	return result;
}

string JsonService::csv_report_getmatrixrow(vector<Location *>olist,int row)
{
	stringstream ss;
	vector<Location *>::iterator it;
	double lastx=-180.0;
	double lasty=-180.0;
	double meters=88;
	Location *base;
	it=olist.begin();
	base = *it;

	Cache cache;

	coord_t locs[2];

	int nlocs=0;
	for (; it!= olist.end(); it++) {
		if (nlocs == row) { 
			Location *pivot =(Location *)*it;
			locs[0].x = pivot->x;
			locs[0].y = pivot->y;
		} 
		nlocs++;
	}

	int lineno=0;
	for (it=olist.begin(); it!= olist.end(); it++) {
		Location *p = (Location *) *it;
		if (p->x == -180.0 || p->y == -180.0)
			p = base;

		locs[1].x = p->x;
		locs[1].y = p->y;

		route_result_t * rr = OsrmRoute(&cache, 2, locs);

		if (lineno) ss<< ";"; 
		//printf("%d %d\n", rr[l].meters,rr[l].seconds);
		ss << rr[0].meters;
		ss << ",";
		ss << rr[0].seconds;
		lineno++;
		if (rr && rr->path) free(rr->path);
		if (rr) free(rr);
	} 
	ss << endl;

	return ss.str();
}

string JsonService::csv_report_setorders(vector<Location *>olist)
{
	stringstream ss;
	vector<Location *>::iterator it;
	double lastx=-180.0;
	double lasty=-180.0;
	double meters=88;

	int lineno=0;
	for (it=olist.begin(); it!= olist.end(); it++) {
		Location *p = (Location *) *it;
		if (lineno) ss<< ";";
		//ss << p->x << "," << p->y << "," << meters;
		string result = p->found ? "1" : "0";
		ss << result;
		lineno++;
	} 
	ss << endl;
	
	return ss.str();
}

string JsonService::json_report(void)
{
	stringstream ss;
	vector<prox_t>::iterator it;
	double lastx=-180.0;
	double lasty=-180.0;
	double meters=88;

	ss << "{\"jsonrpc\":\"2.0\",\"id\":\""<< this->id << "\"";
	ss << "\"method\":\"geocode\",\"params\":{\"locations\":[";
	for (it=this->result.begin(); it!= this->result.end(); it++) {
		prox_t p = (prox_t) *it;
		if (it != this->result.begin()) ss << ",";
		ss << "{\"x\":" << p.x;
		ss << ",\"y\":" << p.y;
		ss << ",\"meters\":" << meters << "}";
	} 
	ss << "]";
	ss << endl;
	
	return ss.str();
}

// set a (new) set of orders, setorders implies deletion of the 
// given orderset id
vector<Location *> JsonService::handle_setorders(string context, string orderset, string data)
{
	static char erbuf[2048];
	static char buf[40960];
	int x, y;
	OrderSet *os;
	Location *base=NULL;

	Context *ctx = contexts[context];
	if (!ctx) { 
		ctx = new Context(context);
		contexts[context] = ctx;
	} 

	os = ctx->addorderset(orderset);

	stringstream datass(data);
	string item;

	while (getline(datass,item,';')) {
		stringstream liness(item);
		//printf("row is %s\n", item.c_str());
		Location *l = new Location();
		string field;
		if (getline(liness,field,',')) {
			l->id = field;
		} 
		if (getline(liness,field,',')) {
			l->postcode = field;
		} 
		if (getline(liness,field,',')) {
			l->nr = field;
		} 
		os->locations.push_back(l);
		if (!base) base = l;
		char * country = (char *)"NL";
		char * city = NULL;
		char * street = NULL;
		char * zip = (char *)l->postcode.c_str();
		char * house = (char *)l->nr.c_str();
		prox_t res= k.KloptGeocode (country,city,street,house,zip);
		l->x = res.x;
		l->y = res.y;
		l->found = true;
		if (l->x == -180.0 || l->y == -180.0) {
			l->found=false;
			l->x = base->x;
			l->y = base->y;
		} 
	} 

	return os->locations;
}

json_object *JsonRpc::rcv_ist(json_object *id, json_object *result, json_object *error)
{
    json_object *res = json_object_new_object();

    if (result == NULL && error == NULL) {
        return NULL;
    }

    json_object_object_add(res, "result", result);
    json_object_object_add(res, "error", error);
    json_object_object_add(res, "id", id);

    return res;
}

json_object *JsonRpc::err_ist(int errornumber, const char *errmsg)
{
    json_object *err = json_object_new_object();

    json_object_object_add(err, "code", json_object_new_int(errornumber));
    json_object_object_add(err, "message", json_object_new_string(errmsg) );

    return err;
}


string JsonRpc::serialize(json_object *result)
{
    json_object *jrpc = json_object_new_object();
    json_object *version = json_object_new_string("2.0");
    json_object *method = json_object_new_string(this->method.c_str());
    
    json_object_object_add(jrpc, "jsonrpc", version);
    json_object_object_add(jrpc, "method", method);
    json_object_object_add(jrpc, "result", result);

	s = json_object_to_json_string(jrpc);;

    json_object_put(jrpc);

    cout << s << endl;

	return s;
}

string JsonRpc::serialize(string result)
{
	char numbuf[20]={0};
	sprintf(numbuf, "%d", this->id);
	string s = ("{\"jsonrpc\":\"2.0\",\"method\":\"");
	s += this->method;
	s += "\",\"id\":";
	s += numbuf;
	s += ",";
	s += result;
	s += "}";

    //cout << s << endl;

	return s;
}

string JsonRpc::get_method()
{
	return this->method;
}

json_object *JsonRpc::get_param(string name)
{
	if (!this->pars) return NULL;

	json_object *par;
	json_bool tpar = json_object_object_get_ex(this->pars, name.c_str(), &par);
	if (!tpar) { this->error("no such parameter", name); return NULL; }

	return par;
}

JsonRpc::~JsonRpc()
{
	if (this->content) 
		json_object_put(this->content);

	this->content=NULL;
}

JsonRpc::JsonRpc(int id,string command)
{
	this->s="created empty";
	this->content=NULL;
	this->method = command;
	this->id = id;
    this->fwd= false;
}

JsonRpc::JsonRpc(string s)
{
    this->s=s;
    this->fwd= false;
    //printf("message now : %s\n", s.c_str());
	this->content = json_tokener_parse((char *)s.c_str());
	if (!content) { this->error("json unparsable", s); return; }

	json_object *method;
	json_object_object_get_ex(content, "method", &method);
	if (!this->content) { this->error("no \"method\" found", s); return; }

	this->method = json_object_get_string(method);

	json_object *id;
	json_bool tid = json_object_object_get_ex(content, "id", &id);
	if (tid) this->id = json_object_get_int(id);

	json_object_object_get_ex(content, "params", &this->pars);
}

void JsonRpc::error(string m,string s)
{
	static char erbuf[2048]={0};
	
	sprintf(erbuf, "%s : \"%s\"", m.c_str(), s.c_str());
	web_error(this->id, -10,erbuf);
}

// set a (new) set of orders, setorders implies deletion of the 
// given orderset id
string JsonService::handle_getdistances(string context, string orderset, string data)
{
	static char erbuf[2048];
	static char buf[40960];
	int x, y;
	OrderSet *os;

	string result;

	Context *ctx = contexts[context];
	if (!ctx) { 
		ctx = new Context(context);
		contexts[context] = ctx;
	} 

	os  = ctx->getorderset(orderset);
	if (!os) {
		result += "fail;orderset " + orderset + " not found";
		return result;
	} 

	stringstream datass(data);
	string item;

	result = csv_report_getdistances(os->locations);

	return result;
}

static bool test_pong(json_object *test)
{
    //cout << "Testing pong : " << s << endl;
	//json_object *test = json_tokener_parse((char *)s.c_str());
    if(!test) return false;

    //cout << "Twesting pong : " << s << endl;
    
    json_bool ttest = json_object_object_get_ex(test, "result", &test);
    //cout << "Testing pong ; " << test << endl;
    if (!ttest) return false;

    //cout << "Definitely returning true " << endl;
    ttest = json_object_object_get_ex(test, "method", &test);
    if (!ttest) return false;

    return true;
}

json_object *JsonService::handle_ping(JsonRpc *message,Client *clt)
{
	int len;
    json_object *subsrv;

    json_object *result = json_object_new_object();
    json_object *sock   = json_object_new_int(SOCK_SRV);
    json_object *arr    = json_object_new_array();

    json_object_array_add(arr,sock);

    subsrv = Forward(message, "127.0.0.1", SOCK_GEO);
    if (test_pong(subsrv) == true)  {
        json_object *sock   = json_object_new_int(SOCK_GEO);
        json_object_array_add(arr,sock);
    } 
    subsrv = Forward(message, "127.0.0.1", SOCK_RTE);
    if (test_pong(subsrv) == true)  {
        json_object *sock   = json_object_new_int(SOCK_RTE);
        json_object_array_add(arr,sock);
    } 
    json_object_object_add(result, "pong", arr);

	k.count("ping",1,1);
    // note for the originator addres (ip) we need to pass it 
    // along in ws.wsgi or it will be localhost, later
	k.log("ping",1,clt->sock->getPeerIp());

	return result;
} 

#ifdef DEBUG
string JsonService::handle_exit(JsonRpc *message,Client *clt)
{
	stringstream ss;
    json_object *subsrv;
    
	int len;
	char workbuf[1024];

    //printf("in handle_ping to %s\n", clt->msg.c_str());

    subsrv = Forward(message, "127.0.0.1", SOCK_GEO);
    json_object_put(subsrv);
    subsrv = Forward(message, "127.0.0.1", SOCK_RTE);
    json_object_put(subsrv);

	return "";
} 
#endif

string JsonService::handle_status(JsonRpc *message,Client *clt)
{
    stringstream ss;

	ss << "\"result\":{\"horizons\":{";
        ss << "\"dawn\":\"" << to_simple_string(k.dawn) << "\"";
        ss << ",\"fault\":\"" << to_simple_string(k.fault) << "\"";
        ss << ",\"up\":\"" << to_simple_string(k.up) << "\"";
    ss << "}, \"counts\": {";
        ss << k.counts();
    ss << "}}";

    // yes, we count/log this after the stats returned
	k.count("status",1,1);
	k.log("status",1,clt->sock->getPeerIp());
    return ss.str();
} 

string JsonService::StripJsonRpc(string s)
{
    // forwarding will also get the jsonrpc header from the other
    // server, strip it here or we will get it twice 
    json_object *jo;
    json_object *result;

	jo = json_tokener_parse((char *)s.c_str());
	if (!jo) { return ""; }

    if (json_object_object_get_ex(jo, "result", &result) == false) return "";

	return json_object_to_json_string(result);
}

json_object *JsonService::Forward(JsonRpc *message, string host, int port)
{
    struct sockaddr_in sa;
	json_object *jo;

    message->fwd=true;

    // TODO : won't be always localhost !!
    sa.sin_addr.s_addr= inet_addr("127.0.0.1");
    sa.sin_family= AF_INET;
    sa.sin_port = htons(port);

	Socket *proxy = new Socket(SOCK_STREAM);
    if ( proxy->Connect((struct sockaddr *)&sa)) { 
        stringstream msg; 
        msg << "Could not connect to port:";
        msg << port;
        delete proxy;
        return create_error(1, msg.str().c_str());
    } 

	//message->s += "\0";
    if (proxy->Send(message->s) == false) { 
        jo = create_error(1, "could not write");
    } 

    jo = json_tokener_parse(proxy->Recv().c_str());
    delete proxy;

	json_object *result;
	json_bool tpar = json_object_object_get_ex(jo, "result", &result);
    return result;
}

/* service using the osrm network */
json_object *JsonService::handle_matrix(JsonRpc *message,Client *clt)
{
    return Forward(message, "127.0.0.1", SOCK_RTE);
}

json_object *JsonService::handle_randomnodes(JsonRpc *message,Client *clt)
{
    return Forward(message, "127.0.0.1", SOCK_RTE);
}

json_object *JsonService::handle_nearest(JsonRpc *message,Client *clt)
{
    return Forward(message, "127.0.0.1", SOCK_RTE);
}

json_object *JsonService::handle_route(JsonRpc *message,Client *clt)
{
    return Forward(message, "127.0.0.1", SOCK_RTE);
}

json_object *JsonService::handle_tsp(JsonRpc *message,Client *clt)
{
    return Forward(message, "127.0.0.1", SOCK_RTE);
}

json_object *JsonService::handle_vrp(JsonRpc *message,Client *clt)
{
    return Forward(message, "127.0.0.1", SOCK_RTE);
}

/* service using the mongo database */
json_object *JsonService::handle_geocode(JsonRpc *message,Client *clt)
{
    return Forward(message, "127.0.0.1", SOCK_GEO);
}

json_object *JsonService::handle_revgeocode(JsonRpc *message,Client *clt)
{
    return Forward(message, "127.0.0.1", SOCK_GEO);
}

intptr_t JsonService::Handle(Client *clt)
{

	Socket *sock = clt->getSocket();
	string command;

	string s = FGetS(clt);
    clt->msg = s;
    //syslog(LOG_INFO, "set msg to %s\n", clt->msg.c_str());
	if (s == "") return 1;

	//cout << "--------- in --------" << endl;
	//cout << s << endl;
	//cout << s.length() << endl;
    //cout << s[0] << "-" << s[s.length()] << endl;

	JsonRpc message(s);

	command = message.get_method();
	if (command == "") return 1;

	//cout << command << endl;

	JsonRpc reply(message.id,command);

	string ss;
    json_object *jo=NULL;

	if (command == "ping") {
		jo = handle_ping(&message,clt);
	} else  
#ifdef DEBUG
	if (command == "exit") {
		ss = handle_exit(&message,clt);
	    return CS_STOP;
	} else  
	if (command == "crash") {
        *(int *)0 = 1;
	} else  
#endif
	if (command == "status") {
		ss = handle_status(&message,clt);
	} else  
	if (command == "nearest") {
		jo = handle_nearest(&message,clt);
	} else  
	if (command == "addresses") {
		jo = handle_revgeocode(&message,clt);
	} else  
	if (command == "locations") {
		jo = handle_geocode(&message,clt);
	} else  
	if (command == "routes") {
		jo = handle_route(&message,clt);
	} else  
	if (command == "matrix") {
		jo = handle_matrix(&message,clt);
	} else  
	if (command == "tsp") {
		jo = handle_tsp(&message,clt);
	} else  
	if (command == "vrp") {
		jo = handle_vrp(&message,clt);
	} else  
	if (command == "randomnodes") {
		jo = handle_randomnodes(&message,clt);
	} 

    //if (message.fwd == false) 
	    command = reply.serialize(jo);
    //else
        //command = ss;

	char *buf = (char *)command.c_str();
    int len = strlen(buf) + 1;

	write(sock->fd, buf, len);

	return CS_CONT;
	//return CS_STOP; // for valgrind tests 
}
