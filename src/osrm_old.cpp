// osrm includes
#include <app.h>
#include <float.h>
#include <math.h>

// osrm includes
#include "Server/DataStructures/QueryObjectsStorage.h"
//#include "Server/ServerConfiguration.h"
#include "Server/ServerFactory.h"

//#include <Plugins/HelloWorldPlugin.h>
//#include <Plugins/LocatePlugin.h>
//#include <Plugins/NearestPlugin.h>
//#include <Plugins/TimestampPlugin.h>
#include <Plugins/ViaRoutePlugin.h>

QueryObjectsStorage * objects;
//SearchEngine<QueryEdge::EdgeData, StaticGraph<QueryEdge::EdgeData> > * searchEngine;
SearchEngine* searchEngine;

/*
typedef struct addr_tag
{
	int country;
	int city;
	int street;
	int num;
	int zip;
	double x;
	double y;
} addr_t;
*/

matrix_result_t Cache::get(int fx, int fy,int tx, int ty)
{
	matrix_result_t res={0};
	vector<matrix_result_t>::iterator it;
	if (this->curx != fx || this->cury != fy) {
		this->curres.clear();
		this->curres = get(fx,fy);
	} 

	for (it = this->curres.begin(); it != this->curres.end(); it++) {
		res = (matrix_result_t) *it;
		if (res.tx == tx && res.ty == ty) return res;
	} 

	res.meters = -1; // not-found condition

	this->curx=fx;
	this->cury=fy;
		
	return res;
}

vector<matrix_result_t>Cache::get(int fx, int fy)
{
	return k.get_matrixrow(fx,fy);
}

bool Cache::set(int fx, int fy,int tx, int ty,route_result_t r)
{
	k.set_matrixrow(fx,fy,tx,ty,r);
	return true;
}

Cache::Cache(string db, string collection)
{
	this->db=db;
	this->collection=collection;
}

Cache::Cache()
{
	this->db="network";
	this->collection="matrix";
}

Cache::~Cache()
{
	// close mongo
}
prox_t OsrmLocation(coord_t find, int *dist)
{
	prox_t ret={0};
    PhantomNode result;
    //FixedPointCoordinate cres;
	int x = 1000000.* find.x;
	int y = 1000000.* find.y;

	unsigned int maxname = objects->names.size();

    FixedPointCoordinate myCoordinate(x, y);

    bool found = objects->nodeHelpDesk->FindPhantomNodeForCoordinate(myCoordinate, result, 18UL);
    //objects->nodeHelpDesk->FindNearestNodeCoordForLatLon(myCoordinate, cres);
	ret.x = result.location.lon * 0.000001;
	ret.y = result.location.lat * 0.000001;
	if (found == false ) { //|| ret.x<= -9000000 || y <= -18000000) {
		ret.name = (char *)"Too far from network data";
		*dist = ret.dist = -1;
	} else {
		if (result.nodeBasedEdgeNameID < maxname) 
			ret.name = (char *)objects->names[result.nodeBasedEdgeNameID].c_str();
		*dist = ret.dist = CalculateDistance( find.x, find.y, ret.x, ret.y );
	} 
	
	return ret;
}

prox_t OsrmNearest(coord_t find, int *dist)
{
	prox_t ret={0};
    PhantomNode result;
    //FixedPointCoordinate cres;
	int x = 1000000.* find.x;
	int y = 1000000.* find.y;

	unsigned int maxname = objects->names.size();

    FixedPointCoordinate myCoordinate(y, x);

    bool found = objects->nodeHelpDesk->FindPhantomNodeForCoordinate(myCoordinate, result, 18UL);
    //objects->nodeHelpDesk->FindNearestNodeCoordForLatLon(myCoordinate, cres);
	ret.x = result.location.lon * 0.000001;
	ret.y = result.location.lat * 0.000001;
	if (found == false ) { //|| ret.x<= -9000000 || y <= -18000000) {
		ret.name = (char *)"Too far from network data";
		ret.x = -180.0;
		ret.y = -180.0;
		*dist = ret.dist = -1;
	} else {
		if (result.nodeBasedEdgeNameID < maxname) 
			ret.name = (char *)objects->names[result.nodeBasedEdgeNameID].c_str();
		*dist = ret.dist = CalculateDistance( find.x, find.y, ret.x, ret.y );
	} 
	
	return ret;
}

prox_t OsrmProximity(coord_t find, int *dist)
{
	prox_t ret={0};
    PhantomNode result;
    //FixedPointCoordinate cres;
	int x = 1000000.* find.x;
	int y = 1000000.* find.y;

	unsigned int maxname = objects->names.size();

    FixedPointCoordinate myCoordinate(x, y);

    bool found = objects->nodeHelpDesk->FindPhantomNodeForCoordinate(myCoordinate, result, 18UL);
    //objects->nodeHelpDesk->FindNearestNodeCoordForLatLon(myCoordinate, cres);
	ret.x = result.location.lon * 100.0;
	ret.y = result.location.lat * 100.0;
	if (found == false ) { //|| ret.x<= -9000000 || y <= -18000000) {
		ret.name = (char *)"Too far from network data";
		*dist = ret.dist = -1;
	} else {
		if (result.nodeBasedEdgeNameID < maxname) 
			ret.name = (char *)objects->names[result.nodeBasedEdgeNameID].c_str();
		*dist = ret.dist = CalculateDistance( find.x, find.y, ret.x/10000000.0, ret.y /10000000.0);
	} 
	
	return ret;
}

route_result_t *OsrmRoute(Cache *cache, int nlocs, coord_t *locs)
{
	//double result;
	int l;
	int d;

	route_result_t *rr = (route_result_t *)calloc(sizeof(route_result_t),nlocs-1);

	if (!locs || nlocs< 2) return rr;

	int fx = 1000000.* locs[0].x;
	int fy = 1000000.* locs[0].y;

	for(l=1; l< nlocs; l++) {

    	FixedPointCoordinate fromCoord(fy, fx);

		RawRouteData rawRoute;
		rawRoute.checkSum = objects->nodeHelpDesk->GetCheckSum();

		PhantomNode from;
		PhantomNode to;
		PhantomNodes pnodes;
		int tx = 1000000.* locs[l].x;
		int ty = 1000000.* locs[l].y;
		int dist;
    	_DescriptorConfig config;
		std::string content;
		FixedPointCoordinate current;
   		FixedPointCoordinate toCoord(ty, tx);
		int numberOfEnteredRestrictedAreas=0;
		DescriptionFactory descriptionFactory;

		if (cache) { 
			matrix_result_t m = cache->get(fx,fy,tx,ty);
			if (m.meters >= 0) {
				rr[l-1].meters = m.meters;
				rr[l-1].seconds = m.seconds;
				rr[l-1].path=NULL;
				goto next;
			} 
		} 
	
        rawRoute.rawViaNodeCoordinates.push_back(toCoord);
        rawRoute.rawViaNodeCoordinates.push_back(fromCoord);

        searchEngine->FindPhantomNodeForCoordinate(fromCoord, from,18UL);
        searchEngine->FindPhantomNodeForCoordinate(toCoord, to,18UL);

		pnodes.startPhantom = from;
		pnodes.targetPhantom = to;

		if (from.isValid(objects->nodeHelpDesk->getNumberOfNodes()) == false || 
		    to.isValid(objects->nodeHelpDesk->getNumberOfNodes()) == false) { 
			dist = -1;
			rr[l-1].meters=dist;
			rr[l-1].seconds=dist;
			rr[l-1].path=NULL;
			goto next;
		} else 
			if (from == to) {
				dist=0;
				rr[l-1].meters = dist;
				rr[l-1].seconds=dist;
				rr[l-1].path=NULL;
				goto next;
			} else {

				rawRoute.segmentEndCoordinates.push_back(pnodes);
				searchEngine->shortestPath(rawRoute.segmentEndCoordinates, rawRoute);
				dist=rawRoute.lengthOfShortestPath;
			} 
			numberOfEnteredRestrictedAreas=0;

            descriptionFactory.SetStartSegment(pnodes.startPhantom);

            BOOST_FOREACH(_PathData & pathData, rawRoute.computedShortestPath) {
                searchEngine->GetCoordinatesForNodeID(pathData.node, current);
				std::string name = searchEngine->GetEscapedNameForNameID((const unsigned int)pathData.nameID);
                descriptionFactory.AppendSegment(current, pathData );

			}
            descriptionFactory.SetEndSegment(pnodes.targetPhantom);

			BaseDescriptor* desc;


		config.instructions=false;
		config.geometry=true;

		desc = new JSONDescriptor();

		desc->SetConfig(config);
		descriptionFactory.Run(*searchEngine,config.z);
		descriptionFactory.AppendEncodedPolylineString(content, true);
		//descriptionFactory.AppendEncodedPolylineString(content, config.encodeGeometry);
		//printf("--------rr %d / %f and %f\n", dist, descriptionFactory.entireTime,descriptionFactory.entireLength);

		// for some reason unroutable paths DO get a length
		// just fix that here : both -1
		if (rawRoute.lengthOfShortestPath == INT_MAX) {
			rr[l-1].seconds = -1;
			rr[l-1].meters = -1;
			rr[l-1].path = NULL;
		} else {	
			rr[l-1].seconds = rawRoute.lengthOfShortestPath/10+1;
			rr[l-1].meters = round(descriptionFactory.entireLength);
			rr[l-1].path = strdup(content.c_str());
		} 
		if (cache) 
			cache->set(fx,fy,tx,ty,rr[l-1]);
next:
		fx = tx;
		fy = ty;
	}

	return rr;
}

coord_t *OsrmRandomNodes(int n)
{
	PhantomNode result;	
	coord_t *list = (coord_t *)calloc(sizeof(coord_t),n);

	for (int i=0; i< n; i++) {
    	objects->nodeHelpDesk->FindPhantomNodeRandom(result,18UL);
		list[i].x = result.location.lon*0.000001;
		list[i].y = result.location.lat*0.000001;
	} 

	return list;
}

// the old version, calculates ever cells seperately
int *OsrmMatrixPerCell(int ndeps, coord_t *deps, int narrs, coord_t *arrs)
{
	//double result;
	int x,y;
	int d;

	printf("Per Cell\n");

	int *rrd=(int*)calloc(sizeof(int),ndeps*narrs);

	for(y=0; y< ndeps; y++) {

		int fx = 1000000.* deps[y].x;
		int fy = 1000000.* deps[y].y;
	
    	FixedPointCoordinate fromCoord(fx, fy);
		
		for(x=0; x< narrs; x++) {
			struct RawRouteData rawRoute;
			rawRoute.checkSum = objects->nodeHelpDesk->GetCheckSum();

			PhantomNode from;
			PhantomNode to;
			PhantomNodes pnodes;
			int tx = 1000000.* arrs[x].x;
			int ty = 1000000.* arrs[x].y;
		
    		FixedPointCoordinate toCoord(tx, ty);
            rawRoute.rawViaNodeCoordinates.push_back(toCoord);
            rawRoute.rawViaNodeCoordinates.push_back(fromCoord);

            searchEngine->FindPhantomNodeForCoordinate(fromCoord, from,18UL);
            searchEngine->FindPhantomNodeForCoordinate(toCoord, to,18UL);

			pnodes.startPhantom = from;
			pnodes.targetPhantom = to;

			//cout << from << " and " << to << endl;

			int dist;
			if (from.isValid(objects->nodeHelpDesk->getNumberOfNodes()) == false || 
			    to.isValid(objects->nodeHelpDesk->getNumberOfNodes()) == false) { 
				dist = -1;
				//mat_set(mp,y,x,dist);
				rrd[y*ndeps+x]= dist;
				continue;
			} else 
			if (from == to) {
				dist=0;
				//mat_set(mp,y,x,dist);
				rrd[y*ndeps+x]= dist;
				continue;
			} else {

				rawRoute.segmentEndCoordinates.push_back(pnodes);

				//printf("from : %u and %u \n", from.nodeBasedEdgeNameID, from.edgeBasedNode);
				//printf("to : %u and %u \n", to.nodeBasedEdgeNameID, to.edgeBasedNode);
				//searchEngine->alternativePaths(rawRoute.segmentEndCoordinates[0], rawRoute);
				searchEngine->shortestPath(rawRoute.segmentEndCoordinates, rawRoute);

				dist=rawRoute.lengthOfShortestPath;
				//printf("Dist now %d\n", dist);
			} 
			FixedPointCoordinate current;
			int numberOfEnteredRestrictedAreas=0;
			DescriptionFactory descriptionFactory;

            descriptionFactory.SetStartSegment(pnodes.startPhantom);
//			printf("7 : %f\n", descriptionFactory.entireLength);

            BOOST_FOREACH(_PathData & pathData, rawRoute.computedShortestPath) {
                searchEngine->GetCoordinatesForNodeID(pathData.node, current);
				std::string name = searchEngine->GetEscapedNameForNameID((const unsigned int)pathData.nameID);
                descriptionFactory.AppendSegment(current, pathData );

			}
            descriptionFactory.SetEndSegment(pnodes.targetPhantom);

			BaseDescriptor * desc;

    _DescriptorConfig config;

		config.instructions=false;
		config.geometry=true;

			desc = new JSONDescriptor();

			desc->SetConfig(config);
			descriptionFactory.Run(*searchEngine,config.z);

            //BOOST_FOREACH(const _PathData & pathData, rawRoute.computedShortestPath) {
                //searchEngine->GetCoordinatesForNodeID(pathData.node, current);
                //descriptionFactory.AppendSegment(current, pathData );
            //}

			std::string content;
			descriptionFactory.AppendEncodedPolylineString(content, config.encodeGeometry);
			//printf("encoded : %s\n", content.c_str());

			//printf("11 : %f\n", descriptionFactory.entireLength);

			//descriptionFactory.BuildRouteSummary(descriptionFactory.entireLength, rawRoute.lengthOfShortestPath - ( numberOfEnteredRestrictedAreas*TurnInstructions.AccessRestrictionPenalty));
			
			//cout<< descriptionFactory.summary.lengthString << "\n";
			//mat_set(mp,y,x,descriptionFactory.entireLength);
			//rrd[y*ndeps+x].distOfShortestPath = descriptionFactory.entireLength;
			//rrd[y*ndeps+x].distOfShortestPath = round(descriptionFactory.entireLength);
			//rrd[y*ndeps+x].lengthOfShortestPath = descriptionFactory.entireTime/10+1;
			rrd[y*ndeps+x]= rawRoute.lengthOfShortestPath;
		}
	}

	return rrd;
}

int *OsrmMatrix(int ndeps, coord_t *deps, int narrs, coord_t *arrs)
{
	//double result;
	int x,y;
	int d;

	//MatrixData *res = (MatrixData *)calloc(sizeof(MatrixData), ndeps*narrs);

	std::vector<PhantomNode>froms;
	std::vector<PhantomNode>tos;

	int *rrd=(int*)calloc(sizeof(int),ndeps*narrs);

	for(y=0; y< ndeps; y++) {

		int fx = 1000000.* deps[y].x;
		int fy = 1000000.* deps[y].y;
		PhantomNode from;
	
    	FixedPointCoordinate fromCoord(fx, fy);
        searchEngine->FindPhantomNodeForCoordinate(fromCoord, from,18UL);

		//cout << fromCoord << endl;

		froms.push_back(from);
	} 

	for(x=0; x< narrs; x++) {
		PhantomNode to;

		int tx = 1000000.* arrs[x].x;
		int ty = 1000000.* arrs[x].y;
		
    	FixedPointCoordinate toCoord(tx, ty);
        searchEngine->FindPhantomNodeForCoordinate(toCoord, to,18UL);

		//cout << toCoord << endl;

		tos.push_back(to);
	} 

	searchEngine->matrixRouting(froms, tos, rrd);

	//free(res);

	return rrd;
}

Mat<int32_t> *OsrmDistanceMatrix(int ndeps, coord_t *deps, int narrs, coord_t *arrs)
{
	//double result;
	int x,y;
	int d;
	Mat<int32_t> *mp = new Mat<int32_t>(ndeps,narrs);

#ifdef OLD
	mp = OsrmMatrixPerCell(ndeps,deps,narrs,arrs);
#else
	int *res = OsrmMatrix(ndeps,deps,narrs,arrs);

	for (y=0; y< ndeps; y++) {
		for(x=0; x< narrs; x++) {
			int dist = res[y*narrs+x];
			mp->Set(y,x,dist);
			//delete res[y*narrs+x];
		} 
	} 

	free(res);
#endif
	return mp;
}


prox_t OsrmGeocode(char *country, char *city, char *street, char *num, char *zipcode)
{
	prox_t p={0};

	p.x = 1.1;
	p.y = 2.2;

	return p;
}

addr_t OsrmEdocoeg(double x,double lng)
{
	addr_t p={0};

	return p;
}

void OsrmExit(void)
{
	delete objects;
	delete searchEngine;
}

void OsrmInit(char *base)
{
	int len = strlen(base);
	char *hsgrbuf = (char *)malloc(len + 20);
	char *nodebuf = (char *)malloc(len + 20);
	char *edgebuf = (char *)malloc(len + 20);
	char *ramibuf = (char *)malloc(len + 20);
	char *filebuf = (char *)malloc(len + 20);
	char *namebuf = (char *)malloc(len + 20);
	char *timebuf = (char *)"";
	char *addrbuf = (char *)malloc(len + 20);
	sprintf(hsgrbuf, "%s.hsgr", base);
	sprintf(nodebuf, "%s.nodes", base);
	sprintf(edgebuf, "%s.edges", base);
	sprintf(ramibuf, "%s.ramIndex", base);
	sprintf(filebuf, "%s.fileIndex", base);
	sprintf(namebuf, "%s.names", base);
	sprintf(addrbuf, "%s.address", base);
	//sprintf(timebuf, "%s.timestamp", base);

	objects = new QueryObjectsStorage(
		hsgrbuf, ramibuf, filebuf, nodebuf, edgebuf, 
		namebuf, timebuf);

	searchEngine = new SearchEngine(objects->graph, objects->nodeHelpDesk, objects->names);

#ifdef LK
	if (init_address(addrbuf) < 0) {
		printf("could not load %s\n", addrbuf);
	}
#endif

	free(hsgrbuf);
	free(nodebuf);
	free(edgebuf);
	free(ramibuf);
	free(filebuf);
	free(namebuf);
	free(addrbuf);
	//free(timebuf);

}
