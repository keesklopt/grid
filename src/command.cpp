#include <app.h>
#include <stdio.h>

#define NETWORK "/data/projects/osrm/netherlands-latest.osrm"

CommandService::CommandService(int port,int type) : Service(port,type) 
{
} 

string CommandService::FGetS(Client *clt)
{
	string s="";
	char c;
	int total=0;
	int ret=0;
	Socket *sock = clt->getSocket();

	// read all
	do { 
		ret = read(sock->fd,&c,1);
		if (ret==0) return s; // disconnect, try to send whatever you read
		s += c;
	} while (ret==1 && c != '\n');

	total+=ret;

	return s;
}

string CommandService::csv_report_getdistances(vector<Location *>olist)
{
	stringstream ss;
	vector<Location *>::iterator it;
	double lastlat=-180.0;
	double lastlon=-180.0;
	double meters=88;
	Location *base;
	it=olist.begin();
	base = *it;

	int nlocs=0;
	for (; it!= olist.end(); it++) nlocs++;

	Cache cache;
	coord_t *locs = (coord_t *)calloc(nlocs+1, sizeof(coord_t));

	int lineno=0;
	for (it=olist.begin(); it!= olist.end(); it++) {
		Location *p = (Location *) *it;

		locs[lineno].x = p->x;
		locs[lineno].y = p->y;
		lineno++;
	} 
	// and route back
	locs[lineno].x = base->x;
	locs[lineno].y = base->y;

	route_result_t * rr = OsrmRoute(&cache, nlocs+1, locs);

	for (int l=0; l< nlocs; l++) {
		if (l) ss<< ";"; 
		//printf("%d %d\n", rr[l].meters,rr[l].seconds);
		ss << rr[l].meters;
	} 
	ss << endl;
	

	free(locs);
	return ss.str();
}

string CommandService::csv_report_getmatrixrow(vector<Location *>olist,int row)
{
	stringstream ss;
	vector<Location *>::iterator it;
	double lastlat=-180.0;
	double lastlon=-180.0;
	double meters=88;
	Location *base;
	it=olist.begin();
	base = *it;

	Cache cache;

	coord_t locs[2];

	int nlocs=0;
	for (; it!= olist.end(); it++) {
		if (nlocs == row) { 
			Location *pivot =(Location *)*it;
			locs[0].x = pivot->x;
			locs[0].y = pivot->y;
		} 
		nlocs++;
	}

	int lineno=0;
	for (it=olist.begin(); it!= olist.end(); it++) {
		Location *p = (Location *) *it;
		if (p->x == -180.0 || p->y == -180.0)
			p = base;

		locs[1].x = p->x;
		locs[1].y = p->y;

		route_result_t * rr = OsrmRoute(&cache, 2, locs);

		if (lineno) ss<< ";"; 
		//printf("%d %d\n", rr[l].meters,rr[l].seconds);
		ss << rr[0].meters;
		ss << ",";
		ss << rr[0].seconds;
		lineno++;
	} 
	ss << endl;

	return ss.str();
}

string CommandService::csv_report_setorders(vector<Location *>olist)
{
	stringstream ss;
	vector<Location *>::iterator it;
	double lastlat=-180.0;
	double lastlon=-180.0;
	double meters=88;

	int lineno=0;
	for (it=olist.begin(); it!= olist.end(); it++) {
		Location *p = (Location *) *it;
		if (lineno) ss<< ";";
		//ss << p->x << "," << p->y << "," << meters;
		string result = p->found ? "1" : "0";
		ss << result;
		lineno++;
	} 
	ss << endl;
	
	return ss.str();
}

string CommandService::json_report(void)
{
	stringstream ss;
	vector<prox_t>::iterator it;
	double lastlat=-180.0;
	double lastlon=-180.0;
	double meters=88;

	ss << "{\"jsonrpc\":\"2.0\",\"id\":\""<< this->id << "\"";
	ss << "\"method\":\"geocode\",\"params\":{\"postcodes\":[";
	for (it=this->result.begin(); it!= this->result.end(); it++) {
		prox_t p = (prox_t) *it;
		ss << "{\"x\":" << p.x;
		ss << ",\"x\":" << p.y;
		ss << ",\"meters\":" << meters << "}";
	} 
	ss << endl;
	
	return ss.str();
}

// set a (new) set of orders, setorders implies deletion of the 
// given orderset id
vector<Location *> CommandService::handle_setorders(string context, string orderset, string data)
{
	static char erbuf[2048];
	static char buf[40960];
	int x, y;
	OrderSet *os;
	Location *base=NULL;

	Context *ctx = contexts[context];
	if (!ctx) { 
		ctx = new Context(context);
		contexts[context] = ctx;
	} 

	os = ctx->addorderset(orderset);

	stringstream datass(data);
	string item;

	while (getline(datass,item,';')) {
		stringstream liness(item);
		printf("row is %s\n", item.c_str());
		Location *l = new Location();
		string field;
		if (getline(liness,field,',')) {
			l->id = field;
		} 
		if (getline(liness,field,',')) {
			l->postcode = field;
		} 
		if (getline(liness,field,',')) {
			l->nr = field;
		} 
		os->locations.push_back(l);
		if (!base) base = l;
		char * country = (char *)"NL";
		char * city = NULL;
		char * street = NULL;
		char * zip = (char *)l->postcode.c_str();
		char * house = (char *)l->nr.c_str();
		prox_t res= k.KloptGeocode (country,city,street,house,zip);
		l->x = res.x;
		l->y = res.y;
		l->found = true;
		if (l->x == -180.0 || l->y == -180.0) {
			l->found=false;
			l->x = base->x;
			l->y = base->y;
		} 
	} 

	return os->locations;
}

// set a (new) set of orders, setorders implies deletion of the 
// given orderset id
string CommandService::handle_getdistances(string context, string orderset, string data)
{
	static char erbuf[2048];
	static char buf[40960];
	int x, y;
	OrderSet *os;

	string result;
	//result += context;
	//result += ":";
	//result += orderset;

	Context *ctx = contexts[context];
	if (!ctx) { 
		ctx = new Context(context);
		contexts[context] = ctx;
	} 

	os  = ctx->getorderset(orderset);
	if (!os) {
		result += "fail;orderset " + orderset + " not found";
		return result;
	} 

	stringstream datass(data);
	string item;

	result = csv_report_getdistances(os->locations);

	return result;
}

string CommandService::handle_getmatrixrow(string context, string orderset, string data)
{
	static char erbuf[2048];
	static char buf[40960];
	int x, y;
	OrderSet *os;

	string result;

	Context *ctx = contexts[context];
	if (!ctx) { 
		ctx = new Context(context);
		contexts[context] = ctx;
	} 

	os  = ctx->getorderset(orderset);
	if (!os) {
		result += "fail;orderset " + orderset + " not found";
		return result;
	} 

	int num = atoi(data.c_str());
	result = csv_report_getmatrixrow(os->locations,num);

	return result;
}

bool CommandService::handle_geocode(json_object *content)
{
	static char erbuf[2048];
	static char buf[40960];
	json_object *options=NULL;
	json_object *params=NULL;
	json_object *pc=NULL;
	json_object *postcode=NULL;
	json_object *number=NULL;
	json_object *postcodes=NULL;
	json_object *timings=NULL;
	int dotimes=0; // default
	int x, y;
	this->id=666;
	micro_t starttime;
	micro_t enddtime;
	//prox_t (*fnc) (char *zip, char *street, char *city, char *house, char *country)=NULL;

	json_bool testparams = json_object_object_get_ex(content, "params",&params);
	if (!testparams) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		//return result;
	}
	//json_object_object_get_ex(params, "options",&options);
	json_bool testpostcodes = json_object_object_get_ex(params, "postcodes",&postcodes);
	if (!testpostcodes) {
		sprintf(erbuf, "geocode function needs postcodes");
		web_error(id, -14,erbuf);
		return false;
		//return result;
	}

/*
	result << "{\"jsonrpc\": \"2.0\",";
	if (id) {
		result << "\"id\": " << id ;
	}
	result << "\"result\": ";
*/

	int rows = json_object_array_length(postcodes);
	int len=0;
	//printf("got %d rows\n", rows);

	starttime = micro_now();

	for (x=0; x< rows; x++) {
		pc = json_object_array_get_idx(postcodes,x);
		json_object_object_get_ex(pc, "p",&postcode);
		json_object_object_get_ex(pc, "n",&number);
		const char *zip=NULL;
		const char *house=NULL;
		const char *street ="";
		const char *city ="";
		const char *country ="NL";

		if (postcode) zip = json_object_get_string(postcode);
		if (number) house = json_object_get_string(number);

		prox_t res= k.KloptGeocode
			(country,city,street,house,zip);

		result.push_back(res);
	}
	enddtime = micro_now();
	return true;
}

intptr_t CommandService::Handle(Client *c)
{
	Socket *sock = c->getSocket();
	json_object *content=NULL;

	string s = FGetS(c);
	if (s == "") return 1;

	printf("Got %s from client\n", s.c_str());

	stringstream ss(s);
	string command;
	if (getline(ss,command,':')) {
		printf("command is %s\n", command.c_str());
	} 
	string context;
	if (getline(ss,context,':')) {
		printf("context is %s\n", context.c_str());
	} 
	string orderset;
	if (getline(ss,orderset,':')) {
		printf("orderset is %s\n", orderset.c_str());
	} 
	string data;
	if (getline(ss,data,':')) {
		printf("data is %s\n", data.c_str());
	} 

	string str;
	if (command == "setorders") {
		vector<Location *>os = handle_setorders(context,orderset,data);
		str = csv_report_setorders(os);
	} else  
	if (command == "getdistances") {
		str = handle_getdistances(context,orderset,data);
		//string str = csv_report_getdistances(os);
	} else  
	if (command == "getmatrixrow") {
		str = handle_getmatrixrow(context,orderset,data);
	} 

	char *buf = (char *)str.c_str();
	cout << buf << endl;
	write(sock->fd, buf, strlen(buf));

	return CS_CONT;
}
