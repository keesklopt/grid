#include <app.h>
#include <cstdlib>
#include <string.h>
#include <iostream>
#include <memory>
//#include <unicode/unistr.h>
//#include <unicode/normalizer2.h>
#ifdef USE_MONGO
#include "mongo/client/dbclient.h"
#elif USE_MONGO_CLIENT
#include "mongo.h"
#endif

#include <unicode/utypes.h>
#include <unicode/unistr.h>
#include <unicode/translit.h>
#include <unicode/uclean.h>

using namespace std;
#ifdef USE_MONGO
using namespace mongo;
using namespace bson;
#endif

Klopt::Klopt()
{
	// one time setup of mongodb connection
#ifdef USE_MONGO
	try {
		this->conn.connect("localhost");
    	std::cout << "Mongodb : connected ok" << std::endl;
  	} catch( const mongo::DBException &e ) {
    	std::cout << "caught " << e.what() << std::endl;
  	}
#endif
 // 	return EXIT_SUCCESS;
#ifdef USE_MONGO_CLIENT
	this->conn = mongo_sync_connect ("localhost", 27017, TRUE);
	if (!this->conn) {
     		perror ("mongo_sync_connect()");
     		exit (1);
   	}
#endif
    this->dawn = time_from_string("2014-12-28 08:00");
    this->fault = time_from_string("2015-01-13 21:22"); 
    this->up   = second_clock::local_time();
}

Klopt::~Klopt()
{
#ifdef USE_MONGO
    BSONObj obj;
	this->conn.logout("localhost",obj); 
    std::cout << "Mongodb : disconnected ok" << std::endl;
#endif
}

#ifdef USE_MONGO
string Klopt::get_city(string id)
{
    // is an id in the number collection : 
    string city="";
	BSONObjBuilder bb;
	bb.append("id", id);

	unique_ptr<DBClientCursor> cursor =
	this->conn.query("bag.city", bb.obj());
	while (cursor->more()) { 
		BSONObj  cn = cursor->next();
        city = cn.getStringField("name");
        break; // first will do
	} 
    return city;
}

using namespace std;

std::string normalize(const std::string& str) {
    // UTF-8 std::string -> UTF-16 UnicodeString
    std::locale loc;
    UnicodeString source = UnicodeString::fromUTF8(StringPiece(str));

    // Transliterate UTF-16 UnicodeString
    UErrorCode status = U_ZERO_ERROR;
    Transliterator *accentsConverter = Transliterator::createInstance(
        "NFD; [:M:] Remove; NFC", UTRANS_FORWARD, status);
    accentsConverter->transliterate(source);
    // TODO: handle errors with status

    delete accentsConverter;


    // UTF-16 UnicodeString -> UTF-8 std::string
    std::string result;
    source.toUTF8String(result);

    for (std::string::size_type i=0; i<result.length(); ++i)
        result[i] = std::tolower(result[i],loc);

    u_cleanup();
    cout << "---" << result << endl;
    return result;
}

string Klopt::get_city_id(string name)
{
    string city="";
	BSONObjBuilder bb;

    string norm = normalize(name);
	bb.append("norm", norm);

	unique_ptr<DBClientCursor> cursor =
	this->conn.query("bag.city", bb.obj());
    //cout << bb.obj().toString();
	while (cursor->more()) { 
		BSONObj  cn = cursor->next();
        city = cn.getStringField("id");
        break; // first will do
	} 
    cout << "S: " << city << endl;
    return city;
}

string Klopt::get_street_id(string city_id, string street)
{
    string city="";
	BSONObjBuilder bb;
	bb.append("city_id", city_id);
    string norm = normalize(street);
	bb.append("norm", norm);

	unique_ptr<DBClientCursor> cursor =
	this->conn.query("bag.street", bb.obj());
	while (cursor->more()) { 
		BSONObj  cn = cursor->next();
        city = cn.getStringField("id");
        break; // first will do
	} 
    return city;
}

/* count maintains one record for each operation and just 
    keeps adding . so a total count */
void Klopt::count(string id,int req,int num)
{
	BSONObjBuilder b,c,e,u;
	b.append("operation", id);

    c.append("req",req);
    c.append("count",num);
    e.append("$inc",c.obj());

	this->conn.update("bag.statistics", b.obj(),e.obj(),true,true);
}

/* count maintains one record for each operation and just 
    keeps adding . so a total count */
string Klopt::counts(void)
{
    stringstream ss;

	unique_ptr<DBClientCursor> cursor =
	this->conn.query("bag.statistics"); 
	while (cursor->more()) { 
		BSONObj  cn = cursor->next();
        if (ss.str().size()>0) ss << ",";
        ss << "\"" << cn.getStringField("operation") << "\":";
        ss << "{\"count\":";
        ss << cn.getIntField("count");
        ss << ",\"req\":";
        ss << cn.getIntField("req");
        ss << "}";
	} 

    return ss.str();
}

/* log creates a record for each request, and logs time and ip adres
    */
void Klopt::log(string id,int num, string ip)
{
	BSONObjBuilder b,c,e;
	b.append("operation", id);

    ptime now = second_clock::local_time(); //use the clock 

    c.append("count",num);
    c.append("time", to_iso_string(now));
    c.append("origin",ip);

	this->conn.insert("bag.requestlog", c.obj());
}

void Klopt::initstats(string str)
{
	BSONObjBuilder b,c,e;
	b.append("operation", str);
	b.append("count", 0);

	this->conn.insert("bag.statistics", b.obj());
}

void Klopt::get_street(string id,addr_t &addr)
{
    // is an id in the number collection : 
    string street="";
	BSONObjBuilder bb;
	bb.append("id", id);

	unique_ptr<DBClientCursor> cursor =
	this->conn.query("bag.street", bb.obj());
	while (cursor->more()) { 
		BSONObj  cn = cursor->next();
        addr.street = cn.getStringField("name");
        addr.city = get_city(cn.getStringField("city_id"));
        break; // first will do
	} 
}

poly_t Klopt::klopt_get_pand(string searchid)
{
    poly_t ret;

    // is an id in the number collection : 
	BSONObjBuilder bb;
	bb.append("id", searchid);

	unique_ptr<DBClientCursor> cursor =
	this->conn.query("bag.pand", bb.obj());

	while (cursor->more()) { 
		BSONObj  cn = cursor->next();
   		cout << ":" << cn.toString() << endl;
        ret.npoints = cn.getIntField("npoints");
        ret.points = cn.getStringField("points");
	} 

    return ret;
}

addr_t Klopt::klopt_get_address(double x, double y,int max, micro_t *timings, poly_t *pand)
{
	json_object *json=NULL;
	json_object *stat;
	char *data;
	int c=0;
	int n=0;
    micro_t starttime= micro_now();

	addr_t ret;
	static char region[1000] = {0};

	// construct a command resembling for example : 
	// db.runCommand({geoNear: "house", near: {type: "Point", coordinates:[52,5]},spherical: true})

	BSONObjBuilder b;
	BSONObjBuilder near;

	b.append("geoNear","house");

	BSONArrayBuilder ba(2);
	ba.append(x);
	ba.append(y);
	near.append("type", "Point");
	near.appendArray("coordinates", ba.arr());

    printf("Doing geonear with %f, %f\n", x, y);

    b.append("near",near.obj());
	b.append("limit", 1);
	b.append("spherical", "true");

    if (max > 0) { 
	    b.append("maxDistance", max);
    } 

    /* coordinate match will give the closest record in the house collection */
	string addridd;
    string searchidd;
	BSONObj commandResult;
	this->conn.runCommand("bag", b.obj(),commandResult);
	cout << commandResult << endl;

    timings[0] = micro_now()-starttime;
    starttime += timings[0];

	BSONObj results = commandResult.getObjectField("results");
	if (!results.isValid()) return ret;
	BSONObj elm = results.getObjectField("0");  
	if (!elm.isValid()) return ret;
	BSONObj obj = elm.getObjectField("obj");
	if (!obj.isValid()) return ret;
	BSONElement addrid = obj.getField("id");
	if (addrid.eoo()) return ret;

	addridd = addrid.String();
    cout << "Found address " << addridd << endl;

	/* unused but examplary 
    BSONElement it = i.next();
	while (i.more()) { 
   		cout << it.toString() << endl;
		it = i.next();
	} // last one will do
	*/

	if (addridd== "") return ret;    // not found at all

	BSONElement searchid = obj.getField("hoofdadres");
	if (searchid.eoo()) {
	    searchid = obj.getField("nevenadres");
    } 
	if (searchid.eoo()) return ret; // nothing found(data problem!) 
    cout << searchid << endl;

    // is an id in the number collection : 
	BSONObjBuilder bb;
	bb.append("id", searchid.String());

	unique_ptr<DBClientCursor> cursor =
	this->conn.query("bag.number", bb.obj());

    timings[1] = micro_now()-starttime;
    starttime += timings[1];

	while (cursor->more()) { 
		BSONObj  cn = cursor->next();
   		cout << ":" << cn.toString() << endl;
        ret.zipcode = cn.getStringField("postcode");
        ret.number = cn.getStringField("nummer");
        ret.country = "NL" ;
        get_street(cn.getStringField("street_id"),ret);
	} 

    // if pand is needed : 
    if (pand != NULL) { 
	    BSONElement pandid = obj.getField("pand");
	    if (pandid.eoo()) { 
            pand->npoints = 0;
            pand->points = "";
        } else { 
            poly_t p = klopt_get_pand(pandid.String());
            pand->npoints = p.npoints;
            pand->points = p.points;
        }  
    } 

    timings[2] = micro_now()-starttime;

	return ret;
}

prox_t Klopt::klopt_get_coord(const char *id,const char *cc, const char *city, const char *street, const char *house,const char *pc)
{
	json_object *json=NULL;
	json_object *stat;
	char *data;
    char *uppc=NULL;
	int c=0;
	int n=0;

	prox_t ret={-180.0,-180.0}; /// biggest is 180.0 90.0

    // the PC letters are uppercase in the database
    if (pc) {
        uppc=strdup(pc);
        for (unsigned int t=0; t< strlen(pc); t++) // so make the search upper as well
            uppc[t] = toupper(pc[t]);
    }

	static char address[1000] = {0};
	static char region[1000] = {0};

	BSONObjBuilder b;
	if (pc && pc[0]) { 
		b.append("postcode", uppc);
    } else {    // we need a sub queries on street/city
	    if (city[0] && street[0]) { 
            string city_id= get_city_id(city);
            cout << "city " << city << " became " << city_id << std::endl;
            string street_id= get_street_id(city_id,street);
            cout << "street " << street << " became " << street_id << std::endl;
		    b.append("street_id", street_id);
        } else {
            free(uppc);
            return ret; // we won't find anything
        } 
    } 
	if (house) 
		b.append("nummer", house);

    free(uppc);
    cout << house << endl;

	string addridd;
	unique_ptr<DBClientCursor> cursor =
	this->conn.query("bag.number", b.obj());
	while (cursor->more()) { 
		BSONObj  cn = cursor->next();
   		cout << cn.toString() << endl;
		//out << cn.getStringField("id");
		addridd = cn.getStringField("id");
	} // last one will do

	//printf("address is gvd %s\n", addridd.c_str());

	if (addridd== "") return ret;
//	cout << addridd << endl;
	BSONObjBuilder bb;
	bb.append("hoofdadres", addridd);

	BSONObj points;
	cursor =
	this->conn.query("bag.house", bb.obj());
	while (cursor->more()) { 
		BSONObj cn = cursor->next();
        BSONObj coords = cn.getObjectField("coords");
		points = coords.getObjectField("coordinates");
	} 

	if (!points.isValid() || points.isEmpty()) {  // try nevenadres 
		BSONObjBuilder bb;
		bb.append("nevenadres", addridd);

		cursor =
		this->conn.query("bag.house", bb.obj());
		while (cursor->more()) { 
		    BSONObj cn = cursor->next();
            BSONObj coords = cn.getObjectField("coords");
		    points = coords.getObjectField("coordinates");
		} 
	} // 0502200000033454

	if (!points.isValid() || points.isEmpty()) {  // try nevenadres 
		BSONObjBuilder bb;
		bb.append("relative", addridd);

		cursor =
		this->conn.query("bag.house", bb.obj());
		while (cursor->more()) { 
		    BSONObj cn = cursor->next();
            BSONObj coords = cn.getObjectField("coords");
		    points = coords.getObjectField("coordinates");
		} 
	} // 0502200000033454

	if (!points.isValid() || points.isEmpty()) return ret;

    BSONObjIterator i(points);
    
    BSONElement bo = i.next();
    BSONType tp = bo.type();
    ret.x = bo.Number();
    bo = i.next();
	ret.y = bo.Number();

	return ret;
}
#endif

prox_t Klopt::KloptGeocode(const char *country, const char *city, const char *street, const char *num, const char *zipcode)
{
	prox_t p = klopt_get_coord((char *)"",country,city,street,num,zipcode);
    cout << p.x << endl;
	if (p.x==-180 || p.y == -180) 
		p = klopt_get_coord((char *)"",country,city,street,NULL,zipcode);
    cout << p.x << endl;
	return p;
}

addr_t Klopt::KloptRevGeocode(double x, double y,int max,micro_t *timings,poly_t *pand)
{
	addr_t p = klopt_get_address(x,y,max,timings,pand);
	return p;
}

#ifdef USE_MONGO
void Klopt::set_matrixrow(int fx, int fy,int tx, int ty,route_result_t r)
{
	BSONObjBuilder b;
	b.append("fx", fx);
	b.append("fy", fy);
	b.append("tx", tx);
	b.append("ty", ty);
	b.append("meters", r.meters);
	b.append("seconds", r.seconds);

	this->conn.insert("network.matrix", b.obj());
}

route_result_t Klopt::get_matrixrowcol(int fx, int fy,int tx, int ty,bool dummy)
{
	route_result_t ret = {-1,-1};

	BSONObjBuilder b;
	b.append("fx", fx);
	b.append("fy", fy);
	b.append("tx", tx);
	b.append("ty", ty);
	unique_ptr<DBClientCursor> cursor =
	this->conn.query("network.matrix", b.obj());
	while (cursor->more()) { 
		BSONObj  cn = cursor->next();
   		//cout << cn.toString() << endl;
		//out << cn.getStringField("id");
		BSONElement e = cn["meters"];
		ret.meters = e.Int();
		e = cn["seconds"];
		ret.seconds = e.Int();
		//ret.seconds = cn.getStringField("seconds");
	} // last one will do
	const BSONObj bo = fromjson("{fx:1,fy:1,tx:1,ty:1},{unique:true}");
	this->conn.ensureIndex("network.matrix", bo);

	return ret;
}

vector<matrix_result_t>Klopt::get_matrixrow(int fx, int fy)
{
	vector<matrix_result_t>ret;
	matrix_result_t entry;

	BSONObjBuilder b;
	b.append("fx", fx);
	b.append("fy", fy);

	unique_ptr<DBClientCursor> cursor =
	this->conn.query("network.matrix", b.obj());

	int i=0;
	while (cursor->more()) { 
		BSONObj  cn = cursor->next();
		BSONElement e;
		entry.fx=fx;
		entry.fy=fy;
		e = cn["meters"]; entry.meters = e.Int();
		e = cn["seconds"]; entry.seconds = e.Int();
		e = cn["tx"]; entry.tx = e.Int();
		e = cn["ty"]; entry.ty = e.Int();
		ret.push_back(entry);
		i++;
		//ret.seconds = cn.getStringField("seconds");
	} // last one will do
	const BSONObj bo = fromjson("{fx:1,fy:1,tx:1,ty:1}");
	this->conn.ensureIndex("network.matrix", bo, /*unique=*/ 1);

	return ret;
}
#endif

