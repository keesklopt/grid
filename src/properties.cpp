#include <app.h>
#include <iostream>
#include <fstream>

#include <syslog.h>

Properties::Properties(std::string fname) {
    readFile(fname);
} 

Properties::~Properties(void) {
	json_object_put(this->root);
} 

string Properties::getStringProperty(char *key)  {
    json_object *o;
    string s="";

    if (this->root == NULL) return "";

    if (json_object_object_get_ex(this->root, key, &o)) { 
        s = json_object_get_string(o);
        return s;
    } 

    return "";
} 

int Properties::getIntProperty(char *key)  {
    return 0;
} 

int Properties::readFile(std::string name) { 
    string buf;
    string line;

    ifstream jsonfile(name);

    this->root=NULL;

    if (jsonfile.is_open()) {
        while (getline(jsonfile,line)) { 
            buf += line;
        } 
        jsonfile.close();
        
        this->root = json_tokener_parse(buf.c_str());
    } else { 
        printf("could not open properties.json file %s\n", name.c_str());
        syslog(LOG_ERR, "could not open properties.json file %s\n", name.c_str());
        exit(-1);
    } 

    return 1;
} 
