#include <app.h>

vector<Client *> Service::getClients(void)
{
	return this->clts;
}

Client::Client (Socket *sock) {
	this->sock=sock;
	this->state = -1;

} 

Client::~Client() {
	if (this->sock) delete this->sock;
	this->sock=NULL;
} 

Socket *Client::getSocket(void)
{
	return this->sock;
}

void Client::setState(int state)
{
	this->state=state;
}

void Client::setService(Service *srvc)
{
	this->service=srvc;
}

int Client::Handle(void) {
	return service->Handle(this);
} 

// delete self from service
int Client::Del(void) {
	return this->service->Del(this);
} 

Service::Service(int port, int type) {
	port=port;
	type=type;

	this->sock = new Socket(SOCK_STREAM);
	this->sock->reUse();
	this->sock->setHostByName("localhost");
	this->sock->Bind(port);

	if (type == SOCK_DGRAM) return;

	if (sock->Listen() ) {
		perror("Listen");
	} 
}

int Service::Del(Client *c)
{
	int pos=0;
	for (vector<Client *>::iterator it = this->clts.begin();
		it != this->clts.end(); it++) 
	{
		if (*it == c) break;
		pos++;
	}

	this->clts.erase(this->clts.begin()+pos);
	return this->clts.size();
}

Service::~Service() {
	delete this->sock;
}

Socket *Service::getSocket(void)
{
	return sock;
}

void Service::setServer(KloptServer *srv)
{
	this->srv =srv;
}

int Service::Add(Client *c)
{
	this->clts.push_back(c);
	return this->clts.size();
}

