#include <app.h>
#include <stdio.h>

BagService::BagService(int port,int type) : Service(port,type) 
{
} 

string BagService::FGetS(Client *clt)
{
	string s="";
	char c;
	int total=0;
	int ret=0;
	Socket *sock = clt->getSocket();

	// read all
	do { 
		ret = read(sock->fd,&c,1);
		if (ret==0) return s; // disconnect, try to send whatever you read
		s += c;
	} while (ret==1 && c != '\n');

	total+=ret;

	return s;
}

string BagService::csv_report(void)
{
	stringstream ss;
	vector<prox_t>::iterator it;
	double lastx=-180.0;
	double lasty=-180.0;
	double meters=88;

	int lineno=0;
	for (it=this->result.begin(); it!= this->result.end(); it++) {
		prox_t p = (prox_t) *it;
		if (lineno) ss<< ",";
		ss << p.x << "," << p.y << "," << meters;
		lineno++;
	} 
	ss << endl;
	
	return ss.str();
}

string BagService::json_report(void)
{
	stringstream ss;
	vector<prox_t>::iterator it;
	double lastx=-180.0;
	double lasty=-180.0;
	double meters=88;

	ss << "{\"jsonrpc\":\"2.0\",\"id\":\""<< this->id << "\"";
	ss << "\"method\":\"geocode\",\"params\":{\"postcodes\":[";
	for (it=this->result.begin(); it!= this->result.end(); it++) {
		prox_t p = (prox_t) *it;
		ss << "{\"x\":" << p.x;
		ss << ",\"y\":" << p.y;
		ss << ",\"meters\":" << meters << "}";
	} 
	ss << endl;
	
	return ss.str();
}

bool BagService::handle_geocode(json_object *content)
{
	static char erbuf[2048];
	static char buf[40960];
	json_object *options=NULL;
	json_object *params=NULL;
	json_object *pc=NULL;
	json_object *postcode=NULL;
	json_object *number=NULL;
	json_object *postcodes=NULL;
	json_object *timings=NULL;
	int dotimes=0; // default
	int x, y;
	this->id=666;
	micro_t starttime;
	micro_t enddtime;
	//prox_t (*fnc) (char *zip, char *street, char *city, char *house, char *country)=NULL;

	json_bool testparams = json_object_object_get_ex(content, "params",&params);
	if (!testparams) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		//return result;
	}
	//json_object_object_get_ex(params, "options",&options);
	json_bool testpostcodes = json_object_object_get_ex(params, "postcodes",&postcodes);
	if (!testpostcodes) {
		sprintf(erbuf, "geocode function needs postcodes");
		web_error(id, -14,erbuf);
		return false;
		//return result;
	}

/*
	result << "{\"jsonrpc\": \"2.0\",";
	if (id) {
		result << "\"id\": " << id ;
	}
	result << "\"result\": ";
*/

	int rows = json_object_array_length(postcodes);
	int len=0;
	//printf("got %d rows\n", rows);

	starttime = micro_now();

	for (x=0; x< rows; x++) {
		pc = json_object_array_get_idx(postcodes,x);
		json_object_object_get_ex(pc, "p",&postcode);
		json_object_object_get_ex(pc, "n",&number);
		const char *zip=NULL;
		const char *house=NULL;
		const char *street ="";
		const char *city =(char *)"";
		const char *country =(char *)"NL";

		if (postcode) zip = json_object_get_string(postcode);
		if (number) house = json_object_get_string(number);

		prox_t res= k.KloptGeocode
			(country,city,street,house,zip);

		result.push_back(res);
	}
	enddtime = micro_now();
	return true;
}

intptr_t BagService::Handle(Client *c)
{
	Socket *sock = c->getSocket();
	json_object *content=NULL;

	string s = FGetS(c);
	if (s == "") return 1;

	printf("Got %s from client\n", s.c_str());

	content = json_tokener_parse((char *)s.c_str());

	printf("Content %p\n", content);
	handle_geocode(content);

	//string str = json_report();
	string str = csv_report();
	char *buf = (char *)str.c_str();

	cout << str << endl;
	
	write(sock->fd, buf, strlen(buf));

	return 0;
}
