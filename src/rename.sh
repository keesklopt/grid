#!/bin/bash
if [ $# -le 0 ]; then
    echo no network name given
    exit
fi

for f in *.osrm.*; do
    arr=(${f//\./ })
    mv $f $1.osrm.${arr[2]}
done
