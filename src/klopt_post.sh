#!/bin/bash
systemctl enable klopt_srv.service
systemctl enable klopt_geo.service
systemctl enable klopt_rte.service
systemctl start klopt_srv.service
systemctl start klopt_geo.service
systemctl start klopt_rte.service
