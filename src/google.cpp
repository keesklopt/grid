//#include "fcgi_stdio.h"

#include <cstddef>

#include <curl/curl.h>
#include <app.h>

#include "privatekey.h"

#define MAX_MESSAGE_LENGTH 4096
#define BUFSIZE 4096
//#define goog_url "http://maps.googleapis.com/maps/api/distancematrix/json?sensor=false"
#define goog_url "http://maps.googleapis.com"

static size_t fill_buf(void * ptr, size_t memberSize, size_t nofMembers, void *userdata) {
    //buf_t *stretchbuf=(buf_t *)userdata;
    stringstream *ss = (stringstream *)userdata;

    //buf_fwrite(ptr, memberSize, nofMembers, stretchbuf);
    ss->write((char *)ptr,memberSize*nofMembers);

    //printf("Got %d and %d \n", (int)memberSize, (int)nofMembers);
    return nofMembers*memberSize;
}

Mat<int32_t> *handle_result(Mat<int32_t> *mp, string data) 
{
    json_object *jt;
	json_object *rows;
	json_object *row;
	json_object *elements;
	json_object *element;
	json_object *distance;
	json_object *duration;
	json_object *value;
	json_object *status;
	int x,y;

	jt = json_tokener_parse((char *)data.c_str());

	json_object_object_get_ex(jt,"rows",&rows);
	int nrows = json_object_array_length(rows);

	for (x=0; x < nrows; x++) {
		row = json_object_array_get_idx(rows,x);
		if (json_object_object_get_ex(row,"elements",&elements) == false) return 0;
		int nelements = json_object_array_length(elements);
		for (y=0; y< nelements; y++) {
			int32_t result=-1;
			if ((element = json_object_array_get_idx(elements,y))==NULL) return 0;
			if (json_object_object_get_ex(element,"status",&status) == false) return 0;
			if (strcmp(json_object_get_string(status),"OK") == 0) {
				if (json_object_object_get_ex(element,"distance",&distance) == false) return 0;
				if (json_object_object_get_ex(element,"duration",&duration) == false) return 0;
				if (json_object_object_get_ex(distance,"value",&value) == false) return 0;
				result = (int32_t)json_object_get_int(value);
			}
			mp->Set(x,y,result);
		}
	}
	
	return mp;
}

Mat<int32_t> *GoogleMatrix(int ndeps, coord_t *deps, int narrs, coord_t *arrs)
{
	double val;
	int x,y;
	char url[BUFSIZE];
	char workstring[BUFSIZE] = {0};
	CURL *curlHandle;
	Mat<int32_t> *mp = new Mat<int32_t>(ndeps,narrs);

    snprintf(url, BUFSIZE, "%s", goog_url);
	for(x=0; x< ndeps; x++) {
		if (x==0)
    		snprintf(workstring, BUFSIZE, "&origins=%f,%f", deps[x].y, deps[x].x);
		else
    		snprintf(workstring, BUFSIZE, "|%f,%f", deps[x].y, deps[x].x);
		strcat(url,workstring);
	}

	for(y=0; y< narrs; y++) {
		if (y==0)
    		snprintf(workstring, BUFSIZE, "&destinations=%f,%f", arrs[y].y, arrs[y].x);
		else
    		snprintf(workstring, BUFSIZE, "|%f,%f", arrs[y].y, arrs[y].x);
		strcat(url,workstring);
	}


//	printf("buffer is %s\n", url);

	curlHandle = curl_easy_init();
    stringstream ss;
    //buf_t *buf =buf_ist(0);

    curl_easy_setopt(curlHandle, CURLOPT_URL, url);
    curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, fill_buf);
    curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &ss);
    int res = curl_easy_perform(curlHandle);

	//char *tostr = buf_get(buf,0);
	mp = handle_result(mp, ss.str());
	return mp;
}

static void spaces(char *orig){
	while (*orig) {
		if (*orig == ' ') *orig='+';
		orig++;
	}
}

#ifdef ENCODE
static void to_urlsafe(char *orig){
	while (*orig) {
		if (*orig == '+') *orig='-';
		if (*orig == '/') *orig='_';
		orig++;
	}
}

static void from_urlsafe(char *orig){
	while (*orig) {
		if (*orig == '-') *orig='+';
		if (*orig == '_') *orig='/';
		orig++;
	}
}

static char *encodeurl(char *oldurl)
{
#define  PLENTYSPACE 2048
	char *privateKey=(char *)PKEY;
	char url[PLENTYSPACE];
	char encoded[100]={0};
    char mac[200]={0};
	size_t len;

	from_urlsafe(privateKey);
	len = strlen(privateKey);
	base64_decode(privateKey, strlen(privateKey), encoded, &len);
	//printf("Pkey is \"%s\"\n", encoded);
	//printf(" %d\n", len);
    hmac_sha1(encoded, strlen(encoded), oldurl, strlen (oldurl), mac);
	//printf("Pkey is \"%s\"\n", mac);
	base64_encode(mac, 20, encoded, 100);
	//printf("Pkey is \"%s\"\n", encoded);
	to_urlsafe(encoded);

	snprintf(url, PLENTYSPACE, "http://maps.googleapis.com%s&signature=%s", 
		oldurl, encoded);

	return strdup(url);
}
#endif

static size_t handle_coord(void *ptr, size_t memberSize, size_t nofMembers, void *userdata) {
    stringstream *ss=(stringstream *)userdata;
	//buf_t *stretchbuf=(buf_t *)userdata;

    ss->write((char *)ptr,memberSize*nofMembers);
	//buf_fwrite(ptr, memberSize, nofMembers, stretchbuf);

	//coord_t *ret= (coord_t *)userdata;
	//printf("Got %d and %d \n", (int)memberSize, (int)nofMembers);
	return nofMembers*memberSize;
}

#define GOOG_URL "http://maps.googleapis.com/maps/api/geocode/json"

static prox_t google_get_coord(char *id,char *cc, char *city, char *street, char *house,char *pc)
{
	static char erbuf[2048];
	json_object *json=NULL;
	json_object *stat;
	CURL *curlHandle;
	CURLcode res;
	char url[BUFSIZE]={0};
	char encoded[BUFSIZE];
	char *data;
	char *clientid = (char *)CID;
	//buf_t *buf =buf_ist(0);
    stringstream ss;
    const char *s;
	int c=0;
	int n=0;

	prox_t ret={-180.0,-180.0}; /// biggest is 180.0 90.0

//#define USING_KEY 1
#ifdef USING_KEY
	snprintf(url, BUFSIZE, "%s&client=%s&region=%s&address=%s", goog_url, clientid, cc, pc);
	char *cpy = encodeurl(url);
	snprintf(encoded, BUFSIZE, "%s", cpy);
	free(cpy);
	//printf("url now %s\n", url);
#else
//#error Hey!! should be using key
	//address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=true_or_false

	static char address[1000] = {0};
	static char region[1000] = {0};

	// most precise : housenumber street,city
	if (house[0] && street[0] && city[0]) { 
		sprintf(address, "address=%s %s %s", house, street, city);
	} else  
	// second comes postal code
	if (pc[0]) { 
		sprintf(address, "components=postal_code:%s", pc);
	} else  
	// street without housenumber
	if (street[0] && city[0]) { 
		sprintf(address, "address=%s %s", street, city);
	} else 
	// just city ?
	if (city[0]) { 
		sprintf(address, "components=locality:%s", city);
	} 
	if (cc[0]) { 
		sprintf(region, "&region=%s", cc);
	} 
	
	snprintf(encoded, BUFSIZE, "%s?%s%s&sensor=false", GOOG_URL, address,region);
#endif

	curlHandle = curl_easy_init();
	spaces(encoded);

	//char *ptr = curl_easy_escape(curlHandle,encoded,strlen(encoded));
	//printf("url now %s\n", encoded);

	curl_easy_setopt(curlHandle, CURLOPT_URL, encoded);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, handle_coord);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &ss);
	res = curl_easy_perform(curlHandle);

	if (res) {
		//web_error(id, -1,"Could not read google coordinates");
		sprintf(erbuf, "Could not resolve combination : (%s,%s)", cc, pc);
		//web_error(id, -6,erbuf);
		goto cleanup;
	}
	//data = buf_strdup(buf);
    data = (char *)ss.str().c_str();
	//buf_rls(buf);
	json = json_tokener_parse(data);
	free(data);
	if (!json) {
		//web_error(id, -9,"Could not read google coordinates");
		goto cleanup;
	}
	json_object_object_get_ex(json, "status",&stat);
    s = json_object_get_string(stat);
	if (s && !strcmp(s,"OK")) {
		json_object *result, *arr;
		json_object *geom;
		json_object *fmtaddr;
		json_object *location;
		json_object *x;
		json_object *y;
		json_object *components;
		json_object_object_get_ex(json, "results", &result);
		arr = json_object_array_get_idx(result, 0);
		json_object_object_get_ex(arr, "geometry", &geom);
		json_object_object_get_ex(arr, "formatted_address",&fmtaddr);
		json_object_object_get_ex(geom, "location", &location);
		json_object_object_get_ex(location, "lng",&x);
		json_object_object_get_ex(location, "lat",&y);
		json_object_object_get_ex(arr, "address_components",&components);

		json_object *component;
		json_object *types;
		json_bool ttypes;
		json_object *tpname;
		json_object *shortname;

		// find out if the country code is correct 
		for (c=0;;c++) {
			component = json_object_array_get_idx(components,c);
			if (!component) break;
			ttypes = json_object_object_get_ex(component,"types",&types);
			if (!ttypes) continue;
			for (n=0;; n++) {
				tpname = json_object_array_get_idx(types,n);
				if (!tpname) break;
				if (strcmp(json_object_get_string(tpname), "country")==0) {
					json_object_object_get_ex(component,"short_name",&shortname);
					if (strcasecmp(json_object_get_string(shortname),cc) != 0) {
						sprintf(erbuf, "Country code mismatch; %s != %s", json_object_get_string(shortname),cc );
						//web_error(NULL, -13, erbuf);
						goto cleanup;
					}
				}
			}
		} 
	
		ret.x = json_object_get_double(x);
		ret.y = json_object_get_double(y);
		ret.name = strdup(json_object_get_string(fmtaddr));
		//printf("fmtaddr : %p\n", fmtaddr);
		curl_easy_cleanup(curlHandle); 
	} else 
		//web_error(id, -14,"Google returned NO RESULT");

cleanup:
	if (json) json_object_put(json);
	return ret;
}

static addr_t google_get_address(double x, double y)
{

//?latlng=40.714224,-73.961452&sensor=true_or_false
	static char erbuf[2048];
	json_object *json=NULL;
	json_object *stat;
	CURL *curlHandle;
	CURLcode res;
	char url[BUFSIZE]={0};
	char encoded[BUFSIZE];
	const char *data;
    const char *s;
	char *clientid = (char *)CID;
    stringstream ss;
	int c=0;
	int n=0;

	addr_t ret={0}; 

//#define USING_KEY 1
#ifdef USING_KEY
	snprintf(url, BUFSIZE, "%s?latlng=%f,%f&sensor=false", goog_url, x, y);
	char *cpy = encodeurl(url);
	snprintf(encoded, BUFSIZE, "%s", cpy);
	free(cpy);
	//printf("url now %s\n", url);
#else
//#error Hey!! should be using key
	snprintf(encoded, BUFSIZE, "%s?latlng=%f,%f&sensor=false", GOOG_URL, x,y );
	//encoded = url;
	//snprintf(encoded, BUFSIZE, "http://maps.googleapis.com%s", url);
	//printf("url now %s\n", encoded);
#endif

	curlHandle = curl_easy_init();

	curl_easy_setopt(curlHandle, CURLOPT_URL, encoded);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, handle_coord);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &ss);
	res = curl_easy_perform(curlHandle);

	if (res) {
		//web_error(id, -1,"Could not read google coordinates");
		sprintf(erbuf, "Could not resolve combination : (%f,%f)", x,y );
		//web_error(id, -6,erbuf);
		goto cleanup;
	}
    data = ss.str().c_str();
	json = json_tokener_parse(data);
	//free(data);
	if (!json) {
		//web_error(id, -9,"Could not read google coordinates");
		goto cleanup;
	}
	json_object_object_get_ex(json, "status", &stat);
    s = json_object_get_string(stat);
	if (s && !strcmp(s,"OK")) {
		json_object *result;
		json_object *geom;
		json_object *fmtaddr;
		json_object *location;
		json_object *x;
		json_object *y;
		json_object *components;

		json_object_object_get_ex(json, "results",&result);
		json_object *arr = json_object_array_get_idx(result, 0);
		json_object_object_get_ex(arr, "geometry", &geom);
		json_object_object_get_ex(arr, "formatted_address", &fmtaddr);
		json_object_object_get_ex(geom, "location", &location);
		json_object_object_get_ex(location, "lng",&x);
		json_object_object_get_ex(location, "lat",&y);
		json_object_object_get_ex(arr, "address_components", &components);

		if (components) {
			json_object *component;
			json_object *types;
			json_bool ok, tshort_name;
			json_object *tpname;
			json_object *short_name;
			json_object *longname;
			int nc = json_object_array_length(components);
			int i;
			for (i=0; i< nc; i++) {
				component = json_object_array_get_idx(components, i);
				tshort_name = json_object_object_get_ex(component,"short_name", &short_name);
				json_object_object_get_ex(component,"long_name",&longname);
				ok= json_object_object_get_ex(component,"types",&types);
				if (ok) {
					int nt = json_object_array_length(types);
					int t;
					for (t=0; t< nt; t++) {
						tpname = json_object_array_get_idx(types,t);
                        s = json_object_get_string(tpname);
						if (!tpname || !s || !tshort_name) continue;
						if (strcmp(s,"country")==0) {
							ret.country = strdup(s);
						}
						if (strcmp(s,"postal_code")==0) {
							ret.zipcode = strdup(s);
						} 
						if (strcmp(s,"locality")==0) {
							ret.city = strdup(s);
						} 
						if (strcmp(s,"route")==0) {
							ret.street = strdup(s);
						} 
						if (strcmp(s,"streetnumber")==0) {
							ret.number = strdup(s);
						} 
					}
				}
			} 
		}

        s = json_object_get_string(fmtaddr);
		ret.name = strdup(s);
		//printf("fmtaddr : %p\n", fmtaddr);
		curl_easy_cleanup(curlHandle); 
	} else 
		//web_error(id, -14,"Google returned NO RESULT");

cleanup:
	if (json) json_object_put(json);
	return ret;
}

prox_t GoogleGeocode(char *country, char *city, char *street, char *num, char *zipcode)
{
	return google_get_coord((char *)"",country,city,street,num,zipcode);
}

addr_t GoogleEdocoeg(double x, double y)
{
	return google_get_address(x,y);
}
