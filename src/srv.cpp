/* mainly because fastci and mongo db bite eachother
	but also for convenience, and for smoorther debugging
	tomcat(&) was tried, but gave to often an out of memory error
	and of course the main code is C 
	A standalone server version
 */
#include <vector>
#include <app.h>
#include <fcntl.h>

#include <execinfo.h>
#include <signal.h>

#include <syslog.h>

#include <unicode/uclean.h>

#define MAXHOSTNAMELEN 64

using namespace std;

static FILE *logfp;
static Client *lastclient;

void handler(int sig) {
  void *array[10];
  char **fnames;
  int size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  syslog(LOG_ERR, "------------- start report\n");
  syslog(LOG_ERR, "Error: signal %d:\n", sig);
  fnames = backtrace_symbols(array, size);

  for (int t=0; t< size; t++) { 
    syslog(LOG_ERR, "%s\n", fnames[t]);
  } 

  syslog(LOG_ERR, "input string : \"%s\"\n", lastclient->msg.c_str());
  syslog(LOG_ERR, "------------- end report \n");

  exit(1);
}

int Service::Del(Client *c)
{
	int pos=0;
	for (vector<Client *>::iterator it = this->clts.begin();
		it != this->clts.end(); it++) 
	{
		if (*it == c) break;
		pos++;
	}

	this->clts.erase(this->clts.begin()+pos);
	return this->clts.size();
}

vector<Client *> Service::getClients(void)
{
	return this->clts;
}

Service::Service(int port, int type) {
	port=port;
	type=type;

	this->sock = new Socket(SOCK_STREAM);
	this->sock->reUse();
	this->sock->setHostByName("localhost");
	this->sock->Bind(port);

	if (type == SOCK_DGRAM) return;

	if (sock->Listen() ) {
		perror("Listen");
	} 
}

Service::~Service() {
	delete this->sock;
}

Socket *Service::getSocket(void)
{
	return sock;
}

void Service::setServer(KloptServer *srv)
{
	this->srv =srv;
}

int Service::Add(Client *c)
{
	this->clts.push_back(c);
	return this->clts.size();
}


Client::Client (Socket *sock) {
	this->sock=sock;
	this->state = -1;

} 

Client::~Client() {
	if (this->sock) delete this->sock;
	this->sock=NULL;
} 

Socket *Client::getSocket(void)
{
	return this->sock;
}

void Client::setState(int state)
{
	this->state=state;
}

void Client::setService(Service *srvc)
{
	this->service=srvc;
}

int Client::Handle(void) {
	return service->Handle(this);
} 

// delete self from service
int Client::Del(void) {
	return this->service->Del(this);
} 


KloptServer::KloptServer(int backlog)
{
	backlog=backlog;
}

void KloptServer::Add(Service *s) {
	services.push_back(s);
	s->setServer(this);
} 

Client *KloptServer::Wait()
{
	int t,u, res,max=0;
    fd_set read_fd;
  	fd_set err_fd;
   	Client *cpp;
   	Client *nc;
   	Service *spp;
   	Socket *ns;
   	int sock;

	FD_ZERO(&err_fd);
   	FD_ZERO(&read_fd);

	// count highest socket fd (for select)
	for (vector<Service *>::iterator it = services.begin();
		it != services.end(); it++) {
		spp = (Service *)*it;
		ns = spp->getSocket();
		sock = ns->getfd();
		if (sock > max) max = sock;
		FD_SET(sock, &err_fd);
       	FD_SET(sock, &read_fd);
		//printf("Clients : %ld\n", spp->clts.size());
		for (vector<Client *>::iterator cit = spp->clts.begin();
			cit != spp->clts.end(); cit++) {
			cpp = (Client *) *cit;
			ns = cpp->getSocket();
			sock = ns->getfd();
			if (sock > max) max = sock;
           	FD_SET(sock, &err_fd);
           	FD_SET(sock, &read_fd);
		} 
	} 

again: // here comes a loop
	//printf("waiting %d\n", max);
	res = select(max + 1, &read_fd, NULL, &err_fd, NULL);
	//printf("waited %d %d %d\n", res, FD_ISSET(sock, &read_fd), FD_ISSET(sock, &err_fd));

	if (res < 0) {
		//printf("retrying with res is %d, err_fd = %d\n", res, err_fd);
		// this is probably one of the child signals, just retry
		goto again;
	}

	for (vector<Service *>::iterator it = services.begin();
		it != services.end(); it++) {
		spp = (Service *)*it;
		ns = spp->getSocket();
		sock = ns->getfd();
		if  (FD_ISSET(sock, &err_fd)) {
			printf("heel jammer\n");
		}
		if  (FD_ISSET(sock, &read_fd)) {
			ns = ns->Accept();
			if (!ns) {
				perror("accept");
				return NULL;
			} else {
				int c=0;
				nc =  new Client(ns);
				nc->setState(CLT_INIT);
				//if (spp->sock->type == SOCK_STREAM)
				c = spp->Add(nc);
				//printf("a: now %d clients \n", c);
				nc->setService(spp);
				return nc;
			}
		}

		for (vector<Client *>::iterator cit = spp->clts.begin();
			cit != spp->clts.end(); cit++) {
			cpp = (Client *) *cit;
			cpp->setState(CLT_RUNNING);
			ns = cpp->getSocket();
			sock = ns->getfd();
			if  (FD_ISSET(sock, &read_fd)) {
				return cpp;
			}
			if  (FD_ISSET(sock, &err_fd)) {
				printf("jammer");
			}
		}
	}
	printf("fell through!\n");
	return NULL;
}

int KloptServer::Handle(Client *c)
{
	intptr_t ret;
	int count;

    lastclient = c;
	ret = c->Handle();

	if (ret == CS_CONT) return 0;

	count = c->Del();
	delete c;

	if (ret == CS_DONE) {
        //printf("[SRV]: ------ waiting for input\n");
		//printf("del : now %d clients left\n", count);
		return 0;
	}

	return 1; // CS_STOP remains : signal server stop 
}

int KloptServer::Loop(void)
{
	int stop=0;
	while(!stop) { 
		Client *c = Wait();
		stop = Handle(c);
		//stop = c->Handle();
	} 

	return stop;
}

class TestServer : public KloptServer
{
public:
	TestServer(int backlog) : KloptServer(backlog) {
	}
};

class HelloService : public Service
{
public:
	HelloService(int port, int type) : Service(port, type)
	{
	} 

	intptr_t Handle(Client * c)
	{
		printf("HelloService\n");
		return 0;
	}
};

int main(int argc, char *argv[])
{
    signal(SIGSEGV, handler);   // install our handler

	TestServer ts(5);

    openlog("klopt.SRV", 0,LOG_NOTICE);
    syslog(LOG_NOTICE, "Service started");

	//CommandService s(8811,SOCK_STREAM);
	JsonService j(SOCK_SRV,SOCK_STREAM);

	//ts.Add(&s);
	ts.Add(&j);
	ts.Loop();

    u_cleanup();

	return 0;
}
