//#pragma execution_character_set("utf-8")

#include <boost/locale.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <iostream>

int main()
{
    std::locale loc = boost::locale::generator().generate("");
    std::locale::global(loc);

	std::wstring grus = L"grütßen";
	//std::wstring grus = L"Amélie";
	std::wstring conv;
	std::wstring fold;

    std::cout << "grüßen vs " << std::endl;
    //std::wcout << boost::locale::to_upper(grus) << std::endl;
    //std::wcout << boost::locale::fold_case(grus) << std::endl;
    //std::wcout << boost::locale::normalize(grus, boost::locale::norm_nfd) << std::endl;

    fold = boost::locale::fold_case(grus);
	conv = boost::locale::normalize(grus, boost::locale::norm_nfd);

	std::wstring low = boost::locale::to_lower(conv);

	std::wcout << conv << std::endl;
	std::wcout << fold << std::endl;

    return 0;
}
