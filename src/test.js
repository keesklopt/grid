#!/usr/bin/nodejs

var net = require('net');
var fs = require('fs');

var HOST = '127.0.0.1';
var PORT = 8812;

var client = new net.Socket();
var args = process.argv.slice(0);

console.log(args.length)

if (args.length < 3) {
    console.log("Usage : " + args[0] + " input file");
    process.exit(1);
} 

    client.connect(PORT, HOST, function() {

        console.log('CONNECTED TO: ' + HOST + ':' + PORT);

        fs.readFile(args[2], 'utf8', function (err,data) {
            if (err) {
                return console.log(err);
            }
            console.log(data);
            client.write(data );
        });
    });

client.on('data', function(data) {
    
    console.log('DATA: ' + data);
    client.destroy();
    
});

client.on('close', function() {
    console.log('Connection closed');
});

client.on('error', function(err){
    // handle the error safely
    console.log(err)
})

