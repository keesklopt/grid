package org.klopt.trace;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;

import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.klopt.trace.R;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import android.view.View.OnClickListener;
import android.util.Log;


class PolylineDecoder {
	/**
	 * Transform a encoded PolyLine to a Array of GeoPoints.
	 * Java implementation of the original Google JS code.
	 * @see Original encoding part: <a href="http://code.google.com/apis/maps/documentation/polylinealgorithm.html">http://code.google.com/apis/maps/documentation/polylinealgorithm.html</a>
	 * @return Array of all GeoPoints decoded from the PolyLine-String.
	 * @param encoded_points String containing the encoded PolyLine. 
	 * @param countExpected Number of points that are encoded in the PolyLine. Easiest way is to use the length of the ZoomLevels-String. 
	 * @throws DecodingException 
	 */public static List <GeoPoint> decodePoints(String encoded_points){
		 int index = 0;
		 int lat = 0;
		 int lng = 0;
		 List <GeoPoint> out = new ArrayList<GeoPoint>();

		 try {
		     int shift;
		     int result;
		     while (index < encoded_points.length()) {
		         shift = 0;
		         result = 0;
		         while (true) {
		             int b = encoded_points.charAt(index++) - '?';
		             result |= ((b & 31) << shift);
		             shift += 5;
		             if (b < 32)
		                 break;
		         }
		         lat += ((result & 1) != 0 ? ~(result >> 1) : result >> 1);

		         shift = 0;
		         result = 0;
		         while (true) {
		             int b = encoded_points.charAt(index++) - '?';
		             result |= ((b & 31) << shift);
		             shift += 5;
		             if (b < 32)
		                 break;
		         }
		         lng += ((result & 1) != 0 ? ~(result >> 1) : result >> 1);
		         /* Add the new Lat/Lng to the Array. */
		         out.add(new GeoPoint((lat*10),(lng*10)));
		     }
		     return out;
		 }catch(Exception e) {
		     e.printStackTrace();
		 }
		 return out;
	 }
	

	public static int[] decodeZoomLevels(String encodedZoomLevels){
	    int[] out = new int[encodedZoomLevels.length()];
	    int index = 0;

	    for(char c : encodedZoomLevels.toCharArray())
	        out[index++] = c - '?';
	    return out;

	}
}

public class MainActivity extends FragmentActivity implements LocationListener {

	GoogleMap googleMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button buttonStart = (Button)findViewById(R.id.button1);
		buttonStart.setOnClickListener(startListener);
		
		// Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        // Showing status
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available
 
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
 
        }else { // Google Play Services are available
 
            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
 
            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();
 
            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);
 
            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
 
            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();
 
            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);
 
            // Getting Current Location
            Location location = locationManager.getLastKnownLocation(provider);
 
            if(location!=null){
                onLocationChanged(location);
            }
            locationManager.requestLocationUpdates(provider, 20000, 0, this);
        }
      
	}
	
	private static String logtag = "Trace";//for use as the tag when logging 

	private OnClickListener startListener = new OnClickListener() {
        public void onClick(View v) {
            Log.d(logtag,"onClick() called - send request");
            
            Toast.makeText(MainActivity.this, "Location report was sent.", Toast.LENGTH_LONG).show();
            try {
				doRequest();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            Log.d(logtag,"onClick() ended - sent request");
          }
    };
    
    private static JSONObject getJsonObjectFromMap(Map params) throws JSONException {

        //all the passed parameters from the post request
        //iterator used to loop through all the parameters
        //passed in the post request
        Iterator iter = params.entrySet().iterator();

        //Stores JSON
        JSONObject holder = new JSONObject();

        //using the earlier example your first entry would get email
        //and the inner while would get the value which would be 'foo@bar.com' 
        //{ fan: { email : 'foo@bar.com' } }

        //While there is another entry
        while (iter.hasNext()) 
        {
            //gets an entry in the params
            Map.Entry pairs = (Map.Entry)iter.next();

            //creates a key for Map
            String key = (String)pairs.getKey();

            //Create a new map
            Map m = (Map)pairs.getValue();   

            //object for storing Json
            JSONObject data = new JSONObject();

            //gets the value
            Iterator iter2 = m.entrySet().iterator();
            while (iter2.hasNext()) 
            {
                Map.Entry pairs2 = (Map.Entry)iter2.next();
                data.put((String)pairs2.getKey(), (String)pairs2.getValue());
            }

            //puts email and 'foo@bar.com'  together in map
            holder.put(key, data);
        }
        return holder;
    }
    
    public void doRequest() throws Exception{
        String path = "http://klopt.org:1900/ws";
        HashMap  params = new HashMap();

        //params.put(new String("jsonrpc"), new String("{\"jsonrpc\":\"2.0\",\"method\":\"route\",\"id\":33,\"params\":{\"options\":{\"algorithm\":\"osrm\",\"timings\":true,\"paths\":true},\"locations\":[52.21446,4.61973,51.9889,5.62198]}}")); 

        String sreq = new String("jsonrpc={\"jsonrpc\":\"2.0\",\"method\":\"route\",\"id\":33,\"params\":{\"options\":{\"algorithm\":\"osrm\",\"timings\":true,\"paths\":true},\"locations\":[52.21446,4.61973,51.9889,5.62198]}}"); 
        String r=null;
        try {
           r = makeRequest(path, sreq);
           Log.d(logtag,r);
       } catch (Exception e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
        
       JSONObject jo = new JSONObject(r);
       JSONArray result = jo.getJSONArray("result");
       JSONObject jo2 = result.getJSONObject(0);
       String encoded = jo2.getString("path");
       
       Log.i(logtag,encoded);
       
       List<GeoPoint> pts = PolylineDecoder.decodePoints(encoded);
       Iterator it=pts.iterator();
   
       while (it.hasNext()) {
    	   GeoPoint entry = (GeoPoint) it.next();
    	   Log.d(logtag, entry.toString());
       }
    }
    
    public static String makeRequest(String path, String params) throws Exception 
    {
        //instantiates httpclient to make request
        DefaultHttpClient httpclient = new DefaultHttpClient();

        //url with the post data
        HttpPost httpost = new HttpPost(path);

            //passes the results to a string builder/entity
        StringEntity se = new StringEntity(params);

        //sets the post request as the resulting string
        httpost.setEntity(se);
        //sets a request header so the page receving the request
        //will know what to do with it
        httpost.setHeader("Accept", "application/json");
        httpost.setHeader("Content-type", "application/json");

        //Handles what is returned from the page 
        ResponseHandler responseHandler = new BasicResponseHandler();
        String rs = httpclient.execute(httpost, responseHandler);
       
        return rs;
    }
    
  
    public static List <GeoPoint> decodePoints(String encoded_points){
    	int index = 0;
    	int lat = 0;
    	int lng = 0;
    	
    	List <GeoPoint> out = new ArrayList<GeoPoint>();

    	try {
    	    int shift;
    	    int result;
    	    while (index < encoded_points.length()) {
    	        shift = 0;
    	        result = 0;
    	        while (true) {
    	            int b = encoded_points.charAt(index++) - '?';
    	            result |= ((b & 31) << shift);
    	            shift += 5;
    	            if (b < 32)
    	                break;
    	        }
    	        lat += ((result & 1) != 0 ? ~(result >> 1) : result >> 1);

    	        shift = 0;
    	        result = 0;
    	        while (true) {
    	            int b = encoded_points.charAt(index++) - '?';
    	            result |= ((b & 31) << shift);
    	            shift += 5;
    	            if (b < 32)
    	                break;
    	        }
    	        lng += ((result & 1) != 0 ? ~(result >> 1) : result >> 1);
    	        /* Add the new Lat/Lng to the Array. */
    	        out.add(new GeoPoint((lat*10),(lng*10)));
    	    }
    	    return out;
    	}catch(Exception e) {
    	    e.printStackTrace();
    	}
    	return out;
    }
    
	public void onLocationChanged(Location location) {
		  
	        TextView tvLocation = (TextView) findViewById(R.id.tv_location);
	 
	        // Getting latitude of the current location
	        double latitude = location.getLatitude();
	 
	        // Getting longitude of the current location
	        double longitude = location.getLongitude();
	 
	        // Creating a LatLng object for the current location
	        LatLng latLng = new LatLng(latitude, longitude);
	 
	        // Showing the current location in Google Map
	        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
	 
	        // Zoom in the Google Map
	        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
	 
	        // Setting latitude and longitude in the TextView tv_location
	        tvLocation.setText("Latitude:" +  latitude  + ", Longitude:"+ longitude );
	 
	}
	 
	@Override
	public void onProviderDisabled(String provider) {
	    // TODO Auto-generated method stub
	}
	 
	@Override
	public void onProviderEnabled(String provider) {
	    // TODO Auto-generated method stub
	}
	 
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	    // TODO Auto-generated method stub
	}
	 
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
