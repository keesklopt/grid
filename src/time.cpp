#include <time.h>
#include <string.h>
#include <stdio.h>
#include <app.h>

char *micro_fmt(micro_t mt)
{
	struct tm *Tm;
	char *buf;
	time_t t,rest;
	const char *sign="+";

	if (mt < 0) {
		sign="-";
		mt *=-1;
	}
	t= mt / 1000000LL;
	rest = mt % 1000000LL;

	/* printf("da was %d\n", t); */
	Tm = localtime(&t);

	buf = (char *)calloc(1,100);
	snprintf(buf, 100, "%s%04d/%02d/%02d %02d:%02d:%02d.%06d", sign, Tm->tm_year+1900, Tm->tm_mon+1, Tm->tm_mday, Tm->tm_hour, Tm->tm_min, Tm->tm_sec, (int)rest);

	return buf;
}

void micro_dmp(micro_t mt)
{
	char *tmp=  micro_fmt(mt);
	printf("%s\n", tmp);
	free(tmp);
}

micro_t micro_now(void)
{
	micro_t m;
	struct timeval tv;
	gettimeofday(&tv,NULL);
	m = (long long)tv.tv_sec ;
	m *= 1000000;
	m += tv.tv_usec;
	return m;
}

struct timeval micro_timeval(micro_t mt)
{
	struct timeval tv;
    (tv).tv_sec = (int) (mt / 1000000LL);
    (tv).tv_usec = (int) (mt % 1000000LL);
	return tv;
}

/* some strftime functions don;t support %S etc
	so just do it manually */
char *hms_fmt(time_t t)
{
	char *buf = (char *)calloc(1,9);
	struct tm *Tm;

	Tm = localtime(&t);

	if (Tm)
		snprintf(buf,9,"%02d:%02d:%02d", Tm->tm_hour, Tm->tm_min, Tm->tm_sec);
	else 
		snprintf(buf,9,"--:--:--");

	return buf;
}

char *hour_fmt(time_t t)
{
	char *buf = (char *)calloc(1,7);
	struct tm *Tm;

	Tm = localtime(&t);
	if (Tm)
		strftime(buf,6,"%H:%M", Tm);
	else 
		snprintf(buf,6,"--:--");

	return buf;
}

char *day_fmt(time_t t)
{
	char *buf = (char *)calloc(1,28);
	struct tm *Tm;

	Tm = localtime(&t);
	strftime(buf,8,"%a %d %b", Tm);
	//strftime(buf,28,"%F %a %d %b", Tm);
	
	return buf;
}

