#include <app.h>
// a context is just an umbrella for sets of orders, 
// see it as a 'company', it can hold ordersets 


Context::Context(string name)
{
	this->id=name;
}

void Context::clearorderset(string osname)
{
	this->ordersets.erase(osname);
}

OrderSet *Context::addorderset(string osname)
{
	OrderSet *os = new OrderSet(osname);
	clearorderset(osname);
	this->ordersets[osname] = os;

	return os;
}

OrderSet *Context::getorderset(string oset)
{
	return ordersets[oset];
}
