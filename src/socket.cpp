#include <app.h>

static socklen_t socklen = sizeof(struct sockaddr_in);

Socket::Socket (int type)
{
	int proto;
	family = AF_INET;
	type = type;

	if (type == SOCK_DGRAM) proto = IPPROTO_UDP;
	if (type == SOCK_STREAM) proto = IPPROTO_TCP;

	fd = socket(family,type, proto);
	/* fcntl(fd,F_SETFD,FD_CLOEXEC); */
	/* fcntl(sp->fd,F_SETFD,0); */
	if (fd < 1) {
		perror("socket");
		exit(0);
	} 
}

/* for after accept, we want a socket structure around
 the accepted socket */
Socket::Socket (int type, int fd)
{
	int proto;
	family = AF_INET;
	type = type;

	if (type == SOCK_DGRAM) proto = IPPROTO_UDP;
	if (type == SOCK_STREAM) proto = IPPROTO_TCP;

	this->fd = fd;
}

Socket::~Socket() {
    //printf("-------- close %d\n", fd);
	close(fd);
} 

string Socket::getIp(struct sockaddr_in sa4)
{
    char *ip = (char *)calloc(1,100);
    string ret;
    
    inet_ntop(AF_INET, &sa4.sin_addr.s_addr, ip,100);
    //cout << "New Client " << ip << endl;

    ret = string(ip);
    free(ip);
    return ret;
}

bool Socket::Send(string s)
{
    int fd = this->fd;
    const char *str = s.c_str();
    int len = s.size()+1;
    
    //printf("### Send : %d %d: %s\n", fd, len, str);

    //printf("[%d][%d][%d]\n", str[len-2], str[len-1], str[len-0]);
    if (send(fd, str, len, 0) < 0) { 
        return false;
    } 

    return true;
}

std::string Socket::Recv(void)
{
    string s="";
    int fd = this->fd;
    int len=0;
    int r;

    char buf[READ_LEN+1]={0};

    // we need a timeout to prevent a complete server hang on this recv

    struct timeval tv;

    tv.tv_sec = REQ_TIMEOUT;  /* 3 Secs Timeout */
    tv.tv_usec = 0;  // Not init'ing this can cause strange errors
    
    // just NEVER hang the server, rather loose a request
    // this will happen if the input is not closed with '\0'
    // try to provide vis ws.wsgi to make sure it does
    setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));
    
    // wait for a short read, or possibly we have to detect the '\0'
    while ((r = recv(fd, buf, READ_LEN, 0)) == READ_LEN && buf[r-1] != '\0') { 
        //printf("recving %d is %s\n", r, buf);
        s+= buf;    
        len += r;
        memset(buf, '\0', READ_LEN+1);
    } 
    s+= buf;    
    len += r;
    s+= '\0';
    //printf("### Recv : %d %d: %s\n", fd, len, s);
	//printf ("Done\n");

    return s;
}

string Socket::getPeerIp(void)
{
    return getIp(this->peer.sa4);
}

string Socket::getAddrIp(void)
{
    return getIp(this->addr.sa4);
}

int Socket::setHostByName(const char *name)
{
    struct hostent *hep;
	char myname[MAXHOSTNAMELEN];

	if (name == NULL) {
    	gethostname(myname, MAXHOSTNAMELEN);
    	name = myname;
	}

	if (name)
    	hep = gethostbyname(name);
	else {
    	this->addr.sa4.sin_addr.s_addr = INADDR_ANY;
    	goto wrapup;
	}

	if (!hep) return -1;

	this->addr.sa4.sin_addr.s_addr = *(int *)hep->h_addr_list[0];
wrapup:
	this->addr.sa4.sin_port = (int)htons(port);
	return 0;
}

int Socket::reUse(void) {
	int yes=1;

	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (void *)&yes, sizeof(int)) == -1) {
    	return -1;
	}
	return 0;
} 

int Socket::Listen(void) {
	return listen(fd,10);
} 

int Socket::Bind(short port)
{
    int ret;
    struct sockaddr a;
	struct sockaddr_in *sa4;
#ifdef HAVEIPV6
	struct sockaddr6_in *sa6;
#endif
	unsigned int namelen = sizeof(a);
   	 
	this->addr.sa4.sin_addr.s_addr = INADDR_ANY;
	this->addr.sa4.sin_port = (int)htons(port);

	this->addr.sa4.sin_family = this->family;
	this->addr.sa4.sin_port = htons(port);
	ret = bind(this->fd, (struct sockaddr *)&this->addr.sa, socklen);

	if (ret != 0)
	{
    	perror("could not bind socket");
    	return ret;
	}

	/* find out which port  */
	getsockname(this->fd, &a, &namelen);
	this->family = a.sa_family;
	if (this->family == AF_INET) {
    	sa4 = (struct sockaddr_in *)&a;
    	this->port = ntohs(sa4->sin_port);
	}
#ifdef HAVEIPV6
	if (this->family == AF_INET6) {
    	sa6 = (struct sockaddr_in *)a;
    	this->port = sa6.sin_port;
	}
#endif

	/* this->port = ntohs(a.sa4.sin_port); */
	return 0;

}

Socket *Socket::Dup(int fd)
{
	Socket *s = new Socket(SOCK_STREAM,fd);
	memcpy(s,this,sizeof(Socket));
	s->fd = fd;
	return s;
}

Socket *Socket::Accept(void)
{
	Socket *np;

	int fd = accept(this->fd,&this->peer.sa,&socklen);
	np = this->Dup(fd);

	return np;
}

int Socket::Connect(struct sockaddr *peer)
{
	if ( connect(this->fd, peer,socklen)) { 
        //printf("Could not connect\n");
        close(this->fd);
        return -1;
    } 

	return 0;
}

int Socket::getfd(void) {
	return fd;
} 


