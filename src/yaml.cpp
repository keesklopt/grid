#include <app.h>
#include <yaml.h>
#include <iostream>
#include <fstream>

#include <syslog.h>

Yaml::Yaml(std::string fname) {
    readFile(fname);
} 

Yaml::~Yaml(void) {
	/* Cleanup */
  	yaml_parser_delete(&parser);
  	fclose(fh);
} 

yaml_token_t Yaml::getProperty(char *key)  {

	yaml_token_t  token;   /* new variable */

	do {
    yaml_parser_scan(&parser, &token);
    switch(token.type)
    {
    /* Stream start/end */
    case YAML_STREAM_START_TOKEN: printf("STREAM START"); break;
    case YAML_STREAM_END_TOKEN:   printf("STREAM END");   break;
    /* Token types (read before actual token) */
    case YAML_KEY_TOKEN:   printf("(Key token)   "); break;
    case YAML_VALUE_TOKEN: printf("(Value token) "); break;
    /* Block delimeters */
    case YAML_BLOCK_SEQUENCE_START_TOKEN: printf("<b>Start Block (Sequence)</b>"); break;
    case YAML_BLOCK_ENTRY_TOKEN:          printf("<b>Start Block (Entry)</b>");    break;
    case YAML_BLOCK_END_TOKEN:            printf("<b>End block</b>");              break;
    /* Data */
    case YAML_BLOCK_MAPPING_START_TOKEN:  printf("[Block mapping]");            break;
    case YAML_SCALAR_TOKEN:  printf("scalar %s \n", token.data.scalar.value); break;
    /* Others */
    default:
      printf("Got token of type %d\n", token.type);
    }
    if(token.type != YAML_STREAM_END_TOKEN)
      yaml_token_delete(&token);
  } while(token.type != YAML_STREAM_END_TOKEN);

    return token;
}


string Yaml::getStringProperty(char *key)  {
    json_object *o;
    string s="";
    yaml_token_t token = getProperty(key);

  yaml_token_delete(&token);
    return "";
} 

int Yaml::getIntProperty(char *key)  {
    return 0;
} 

int Yaml::readFile(std::string name) { 
	fh = fopen(name.c_str(), "r");
	
	if(!yaml_parser_initialize(&parser)) { 
    	fputs("Failed to initialize parser!\n", stderr);
        exit(-1);
    } 
  	if(fh == NULL) { 
    	fputs("Failed to open file!\n", stderr);
        exit(-1);
    } 

	/* Set input file */
  	yaml_parser_set_input_file(&parser, fh);

    return 1;
} 
