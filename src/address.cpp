#include <stdio.h>
#include <app.h>

int init_address(char *name)
{
	FILE *fp = fopen(name,"r");

	if (!fp) return -1;

#define MAXLINE 4096

	int r=0;
	char line[MAXLINE];
	char *ptr;

	while ( (ptr= fgets(&line[0],MAXLINE,fp)) != NULL)
	{
		r++;
	}

	printf("%d records\n", r);

	return 0;
}

