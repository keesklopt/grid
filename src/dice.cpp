#include <app.h>
#include <float.h>
#include <math.h>

prox_t DiceNearest(coord_t find, int *dist)
{
	prox_t found= {1.1,2.2};
	return found;
}

prox_t DiceProximity(coord_t find, int *dist)
{
	prox_t found= {1.1,2.2};
	return found;
}

void DiceInit(char *network)
{
	np = net_open((char *)"../gen", (char *)network);
}

Mat<int32_t> *DiceMatrix(int ndeps, coord_t *deps, int narrs, coord_t *arrs)
{
	double result;
	int x,y;
	int d;

	Mat<int32_t> *mp = new Mat<int32_t>(ndeps,narrs);

	for(y=0; y< ndeps; y++) {
		tilenode_t *from = net_get_node_by_ll(np,(int32_t)(deps[y].x*10000000.0), (int32_t)(deps[y].y*10000000.0),&d);
		//net_edsger(np,from,&node_dontmatch);
		for(x=0; x< narrs; x++) {
			tilenode_t *to = net_get_node_by_ll(np,(int32_t)arrs[x].x*10000000, (int32_t)arrs[x].y*10000000,&d);
			//result = net_meters(np,to);
			//mat_set(mp,y,x,(int32_t)round(result));
		}
	}

	return mp;
}


double Dice(double x1,double y1, double x2, double y2)
{
	int d;

	tilenode_t *from = net_get_node_by_ll(np,(int32_t)x1*10000000, (int32_t)y1*10000000,&d);
	tilenode_t *to = net_get_node_by_ll(np,(int32_t)x2*10000000, (int32_t)y2*10000000,&d);


	return 1.1;
}
