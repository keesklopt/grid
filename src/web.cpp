		extern "C" {
#include "fcgi_stdio.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <curl/curl.h>

}
#include "app.h"

using namespace std;

//#include "mongo/client/dbclient.h"

//#define NETWORK "/projects/gen/nl.osrm"
//#define NETWORK "/projects/gen/l6.osrm"
//#define NETWORK "/projects/gen/e1.osrm"
//#define NETWORK "/usr/local/share/klopt/luxembourg.osrm"
//#define NETWORK "/usr/local/share/klopt/netherlands.osrm"
//#define NETWORK "/usr/local/share/klopt/eur.osrm"
//#define NETWORK "/oldhome/kees/projects/3pty/osrm/europe-latest.osrm"
//#define NETWORK "/home/kees/projects/grid/3pty/osrm/netherlands-latest.osrm"
//#define NETWORK "/data/projects/osrm/europe-latest.osrm"
#define NETWORK "/projects/klopt/3pty/netherlands-latest.osrm"
//#define NETWORK "/projects/klopt/3pty/europe-latest.osrm"

//#define NETWORK "/projects/klopt/eurmain.osrm"
//#define NETWORK "/projects/klopt/europe.osrm"
//IO *io = new IO();

//#include <mysql/mysql.h>

#define DEFAULTNET "../dat"

//#define MAX_MESSAGE_LENGTH 4096
#define BUFSIZE 4096
#define goog_url "/maps/api/geocode/json?sensor=false"

// tweaking :
// normally : all should be #undef 
#undef LEAVE_DB_OPEN 
#define MEMTESTING   

//MYSQL *gconn = NULL;
net_t *np=NULL;

typedef struct work_tag
{
	int t;
	int d;
	int c;
} work_t;

void to_urlsafe(char *orig){
	while (*orig) {
		if (*orig == '+') *orig='-';
		if (*orig == '/') *orig='_';
		orig++;
	}
}

void from_urlsafe(char *orig){
	while (*orig) {
		if (*orig == '-') *orig='+';
		if (*orig == '_') *orig='/';
		orig++;
	}
}

void web_error(int id, int num, const char *msg)
{
	char *str;
	//json_t *out = json_object();
	//json_t *cod = json_int(num);
	//json_t *mes = json_string(msg);

	json_t *out;
	json_t *err;

	//json_object_add(res, "code", cod);
	//json_object_add(res, "message", mes);
	//json_object_add(out, "error", res);
	//json_object_add(out, "result", json_null);
	//json_object_add(out, "id", json_null);

	err = json_rpc_err_ist(num, msg);
	out = json_rpc_rcv_ist(json_int(id), NULL, err);

	str = json_stringify(out);
	printf("%s", str);

	if (str) free(str);
	if (out) json_rls(out);
}

void init(string network)
{
	// do one time init here 
	//mysql_library_init(0,NULL,NULL);
#ifdef USE_GOSMORE
	int r = GosmoreInit("/home/kees/gosmore.pak");
#endif
	OsrmInit((char *)network.c_str());
	//DiceInit((char *)DEFAULTNET);
#ifdef LEAVE_DB_OPEN
#error No
#else
	//gconn = db_init();
	//mconn = mongo_connect ("localhost", 27017);
	//printf("Mongo connection %p\n", mconn);
#endif
	//read_matrix();
}

void web_get(char *qry)
{
	printf("only rest supported right now\n");
	return;
}

void handle_nearest(json_t *content, int id)
{
	static char erbuf[2048];
	static char buf[40960];
	json_t *options=NULL;
	json_t *params=NULL;
	json_t *coordinates=NULL;
	json_t *depx=NULL;
	json_t *depy=NULL;
	json_t *arrx=NULL;
	json_t *arry=NULL;
	json_t *algorithm=NULL;
	json_t *timings=NULL;
	int dotimes=0; // default
	int x, y;
	micro_t starttime;
	micro_t enddtime;

	prox_t (*fnc) (coord_t, int *len) = NULL;

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		return;
	}
	options = json_object_get(params, "options");
	if (options) {
		algorithm = json_object_get(options, "algorithm");
		if (algorithm && algorithm->val.s) {
			//if (strcmp(algorithm->val.s,"haversine")==0) 
				//fnc=HaversineMatrix;
			//if (strcmp(algorithm->val.s,"google")==0) 
				//fnc=GoogleMatrix;
			if (strcmp(algorithm->val.s,"osrm")==0) 
				fnc=OsrmNearest;
			//if (strcmp(algorithm->val.s,"klopt")==0) 
				//fnc=DiceNearest;
			//if (strcmp(algorithm->val.s,"dice")==0) 
				//fnc=DiceNearest;
		}
		if (!fnc) {
			sprintf(erbuf, "algorithm %s unknown", algorithm->val.s);
			web_error(id, -15,erbuf);
			return;
		}
		timings = json_object_get(options, "timings");
		if (timings) {
			if (timings->val.b) dotimes=1;
		} 
	}
	coordinates = json_object_get(params, "coordinates");
	if (!coordinates) {
		sprintf(erbuf, "nearest function needs coordinates");
		web_error(id, -14,erbuf);
		return;
	}


	printf("{\"jsonrpc\": \"2.0\",");
	if (id) {
		printf("\"id\": %d,", id);
	}
	printf("\"result\": ");
	int cols = json_array_len(coordinates)/2;
	double lat=0.0,latfound;
	double lon=0.0,lonfound;
	int len=0;
	starttime = micro_now();
	printf("[");
	for (x=0; x< cols; x++) {
		if (x>0) printf(",");
		depy = json_array_get(coordinates, (x*2));
		if (depy) lat = depy->val.d;
		depx = json_array_get(coordinates, (x*2)+1);
		if (depx) lon = depx->val.d;

		coord_t match;
		match.lat=lat;
		match.lon=lon;
		prox_t result = fnc(match,&len);

		if (result.name == NULL) {
			result.name = (char *)("null");
		} 
		printf("{\"lat\":%f,\"lon\":%f,\"meters\":%d,\"name\":\"%s\"}", result.lat, result.lon, len/1000, result.name);
	}
	enddtime = micro_now();

	printf("]");
	if (dotimes) {
		printf(",\"timings\":{\"nearest\":%d}", enddtime-starttime);
	} 
	printf("}");

	// do NOT !! it is freed with content
	//if (coordinates) free(coordinates);
}

/* LATER, proximity should be finding POI near to a center */
void handle_proximity(json_t *content, int id)
{
	static char erbuf[2048];
	static char buf[40960];
	json_t *options=NULL;
	json_t *params=NULL;
	json_t *coordinates=NULL;
	json_t *depx=NULL;
	json_t *depy=NULL;
	json_t *arrx=NULL;
	json_t *arry=NULL;
	json_t *algorithm=NULL;
	json_t *timings=NULL;
	int dotimes=0; // default
	micro_t starttime;
	micro_t enddtime;
	int x, y;

	// FOR NOW ::
	sprintf(erbuf, "proximity not implemented yet (did you mean nearest ?, see http://code.google.com/p/klopt/wiki/interface)");
	web_error(id, -10,erbuf);
	return;

	prox_t (*fnc) (coord_t, int *len) = NULL;

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		return;
	}
	options = json_object_get(params, "options");
	if (options) {
		algorithm = json_object_get(options, "algorithm");
		if (algorithm && algorithm->val.s) {
			//if (strcmp(algorithm->val.s,"haversine")==0) 
				//fnc=HaversineMatrix;
			//if (strcmp(algorithm->val.s,"google")==0) 
				//fnc=GoogleMatrix;
			if (strcmp(algorithm->val.s,"osrm")==0) 
				fnc=OsrmProximity;
			if (strcmp(algorithm->val.s,"klopt")==0) 
				fnc=DiceProximity;
			if (strcmp(algorithm->val.s,"dice")==0) 
				fnc=DiceProximity;
		}
		timings = json_object_get(options, "timings");
		if (timings) {
			if (timings->val.b) dotimes=1;
		} 
	}
	coordinates = json_object_get(params, "coordinates");
	if (!coordinates) {
		sprintf(erbuf, "proximity function needs coordinates");
		web_error(id, -14,erbuf);
		return;
	}

	printf("{\"jsonrpc\": \"2.0\",");
	if (id) {
		printf("\"id\": %d,", id);
	}
	printf("\"result\": ");
	int cols = json_array_len(coordinates)/2;
	double lat=0.0,latfound;
	double lon=0.0,lonfound;
	int len=0;
	starttime = micro_now();
	printf("[");
	for (x=0; x< cols; x++) {
		if (x>0) printf(",");
		depy = json_array_get(coordinates, (x*2));
		if (depy) lat = depy->val.d;
		depx = json_array_get(coordinates, (x*2)+1);
		if (depx) lon = depx->val.d;

		coord_t match;
		match.lat=lat;
		match.lon=lon;
		prox_t result = fnc(match,&len);

		if (result.name == NULL) {
			result.name = (char *)("null");
		} 
		printf("{\"lat\":%f,\"lon\":%f,\"meters\":%d,\"name\":\"%s\"}", result.lat, result.lon, len/1000, result.name);
	}
	enddtime = micro_now();
	printf("]");
	if (dotimes) {
		printf(",\"timings\":{\"proximity\":%d}", enddtime-starttime);
	} 
	printf("}");

	// do NOT !! it is freed with content
	//if (coordinates) free(coordinates);
}

void handle_geocode(json_t *content, int id)
{
	static char erbuf[2048];
	static char buf[40960];
	json_t *options=NULL;
	json_t *params=NULL;
	json_t *algorithm=NULL;
	json_t *zip_json=NULL;
	json_t *street_json=NULL;
	json_t *city_json=NULL;
	json_t *country_json=NULL;
	json_t *house_json=NULL;
	json_t *timings=NULL;
	json_t *address_array=NULL;
	json_t *address_json=NULL;
	int dotimes=0; // default
	micro_t starttime;
	micro_t enddtime;
	char *zip = NULL;
	char *street = NULL;
	char *house = NULL;
	char *city = NULL;
	char *country = NULL;
	prox_t *prox;
	int x, y;

	prox_t (*fnc) (char *zip, char *street, char *city, char *house, char *country)=NULL;

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		return;
	}
	options = json_object_get(params, "options");
	if (options) {
		algorithm = json_object_get(options, "algorithm");
		if (algorithm && algorithm->val.s) {
			//if (strcmp(algorithm->val.s,"haversine")==0) 
				//fnc=HaversineMatrix;
			if (strcmp(algorithm->val.s,"google")==0) 
				fnc=GoogleGeocode;
			if (strcmp(algorithm->val.s,"osrm")==0) 
				fnc=OsrmGeocode;
		}
		timings = json_object_get(options, "timings");
		if (timings) {
			if (timings->val.b) dotimes=1;
		} 
	}

	address_array = json_object_get(params, "addresses");

	if (!address_array) {
		sprintf(erbuf, "geocode function needs addresses");
		web_error(id, -14,erbuf);
		return;
	}

	int i;
	int len = json_array_len(address_array);

	printf("{\"jsonrpc\": \"2.0\",");
	if (id) {
		printf("\"id\": %d,\"result\":[", id);
	}

	starttime = micro_now();
	for (i=0; i< len; i++) {

		address_json = json_array_get(address_array,i);
		zip_json = json_object_get(address_json, "zipcode");
		street_json = json_object_get(address_json, "street");
		city_json = json_object_get(address_json, "city");
		country_json = json_object_get(address_json, "country");
		house_json = json_object_get(address_json, "house");

		if (i>0) printf(",");

		if (!zip_json && !street_json && !city_json) {
			printf("{\"lat\":-180,\"lon\":-180}");
			continue;
		}

		if (zip_json) zip = zip_json->val.s;
		if (street_json) street = street_json->val.s;
		if (house_json) house = house_json->val.s;
		if (city_json) city = city_json->val.s;
		if (country_json) country = country_json->val.s;

		prox_t result = fnc(country,city,street,house,zip);

		int field=0;
		printf("{");
		if (result.lat) printf("\"lat\":%f,", result.lat);
		if (result.lon) printf("\"lon\":%f", result.lon);
		printf("}");
	}
	printf("]");
	enddtime = micro_now();
	
	if (dotimes) {
		printf(",\"timings\":{\"geocode\":%d}", enddtime-starttime);
	} 
	printf("}");

	// do NOT !! it is freed with content
	//if (coordinates) free(coordinates);
}

void handle_edocoeg(json_t *content, int id)
{
	static char erbuf[2048];
	static char buf[40960];
	json_t *options=NULL;
	json_t *params=NULL;
	json_t *algorithm=NULL;
	json_t *coord_array=NULL;
	json_t *lat_json=NULL;
	json_t *lng_json=NULL;
	json_t *timings=NULL;
	int dotimes=0; // default
	micro_t starttime;
	micro_t enddtime;
	char *coord = NULL;
	double lat;
	double lng;
	int x, y;

	addr_t (*fnc) (double,double)=NULL;

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		return;
	}
	options = json_object_get(params, "options");
	if (options) {
		algorithm = json_object_get(options, "algorithm");
		if (algorithm && algorithm->val.s) {
			//if (strcmp(algorithm->val.s,"haversine")==0) 
				//fnc=HaversineMatrix;
			if (strcmp(algorithm->val.s,"google")==0) 
				fnc=GoogleEdocoeg;
			if (strcmp(algorithm->val.s,"osrm")==0) 
				fnc=OsrmEdocoeg;
		}
		timings = json_object_get(options, "timings");
		if (timings) {
			if (timings->val.b) dotimes=1;
		} 
	}
	coord_array = json_object_get(params, "coordinates");

	if (!coord_array) {
		sprintf(erbuf, "edocoeg function needs coordinates");
		web_error(id, -14,erbuf);
		return;
	}

	int i;
	int len = json_array_len(coord_array);

	printf("{\"jsonrpc\": \"2.0\",");
	if (id) {
		printf("\"id\": %d,\"result\":[", id);
	}

	starttime = micro_now();
	for (i=0; i< len; i+=2) {
		lat_json = json_array_get(coord_array,i);
		lat = lat_json->val.d;
		lng_json = json_array_get(coord_array,i+1);
		lng = lng_json->val.d;
		addr_t result = fnc(lat,lng);
		int field=0;
		printf("{");
		if (result.country) printf("%s\"country\":\"%s\"", field++ >0 ?",":"",result.country);
		if (result.city) printf("%s\"city\":\"%s\"", field++ >0 ?",":"",result.city);
		if (result.street) printf("%s\"street\":\"%s\"", field++ >0 ?",":"",result.street);
		if (result.house) printf("%s\"house\":\"%s\"", field++ >0 ?",":"",result.house);
		if (result.zipcode) printf("%s\"zipcode\":\"%s\"", field++ >0 ?",":"",result.zipcode);
		printf("}");
	}
	enddtime = micro_now();
	
	printf("]");
	if (dotimes) {
		printf(",\"timings\":{\"edocoeg\":%d}", enddtime-starttime);
	} 
	printf("}");

	// do NOT !! it is freed with content
	//if (coordinates) free(coordinates);
}

void handle_matrix(json_t *content, int id)
{
	static char erbuf[2048];
	static char buf[40960];
	json_t *options=NULL;
	json_t *params=NULL;
	json_t *departures=NULL;
	json_t *depx=NULL;
	json_t *depy=NULL;
	json_t *arrx=NULL;
	json_t *arry=NULL;
	json_t *arrivals=NULL;
	json_t *algorithm=NULL;
	json_t *timings=NULL;
	int dotimes=0; // default
	micro_t starttime;
	micro_t enddtime;
	int x, y;

	// default : Gosmore
	mat_t *(*fnc) (int,coord_t *,int,coord_t *) = NULL;

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		return;
	}
	options = json_object_get(params, "options");
	if (options) {
		algorithm = json_object_get(options, "algorithm");
		if (algorithm && algorithm->val.s) {
			if (strcmp(algorithm->val.s,"haversine")==0) 
				fnc=HaversineMatrix;
			//if (strcmp(algorithm->val.s,"gosmore")==0) 
				//fnc=GosmoreMatrix;
			if (strcmp(algorithm->val.s,"osrm")==0) 
				fnc=OsrmDistanceMatrix;
			if (strcmp(algorithm->val.s,"google")==0) 
				fnc=GoogleMatrix;
			if (strcmp(algorithm->val.s,"klopt")==0) 
				fnc=DiceMatrix;
			if (strcmp(algorithm->val.s,"dice")==0) 
				fnc=DiceMatrix;
		}
		timings = json_object_get(options, "timings");
		if (timings) {
			if (timings->val.b) dotimes=1;
		} 
	}
	departures = json_object_get(params, "departures");
	arrivals = json_object_get(params, "arrivals");
	if (!departures && !arrivals) {
		sprintf(erbuf, "matrix function needs departures and/or arrivals");
		web_error(id, -14,erbuf);
		return;
	} else if (!departures) {
		arrivals = departures;
	} else if (!arrivals) {
		departures = arrivals;
	}

	int cols = json_array_len(departures)/2;
	int rows = json_array_len(arrivals)/2;
	coord_t *deps= (coord_t *)calloc(cols,sizeof(coord_t));
	coord_t *arrs= (coord_t *)calloc(rows,sizeof(coord_t));

	for (x=0; x< cols; x++) {
		depy = json_array_get(departures, (x*2));
		if (depy) deps[x].lat =  depy->val.d;
		depx = json_array_get(departures, (x*2)+1);
		if (depx) deps[x].lon =  depx->val.d;
	}
	for (y=0; y< rows; y++) {
		depy = json_array_get(arrivals, (y*2));
		if (depy) arrs[y].lat =  depy->val.d;
		depx = json_array_get(arrivals, (y*2)+1);
		if (depx) arrs[y].lon =  depx->val.d;
	}

	starttime = micro_now();
	mat_t *mp = fnc(cols,deps,rows,arrs);
	enddtime = micro_now();
	
	printf("{\"jsonrpc\": \"2.0\",");
	if (id) {
		printf("\"id\": %d,", id);
	}
	printf("\"result\": ");
	buf[0] = '\0';
	mat_array(&buf[0], mp);
	printf("%s",buf);

	if (dotimes) {
		printf(",\"timings\":{\"matrix\":%d}", enddtime-starttime);
	} 
	printf("}");

	mat_rls(mp);
	if (deps) free(deps);
	if (arrs) free(arrs);

	//printf("coord: %f\n", coords->val.d);
	if (!departures) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		return;
	}
}

void print_route_array(route_result_t *mp,int count, int options)
{
	int r;
	int c;

	printf("[");
	for (r=0; r< count; r++) {
		int cols=0;
		if (r>0) printf(",");
		printf("{");
		if (options & OPT_METERS) {
			printf("%s\"mtr\":%d", (cols++)?",":"", mp[r].meters);
		} 
		if (options & OPT_MINUTES) {
			printf("%s\"min\":%d", (cols++) ? "," : "", mp[r].seconds);
		} 
		if (options & OPT_PATHS) {
			char *path=(char *)"";
			if (mp[r].path) 
				path=mp[r].path;
			printf("%s\"path\":%s", (cols++) ? ",": "", path);
		} 
		printf("}");
	}
	printf("]");
}

void handle_route(json_t *content, int id)
{
	static char erbuf[2048];
	static char buf[40960]={0};
	json_t *options=NULL;
	json_t *params=NULL;
	json_t *departures=NULL;
	json_t *depx=NULL;
	json_t *depy=NULL;
	json_t *arrivals=NULL;
	json_t *algorithm=NULL;
	json_t *varpar=NULL;
	micro_t starttime;
	micro_t enddtime;
	int x, y;
	int opts=OPT_METERS;

	// default : Gosmore
	route_result_t *(*fnc) (Cache *, int,coord_t *) = NULL;

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		return;
	}
	options = json_object_get(params, "options");
	if (options) {
		algorithm = json_object_get(options, "algorithm");
		if (algorithm && algorithm->val.s) {
			//if (strcmp(algorithm->val.s,"haversine")==0) 
				//fnc=HaversineRoute;
			if (strcmp(algorithm->val.s,"osrm")==0) 
				fnc=OsrmRoute;
			//if (strcmp(algorithm->val.s,"google")==0) 
				//fnc=GoogleRoute;
		}
		varpar = json_object_get(options, "timings");
		if (varpar) {
			if (varpar->val.b) opts |= OPT_TIMINGS;
		} 
		varpar = json_object_get(options, "meters");
		if (varpar) {
			if (varpar->val.b==0) opts &= ~OPT_METERS;
		} 
		varpar = json_object_get(options, "minutes");
		if (varpar) {
			if (varpar->val.b) opts |= OPT_MINUTES;
		} 
		varpar = json_object_get(options, "paths");
		if (varpar) {
			if (varpar->val.b) opts |= OPT_PATHS;
		} 
	}
	departures = json_object_get(params, "locations");
	if (!departures) {
		sprintf(erbuf, "route function needs locations");
		web_error(id, -14,erbuf);
		return;
	}

	int cols = json_array_len(departures)/2;
	coord_t *deps= (coord_t *)calloc(cols,sizeof(coord_t));

	for (x=0; x< cols; x++) {
		depy = json_array_get(departures, (x*2));
		if (depy) deps[x].lat =  depy->val.d;
		depx = json_array_get(departures, (x*2)+1);
		if (depx) deps[x].lon =  depx->val.d;
	}

	starttime = micro_now();
	route_result_t *mp = fnc(NULL,cols,deps);
	enddtime = micro_now();
	
	printf("{\"jsonrpc\": \"2.0\",");
	if (id) {
		printf("\"id\": %d,", id);
	}
	printf("\"result\": ");
	print_route_array(mp,cols-1, opts);

	if (opts & OPT_TIMINGS) {
		printf(",\"timings\":{\"route\":%d}", enddtime-starttime);
	} 
	printf("}");

	free(mp);
	if (deps) free(deps);

	//printf("coord: %f\n", coords->val.d);
	if (!departures) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		return;
	}
}

void handle_tsp(json_t *content, int id)
{
	static char erbuf[2048];
	static char buf[40960];
	json_t *options=NULL;
	json_t *params=NULL;
	json_t *locations=NULL;
	json_t *depx=NULL;
	json_t *depy=NULL;
	json_t *start=NULL;
	json_t *end=NULL;
	json_t *algorithm=NULL;
	json_t *max_ms=NULL;
	json_t *max_iter=NULL;
	json_t *timings=NULL;
	int dotimes=0; // default
	coord_t *startloc=NULL;
	coord_t *endloc=NULL;
	int ms=0;
	int it=0;
	int x, y;

	// default : Gosmore
	tsp_result_t *(*fnc) (int,coord_t *,coord_t *,coord_t *,int,int) = NULL;

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		return;
	}
	options = json_object_get(params, "options");
	if (options) {
		algorithm = json_object_get(options, "algorithm");
		if (algorithm && algorithm->val.s) {
			if (strcmp(algorithm->val.s,"optimal")==0) 
				fnc=TspLp;
			if (strcmp(algorithm->val.s,"exhaustive")==0) 
				fnc=TspExhaustive;
			if (strcmp(algorithm->val.s,"lp")==0) 
				fnc=TspLp;
			if (strcmp(algorithm->val.s,"heuristic")==0) 
				fnc=TspLk;
			if (strcmp(algorithm->val.s,"lk")==0) 
				fnc=TspLk;
		}
		max_ms = json_object_get(options, "max_ms");
		if (max_ms && max_ms->val.d) {
			ms = max_ms->val.ll;
		}
		max_iter = json_object_get(options, "max_iter");
		if (max_iter && max_iter->val.d) {
			it = max_iter->val.ll;
		}
		timings = json_object_get(options, "timings");
		if (timings) {
			if (timings->val.b) dotimes=1;
		} 
	}
	locations = json_object_get(params, "locations");
	if (!locations) {
		sprintf(erbuf, "tsp function needs a location list");
		web_error(id, -14,erbuf);
		return;
	}

	int cols = json_array_len(locations)/2;
	coord_t *locs= (coord_t *)calloc(cols,sizeof(coord_t));

	for (x=0; x< cols; x++) {
		depy = json_array_get(locations, (x*2));
		if (depy) locs[x].lat =  depy->val.d;
		depx = json_array_get(locations, (x*2)+1);
		if (depx) locs[x].lon =  depx->val.d;
	}

	start = json_object_get(params, "start");
	end = json_object_get(params, "end");

	tsp_result_t *mp = fnc(cols,locs,startloc,endloc,ms,it);
	
	printf("{\"jsonrpc\": \"2.0\",");
	if (id) {
		printf("\"id\": %d,", id);
	}
	printf("\"result\": {");
	printf("\"meters\": %f, \"order\" : [", mp[cols].dist);
	int frst=0;
	for (int t=0; t< cols; t++) {
		if (mp[t].index<0) continue;
		if (frst>0) printf (",");
		printf("%d",mp[t].index);
		frst++;
	}
	printf("], \"dist\" : [");
	frst=0;
	for (int t=0; t< cols; t++) {
		if (mp[t].index<0) continue;
		if (frst>0) printf (",");
		printf("%f",mp[t].dist);
		frst++;
	}
	printf("], \"unresolved\" : [");
	frst=0;
	for (int t=0; t< cols; t++) {
		if (mp[t].index>=0) continue;
		if (frst>0) printf (",");
		printf("%d",t);
		frst++;
	}
	printf("]}");
	if (dotimes) {
		printf(",\"timings\":{\"matrix\":%d,\"tsp\":%d}", mp->matrixtime, mp->tsptime);
	} 
	printf("}");

	free(mp);
	if (locs) free(locs);

	//printf("coord: %f\n", coords->val.d);
	if (!locations) {
		sprintf(erbuf, "json rpc message needs a params member");
		web_error(id, -10,erbuf);
		return;
	}
}

void dispatch_post(char *qry)
{
	static char erbuf[2048];
	long long id;
	json_t *content=NULL;
	json_t *ident=NULL;
	json_t *method=NULL;
	char *var=NULL, *val=NULL;

	//printf("query is %d long", strlen(qry));
	var = strtok(qry,"=");
	if (!var) {
		sprintf(erbuf, "Unparsable input: \"%s\"", qry);
		web_error(-1, -17, erbuf);
		goto cleanup;
	}
	//printf("query is %d long", strlen(qry));
	val = strtok(NULL,"\0");
	//printf("val is %d long", strlen(val));
	if (!val) {
		sprintf(erbuf, "No jsonrpc parameter found: %s", var);
		web_error(-1, -18, erbuf);
		goto cleanup;
	}

	//printf("We have var = %s<br>", var);
	//printf("We have val = \"%s\"", val);
	content = json_parse(val);
	//printf("Content : %s\n", content);
	if (!content) {
		sprintf(erbuf, "Could not parse json : %s", val);
		web_error(-1, -16, erbuf);
		goto cleanup;
	}

	//printf("Succeeded parsing !!");
	ident = json_object_get(content, "id");
	if (!ident) { // version 2 also allows this 
		ident= json_null();
	}
	id = ident->val.ll;

	method = json_object_get(content, "method");
	if (!method || method->val.s==NULL) {
		sprintf(erbuf, "no method : %s", qry);
		web_error(id, -11, erbuf);
		goto cleanup;
	}
	//printf("Method: %s\n", method->val.s);
	if (strcmp(method->val.s, "matrix") == 0) {
		handle_matrix(content,id);
	} else  
	if (strcmp(method->val.s, "route") == 0) {
		handle_route(content,id);
	} else  
	if (strcmp(method->val.s, "geocode") == 0) {
		handle_geocode(content,id);
	} else  
	if (strcmp(method->val.s, "edocoeg") == 0) {
		handle_edocoeg(content,id);
	} else  
	if (strcmp(method->val.s, "nearest") == 0) {
		handle_nearest(content,id);
	} else  
	if (strcmp(method->val.s, "proximity") == 0) {
		handle_proximity(content,id);
	} else  
	if (strcmp(method->val.s, "tsp") == 0) {
		handle_tsp(content,id);
	} else  {
		sprintf(erbuf, "Unknown method: %s", method->val.s);
		web_error(id, -12, erbuf);
		goto cleanup;
	}
cleanup:
	if (content) //json_rls(content);
		;
}

void web_post(char *qry)
{
	//CURL *curlHandle;
	//printf("received a post request: %s \n<br>", qry);

	//curlHandle = curl_easy_init();
	char *escdata=NULL;
	escdata = html_unescape(qry);
	ego_plus_2_space(escdata);
	//printf("escaped post data: \"%s\" (%d)\n<br>", escdata, strlen(escdata));

	//curl_free(escdata);
	dispatch_post(escdata);
	//printf("HLOOOOO : %p\n",escdata);
	//free(escdata);
	escdata=NULL;
	//curl_easy_cleanup(curlHandle); 
}

void web_agents(char *qry)
{
	if (qry) 
		printf("Unknown command %s\n", qry);
	else
		printf("No query\n");
}

void web_data(char *qry)
{
	char *subdir;
	subdir=strtok(NULL, "/");
	if (subdir && !strcmp(subdir, "geocode")) {
		printf("TW");
	} else 
	{
		static char err[100];
		if (subdir) 
			snprintf(err,100,"Unknown command %s", subdir);
		else
			snprintf(err,100,"No command ");
	
		web_error(-1,-7,err);
	}
}

void web_rest(char *dir, char *qry)
{
	char *subdir;
	subdir=strtok(dir, "/");
	//printf("sub found %s\n", subdir);
	if (subdir && !strcmp(subdir, "agents")) {
		web_agents(qry);
	} else {
			if (subdir && !strcmp(subdir, "data")) {
				web_data(qry);
			} else 
			{
				static char err[100];
				if (subdir)
					snprintf(err,100,"Unknown command %s", subdir);
				else 
					snprintf(err,100,"No command");
				web_error(-1,-9,err);
			}
	}
}

int main(int argc, char *argv[])
{
	// reenable when using database !
	//init((const char *)"/usr/local/share/grid/europe_car/network.osrm");
	//init((const char *)"/home/kees/projects/grid/3pty/osrm/luxembourg.osrm");
	//init((const char *)"/usr/local/share/grid/nl_car/netherlands.osrm");
	//init((const char *)"/projects/gen/netherlands.osrm");
	init((const char *)NETWORK);
	//init((const char *)"/projects/gen/luxembourg.osrm");
	//init((const char *)"/projects/gen/r6.osrm");
	//init((const char *)"/projects/gen/nl.osrm");

	while (FCGI_Accept () >= 0) {
		printf ("Content-type: text/html\r\n\r\n");
		char *qry = getenv("QUERY_STRING");
		char *pi = getenv("PATH_INFO");

		// test for GET or POST or REST
		if (pi!=NULL && pi[0] != '\0') {
			web_rest(pi,qry);
		} else {
			if (qry!=NULL && qry[0] == '\0') { // POST
            	//printf ("method is POST<br>");
            	char *cl = getenv ("CONTENT_LENGTH");
            	int sz = atoi (cl); 
				//printf("Content length is %d\n", sz);
            	char *stdinqry = (char *)calloc (sz+1,1);
            	FCGI_fread (stdinqry, 1, sz, (FILE *)stdin);
				if (!stdinqry) web_error(-1,-7,"no POST command found");
				else web_post(stdinqry);
				//free(stdinqry);
        	} else { // GET
            	printf ("method is GET<br>");
				if (!qry) web_error(-1,-8,"no GET command found");
				else web_get(qry);
        	}
		}
#ifdef MEMTESTING
	// mysql tempts to leave stuff open internally
	// only enable this on testing
		//mysql_library_end();
		//printf("closed mysql library !");
#endif
	}

	return 0;
}
