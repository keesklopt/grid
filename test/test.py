#!/usr/bin/python

# this need request for python , you could use pip :
# sudo apt-get install python-pip
# pip install requests

import httplib, urllib
import random
import requests

def restit(postdata):
	params = urllib.urlencode({'jsonrpc':postdata})
	#headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
	
	#conn = httplib.HTTPConnection("localhost")
	#conn.request("POST", "/matrix/rest/", params, headers)
	#r = requests.post("http://192.168.2.8/ws", data=postdata);
	print postdata
	r = requests.post("http://192.168.2.5/ws", data=postdata);
	response = r.text
	print response

def postit(postdata):
	#postdata='{"jsonrpc":"2.0","method":"matrix","id":33,"params":{"options" : {"algorithm": "gosmore"},"departures":[52.9976509755943,5.185619252035394,52.47328829718754,5.872080947039649,52.513675135560334,5.651127592660487,52.84789389697835,5.766880027484149,52.84887700714171,5.877102087950334,52.212042027153075,5.083785379771143,52.314153442159295,5.710921450983733,52.502960193902254,5.593012633034959,52.013963281875476,5.1497770545538515,52.381215553730726,5.090464182663709],"arrivals":[52.9976509755943,5.185619252035394,52.47328829718754,5.872080947039649,52.513675135560334,5.651127592660487,52.84789389697835,5.766880027484149,52.84887700714171,5.877102087950334,52.212042027153075,5.083785379771143,52.314153442159295,5.710921450983733,52.502960193902254,5.593012633034959,52.013963281875476,5.1497770545538515,52.381215553730726,5.090464182663709]}}'
	#postdata='{"jsonrpc":"2.0","method":"matrix","id":33,"params":{"options":{"algorithm":"gosmore"},"departures":[52.0,4.2,51.9,4.4],"arrivals":[52.0,4.2, 51.9,4.4, 52.4,4.9]}}'
	#postdata='{"jsonrpc":"2.0","method":"matrix","id":33,"params":{"options":{"algorithm":"gosmore"},"departures":[52.97538373200223,4.913825348718092,52.37634424190037,4.940423322143033,52.338702973444015,4.0894381534308195],"arrivals":[52.0,4.2,51.9,4.4,52.4,4.9]}}'
	#postdata='{"jsonrpc":"2.0","method":"matrix","id":33,"params":{"options":{"algorithm":"osrm"},"departures":[52.224099,5.142664,52.224099,5.142664,52.014485,4.298825,52.156817,5.391588,51.617793,4.768903,51.796369,5.200752,51.261804,5.908336,52.261989,6.70947,52.565897,5.929364,53.164758,6.733783],"arrivals":[52.224099,5.142664,52.224099,5.142664,52.014485,4.298825,52.156817,5.391588,51.617793,4.768903,51.796369,5.200752,51.261804,5.908336,52.261989,6.70947,52.565897,5.929364,53.164758,6.733783]}}'
	#postdata='{"jsonrpc":"2.0","method":"matrix","id":33,"params":{"options" : {"algorithm": "osrm"},"departures":[52.24209598288871,5.11783932405524,52.93621819652617,5.753571553854272,52.01787140313536,5.434042599983513,52.55098614026792,5.765382315032184,52.09281552187167,5.044210322666913,52.2906802482903,5.311149526154622,52.84028829354793,5.469277803087607,52.30574019975029,5.698526917491108,52.956231186166406,5.941500230226666,52.80890071182512,5.313514565816149],"arrivals":[52.24209598288871,5.11783932405524,52.93621819652617,5.753571553854272,52.01787140313536,5.434042599983513,52.55098614026792,5.765382315032184,52.09281552187167,5.044210322666913,52.2906802482903,5.311149526154622,52.84028829354793,5.469277803087607,52.30574019975029,5.698526917491108,52.956231186166406,5.941500230226666,52.80890071182512,5.313514565816149]}}';
	params = urllib.urlencode({'jsonrpc':postdata})
	headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
	
	conn = httplib.HTTPConnection("localhost")
	conn.request("POST", "/ws/", params, headers)
	response = conn.getresponse()
	print response.status, response.reason
	data = response.read()
	print data
	conn.close()

def randit():

	# europe 
	#longstart=46.0;
	#longend=52.0;
	#latstart=4.0;
	#latend=16.0;

	# netherlands
	longstart=51.5;
	longend=52.5;
	latstart=4.5;
	latend=6.0;

	start = random.randint(10,20);
	stop = random.randint(30,40);
	locstring = "";
	for d in range(start,stop):
		t = random.random();
        locstring += str(longstart + t * (longend-longstart))
		locstring += ",";
		t = random.random();
        locstring += str(latstart + t * (latend-latstart))
		locstring += ",";
	locstring = locstring[:-1];
	postdata='jsonrpc={"jsonrpc":"2.0","method":"matrix","id":33,"params":{"options" : {"algorithm":"osrm"},"departures":[' + locstring + '],"arrivals":[' + locstring +']}}';
	return postdata

for x in range(10000):
	postdata = randit();
	restit(postdata)
