#!/usr/bin/python

# this need request for python , you could use pip :
# sudo apt-get install python-pip
# pip install requests

import httplib, urllib
import random
import requests

l=1000

def restit(postdata):
    r = requests.post("http://localhost/ws", data=postdata);
    print postdata
    response = r.text
    print response

def randit():

    # europe 
    #longstart=46.0;
    #longend=52.0;
    #latstart=4.0;
    #latend=16.0;

    # netherlands
    latstart=51.5;
    latend=52.5;
    longstart=4.5;
    longend=6.0;
    amount=1000 # first entry is total capacity

    start = random.randint(10,45);
    stop = random.randint(45,80);
    #start = 0
    #stop = 18
    locstring = "{";
    for d in range(start,stop):
        locstring += '"amount":' + str(amount) + ',"location":['
        t = random.random();
        locstring += str(longstart + t * (longend-longstart))
        locstring += ",";
        t = random.random();
        locstring += str(latstart + t * (latend-latstart))
        locstring += "]},{";
        amount = random.randint(10,40);
    locstring = locstring[:-2];
    postdata='{"jsonrpc":"2.0","method":"vrp","id":33,"params":{"options" : {"algorithm":"osrm"},"orders":[' + locstring + ']}}';
    return postdata

l=1;

for x in range(0,l):
    postdata = randit();
    restit(postdata)
