#!/usr/bin/python

# this need request for python , you could use pip :
# sudo apt-get install python-pip
# pip install requests

import httplib, urllib
import random
import requests

l=19

def restit(postdata):
    #headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    
    #conn = httplib.HTTPConnection("localhost")
    #conn.request("POST", "/matrix/rest/", params, headers)
    r = requests.post("http://localhost/ws", data=postdata);
    print postdata
    response = r.text
    print response

def randit():

    # europe 
    #longstart=46.0;
    #longend=52.0;
    #latstart=4.0;
    #latend=16.0;

    # netherlands
    latstart=51.5;
    latend=52.5;
    longstart=4.5;
    longend=6.0;

    start = random.randint(10,15);
    stop = random.randint(15,20);
    start = 0
    stop = 18
    locstring = "[";
    for d in range(start,stop):
        t = random.random();
        locstring += str(longstart + t * (longend-longstart))
        locstring += ",";
        t = random.random();
        locstring += str(latstart + t * (latend-latstart))
        locstring += "],[";
    locstring = locstring[:-2];
    postdata='{"jsonrpc":"2.0","method":"tsp","id":33,"params":{"options" : {"algorithm":"osrm"},"locations":[' + locstring + ']}}';
    return postdata


for x in range(0,l):
    postdata = randit();
    restit(postdata)
