#!/usr/bin/python

# this need request for python , you could use pip :
# sudo apt-get install python-pip
# pip install requests

import httplib, urllib
import random
import sys
import requests
import time
import unicodedata
import json

def randit():

    # urp
    global diff
    latstart=46.0;
    latend=52.0;
    longstart=4.0;
    longend=16.0;

    # netherlands
    #latstart=51.5;
    #latend=52.5;
    #longstart=4.5;
    #longend=6.0;

    #stop = random.randint(30,1000);

    stop = 20
    diff = stop
    sys.stdout.write ("locations: " + str(diff));
    sys.stdout.write (" calculations: " + str(diff * diff));
    locstring = "[";
    for d in range(0,stop):
        t = random.random();
        locstring += str(longstart + t * (longend-longstart))
        locstring += ",";
        t = random.random();
        locstring += str(latstart + t * (latend-latstart))
        locstring += "],[";
    locstring = locstring[:-2];
    #postdata='jsonrpc={"jsonrpc":"2.0","method":"route","id":33,"params":{"options":{"algorithm":"osrm","paths":true},"departures":[' + locstring + '],"arrivals":[' + locstring +']}}';
    postdata='{"jsonrpc":"2.0","method":"matrix","id":33,"params":{"options":{"algorithm":"osrm"},"locations":[' + locstring + ']}}';

    return postdata

def restit(postdata):
    global diff
    params = {'jsonrpc':postdata}
    #r = requests.post("http://192.168.2.8/ws", data=postdata);
    #print postdata
    #r = requests.post("http://klopt.org/ws", data=postdata);
    r = requests.post("http://localhost/ws", postdata);
    #r = requests.post("http://192.168.2.5:1901/post.php", data=params);
    response = r.text
    print response
    decoded = json.loads(response)
    #num = decoded['result']['timings']['matrix']
    #print(" mseconds " + str(num/1000) + "," + str(num/diff/1000) + " per location") ;

l=1
l=10000

for x in range(0,l):
    postdata = randit();
    restit(postdata)
    print " %d/%d\r" % (x, l),
    sys.stdout.flush()

