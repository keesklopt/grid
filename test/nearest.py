#!/usr/bin/python

# this need request for python , you could use pip :
# sudo apt-get install python-pip
# pip install requests

import httplib, urllib
import random
import requests
import sys

#SERVICE = "http://klopt.org/ws";
SERVICE = "http://localhost/ws";

def restit(postdata):
    print postdata

    r = requests.post(SERVICE, postdata);
    response = r.text
    print response

def randit():

    # europe 
    longstart=4.0;
    longend=16.0;
    latstart=46.0;
    latend=52.0;

    # netherlands
    #longstart=4.5;
    #longend=6.0;
    #latstart=51.5;
    #latend=52.5;

    start = random.randint(10,200);
    stop = random.randint(30,400);
    locstring = "";
    for d in range(start,stop):
        t = random.random();
        locstring += "[";
        locstring += str(longstart + t * (longend-longstart))
        locstring += ",";
        t = random.random();
        locstring += str(latstart + t * (latend-latstart))
        locstring += "],";
    locstring = locstring[:-1];
    optionstring = "\"algorithm\":\"osrm\"";
    if (random.randint(0,1) == 1): optionstring += ",\"meters\":true";
    if (random.randint(0,1) == 1): optionstring += ",\"matchname\":true";
    if (random.randint(0,1) == 1): optionstring += ",\"membernames\":true";
    postdata='{"jsonrpc":"2.0","method":"nearest","id":33,"params":{"options":{' + optionstring + '},"locations":[' + locstring + ']}}';

    return postdata

l=100

for x in range(0,l):
    postdata = randit();
    restit(postdata)
    print "%d/%d\r" % (x, l),
    sys.stdout.flush()
