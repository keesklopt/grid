#!/usr/bin/python

# this need request for python , you could use pip :
# sudo apt-get install python-pip
# pip install requests

import httplib, urllib
import random
import requests
import sys
import json

def restit(postdata):
    params = {'jsonrpc':postdata}
    
    #print postdata

    #r = requests.post("http://klopt.org/ws", postdata);
    r = requests.post("http://localhost/ws", postdata);
    response = r.text
    print response
    try:
        decoded = json.loads(response)
        #print zipcode,number,"->",obj['result']['locations'][0]
        #print obj
    except Exception as err:
        print "Json parse error"
        print err
        print utext

def randit():

    # europe 
    #longstart=46.0;
    #longend=52.0;
    #latstart=4.0;
    #latend=16.0;

    # netherlands
    longstart=4.5;
    longend=6.0;
    latstart=51.5;
    latend=52.5;

    start = random.randint(10,20);
    stop = random.randint(30,40);
    locstring = "";
    for d in range(start,stop):
        t = random.random();
        locstring += str(longstart + t * (longend-longstart))
        locstring += ",";
        t = random.random();
        locstring += str(latstart + t * (latend-latstart))
        locstring += "],[";
    locstring = locstring[:-3];
    postdata='{"jsonrpc":"2.0","method":"routes","id":33,"params":{"options":{"algorithm":"osrm","paths":true},"locations":[[' + locstring + ']]}}';

    return postdata

l=1
l=10000

for x in range(0,l):
    postdata = randit();
    restit(postdata)
    print "%d/%d\r" % (x, l),
    sys.stdout.flush()
