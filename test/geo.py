#!/usr/bin/python

# this need request for python , you could use pip :
# sudo apt-get install python-pip
# pip install requests

import httplib, urllib
import random
import requests
import json
import sys

#url="http://klopt.org/ws"
url="http://localhost/ws"

def rev_zip(zipcode,number):
    locstring='"zipcode":"' + zipcode + '","number":"' + number + '"';
    postdata='{"jsonrpc":"2.0","method":"locations","id":33,"params":{"options":{"algorithm":"osrm","timings":true},"addresses":[{' + locstring + '}]}}';
    r = requests.post(url, data=postdata);
    response = r.text
    utext = unicode(response).encode("utf-8");
    print "rev_zip -----------"
    print postdata
    print utext
    try:
        obj = json.loads(utext);
        print zipcode,number,"->",obj['result']['locations'][0]
        #print obj
    except Exception as err:
        print "Json parse error"
        print err
        print utext
    
def rev_street(city,street,number):
    locstring='"city":"' + city + '","street":"' + street + '","number":"' + number + '"';
    postdata='{"jsonrpc":"2.0","method":"locations","id":33,"params":{"options":{"algorithm":"osrm","timings":true},"addresses":[{' + locstring + '}]}}';
    r = requests.post(url, data=postdata);
    response = r.text
    utext = unicode(response).encode("utf-8");
    #print "rev_street -----------"
    #print postdata
    #print utext
    try:
        obj = json.loads(utext);
        print city,street,number,"->",obj['result']['locations'][0]
        #print obj
    except Exception as err:
        print "Json parse error"
        print err
        print utext
    

def restit(postdata):
    print "geo -----------"
    print postdata

    r = requests.post(url, data=postdata);
    response = r.text
    utext = unicode(response).encode("utf-8");

    print utext
    try:
        obj = json.loads(utext);
        city = obj['result']['addresses'][0]['city']
        zipcode = obj['result']['addresses'][0]['zipcode']
        street = obj['result']['addresses'][0]['street']
        number = obj['result']['addresses'][0]['number']
        rev_zip(zipcode,number)
        rev_street(city,street,number)
    except Exception as err:
        print "Json parse error"
        print err
        print utext

def randit():

    # netherlands
    longstart=4.5;
    longend=6.0;
    latstart=51.5;
    latend=52.5;

    start = random.randint(10,200);
    stop = start + random.randint(30,400);
    start=0
    stop=1
    locstring = "";
    for d in range(start,stop):
        t = random.random();
        locstring += str(longstart + t * (longend-longstart))
        locstring += ",";
        t = random.random();
        locstring += str(latstart + t * (latend-latstart))
        locstring += ",";
    locstring = locstring[:-1];
    #{"jsonrpc":"2.0","method":"addresses","id":533,"params":{"options":{"algorithm":"osrm","timings":true,"maxmeters":9999},"locations":[[4.692277905705851,52.29318747388248]]}}
    print (locstring);

    postdata='{"jsonrpc":"2.0","method":"addresses","id":33,"params":{"options":{"algorithm":"osrm","timings":true,"maxmeters":999},"locations":[[' + locstring + ']]}}';

    return postdata

l=100000
l=1
l=10000

for x in range(0,l):
    postdata = randit();
    restit(postdata)
    print "%d/%d\r" % (x, l),
    sys.stdout.flush()
