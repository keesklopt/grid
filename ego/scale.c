#include <ego.h>
#include <float.h>
#include <math.h>

void scale_recalc(scale_t *sp)
{
    sp->ww = sp->wr-sp->wl;
    sp->mw = sp->mr-sp->ml;
    sp->scale = (float)sp->mw/(float)sp->ww;
}

void scale_set_world_snap(scale_t *sp, int offset, int step)
{
    sp->wo = offset;
    sp->ws = step;
}

int scale_snap_world(scale_t *sp, int wx)
{
	int left;
    int absl = wx - sp->wo; // calc from first snap point
    int  nsnaps = (int)((float)(absl/sp->ws) + 0.5);      // which snapslot is it in
    left = nsnaps * sp->ws;    // get left side of slot
	printf("absl %d, nsnaps %d, left %d, %d\n", absl, nsnaps, left, left+sp->wo);
    left += sp->wo;        // restore offset


    return left;
}

scale_t * scale_ist(int wl, int wr, int ml, int mr)
{
	scale_t *sp = (scale_t *)calloc(sizeof(scale_t),1);

	sp->wl=wl;
	sp->wr=wr;
	sp->ml=ml;
	sp->mr=mr;
	
	scale_recalc(sp);

	return sp;
}

void scale_set_world(scale_t *sp, int start, int end)
{
    sp->wl = start;
    sp->wr = end;
    scale_recalc(sp);
}

void scale_set_model(scale_t *sp, int start, int end)
{
    sp->ml = start;
    sp->mr = end;
    scale_recalc(sp);
}

int scale_get_world(scale_t *sp, int m)
{
	int wr = (((float)(m - sp->ml)+0.5) / sp->scale) + (float)sp->wl;
    return wr;
}

int scale_get_model (scale_t *sp, int w)
{
    int mr = (((float)(w - sp->wl)+0.5) * sp->scale) + (float)sp->ml;
	return mr;
}

void scale_dmp(scale_t *sp)
{
	
	printf("world %d-%d %d\n", sp->wl, sp->wr, sp->ww);
	printf("model %d-%d %d\n", sp->ml, sp->mr, sp->mw);

	printf("snap %d %d (scale %f)\n", sp->wo, sp->ws, sp->scale);
}

int scale_snap_model (scale_t *sp, int mr)
{
    // get world equivalent
    int wrld = (int)scale_get_world(sp,mr);
    // snap world !!
    int val = scale_snap_world(sp,wrld);
    // return model equivalent
    return (int)scale_get_model(sp,val);
}

int scale_test(int w, int h)
{
	scale_t *sp = scale_ist(10,100, 40, 5678);

	printf("-> %d\n", scale_get_model(sp,50));
	printf("-> %d\n", scale_get_model(sp,10));
	printf("-> %d\n", scale_get_model(sp,100));
	return 1;
}
