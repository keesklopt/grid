#include <ego.h>
/** @file 
 * string related functions
 * 
 */

/** @brief is whitespace ?
 *
 * is whitespace 
 *
 *	@param c character to test
 *	@return 1 if c is whitespace, 0 otherwise
 */
int ego_iswhitespace(int c)
{
	return (c == ' ' || c == '\t' || c ==  '\n' || c =='\r');
}

/** @brief is a digit ?
 *
 * isdigit was present in some libc's and not in others.
 *	So i made this very complex function myself, but ego_ is prepended
 *	to avoid name clashes.
 *
 *	@param c character to test
 *	@return 1 if c is a digit, 0 otherwise
 */
int ego_isdigit(int c)
{
	return (c >= '0' && c <= '9');
}

/** @brief is printable ?
 *
 * isprint was present in some libc's and not in others.
 *
 * @param c character to test
 * @return 1 if c is printable, 0 otherwise
 */
int ego_isprint(int c)
{
	return (c >= ' ' && c <= '~');
}

static double ego_round(double dubbel, unsigned int after)
{
	int t = after;

	while(t--)
		dubbel *= 10.0;

	dubbel += 0.5;

	t = after;
	while(t--)
		dubbel /= 10.0;

	return dubbel;
}

/** @brief characters needed for X-based numbers
 *
 * returns the length of a number when printed in a certain base
 * used in ego_sprintf and ego_strlenf for calculating character positions
 *
 * @param num number to be printed
 * @param base of num (16=hex, 2 is binary etc).
 * @param mod modifier used in printf (2,4 or 8 for %h,%l and %L).
 * @param sign length will be 1 more if negative
 * @return length of number in characters
 */
int baselength(long long num, long base, long mod, long sign)
{
	int  i=0;
	unsigned long ulang;
	unsigned short ukort;
	unsigned long long ullang;
	
	ullang = num;
	ulang = (unsigned long)num;
	ukort = (unsigned short)num;

	if (sign < 0) {
		if (num < 0) {
			i++;
			ulang = num * -1;
			ukort = (unsigned short)num * -1;
		}
	}

	if (mod == 8) {
		if (ullang == 0)
			return i+1;
		while(ullang > 0) {
			ullang /= base; 
			i++;
		}
	} else 
	if (mod == 4) {
		if (ulang == 0)
			return i+1;
		while(ulang > 0) {
			ulang /= base; 
			i++;
		}
	} else {
		if (ukort == 0)
			return i+1;
		while(ukort > 0) {
			ukort /= (unsigned short)base; 
			i++;
		}
	}
	return i;
}

/** @brief allocating strcat
 *
 * strcat version that allocates. SO USE RETURN VALUE because it can 
 * be reallocated  
 *
 * @param dst first string
 * @param src second string
 * @return new and concattenated string
 */
char *ego_strcat(char *dst, char *src)
{
	int len = strlen(dst)+strlen(src)+1;

	dst = realloc(dst, len);
	strncat(dst, src, len);
	
	return dst;
}

/** @brief allocating strcat that frees it's parameters
 *
 * like ego_strcat but also free src 
 *
 * @param dst first string, freed afterwards
 * @param src second string, freed afterwards
 * @return new and concattenated string
 */
char *ego_strcat_free(char *dst, char *src)
{
	int len = strlen(src)+strlen(dst)+1;

	dst = realloc(dst, len);
	strncat(dst, src, len);

	free(src);
	
	return dst;
}

/** @brief duplicate a string, also if it is NULL
 *
 * like ego_strdup but handle NULL
 *
 * @param orig original string
 * @return newly duplicated and allocated string
 */
char *ego_strdup(const char *orig)
{
	char *cpy;
	if (!orig) return NULL;
	int len = strlen(orig);

	cpy = malloc(len+1);
	memcpy(cpy,orig,len+1);

	return cpy;
}

/** @brief characters needed for X-based double
 *
 * return the length of a double when printed in a certain base
 * the number of characters is given using :
 *  
 * @param lang double precision number to print
 * @param base (16=hex, 2 is binary etc).
 * @param mod modifier used in printf (2,4 or 8 for %h,%l and %L).
 * @param sign length will be 1 more if negative
 * @return length of number in characters
 */
int floatlength(double lang, long base, long mod, long sign)
{
	int  i=0;
	
	if (sign < 0) {
		if (lang < 0) {
			i++;
			lang = lang * -1;
		}
	}

	while(lang >= 1.0) {
		lang /= base; 
		/* printf("das %d %f\n", i, lang); */
		i++;
	}
	if (i== 0)
		i = 1;
	return i;
}

static int strlenf(const char *fmt, va_list argptr)
{
	int t, c=0;
	int len=0, precision[2]; 
	long long num;
	unsigned long long unum;
	//char kar;
	char *string;
	double dubbel;
	//int *intp;
	//void *leeg;
	int longest=0;
	char *ptr;
	static char *buf=NULL;

	/* va_list argptr; */

	/* c+= strlen(format); */
	while(*(fmt)) {
#ifdef TESTING
		printf("before all (%s) \n", fmt);
#endif
		if (*fmt == '%') {
			int sign = 0;
			int alt  = 0;
			int mod  = 4;
			/* options */
#ifdef TESTING
			printf("found percent(%s) \n", fmt);
#endif
			switch (*(++fmt)) { 
				case '%' : fmt++; 
					c++; break;
				case '+' : case ' ' : case '-' : case '#' : case '0' : 
				 	while(*fmt) {
			 			switch (*(fmt)) { 
				 			case '+' : fmt++; sign = 1; break;
							case ' ' : fmt++; break;
							case '-' : fmt++; break;
							case '#' : fmt++; alt = 1; break;
							case '0' : fmt++; break;
							default  : /* end of options */
								goto precision;
							break;
						}
					}
			}
precision:
#ifdef TESTING
			printf("after options (%s) \n", fmt);
#endif
			
			precision[0] = 0;
			precision[1] = -1;
			for (t=0; t< 2; t++) {
#ifdef TESTING
				printf("[%d] (%s) \n", t, fmt);
#endif
				switch (*(fmt)) { 
				 	case '*' : precision[t] = va_arg(argptr, int); 
#ifdef TESTING
						printf("varg was %d\n", precision[t]);
#endif
						fmt++;
					break;
				 	case '0' : case '1' : case '2' : case '3' : case '4' :
				 	case '5' : case '6' : case '7' : case '8' : case '9' :
						ptr = (char *)fmt;
				 		while(*fmt && ego_isdigit(*fmt)) {
							fmt++;
						}
						if (fmt-ptr >= longest) {
							longest = (fmt-ptr)+1;
							buf = realloc(buf, longest);
						}
						strncpy(buf, ptr, longest-1);
						precision[t] = atoi(buf);
						free(buf);
						longest = 0;
						buf = NULL;
#ifdef TESTING
						printf ("%s became %d\n", buf, precision[t]);
#endif
					break;
				}
				if (t == 0 && *(fmt) == '.') {
					precision[1] = 0;
					fmt++;
				} else  {
					goto modifiers;
				}
			}
modifiers:

#ifdef TESTING
			printf("before modifiers %s [%d/%d]\n", fmt, precision[0], precision[1]);
#endif
			/* read the modifiers */
			switch (*(fmt)) { 
				case 'h' : 
					mod = 2;
					fmt++;
					if (*fmt == 'h') {
						fmt++;
						mod=1;
					}
					break;
				case 'l' : 
					mod = 4;
					fmt++;
					if (*fmt == 'l') {
						fmt++;
						mod=8;
					}
					break;
				case 'L' : 
					mod = 8;
					fmt++;
					break;
				default:
				break;
			}
#ifdef TESTING
			printf("after modifiers %s\n", fmt);
#endif

			/* read the type letters */
			switch (*(fmt)) { 
				case 'd' : 
				case 'i' : 
				 	num = va_arg(argptr, long long);
					len = baselength(num, 10, mod, -1);
					len = EGO_MAX(len, precision[0]);
					len = EGO_MAX(len, precision[1]);
					if (sign && num >= 0)
						len++;
				break;
				case 'o' : 
				 	unum = va_arg(argptr, unsigned long long);
					len = baselength(unum, 8, mod, 1);
					if (alt) len++;
					len = EGO_MAX(len, precision[0]);
					len = EGO_MAX(len, precision[1]);
				break;
				case 'u' : 
				 	unum = va_arg(argptr, unsigned long long);
					len = baselength(unum, 10, mod, 1);
					len = EGO_MAX(len, precision[0]);
					len = EGO_MAX(len, precision[1]);
					if (sign) len++;
				break;
				case 'x' : 
				case 'X' : 
				 	num = va_arg(argptr, long long);
					len = baselength(num, 16, mod, 1);
					len = EGO_MAX(len, precision[0]);
					len = EGO_MAX(len, precision[1]);
					if (alt) len += 2;
				break;
				case 'c' : 
				 	va_arg(argptr, int);
					len = 1;
					if (precision[1] == -1) {
						num = EGO_MAX(len, precision[0]);
					} else  {
						num = EGO_MAX(precision[0],  EGO_MIN(len, precision[1]));
					}
					len = num;
				break;
				case 's' : 
				 	string = va_arg(argptr, char *);
					len = strlen(string);
					if (precision[1] == -1) {
						num = EGO_MAX(len, precision[0]);
					} else  {
						num = EGO_MAX(precision[0],  EGO_MIN(len, precision[1]));
					}
					len = num;
				break;
				case 'f' : 
				 	dubbel = va_arg(argptr, double);
					if (precision[1] == -1)
						precision[1] = 6;
					dubbel = ego_round(dubbel, precision[1]);
					len = floatlength(dubbel, 10, mod, 1);
#ifdef TESTING
					printf("%d %d and %d \n", precision[0], precision[1], len);
#endif
					len = EGO_MAX(precision[0], (len+ precision[1]+ (precision[1] == 0 ? 0 : 1)));
				break;
				case 'e' : 
				case 'E' : 
				 	dubbel = va_arg(argptr, double);
					if (precision[1] == -1)
						precision[1] = 6;
					len = 5;  /* 1 digit, an E, a sign and two digits */
					len = EGO_MAX(precision[0], (len+ precision[1]+ (precision[1] == 0 ? 0 : 1)));
				break;
				case 'g' : 
				case 'G' : 
				 	dubbel = va_arg(argptr, double);
					len = 100;
				break;
				case 'p' : 
				 	va_arg(argptr, void *);
					len = 100;
				break;
				case 'n' : 
				 	va_arg(argptr, int *);
				break;
				case '\0' : 
					len = -1;
				break;
				default:
					/* unknown digit, just copied to the output */
					len = 1;
				break;
			}

			c+=len;
#ifdef TESTING
			printf("me thinx %d, %d %s\n", len, c, fmt);
#endif

		} else 
			c++;
#ifdef TESTING
		printf("----> %d %s\n", c, fmt);
#endif
		fmt++;
	}
	return c;
}

/** @brief length needed for sprintf
 * 
 * calculate the length a printf would generate.
 * 
 * dont print it, just calculate based upon the format string and
 * the arguments.
 * @param fmt printf format string
 * @return number of characters a (s)printf command would need
 */
int ego_strlenf(const char *fmt, ...) 
{
	int len;
	va_list argptr;

	va_start(argptr, fmt);
	len = strlenf(fmt, argptr);
	va_end(argptr);

	return len;
}

/** @brief allocating vsprintf
 *
 * this function calculates the space needed for the formatstring and 
 * 	arguments given, than allocates and prints the string.
 * 
 * 	@param fmt format string
 * 	@param argptr pointer to first argument
 * 	@return allocated and printed string
 */
char *ego_vsprintf(const char *fmt, va_list argptr)
{
	int len;
	char *ptr;
	va_list argcopy;

	va_copy(argcopy,argptr); // cannot use va_list twice

	len = strlenf(fmt, argptr);
	len++;
	ptr = (char *)malloc(len);

	vsnprintf(ptr, len, fmt, argcopy);
	return ptr;
}

/** @brief allocatingvsprintf
 *
 * this function calculates the space needed for the formatstring and 
 * 	arguments given, than allocates and prints the string.
 * 
 * 	@param fmt format string
 *	@param ... variable number of arguments
 * 	@return allocated and printed string
 */
char *ego_sprintf(const char *fmt, ...)
{
	char *ptr;

	va_list argptr;
	va_start(argptr, fmt);

	ptr = ego_vsprintf(fmt, argptr);

	va_end(argptr);
	return ptr;
}

/** @brief strip
 *
 * this function strips all whitespace characters from the string
 * 
 * 	@param str string to be stripped
 */
void ego_strip(char *str)
{
	int lo=0;
	int hi=0;

	if (!str) return;
	while (str[hi]!= '\0') {
		str[lo] = str[hi];
		if (!ego_iswhitespace(str[lo])) {
			lo++;
		}
		hi++;
	}
	str[lo] = str[hi];
}

static int get_code(char *str)
{
	int code=0;
	int c;
	int mult[2] = { 16, 1};

	for (c=0; c< 2; c++) {
		char x = str[c];
		if (x >= '0' && x <= '9') {
			code += (x - '0');
		} else 
		if (x >= 'A' && x <= 'F') {
			code += 10 + (x - 'A');
		} else 
		if (x >= 'a' && x <= 'f') {
			code += 10 + (x - 'a');
		} else {
			return -1;
		}

		code *= mult[c];
	}

	//printf("Found %d\n", code);

	return code;
}

/** @brief unescape html codes
 *
 * this function converts HTML escape codes back into characters
 * 
 * 	@param str string to be converted
 */
char *html_unescape(char *str)
{
	char *newstring=NULL;
	int len=0;
	int newlen=0;
	int code;

	if (!str) return str;
	// first count
	while (str[len]) {
		if (str[len] == '%') {
			code = get_code(&str[len+1]);
			if (code >= 0) {
				len += 2;
			}
		}
		newlen++;
		len++;
	}
	//printf("Counted %d and %d\n", len, newlen);
	newstring = calloc(newlen+1,1);
	len=0;
	newlen=0;
	while (str[len]) {
		newstring[newlen] = str[len];
		if (str[len] == '%') {
			code = get_code(&str[len+1]);
			if (code >= 0) {
				newstring[newlen] = code;
				len += 2;
			} 
		}
		newlen++;
		len++;
	}
	return newstring;
}

/** @brief convert html + signs
 *
 * this function converts all + signs to a space (POST strings)
 * because curl_easy_unescape doesn't
 * 
 * 	@param str string to be converted
 */
void ego_plus_2_space(char *str)
{
	int hi=0;

	if (!str) return;
	while (str[hi]!= '\0') {
		if (str[hi]=='+') 
			str[hi] = ' ';
		hi++;
	}
}

int string_test(int w, int l)
{
	//char *test = ego_sprintf("jaj %lld ha", 1007862003030300000LL);
	//int len = ego_strlenf("jaj %lld ha", 1000330378638756200LL);
	//printf("jaj %lld ha\n",  1620000000000000000LL);
	//printf("jaha %d %d (%s)\n", len, strlen(test),test);

	char *result=html_unescape("slkj%22%3A  %5C %");
	printf(": %s\n", result);

	return 1;
}
