/** @file
 * 
	table structure

	This implements a table implementation. 
 * or matrix, something rather rectangular
 */
#include <ego.h>
/** @brief instantiate table
 *
 * creat a table with the given rows and cols
 *
 * @param rows initial number of rows in the table
 * @param cols initial number of columns in the table
 * @return a pointer to a new table structure
 */
table_t *table_ist(int rows, int cols)
{
	int x, y;
	table_t *tbl = (table_t *)calloc(1,sizeof(table_t));
	tbl->rows = da_ist(sizeof(da_t *),0);

	for (x=0; x< cols; x++) {
		table_ins_col(tbl,0);
	}

	for (y=0; y< rows; y++) {
		table_ins_row(tbl,0);
	}

	return tbl;
}

/** @brief get table data
 *
 * get data on the given x and y position in the table
 *
 * @param tbl table to get the cell data from
 * @param x at which column or x position resides the data
 * @param y at which row or y position resides the data
 * @return a pointer the data at x,y
 */
char *table_get(table_t *tbl, int x, int y)
{
	da_t **dap;
	char **result;

	if (x > tbl->cols) return NULL;
	if (y > da_len(tbl->rows)) return NULL;

	dap = da_get(tbl->rows, y);
	if (!dap || !*dap) return NULL;

	result = da_get(*dap,x);
	if (!result) return NULL;
	return *result;
}

/** @brief get integer data
 *
 * get data on the given x and y position in the table as integer
 *
 * @param tbl table to get the cell data from
 * @param x at which column or x position resides the data
 * @param y at which row or y position resides the data
 * @return integer value of cell x,y
 */
int table_get_int(table_t *tbl, int x, int y)
{
	char *tmp = table_get(tbl,x,y);
	if (!tmp) return 0;
	return atol(tmp);
}

/** @brief get number of rows
 *
 * get the current number of rows in the table
 *
 * @param tbl table to get the rows from
 * @return number of rows
 */
int table_get_rows(table_t *tbl)
{
	return da_len(tbl->rows);
}

/** @brief get number of columns
 *
 * get the current number of columns in the table
 *
 * @param tbl table to get the columns from
 * @return number of columns
 */
int table_get_cols(table_t *tbl)
{
	return tbl->cols;
}

/** @brief set data at pos
 *
 * set data at x,y in the table
 *
 * @param tbl table to get the columns from
 * @param x column or x position of the data to be set
 * @param y row or y position of the data to be set
 * @param data pointer to the data to be set
 * @return 0 on succes or -1 on failure
 */
int table_set(table_t *tbl, int x, int y, char *data)
{
	da_t **dap;

	if (x >= tbl->cols) return -1;
	if (y >= da_len(tbl->rows)) return -1;

	dap = da_get(tbl->rows, y);
	if (!dap) return -1;

	da_set(*dap,&data,x);
	return 0;
}

/** @brief delete a row
 *
 * delete a row from the table
 *
 * @param tbl table to get the columns from
 * @param y row or y position of row to delete
 * @return 0 on succes or -1 on failure
 */
int table_del_row(table_t *tbl, int y)
{
	int rows = da_len(tbl->rows);
	da_t **dap;

	if (y >= rows || y < 0) return -1;
	// NOTE : this does NOT cleanup the data inside
	// user table_traverse_row for that yourself
	dap = da_get(tbl->rows, y);
	da_rls(*dap);
	da_del(tbl->rows,y,1);
	return 0;
}

/** @brief insert a row
 *
 * insert a new row at pos y, no data is set yet only space is created
 *
 * @param tbl table to add the row to
 * @param y row or y position of row to add
 * @return 0 on succes or -1 on failure
 */
int table_ins_row(table_t *tbl, int y)
{
	int cols = tbl->cols;
	int rows = da_len(tbl->rows);
	da_t *nurow;

	if (y > rows || y < 0) return -1;
	nurow = da_ist(sizeof(char *),cols);
	da_ins(tbl->rows,&nurow,y,1);

	return 0;
}

/** @brief delete a column
 *
 * delete a column at pos x,
 *
 * @param tbl table to get the columns from
 * @param x column or x position of column to delete
 * @return 0 on succes or -1 on failure
 */
int table_del_col(table_t *tbl, int x)
{
	int y;
	da_t **dap;
	int rows = da_len(tbl->rows);

	if (x >= tbl->cols || x< 0) return -1;

	for (y=0; y< rows; y++) {
		dap = da_get(tbl->rows, y);
		da_del(*dap,x,1);
	}

	tbl->cols--;

	return 0;
}

/** @brief insert a col
 *
 * insert a new col at pos x, no data is set yet only space is created
 *
 * @param tbl table to add the columns to
 * @param x column or x position of column to add
 * @return 0 on succes or -1 on failure
 */
int table_ins_col(table_t *tbl, int x)
{
	int y;
	da_t **dap;
	char *nul=NULL;
	int rows = da_len(tbl->rows);

	if (x > tbl->cols || x< 0) return -1;

	for (y=0; y< rows; y++) {
		dap = da_get(tbl->rows, y);
		da_ins(*dap,&nul,x,1);
	}

	tbl->cols++;

	return 0;
}

/** @brief dump a table
 *
 * dump a table and it's content to the console
 *
 * @param tbl table to dump
 * @param dmp function to dump the data in a cell
 * @return 0 on succes or -1 on failure
 */
int table_dmp(table_t *tbl, dmp_fnc_t dmp)
{
	int x,y;
	int rows = da_len(tbl->rows);
	int cols = tbl->cols;

	printf("Table [%d row,%d cols]\n", rows, cols);

	for (y=0; y< rows; y++) {
		for (x=0; x< cols; x++) {
			char *dta = table_get(tbl,x,y);
			printf("[");
			dmp(0,dta,NULL);
			printf("]");
		}
		printf("\n");
	}
	printf("\n");
	return 0;
}

/** @brief traverse a row
 *
 * traverse all columns in a row in a table and perform fnc() on the data
 *
 * @param tbl table to use
 * @param y row number to traverse
 * @param fnc function to perform on the cell data
 * @return 0 on succes or -1 on failure
 */
void table_traverse_row(table_t *tbl, int y, void_fnc_t fnc)
{
	int x;
	int cols = tbl->cols;
	da_t **row;

	/* if (y >= rows || y < 0) return -1; */

	row = da_get(tbl->rows,y);
	for (x=0; x< cols; x++) {
		void *item = da_get(*row,x);
		fnc(item);
	}
}

/** @brief traverse a column
 *
 * traverse all rows in a column in a table and perform fnc() on the data
 *
 * @param tbl table to use
 * @param x column number to traverse
 * @param fnc function to perform on the cell data
 * @return 0 on succes or -1 on failure
 */
int table_traverse_col(table_t *tbl, int x, void_fnc_t fnc)
{
	int y,rows,cols;

	rows = da_len(tbl->rows);
	cols = tbl->cols;

	if (x >= cols || x < 0) return -1;

	for (y=0; y< rows; y++) {
		da_t **row = da_get(tbl->rows,y);
		void *item = da_get(*row,x);
		fnc(item);
	}
	return 0;
}

/** @brief traverse a table
 *
 * traverse all cell in a table in a table and perform fnc() on the data
 * the traversel order is all cells from row 0 from left to right and then 
 * row 1 down to the lasr row
 *
 * @param tbl table to traverse
 * @param fnc function to perform on the cell data
 */
void table_traverse(table_t *tbl, void_fnc_t fnc)
{
	int x,y,rows,cols;

	rows = da_len(tbl->rows);
	cols = tbl->cols;

	for (y=0; y< rows; y++) {
		da_t **row = da_get(tbl->rows,y);
		for (x=0; x< cols; x++) {
			void *item = da_get(*row,x);
			fnc(item);
		}
	}
}

/** @brief find columns in a table
 *
 * find all columns in a table where row Y contains the given data
 *
 @param tbl table to find in
 @param Y row to search
 @param data data to find
 @param cmp function to compare a cell with the data
 @param hdl userdata pointer for th ecompare function
 @return a new table containing the found rows
 */
table_t *table_find_cols(table_t *tbl,int Y, void *data, cmp_fnc_t cmp, void *hdl)
{
	int x,y,rows,cols;
	int nr=0;
	int res;

	// alwas alocate, kust return an empty table of needed
	table_t *fnd;

	rows = da_len(tbl->rows);
	cols = tbl->cols;

	if (Y >= rows || Y < 0) return NULL;

	fnd=table_ist(rows,0);

	da_t **row = da_get(tbl->rows,Y);

	for (x=0; x< cols; x++) {
		void *item = da_get(*row,x);
		res=cmp(item,data,hdl);

		if (!res) {
			table_ins_col(fnd, nr);
			for (y=0; y < rows; y++) {
				char *dta = table_get(tbl, x, y);
				table_set(fnd,nr,y,dta);
			}
			nr++;
		}
	}
	return fnd;
}

/** @brief find rows in a table
 *
 * find all rows in a table where column X contains the given data
 *
 @param tbl table to find in
 @param X column to search
 @param data data to find
 @param cmp function to compare a cell with the data
 @param hdl userdata pointer for th ecompare function
 @return a new table containing the found rows
 */
table_t *table_find_rows(table_t *tbl,int X, void *data, cmp_fnc_t cmp, void *hdl)
{
	int x,y,rows,cols;
	int nr=0;
	int res;

	// alwas alocate, kust return an empty table of needed
	table_t *fnd;

	rows = da_len(tbl->rows);
	cols = tbl->cols;

	if (X >= cols || X < 0) return NULL;

	fnd=table_ist(0,cols);

	for (y=0; y< rows; y++) {
		da_t **row = da_get(tbl->rows,y);
		void *item = da_get(*row,X);
		res=cmp(item,data,hdl);
		if (!res) {
			table_ins_row(fnd, nr);
			for (x=0; x < cols; x++) {
				char *dta = table_get(tbl, x, y);
				table_set(fnd,x,nr,dta);
			}
			nr++;
		}
	}
	return fnd;
}


/** @brief sort a table
 *
 * sort a table's rows given a sort function, the sort function get's a
 * handle to the row and should be able to get the columndata needed for sorting
 * the hdl pointer is passed directly to the udata parameter of the compare 
 * function.
 *
 * @param tbl table to sort
 * @param cmp function to compare  a row
 * @param hdl userdata pointer for th ecompare function
 */
void table_sort(table_t *tbl,cmp_fnc_t cmp, void *hdl)
{
	if (!tbl || !tbl->rows) return;
	da_sort(tbl->rows,cmp,hdl);
}

/** @brief release table
 * 
 * free up a table structure, leave data inside intact
 * if you want to release the internal data, provide a release function to 
 * one of the traversal function first.
 * @param tbl table to release
 */
void table_rls(table_t *tbl)
{
	int y;
	int rows;
	da_t **row;

	rows = da_len(tbl->rows);

	for (y=0; y< rows; y++) {
		row = da_get(tbl->rows,y);
		da_rls(*row);
	}
	da_rls(tbl->rows);

	free(tbl);
}

static void voiddmp(void *a)
{
	char *str = *(char **)a;

	printf("%s", str);
}

static int table_test_add_del(int num,int len)
{
	int x;
	int y;
	char *dta;

	table_t *tbl; 

	num=5;
	tbl=table_ist(num,num);

	table_ins_row(tbl, 0);
	for (x=0; x < num; x++) {
		for (y=0; y < num; y++) {
			dta = strrand(10);
			if (dta) printf("%s\n", dta);
			table_set(tbl,x,y,dta);
		}
	}

	// add an empty row
	table_ins_row(tbl, 1);
	table_dmp(tbl,str_dmp);
	table_traverse_row(tbl,4,voiddmp);
	table_traverse_col(tbl,1,strp_rls);
	table_del_col(tbl,1);
	table_dmp(tbl,str_dmp);

	// cleanup 
	table_traverse(tbl,strp_rls);
	// release again !!
	table_rls(tbl);

	return 1;
}

static int table_test_find(int num,int len)
{
	int x;
	int y;
	char *dta;

	table_t *tbl; 
	table_t *find; 

	num=5;
	tbl=table_ist(num,num);

	char *strdata[10] = { 
		"jopie",
		"pietje",
		"keessie",
		"klaassie",
		"jantje",
		"sjeffie",
		"henkie",
		"edje",
		"robbie",
		"harrie"
	};

	for (x=0; x < num; x++) {
		for (y=0; y < num; y++) {
			int wellukku = intrand(0,9);
			dta = strdup(strdata[wellukku]);
			table_set(tbl,x,y,dta);
		}
	}

	table_dmp(tbl,str_dmp);

	// now get a new table on row
	find = table_find_rows(tbl, 0, &strdata[6], strp_cmp, NULL);
	table_dmp(find,str_dmp);
	table_rls(find);

	// now get a new table on row
	find = table_find_cols(tbl, 0, &strdata[6], strp_cmp, NULL);
	table_dmp(find,str_dmp);
	table_rls(find);

	// cleanup 
	table_traverse(tbl,strp_rls);
	// release again !!
	table_rls(tbl);

	return 1;
}


/** @brief test a table
 * internal test function 
 @param num width of the test
 @param len length of the test
 */
int table_test(int num,int len)
{
	int ret=1;

	ret = table_test_add_del(num,len);
	ret |= table_test_find(num,len);

	/* printf("duration : %d\n", (int)(t1-t)); */
	return ret;
}
