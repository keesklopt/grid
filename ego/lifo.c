/** @file
 * 
	lifo, last-in-first-out or stack implementation

	a lifo is a stack data structure where you can push object onto the
	top of the stack only, and pop them off the top only as well.

 */
#include <ego.h>

/** @brief instantiate stack
 *
    create a new stack of op object types
 @param op object type that can be pused on or off this stack
 @return pointer to a new lifo/stack object
 */
lifo_t *lifo_ist(object_t *op)
{
	lifo_t *lp = (lifo_t *)calloc(sizeof(lifo_t),1);

	lp->lst = dlst_ist(op);

	return lp;
}

/** @brief release stack
 *
    release a stack and it'r resources
 @param lp pointer to a lifo/stack object to release
 */
void lifo_rls(lifo_t *lp)
{
	dlst_rls(lp->lst);
	free(lp);
}

/** @brief push object
 *
    push an object on top of the stack
 @param lp to a lifo/stack object to use
 @param what pointer to the object data to push
 */
void lifo_push(lifo_t *lp, void *what)
{
	dlst_add(lp->lst, what);
}
 
/** @brief dump stack
 *
    dump the stack bottom first
 @param lp to a lifo/stack object to use
 */
void lifo_dmp(lifo_t *lp)
{
	dlst_dmp(lp->lst);
}

/** @brief get first entry 
 *
    get the first or bottom entry of the stack
 @param lp to a lifo/stack object to use
 @return pointer to the bottom element
 */
void *lifo_first(lifo_t *lp)
{
	lp->cur = dlst_get(lp->lst,0);
	if (!lp->cur) return NULL;
	return lp->cur->data;
}

/** @brief get next stack element
 *
	get the next object on a stack relative to the last lifo_first or lifo_next
	function call
 @param lp to a lifo/stack object to use
 @return pointer to the next element
 */
void *lifo_next(lifo_t *lp)
{
	if (!lp->cur) return NULL;
	lp->cur = lp->cur->next;
	if (!lp->cur) return NULL;
	return lp->cur->data;
}

/** @brief pop an element
 *
	pop the topmost element from the stack, so also remove it
 @param lp to a lifo/stack object to use
 @return pointer to the top element
 */
void *lifo_pop(lifo_t *lp)
{
	void *item;

	if (!lp) return NULL;

	item = dlst_get(lp->lst, LST_LAST);
	dlst_del(lp->lst, NULL, LST_LAST);
	return item;
}

/** @brief get the top an element
 *
    get the last or top entry of the stack without removing it
 @param lp to a lifo/stack object to use
 @return pointer to the top element
 */
void *lifo_top(lifo_t *lp)
{
	void *item;

	if (!lp)return NULL;

	item = dlst_get(lp->lst, LST_LAST);
	return item;
}

/** @brief get stack length
 *
	or depth as you want, get the total number of elements on the stack
 @param lp to a lifo/stack object to use
 @return length of the stack
 */
int lifo_len(lifo_t *lp)
{
	if (!lp || !lp->lst) return 0;

	return dlst_len(lp->lst);
}

/** @brief test a stack
 *
	internal test function
 */
int lifo_test(int len, int wid)
{
	lifo_t *lp;
	int ret=1,t;

	lp = lifo_ist(&int_object);

	for (t=0; t< len; t++) {
		lifo_push(lp, &t);
		/* lifo_dmp(lp); */
	}
	for (t=0; t< len; t++) {
		lifo_pop(lp);
		/* lifo_dmp(lp); */
	}

	lifo_rls(lp);

	return ret;
}
