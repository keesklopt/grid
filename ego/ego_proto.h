/* mat.c */
extern mat_t *mat_ist(int rows, int cols);
extern void mat_dmp(mat_t *mp);
extern void mat_array(char *buf, mat_t *mp);
extern void mat_dmp_array(mat_t *mp);
extern void mat_set(mat_t *mp, int y, int x, int32_t val);
extern int32_t mat_get(mat_t *mp, int y, int x);
extern void mat_rls(mat_t *mp);
extern int mat_test(int wid, int len);
/* web.c */
extern int web_test(int wid, int len);
/* xml.c */
extern xml_t *xml_ist(io_t *iop);
extern int xml_file(xml_t *xp, int c);
extern int xml_whitespace(xml_t *xp, int c);
extern void xml_tokenize(xml_t *xp);
extern int xml_test(int w, int h);
/* string.c */
extern int ego_iswhitespace(int c);
extern int ego_isdigit(int c);
extern int ego_isprint(int c);
extern int baselength(long long num, long base, long mod, long sign);
extern char *ego_strcat(char *dst, char *src);
extern char *ego_strcat_free(char *dst, char *src);
extern char *ego_strdup(const char *orig);
extern int floatlength(double lang, long base, long mod, long sign);
extern int ego_strlenf(const char *fmt, ...);
extern char *ego_vsprintf(const char *fmt, va_list argptr);
extern char *ego_sprintf(const char *fmt, ...);
extern void ego_strip(char *str);
extern char *html_unescape(char *str);
extern void ego_plus_2_space(char *str);
extern int string_test(int w, int l);
/* socket.c */
extern void socket_rls(socket_t *sp);
extern socket_t *socket_ist_ext(int family, int type, int proto);
extern socket_t *socket_ist(int type);
extern int socket_reuse(socket_t *sp);
extern int socket_setpeerbyname(socket_t *sp, char *name, short port);
extern int socket_sethostbyname(socket_t *sp, char *name, short port);
extern int socket_bind(socket_t *sp, short port);
extern char *ip4_string(struct sockaddr_in *saddr);
extern int socket_connect(socket_t *sp, char *name, short port);
extern void address_dmp(struct sockaddr_in sa);
extern void socket_dmp(socket_t *sp);
extern socket_t *socket_accept(socket_t *sp);
extern int socket_listen(socket_t *sp);
extern int socket_test(int len, int num);
/* date.c */
extern date_t *date_ist(time_t stamp);
extern void date_set_now(date_t *dp);
extern date_t *date_ist_now(void);
extern struct tm *date_tm(date_t *dp);
extern time_t date_time(date_t *dp);
extern void date_add(date_t *which, date_t *what);
/* bio.c */
extern void bio_rls(bio_t *bp);
extern bio_t *bio_ist(io_t *iop, int blocksize, int lookback);
extern size_t bio_read(bio_t *iop, char *buf, size_t len, size_t nmemb);
extern size_t bio_write(io_t *iop, char *buf, size_t len, size_t nmemb);
extern void bio_dmp(bio_t *bp);
extern int bio_test(int len, int wid);
/* ego.c */
extern int is_numeric(char c);
extern int is_alpha(char c);
extern int is_whitespace(char c);
extern int std_cmp(const void *L, const void *R, void *xtra);
extern int str_cmp(const void *L, const void *R, void *xtra);
extern int strp_cmp(const void *L, const void *R, void *xtra);
extern int int_abuse_cmp(const void *left, const void *right, void *xtra);
extern int float_cmp(const void *left, const void *right, void *xtra);
extern int int_cmp(const void *left, const void *right, void *xtra);
extern int int32_cmp(const void *left, const void *right, void *xtra);
extern int int64_cmp(const void *left, const void *right, void *xtra);
extern int intp_cmp(const void *left, const void *right, void *xtra);
extern int test_cmp(const void *left, const void *right, void *xtra);
extern void int_abuse_dmp(intptr_t wid, const void *data, void *nop);
extern void time_t_dmp(intptr_t wid, const void *a, void *b);
extern void int64_dmp(intptr_t wid, const void *a, void *b);
extern void int_dmp(intptr_t wid, const void *a, void *b);
extern void float_dmp(intptr_t wid, const void *a, void *b);
extern void char_dmp(intptr_t wid, const void *a, void *b);
extern void char_abuse_dmp(intptr_t wid, const void *a, void *b);
extern void intp_dmp(intptr_t wid, const void *a, void *b);
extern void str_dmp(intptr_t bytes, const void *a, void *b);
extern void strp_dmp(intptr_t bytes, const void *a, void *b);
extern void strp_rls(void *a);
extern void str_rls(void *a);
extern void anything_rls(void *data);
extern void test_dmp(int bytes, const void *a, const void *b);
extern void stddmpfnc(intptr_t wid, const void *elm, void *usr);
extern void ego_exit(void);
extern int ego_init(int loglevel);
extern void time_sleep(int sec);
extern int intrand(int lo, int hi);
extern char *strrand(int len);
extern char *str_sep(char **stringp, const char *delim);
extern void randist(void);
extern tuple_t *tuple_ist(const char *k, const char *v);
extern char *bit_string_32(int32_t base);
extern char *bit_string_16(int16_t base);
extern char *bit_string_8(int8_t base);
extern int ego_test(int len, int wid);
/* da.c */
extern void ego_qsort(void *a, size_t n, intptr_t es, cmp_fnc_t cmp, void *hdl);
extern int da_issorted(da_t *dap, cmp_fnc_t cmp, void *udata);
extern void da_data(void);
extern void da_dmp(da_t *dap, dmp_fnc_t dmp, void *udata);
extern void *ego_bsearch(register const void *key, const void *base0, size_t nmemb, register size_t size, cmp_fnc_t compar, void *udata);
extern void *ego_bisearch(register const char *key, const char *base0, size_t nmemb, register size_t size, cmp_fnc_t compar, void *udata);
extern intptr_t da_insertpos(da_t *dap, void *key, cmp_fnc_t cmp, void *hdl);
extern int da_ins_sorted(da_t *dap, void *data, cmp_fnc_t cmp, void *hdl);
extern intptr_t da_pos(da_t *dap, void *key, cmp_fnc_t cmp, void *hdl);
extern int da_sort(da_t *dap, cmp_fnc_t cmp, void *hdl);
extern da_t *da_dup(da_t *orig);
extern void da_init(da_t *dap, int wid, int len);
extern da_t *da_ist(int wid, int len);
extern void da_rls(da_t *dap);
extern int da_len(da_t *dap);
extern intptr_t da_resize(da_t *dap, unsigned int len);
extern void da_cpy(void *dst, da_t *src, int from, int to);
extern void *da_get(da_t *dap, intptr_t pos);
extern intptr_t da_set(da_t *dap, void *data, intptr_t pos);
extern int da_swap(da_t *dap, int a, int b);
extern int da_del(da_t *dap, int pos, int len);
extern int da_add(da_t *dap, void *data);
extern int da_ins(da_t *dap, void *data, int pos, int len);
extern int da_test(int num, int len);
/* err.c */
extern int err_init(void);
extern void err_exit(void);
extern void err_set_string(int num, char *fmt, ...);
extern char *err_string(void);
extern int err_errno(void);
extern int err_set_to(errspace_t *esp, int ernum);
extern int err_set(int ernum);
extern int err_register(char *message);
extern void err_exit_if(int what, char *msg);
/* io.c */
extern int io_init(void);
extern int io_open(io_t *iop, char *hdl, int flags);
extern void io_close(io_t *iop);
extern int io_wait(io_t *iop, int which, int timeout);
extern size_t io_read_exactly(io_t *iop, char *buf, size_t len);
extern size_t io_read(io_t *iop, char *buf, size_t len);
extern int io_fgetc(io_t *iop);
extern char *io_fgets(char *buf, size_t len, io_t *iop);
extern size_t io_write(io_t *iop, char *buf, size_t len);
extern void io_rewind(io_t *iop);
extern int io_lseek(io_t *iop, long pos, int whence);
extern int io_ftell(io_t *iop);
extern void io_rls(io_t *iop);
extern io_t *io_ist(int type, void *location, char *rw, unsigned int size);
extern int io_vprintf(io_t *iop, const char *fmt, va_list argptr);
extern int io_printf(io_t *iop, const char *fmt, ...);
extern void io_setcolor(io_t *iop, int color);
extern io_t *io_ist_string(char *str);
extern int io_wr(io_t *dst, io_t *src);
extern int io_test(int size, int num);
/* log.c */
extern int logsetlevel(unsigned int lvl);
extern void log_exit(void);
extern int log_init(int level, io_t *err, io_t *warn, io_t *debug);
extern int log_vprintf(log_t *logp, const char *fmt, va_list argptr);
extern int log_printf(log_t *logp, char *fmt, ...);
extern int log_error(int level);
extern void log_setcolor(int level, int color);
extern void log_ok(int depth, char *msg, int ok);
extern void log_rls(log_t *lp);
extern log_t *log_ist(io_t *iop, int level);
extern int log_test_levels(int size, int num);
extern int log_test_files(int size, int num);
extern int log_test(int size, int num);
/* buf.c */
extern buf_t *buf_ist(int initialsize);
extern char *buf_strdup(buf_t *orig);
extern buf_t *buf_dup(buf_t *orig);
extern intptr_t buf_read(buf_t *stream, void *ptr, intptr_t nbytes);
extern intptr_t buf_write(buf_t *stream, void *ptr, intptr_t nbytes);
extern size_t buf_fread(void *ptr, size_t size, size_t nmemb, buf_t *stream);
extern size_t buf_fwrite(void *ptr, size_t size, size_t nmemb, buf_t *stream);
extern int buf_fseek(buf_t *stream, intptr_t offset, int whence);
extern intptr_t buf_ftell(buf_t *bp);
extern void buf_rewind(void *stream);
extern void buf_dmp(buf_t *bp);
extern int buf_clear(buf_t *b);
extern int buf_reset(buf_t *b);
extern intptr_t buf_resize(buf_t *b, intptr_t bytes);
extern int buf_stretch(buf_t *b, intptr_t offset, intptr_t bytes);
extern int buf_shrink(buf_t *b, intptr_t offset, intptr_t bytes);
extern intptr_t buf_pos(buf_t *b);
extern intptr_t buf_len(buf_t *b);
extern int buf_del(buf_t *buf, int off, int bytes);
extern int buf_ins(buf_t *buf, intptr_t off, intptr_t bytes, void *data);
extern intptr_t buf_fputc(char val, buf_t *buf);
extern intptr_t buf_putc(char val, buf_t *buf);
extern intptr_t buf_add(buf_t *buf, char *txt);
extern intptr_t buf_fputs(char *txt, buf_t *buf);
extern char *buf_get(buf_t *buf, intptr_t offs);
extern void buf_get_str(char *dst, buf_t *src, intptr_t offset, intptr_t bytes);
extern int buf_get_int(buf_t *buf, intptr_t offs);
extern int buf_set_int(buf_t *buf, intptr_t offs, int bytes, intptr_t val);
extern intptr_t buf_set_str(buf_t *buf, intptr_t offs, intptr_t len, char *val);
extern int buf_set(buf_t *buf, int offs, char val);
extern int buf_set_bits(buf_t buf, bitseq_t *bs);
extern char *buf_ptr(buf_t *dm, int pos);
extern int buf_fill(buf_t *b, unsigned char def);
extern void buf_rls(buf_t *b);
extern int buf_test_dup(int num, int len);
extern int buf_test_randfill(buf_t *bufp, int min, int max);
extern int buf_test_data(int num, int len);
extern int buf_test_strdup(int num, int len);
extern int buf_test_add_del(int num);
extern int buf_test(int num, int len);
/* dlist.c */
extern int dlst_get_pos(dlst_t *lst, ditem_t *li);
extern dlst_t *dlst_dup(dlst_t *lst);
extern int dlst_len(dlst_t *lst);
extern ditem_t *dlst_get_item(dlst_t *lst, int pos);
extern ditem_t *dlst_find(dlst_t *lst, void *data, cmp_fnc_t alternate);
extern ditem_t *dlst_insertfind(dlst_t *lst, void *data);
extern void dlst_item_rls(ditem_t *li);
extern ditem_t *dlst_item(dlst_t *lst, int where);
extern void dlst_del(dlst_t *lst, ditem_t *old, int where);
extern ditem_t *dlst_ins(dlst_t *dp, void *data);
extern ditem_t *dlst_ins_at(dlst_t *lst, ditem_t *old, void *data, int where);
extern int dlst_traverse(dlst_t *lst, ditem_fnc_t fnc, void *udata);
extern void ditem_rls(dlst_t *lst, ditem_t *dit, void *udata);
extern void dlst_gut(dlst_t *dp, object_t *op);
extern ditem_t *dlst_add(dlst_t *lst, void *data);
extern int dlst_join(dlst_t *a, dlst_t *b);
extern dlst_t *dlst_ist(object_t *op);
extern void dlst_dmp_r(dlst_t *lst, ditem_t *li, dmp_fnc_t dmpfnc);
extern void dlst_dmp_l(dlst_t *lst, ditem_t *li);
extern void dlst_dmp_double(dlst_t *lst);
extern void dlst_dmp(dlst_t *lst);
extern void dlst_empty(dlst_t *lst);
extern void dlst_rls(dlst_t *lst);
extern ditem_t *dlst_first(dlst_t *dp);
extern ditem_t *dlst_last(dlst_t *dp);
extern ditem_t *dlst_next(ditem_t *li);
extern ditem_t *dlst_prev(ditem_t *li);
extern void *dlst_get(dlst_t *lst, long arg);
extern void dlst_merge(dlst_t *dp, ditem_t *from, ditem_t *to, int len, void *udata);
extern void dlst_sort(dlst_t *dp, object_t *op, void *hdl);
extern int dlst_issorted(dlst_t *dp);
extern int dlst_test(int num, int wid);
/* list.c */
extern void ego_list_add(ego_list_t **position, void *item);
extern void ego_list_del(ego_list_t **position);
extern void ego_list_dmp(ego_list_t *lp, dmp_fnc_t how);
extern ego_list_t **ego_list_find_insertpos(ego_list_t **lp, void *item, cmp_fnc_t where);
extern ego_list_t **ego_list_find_deletepos(ego_list_t **lp, void *item, cmp_fnc_t where);
extern int ego_list_test(int wid, int len);
/* iv.c */
extern void iv_rls(void *ivp);
extern void iv_dmp(intptr_t wid, const void *data, void *udata);
extern int iv_cmp(const void *left, const void *right, void *udata);
extern void ivset_dmp(intptr_t wid, const void *data, void *xtra);
extern int ivset_cmp(const void *a, const void *b, void *xtra);
extern ivset_t *ivset_dup(ivset_t *orig);
extern void ivset_merge(ivset_t *a, ivset_t *b);
extern void ivset_union(ivset_t *a);
extern void ivset_intersection(ivset_t *a);
extern ivset_t *ivset_ist(object_t *op);
extern void ivset_gut(void *opaq, object_t *op);
extern void ivset_debone(ivset_t *isp);
extern void ivset_rls(void *opaq);
extern int ivset_del(ivset_t *isp, int pos);
extern int ivset_ins(ivset_t *isp, void *l, void *r);
extern int ivset_len(ivset_t *isp);
extern int iv_test(int num, int wid);
/* idx.c */
extern void idx_number(idx_t *ip);
extern idx_t *idx_dup(idx_t *orig, void *udata);
extern idx_t *idx_ist(int len, cmp_fnc_t cmp, void *udata);
extern int idx_issorted(idx_t *ip);
extern void idx_invert(idx_t *ip);
extern void idx_rls(idx_t *ip);
extern int ascending_srt(const void *left, const void *right, void *udata);
extern int descending_srt(const void *left, const void *right, void *udata);
extern int idx_insertpos(idx_t *ip, void *data);
extern int idx_pos(idx_t *ip, void *data);
extern void idx_sort(idx_t *ip);
extern int idx_index(idx_t *ip, int pos);
extern int idx_inv(idx_t *ip, int pos);
extern int idx_dmp_da(idx_t *ip, dmp_fnc_t dmp, da_t *dap);
extern int idx_recalc_added(idx_t *ip, unsigned int pos);
extern int idx_recalc_deleting(idx_t *ip, int pos);
extern int idx_reindex(idx_t *ip);
extern int idx_dmp(idx_t *ip);
extern int idx_test(int num, int wid);
/* heap.c */
extern eheap_t *eheap_ist(int wid, int k, int len, cmp_fnc_t cmp);
extern void eheap_rls(eheap_t *hp);
extern int eheap_swap(eheap_t *hp, int a, int b);
extern int eheap_len(eheap_t *hp);
extern int eheap_size(eheap_t *hp, int depth);
extern int eheap_nofparents(eheap_t *hp);
extern int eheap_parent(eheap_t *hp, int node);
extern int eheap_child(eheap_t *hp, int node, unsigned int which);
extern int eheap_level(eheap_t *hp, int node);
extern intptr_t eheap_set(eheap_t *hp, void *elm, int loc);
extern int eheap_add(eheap_t *hp, void *elm);
extern int eheap_isleaf(eheap_t *hp, int pos);
extern int eheap_isparent(eheap_t *hp, int pos);
extern int eheap_del(eheap_t *hp, int pos);
extern void *eheap_get(eheap_t *hp, int loc);
extern void eheap_up(eheap_t *hp, int pos);
extern int eheap_issorted(eheap_t *hp);
extern int eheap_isheap(eheap_t *hp);
extern void eheap_down(eheap_t *hp, unsigned int pos);
extern void eheap_sortpartial(eheap_t *hp);
extern void eheap_sort(eheap_t *hp, cmp_fnc_t cmp);
extern int eheap_treedmp_r(eheap_t *hp, unsigned int root, unsigned int lvl, dmp_fnc_t dmp);
extern int eheap_treedmp(eheap_t *hp, dmp_fnc_t dmp);
extern int eheap_dmp(eheap_t *hp, dmp_fnc_t dmp);
extern int eheap_test(int num, int wid);
/* ida.c */
extern idx_t *ida_index(ida_t *idap, int n);
extern int ida_idx_get_pos(ida_t *idap, int idx, int n);
extern void *ida_idx_get(ida_t *idap, int idx, int n);
extern void ida_rls(ida_t *idap);
extern ida_t *ida_ist(int wid, int len);
extern int ida_nidx(ida_t *idap);
extern int ida_issorted(ida_t *idap);
extern idx_t *ida_add_index(ida_t *idap, idx_t *ip);
extern int ida_len(ida_t *idap);
extern int ida_dmp(ida_t *idap, dmp_fnc_t dmp, void *udata);
extern int ida_set_sorted(ida_t *idap, int sorted);
extern int ida_set(ida_t *idap, void *data, int pos);
extern void *ida_get(ida_t *idap, unsigned int pos);
extern int ida_add(ida_t *idap, void *data);
extern int ida_del(ida_t *idap, int pos);
extern int ida_ins(ida_t *idap, void *data, int pos);
extern ida_t *ida_dup(ida_t *idap);
extern int ida_test(int num, int wid);
/* tree.c */
extern tnode_t *tnode_ist(void *dta, int wid);
extern tnode_t *tnode_dup(tnode_t *tp, int wid);
extern void tnode_rls(tnode_t *nd);
extern tnode_t *tnode_get(tnode_t *parent, int pos);
extern int tnode_set(tnode_t *where, tnode_t *what, int pos);
extern int tnode_link(tnode_t **where, tnode_t *what, int pos);
extern void tree_tnode_rls(tree_t *tp);
extern void tree_rls(tree_t *tp);
extern tree_t *tree_ist(int wid);
extern int tree_node_nchildren(tree_t *tp, tnode_t *node);
extern int tree_ins_pure(tree_t *tp, tnode_t **where, tnode_t *what, int pos);
extern int tree_ins(tree_t *tp, tnode_t **where, tnode_t *what, int pos);
extern int tree_traverse_pre(tree_t *tp, tnode_t *np, tnode_fnc_t fnc, void *udata, int level);
extern int tree_traverse_post(tree_t *tp, tnode_t *np, tnode_fnc_t fnc, void *udata, int level);
extern int tree_traverse(tree_t *tp, tnode_t *np, tnode_fnc_t fnc, void *udata, int level);
extern void tree_strip(tree_t *tp, rls_fnc_t rls);
extern void tree_dmp(tree_t *tp, dmp_fnc_t dmp);
extern da_t *tree_flatten(tree_t *tp);
extern tree_t *tree_dup(tree_t *tp);
extern void stree_strip(stree_t *stp, rls_fnc_t rls);
extern void stree_rls(stree_t *tp);
extern void stree_dmp(stree_t *tp, dmp_fnc_t dmp);
extern stree_t *stree_ist(int wid, cmp_fnc_t cmp, unsigned int max);
extern int tnode_pos(stree_t *tp, tnode_t **root, tnode_t *nd);
extern void tree_swap_data(tree_t *tp, tnode_t *a, tnode_t *b);
extern int stree_add(stree_t *tp, tnode_t **where, tnode_t *what);
extern int stree_test(int num, int len, int k);
extern int tree_test_data(int num, int len, int k);
extern int utree_test(int num, int len);
extern int tree_test(int num, int len);
/* rbuf.c */
extern void rbuf_rls(rbuf_t *rbp);
extern rbuf_t *rbuf_ist(int size);
extern void rbuf_eof(rbuf_t *rbf);
extern int rbuf_filled(rbuf_t *rbf);
extern int rbuf_len(rbuf_t *rbf);
extern int rbuf_empty(rbuf_t *rbf);
extern void rbuf_dmp(rbuf_t *rbp, dmp_fnc_t dmp);
extern int rbuf_read(rbuf_t *stream, void *p, size_t nbytes);
extern int rbuf_write(rbuf_t *stream, void *ptr, size_t nbytes);
extern int rbuf_test_exhaust(int len, int rlen, int r, int w);
extern int rbuf_test(int len, int wid);
/* lifo.c */
extern lifo_t *lifo_ist(object_t *op);
extern void lifo_rls(lifo_t *lp);
extern void lifo_push(lifo_t *lp, void *what);
extern void lifo_dmp(lifo_t *lp);
extern void *lifo_first(lifo_t *lp);
extern void *lifo_next(lifo_t *lp);
extern void *lifo_pop(lifo_t *lp);
extern void *lifo_top(lifo_t *lp);
extern int lifo_len(lifo_t *lp);
extern int lifo_test(int len, int wid);
/* fifo.c */
extern fifo_t *fifo_ist(object_t *op);
extern void fifo_rls(fifo_t *fp);
extern void fifo_push(fifo_t *fp, void *what);
extern void fifo_dmp(fifo_t *fp);
extern void *fifo_first(fifo_t *fp);
extern void *fifo_next(fifo_t *fp);
extern void *fifo_pop(fifo_t *fp);
extern int fifo_len(fifo_t *fp);
extern intptr_t fifo_test(int len, int wid);
/* tlv.c */
extern tlv_t *tlv_ist(int t, int l, void *v);
extern void tlv_rls_data(tlv_t *tp);
extern void tlv_rls(tlv_t *tp);
extern tlv_t *tlv_recv(io_t *iop, int timeout);
extern int tlv_send(io_t *iop, tlv_t *tp);
extern int tlv_test(int n, int w);
/* unix.c */
/* aa.c */
extern aa_t *aa_ist(int l, int r, cmp_fnc_t cmp);
extern aa_t *aa_ist_init(int l, int r, int initial, cmp_fnc_t cmp);
extern int aa_len(aa_t *aap);
extern void *aa_val(aa_t *aap, void *key);
extern int aa_pos(aa_t *aap, void *key);
extern int aa_del(aa_t *aap, void *key);
extern aa_item_t *aa_get(aa_t *aap, intptr_t pos);
extern void aa_strip(aa_t *aap, rls_fnc_t rlskey, rls_fnc_t rlsval);
extern void aa_rls(aa_t *aap);
extern int aa_set_at(aa_t *aap, int32_t p, void *a, void *b);
extern int aa_set(aa_t *aap, void *a, void *b);
extern int aa_add(aa_t *aap, void *a, void *b);
extern void aa_item_dmp(intptr_t wid, const void *data, void *udata);
extern void aa_dmp(aa_t *aap, dmp_fnc_t dmpa, dmp_fnc_t dmpb);
extern int aa_test(int len, int wid);
/* graph.c */
extern int edge_cmp(const void *left, const void *right, void *udata);
extern void edge_rls(void *opaque);
extern void node_dmp(intptr_t wid, const void *opaq, void *xtra);
extern void edge_dmp(intptr_t wid, const void *opaq, void *xtra);
extern int node_cmp(const void *left, const void *right, void *udata);
extern void node_rls(void *data);
extern void nodedup(dlst_t *dp, ditem_t *dip, void *udata);
extern void edgefill(dlst_t *gp, ditem_t *dip, void *udata);
extern void edgedup(dlst_t *dp, ditem_t *dip, void *udata);
extern void erls(void *data);
extern void nrls(void *data);
extern graph_t *graph_ist(object_t *nodes, object_t *edges);
extern void graph_gut(graph_t *gp, object_t *nop, object_t *eop);
extern void graph_rls(graph_t *gp);
extern void graph_dmp(graph_t *gp);
extern eedge_t *edge_ist(void *udata);
extern void e_dmp(eedge_t *np, dmp_fnc_t dmp);
extern enode_t *node_ist(void *udata);
extern eedge_t *edge_dup(eedge_t *orig);
extern enode_t *node_dup(enode_t *orig, aa_t *tmp);
extern enode_t *graph_get_node(graph_t *gp, int t);
extern void graph_add_node(graph_t *gp, enode_t *np);
extern void graph_add_edge(graph_t *gp, enode_t *from, enode_t *to, void *udata);
extern graph_t *graph_dup(graph_t *orig);
extern int graph_join(graph_t *a, graph_t *b);
extern enode_t *graph_get_entry(graph_t *gp);
extern enode_t *graph_get_exit(graph_t *gp);
extern void graph_set_entry(graph_t *gp, enode_t *np);
extern void graph_set_exit(graph_t *gp, enode_t *np);
extern int graph_cat(graph_t *a, graph_t *b, void *data);
extern int graph_test(int wid, int len);
/* permute.c */
extern void prm_rls(prm_t *prm);
extern prm_t *prm_ist(int elmwid, int wid);
extern int prm_add(prm_t *prm, void *elm);
extern void *Number(prm_t *prm, prm_fnc_t fnc, void *data);
extern void Combine(prm_t *prm, prm_fnc_t fnc, void *data);
extern void Permute(prm_t *prm, prm_fnc_t fnc, void *data);
extern void prm_func(prm_t *prm, prm_fnc_t fnc, void *data);
extern int prm_set(prm_t *prm, int tp);
extern void prm_dmp(prm_t *prm, dmp_fnc_t fnc);
extern int print(void **res, int n, void *data);
extern int prm_test(int num, int wid);
/* object.c */
extern void nop_rls(void *data);
extern int nop_cmp(const void *a, const void *b, void *data);
extern void nop_dmp(intptr_t wid, const void *a, void *data);
extern object_t *object_ist(char *name, rls_fnc_t rls, cmp_fnc_t cmp, dmp_fnc_t dmp, void *udata);
extern object_t *object_dup(object_t *orig);
extern void object_rls(object_t *op);
/* data.c */
extern intptr_t alignsize(intptr_t a, intptr_t b);
extern type_t *type_get(int pos);
extern type_t *struct_type_get(type_t *tp, intptr_t mem_pos);
extern void instance_stretch(instance_t *i, intptr_t offset, intptr_t bytes);
extern intptr_t gen_traverse_struct_d(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc);
extern int gen_traverse_ptr_d(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc);
extern int gen_traverse_ptr_b(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc);
extern int gen_traverse_arr_d(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc);
extern intptr_t cstr_traverse_d(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc);
extern intptr_t gen_traverse_d(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc);
extern int basetag_traverse_d(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t dummy, gfunc_t fnc);
extern intptr_t gen_traverse_b(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t dummy, gfunc_t fnc);
extern int type_cmp(const void *a, const void *b, void *c);
extern int ptr_cmp(const void *a, const void *b, void *c);
extern void wlk_rls(instance_t *i, type_t *tp);
extern int offset_cmp(const void *a, const void *b, void *c);
extern void add_pointer(instance_t *i, int offset, void *ptr);
extern ptr_t *find_pointer(instance_t *ip, int offset, void *ptr);
extern int wlk_unpack(instance_t *i, type_t *tp, char **patient, char *donor);
extern type_t *type_ist(char *name, int szof, int count, char *type, wbfunc_t wlk_b, wdfunc_t wlk_d);
extern void gen_dmp(instance_t *i, char *wlk, type_t *tp);
extern int type_dmp(void);
extern void instance_dmp(instance_t *i, int level);
extern type_t *type_find(char *name);
extern int type_reg(type_t *tp);
extern type_t *type_reg_arr(char *name, char *base, int count, wbfunc_t wlk_b, wdfunc_t wlk_d);
extern type_t *type_reg_ptr(char *name, char *base, wbfunc_t wlk_b, wdfunc_t wlk_d);
extern type_t *type_reg_base(char *name, int szof, wbfunc_t wlk_b, wdfunc_t wlk_d);
extern int type_pos(char *name);
extern int instance_reindex(instance_t *i);
extern intptr_t dump(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t count);
extern intptr_t unpack(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t count);
extern intptr_t pack(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t count);
extern intptr_t instance_pos(instance_t *i);
extern int instance_pack(instance_t *i, char *donor, int count);
extern int instance_unpack(instance_t *i, char *patient);
extern instance_t *instance_ist(char *mytype);
extern intptr_t type_resolve_size(type_t *tp);
extern int type_calc(intptr_t alignment);
extern void instance_rls(instance_t *i);
extern void instance_dump(instance_t *i);
extern int type_add_member(type_t *tp, member_t memb);
extern int type_add(type_t *tp, char *tagname, char *name);
extern void member_rls(member_t m);
extern void type_rls(type_t *tp);
extern void type_unreg(type_t *tp);
extern void cleanup(void);
extern intptr_t get_count(char *ptr);
extern intptr_t std_cnt(char *ptr, instance_t *i, type_t *t);
extern intptr_t str_cnt(char *ptr, instance_t *i, type_t *t);
extern int data_init(int complete);
extern int data_test_loop(int wid, int len);
extern int data_test_default(int wid, int len);
extern int data_test(int wid, int len);
/* cs.c */
extern client_t *client_ist(socket_t *sp);
extern void client_set_udata(client_t *cp, void *udata);
extern void client_rls(client_t *cp);
extern void client_dmp(client_t *cp);
extern server_t *server_ist(int backlog);
extern void server_rls(server_t *sp);
extern void server_catch_signals(void);
extern client_t *server_wait(server_t *sp);
extern int server_handle(client_t *cp);
extern int server_loop(server_t *sp);
extern int service_del_client(client_t *cp);
extern int service_add_client(service_t *sp, client_t *cp);
extern service_t *service_ist(int port, int type, service_fnc_t handler, void *udata);
extern int server_add_service(server_t *sp, service_t *svp);
extern void service_rls(service_t *sp);
extern void server_testserver(void);
/* table.c */
extern table_t *table_ist(int rows, int cols);
extern char *table_get(table_t *tbl, int x, int y);
extern int table_get_int(table_t *tbl, int x, int y);
extern int table_get_rows(table_t *tbl);
extern int table_get_cols(table_t *tbl);
extern int table_set(table_t *tbl, int x, int y, char *data);
extern int table_del_row(table_t *tbl, int y);
extern int table_ins_row(table_t *tbl, int y);
extern int table_del_col(table_t *tbl, int x);
extern int table_ins_col(table_t *tbl, int x);
extern int table_dmp(table_t *tbl, dmp_fnc_t dmp);
extern void table_traverse_row(table_t *tbl, int y, void_fnc_t fnc);
extern int table_traverse_col(table_t *tbl, int x, void_fnc_t fnc);
extern void table_traverse(table_t *tbl, void_fnc_t fnc);
extern table_t *table_find_cols(table_t *tbl, int Y, void *data, cmp_fnc_t cmp, void *hdl);
extern table_t *table_find_rows(table_t *tbl, int X, void *data, cmp_fnc_t cmp, void *hdl);
extern void table_sort(table_t *tbl, cmp_fnc_t cmp, void *hdl);
extern void table_rls(table_t *tbl);
extern int table_test(int num, int len);
/* smtp.c */
extern smtp_t *smtp_ist(char *server, short port);
extern void smtp_rls(smtp_t *sp);
extern void smtp_hdrs_rls(smtp_t *sp);
extern int smtp_add_hdr(smtp_t *sp, char *hdr, char *val);
extern int smtp_add_rcpt(smtp_t *sp, char *rcpt);
extern void smtp_add_msg(smtp_t *sp, char *msg);
extern void smtp_set_msg(smtp_t *sp, char *msg);
extern int smtp_expect(smtp_t *sp, io_t *iop, buf_t *bp, char *expected, char *altexpect);
extern int smtp_send(smtp_t *smp);
extern int smtp_test(int w, int h);
/* time.c */
extern time_t maketime(int Y, int M, int D, int h, int m);
extern char *micro_fmt(micro_t mt);
extern void micro_dmp(micro_t mt);
extern micro_t micro_now(void);
extern struct timeval micro_timeval(micro_t mt);
extern char *hms_fmt(time_t t);
extern char *hour_fmt(time_t t);
extern char *day_fmt(time_t t);
/* set.c */
extern int set_len(set_t *set);
extern int set_empty(set_t *set);
extern set_t *set_ist(int wid, cmp_fnc_t cmp);
extern void *set_get(set_t *set, void *key);
extern void set_rls(set_t *set);
extern void *set_next(set_t *set);
extern void *set_first(set_t *set);
extern char *set_string(set_t *set, str_fnc_t dmp);
extern void set_dmp(set_t *set, dmp_fnc_t dmp, void *xtra);
extern int set_pos(set_t *set, void *a);
extern int set_contains(set_t *set, void *a);
extern int set_add(set_t *set, void *a);
extern set_t *sets_combine(set_t *a, set_t *b, int how);
extern set_t *set_dup(set_t *a);
extern int sets_equal(set_t *a, set_t *b);
extern set_t *sets_subtract(set_t *a, set_t *b);
extern set_t *sets_union(set_t *a, set_t *b);
extern set_t *sets_intersection(set_t *a, set_t *b);
extern set_t *sets_add(set_t *a, set_t *b);
extern int set_test2(int w, int h);
extern int set_test1(int w, int h);
extern int set_test(int w, int l);
/* w32.c */
/* scale.c */
extern void scale_recalc(scale_t *sp);
extern void scale_set_world_snap(scale_t *sp, int offset, int step);
extern int scale_snap_world(scale_t *sp, int wx);
extern scale_t *scale_ist(int wl, int wr, int ml, int mr);
extern void scale_set_world(scale_t *sp, int start, int end);
extern void scale_set_model(scale_t *sp, int start, int end);
extern int scale_get_world(scale_t *sp, int m);
extern int scale_get_model(scale_t *sp, int w);
extern void scale_dmp(scale_t *sp);
extern int scale_snap_model(scale_t *sp, int mr);
extern int scale_test(int w, int h);
/* json.c */
extern json_t *json_null(void);
extern json_t *json_object(void);
extern json_t *json_array(void);
extern json_t *json_string(const char *s);
extern json_t *json_int(long long i);
extern json_t *json_double(double i);
extern void json_rls(json_t *json);
extern json_t *json_object_get(json_t *o, const char *key);
extern json_t *json_array_get(json_t *a, int i);
extern int json_array_len(json_t *a);
extern int json_parse_comment(char **string);
extern void json_parse_ws(char **string);
extern json_t *json_parse_number(char **string);
extern char *json_parse_string(char **string);
extern json_t *json_parse_literals(char **string);
extern da_t *json_parse_array(char **string);
extern json_t *json_parse_value(char **string);
extern void *syntax_error(char *msg, char *where);
extern da_t *json_parse_object(char **string);
extern json_t *json_parse(char *string);
extern char *json_stringify(json_t *json);
extern void json_stringify_recurse(json_t *json, buf_t *bp);
extern void json_dump(json_t *json);
extern void json_prettyprint(json_t *json, int depth);
extern void json_prettyprint_custom(json_t *json, int depth, char *linebreak);
extern void json_object_add(json_t *o, const char *key, json_t *mem);
extern json_t *json_array_add(json_t *o, json_t *mem);
extern json_t *json_rpc_err_ist(int errornumber, const char *errmsg);
extern json_t *json_rpc_snd_ist(json_t *id, const char *method, json_t *params);
extern void json_rpc_snd_addparam(json_t *rpc, json_t *param);
extern json_t *json_rpc_rcv_ist(json_t *id, json_t *result, json_t *error);
extern int json_rpc_test(int w, int h);
extern int json_data_test(int w, int h);
extern int json_test(int w, int h);
/* md5.c */
extern void MD5Init(MD5_CTX *mdContext);
extern void MD5Update(MD5_CTX *mdContext, unsigned char *inBuf, unsigned int inLen);
extern void MD5Final(MD5_CTX *mdContext);
extern char *MDString(MD5_CTX *mdContext);
extern char *md5_encode(char *str);
extern int md5_test(int w, int h);
/* writepid.c */
extern int writePid_currentPid(char *filename);
extern void writePid_write(char *filename);
extern void writePid_sendsignal(int pid, int signal);
extern void writePid_init(char *programName, int action);
/* parse.c */
extern void parse_rls(parse_t *pp);
extern parse_t *parse_ist(io_t *iop);
extern void parse_add_context(parse_t *pp, pcontext_t *parent, pcontext_t *child);
extern void context_rls(pcontext_t *ctp);
extern pcontext_t *context_ist(char *before, char *start, char *end, char *after);
extern int parse_it(parse_t *pp);
extern int parse_test(int len, int wid);
/* tok.c */
extern tok_t *tok_ist(char *name);
extern void tok_rls(tok_t *tok);
extern int tok_test(int len, int wid);
/* tw.c */
/* base64.c */
extern void base64_encode(const char *in, size_t inlen, char *out, size_t outlen);
extern size_t base64_encode_alloc(const char *in, size_t inlen, char **out);
extern _Bool isbase64(char ch);
extern _Bool base64_decode(const char *in, size_t inlen, char *out, size_t *outlen);
extern _Bool base64_decode_alloc(const char *in, size_t inlen, char **out, size_t *outlen);
/* sha1.c */
extern void sha1_init_ctx(struct sha1_ctx *ctx);
extern void *sha1_read_ctx(const struct sha1_ctx *ctx, void *resbuf);
extern void *sha1_finish_ctx(struct sha1_ctx *ctx, void *resbuf);
extern int sha1_stream(FILE *stream, void *resblock);
extern void *sha1_buffer(const char *buffer, size_t len, void *resblock);
extern void sha1_process_bytes(const void *buffer, size_t len, struct sha1_ctx *ctx);
extern void sha1_process_block(const void *buffer, size_t len, struct sha1_ctx *ctx);
/* hmac-sha1.c */
extern int hmac_sha1(const void *key, size_t keylen, const void *in, size_t inlen, void *resbuf);
/* memxor.c */
extern void *memxor(void *dest, const void *src, size_t n);
