/** @file 
	@brief indexed, dynamic array wraps all functions of da, but takes care 
	of reindexing !! 
 
	ida is built up with indices (idx.c) so every index has forward and
	reverse indexing.
	if you are looking for 1-way mapping : don't use ida.c
*/

#include <ego.h>

/** @brief get index 
 * get index number n
 * @param idap indexed array pointer
 * @param n which index to report
 * @return
 */
idx_t * ida_index(ida_t *idap, int n)
{
	idx_t **idp;

	idp = (idx_t **)da_get (idap->idxs, n);
	return *idp;
}

/** @brief get position 
 * at what position in the forward index is offset n, which is the same as
 * get position n in the inverse index
 * 
 * @param idap indexed array pointer
 * @param idx which index to search, so 0 if you only have 1 index
 * @param n position to search for 
 * @return position at which n is
 */
int ida_idx_get_pos(ida_t *idap, int idx, int n)
{
	idx_t *idp;

	if (idx >= da_len(idap->idxs)) return -1;

	idp = ida_index(idap,idx);
	return idx_index(idp,n);
}

/** @brief get data
 * get the data on nth position in index idx, for instance 
 * ida_idx_get(idap,0,0) gets the first item in the sorted view of index 0
 * @param idap indexed array pointer
 * @param idx which index to use, 0 ir only 1 index
 * @param n at which position in that index
 * @return pointer to the data at position n in index idx
 */
void *ida_idx_get(ida_t *idap, int idx, int n)
{
	int redir;
	void *p;
	idx_t *idp;

	if (idx >= da_len(idap->idxs)) return NULL;

	idp = ida_index(idap,idx);
	redir = idx_index(idp,n);

	p = ida_get(idap, redir);

	return p;
}

/** @brief release indexed array
 * release indexed array and free up all it's resources
 * @param idap indexed array pointer
 */
void ida_rls(ida_t *idap)
{
	int t;
	idx_t *idp;
	if (!idap)return;

	if (idap->dap) da_rls(idap->dap);
	if (!idap->idxs) return;

	for (t=0; t< da_len(idap->idxs); t++) {
		idp = ida_index(idap,t);
		idx_rls(idp);
	}

	da_rls(idap->idxs);
	free(idap);
}

/** @brief instantiate indexed array
 * instantiate an indexed array holding len elements of wid wide
 * 
 * @param wid width in bytes of data elements in the indexed array
 * @param len initial length of the indexed array
 * @return pointer to indexed array object
 */
ida_t *ida_ist(int wid, int len)
{
	ida_t *idap = (ida_t *)calloc(1,sizeof(ida_t));

	idap->dap = da_ist(wid,len);
	idap->idxs = da_ist(sizeof(idx_t *),0);

	return idap;
}

/** @brief number or indices
 * return the number of indixes added to this indexed array
 * @param idap indexed array pointer
 * @return number if indices
 */
int ida_nidx(ida_t *idap)
{
	return da_len(idap->idxs);
}

/** @brief test sortedness
 * test if this indexed array is sorted (reindexed) for all indices
 * for instance after adding anelement the sortedness is likely gone
 * @param idap indexed array pointer
 * @return 0 or 1 
 */
int ida_issorted(ida_t *idap)
{
	int t;
	idx_t *idp;
	for (t=0; t< ida_nidx(idap); t++) {
		idp = ida_index(idap,t);
		if (!idx_issorted(idp)) return 0;
	}

	return 1;
}

/** @brief add index
 * add a new index and inverse index to the indexed array and try to sort them
 * @param idap indexed array pointer
 * @param ip index to add
 * @return pointer to the index
 */
idx_t *ida_add_index(ida_t *idap, idx_t *ip)
{

	da_add(idap->idxs, &ip);

	idx_sort(ip);
	idx_invert(ip);

	return ip;
}

/** @brief index length
 * returns the number of elements in the indexed array
 * @param idap indexed array pointer
 * @return number of elements
 */
int ida_len(ida_t *idap)
{
	return da_len(idap->dap);
}

/** @brief dump indexed array
 * dump the content of th edata, and all indices of this indexed array
 * @param idap indexed array pointer
 * @param dmp function that dumps one element of array data
 * @param udata optional extra static data to be passed to the dump function
 * @return ERR_NONE
 */
int ida_dmp(ida_t *idap, dmp_fnc_t dmp, void *udata)
{
	int t;

	/* lprintf(LOG_ALOT, "idap %d long %d indices\n", da_len(idap->dap),  */
		/* da_len(idap->idxs)); */
	
	lprintf(LOG_ALOT, "-- actual data --\n");
	da_dmp(idap->dap, dmp, udata);

	for (t=0; t< da_len(idap->idxs); t++)
	{
		lprintf(LOG_ALOT, "-- index %d --\n", t);
		idx_t *idp;
		idp = ida_index(idap,t);
		idx_dmp(idp);
		lprintf(LOG_ALOT, "-- index %d (data) --\n", t);
		idx_dmp_da(idp,int_dmp,idp->udata);
	}
	return ERR_NONE;
}

/** @brief set sorted
 * disable sorting for bulk updates. for instance when adding multiple
 * elements, ida sorts when sorted is set. So disable it add all items and then
 * sort once .
 * 
 * @param idap indexed array pointer
 * @param sorted set sorted on or off
 * @return ERR_NONE
 */
int ida_set_sorted(ida_t *idap, int sorted)
{
	int t;
	idx_t *idp;
	for (t=0; t< ida_nidx(idap); t++) {
		idp = ida_index(idap,t);
		idp->sorted = sorted;
	}

	return ERR_NONE;
}

/** @brief set data
 * set data at the unsorted element in the indexed array
 * @param idap indexed array pointer
 * @param data data pointer to be pasted in
 * @param pos position at which to set the data
 * @return 0
 */
int ida_set(ida_t *idap, void *data, int pos)
{
	int t;
	unsigned int sortpos_o;
	idx_t *idp;

	/* invalidates sort order, get all positions */
	for (t=0; t< ida_nidx(idap); t++) {
		/* if setting a lot at once, you don't want reindexing */
		idp = ida_index(idap,t);
		if (!idp->sorted) continue;
		idp->opos = pos;
		idp->npos = idx_insertpos(idp, data);
		sortpos_o = idx_inv(idp, idp->opos);
		if (idp->npos > sortpos_o) idp->npos--; /* don't count 'self' */
	}

	da_set(idap->dap,data,pos);

	for (t=0; t< ida_nidx(idap); t++) {
		idp = ida_index(idap,t);
		if (!idp->sorted) continue; /* remember to resort manually ! */
		idx_reindex(idp);
	}
	return 0;
}

/** @brief get data
 * get the data at the given position in the unsorted array, use one of the 
 * index functions to find the correct spot.
 * @param idap indexed array pointer
 * @param pos physical position in the array data where to get the data
 * @return pointer to the element requested
 */
void *ida_get(ida_t *idap, unsigned int pos)
{	
	return da_get(idap->dap,pos);
}

/** @brief add element
 * add a new element to the end of the array data
 * @param idap indexed array pointer
 * @param data pointer to the element data
 * @return 0
 */
int ida_add(ida_t *idap, void *data)
{
	return ida_ins(idap, data, ida_len(idap));
}

/** @brief delete element
 * delete the element at position pos
 * @param idap indexed array pointer
 * @param pos position of th eelement to be deleted
 * @return 0
 */
int ida_del(ida_t *idap, int pos)
{
	int ret,t;
	idx_t *idp;

	for (t=0; t< ida_nidx(idap); t++) {
		idp = ida_index(idap,t);
		idp->npos = idx_inv(idp, pos);
		idx_recalc_deleting(idp, pos);
	}

	ret = da_del(idap->dap, pos, 1);

	return ret;
}

/** @brief insert element
 * insert an element into the given position 
 * detailed information:
<pre>
 * insertion :
      0 1 2 3 4  
     +-+-+-+-+-+ 
     |7|5|1|9|3| actual data
     +-+-+-+-+-+ 
     |2|4|1|0|3| position (int_cmp)
     +-+-+-+-+-+
     |3|2|0|4|1| index (inverse of position)
     +-+-+-+-+-+

    adding 6 at 2 means it will be at positions:
    physical : 2
    sorted   : 3 (after 1,3,5)
    
      0 1 2 3 4 5
     +-+-+=+-+-+-+ 
     |7|5|6|1|9|3| actual data (6 inserted at 2)
     +-+-+=+=+-+-+ 
     |3|5|1|2|0|4| position (see below)
     +-+-+-+=+-+-+
     |3|2|0|4|1| | index (just invert again, we have to walk all items anyway)
     +-+-+-+-+-+-+

    we actually do two operations, shift all to the right of pos 3 1 position:

     +-+-+-+-+-+-+
     |2|4|1| |0|3| 
     +-+-+-+-+^+^+

    the gap is filled with the new elements position and all values higher
    than the PHYSICAL postion are increased by one 

     +-+-+-+-+-+-+
     |3|5|1|2|0|4|
     +^+^+-+-+-+^+
</pre>
 * @param idap indexed array pointer
 * @param data pointer to data to insert
 * @param pos position where to insert
 * @return 0
 */
int ida_ins(ida_t *idap, void *data, int pos)
{
	int t;
	idx_t *idp;

	for (t=0; t< ida_nidx(idap); t++) {
		idp = ida_index(idap,t);
		idp->npos = idx_insertpos(idp, data);
	}

	da_ins(idap->dap,data,pos,1);

	for (t=0; t< ida_nidx(idap); t++) {
		idp = ida_index(idap,t);
		idx_recalc_added(idp,pos);
	}

	return 0;
}

/** @brief duplicate indexed array
 * duplicate an indexed array with all it's indices
 * @param idap indexed array source
 * @return duplicated indexed array object pointer
 */
ida_t *ida_dup(ida_t *idap)
{
	int t;
	ida_t *copy = (ida_t *)calloc(1,sizeof(ida_t));
	idx_t *ip;
	idx_t *ipcpy;

	copy->dap = da_dup(idap->dap);
	copy->idxs = da_dup(idap->idxs);

	for (t=0; t< da_len(idap->idxs); t++) {
		ip = ida_index(idap,t);
		ipcpy = idx_dup(ip,copy->dap);
		da_set(copy->idxs, &ipcpy, t);
	}

	return copy;
}

static int ida_test_ins_del(int num, int len)
{
	int t,x,p;
	idx_t *idxp;
	ida_t *idap =ida_ist(sizeof(int),0);

	idxp = idx_ist(len,ascending_srt, idap->dap);
	idxp = ida_add_index(idap, idxp);

	for (t=0; t< num; t++) {
		x = intrand(1,100);
		p = intrand(0,t);
		ida_ins(idap, &x, p);
		if (!ida_issorted(idap)) {
			lprintf(LOG_ERR, "FAIL INSERTING\n");
			ida_dmp(idap, int_dmp,NULL);
			return 0;
		}
	}
		/* printf("due\n"); */

	for (t=0; t< num; t++) {
		x = ida_len(idap);
		p = intrand(0,x-1);
		ida_del(idap, p);
		/* ida_dmp(idap,int_dmp,NULL); */
		if (!ida_issorted(idap)) {
			lprintf(LOG_ERR, "FAIL DELETING\n");
			ida_dmp(idap, int_dmp,NULL);
			return 0;
		}
	}

	ida_rls(idap);
	
	return 1;
}

static int ida_test_set(int num, int len)
{
	int t,x;
	ida_t *idap =ida_ist(sizeof(int),len);
	idx_t *idxp;
	ida_t *cpy;

	for (t=0; t< len; t++) {
		int n = intrand(1,100);
		ida_set(idap, (void *)&n, t);
	}

	idxp = idx_ist(len, ascending_srt, idap->dap);
	idxp = ida_add_index(idap, idxp);
	/* ida_dmp(idap,int_dmp); */

	for (t=0; t< num; t ++) {
		for (x=0; x< len; x ++) {
			cpy = ida_dup(idap);
			/* ida_dmp(cpy, int_dmp); */
			ida_set(cpy,&t,x);
			if (!ida_issorted(cpy)) {
				lprintf(LOG_ERR, "FAIL\n");
				/* ida_dmp(cpy, int_dmp); */
				return 0;
			}
			/* ida_dmp(cpy, int_dmp); */
			ida_rls(cpy);
		}
	}

	/* ida_dmp(idap,int_dmp); */
	idx_invert(idxp);
	/* ida_dmp(idap,int_dmp); */

	ida_rls(idap);

	return ERR_NONE;
}

static int ida_demo()
{
	int t,x;
	int len = 10;
	int num =1;
	ida_t *idap =ida_ist(sizeof(int),len);
	idx_t *idxp;
	idx_t *idxp2;
	ida_t *cpy;

	intptr_t a;

	printf("ida.c Indexed Array example :\n");
	printf("this creates a sorted index on an array of unsorted data \n");
	printf("and also an inverse index\n");
	printf("let's fill a %d item index with random values \n", len);

	for (t=0; t< len; t++) {
		int n = intrand(1,100);
		ida_set(idap, (void *)&n, t);
	}

	printf("The index gives the sorted order, first index points at the lowest value\n");
	printf("------------------------------------------------------- \n");
	idxp = idx_ist(len, ascending_srt, idap->dap);
	idxp = ida_add_index(idap, idxp);
	ida_dmp(idap,int_dmp,NULL);
	printf("------------------------------------------------------- \n");
	printf("The reverse index does the same for the sorted data, it's first index points at the first actual data\n");
	printf("\n\n\nalso you can add more indeces, for instance one sorting backwards\n");
	printf("------------------------------------------------------- \n");
	idxp2 = idx_ist(len, descending_srt, idap->dap);
	idxp = ida_add_index(idap, idxp2);
	ida_dmp(idap,int_dmp,NULL);
	printf("------------------------------------------------------- \n");

	a = (intptr_t)ida_idx_get(idap, 0,0 );
	printf (" %d\n", *(int *)a);

	for (t=0; t< num; t ++) {
		for (x=0; x< len; x ++) {
			cpy = ida_dup(idap);
			/* ida_dmp(cpy, int_dmp); */
			ida_set(cpy,&t,x);
			if (!ida_issorted(cpy)) {
				lprintf(LOG_ERR, "FAIL\n");
				/* ida_dmp(cpy, int_dmp); */
				return 0;
			}
			/* ida_dmp(cpy, int_dmp); */
			ida_rls(cpy);
		}
	}

	/* ida_dmp(idap,int_dmp); */
	idx_invert(idxp);
	/* ida_dmp(idap,int_dmp); */

	ida_rls(idap);

	return 1;
}

/** @brief test function
 * internal test function
 */
int ida_test(int num, int wid)
{
	int ret=1;


	ret = ida_test_set(num,wid);
	ret |= ida_test_ins_del(num,wid);

	ret = ida_demo();

	return ret;
}

