/** @file
 * 
	log : a very tiny layer on top og io_t, adding levels like debug, err etc
	almost all io_ functions are in log_ form , but have an extra parameter
	that specifies the level of logging 
*/
#include <ego.h>
#include <stdarg.h>

static log_t *loggers[LOG_MAX+1];

static int loglevel=LOG_ERR;

/** @brief set loggin level
	
	set the global logging level, all messages will honour this level
	messages will be printed if they have a log level lower or equal to this

	@param lvl the new level to set
	@return 0
 */
int logsetlevel(unsigned int lvl)
{
	loglevel = lvl;

	if (loglevel >= LOG_MAX ) loglevel = LOG_ERR;

	return 0;
}

/** @brief exinitialise logging
	
	release some log data created with log_init()
 */
void log_exit(void)
{
	log_rls(loggers[LOG_ERR]);
	log_rls(loggers[LOG_WARN]);
	log_rls(loggers[LOG_DEBUG]);
}

/** @brief initialise logging
	
	initialize the logging until log_exit is called, 
@param level the log level to set
@param err io object to use for errors (see io.c) if NULL defaults to stdout
@param warn io object to use for warnings (see io.c) if NULL defaults to stdout
@param debug io object to use for debug messages (see io.c) if NULL defaults to stdout 
@return 0 on succes 1 on error
 */
int log_init(int level,io_t *err, io_t *warn, io_t *debug)
{
	void *test;
	int ret=0;
	logsetlevel(level);

	io_init();

	/* only IO_FP is supported now because we use vprintf  */
	/* sorry ! */
	if (!err || err->type != IO_FP) err = &io_stdout;
	test = loggers[LOG_ERR] = log_ist(err, LOG_ERR);
	if (!test)ret = 1;
	if (!warn || warn->type != IO_FP) warn = &io_stdout;
	test = loggers[LOG_WARN] = log_ist(warn, LOG_WARN);
	if (!test)ret = 1;
	if (!debug || debug->type != IO_FP) debug = &io_stdout;
	test = loggers[LOG_DEBUG] = log_ist(debug, LOG_DEBUG);
	if (!test)ret = 1;
	loggers[LOG_SILENT] = NULL;
	
	return ret;
}

/** @brief variable arg version log printing

@param logp pointer to a log object
@param fmt format string 
@param argptr pointer to the list of arguments
@return the number of characters written like printf
 */
int log_vprintf(log_t *logp, const char *fmt, va_list argptr) {
	if (!logp) return 0; /* is NOT an error */
	return io_vprintf(logp->iop, fmt, argptr);
}

/** @brief log print function

	log print function with printf interface

@param logp pointer to a log object
@param fmt format string 
@param ... the list of arguments
@return the number of characters written like printf
 */
int log_printf(log_t *logp, char *fmt, ...)
{
	int ret=0;
	va_list vl;

	va_start(vl, fmt);
	ret = log_vprintf(logp, fmt, vl);

	return ret;
}

/** @brief log print function 

	<pre>
	for instance : lprintf(LOG_VERBOSE, ..) : 
	please note that you mean, print if level is set to VERBOSE !!!!
 	it does NOT mean that the printing will be verbose. For that i added
	the level LOG_ALOT, which is the same level as LOG_ERROR 
	LOG_SILENT is reserved to REALLY shutup the system. If you want to 
	override that you may still bypass the log functions 
	</pre>
	@param level at which log level set will this function start logging
	@param fmt format string
	@param ... arguments to print
	@return number of characters printed
 */
int lprintf(int level, char *fmt, ...)
{
	int ret=0;
	va_list vl;

	va_start(vl, fmt);

	/* this overrules all, even lprintf(LOG_ALL, xx) won't pass */
	if (loglevel == LOG_SILENT) return ret; 

	if (level >= loglevel) {
		if (level < 1 || level > 3) return ret;
		ret = log_vprintf(loggers[level], fmt, vl);
	}

	return ret;
}

/** @brief log error function

	log the last reported error string 
@param level up to which level should be reported
@return number of characters printed
 */
int log_error(int level)
{
	return lprintf(level, "%s", err_string());
}

/** @brief set color for logging

	set the color to be used for all logging messages of the given level
	supports coloring
@param level which link level should be colored
@param color which color number is used
 */
void log_setcolor(int level, int color)
{
	io_setcolor(loggers[level]->iop, color);
}

/** @brief log ok

	simply print a green ok or red fail message
@param depth how far should the message be indented
@param msg message to be printed before ok/fail
@param ok print ok 1 or fail 0
 */
void log_ok(int depth, char *msg, int ok)
{
	lprintf(depth, "%-60s", msg);
	if (ok) {
		log_setcolor(depth, IO_GREEN);
		lprintf(depth, "OK\n");
	} else {
		log_setcolor(depth, IO_RED);
		lprintf(depth, "FAIL\n");
	}
	log_setcolor(depth, IO_NORMAL);

}

/** @brief release log object

	free up a logger object
@param lp pointer to a log object 
 */
void log_rls(log_t *lp)
{
	if (lp) free(lp);
}

/** @brief instantiate a logging object

	create a new logging object for a certain io type
@param iop pointer to input output object to use
@param level which level this object will log
@return pointer to e noew logging object
 */
log_t *log_ist(io_t *iop, int level)
{
	log_t *logp = (log_t *)calloc(sizeof(log_t),1);

	logp->iop = iop;
	logp->level = level;

	return logp;
}

/* test section */

int log_test_levels(int size, int num)
{
	printf("Log level LOG_DEBUG--------\n");
	log_init(LOG_DEBUG, NULL, NULL, NULL);
	lprintf(LOG_ERR, "logs LOG_ERROR\n");
	lprintf(LOG_WARN, "logs LOG_WARNING\n");
	lprintf(LOG_DEBUG, "logs LOG_DEBUG\n");
	lprintf(LOG_BLAH, "logs LOG_BLAH\n");

	printf("Log level LOG_WARN--------\n");
	log_init(LOG_WARN, NULL, NULL, NULL);
	lprintf(LOG_ERR, "logs LOG_ERROR\n");
	lprintf(LOG_WARN, "logs LOG_WARNING\n");
	lprintf(LOG_DEBUG, "logs LOG_DEBUG\n");
	lprintf(LOG_BLAH, "logs LOG_BLAH\n");

	printf("Log level LOG_ERR--------\n");
	log_init(LOG_ERR, NULL, NULL, NULL);
	lprintf(LOG_ERR, "logs LOG_ERROR\n");
	lprintf(LOG_WARN, "logs LOG_WARNING\n");
	lprintf(LOG_DEBUG, "logs LOG_DEBUG\n");
	lprintf(LOG_BLAH, "logs LOG_BLAH\n");

	printf("Log level LOG_SILENT--------\n");
	log_init(LOG_SILENT, NULL, NULL, NULL);
	lprintf(LOG_ERR, "logs LOG_ERROR\n");
	lprintf(LOG_WARN, "logs LOG_WARNING\n");
	lprintf(LOG_DEBUG, "logs LOG_DEBUG\n");
	lprintf(LOG_BLAH, "logs LOG_ALL\n");

	return 1; /* visual test */
}

int log_test_files(int size, int num)
{
	printf("TODO: test here with three different files and see if each log go to the correct file\n");
	return 0; /* manually checked test */
}

int log_test(int size, int num)
{
	int ret;

	ret  = log_test_files(size, num);
	ret &= log_test_levels(size, num);

	return ret;
}

