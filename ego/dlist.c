/** @file
 * 
	doubly linked list, for now only doubly linked list is implemented
    since strangely enough i never needed a singly linked list.
	
 */
#include <ego.h>

/** @brief link first and only 
 * 
 * macro for linking the first item in an empty list
 @param list (empty) list to link into
 @param item new item to link
 */
#define D_LINK_ONLY(list,item)		\
		item->next = &list->tail; 	\
		item->prev = &list->head;		\
		list->head.next = item;		\
		list->tail.prev = item	\
		
/** @brief link first 
 * 
 * macro for linking the first item in filled list
 @param list list to link into
 @param item new item to link
 */
#define D_LINK_FIRST(list,item) \
		item->next = list->head.next; 	\
		item->prev = &list->head;		\
		list->head.next->prev = item;		\
		list->head.next = item 	\

/** @brief link last
 * 
 * macro for linking the last item in a filled list
 @param list list to link into
 @param item new item to link
 */
#define D_LINK_LAST(list,item) \
		item->prev = list->tail.prev; 	\
		item->next = &list->tail; 	\
		list->tail.prev->next = item;		\
		list->tail.prev = item 	\

/** @brief link after an item
 * 
 * macro for linking an item after another item
 @param old item to link after
 @param item new item to link
 */
#define D_LINK_AFTER(old,item)			\
		item->next = old->next; 	\
		item->prev = old;			\
		if (old->next) old->next->prev = item;	\
		old->next = item\

/** @brief unlink first item
 * 
 * macro for unlinking the first item in a list
 @param list list to unlink from
 */
#define D_UNLINK_FIRST(list) 	\
		list->head.next->next->prev = &list->head;	\
		list->head.next = list->head.next->next;	\

/** @brief unlink first and only 
 * 
 * macro for unlinking the only item in a list
 @param list (empty) list to link into
 */
#define D_UNLINK_ONLY(list) 	\
		list->head.next = &list->tail; \
		list->tail.prev = &list->head;

/** @brief unlink last item
 * 
 * macro for unlinking the last item in a list
 @param list (empty) list to link into
 */
#define D_UNLINK_LAST(list) 	\
		list->tail.prev->prev->next = &list->tail;	\
		list->tail.prev = list->tail.prev->prev;	\

/** @brief unlink an item from a list
 * 
 * macro for unlinking the given item from a list
 @param pos liste item to unlink from it's list
 */
#define D_UNLINK(pos)		\
		pos->prev->next = pos->next;	\
		pos->next->prev = pos->prev;	\

/** @brief connect two elements
 * 
 * macro for linking two items in a list
 @param a previous item to link
 @param b next item to link
 */
#define D_CONNECT(a,b)		\
		a->next = b;		\
		b->prev = a;

#ifdef DEBUG
static void dmp(int wid, void *a, void *unused)
{
	int b = **(int **)a;
	printf("item now %d\n", b);
}	
#endif

/** @brief get ordinal position 
	
 get the ordinal position of the given item in a list
 this is done by sequentially traversing the list
 @param lst list in which to search (count) the position
 @param li list item to get the position of
 @return position in the list
 */
int dlst_get_pos(dlst_t *lst, ditem_t * li)
{
	int i;
	ditem_t * walk;

	for (i=0, walk = lst->head.next; walk != NULL; i++,walk= walk->next)
	{
		/* printf("%p != %p\n", walk, li); */
		if (walk == li)
			return i;
	}

	return -1; /* not there */
}

/** @brief duplicate a list
	
	duplicate a list with all of it's items.
 @param lst list to duplicate
 @return duplicate list
 */
dlst_t *dlst_dup(dlst_t *lst)
{
	int i;
	dlst_t *dup;
	ditem_t * walk;

	dup = dlst_ist(lst->op);

	for (i=0, walk = lst->head.next; walk != NULL; i++,walk= walk->next)
	{
		dlst_add(dup,walk->data);
	}

	return dup; 
}

/** @brief list length
	
	get the total number of items in the list
 @param lst list used
 @return number of items
 */
int dlst_len(dlst_t * lst)
{
	return lst->len;
}

/** @brief get list item
	
	get the items at pos in the list
 @param lst list used
 @param pos at which position in the list to get the item
 @return nth item
 */
ditem_t * dlst_get_item(dlst_t * lst, int pos)
{
	int i;
	ditem_t * walk;

	/* at least cut down the search time */
	if (pos < lst->len/2) {
		for (i=0, walk = lst->head.next; walk != &lst->tail; i++,walk= walk->next)
		{
			if (pos == i) 
				return walk;
		}
	} else {
		for (i=lst->len-1, walk = lst->tail.prev; walk != &lst->head; i--,walk= walk->prev)
		{
			if (pos == i) 
				return walk;
		}
	}
	return NULL; /* not found */
}

/** @brief find item data 
	
	find an item given it's data and a suitable compare function
 @param lst list used
 @param data opaque pointer to the data to search for
 @param alternate optional search function, if NULL the internal compare 
	functions are used
 @return item found, or NULL
 */
ditem_t * dlst_find(dlst_t * lst, void *data, cmp_fnc_t alternate)
{
	int i;
	ditem_t * walk;

	if (!lst->op->cmp) return NULL;

	for (i=0, walk = lst->head.next; walk != &lst->tail; i++,walk= walk->next)
	{
		if (alternate) {
			if (!alternate(walk->data,data,lst->op))
				return walk;
		} else {
			if (!lst->op->cmp(walk->data,data,lst->op))
				return walk;
		}
	}
	return NULL; /* not found */
}

/** @brief find insertion position
	
	return best position to insert an item if it is not found
 @param lst list used
 @param data opaque pointer to the data to search for
 @return item after which to insert the given data
 */
ditem_t * dlst_insertfind(dlst_t * lst, void *data)
{
	int i;
	ditem_t * walk;
	ditem_t * the_one;
	ditem_t finder;

	if (!lst->op->cmp) return NULL;

	finder.data = data;

	the_one = &lst->head;
	for (i=0, walk = lst->head.next; walk != &lst->tail; i++,walk= walk->next)
	{
		if (lst->op->cmp(walk->data,finder.data,lst->op)<=0)
			the_one = walk; 
	}
	return the_one; /* not found, so return the end */
}

/** @brief release list item
	
	release a list item
 @param li lits item to free
 */
void dlst_item_rls(ditem_t * li)
{
	if (!li) return;
	free(li);
}

/** @brief get list item
	
	Get the correct item according to LST_FIRST, LST_LAST or pos
 @param lst list used
 @param where item position to retrieve, special values are LST_FIRST (0 
	of course) and LST_LAST (negative so as not to conflict with real positions)
 @return item at pos where
 */
ditem_t * dlst_item(dlst_t * lst, int where)
{
	if (lst->len <= 0 || where > lst->len) 
		return NULL;

	if (where == LST_LAST) 
		return lst->tail.prev;

	if (where < 1) /* discard LST_BEFORE/LST_AFTER */
		return lst->head.next;

	return dlst_get_item(lst, where);
}

/** @brief delete list item
	
	delete an item from the list lst, either old can be used as the item 
	tp release, or where can be used to find the item to release
 @param lst list used
 @param old item to be released
 @param where item position to release, special values are LST_FIRST (0 
	of course) and LST_LAST (negative so as not to conflict with real positions)
 */
void dlst_del(dlst_t * lst, ditem_t * old, int where)
{
	ditem_t * li;

	if (lst->len <= 0) 
		return;

	if (lst->len == 1) {
		li=lst->head.next;
		D_UNLINK_ONLY(lst);
		dlst_item_rls(li);
		lst->len--;
		return;
	}
	if (old == lst->head.next || where == LST_FIRST ) {
		li=lst->head.next;
		D_UNLINK_FIRST(lst);
		dlst_item_rls(li);
		lst->len--;
		return;
	}

	if (where == LST_LAST || where >= lst->len || old == lst->tail.prev)  {
		li=lst->tail.prev;
		D_UNLINK_LAST(lst)
		dlst_item_rls(li);
		lst->len--;
		return;
	}

	if (where > 0 && old == NULL) {
		old = dlst_get_item(lst, where);
	}
	if (old) {
		D_UNLINK(old);
		dlst_item_rls(old);
	}
	lst->len--;
}

/** @brief insert data
	
	insert the given data into a list, use the internal object_t to position
	it correctly
 @param dp list used
 @param data opaque pointer to the data to insert
 @return number of items
 */
ditem_t *dlst_ins(dlst_t *dp, void *data)
{
	ditem_t *old, *li;

	if (!dp->op->cmp) return NULL;

	li = (ditem_t *)calloc(1,sizeof(ditem_t));
	li->data = data;

	dp->len++;
	old = dlst_insertfind(dp, data);
	D_LINK_AFTER(old, li);
	return li;
}

/** @brief insert data at position
	
	insert new data into a list,relative to a given item
 @param lst list used
 @param old item to use to insert around. parameter where states where, 
		for instance LST_BEFORE or LST_AFTER, etc
 @param data opaque pointer to the data to be inserted
 @param where item position to release, special values are LST_FIRST, 
		LST_BEFORE, LST_AFTER , LST_LAST. They are 0 or negative and 
		don't clash with 'real' positions
 @return pointer to the inserted item
 */
ditem_t * dlst_ins_at(dlst_t * lst, ditem_t * old, void *data, int where)
{
	ditem_t * li = (ditem_t *)calloc(1,sizeof(ditem_t));

	li->data = data;

	if (lst->len == 0) {
		D_LINK_ONLY(lst, li);
		lst->len++;
		return li;
	}

	lst->len++;

	/* if item should be linked at the beginning */
	if ( (where == LST_BEFORE && old == lst->head.next) || 
		where == LST_FIRST || 
		(where == LST_AFTER && old == NULL) ) {
		/* printf("chosen for link start\n"); */
		D_LINK_FIRST(lst,li);
		return li;
	}

	if (where == LST_LAST || where >= lst->len-1)  {
		D_LINK_LAST(lst,li);
		lst->tail.prev = li;
		return li;
	}

	if (where == LST_AFTER) {
		if (old == lst->tail.prev) {
		/* printf("chosen for link last\n"); */
			D_LINK_LAST(lst,li);
		} else {
			D_LINK_AFTER(old,li);
		}
	} else if (where == LST_BEFORE) {
		old = old->prev;
		D_LINK_AFTER(old,li);
	} else {
		old = dlst_get_item(lst, where-1);
		D_LINK_AFTER(old,li);
		return li;
	}

	return li;
}

/** @brief traverse a list 
	
	walk along all items in the list and perform the function on them
 @param lst list to traverse
 @param fnc function to perform on each item
 @param udata optional user data to pass to the given functions udata parameter
 @return 0
 */
int dlst_traverse(dlst_t *lst, ditem_fnc_t fnc,void *udata)
{
	ditem_t * walk;
	int i;

	for (i=0, walk = lst->head.next; walk != &lst->tail; i++,walk= walk->next)
	{
		fnc(lst,walk,udata);
	}
	return 0;
}

/** @brief release a list item data
	
	release a list item's internal data, according to it's object_t 
 @param lst list used
 @param dit item to clean up
 @param udata opaque pointer to the object type, opaque to be able to use 
 	the rls_fnc_t prototype
 */
void ditem_rls(dlst_t *lst, ditem_t *dit, void *udata)
{
	/* ISO C forbids : */
	object_t *op=(object_t*)udata;
	/* well that's lame because then i do it this way buttheads */
	rls_fnc_t rls = op->rls;

	//printf("vrijing %p\n", dit->data);
	if (rls == NULL) rls = anything_rls;

	rls(dit->data);
}

/** @brief gut a list
	
	release all data from a list, but maintrain it's bone structure
 @param dp list to gut
 @param op object pointer describing the data kept in the list
 */
void dlst_gut(dlst_t *dp, object_t *op)
{
	dlst_traverse(dp,ditem_rls,op);
}


/** @brief add item
	
	add an item to the end of the list

	normally you need to provide an allocated pointer :
	type *ptr; int n; int *np;

	normal usage : 	
		dlst_add(d, (void *)ptr);
	integers: 		
		dlst_add(d, (void *)np);	
		dlst_add(d, (void *)&n);
		dlst_add(d, (void *)n);

	but of course you have to take care of cleaning up the pointers 
	see dlst_gut() for that 
 @param lst list used
 @param data opaque pointer to the data to add
 @return added item
 */
ditem_t * dlst_add(dlst_t * lst, void *data)
{
	ditem_t * li ;

	li = (ditem_t *)malloc(sizeof(ditem_t));
	/* save the data according to given wid */
	/* li->data = (void *)malloc(lst->wid); */
	/* memcpy(li->data, data, lst->wid); */
	li->data = data;
	lst->len++;


	/* and link it to the end */
	if (!lst->head.next) {
		D_LINK_ONLY(lst,li);
	} else {
		D_LINK_LAST(lst,li);
		lst->tail.prev = li;
	}

	return li;
}

/** @brief join two lists
	
	join two lists together, a will contain both lists data, b will be empty
 @param a list to add b to 
 @param b list to add to a, it will remain empty after this
 @return 0
 */
int dlst_join(dlst_t *a, dlst_t *b)
{

	if (!b->head.next) return 0; /* not'n to do */
	if (!a->head.next) { /* link b's list to a */
		a->head.next = b->head.next;
		a->tail.prev = b->tail.prev;
	} else {	/* link b's list after a */
		a->tail.prev->next = b->head.next;
		b->head.next->prev = a->tail.prev;
		a->tail.prev = b->tail.prev;
	}

	/* empty b in fear of double releases */
	b->head.next = &b->tail;
	b->tail.prev = &b->head;
	a->len += b->len;
	b->len = 0;

	return 0;
}

/** @brief instantiate list
	
	create a new list for containing objects of a certain type
 @param op object pointer see object.c
 @return newly created list
 */
dlst_t * dlst_ist(object_t *op)
{
	dlst_t * lst = (dlst_t *)calloc(1,sizeof(dlst_t));

	lst->head.next = &lst->tail;
	lst->tail.prev = &lst->head;
	if (op == NULL) op = &null_object;
	lst->op = op;
	lst->len   = 0;

	return lst;
}

/** @brief dump list right
	
	dump a list from the given item onwards
 @param lst list used
 @param li list item to be dumped 
 @param dmpfnc dump function to use on the void * data
 */
void dlst_dmp_r(dlst_t * lst, ditem_t * li, dmp_fnc_t dmpfnc)
{
	if (dmpfnc == NULL)
		dmpfnc = stddmpfnc;

	if (!li || !li->next) /* end of the road */
		return;

	dmpfnc(0, li->data, lst->op);

	/* recurse on */
	dlst_dmp_r(lst, li->next, dmpfnc);
}

/** @brief dump list left

 @param lst list used
 @param li list item to dump
 */
void dlst_dmp_l(dlst_t * lst, ditem_t * li)
{
	if (!li || !li->prev) /* end of the road */
		return;

	lst->op->dmp(0, li->data, NULL);

	/* recurse on */
	dlst_dmp_l(lst, li->prev);
}

/** @brief dump list forward and backward
	
	dump both right and left
 @param lst list used
 */
void dlst_dmp_double(dlst_t * lst)
{
	dlst_dmp(lst);
	dlst_dmp_l(lst, lst->tail.prev);
	printf("\n");
}


/** @brief dump a list
	
	dump all items in a list
 @param lst list used
 */
void dlst_dmp(dlst_t * lst)
{
	/* printf("dumping a list %d big \n", lst->len); */
	/* printf("startptr : %p, endptr %p\n", lst->head.next, lst->tail.prev); */
	dlst_dmp_r(lst, lst->head.next, lst->op->dmp);
	printf("\n");
}

/** @brief empty list
	
	delete all items from a list
 @param lst list to empty
 */
void dlst_empty(dlst_t *lst)
{
	if (!lst) return ;

	while (dlst_len(lst)) {
		dlst_del(lst, NULL, LST_FIRST);
	}
}

static void dlst_clear(ditem_t * li)
{
	if (!li || !li->next)
		return;

	dlst_clear(li->next);
	dlst_item_rls(li);
}

/** @brief release a list
		
	clear the list and free it
	we do not want to try to automatically release the data (through object_t),
	because there are too many cases where you don't want that 
	(one example: lifo_pop() you want the data to remain !!!). 
	Use dlst_gut();dlst_rls(); for a complete release !!
 @param lst list used
 */
void dlst_rls(dlst_t * lst)
{
	ditem_t *li= lst->head.next;

	dlst_clear(li);

	free(lst);
}

/** @brief get first item 
	
	get the first item in the list, for use in combination with dlst_next to 
	create a 'for' loop to walk the list items
 @param dp list used
 @return first item
 */
ditem_t * dlst_first(dlst_t *dp)
{
	if (!dp || !dp->head.next) return NULL;
	return dp->head.next;
}

/** @brief get last item
	
	get the last item in the list, for use in combination with dlst_prev to 
	create a 'for' loop to reversely walk the list items
 @param dp list used
 @return number of items
 */
ditem_t * dlst_last(dlst_t *dp)
{
	if (!dp || !dp->tail.prev) return NULL;
	return dp->tail.prev;
}

/** @brief next item
	
	get the next item in the list, for use in combination with dlst_first to 
	create a 'for' loop to walk the list items
 @param li list item to get next from
 @return number of items
 */
ditem_t * dlst_next(ditem_t * li)
{
	if (!li || !li->next) return NULL;
	return li->next;
}

/** @brief previous item
	
	get the previous item item in the list, for use in combination with 
	dlst_next to reversely walk the list items
 @param li list item to get prvious from 
 @return number of items
 */
ditem_t * dlst_prev(ditem_t * li)
{
	if (!li || li->prev == NULL)return NULL;
	return li->prev;
}

/** @brief get data
	
	get an opaque pointer to the data in an item in the list
 @param lst list used
 @param arg position of the wanted item
 @return data pointer at given location
 */
void *dlst_get(dlst_t * lst, long arg)
{
	ditem_t * li;

	if (!lst)
		return NULL;

	li = dlst_item(lst, arg);
	if (!li || !li->next)
		return NULL;

	return li->data;
}

//static void *dlst_elm(dlst_t * lst, void *Elm, long arg)
//{
	/* TODO : implement ? */
	//return NULL;
//}

static int nul_cmp(cmp_fnc_t cmp, void *left, void *right,void *udata)
{
	ditem_t *a = left;
	ditem_t *b = right;

	if (!a->data && !b->data) return 0;
	if (!a->data) return 1;
	if (!b->data) return -1;

	return cmp(a->data, b->data, udata);
}
	

/** @brief merge two lists
	
	recursively merge two sorted lists into a sorted list, 
	based on the object_t types of
	the lists.  Mostly used for the dlst_sort() function.
 @param dp base list used
 @param from left list tail to merge
 @param to right list tail to merge
 @param len length of both left and right tail
 @param udata userdata to pass to any sort function
 */
void dlst_merge(dlst_t *dp, ditem_t *from, ditem_t *to, int len, void *udata)
{
	int ret;
	int nl=0, nr=0;
	ditem_t *cur;
	ditem_t *left;
	ditem_t *temp;
	ditem_t *mid;

	/* split in two ------ */
	cur = mid = from->next;
	while (nl < len/2 && mid) {
		/* ret = dp->cmp(*cur, *mid,NULL); */ /* later */
		/* printf("ret : %d\n",ret); */
		mid = mid->next; nl++;
	}
	nr = len-nl;
	temp = mid; /* remember start, it might change */
	if (nl > 1) dlst_merge(dp,from,mid,nl,udata);
	mid = temp;
	temp = mid->prev; /* remember start, it might change */
	if (nr > 1) dlst_merge(dp,mid->prev,to,nr,udata);
	mid = temp->next;
	left = from->next;

	/* printf("------- split %d in %d and %d\n", len, nl, nr); */

	/* now actually merge ! ------ */
	cur = from;
	while (nr>0 || nl>0) {
		ret = nul_cmp(dp->op->cmp, left, mid, udata);
		if ((ret < 0 && nl) || nr == 0) { 	/* left is smaller */
			D_CONNECT(cur, left);
			cur = left;
			left = left->next;
			nl --;
		} else {
			D_CONNECT(cur, mid);
			cur = mid;
			mid = mid->next;
			nr --;
		}
	}
	/* tie up */
	to->prev = cur;
	cur->next = to;

}

/** @brief sort list
	
	sort a list with a merge sort algorithm
 @param dp list to be sorted
 @param op object type to use to sort this list, the given object type will 
	become the internal object type of this list object
 @param hdl pointer to user data used for compare functions
 @return number of items
 */
void dlst_sort(dlst_t *dp, object_t *op,void *hdl)
{
	dp->op = op;
	dlst_merge (dp, &dp->head, &dp->tail, dlst_len(dp),hdl);
}

/** @brief test sortedness
	
	tes if a list is sorted or not 
 @param dp list to test
 @return 1 for a sorted list, 0 for non-sorted
 */
int dlst_issorted(dlst_t *dp)
{
	ditem_t *last, *next;

	if (!dp->op->cmp) return 0;

	last = dp->head.next;
	if (!last || !last->next) return 1;
	for (next = last->next; next != &dp->tail; next= next->next)
	{
		if (dp->op->cmp(&last->data,&next->data,NULL) > 0) return 0;
	}

	return 1;
}

/* test section */
static int exhaust (void **res, int wid, void *data)
{
	intptr_t t,i;
	dlst_t *d1;
	ditem_t *li;
	d1 = dlst_ist(NULL);

	for (t=0; t< wid; t++) {
		i = (intptr_t)res[t];
		dlst_add(d1, (void *)i);
	}
	
	/* printf("Test : \n"); */
	/* dlst_dmp_double(d1); */

	dlst_sort(d1, &int_object, NULL);
	for (t=0; t< wid; t++) {
		// dlst_find is altered !!
		// don't know if this still works :
		li = dlst_find(d1, (void *)t, NULL);
		if (!li ) {
			printf("could not find %d\n", (int)t);
		}
	}
	dlst_rls(d1);

	return 0;
}

static int dlst_sorted_ins_test(int num,int wid)
{
	intptr_t t,i;
	dlst_t *d1;
	d1 = dlst_ist(NULL);
	srand(1);

	wid = 40;

	/* dlst_dmp_double(d1); */
	/* printf("VOOR SORTED : %d\n", dlst_issorted(d1)); */
	dlst_sort(d1, &int_object, NULL);
	for (t=0; t< wid; t++) {
		i = intrand(10,100);
		dlst_add(d1, (void *)i);

	}
	
	/* printf("SORTED : %d\n", dlst_issorted(d1)); */
	dlst_rls(d1);

	return 1;
}

static int dlst_sort_test(int num,int wid)
{
	int t,i;
	prm_t *prm;

	/* 10 is the practical limit ?  9 takes about a minute  */
	for (t=2; t< 4; t++) {
		prm = prm_ist(sizeof(int),t);
		for (i=0; i <t; i++) 
			prm_add(prm, (void *)&i);

		prm_set(prm, PERM_PERMUTE);
		prm_func(prm, exhaust, NULL);
		prm_rls(prm);
		/* printf("%d\n", t); */
	}

	return 1;
}

static int dlst_join_test(int num,int wid)
{
	intptr_t t,i;
	dlst_t *d1;
	dlst_t *d2;
	d1 = dlst_ist(&int_object);
	d2 = dlst_ist(&int_object);

	for (t=0; t< 5; t++) {
		i = intrand(10,90);
		dlst_add(d1, (void *)i);

		i = intrand(10,90);
		dlst_add(d2, (void *)i);
	}

	/* dlst_dmp_double(d1); */
	/* dlst_dmp_double(d2); */

	dlst_join(d1,d2);
	/* dlst_dmp_double(d1); */
	/* dlst_dmp_double(d2); */

	dlst_rls(d1);
	dlst_rls(d2);
	return 1;
}

static int dlst_test_add_del(dlst_t *d,int num)
{
	intptr_t rnd;
	int t;

	/* INS num members randomly */
	for (t=0; t< num; t++) {
		int r = intrand(0,dlst_len(d));
		rnd = intrand(10,1000);
		dlst_ins_at(d, NULL, (void *)rnd, r);
		/* dlst_dmp(d); */
	}
	
	/* delete all members from 0 */
	while(dlst_len(d)) {
		dlst_del(d, NULL, LST_FIRST);
		/* dlst_dmp(d); */
	}

	/* add num members  */
	for (t=0; t< num; t++) {
		rnd = intrand(10,1000);
		dlst_add(d, (void *)rnd);
		/* dlst_dmp(d); */
	}
	
	/* randomly delete all members */
	while(dlst_len(d)) {
		int r = intrand(0,dlst_len(d));
		dlst_del(d, NULL, r);
	}

	/* INS num members at start */
	for (t=0; t< num; t++) {
		rnd = intrand(10,1000);
		dlst_ins_at(d, NULL, (void *)rnd, 0);
	}
	/* delete last member */
	while(dlst_len(d)) {
		dlst_del(d, NULL, LST_LAST);
	}

	return 1;
}
	
/** @brief test entry point
	
	internal testing function
 */
int dlst_test(int num, int wid)
{
	int ret=1, t;
	dlst_t *d;

	ret &= dlst_sort_test(num,wid);
	ret &= dlst_sorted_ins_test(num,wid);
	ret &= dlst_join_test(num,wid);

	d = dlst_ist(&int_object);
	for (t=0; t< 300; t++) {
		dlst_test_add_del(d,t);
	}
	dlst_rls(d);
	
	return ret;
}

