#ifndef WIN32
#include <sys/time.h>
#endif
#include <ego.h>
#include <string.h>

time_t maketime(int Y, int M, int D, int h, int m)
{
	struct tm Tm={0};

	Tm.tm_year=Y-1900;
	Tm.tm_mon=M-1;
	Tm.tm_mday=D;
	Tm.tm_hour=h;
	Tm.tm_min=m;

	return mktime(&Tm);
}

#ifdef WIN32
#include <stdio.h>
#include <stdarg.h>

int
snprintf (char *str, int n, char *fmt, ...)
{
	int ret;
va_list a;
va_start (a, fmt);
ret = vsnprintf_s (str, n, n, fmt, a);
va_end (a);
return ret;
}

//#include "config.h"
//#include "wine/port.h"

#if defined(_MSC_VER) || defined(__BORLANDC__)
#define EPOCHFILETIME (116444736000000000i64)
#else
#define EPOCHFILETIME (116444736000000000LL)
#endif

#ifndef HAVE_GETTIMEOFDAY
__inline int gettimeofday(struct timeval *tv, void *dummytz)
{
    FILETIME        ft;
    LARGE_INTEGER   li;
    __int64         t;
    static int      tzflag;

    if (tv)
    {
        GetSystemTimeAsFileTime(&ft);
        li.LowPart  = ft.dwLowDateTime;
        li.HighPart = ft.dwHighDateTime;
        t  = li.QuadPart;       /* In 100-nanosecond intervals */
        t -= EPOCHFILETIME;     /* Offset to the Epoch time */
        t /= 10;                /* In microseconds */
        tv->tv_sec  = (long)(t / 1000000);
        tv->tv_usec = (long)(t % 1000000);
    }
 
    return 0;

#endif /* HAVE_GETTIMEOFDAY */
}


#endif

char *micro_fmt(micro_t mt)
{
	struct tm Tm;
	char *buf;
	time_t t,rest;
	char *sign="+";

	if (mt < 0) {
		sign="-";
		mt *=-1;
	}
	t= mt / 1000000LL;
	rest = mt % 1000000LL;

	/* printf("da was %d\n", t); */
	localtime_s(&Tm,&t);

	buf = calloc(1,100);
	snprintf(buf, 100, "%s%04d/%02d/%02d %02d:%02d:%02d.%06d", sign, Tm.tm_year+1900, Tm.tm_mon+1, Tm.tm_mday, Tm.tm_hour, Tm.tm_min, Tm.tm_sec, (int)rest);

	return buf;
}

void micro_dmp(micro_t mt)
{
	char *tmp=  micro_fmt(mt);
	printf("%s\n", tmp);
	free(tmp);
}

micro_t micro_now(void)
{
	micro_t m;
	struct timeval tv;
	gettimeofday(&tv,NULL);
	m = (long long)tv.tv_sec ;
	m *= 1000000;
	m += tv.tv_usec;
	return m;
}

struct timeval micro_timeval(micro_t mt)
{
	struct timeval tv;
    (tv).tv_sec = (int) (mt / 1000000LL);
    (tv).tv_usec = (int) (mt % 1000000LL);
	return tv;
}

/* some strftime functions don;t support %S etc
	so just do it manually */
char *hms_fmt(time_t t)
{
	char *buf = calloc(1,9);
	struct tm *Tm;

	Tm = localtime(&t);

	if (Tm)
		snprintf(buf,9,"%02d:%02d:%02d", Tm->tm_hour, Tm->tm_min, Tm->tm_sec);
	else 
		snprintf(buf,9,"--:--:--");

	return buf;
}

char *hour_fmt(time_t t)
{
	char *buf = calloc(1,7);
	struct tm *Tm;

	Tm = localtime(&t);
	if (Tm)
		strftime(buf,6,"%H:%M", Tm);
	else 
		snprintf(buf,6,"--:--");

	return buf;
}

char *day_fmt(time_t t)
{
	char *buf = calloc(1,28);
	struct tm *Tm;

	Tm = localtime(&t);
	strftime(buf,8,"%a %d %b", Tm);
	//strftime(buf,28,"%F %a %d %b", Tm);
	
	return buf;
}

