#include <ego.h>

#define LEN 9
#define WID 4

void prm_rls(prm_t * prm)
{
	if (!prm)
		return;

	if (prm->vals)
		da_rls(prm->vals);
	if (prm->seq)
		free(prm->seq);

	free(prm);
}

prm_t * prm_ist(int elmwid, int wid)
{
	prm_t * prm = (prm_t *)calloc(sizeof(prm_t), 1);

	prm->wid  = wid;
	prm->type = 0;
	prm->vals = da_ist(elmwid, 0);
	prm->seq  = (int *)calloc(sizeof(int),wid);

	return prm;
}

int prm_add(prm_t * prm, void *elm)
{
	if (!prm || !prm->vals)
		return -1;

	return da_add(prm->vals, elm);
}

/* traverse all combinations of all numbers */
void *Number(prm_t * prm, prm_fnc_t fnc, void *data)
{
	int t;
	int len = da_len(prm->vals);

	/* keep everything 'flat' and avoid recursion */
	for (t=0; t< prm->wid; t++)
	{
		prm->seq[t] = 0;
	}
	while(prm->seq[0] != len-1) {
		t = prm->wid-1;
		while(t>=0 && prm->seq[t] == len-1) {
			prm->seq[t] = 0;
			t--;
		}
		prm->seq[t]++; 
		/* for (t=0; t< prm->wid; t++) */
			/* printf("%d ", prm->seq[t]); */
		/* printf("\n"); */
	}
	return 0;
}

/* traverse all unique combinations, no matter what order */
void Combine(prm_t * prm, prm_fnc_t fnc, void *data)
{
	int t;
	int len = da_len(prm->vals);

	/* keep everything 'flat' and avoid recursion */
	// initialize members 0,1,2,3....
	for (t=0; t< prm->wid; t++)
	{
		prm->seq[t] = t;
	}
	while(prm->seq[0] != (len-prm->wid)) {
		for (t=0; t< prm->wid; t++)
			printf("%d ", prm->seq[t]);
		printf("\n");
		t = prm->wid-1;
		while(t>=0 && prm->seq[t] == len-(prm->wid-t)) {
			/* prm->seq[t] = 0; */
			t--;
		}
		prm->seq[t]++; 
	}

}

void Permute(prm_t * prm, prm_fnc_t fnc, void *data)
{
	long long limit, e, t, d, tmp;
	long long len = da_len(prm->vals);
	long long *fac=(long long*)calloc (sizeof(long long),(1+len));
	long long *hlp=(long long*)calloc (sizeof(long long),(1+len));
	void **res=(void **)calloc (sizeof(void *),(1+prm->wid+1));

	if (len < prm->wid)
		return;

	fac[0] = 1;
	fac[1] = 1;
	for (t=2; t< len; t++) {
		fac[t] = fac[t-1]*(t);
	}
	fac[t] = fac[t-1]*(t);

	for (t=0; t< len; t++) {
		hlp[t] = t;
	}

	/* keep everything 'flat' and avoid recursion */
	for (t=0; t< prm->wid; t++)
	{
		prm->seq[t] = t;
	}

	//t = prm->wid-1;

	limit = len-prm->wid;
	for (t=fac[limit]; t<= fac[len]; t+=fac[limit]) {
		for (d=limit; d < len; d++) {
			if (t%fac[d] == 0) {
				/* changed = 1; */
				//printf("got %d (%d) (%d)\n", d, t, fac[d]);
				tmp = hlp[len-d-1];
				memmove(&hlp[len-d-1], &hlp[len-d], sizeof(int)*d);
				hlp[len-1] = tmp;
			}
		}
		for (e=0; e< prm->wid; e++) {
			res[e] = (void *)da_get(prm->vals, hlp[e]);
			/* printf("[%d]", hlp[e]); */
		}
		/* printf("\n"); */
		fnc(res, prm->wid, data);
	}
	free(hlp);
	free(fac);
	free(res);
}

void prm_func(prm_t * prm, prm_fnc_t fnc, void *data)
{
	switch (prm->type) {
		case PERM_NUMBER:
			Number(prm, fnc, data);
		break;
		case PERM_PERMUTE:
			Permute(prm, fnc, data);
		break;
		case PERM_COMBINE:
			Combine(prm, fnc, data);
		break;
	}
}

int prm_set(prm_t * prm, int tp)
{
	prm->type = tp;
	return 0;
}

void prm_dmp(prm_t * prm, dmp_fnc_t fnc)
{
	da_dmp(prm->vals, fnc, NULL);
}

int print(void **res, int n, void *data)
{
	int t;
	dmp_fnc_t dmp = data;
	/* printf("was di %p %d %p\n", res, n, data); */
	for (t=0; t < n; t++) {
		dmp(0,res[t],NULL);
	}
	printf("\n");
	return 0;
}

int prm_test(int num, int wid)
{
	int t, *teller;
	prm_t * prm;

#define WID 4

	prm = prm_ist(sizeof(int), WID);
    teller = (int *)calloc(sizeof(int) * WID, 1);

	for (t=0; t< WID; t++) {
		prm_add(prm, (void *)&t);
	}

	//prm_set(prm, PERM_COMBINE);
	prm_set(prm, PERM_PERMUTE);
	prm_func(prm, print, int_dmp); 
	/* prm_func(prm, count, teller); */

	free(teller);
	prm_rls(prm);
	return 1;

	while(teller[0] != LEN-1) {
		t = WID-1;
		while(t>=0 && teller[t] == LEN-1) {
			teller[t] = 0;
			t--;
		}
		teller[t]++; 
		/* for (t=0; t< WID; t++) */
			/* printf("%d ", teller[t]); */
		/* printf("\n"); */
	}

	return 1;
}
