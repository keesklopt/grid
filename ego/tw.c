#ifdef USED_AT_ALL
#include <ego.h>
#include <limits.h>

char *statenames[] = { "no event", "active", "idle" };
char *opnames[] = {
	"NOOP", 
	"UNION", 
	"INTERSECTION", 
	"NOT" 
};

int tp_wlk(tw_t tw, tp_fnc_t fnc)
{
	return 0;
}

// time patterns : constructions of time windows and patterns
tp_t *tp_ist(ts_t start, ts_t end, int oper)
{	
	tp_t *tp = (tp_t *)calloc(sizeof (tp_t),1);
	
	if (start < BOT) start = BOT;
	/* if (end > EOT) end = EOT; */

	tp->oper = oper;
	if (start > end) { 
		tp->tw.start=end;
		tp->tw.end=start;
	} else {
		tp->tw.start=start;
		tp->tw.end=end;
	}

	if (oper != NOOP) 
		tp->tps = da_ist(sizeof(tp_t *),0);
	
	return tp;
}

// these functions work so the result from the last action
// can be used as input for the next,
te_t tp_next_active(tp_t *tp, ts_t after)
{
	te_t s,e= { 0, NOSTATE};
	te_t s1,e1;
	te_t s2,e2;
	tp_t **pat1;
	tp_t **pat2;
	ti_t **ival;

	switch (tp->oper) {
		case NOOP: s.ts = tp->tw.start; break;
		case NOT: 
			pat1 = da_get(tp->tps,0);
			s = tp_next_idle(*pat1, after);
			if (s.ts == BOT) {	
				s = tp_next_idle(*pat1, BOT);
			}
	
			if (after == EVEN_EARLIER) {
				s.ts=BOT;
				s.state=ACTIVE;
			} else {
				if (e.state == NOSTATE) {
					s.ts = EOT;
					s.state= IDLE;
				}
			}
		break;
		case UNION: 
			pat1 = da_get(tp->tps,0);
			pat2 = da_get(tp->tps,1);
			s1 = tp_next_active(*pat1, after);
			s2 = tp_next_active(*pat2, after);

			if (TE_SOONER(s1,s2) )
				return s1;
			else 
				return s2;
		break;
		case INTERSECTION: 
			pat1 = da_get(tp->tps,0);
			pat2 = da_get(tp->tps,1);
			s1 = tp_next_active(*pat1, after);
			s2 = tp_next_active(*pat2, after);
			e1 = tp_next_idle(*pat1, after);
			e2 = tp_next_idle(*pat2, after);

			if (TE_LATER(s1,s2) && TE_LATER(e1,s2))
				return TE_LATER(s1,s2) ? s1 : s2;
			else {
				s.state = NOSTATE;
				s.ts = NOTIME;
				return s;
			}
		break;
		case REPEAT:
			pat1 = da_get(tp->tps,0);
			ival = da_get(tp->tps,1);
			if (!(*ival)->pat) {
				s = tw_next_active((*pat1)->tw,after,(*ival)->dur);
			} else {
				s1 = tp_next_active(*pat1, after);
			}
		break;
		default: s.ts = BOT; break;
	}

	s.state=ACTIVE;
	if (s.ts <= after) {
		s.ts=NOTIME;
		s.state=NOSTATE;
	}
	return s;
}

te_t tp_next_idle(tp_t *tp, ts_t after)
{
	te_t e,s;
	te_t s1,s2;
	te_t e1,e2;
	tp_t **pat1;
	tp_t **pat2;
	ti_t **ival;

	switch (tp->oper) {
		case NOOP: e.ts = tp->tw.end; break;
		case NOT: 
			pat1 = da_get(tp->tps,0);
			if (!pat1) e.ts = NOTIME;
			else {
				s = tp_next_active(*pat1, after);
				if (s.ts == NOTIME) {
					e.state = IDLE;
					e.ts = EOT;
				} else 
					e=s; 
			}
		break;
		case UNION: 
			pat1 = da_get(tp->tps,0);
			pat2 = da_get(tp->tps,1);
			s1 = tp_next_active(*pat1, after);
			s2 = tp_next_active(*pat2, after);
			e1 = tp_next_idle(*pat1, after);
			e2 = tp_next_idle(*pat2, after);

			if (TE_SOONER(s1,s2) && TE_SOONER(e1,s2))
				return TE_SOONER(e1,e2) ? e1 : e2;
			else 
				return TE_LATER(e1,e2) ? e1 : e2;
		break;
		case INTERSECTION: 
			pat1 = da_get(tp->tps,0);
			pat2 = da_get(tp->tps,1);
			s1 = tp_next_idle(*pat1, after);
			s2 = tp_next_idle(*pat2, after);

			if (TE_SOONER(s1,s2) )
				return s1;
			else 
				return s2;
		break;
		case REPEAT: 
			pat1 = da_get(tp->tps,0);
			ival = da_get(tp->tps,1);
			if (!(*ival)->pat) {
				e = tw_next_idle((*pat1)->tw,after,(*ival)->dur);
			} else {
				s1 = tp_next_idle(*pat1, after);
			}
		break;
		default: e.ts = BOT; break;
	}

	e.state=IDLE;
	if (e.ts <= after) {
		e.ts=NOTIME;
		e.state=NOSTATE;
	}
	return e;
}

// previous implies backwards in time, so the notion of 
// becoming active or idle is also reversed !!
te_t tp_prev_active(tp_t *tp, ts_t before)
{
	te_t s;

	switch (tp->oper) {
		case NOOP: s.ts = tp->tw.end; break;
		default: s.ts = BOT; break;
	}

	s.state=ACTIVE;
	if (s.ts >= before) {
		s.ts=NOTIME;
		s.state=NOSTATE;
	}
	return s;
}

te_t tp_prev_idle(tp_t *tp, ts_t before)
{
	te_t e;

	switch (tp->oper) {
		case NOOP: e.ts = tp->tw.start; break;
		default: e.ts = BOT; break;
	}

	e.state=IDLE;
	if (e.ts >= before) {
		e.ts=NOTIME;
		e.state=NOSTATE;
	}
	return e;
}

// any event : (does everything double !!)
te_t tp_next_event(tp_t *tp, ts_t after)
{
	te_t s,e;

	s = tp_next_active(tp,after);
	e = tp_next_idle(tp,after);

	return TE_SOONER(s,e) ? s : e;
}

te_t tw_next_active(tw_t tw, ts_t after, ts_t dur)
{
	te_t e;
	
	e.ts = (((after - tw.start) / dur ) +1 ) * dur;
	e.ts += tw.start;
	e.state = ACTIVE;
	return e;
}

te_t tw_next_idle(tw_t tw, ts_t after, ts_t dur)
{
	te_t e;
	
	e.ts = (((after - tw.end) / dur ) +1 ) * dur;
	e.ts += tw.end;
	e.state = IDLE;
	return e;
}

te_t tp_first_event(tp_t *tp)
{

	return tp_next_event(tp,EVEN_EARLIER);
}

// better do 1 tp_first_event test the state and then 
// use this function
te_t tp_next_alternate(tp_t *tp, te_t after)
{
	te_t t;

	if (after.state == ACTIVE) 
		t = tp_next_idle(tp,after.ts);
	else
		t = tp_next_active(tp,after.ts);

	return t;
}

te_t tp_prev_event(tp_t *tp, ts_t after)
{
	te_t s,e;

	s = tp_prev_active(tp,after);
	e = tp_prev_idle(tp,after);

	return TE_LATER(s,e) ? s : e;
}

void te_dmp(te_t evt)
{
	char *str=NULL;

	if (evt.ts == BOT) str="BOT";
	if (evt.ts == EOT) str="EOT";
	if (str) {
		printf("%s: %s\n", statenames[evt.state], str);
	} else {
		printf("%s: %lld\n", statenames[evt.state], evt.ts);
	}
}

void tp_traverse(tp_t *tp, tp_fnc_t fnc)
{
	te_t s;

	s = tp_first_event(tp);  // ey? before beginging of time ?
	if (s.state != ACTIVE) return; // must be ACTIVE

	while ( s.state != NOSTATE) 
	{
		fnc(s,tp,NULL);
		s = tp_next_alternate(tp,s);
	}
	
}

void tp_rls(tp_t *tp)
{
	if (!tp) return;
	if (tp->tps) da_rls(tp->tps);
	free(tp);
}

// to save a lot of testing , force these functions :
tp_t *tp_not(tp_t *bot)
{
	tp_t *top=tp_ist(BOT, EOT, NOT);
	tp_add(top,bot);
	return top;
}

tp_t *tp_or(tp_t *left,tp_t *right)
{
	tp_t *top=tp_ist(BOT, EOT, UNION);
	tp_add(top,left);
	tp_add(top,right);
	return top;
}

tp_t *tp_and(tp_t *left,tp_t *right)
{
	tp_t *top=tp_ist(BOT, EOT, INTERSECTION);
	tp_add(top,left);
	tp_add(top,right);
	return top;
}

tp_t *tp_repeat(tp_t *what,ti_t *how)
{
	tp_t *top=tp_ist(BOT, EOT, REPEAT);
	tp_add(top,what);
	tp_add(top,how);
	return top;
}

tp_t *tp_union(tp_t *left, tp_t *right) { return tp_or(left,right); }
tp_t *tp_intersection(tp_t *left, tp_t *right) { return tp_and(left,right); }

// don't provide the manual add function
void tp_add(tp_t *top, void *bot)
{
	if (!top || !bot) return;
	da_add(top->tps, &bot);
}

void *dmp(te_t te, tp_t *tp,void *data)
{
	te_dmp(te);

	return NULL;
}

ti_t *ti_ist(tp_t *pat, ts_t dur)
{
	ti_t *p = (ti_t *)calloc(sizeof(ti_t),1);

	p->pat=pat;
	p->dur=dur;

	return p;
}

int tw_test(int x, int y)
{
	te_t evt;

	int ret=1;
	tp_t *a;
	tp_t *b = tp_ist(100,301, NOOP);
	tp_t *c = tp_ist(300,400, NOOP);
	tp_t *d;
	tp_t *e = tp_ist(0,300000000, NOOP);
	tp_t *not;
	tp_t *rep;
	ti_t *year;

		
	year = ti_ist(NULL, 365*24*60*60);

	a = tp_or(b,c);
	d = tp_and(b,c);
	
	not = tp_not(a);

	rep = tp_repeat(b,year);
	d = tp_and(rep,e);

	// traverse tests
	tp_traverse(d,dmp);

exit(0);

	// test next and prev functions (especially the border points)
	evt = tp_next_event(b,99);
	if (evt.state != ACTIVE || evt.ts != 100) ret = 0;
	evt = tp_next_event(b,100);
	if (evt.state != IDLE || evt.ts != 200) ret = 0;
	evt = tp_next_event(b,199);
	if (evt.state != IDLE || evt.ts != 200) ret = 0;
	evt = tp_next_event(b,200);
	if (evt.state != NOSTATE || evt.ts == 200) ret = 0;
	
	evt = tp_prev_event(b,100);
	if (evt.state != NOSTATE || evt.ts == 100) ret = 0;
	evt = tp_prev_event(b,101);
	if (evt.state != IDLE || evt.ts != 100) ret = 0;
	evt = tp_prev_event(b,200);
	if (evt.state != IDLE || evt.ts != 100) ret = 0;
	evt = tp_prev_event(b,201);
	if (evt.state != ACTIVE || evt.ts != 200) ret = 0;

	
	return ret;
}

#endif
