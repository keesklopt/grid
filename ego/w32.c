#ifdef WIN32

#include <ego.h>

static wsainit=0;

void StartSockets(void)
{
    WSADATA lpw;
	char hname[64];


    if (!wsainit)  {
        WSAStartup(MAKEWORD(2,2), &lpw);
        wsainit = 1;
        printf("WSA is NOW initialized !!\n");
    }

gethostname(hname,64);

printf("socket lib, host name is %s\n", hname);

}

#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
void HandleError(char *s)
{
    printf("An error occurred in running the program.\n");
    printf("%s\n",s);
    printf("Error number %x\n.",GetLastError());
    printf("Program terminating.\n");
    exit(1);
}
/* mainly to shut up windows compilers */
/* nr_ : non reentrant 
 * r_  : reentrant
 */

struct tm _tm;
struct tm* nr_localtime(const time_t *tm)
{
	errno_t e;

	e = localtime_s(&_tm, tm);

	return &_tm;
}

#define BUFLEN 100
char buf[BUFLEN];

char *nr_ctime(const time_t *clock)
{

	errno_t e;

	e = ctime_s(buf, BUFLEN, clock);
	return buf;
}

#endif
