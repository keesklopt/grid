#include <ego.h>

tok_t *tok_ist(char *name)
{
	tok_t *tok = calloc(sizeof(tok_t),1);

	tok->name = strdup(name);

	return tok;
}

void tok_rls(tok_t *tok)
{
	if (!tok) return;

	if (tok->name) free(tok->name);

	/* return tok; */
}

int tok_test(int len, int wid)
{
	tok_t *tok;

	tok=tok_ist("file");
	/* tok_add(tok, SEP, ","); */
	/* tok_add(tok, LAST, ","); */

	tok_rls(tok);

	return 1;
}
