/** @file

	dynamic memory buffer

	description : dynamic memory, stretches and shrinks when needed 
    do NOT use data directly because of the behaviour of realloc!!

*/

#include <ego.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** @brief right set bit sequence
  * set bit sequence : a contigues sequence of bits, kept in a char array 
  * used for masking when putting bits into a dynamic array
  */
unsigned char r_set[] = { 0x00,0x01,0x03,0x07,0x0F,0x1F,0x3F,0x7F,0xFF};
/** @brief right clear bit sequence
  * clear sequence : a contigues sequence of bits, kept in a char array 
  * used for masking when clearing bits from a dynamic array
  */
unsigned char r_clr[] = { 0xFF,0xfe,0xfc,0xf8,0xf0,0xe0,0xc0,0x80,0x00};
/** @brief left set bit sequence
  * set bit sequence : a contigues sequence of bits, kept in a char array 
  * used for masking when putting bits into a dynamic array
  */
unsigned char l_set[] = { 0x00,0x80,0xc0,0xe0,0xf0,0xf8,0xfc,0xfe,0xff};
/** @brief left clear bit sequence
  * clear sequence : a contigues sequence of bits, kept in a char array 
  * used for masking when clearing bits from a dynamic array
  */
unsigned char l_clr[] = { 0xFF,0x7f,0x3f,0x1f,0x0f,0x07,0x03,0x01,0x00};

/** @brief instantiate buffer
 *
 * instantiate a dynamic buffer
 *
 * @param initialsize start wit ha buffer this size instaed of 0 (default)
 * @return pointer to a dynamic buffer
 */
buf_t *buf_ist(int initialsize)
{
    buf_t *dm = calloc(sizeof(buf_t),1);

	if (!dm) { err_set(ERR_MEM); return NULL;}
    dm->bytes = initialsize;
	if (initialsize)  {
    	dm->data = calloc(initialsize,1);
		if (!dm->data) { err_set(ERR_MEM); return NULL;}
	}
	dm->cur = dm->data;

    return dm;
}

#ifdef USE_DATABLOCK

int buf_traverse_d(instance_t * i, type_t *tp, int pos, char *data, int dummy, gfunc_t fnc)
{
	int len,diff;
	int structcounter= 0;
	type_t *base_type, *member_type;

	/* it is always handy to cast back to your struct */
	buf_t *bp = (buf_t *)data;

/* -------- buf_t.bytes --------- */
	/* this part is still generic  : member int bytes */
	member_type = struct_type_get(tp, structcounter++); 
	gen_traverse_d(i,member_type,pos,(char*)data,1,fnc);
	/* if we write the difference between data and cur here we can restore it later !!  */
	STRUCT_SHIFT(i,pos,data,member_type)

/* -------- buf_t.cur --------- */
	len = 0;
	/* this is member char * cur, a loose pointer (no real data so len = 0) */
	member_type = struct_type_get(tp, structcounter++);
	base_type = type_get(member_type->basetype);
	diff = bp->cur - bp->data;
	if (diff < 0 || diff > bp->bytes) 
		diff = 0;
	buf_set_int(i->bufp, pos ,sizeof(REF_TYPE), diff);
	STRUCT_SHIFT(i,pos,data,member_type)

/* -------- buf_t.data --------- */
	/* we need  the length now, here's where casting to buf_t* comes in handy */
	len = bp->bytes;
	base_type = struct_type_get(tp, structcounter++);
	gen_traverse_ptr_d(i,base_type,pos,(char*)data,len,fnc);
	STRUCT_SHIFT(i,pos,data,base_type)
	return 0;
}

/* unpack/traverse on buffer */
int buf_traverse_b(instance_t * i, type_t *tp, int pos, char *data, int dummy, gfunc_t fnc)
{
	int len, diff;
	int structcounter= 0;
	type_t *base_type, *member_type;

	buf_t *bp = (buf_t *)data;

	/* this part is still generic  : member int bytes */
	member_type = struct_type_get(tp, structcounter++); 	
	gen_traverse_b(i,member_type,pos,data,1,fnc);
	/* ALWAYS use STRUCT_SHIFT, because it ALIGNS */
	STRUCT_SHIFT(i,pos,data,member_type)

	len = 0;
	/* this is member char * cur, a loose pointer (no real data so len = 0) */
	member_type = struct_type_get(tp, structcounter++);
	base_type = type_get(member_type->basetype);
	/* gen_traverse_arr_d(i,base_type,pos,(char*)data,len,fnc); */
	diff = buf_get_int(i->bufp, pos); 
	bp->cur = bp->data+diff;

	/* ALWAYS use STRUCT_SHIFT, because it ALIGNS */
	STRUCT_SHIFT(i,pos,data,member_type)

	/* we need  the length now, here's where casting to buf_t comes in handy */
	len = bp->bytes;
	base_type = struct_type_get(tp, structcounter++);
	gen_traverse_ptr_b(i,base_type,pos,(char*)data,len,fnc);
	STRUCT_SHIFT(i,pos,data,base_type)

	return 0;
}

int buf_data(void)
{
	type_t *type;

    unsigned int bytes;
	char *cur; 
    char *data;
	type = type_ist("buf_t", CALC_SIZE, 0, NULL, buf_traverse_b, buf_traverse_d);
	/* buf_t *bufp; */
	/* put bit fields in one int, also we only do signed  */
	type_add(type, "bytes", "int");
	type_add(type, "cur", "char *");
	type_add(type, "data", "char *");
	type_reg(type);

	type_reg_ptr("buf_t *", "buf_t", gen_traverse_b, gen_traverse_d);

	return 0;
}

#endif

/** @brief duplicate as string
 *
 * duplicate a dynamic buffer content into a string
 *
 * @param orig original or source buffer
 * @return newly duplicated string
 */
char *buf_strdup(buf_t *orig)
{
	char *buf;
	intptr_t count=0;
	if (!orig || !orig->data) return NULL;
	
	while (count < orig->bytes && orig->data[count] ) count++;
	
	buf = calloc(count+1,1);
	memcpy(buf,orig->data, count);
	buf[count] = '\0';
	
	return buf;
}

/** @brief duplicate
 *
 * duplicate a dynamic buffer
 *
 * @param orig  original or source buffer
 * @return newly duplicated buffer
 */
buf_t *buf_dup(buf_t *orig)
{
    buf_t *dm = calloc(sizeof(buf_t),1);

	if (!dm) { err_set(ERR_MEM); return NULL;}

    dm->bytes = orig->bytes;
	if (dm->bytes)  {
    	dm->data = calloc(dm->bytes,1);
		if (!dm->data) { err_set(ERR_MEM); return NULL;}
		memcpy(dm->data, orig->data, dm->bytes);
	}
	dm->cur = dm->data;


    return dm;
}

/** @brief read from buffer
 *
 * read-like interface to read from a dynamic buffer, the current pointer will
 * be advanced by the number of bytes read
 *
 * @param stream dynamic buffer to read from  
 * @param ptr pointer to a buffer to read into
 * @param nbytes number of bytes to read
 * @return number of bytes actually read
 */
intptr_t buf_read(buf_t *stream, void *ptr, intptr_t nbytes)
{
	intptr_t rest;
	buf_t *bp = (buf_t *)stream;

	rest = bp->bytes - (bp->cur - bp->data);
	if (nbytes > rest) nbytes = rest;

	memcpy(ptr, bp->cur, nbytes);
	bp->cur += nbytes;
	return nbytes;
}

/** @brief write to buffer
 *
 * write-like interface to write to a dynamic buffer, the current pointer will
 * be advanced by the number of bytes written
 *
 * @param stream dynamic buffer to write to
 * @param ptr pointer to a buffer to write from
 * @param nbytes number of bytes to write
 * @return number of bytes actually written
 */
intptr_t buf_write(buf_t *stream, void *ptr, intptr_t nbytes)
{
	intptr_t rest;
	buf_t *bp = (buf_t *)stream;

	rest = bp->bytes - (bp->cur - bp->data);
	if (nbytes > rest) buf_resize(bp, (bp->bytes - rest) +  nbytes);

	memcpy(bp->cur, ptr, nbytes);
	bp->cur += nbytes;
	return nbytes;
}

/* TODO : the rest (lseek etc ...) */

	
/** @brief read from buffer
 *
 * fread-like interface to read from a dynamic buffer, the current pointer will
 * be advanced by the number of bytes read
 *
 * @param ptr pointer to a buffer to read into
 * @param size size of 1 member to read
 * @param nmemb number of members to read
 * @param stream dynamic buffer to read from  
 * @return number of bytes actually read
 */
size_t buf_fread(void *ptr, size_t size, size_t nmemb, buf_t *stream)
{
	intptr_t rest;
	intptr_t nbytes = size * nmemb;
	buf_t *bp = (buf_t *)stream;

	rest = bp->bytes - (bp->cur - bp->data);
	if (nbytes > rest) nbytes = rest;

	/* printf("writing %d on a buffer long %d (%d)\n", nbytes, buf_len(bp), bp->cur-bp->data); */
	memcpy(ptr, bp->cur, nbytes);
	bp->cur += nbytes;
	return nbytes;
}

/** @brief write to buffer
 *
 * fwrite-like interface to write to a dynamic buffer, the current pointer will
 * be advanced by the number of bytes written
 *
 * @param ptr pointer to a buffer to write from
 * @param size size of 1 member
 * @param nmemb number of members to write
 * @param stream dynamic buffer to write to
 * @return number of bytes actually written
 */
size_t buf_fwrite(void *ptr, size_t size, size_t nmemb, buf_t *stream)
{
	intptr_t rest;
	intptr_t nbytes = size * nmemb;
	buf_t *bp = (buf_t *)stream;

	rest = bp->bytes - (bp->cur - bp->data);
	if (nbytes > rest) buf_resize(bp, (bp->bytes - rest) +  nbytes);

	memcpy(bp->cur, ptr, nbytes);
	bp->cur += nbytes;
	return nbytes;
}

/** @brief seek buffer
 *
 * fseek-like interface to a dynamic buffer, the current pointer will
 * be advanced to the position specified
 *
 * @param stream dynamic buffer to seek in
 * @param offset what amount to offset depending on the whence parameter
 * @param whence If whence is set to SEEK_SET, SEEK_CUR, or SEEK_END, the offset  is  relative  to the  start of the buffer, the current position indicator, or the end of the buffer
 * @return 0 on succes or -1 on failure
 */
int buf_fseek(buf_t *stream, intptr_t offset, int whence)
{
	intptr_t pos = stream->cur-stream->data;
	
	if (whence == SEEK_CUR) offset += pos;
	if (whence == SEEK_END) offset += stream->bytes;

	if (offset < 0 || offset >= (int)stream->bytes) return -1;
	stream->cur = stream->data+offset;
	return 0;
}

/** @brief current position
 *
 * ftell-like interface to a dynamic buffer, the current pointer will
 * be returned
 *
 * @param bp dynamic buffer 
 * @return place in the buffer where the current position resides
 */
intptr_t buf_ftell(buf_t *bp)
{
	return bp->cur-bp->data;
}

/** @brief rewind buffer
 *
 * rewind-like interface to a dynamic buffer, the current pointer will
 * be set t the beginning of the buffer
 *
 * @param stream dynamic buffer to rewind
 */
void buf_rewind(void *stream)
{
	buf_t *bp = (buf_t *)stream;
	
	bp->cur = bp->data;
}

/** @brief dump buffer
 *
 * make a dump of the buffer content in hexadecimal form
 *
 * @param bp dynamic buffer to dump
 */
void buf_dmp(buf_t *bp)
{
	int t, u;
	intptr_t max;
	intptr_t len = bp->bytes;
	unsigned char *buf = (unsigned char *)bp->data;

    lprintf(LOG_ALOT, "Dyn mem : size is %d\n", bp->bytes);

	max = ((len+16)/16)*16;

	for (t=0; t<= max; t++) {
		if (t%16 == 0 || t == max) {
			if (t) {
				for (u=t-16; u< t; u++) {
					if (u >= len) 
						lprintf(LOG_DEBUG, " ");
					else if(buf[u] < ' ' || buf[u] > '~')
						lprintf(LOG_DEBUG, ".");
					else
						lprintf(LOG_DEBUG, "%c", buf[u]);
				}
			}
				
			if (t != max)
				lprintf(LOG_DEBUG, "\n %-4x:", t);
		}
		if (t >= len) 
			lprintf(LOG_DEBUG, "   ");
		else 
			lprintf(LOG_DEBUG, "%2.2x ", buf[t]);
	}
	lprintf(LOG_DEBUG, "\n");
}

/** @brief clear buffer
 *
 * clear a dynamic buffer, fill all data with 0
 *
 * @param b dynamic buffer to clear
 * @return 0
 */
int buf_clear(buf_t *b)
{
	if (!b) return err_set(ERR_NULL);


	if (!b->data) return 0;
	buf_fill(b,'\0');
	b->cur = b->data;
	return 0;
}

/** @brief reset buffer
 *
 * reset buffer for writing ? reset current pointer and set byte 0 to 0
 *
 * @param b dynamic buffer to reset to
 * @return 0 on succes
 */
int buf_reset(buf_t *b)
{
	if (!b) return err_set(ERR_NULL);

	if (!b->data) return 0;
	b->data[0]= 0;
	b->cur = b->data;
	return 0;
}

/** @brief resize buffer
 *
 * resize a dynamic buffer 
 *
 * @param b dynamic buffer to resize
 * @param bytes new size
 * @return  0 on success, error code on failure
 */
intptr_t buf_resize(buf_t *b, intptr_t bytes)
{
	intptr_t pos;
	if (!b) return err_set(ERR_NULL);

	pos = b->cur - b->data;

	if (b->data) {
		b->cur =
		b->data = realloc(b->data, bytes);
	} else {
		b->cur = 
		b->data = calloc(bytes,sizeof(char));
		if (!b->data) return err_set(ERR_MEM);
	}

	/* reset cur in case of a 'real'loc (the pointer changed ;) */
	b->cur = b->data + pos;
	b->bytes = bytes;
	return 0;
}

/** @brief stretch a buffer
 *
 * stretch a dynamic buffer from a certain position outward, this will create
 * a new section inside a buffer
 *
 * @param b dynamic buffer to stretch
 * @param offset at which position to stretch
 * @param bytes how many bytes to expand inside the buffer
 * @return 0 on success, error codes otherwise
 */
int buf_stretch(buf_t *b, intptr_t offset, intptr_t bytes)
{
	intptr_t oldsize = b->bytes;
	if (offset < 0) offset = oldsize;
	if (buf_resize(b, b->bytes+bytes)) return err_set(ERR_MEM);

	/* mark the new data, .... or ... shut up valgrind. */
	/* memset(&b->data[offset], '\0', bytes); */

	memmove(&b->data[offset+bytes], &b->data[offset], oldsize-offset);

	return 0;
}

/** @brief shrink a buffer
 *
 * shrink a dynamic buffer at a certain location, this will cut a part out 
 * of a buffer
 *
 * @param b dynamic buffer to shrink
 * @param offset place at which to start the shrink operation
 * @param bytes the number of bytes to cut out of the buffer from offset
 * @return 0 on success, error code otherwise
 */
int buf_shrink(buf_t *b, intptr_t offset, intptr_t bytes)
{
	if (!b) return err_set(ERR_NULL);
	if (b->bytes < offset+bytes) return err_set(ERR_RANGE);
	memmove(&b->data[offset], &b->data[offset+bytes], b->bytes-offset-bytes);
	if (buf_resize(b, b->bytes-bytes)) return err_set(ERR_MEM);

	return ERR_NONE;
}

/** @brief current position
 *
 * give the current position in a dynamic buffer
 *
 * @param b dynamic buffer 
 * @return place in the buffer where the current position resides
 */
intptr_t buf_pos(buf_t *b)
{
    return b->cur - b->data;
}

/** @brief bufer length
 *
 * total lengt/size of a dynamic buffer, 
 *
 * @param b dynamic buffer 
 * @return size in bytes
 */
intptr_t buf_len(buf_t *b)
{
    return b->bytes;
}

/** @brief delete
 *
 * delete part of a dynamic buffer, same as buf_shrink
 *
 * @param buf dynamic buffer delet from 
 * @param off offset from where to delete
 * @param bytes how many bytes to delete
 * @return 0 on success, error code otherwise
 */
int buf_del(buf_t *buf, int off, int bytes)
{
	if (!buf) return err_set(ERR_NULL);

	buf_shrink(buf, off, bytes);

	return ERR_NONE;
}

/** @brief insert into buffer
 *
 * insert data into a dynamic buffer, the buffer will be stretched to 
 * include the data
 *
 * @param buf dynamic buffer to insert into
 * @param off offset where to start the insertion
 * @param bytes how manu bytes to insert
 * @param data insertion data
 * @return place in the buffer where the current position resides
 */
int buf_ins(buf_t *buf, intptr_t off, intptr_t bytes, void *data)
{
	if (!buf) return err_set(ERR_NULL);

	buf_stretch(buf, off, bytes);
	if (data == NULL) return ERR_NONE; 
	buf_set_str(buf, off, bytes, data);

	return ERR_NONE;
}

/** @brief current position
 *
 * fputc-like interface to a dynamic buffer, 
 *
 * @param val char value to put
 * @param buf dynamic buffer to write to
 * @return place in the buffer where the current position resides
 */
intptr_t buf_fputc(char val, buf_t *buf)
{
	intptr_t pos, ret;
	if (!buf) return err_set(ERR_NULL);

	pos = buf->cur - buf->data;
	if ( (pos + 1) > buf->bytes)
		if (buf_resize(buf, pos+1)) 
			return err_errno();

    ret = buf_set_int(buf, pos, 1, val);
	buf->cur ++;

	return ret;
}

/** @brief current position
 *
 * putc-like interface to a dynamic buffer, 
 *
 * @param val char value to put
 * @param buf dynamic buffer to write to
 * @return place in the buffer where the current position resides
 */
intptr_t buf_putc(char val, buf_t *buf)
{
	return buf_fputc(val,buf);
}

/** @brief add to buffer
 *
 * add data to a dynamic buffer, this will stretch /expand the buffer size
 *
 * @param buf dynamic buffer to add to
 * @param txt null terminated string to add
 * @return place in the buffer where the current position resides
 */
intptr_t buf_add(buf_t *buf,char *txt)
{
	intptr_t len, pos, ret;
	if (!buf || !txt) return err_set(ERR_NULL);
	len = strlen(txt);

	pos = buf->cur - buf->data;
	if ( (pos + len) > buf->bytes)
		if (buf_resize(buf, pos+len)) 
			return err_errno();

	ret = buf_set_str(buf, pos, len, txt);
	buf->cur += len;

	return ret;
}

/** @brief current position
 *
 * fputs-like interface to a dynamic buffer, 
 *
 * @param txt string to write
 * @param buf dynamic buffer to put to
 * @return place in the buffer where the current position resides
 */
intptr_t buf_fputs(char *txt, buf_t *buf)
{
	return buf_add(buf,txt);
}

/** @brief get data pointer
 *
 * get a pointer at position in a dynamic buffer, don;t rely on this data after
 * after calling buf_stretch, buf_shrink, buf_write ... etc 
 *
 * @param buf dynamic buffer to read from
 * @param offs where the pointer should start
 * @return place in the buffer where the current position resides
 */
char *buf_get(buf_t *buf,intptr_t offs)
{
    return &buf->data[offs];
}

/** @brief get a string
 *
 * get a string from some position in a dynamic buffer,
 *
 * @param dst destination fro the string, it is expected this contains enough 
 *        room to store bytes bytes
 * @param src dynamic buffer to read from
 * @param offset offset in the buffer where to read from
 * @param bytes number of string bytes to read
 * @return place in the buffer where the current position resides
 */
void buf_get_str(char *dst,buf_t *src, intptr_t offset, intptr_t bytes)
{
    memcpy(dst, &src->data[offset], bytes);
}

/** @brief get integer
 *
 * get an integere from a position in a dynamic buffer
 *
 * @param buf dynamic buffer to read from
 * @param offs at what position to get val
 * @return place in the buffer where the current position resides
 */
int buf_get_int(buf_t *buf,intptr_t offs)
{
    int val=0;
    memcpy(&val, &buf->data[offs], sizeof(int));

    return val;
}

/** @brief set integer
 *
 * set an integer at offset in a dynamic buffer
 *
 * @param buf dynamic buffer to write to
 * @param offs what position to set val
 * @param bytes number of bytes to write
 * @param val integer data
 * @return place in the buffer where the current position resides
 */
int buf_set_int(buf_t *buf,intptr_t offs,int bytes, intptr_t val)
{
#ifdef B_ENDIAN
    val <<= 64-(bytes*8);
#endif
    /* lprintf(LOG_ALOT, "set %x at %d %d bytes \n", val , offs, bytes); */
    memcpy(&buf->data[offs], &val, bytes);

	return ERR_NONE;
}

/** @brief set a string
 *
 * set a string at a certain offset in a dynamic buffer
 *
 * @param buf dynamic buffer to write to
 * @param offs what position to set val
 * @param len length of the string to set
 * @param val string data
 * @return place in the buffer where the current position resides
 */
intptr_t buf_set_str(buf_t *buf,intptr_t offs,intptr_t len, char *val)
{
	if (!buf || !val) return err_set(ERR_NULL);
	if (len < 1) len = strlen(val);

    memcpy(&buf->data[offs], val, len);

	return 0;
}

/** @brief set a character
 *
 * set a character at a certain offset in a dynamic buffer
 *
 * @param buf dynamic buffer to write to
 * @param offs what position to set val
 * @param val what character to set 
 * @return place in the buffer where the current position resides
 */
int buf_set(buf_t *buf,int offs,char val)
{
    buf_set_int(buf, offs, 1, val);

	return ERR_NONE;
}

/** @brief set bit sequence inside the buffer
 *
 * write or set a bit sequence in a dynamic buffer
 *
 * @param buf dynamic buffer to write to
 * @param bs bit sequence to write
 * @return 0 on succes -1 otherwise
 */
int buf_set_bits(buf_t buf,bitseq_t *bs)
{
    char *ptr;
    unsigned char c, cleanmask;
    unsigned int uval=0;
    int lsb, lastbit, end;

    end = (bs->bitrow->shift+bs->bitrow->bits)/ MEM_SIZE;
    lastbit = (bs->bitrow->shift+bs->bitrow->bits)% MEM_SIZE;

    ptr = (char *)&buf.data[end];
    /* tail bits */
    c = uval & r_set[lastbit];
    c <<= (MEM_SIZE-lastbit);
    lsb = EGO_MIN(bs->bitrow->bits,lastbit);
    cleanmask = r_set[lsb];
    cleanmask <<= (MEM_SIZE-lastbit);
    cleanmask = ~cleanmask;

    while (bs->bitrow->bits > 0) {
        *ptr &= cleanmask;
        *ptr |= c;

        uval >>= lastbit;

        bs->bitrow->bits -=lastbit;
        lastbit = EGO_MIN(bs->bitrow->bits,8);
        cleanmask = r_clr[lastbit];
        c = uval & r_set[lastbit];

        ptr--;
    }
    if (uval) {
        /* printf("%d left (value does not fit in %d bits !!\n", uval, ba.bits); */
        return -1;
    }
    return 0;
}

/** @brief return pointer at position
 *
 * return a pointer to the data at position pos, don't rely on the value of 
 * this pointer after using stretching,shrinking or write functions  !!
 *
 * @param dm dynamic buffer 
 * @param pos at what position should the data start
 * @return pointer to internal data
 */
char *buf_ptr(buf_t *dm, int pos)
{
    return &dm->data[pos];
}

/** @brief fill buffer
 *
 * fill a dynamic buffer with the given character
 *
 * @param b dynamic buffer to fill
 * @param def character to fill the buffer with
 * @return 0 in success, error code otherwise
 */
int buf_fill(buf_t *b,unsigned char def)
{
    memset(b->data, def, b->bytes);
	return ERR_NONE;
}

/** @brief release buffer
 *
 * release a dynamic buffer, free up all used data
 *
 * @param b dynamic buffer to release
 */
void buf_rls(buf_t *b)
{
	if (!b) return;
	/* printf("FREEING : %p\n", b->data); */
    if (b->data) free(b->data);
    free(b);
}

/** @brief test buffer length */
#define BLEN 100

/* testing section */

/** @brief test duplcation
 *
 * internal test function
 */
int buf_test_dup(int num, int len)
{
	buf_t *c;
	int t;
	buf_t *b = buf_ist(len);

	for (t=0; t< num; t++) {
		c = buf_dup(b);
		buf_rls(b);
		b = c;
	}

	buf_rls(c);
	return 1;
}

/** @brief test random fill
 *
 * internal test function
 */
int buf_test_randfill(buf_t *bufp, int min, int max)
{
	int t, r;
	char c[2]= { 0,0 };

	if (!bufp) return 0;

	r = intrand(min, max);

	for (t=0; t< r; t++) {
		c[0] = intrand('a','z');
		buf_add(bufp, c);
	}

	return 1;
}

/** @brief test data
 *
 * internal test function
 */
int buf_test_data(int num, int len)
{
	buf_t *b = buf_ist(0);
	buf_t *bb;
	instance_t *ip;

	len=10;
	num=10;

	data_init(1);

	buf_test_randfill(b, 0, len);
	buf_dmp(b);

	ip= instance_ist("buf_t *");
	instance_pack(ip, (void *)&b,1);

	buf_dmp(b);
	buf_rls(b);

	instance_unpack(ip, (void *)&bb);
	buf_dmp(bb);
	/* buf_rls(b); */

	/* now free this, it was allocated in instance_unpack */
	free(bb->data);
	free(bb);

	instance_rls(ip);

	return 1;
}

/** @brief test strdup functionality
 *
 * internal test function
 */
int buf_test_strdup(int num, int len)
{
	buf_t *c;
	int t;
	int rnd;
	buf_t *b = buf_ist(0);
	char *bufjo;

	/* test limits  */
	/* empty buffer : */
	bufjo = buf_strdup(b);
	buf_rls(b);
	free(bufjo);

	b = buf_ist(0);
	bufjo = buf_strdup(b);
	free(bufjo);
	buf_rls(b);

	/* test bulk (random) */
	b = buf_ist(0);
	for (t=0; t< num; t++) {
		c = buf_dup(b);
		buf_test_randfill(c, 0, len);
		bufjo = buf_strdup(c);
		rnd = intrand(0,1) ;
		if (rnd < 1) {
			buf_rls(c);
			free(bufjo);
		} else {
			free(bufjo);
			buf_rls(c);
		}
	}
	buf_rls(b);

	return 1;
}

/** @brief test addition deletion
 *
 * internal test function
 */
int buf_test_add_del(int num)
{
	int t,x,l;
	intptr_t len;
	int ret = 0;
	int counter = 0;
	int addcounter = 0;
	int delcounter = 0;
	char buf[BLEN];
	char *ptr;

	buf_t *b = buf_ist(0);

	for (t=0; t< num; t++) {
		for (x=0; x< BLEN;x++) {
			buf[x] = intrand(1,100);
			counter += buf[x];
		}
		len = buf_len(b);
		buf_ins(b, (intptr_t)intrand(0,(int)len), BLEN, buf);
	}
	
	/* lprintf(LOG_DEBUG, "buf is %d long\n", buf_len(b)); */
	/* lprintf(LOG_DEBUG, "counter is %d\n", counter); */

	for (t=0; t< buf_len(b); t++) {
		ptr = buf_get(b,t);
		addcounter -= *ptr;
	}
	if (counter != addcounter) ret =  1;
	/* lprintf(LOG_DEBUG, "counter is %d\n", counter); */

	while(buf_len(b) > 0) {
		len = buf_len(b);
		t = intrand(0,(int)len);	  /* at most the whole buffer */
		l = intrand(0,(int)len-t); /* at most the rest of the buffer */

		ptr = buf_get(b,t);
		for(x=0; x< l; x++) 
			delcounter -= *ptr;
		buf_del(b,t,l); /* remove the part of the buffer just counted */
	}
	
	/* lprintf(LOG_DEBUG, "counter is %d\n", counter); */
	if (counter != addcounter) ret =  1;

	buf_rls(b);

	return ret;
}

/** @brief test multiple functions
 *
 * main testing entry point for buf.c
 */
int buf_test(int num,int len)
{
	int ret;

	ret = buf_test_add_del(num);
	ret &= buf_test_dup(num,len);
	ret &= buf_test_strdup(num,len);

	ret &= buf_test_data(num,len);

	return ret;
}
