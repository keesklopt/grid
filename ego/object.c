#include <ego.h>

/* object_t is an autonomous object, it was made for the following problem:
	we want each container object to be able to _rls itself but first _rls
	all it's data. You can do that by passing a rls function to _rls but
	what when you have an array (da_rls) of lists (dlst_rls) ? pass two
	_rls functions ?, an array of _rls functions ?.

	better to invent autonomous objects that follow a convention on how 
	to _rls themselves, _cmp themselves with other instances etc.

*/
	
/* the null object is mainly to avoid testing for NULL pointers .. */
void nop_rls(void *data)
{
}

// null object is smaller than anything
int nop_cmp(const void *a, const void *b, void *data)
{
	return -2;
}

void nop_dmp(intptr_t wid, const void *a, void *data)
{
	printf ("nullobject %p", a);
}

// here are a few i did earlier 
object_t null_object = { "nullobject", nop_rls, nop_cmp, nop_dmp, NULL }; 
object_t unsorted_object = { "unsorted generic object", nop_rls, NULL, nop_dmp, NULL }; 
object_t int_object = { "integer", nop_rls, int_abuse_cmp, int_abuse_dmp, NULL }; 
object_t intchar_object = { "integer (c)", nop_rls, int_abuse_cmp, char_abuse_dmp, NULL }; 
object_t intp_object = { "pointer to integer", free, intp_cmp, intp_dmp, NULL };
object_t str_object = { "character pointer", free, str_cmp, str_dmp, NULL };

object_t *object_ist(char *name, rls_fnc_t rls, cmp_fnc_t cmp, dmp_fnc_t dmp, void *udata)
{
	object_t *op = calloc(sizeof(object_t),1);

	if (name) op->name = strdup(name);
	op->rls = rls;
	op->cmp = cmp;
	op->dmp = dmp;
	op->udata = udata;
	return op;
}

object_t *object_dup(object_t *orig)
{
	return object_ist(orig->name, orig->rls, orig->cmp, orig->dmp, orig->udata);
}

void object_rls(object_t *op)
{
	if (op->name) free(op->name);
	free(op);
}
