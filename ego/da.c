/** @file
 * 
	dynamic array

	This implements a dynamic array with a functional interface.
 */
#include <ego.h>

#include <time.h>
#include <string.h>

/** @brief compare function type
 * compare function prototype typedef
 */
typedef int              cmp_t(const void *, const void *);
/* static __inline char    *med3(char *, char *, char *, cmp_fnc_t, void *); */
/* static __inline void     swapfunc(char *, char *, int, int); */

static __inline char *
med3(char *a, char *b, char *c, cmp_fnc_t cmp, void *hdl)
{
        return cmp(a, b,hdl) < 0 ?
               (cmp(b, c,hdl) < 0 ? b : (cmp(a, c,hdl) < 0 ? c : a ))
              :(cmp(b, c,hdl) > 0 ? b : (cmp(a, c,hdl) < 0 ? a : c ));
}

/** @brief swap typed
 * Qsort routine from Bentley & McIlroy's "Engineering a Sort Function".
 */
#define swapcode(TYPE, parmi, parmj, n) {               \
        intptr_t i = (n) / sizeof (TYPE);                   \
        TYPE *pi = (TYPE *) (parmi);            \
        TYPE *pj = (TYPE *) (parmj);            \
        do {                                            \
                TYPE    t = *pi;                \
                *pi++ = *pj;                            \
                *pj++ = t;                              \
        } while (--i > 0);                              \
}

/** @brief initialiase swap
 * Qsort routine from Bentley & McIlroy's "Engineering a Sort Function".
 */
#define SWAPINIT(a, es) swaptype = ((char *)a - (char *)0) % sizeof(int32_t) || \
        es % sizeof(int32_t) ? 2 : es == sizeof(int32_t)? 0 : 1;

static __inline void
swapfunc(char *a, char *b, intptr_t n, int swaptype)
{
        if(swaptype <= 1)
                swapcode(intptr_t, a, b, n)
        else
                swapcode(char, a, b, n)
}


/** @brief swap two values
 * Qsort routine from Bentley & McIlroy's "Engineering a Sort Function".
 */
#define swap(a, b)                                      \
        if (swaptype == 0) {                            \
                intptr_t t = *(intptr_t *)(a);                  \
                *(intptr_t *)(a) = *(intptr_t *)(b);            \
                *(intptr_t *)(b) = t;                       \
        } else                                          \
                swapfunc(a, b, es, swaptype)

/** @brief swap vector
 * Qsort routine from Bentley & McIlroy's "Engineering a Sort Function".
 */
#define vecswap(a, b, n)        if ((n) > 0) swapfunc(a, b, n, swaptype)
/** @brief custom qsort 
 * custom qsort algorithm working on a dynamic array
 *
 * @param a base of the array data
 * @param n number of elements to sort
 * @param es size of one array element
 * @param cmp compare function to use
 * @param hdl optional user data handle to be passed to compare function
 */
void
ego_qsort(void *a, size_t n, intptr_t es, cmp_fnc_t cmp, void *hdl)
{
        char *pa, *pb, *pc, *pd, *pl, *pm, *pn;
		size_t d,r;
		int swaptype, swap_cnt;

loop:   SWAPINIT(a, es);
        swap_cnt = 0;
        if (n < 7) {
                for (pm = (char *)a + es; pm < (char *)a + n * es; pm += es)
                        for (pl = pm; pl > (char *)a && cmp(pl - es, pl,hdl) > 0;
                             pl -= es)
                                swap(pl, pl - es);
                return;
        }
        pm = (char *)a + (n / 2) * es;
        if (n > 7) {
                pl = a;
                pn = (char *)a + (n - 1) * es;
                if (n > 40) {
                        d = (n / 8) * es;
                        pl = med3(pl, pl + d, pl + 2 * d, cmp,hdl);
                        pm = med3(pm - d, pm, pm + d, cmp,hdl);
                        pn = med3(pn - 2 * d, pn - d, pn, cmp,hdl);
                }
                pm = med3(pl, pm, pn, cmp,hdl);
        }
        swap(a, pm);
        pa = pb = (char *)a + es;

        pc = pd = (char *)a + (n - 1) * es;
        for (;;) {
                while (pb <= pc && (r = cmp(pb, a,hdl)) <= 0) {
                        if (r == 0) {
                                swap_cnt = 1;
                                swap(pa, pb);
                                pa += es;
                        }
                        pb += es;
                }
                while (pb <= pc && (r = cmp(pc, a,hdl)) >= 0) {
                        if (r == 0) {
                                swap_cnt = 1;
                                swap(pc, pd);
                                pd -= es;
                        }
                        pc -= es;
                }
                if (pb > pc)
                        break;
                swap(pb, pc);
                swap_cnt = 1;
                pb += es;
                pc -= es;
        }
        if (swap_cnt == 0) {  /* Switch to insertion sort */
                for (pm = (char *)a + es; pm < (char *)a + n * es; pm += es)
                        for (pl = pm; pl > (char *)a && cmp(pl - es, pl,hdl) > 0;
                             pl -= es)
                                swap(pl, pl - es);
                return;
        }

        pn = (char *)a + n * es;
        r = EGO_MIN(pa - (char *)a, pb - pa);
        vecswap(a, pb - r, r);
        r = EGO_MIN((size_t)(pd - pc), (size_t)(pn - pd) - es);
        vecswap(pb, pn - r, r);
        if ((intptr_t)(r = pb - pa) > es)
                ego_qsort(a, r / es, es, cmp,hdl);
        if ((intptr_t)(r = pd - pc) > es) {
                /* Iterate rather than recurse to save stack space */
                a = pn - r;
                n = r / es;
                goto loop;
        }
/*              qsort(pn - r, r / es, es, cmp);*/
}


#ifdef DEBUG
static void dmp(int len, const void *a, const void *b)
{
	int c = *(int *)a;
	lprintf(LOG_ALOT, "%d\n", c);
}
#endif

/** @brief test if already sorted
 * test this array on sortedness
 *
 * @param dap dynamic array to be tested
 * @param cmp compare function to use
 * @param udata optional user data handle to be passed to compare function
 * @return 1 if sorted, 0 if not sorted
 */
int da_issorted(da_t *dap, cmp_fnc_t cmp, void *udata)
{
	unsigned int t;
	char *last;
	char *next;

	dap->sorted = 0;

	last =(char *) da_get(dap,0);
	for (t=1; t< dap->len; t++) {
		next = (char *)da_get(dap,t);
		if (cmp(last, next,udata) > 0) return 0;
		/* printf("%d-%d\n", *(int *)last, *(int *)next); */
		last = next;
	}
	return dap->sorted = 1;
}

/** @brief compact da 
 * used for data.c 
 */
void da_data(void)
{
	type_t *type;

	type = type_ist("da_t", CALC_SIZE, 0, NULL, gen_traverse_b, gen_traverse_struct_d);
	/* buf_t *bufp; */
	/* put bit fields in one int, also we only do signed  */
	type_add(type, "len", "int");
	type_add(type, "len_bitfields", "int");
	type_add(type, "bufp", "buf_t *");
	type_reg(type);

	type_reg_ptr("da_t *", "da_t", gen_traverse_b, gen_traverse_d);
}

/** @brief dump dynamic array
 * dump a dynamic array with content
 *
 * @param dap dynamic array to be dumped
 * @param dmp function to use for dumping one element
 * @param udata optional user data handle to be passed to dump function
 */
void da_dmp(da_t *dap, dmp_fnc_t dmp, void *udata)
{
	intptr_t t;
	char *p;

	lprintf(LOG_ALOT, "{");
	for (t=0; t< da_len(dap); t++) {
		if (t!=0) lprintf(LOG_ALOT, ",");
		p = da_get(dap, t);
		dmp(dap->wid,(const void *)p,udata);
	}
	lprintf(LOG_ALOT, "}\n");
}

/** @brief custom bsearch
 * custom  bsearch made fo dynamic array type
 *
 * @param key key to be searched
 * @param base0 base of the array data to search in 
 * @param nmemb number of members to search in 
 * @param size size of 1 member
 * @param compar compare function to use
 * @param udata optional user data handle to be passed to compare function
 * @return pointer to found element, or NULL if not found
 */
void *ego_bsearch(
     register const void *key,
     const void *base0,
     size_t nmemb,
     register size_t size,
	cmp_fnc_t compar, void *udata)
{
     register const char *base = base0;
     register size_t lim;
     register int cmp;
     register const void *p;

     for (lim = nmemb; lim != 0; lim >>= 1) {
             p = base + (lim >> 1) * size;
             cmp = (*compar)(key, p,udata);
             if (cmp == 0)
                     return ((void *)p);
             if (cmp > 0) {  /* key > p: move right */
                     base = (char *)p + size;
                     lim--;
             }               /* else move left */
     }
     return (NULL);
}

/** @brief custom insertion bsearch 
 * custom bsearch algorithm that returns an insertion position for the key
 *
 * @param key key to be searched
 * @param base0 base of the array data to search in 
 * @param nmemb number of members to search in 
 * @param size size of 1 member
 * @param compar compare function to use
 * @param udata optional user data handle to be passed to compare function
 * @return pointer to found element, or NULL if not found
 */
void *ego_bisearch(
     register const char *key,
     const char *base0,
     size_t nmemb,
     register size_t size,
	cmp_fnc_t compar, void *udata)
{
     register const char *base = base0;
     register size_t lim;
     register int cmp;
     register const char *p=NULL;

     for (lim = nmemb; lim != 0; lim >>= 1) {
             p = base + (lim >> 1) * size;
             cmp = (*compar)(key, p,udata);
             if (cmp == 0)
                     return ((void *)p);
             if (cmp > 0) {  /* key > p: move right */
                     base = (char *)p + size;
                     lim--;
             }               /* else move left */
     }
	 /* there may be identical values so shift on */
	 while (size && 
		 p< base0+nmemb*size 
		 && 
		 compar
		 (key
		 ,
		 p
		 ,udata) >= 0 ) {
		 p+= size;
	}
     return ((void *)p);
}

/** @brief get insert position
 * search for key but don't return NULL when not found, but the place to insert 
 *
 * @param dap dynamic array to search
 * @param key key to be found
 * @param cmp compare function to use
 * @param hdl optional user data handle to be passed to compare function
 * @return found position, or position to insert the key
 */
intptr_t da_insertpos(da_t *dap, void *key, cmp_fnc_t cmp,void *hdl)
{
	char *pos;
	intptr_t rval;

	if (!dap || !cmp) return 0;

	pos = ego_bisearch (key, dap->bufp->data, dap->len, dap->wid, cmp,hdl);
	rval = (pos - dap->bufp->data);
	return rval / dap->wid;
}

/** @brief insert element maintaining sortedness
 * insert 1 element maintaining sorting order,
 * da_ins_sorted() can only add 1 item to add more items use da_ins
 *
 * @param dap dynamic array 
 * @param data data fo the element to insert
 * @param cmp compare function to use
 * @param hdl optional user data handle to be passed to compare function
 * @return 0 on success, an error code otherwise
 */
int da_ins_sorted(da_t *dap, void *data, cmp_fnc_t cmp,void *hdl)
{
	intptr_t pos;
	intptr_t offset;

	if (!dap) return err_set(ERR_NULL);
	
	if (!dap->sorted) /* your own fault now i'll take my time to  sort it !! */
		da_sort(dap,cmp,hdl);

	pos = da_insertpos(dap,data, cmp, hdl);
	offset = pos * dap->wid;

	/* printf("strets %d\n", dap->wid); */
	buf_stretch(dap->bufp, offset, dap->wid);
	da_set(dap, data, pos);
	dap->len++;

	dap->sorted = 1;

	return 0;
}

/** @brief get element position
 * find the position of the given key, or -1 if not found
 *
 * @param dap dynamic array
 * @param key element to find
 * @param cmp compare function to use
 * @param hdl optional user data handle to be passed to compare function
 * @return 1 if sorted, 0 if not sorted
 */
intptr_t da_pos(da_t *dap, void *key, cmp_fnc_t cmp,void *hdl)
{
	char *pos=NULL;
	intptr_t rval;
	int t;
	char *p;

	if (!dap || !cmp) { 
		err_set(ERR_NULL);
		return -1;
	}

	if (dap->sorted) 
		pos = ego_bsearch (key, dap->bufp->data, dap->len, dap->wid, cmp,hdl);
	else  { /* do it linear, i still want to know where it (or the first) is  */
		for (t=0; t< da_len(dap); t++) {
			p = da_get(dap, t);
			if ( !cmp((const void *)p,key,hdl)) {
				pos = p;
				break;
			}
		}
	}

	/* printf("is sorted ?! %d", da_issorted(dap,cmp,hdl)); */
	if (!pos) return -1;

	rval = (pos - dap->bufp->data);

	return rval / dap->wid;
}

/** @brief sort dynamic array
 * sort the dynamic array
 *
 * @param dap dynamic array to be sorted
 * @param cmp compare function to use
 * @param hdl optional user data handle to be passed to compare function
 * @return 0 on success, an error code otherwise
 */
int da_sort(da_t *dap, cmp_fnc_t cmp,void *hdl)
{
	char *data;
	if (!dap || !cmp) return err_set(ERR_NULL);

	data = buf_get(dap->bufp, 0);

	ego_qsort (data, dap->len, dap->wid, cmp,hdl);

	dap->sorted = 1;

	/* printf("is sorted ?! %d", da_issorted(dap,cmp,hdl)); */
	return 0;
}

/** @brief duplicate array
 * duplicate a dynamic array and all it;s content
 *
 * @param orig dynamic array to be duplicated
 * @return new duplicate of the dynamic array
 */
da_t *da_dup(da_t *orig)
{
	da_t *dap = (da_t *)calloc(sizeof(da_t),1);

	dap->len = orig->len;
	dap->wid = orig->wid;
	dap->bufp = buf_dup(orig->bufp);

	return dap;
}

/** @brief initialise dynamic array
 * initialise a dynamic array structure 
 *
 * @param dap dynamic array to be initialised
 * @param wid width in bytes of an array element
 * @param len initiali length of the array
 */
void da_init(da_t *dap,int wid,int len)
{
	dap->len = len;
	dap->wid = wid;	
	dap->sorted = 0;
	dap->bufp = buf_ist(dap->len * dap->wid);
}

/** @brief instantiate dynamic array
 * instantiate dynamic array and initialise
 *
 * @param wid width in bytes of an array element
 * @param len initiali length of the array
 * @return pointer to e newly created dynamic array
 */
da_t *da_ist(int wid, int len)
{
	da_t *dap = (da_t *)calloc(sizeof(da_t),1);
	da_init(dap,wid,len);
	return dap;
}

/** @brief release dynamic array
 * release dynamic array and free up it's resources
 *
 * @param dap dynamic array 
 */
void da_rls(da_t *dap)
{
	if (!dap) return;
	if (dap->bufp) buf_rls(dap->bufp);

	free(dap);
}

/** @brief array length
 * return the length of the dynamic array
 *
 * @param dap dynamic array 
 * @return number of elements in this array
 */
int da_len(da_t *dap)
{
	if (!dap) return 0;
	return dap->len;
}

/** @brief resize dynamic array
 * resize this dynamic array by the given amount
 *
 * @param dap dynamic array to be enlarged
 * @param len number of elements to add
 * @return 0 on success, error code on failure
 */
intptr_t da_resize(da_t *dap, unsigned int len) 
{
	dap->len = len;
	return buf_resize(dap->bufp, len*dap->wid);
}

/** @brief copy data from a dynamic array
 * copy part of a dynamic array into a buffer
 *
 * @param dst buffer to copy the data into
 * @param src dynamic array where to get the data
 * @param from start element to copy from
 * @param to end element to copy from
 */
void da_cpy(void *dst, da_t *src, int from, int to)
{
	int bytes = ((to-from)+1) * src->wid;

	if (bytes < 0) return;
	buf_get_str(dst, src->bufp, from*src->wid, bytes);
}

/** @brief get data
 * get pointer to the data of the element given
 *
 * @param dap dynamic array 
 * @param pos position of the element to be retrieved
 * @return pointer to the internal data 
 */
void *da_get(da_t *dap, intptr_t pos)
{	
	void *p;
	unsigned int bytes;

	if (!dap)return NULL;

	bytes = dap->wid * pos;
	if (pos >= (intptr_t)dap->len) {
		err_set(ERR_RANGE);
		return NULL;
	}
	
	p =  buf_get(dap->bufp, bytes);
	return p;
}

/** @brief set element
 * set the data at the given element in the dynamic array
 *
 * @param dap dynamic array 
 * @param data data to be pasted into the dynamic array
 * @param pos position opf the element to be overwritten
 * @return 0 
 */
intptr_t da_set(da_t *dap, void *data, intptr_t pos)
{
	buf_set_str(dap->bufp, pos*dap->wid, dap->wid, data);
	return 0;
}

static unsigned int t;
static char tmp;

/** @brief swap element
 * swap the elements given in the dynamic array
 *
 * @param dap dynamic array
 * @param a position of the first element
 * @param b position of the second element
 * @return 0 always
 */
int da_swap(da_t *dap, int a, int b)
{
	/* B=da_get(dap,b); */
	char *A = &dap->bufp->data[a*dap->wid];
	char *B = &dap->bufp->data[b*dap->wid];

	for (t=0; t< dap->wid; t++) {
		tmp = *A;
		*A  = *B;
		*B  = tmp;
		A++; B++;
	}
	return 0;

}

/** @brief delete elements
 * delete a number of elemenst from a dynamic array
 *
 * @param dap dynamic array 
 * @param pos start element to delete
 * @param len number of elements to delete
 * @return 0 on success, error code otherwise
 */
int da_del(da_t *dap, int pos, int len)
{
	int offset;
	if (!dap) return err_set(ERR_NULL);
	if (len < 1) len = 1;

	offset = pos * dap->wid;

	buf_shrink(dap->bufp, offset, dap->wid*len);

	dap->len--;
    return 0;
}

/** @brief add an element
 * add an element at the end of a dynamic array
 *
 * @param dap dynamic array to add to
 * @param data data of the element to add
 * @return 0 on success, error code otherwise
 */
int da_add(da_t *dap, void *data)
{
	if (!dap) return err_set(ERR_NULL);
	
	da_resize(dap, dap->len+1);
	da_set(dap, data, dap->len-1);

	/* ALWAYS unsorted, see da_ins_sorted() */
	dap->sorted = 0;
	return 0;
}

/** @brief insert elements
 * insert multiple elements into a dynamic array. Sortedness will ALWAYS
 * be set to false. Use da_issorted if you are sure you added sorted data 
 * or da_sort if you are not sure
 *
 * @param dap dynamic array to be tested
 * @param data data of the elements to be inserted
 * @param pos position at which to insert the elements
 * @param len number of elements to insert at pos
 * @return 1 if sorted, 0 if not sorted
 */
int da_ins(da_t *dap, void *data, int pos, int len)
{
	int offset;
	if (len < 1) len = 1;

	if (!dap) return err_set(ERR_NULL);

	offset = pos * dap->wid;

	buf_stretch(dap->bufp, offset, dap->wid*len);
	da_set(dap, data, pos);
	dap->len++;

	/* ALWAYS unsorted, see da_ins_sorted() */
	dap->sorted = 0;

	return 0;
}

static int da_test_dup(int num, int len)
{
	da_t *dap = da_ist(sizeof(int),len);
	int t;
	da_t *c;

	for (t=0; t< num; t++) {
		c = da_dup(dap);
		da_rls(dap);
		dap = c;
	}

	da_rls(dap);

	return 1;
}

static int da_test_sorting(int num)
{
	intptr_t t;
	int r,p ;
	da_t *dap = da_ist(sizeof(int),0);

	/* insert random numbers */
	for (t=0; t< num; t++) {
		p = intrand(0,(int)t);
		r = intrand(10,1000);
		da_ins(dap, (void *)&r, p,1);
	}
	/* this *might* yield sorted per accident, but i doubt it */
	printf("filled an array with random numbers, tested sorting=%d, marked sorting=%d\n", da_issorted(dap,int_cmp, NULL),dap->sorted);
	/* da_dmp(dap, int_dmp, NULL); */
	t = da_pos(dap, &r, int_cmp, NULL);
	printf("(linear) search for %d inserted at %d = %d\n", r, p,(int)t);

	/* find should alwayus work  */
	da_sort(dap, int_cmp,NULL);

	/* but this should be faster  */
	t = da_pos(dap, &r, int_cmp, NULL);
	printf("(sorted/binary) search for %d (sorted so position is probably different) = %d\n", r, (int)t);

	/* this MUST yield sorted  */
	printf("after sorting, tested sorting=%d, marked sorting=%d\n", da_issorted(dap,int_cmp, NULL),dap->sorted);
	/* da_dmp(dap, int_dmp, NULL); */

	/* now this should keep sorting ok, i insert a known nmber to be able to find it */
	r = 500;
	da_ins_sorted(dap, (void *)&r, int_cmp,NULL);
	printf("insert 1 element sorted, tested sorting=%d, marked sorting=%d\n", da_issorted(dap,int_cmp, NULL),dap->sorted);
	/* da_dmp(dap, int_dmp, NULL); */
	
	/* now this should ALWAYS invalidate sorting, even if i put 0 (< 10) at 0, so  */
	/* it IS sorted, but we do not know for sure,  */
	p = 0;
	r = 0;
	da_ins(dap, (void *)&r, p,1);
	printf("insert 1 element with plain insert (though at a sorted spot), tested sorting=%d, marked sorting=%d\n", da_issorted(dap,int_cmp, NULL),dap->sorted);
	printf("da_issorted marks sorted when it succeeds so marked sorting=%d,%d\n", da_issorted(dap,int_cmp, NULL),dap->sorted);
	/* since da_issorted also marks the sortedness, the MARK changes  */
	/* c handles last parameter first, hence the weird order of BEFORE and AFTER !! */
	/* da_dmp(dap, int_dmp, NULL); */
	p = 0;
	r = 100000;
	da_ins(dap, (void *)&r, p,1);
	printf("insert 1 element with plain insert (at an unsorted spot), tested sorting=%d, marked sorting=%d\n", da_issorted(dap,int_cmp, NULL),dap->sorted);
	printf("da_issorted marks sorted when it succeeds so marked sorting=%d,%d\n", da_issorted(dap,int_cmp, NULL),dap->sorted);
	da_rls(dap);

	return 1;
}

#ifdef USED
static int da_test_data(int num)
{
	int t, r,p ;
	da_t *dap = da_ist(sizeof(int),0);
	instance_t *ip;

	data_init(1);

	num=2;
	
	for (t=0; t< num; t++) {
		p = intrand(0,t);
		r = intrand(10,1000);
		da_ins(dap, (void *)&r, p,1);
	}

	da_dmp(dap, int_dmp,NULL); 

	ip= instance_ist("da_t *");
	instance_pack(ip, (void *)&dap,1);
	instance_dmp(ip,LOG_ALOT);

return 1;

	da_rls(dap);
	instance_unpack(ip, (void *)&dap);

	da_dmp(dap, int_dmp,NULL); 
	da_rls(dap);

	instance_rls(ip);


	return 1;
}
#endif

static int da_test_ins_del(int num)
{
	int t, r,p ;
	da_t *dap = da_ist(sizeof(int),0);
	
	for (t=0; t< num; t++) {
		p = intrand(0,t);
		r = intrand(10,1000);
		da_ins(dap, (void *)&r, p,1);
	}
	/* da_sort(dap, intcmp,NULL); */
	for (t=0; t< num; t++) {
		int len = da_len(dap);
		p = intrand(0,len);
		da_del(dap, p,1);
	}
	da_rls(dap);

	return 1;
}

static int da_test_int(int num)
{
	int t, r;
	da_t *dap = da_ist(sizeof(int),0);
	
	for (t=0; t< num; t++) {
		r = intrand(10,1000);
		da_add(dap, (void *)&r);
	}
	/* da_dmp(dap, dmp); */
	da_sort(dap, int_cmp,NULL);
	/* da_dmp(dap, dmp); */

	da_rls(dap);

	return 1;
}

static int da_test_struct(int num)
{
	int t;
	test_t r;
	da_t *dap = da_ist(sizeof(test_t),0);
	
	for (t=0; t< num; t++) {
		r.i = intrand(10,1000);
		r.c = 'a';
		r.str = "hallo";
		snprintf(r.carr, 5, "dag!");
		da_add(dap, (void *)&r);
	}
	/* da_dmp(dap, test_dmp); */
	da_sort(dap, test_cmp,NULL);
	/* da_dmp(dap, test_dmp); */

	da_rls(dap);

	return 1;
}

/** @brief test function
 * run some internal tests on dynamic arrays
 */
int da_test(int num,int len)
{
	int ret=1;

	/* time_t t1,t = time(NULL); */
	ret &= da_test_struct(num);
	/* t1 = time(NULL); */
	ret &= da_test_int(num);
	ret &= da_test_dup(num,len);
	ret &= da_test_ins_del(num);
	/* this one is verbose : */
	ret &= da_test_sorting(num);

	//ret &= da_test_data(num);

	/* printf("duration : %d\n", (int)(t1-t)); */
	return ret;
}

