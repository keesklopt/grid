/** @file 
	input and output objects

	provides a generic interface to anything that could produce and/or 
	receive a byte stream

	the supported types of io objects are currently

	IO_FP: a system file pointer as opened with fopen()
	IO_FD: a system file descripor as opened with open()
	IO_BUF: an io object built upon a libego dynamic buffer,see buf.c
	IO_BUF: plain memory as allocated by malloc/realloc
	IO_SOCKET: a system socket implementation
*/
	
#include <ego.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#include <netdb.h>

#else
#include <winsock2.h>
#include <io.h>

#endif
#include <errno.h>

io_t io_stdout;
io_t io_stdin; 

static char *make_fflags(int m)
{
	if (m == O_RDWR) return "rw";
	if (m == O_RDONLY) return "r";
	if (m == O_WRONLY) return "w";
	if (m == O_APPEND) return "a";
	return "r"; 
}

static int make_flags(char *m)
{
	if (!m) return 0;
	if (m[0] == 'r' && m[1]=='w') return O_RDWR;
	if (m[0] == 'r') return O_RDONLY;
	if (m[0] == 'w') return O_WRONLY;
	if (m[0] == 'a') return O_APPEND;
	return O_RDONLY; 
}

/* IO_FP type, file pointers (FILE) also called streams , ------- */
static int io_fp_open(io_t *iop, const void *hdl, int flags)
{
	char *fmode=make_fflags(flags);

	if (!fopen_r(iop->handle.fp,(char *)hdl, fmode))
	{
		perror("roblem");
		return -1;
	}
	return 0;
}

static int io_fp_close(io_t *iop)
{
	if (iop->handle.fp)
		return fclose(iop->handle.fp);

	return 0;
}

static size_t io_fp_read(io_t *iop,void *ptr, size_t size)
{
	size_t ret;
	ret = fread(ptr, 1, size, iop->handle.fp);
	return ret;
}

static size_t io_fp_write(io_t *iop, void *ptr, size_t size)
{
	size_t ret = fwrite(ptr, 1, size, iop->handle.fp);
	return ret;
}

static int io_fp_lseek(io_t *iop, long pos, int whence)
{
	return fseek(iop->handle.fp, pos, whence);
}

static intptr_t io_fp_ftell(io_t *iop)
{
	return ftell(iop->handle.fp);
}

static void io_fp_rewind(io_t *iop)
{
	rewind(iop->handle.fp);
}


static int static_fd_wait(int fd, int which, int timeout)
{
	fd_set rdfd, wrfd, errfd;
	fd_set *rdp = NULL, *wrp = NULL; 
	struct timeval tv, *tvp; 
	int nfd;

	tv.tv_sec = timeout / 1000;
	tv.tv_usec = (timeout % 1000) * 1000;
	tvp = &tv;

	if (timeout < 0)/* negative is wait forever */
		tvp = NULL;

	if (which & WAIT_WRITE) {
		FD_ZERO(&wrfd);
		FD_SET(fd, &wrfd);
		wrp = &wrfd;
	}
	if (which & WAIT_READ) {
		FD_ZERO(&rdfd);
		FD_SET(fd, &rdfd);
		rdp = &rdfd;
	}
	nfd = select(fd+1 , rdp, wrp, &errfd, tvp);

    if (rdp && FD_ISSET(fd, rdp)) {
        /* printf("read clear\n"); */
        return WAIT_READ;
    }

    if(wrp && FD_ISSET(fd, wrp)) {
        /* printf("write clear\n"); */
        return WAIT_WRITE;
    }
	if (nfd <=0 ) {
		return 0;
	}
	if(FD_ISSET(fd, &errfd)) {
	    return WAIT_ERROR;
	}

	return 0;
}

static int io_fd_wait(io_t *iop, int which, int timeout)
{
	return static_fd_wait(iop->handle.fd, which, timeout);
}

static int io_fd_open(io_t *iop, const void *hdl, int flags)
{
#ifdef WIN32
	int err;
	
	if ( err = _sopen_s(&iop->handle.fd, (char *)hdl, flags, 0, 0644)) 
		return err;
#else
	if ( (iop->handle.fd = _open((char *)hdl, flags, 0644)) == -1)
		return -1;
#endif

	return 0;
}

static int io_fd_close(io_t *iop)
{
	return _close(iop->handle.fd);
}

static size_t io_fd_read(io_t *iop,void *ptr, size_t size)
{
	size_t ret=0, max, total;
	char *bptr=(char *)ptr;

	max=total=size;

	if (max > SSIZE_MAX) max = SSIZE_MAX;

	while (total > 0) {
		ret = read(iop->handle.fd, bptr, max);
		if (errno) {
			perror("read error, cause : ");
			return ret;
		}
		total -= ret;
		bptr  += ret;
		if (total < SSIZE_MAX) max=total;
	}
	return ret;
}

static size_t io_fd_write(io_t *iop,void *ptr, size_t size)
{
	size_t ret;
	ret = write(iop->handle.fd, ptr, size);
	return ret;
}

static int io_fd_lseek(io_t *iop, long pos, int whence)
{
	return _lseek(iop->handle.fd, pos, whence);
}

static intptr_t io_fd_ftell(io_t *iop)
{
	return _lseek(iop->handle.fd, 0, SEEK_CUR);
}

static void io_fd_rewind(io_t *iop)
{
	_lseek(iop->handle.fd, 0, SEEK_SET);
}

static int io_sock_lseek(io_t *iop, long pos, int whence)
{
	return 0;
}

static int io_sock_wait(io_t *iop, int which, int timeout)
{
	return static_fd_wait(iop->handle.sock->fd, which, timeout);
}

static int io_sock_open(io_t *iop, const void *hdl, int flags)
{
	iop->handle.sock = (socket_t *)hdl;

	return 0;
}

static int io_sock_close(io_t *iop)
{
	return _close(iop->handle.sock->fd);
}

static size_t io_sock_read(io_t *iop,void *ptr, size_t size)
{
	ssize_t ret;
	char *bptr=(char *)ptr;
#ifdef DEBUG
	int x;
#endif

	ret = recv(iop->handle.sock->fd, bptr, size, 0);
	if (ret < 0) {
		/* printf("errno is %d\n", errno); */
		perror("read error, cause : ");
		return ret;
	}
#ifdef DEBUG
	printf("read %d of %d (%d)\n", ret, size, iop->handle.sock->fd);
#endif
	return ret;
}

#ifdef OBSOLETE
static size_t io_sock_read_exactly(io_t *iop,void *ptr, size_t size)
{
	ssize_t ret;
	size_t total=size;
	char *bptr=(char *)ptr;
#ifdef DEBUG
	int x;
#endif

	// checked 
	while (total > 0) {
		ret = recv(iop->handle.sock->fd, bptr, total, 0);
		if (ret < 0) {
			/* printf("errno is %d\n", errno); */
			perror("read error, cause : ");
			return ret;
		}
		if (ret == 0) {
			// other side orderly shut down, still regard this as an
			// 'error' because we were waiting on 'size' bytes
			return size-total;
		}
		total -= ret;

#ifdef DEBUG
		for (x=0; x< ret; x++) {
			printf("%d,", bptr[x]);
		}
		printf("\n");
#endif
		bptr  += ret;
#ifdef DEBUG
		printf("left : %d %d\n", ret, total);
#endif
	}
#ifdef DEBUG
	printf("read %d of %d (%d)\n", total, size, iop->handle.sock->fd);
#endif
	return size;
}
#endif

static size_t io_sock_write(io_t *iop,void *ptr, size_t size)
{
	size_t ret;
	ret = send(iop->handle.sock->fd, ptr, size,0);
	/* printf("written %d of %d (%d)\n", ret, size*nmemb, iop->handle.sock->fd); */
	return ret;
}

static intptr_t io_sock_ftell(io_t *iop)
{
	return 0;
}

static void io_sock_rewind(io_t *iop)
{
	return;
}

/* IO_BUF type, the dynamic buffer type as implemeneted in buf.c  ------- */
static int io_buf_open(io_t *iop, const void *hdl, int flags)
{
	if ( (iop->handle.buf = (buf_t *)hdl) == NULL) return -1;
	return 0;
}

static int io_buf_close(io_t *iop)
{
	return 0;
}

static size_t io_buf_read(io_t *iop, void *ptr, size_t size)
{
	return buf_read(iop->handle.buf,ptr, size);
}

static size_t io_buf_write(io_t *iop, void *ptr, size_t size)
{
	return buf_write(iop->handle.buf, ptr, size);
}

static int io_buf_lseek(io_t *iop, long pos, int whence)
{
	return buf_fseek(iop->handle.buf, pos, whence);
}

static intptr_t io_buf_ftell(io_t *iop)
{
	return buf_ftell(iop->handle.buf);
}

static void io_buf_rewind(io_t *iop)
{
	buf_rewind(iop->handle.buf);
}

/* IO_MEM  -- this one should be monitored closely */
static int io_mem_open(io_t *iop, const void *hdl, int flags)
{
	if ((iop->handle.mem = calloc(1,sizeof(mem_t)))== NULL) return -1;
	iop->handle.mem->cur = (char *)hdl;
	iop->handle.mem->base = (char *)hdl;
	iop->handle.mem->size = iop->size;

	return 0;
}

static int io_mem_close(io_t *iop)
{
	if (iop->handle.mem) {
		free(iop->handle.mem);
	}
	return 0;
}

static size_t io_mem_read(io_t *iop, void *ptr, size_t size)
{
	size_t bytes=size;
	intptr_t offset = iop->handle.mem->cur - iop->handle.mem->base;

	if (offset > (intptr_t)iop->size) return -1;
	if (offset+bytes > iop->size) bytes = iop->size - offset;

	memcpy(ptr,&iop->handle.mem->base[offset], bytes);
	iop->handle.mem->cur += bytes;

	return bytes;
}

static size_t io_mem_write(io_t *iop,void *ptr, size_t size)
{
	return fwrite(ptr, 1, size, iop->handle.fp);
}

static int io_mem_lseek(io_t *iop, long offset, int whence)
{
	intptr_t pos = iop->handle.mem->cur-iop->handle.mem->base;
	
	if (whence == SEEK_CUR) offset += pos;
	if (whence == SEEK_END) offset += iop->size;

	if (offset < 0 || offset >= (int)iop->size) return -1;
	iop->handle.mem->cur = iop->handle.mem->base+offset;

	return 0;
}

static intptr_t io_mem_ftell(io_t *iop)
{
	return iop->handle.mem->cur - 
		iop->handle.mem->base;
}

static void io_mem_rewind(io_t *iop)
{
	iop->handle.mem->cur =
		iop->handle.mem->base;
}

/** @brief function matrix
 *
 * array of io_functions_t structure implementing the various io types
 */
io_functions_t io_f[] = {
/* #define IO_FP 		0x00 */
	{ io_fp_open, io_fp_close,   io_fp_read,   io_fp_write,	 
	io_fp_lseek, io_fp_ftell,  io_fp_rewind,  NULL },
/* #define IO_FD 		0x01 */
	{ io_fd_open,  io_fd_close,   io_fd_read,   io_fd_write,	 
	  io_fd_lseek, io_fd_ftell,  io_fd_rewind,  io_fd_wait },
/* #define IO_BUF   		0x02  */
	{ io_buf_open,  io_buf_close, io_buf_read,  io_buf_write, 
	  io_buf_lseek, io_buf_ftell,io_buf_rewind, NULL},
/* #define IO_MEM   		0x03  */
	{ io_mem_open,  io_mem_close, io_mem_read,  io_mem_write, 
	  io_mem_lseek, io_mem_ftell,io_mem_rewind, NULL },
/* #define IO_SOCKET     0x04 */
	{ io_sock_open,  io_sock_close,io_sock_read,   io_sock_write,	 
	  io_sock_lseek, io_sock_ftell,io_sock_rewind,  io_sock_wait  },
/* #define IO_STDOUT 	0x0x */
/* #define IO_ERRWIN		0x0x */
};

static int io_systemcheckcolerable(void)
{
#ifdef WIN32
	return 0;
#else
	/* OK, i think all consoles on linux are colorable */
	return 1;
#endif

	/* just alter this whenever you find an environment that fails !! */
}

/** @brief init io library
 * 
 * initialise some stdout structures 
 */
int io_init(void)
{
    io_stdout.type = IO_FP;
    io_stdout.handle.fp = stdout;
    io_stdout.colorable = io_systemcheckcolerable();

	return 0;
}   
    
/** @brief open io object
 *
 * open input output object or initialise it
 * after this the object is ready to read or write
 @param iop pointer to input output object
 @param hdl	handle to io specific data to be opened
 @param flags flags like in open system call O_RDONLY, O_WRONLY or O_RDWR
 @return 0 on success, error code otherwise
 */
 
int io_open(io_t *iop,char *hdl, int flags)
{
	return io_f[iop->type].open(iop, hdl, flags);
}

/** @brief close io object
 *
 * close input output object
 * after this the object is no longer ready to read or write
 @param iop pointer to input output object
 @return 0 on success, error code otherwise
 */
void io_close(io_t *iop)
{
	io_f[iop->type].close(iop);
}

/** @brief wait on io object
 *
 * wait on an input output object to become ready to write to or read from
 @param iop pointer to input output object
 @param which which type of wait WAIT_READ or WAIT_WRITE
 @param timeout how long to wait before bailing out 0 is wait forever
 @return 0 on success, error code otherwise
 */
int io_wait(io_t *iop, int which, int timeout)
{
	int ret=0;

	if (io_f[iop->type].read != NULL)
		ret =  io_f[iop->type].wait(iop, which, timeout);

	return ret;
}

/** @brief read exactly
 *
 * read from an io object and continue until exactly len bytes have been read
 @param iop pointer to input output object
 @param buf buffer to put the data in
 @param len how many bytes are expected
 @return number of bytes read, so.. len
 */
size_t io_read_exactly(io_t *iop,char *buf, size_t len)
{
	int ret;
	int total= len;
	while (total > 0) {
		ret =  io_f[iop->type].read(iop, buf, len);
		total -= ret;
	}
	return len;
}

/** @brief read from io object
 *
 * try to read len bytes from an io object, if not enough data is available
 * return the bytes read
 @param iop pointer to input output object
 @param buf buffer to put the data in
 @param len how many bytes are expected
 @return number of bytes read 
 */
size_t io_read(io_t *iop,char *buf, size_t len)
{
	int ret;
	ret =  io_f[iop->type].read(iop, buf, len);
	return ret;
}

/** @brief fgetc on an io object
 *
 * use fgetc like interface to io objects to get next character 
 @param iop pointer to input output object
 @return next character on the io object
 */
int io_fgetc(io_t *iop)
{
	int ret=EOF;
	int r;
	char c;

	r = io_read(iop,&c, 1);
	if (r == 1) ret = (int)c;

	return ret;
}

/** @brief fgets on an io object
 *
 * use fgets like interface to io objects to get len characters
 @param buf buffer to contain the date to get
 @param iop pointer to input output object
 @param len number of bytes to read
 @return the string read
 */
char *io_fgets(char *buf, size_t len,io_t *iop)
{
	int ret;
	unsigned int t=0;

	while ((ret = io_fgetc(iop)) != EOF && t< (unsigned int)len) {
		if (ret == '\n') break;
		buf[t++] = (char)ret;
	}
	buf[t] = '\0';

	/* printf("t is now %d\n", t); */
	if(t==0) return NULL;

	return buf;
}

/** @brief write on io object
 *
 * try to write len bytes on an io object, return the bytes written
 @param iop pointer to input output object
 @param buf buffer with data to write
 @param len size of the data to write
 @return number of bytes read 
 */
size_t io_write(io_t *iop,char *buf, size_t len)
{
	return io_f[iop->type].write(iop, buf, len);
}

/** @brief rewind io object
 *
 * rewind an io object 
 @param iop pointer to input output object
 */
void io_rewind(io_t *iop)
{
	io_f[iop->type].rewind(iop);
}

/** @brief reposition io object
 *
 * reposition an io object to pos, regarding the whence type
 @param iop pointer to input output object
 @param pos	where to position the io object
 @param whence how to position , with SEEK_SET the pos is taken from the beginnning, with SEEK_CUR it is relative to the current position, SEEK_END is taken relative to the end of the io object
 @return offset after position from the beginning of the file
 */
int io_lseek(io_t *iop, long pos, int whence)
{
	return io_f[iop->type].lseek(iop, pos, whence);
}

/** @brief give current position
 *
 * return the current position in an io object
 @param iop pointer to input output object
 @return offset from the beginning of the file
 */
int io_ftell(io_t *iop)
{
	return io_f[iop->type].ftell(iop);
}

/** @brief release io object
 *
 * release the data occupied by an io object
 @param iop pointer to input output object
 */
void io_rls(io_t *iop)
{
	if (!iop) return;
	switch(iop->type) {
		case IO_FP:
			io_fp_close(iop);
		break;
		case IO_MEM:
			io_mem_close(iop);
		break;
		/* the rest : no action */
		default:
		break;
	}

	free(iop);
}

/** @brief instantiate an object
 *
 * create a new input output object 
 @param type, type of the object as described in the beginning of this file
 @param location, location of the object for instance a filename
 @param rw string specifying read access "r" write, "r" or both "rw"
 @param size size for some types (like mem etc) 
 @return pointer to input output object
 */
io_t *io_ist(int type, void *location, char *rw, unsigned int size)
{
    io_t *iop = (io_t *)calloc(sizeof(io_t),1);
	int RW = make_flags(rw);

    iop->type = type;
	iop->size = size;
    if (io_open(iop, location, RW)) {
		io_rls(iop);
		return NULL;
	}
    iop->colorable = 0;
    return iop;
}

/** @brief vprintf to an io object
 *
 * use vprintf to print to an input/output object 
 @param iop pointer to input output object
 @param fmt format string
 @param argptr variable argument list 
 @return number of bytes writtem
 */
int io_vprintf(io_t *iop, const char *fmt, va_list argptr) 
{
    int rval= vfprintf(iop->handle.fp, fmt, argptr);

	fflush(iop->handle.fp);
	return rval;
}

/** @brief printf to an io object
 *
 * use printf to print to an input/output object 
 @param iop pointer to input output object
 @param fmt format string
 @param ... variable argument list 
 @return number of bytes writtem
 */
int io_printf(io_t *iop, const char *fmt, ...)
{
	int ret;
    va_list argptr;
    va_start(argptr, fmt);

    if (iop->options == 0) iop = &io_stdout;

    ret = vfprintf(iop->handle.fp, fmt, argptr);

    va_end(argptr);
	return ret;
}

/** @brief set output color
 *
 * set output color if input/output object supports it
 @param iop pointer to input output object
 @param color color number
 */
void io_setcolor(io_t *iop, int color)
{
    if (!iop->colorable) return;
#ifndef WIN32
    if (color == IO_NORMAL) {
        printf(COLOR, 0,0 );
        return;
    }
    printf(COLOR, color+30, 1);
#endif
}

/** @brief make io read only string
 *
 * convenience function to create an io object from a character string
 @param str string to use as data
 @return pointer to input output object
 */
io_t *io_ist_string(char *str)
{
	io_t *base_mem = io_ist(IO_MEM, str, "r", strlen(str));

	return base_mem;
}

static char *randombuffer(int len, int printable)
{
	int t;
	int lo=0,hi=255;
	unsigned char r;
	char *buffer = malloc(len);

	if (printable) {
		lo = '0';
		hi = 'z';
	}

	/* this means ANY char can be in the buffer so also  */
	/* strings terminated halfway !! */
	for (t=0; t< len; t++) {
		r = intrand(lo,hi);
		buffer[t]= r;
	}

	return buffer;
}

/** @brief blocksize for write function */
#define BLOCKSIZE 512

/** @brief write io object 
 * 
 * write all data from source into destionation
 @param dst destination io object
 @param src source io object
 @return 1
 */
int io_wr(io_t *dst, io_t*src)
{
	int bytes;
	char buf[BLOCKSIZE];
	
	while( (bytes = io_read(src, buf,BLOCKSIZE)) > 0) {
		//printf("hevel %d\n", bytes);
		io_write(dst, buf, bytes);
	}
	return 1;
}

static int io_test_sock(int size, int num)
{
	/* buf_t *bufp; */
	/* io_t *udp;  */
	/* io_t *tcp; */
	/* io_t *buf; */
	/* int sock; */
	char *ptr;
	/* struct hostent *host; */

	ptr = randombuffer(size,1);

	/* tcp = io_ist(IO_SOCKET, sock, "r", 0); */
	/* bufp = io_ist(IO_BUF, bufp, "w", IO_RESIZABLE); */
	/* io_wr(tcp, bufp); */

	free(ptr);

	return 1;
}

static int io_test_rw(int size, int num)
{
	buf_t *bufp;
	io_t *iofp; /*  *iofd, *iomem, *iobuf; */
	io_t *base_fp, *base_fd, *base_mem, *base_buf;
	int t;
	char *ptr;

	bufp = buf_ist(0);

	ptr = randombuffer(size,1);
	base_mem = io_ist(IO_MEM, ptr, "rw", size);

	base_fp = io_ist(IO_FP, "io_dump_fp", "w", IO_RESIZABLE);
	if (!base_fp) {
		printf("not open file \n");
		exit(0);
	}
	io_wr(base_fp, base_mem);
	/* io_close(base_fp); */

	io_rewind(base_mem);
	base_fd = io_ist(IO_FD, "io_dump_fd", "w", IO_RESIZABLE);
	io_wr(base_fd, base_mem);
	io_close(base_fd);

	io_rewind(base_mem);
	base_buf = io_ist(IO_BUF, bufp, "w", IO_RESIZABLE);
	io_wr(base_buf, base_mem);

	/* buf_dmp(bufp); */

	/* re-open the files? */
	iofp = io_ist(IO_FP, "io_dump_fp", "r", IO_RESIZABLE);

	/* hand-tests */
	for (t=0; t< size; t++) {
		char k;
		io_lseek(base_fp, t, SEEK_SET);
		while (io_read(base_fp, &k, 1)) ;
			/* printf("[%c]", k); */
		/* printf("\n"); */
	}

	io_rls(base_mem);
	io_rls(base_fp);
	io_rls(base_buf);
	io_rls(base_fd);
	io_rls(iofp);

	buf_rls(bufp);
	free(ptr);

	return 1;
}

/** @brief test io code
 *
 * internal test function
 */
int io_test(int size, int num)
{
	int ret;

	ret  = io_test_rw(size, num);
	ret &= io_test_sock(size, num);

	return ret;
}

