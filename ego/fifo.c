/** @file 
	First In First Out datatype or queue but that sucks typing;)

	it is implemented using a doubly linked list dlist_t
 */
#include <ego.h>

/** @brief instantiate fifo
 * create a new fifo 
 * @param op object type to put in the fifo (see object.c)
 * @return new fifo object
 */
fifo_t *fifo_ist(object_t *op)
{
	fifo_t *fp = (fifo_t *)calloc(sizeof(fifo_t),1);

	fp->lst = dlst_ist(op);

	return fp;
}

/** @brief release fifo
 * release fifo object and its reaources
 * @param fp pointer to fifo object
 */
void fifo_rls(fifo_t *fp)
{
	dlst_rls(fp->lst);

	free(fp);
}

/** @brief push data onto fifo
 * push new data onto the front of the queue
 * @param fp pointer to fifo object
 * @param what what data to push
 * @return 
 */
void fifo_push(fifo_t *fp, void *what)
{
	dlst_add(fp->lst, what);
}

/** @brief dump fifo data
 * dump a fifo and its data to the console
 * @param fp pointer to fifo object
 */
void fifo_dmp(fifo_t *fp)
{
	dlst_dmp(fp->lst);
}

/** @brief get first object
 * get the first object in the fifo (the last object pushed)
 * @param fp pointer to fifo object
 * @return pointer to the data of the first object
 */
void *fifo_first(fifo_t *fp)
{
	fp->cur = dlst_get_item(fp->lst,0);
	if (!fp->cur) return NULL;
	return fp->cur->data;
}

/** @brief get next object
 * get next object after fifo_first or fifo_next calls, a current pointer
 * maintains the last position
 * @param fp pointer to fifo object
 * @return pointer to the data of the next object
 */
void *fifo_next(fifo_t *fp)
{
	if (!fp->cur) return NULL;
	fp->cur = fp->cur->next;
	if (!fp->cur) return NULL;
	return fp->cur->data;
}

/** @brief pop from fifo
 * pop the last element, so get the data and remove it from the front of 
 * the queue
 * @param fp pointer to fifo object
 * @return data item at the head of the queue
 */
void *fifo_pop(fifo_t *fp)
{
	void *item;

	item = dlst_get(fp->lst, LST_FIRST);
	dlst_del(fp->lst, NULL, LST_FIRST);
	return item;
}

/** @brief get fifo length
 * get the total number of items in the queue
 * @param fp pointer to fifo object
 * @return number of items in the queue
 */
int fifo_len(fifo_t *fp)
{
	if (!fp) return 0;
	return dlst_len(fp->lst);
}

/** @brief test fifo
 * internal test function
 */
intptr_t fifo_test(int len, int wid)
{
	fifo_t *fp;
	intptr_t ret=1,t;

	fp = fifo_ist(&int_object);

	for (t=0; t< len; t++) {
		fifo_push(fp, (void *)t);
		//fifo_dmp(fp);
	}
	for (t=0; t< len; t++) {
		fifo_pop(fp);
		//fifo_dmp(fp);
	}

	fifo_rls(fp);

	return ret;
}
