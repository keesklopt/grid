#include <ego.h>

/*
    first specify the row,then the column 
	so in the _get and _set also keep the order y,x

	so cells are fille like this for mat_ist(2,4)  :

      xxxx
    y 0123
    y 4567

 */

mat_t *mat_ist(int rows, int cols)
{
	mat_t *mp=
	mp= calloc(sizeof(mat_t),1);
	mp->cols=cols;
	mp->rows=rows;
	mp->data = calloc(sizeof(int32_t),cols*rows);

	return mp;
}

void mat_dmp(mat_t *mp)
{
	int x;
	int y;

	for (y=0; y< mp->rows; y++) {
		for (x=0; x< mp->cols; x++) {
			printf("%d,", mp->data[y*mp->cols+x]);
		}
		printf("\n");
	}
}

void mat_array(char *buf, mat_t *mp)
{
	int r;
	int c;
	char wbuf[20];

	strcat(buf,"[");
	for (r=0; r< mp->rows; r++) {
		if (r>0) strcat(buf,",");
		strcat(buf,"[");
		for (c=0; c< mp->cols; c++) {
			if (c>0) strcat(buf,",");
			sprintf(wbuf,"%d", mp->data[r*mp->cols+c]);
			strcat(buf,wbuf);
		}
		strcat(buf,"]");
	}
	strcat(buf,"]");
}

void mat_dmp_array(mat_t *mp)
{
	int x;
	int y;

	printf("[");
	for (y=0; y< mp->rows; y++) {
		if (y>0) printf(",");
		printf("[");
		for (x=0; x< mp->cols; x++) {
			if (x>0) printf(",");
			printf("%d", mp->data[x*mp->rows+y]);
		}
		printf("]");
	}
	printf("]");
}

void mat_set(mat_t *mp,int y, int x, int32_t val)
{
	if (!mp || !mp->data) return;
	if (x > mp->cols) return;
	if (y > mp->rows) return;
	mp->data[y*mp->cols+x]=val;
}

int32_t mat_get(mat_t *mp,int y, int x)
{
	if (!mp || !mp->data) return -1;
	if (x > mp->cols) return -1;
	if (y > mp->rows) return -1;
	return mp->data[y*mp->cols+x];
}

void mat_rls(mat_t *mp)
{
	if (!mp) return;
	if (mp->data) free (mp->data);
	free(mp);
}

int mat_test(int wid, int len)
{
	mat_t *mp = mat_ist(3,6);

	int x;
	int y;
	int32_t val;

	for (y=0; y< mp->rows; y++) {
		for (x=0; x< mp->cols; x++) {
			val=x*100+y;
			mat_set(mp,y,x,val);
		}
	}
	mat_dmp(mp);
	return 1;
}
