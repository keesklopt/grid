
/** @file 
	interval handling
<pre>
 you can steer the handling with the compare function !!
 union of [a-d,e-f] is [a-d,e-f] exlusive [a-f] inclusive
 we cannot assume the data is integer, so your cmp function should return :
 -2 : smaller than
 -1 : exactly 1 smaller than (consecutive)
 0 : same
 1 : exactly 1 larger than
 2 : larger than
</pre>
 */
#include <ego.h>

/** @brief release interval object 
 free data associated with an interval object in the format needed to create 
 an object_t generic object (object.c)
 @param ivp  pointer to interval object as a void *
 */
void iv_rls(void *ivp)
{
	free(ivp);
}

/** @brief dump an interval object
 dump an interval object in the format needed to create 
 an object_t generic object (object.c)
 @param wid width of printed data, optionally used
 @param data generic pointer to the data to be printed, mainly cast this back
        to the object you want to dump
 @param udata generic pointer to any data a user might want to have passed
 */
void iv_dmp(intptr_t wid, const void *data, void *udata)
{
	const iv_t *ivp=data;
	object_t *op= udata;
	const object_t *sop= op->udata;

	sop->dmp(0,ivp->left,op);
	printf("-");
	sop->dmp(0,ivp->right,op);
}

/** @brief compare two interval objects
 compare two interval objects in the format needed to create 
 an object_t generic object (object.c)
 @param left generic pointer to the left operand in the compare operation
 @param right generic pointer to the right operand in the compare operation
 @param udata generic pointer to any data a user might need in comparing
 */
int iv_cmp(const void *left, const void *right, void *udata)
{
	int ret;
	const iv_t *a = left;
	const iv_t *b = right;
	object_t *op = udata;
	op = op->udata;
	
	if (!a && !b) return 0;
	if (!a) return 1; /* NULL is after the rest */
	if (!b) return -1; /* NULL is after the rest */

	ret = op->cmp(a->left,b->left,udata);
	if (ret == 0)
	ret = op->cmp(a->right,b->right,udata);

	return ret;
}

/* list traversal function that draws a comma between items */
static void comma_traverse(dlst_t *lst, ditem_t *dp, void *udata)
{
	object_t *op = udata;
	iv_t *ivp = dp->data;

	if (dp != lst->head.next) printf(",");
	op->dmp(0,ivp,op);
}

/** @brief dump an interval set
 
 dump a complete interval set as [ interval,interval,...]

 @param wid width to be printed, make 0 if not needed
 @param data generic pointer to the intervalset to be printed
 @param xtra generic pointer to optional extra data
*/
void ivset_dmp(intptr_t wid, const void *data, void *xtra)
{
	const ivset_t *isp=data;
	//object_t *op = xtra;
	object_t op = isp->op;

	lprintf(LOG_ALOT, "[");
	dlst_traverse(isp->ivs, comma_traverse, &op);
	lprintf(LOG_ALOT, "]");
}

/** @brief compare two interval sets

	not implemented yet !!!
 @param a not implemented yet
 @param b not implemented yet
 @param xtra not implemented yet
*/
int ivset_cmp(const void *a, const void *b, void *xtra)
{
	return 0;
}

/** @brief duplicate an interval set

	make an exact copy if an interval  set
 @param orig pointer to the original interval set
 @return newly created copy or duplicate
*/
ivset_t *ivset_dup(ivset_t *orig)
{
	ditem_t *li;
	iv_t *ip;

	ivset_t *isp = ivset_ist(orig->op.udata);

	for (li = dlst_first(orig->ivs); li != &orig->ivs->tail; li = dlst_next(li))
	{
		ip = li->data;
		ivset_ins(isp, ip->left, ip->right);
	}

	return isp;
}

/** @brief generic object representation of a single interval */
object_t iv_object = { "interval", iv_rls, iv_cmp, iv_dmp, NULL};
/** @brief generic object representation of an interval set */
object_t ivset_int_object = { "interval set", ivset_rls, ivset_cmp, ivset_dmp, &intchar_object};

/** @brief merge interval sets 

  merge interval set b into a this side-effects a, while b remains unchanged
	it has the same effect as ivset_ins(a) all intervals from b 

<pre>
	example :
	a = [left-right]
	b = [middle-zzz]

	ivset_merge(a,b) = [left-right,middle-zzz]
</pre>

 @param a destination interval set 
 @param b source interval set to be merged into a
 */
void ivset_merge(ivset_t *a, ivset_t *b)
{
	ditem_t *li;
	iv_t *ip;

	for (li = dlst_first(b->ivs); li != &b->ivs->tail; li = dlst_next(li))
	{
		ip = li->data;
		ivset_ins(a, ip->left, ip->right);
	}
}

/** @brief unite intervals in a set

  make a union of all intervals in an interval set. 
<pre>
	example :
	ab = [left-right,middle-zzz]

	ivset_union(ab) = [middle-right] 

	because graphically / alphabetically :
	left ------------------ right
	          middle ------------------ zzz
	leaves :
    left ------------------------------ zzz
</pre>

 @param a interval set 
 */
void ivset_union(ivset_t *a)
{
	ditem_t *li;
	iv_t *prev=NULL,*next;
	object_t *sob = (object_t *)a->op.udata;
	int ret;

again:
	for (li = dlst_first(a->ivs); li != &a->ivs->tail; li = dlst_next(li))
	{
		next = li->data;
		if (prev) {
			ret = sob->cmp(prev->right,next->left,NULL);
			if (ret >= -1) { /* merge next into last */
				ret = sob->cmp(prev->right,next->right,NULL);
				if (ret < 0) {
					prev->right = next->right;
				}
				dlst_del(a->ivs, li, LST_THIS);
				prev = NULL;
				goto again;
			}
		}
		prev = next;
	}
}

/** @brief intersect intervals in a set

  make an intersection of all intervals in an interval set. 
<pre>
	example :
	ab = [left-right,middle-zzz]

	ivset_intersection(ab) = [middle-right] 

	because graphically / alphabetically :
	left ------------------ right
	          middle ------------------ zzz
	leaves :
              middle ------ right
</pre>

 @param a destination interval set 
 */
void ivset_intersection(ivset_t *a)
{
	ditem_t *li;
	iv_t *prev=NULL,*next;
	object_t *sob = (object_t *)a->op.udata;
	int ret;

again:
	for (li = dlst_first(a->ivs); li != &a->ivs->tail; li = dlst_next(li))
	{
		next = li->data;
		if (prev) {
			ret = sob->cmp(prev->right,next->left,&a->op);
			if (ret >= 0) {
				prev->left = next->left;
				ret = sob->cmp(prev->right,next->right,&a->op);
				if (ret >= 0) {
					prev->right = next->right;
				}
				dlst_del(a->ivs, li, LST_THIS);
				prev = NULL;
				goto again;
			} else {
				dlst_empty(a->ivs);
				goto again;
			}
		}
		prev = next;
	}
}

/** @brief instantiate interval set object

 construct an object that has the 'real' object as handle
 @param op generic object pointer to use as base type
 @return pointer to a new interval set object
 */
ivset_t *ivset_ist(object_t *op)
{
	ivset_t *isp = (ivset_t *)calloc(sizeof(ivset_t),1);
	//isp->op = *op;
	isp->op = iv_object;
	isp->op.udata = op;

	isp->ivs = dlst_ist(&isp->op);
	dlst_sort(isp->ivs, &isp->op , NULL);

	return isp;
}

/** @brief gut interval set

 * take out the inner data of an interval object but leave the structure intact
 * gutting and deboning are use when you either only want to free up the data
 * from a data structure (gut) or only want ti remove the structure (debone) 
 @ param opaq unused
 @ op pointer to interval set to disembowle
 */
void ivset_gut(void *opaq, object_t *op)
{
	ivset_t *isp=opaq;
	if (!isp) return;

	/* release the intervals */
	if (isp->ivs) {
		dlst_gut(isp->ivs, op);
		/* dlst_debone(isp->ivs); */
	}
}

/** @brief debone interval set

 * take out the the structure of an interval object but leave the internal 
 data intact, for instance when you still want to use that data
 @ isp pointer to interval set to debone
 */
void ivset_debone(ivset_t *isp)
{
	if (!isp) return;

	free(isp);
}

/** @brief release interval set

 free up all data used by aninterval set
 @param opaq opaque pointer to the interval set to make this suitable for 
	use in an object_t format
*/
void ivset_rls(void *opaq)
{
	ivset_t *isp=opaq;

	ivset_gut(isp, &isp->op);
	ivset_debone(isp);
}


/** @brief delete an interval

 delete one interval from an interval set
 @param isp pointer to an interval set
 @param pos interval at which position to delete
 @return 0
*/
int ivset_del(ivset_t *isp, int pos)
{
	iv_t *ip = dlst_get(isp->ivs, pos);
	free(ip);
	dlst_del(isp->ivs, NULL, pos);
	return 0;
}
 
/** @brief insert an interval

 insert one interval into an interval set
 @param isp pointer to an interval set
 @param l opaque pointer to the left border element
 @param r opaque pointer to the right border element
 @return 0
*/
int ivset_ins(ivset_t *isp, void *l, void *r)
{
	iv_t *iv = calloc(sizeof(iv_t),1);

	iv->left = l;
	iv->right = r;
	dlst_ins(isp->ivs, iv);
	return 0;
}

/** @brief interval set length

	return the number of intervals in an interval set
 @param isp pointer to an interval set
 @return number of intervals
*/
int ivset_len(ivset_t *isp)
{
	if (!isp) return 0;
	return dlst_len(isp->ivs);
}

static int iv_test_combinations(void)
{
	ivset_t *a;
	ivset_t *b;

	a = ivset_ist(&int_object);
	b = ivset_ist(&int_object);

	ivset_ins(a, (void *)10, (void *)29);
	ivset_ins(b, (void *)28, (void *)40);
	ivset_ins(b, (void *)31, (void *)50);

	ivset_dmp(0, a, NULL);
	ivset_dmp(0, b, NULL);

	ivset_merge(a,b);
	ivset_dmp(0, a, NULL);

	/* ivset_intersection(b); */
	/* ivset_union(b); */
	ivset_dmp(0, b, NULL);

	ivset_rls(a);
	ivset_rls(b);

	return 1;
}

static void iv_demo(void)
{
	ivset_t *ia;
	ivset_t *ib;
	ivset_t *ic;

	ia = ivset_ist(&int_object);
	ib = ivset_ist(&str_object);
	ic = ivset_ist(&str_object);

	lprintf(LOG_ALOT,"An interval is any object with a left and a right border\n");
	lprintf(LOG_ALOT,"integer values seem obvious : \n");
	log_setcolor(LOG_ALOT, IO_GREEN);
	lprintf(LOG_ALOT,"ia = ivset_ist(&int_object);\n");
	lprintf(LOG_ALOT,"ivset_ins(ia, (void *)100, (void *)1000);\n");
	lprintf(LOG_ALOT,"ivset_dmp(0, ia, NULL);\n");
	log_setcolor(LOG_ALOT, IO_ORANGE);

	ivset_ins(ia, (void *)100, (void *)1000);
	ivset_dmp(0, ia, NULL);
	log_setcolor(LOG_ALOT, IO_NORMAL);

	printf("\n\nBut if you provide a sensible object definition (see object.c)\n");
	printf("any type is possible, for instance strings :\n");
	log_setcolor(LOG_ALOT, IO_GREEN);
	lprintf(LOG_ALOT,"ia = ivset_ist(&str_object);\n");
	lprintf(LOG_ALOT,"ivset_ins(ia, (void *)\"left\", (void *)\"right\");\n");
	lprintf(LOG_ALOT,"ivset_dmp(0, ia, NULL);\n");
	log_setcolor(LOG_ALOT, IO_ORANGE);
	ivset_ins(ib, (void *)"left", (void *)"right");
	ivset_dmp(0, ib, &str_object);
	log_setcolor(LOG_ALOT, IO_NORMAL);

	printf("\n\nbecause only the borders are stored, NOT the intermediate values\n");
	printf("so only sorting order matters\n\n");
	printf("A interval set is a set of intervals, here's an overlapping interval added:\n\n");
	log_setcolor(LOG_ALOT, IO_GREEN);
	lprintf(LOG_ALOT,"ivset_ins(ia, (void *)\"middle\", (void *)\"zzz\");\n");
	lprintf(LOG_ALOT,"ivset_dmp(0, ia, NULL);\n");
	log_setcolor(LOG_ALOT, IO_ORANGE);
	ivset_ins(ib, (void *)"middle", (void *)"zzz");
	ivset_dmp(0, ib, NULL);
	log_setcolor(LOG_ALOT, IO_NORMAL);
	printf("\n\nYou can now generate an intersection over these intervals:\n");
	log_setcolor(LOG_ALOT, IO_GREEN);
	lprintf(LOG_ALOT,"ib = ivset_dup(ib)\n");
	lprintf(LOG_ALOT,"ic = ivset_dup(ib)\n");
	ic = ivset_dup(ib);
	lprintf(LOG_ALOT,"ivset_intersection(ib)");
	ivset_intersection(ib);
	lprintf(LOG_ALOT,"ivset_dmp(0, ib, NULL);\n");

	log_setcolor(LOG_ALOT, IO_ORANGE);
	ivset_dmp(0, ib, NULL);

	log_setcolor(LOG_ALOT, IO_NORMAL);
	printf("\n\nOr a union over these intervals:\n");
	log_setcolor(LOG_ALOT, IO_GREEN);
	lprintf(LOG_ALOT,"ivset_intersection(ic)");
	ivset_union(ic);
	lprintf(LOG_ALOT,"ivset_dmp(0, ic, NULL);\n");
	log_setcolor(LOG_ALOT, IO_ORANGE);
	ivset_dmp(0, ic, NULL);

	printf("\n");
	log_setcolor(LOG_ALOT, IO_NORMAL);
}

/** @brief test intervals
 
 internal  tests 
 @param num
 @param wid
 */
int iv_test(int num, int wid)
{
	int t;
	intptr_t a,b;
	ivset_t *isp;

	num=10;

	iv_demo();

	iv_test_combinations();

	return 0;

	isp = ivset_ist(&int_object);

	for (t=0; t< num; t++) {
		a = intrand(10,1000); 
		b = intrand(a, 1001);
		ivset_ins(isp, (void *)a, (void *)b);
		ivset_dmp(0, isp, &int_object);
	}
	for (t=0; t< num; t++) {
		a = ivset_len(isp)-1;
		b = intrand(0,a);
		ivset_del(isp, b);
	}

	ivset_rls(isp);
	return 1;
}
