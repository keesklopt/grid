#include <ego.h>
/*  @file   (UNFINISHED don't document until then)
 * buffered io
	simple buffered io just to be able to 'peek' forward and backward by the amount 
	specified, forward it is possible to just stretch the buffer on demand
	backwards we need a specified amount otherwise we can never release the cache

*/

/** @brief release buffered io object
 *
 * release an bio pointer instantiated by bio_ist and all it's subsctructures
 *
 * @param bp pointer to an bio_t object
 */
void bio_rls(bio_t *bp)
{
	if (!bp) return;

	io_rls(bp->iop);
	buf_rls(bp->cache);
	free(bp);
}

/** @brief instantiate buffered io object
 *
 * create an buffered io objects and return a handle to it
 *
 * @param iop pointer to a non buffered io object acting as input for buffering
 * @param blocksize blocksize of the caching
 * @param lookback maximum lookback cache size
 * @return pointer to bio_t structure
 */
bio_t *bio_ist(io_t *iop, int blocksize, int lookback)
{
	bio_t *bp = (bio_t *)calloc(sizeof(bio_t),1);

	/* bp->cache = fifo_ist(0); */
	/* bp->cache = buf_ist(blocksize); */

	/* fifo works with generic object so construct one: */
	/* objectp = object_ist("buf", rls_fnc_t rls, cmp_fnc_t cmp, dmp_fnc_t dmp, void *udata) */
	return bp;
}

size_t bio_read(bio_t *iop,char *buf, size_t len, size_t nmemb)
{
	int ret=0;

	return ret;
}

size_t bio_write(io_t *iop,char *buf, size_t len, size_t nmemb)
{
	return 0;
}

void bio_dmp(bio_t *bp)
{
	/* printf("Buffered io object remebering %d and looking %d ahead \n",  */
		/* bp->trail, bp->lead); */

}

int bio_test(int len, int wid)
{
	io_t *filep = io_ist(IO_FD, "parsefile.txt", "r", 0);
	bio_t *bp = bio_ist(filep, 10, 10);

	bio_dmp(bp);

	bio_rls(bp);
	io_rls(filep);

	return 1;
}
