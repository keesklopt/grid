#include <ego.h>
/* ring buffer implementation :

	- NOT resizable 
	- the read/write interface is somewhat like libc read/write except if the 
	  return values is short, this means there was no room in the buffer.
	- end of file is signalled through the eof structure member. when writing you 
      should call rbuf_eof() to signal you'v written the last bytes, rbuf_read() 
      returns -1 AFTER it has read all bytes (so you always do 1 extra rbuf_read()) 

	test results : testbuffer len,rbuf size,#written,#read
	plain test:	100,100,100,100	
	valgrind :	100,20,20,20
	threadsafe: no (see 1pty's ts_rbuf !!)
*/

void rbuf_rls(rbuf_t *rbp)
{
	if (!rbp) return;

	buf_rls(rbp->bufp);
	free(rbp);
}

rbuf_t *rbuf_ist(int size)
{
	rbuf_t * rbp = (rbuf_t *)calloc(sizeof(rbuf_t),1);

	rbp->bufp = buf_ist(size);
	rbp->eof = 0;

	return rbp;
}

void rbuf_eof(rbuf_t *rbf)
{
	rbf->eof = 1;
}

int rbuf_filled(rbuf_t *rbf)
{
	return rbf->hi - rbf->lo;
}

int rbuf_len(rbuf_t *rbf)
{
	return buf_len(rbf->bufp);
}

int rbuf_empty(rbuf_t * rbf)
{
	return rbuf_len(rbf) - rbuf_filled(rbf);
}

void rbuf_dmp(rbuf_t *rbp, dmp_fnc_t dmp)
{

	printf("Ring buffer [%d] [%d,%d]\n", rbuf_len(rbp), rbp->lo, rbp->hi);

	buf_dmp(rbp->bufp);
}

int rbuf_read(rbuf_t *stream, void *p, size_t nbytes)
{
	unsigned int rest, ret;
	int len, pos;
	char *ptr = p;

	len = rbuf_len(stream);
	rest = rbuf_filled(stream);

	if (rest < nbytes) nbytes = rest;

	/* read hi part first */
	pos = stream->lo % len;
	ret = len-pos;
	ret = EGO_MIN(ret, nbytes);
	buf_get_str(ptr ,stream->bufp, pos, ret);
	nbytes -= ret;
	ptr += ret;

	/* wrap to lo part */
	if (nbytes > 0) {
		buf_get_str(ptr ,stream->bufp, 0, nbytes);
		ptr += nbytes;
		ret += nbytes;
	}

	stream->lo += ret;

	// wrap protection !
	while (stream->lo > rbuf_len(stream) &&
	    stream->hi > rbuf_len(stream))
	{
		stream->lo -= rbuf_len(stream);
		stream->hi -= rbuf_len(stream);
	}
	

	if (!ret && stream->eof) return -1; // buf empty 

	return ret;
}

int rbuf_write(rbuf_t *stream, void *ptr, size_t nbytes)
{
	size_t len;
	int rest;
	int total=0;
	int hi,lo,end;
	char *cptr=ptr;

	rest = rbuf_empty(stream);

	if (stream->eof) return 0;
	if ((size_t)rest < nbytes) nbytes = rest;

	/* hi = (stream->hi-1) % rbuf_len(stream) +1; */
	hi = stream->hi % rbuf_len(stream);
	lo = stream->lo % rbuf_len(stream);
	end = (stream->hi + nbytes ) % rbuf_len(stream);
	stream->hi += nbytes;
	if (hi <= end) { // contigues bytes 1,1 is also contigues (1 bytes from 1 to 1)
		buf_set_str(stream->bufp, hi, nbytes, cptr);
		return nbytes;
	}
	
	// we have a split buffer, so write front (hi-len)
	len = rbuf_len(stream)-hi;
	if (len) {
		hi %= rbuf_len(stream);
		buf_set_str(stream->bufp, hi, len, cptr);
		total += len; 
		nbytes -= len;
		cptr += len;
	}

	// then write back 0-lo)
	if (nbytes > 0) {
		len = lo;
		len = EGO_MIN(nbytes,len); // but of course no more than nbytes
		buf_set_str(stream->bufp, 0, len, cptr);
		total += len; 
	}

	return total;
}

int rbuf_test_exhaust(int len, int rlen, int r, int w)
{
	rbuf_t *rbp = rbuf_ist(rlen);
	char *get, *set;
	char *inp;
	buf_t *base;
	int bufcounter=len;
	int nr=0, nw=0;
	/* int shortr, shortwr; */

	int  t;

	/* make all buffers max */
	set = calloc(len,1);
	get = calloc(len,1);
	memset(get, 0, len);
	base = buf_ist(len);

	buf_test_randfill(base, len, len);
	inp = buf_get(base, 0);
	memset(inp, 0, len);
	/* printf ("das %s------------------------\n", inp); */

	while(1) {
		w = EGO_MIN(w,bufcounter); // signal the end with bufcounter
		if (!w) rbuf_eof(rbp);
		t = rbuf_write(rbp, &inp[nw], w);
		nw += t;
		bufcounter -= t;
		/* rbuf_dmp(rbp,NULL); */
		t = rbuf_read(rbp, &get[nr], r);
		if (t< 0) break;
		nr += t;
		/* printf("--> %s (%d,%d)\n", get, nw, nr); */
		/* rbuf_dmp(rbp,NULL); */
	}

	if (nr != nw) {
		log_setcolor(1,IO_RED);
		lprintf(1,"num fail %d != %d\n", nw, nr);
		return 0;
	}
	if (strcmp(inp, get)) {
		log_setcolor(1,IO_YELLOW);
		lprintf(1,"cmp fail %s != %s\n", inp, get);
		return 0;
	}

	rbuf_rls(rbp);
	free(get);
	free(set);
	buf_rls(base);

	return 1;
}

int rbuf_test(int len, int wid)
{
	int ret=1;
	int sub;
	int x,y,z;

	wid = 10;

	for (x=1; x< wid; x++) {
		for (y=1; y< wid; y++) {
			for (z=1; z< wid; z++) {
				sub = rbuf_test_exhaust(len,x,y,z);
				ret &= sub;
			}
		}
	}
	return ret;
}
