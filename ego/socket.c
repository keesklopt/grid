#include <ego.h>

#ifdef WIN32
#include <stdio.h>
#include <winsock2.h>
#include <io.h>
#define close _close
#else
#include <sys/socket.h>
#include <sys/param.h>
#include <fcntl.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <dirent.h>
#endif

/* socket wrapper functions, IPV6 is kept in mind, but not completely implemented/tested
*/

static unsigned int socklen = sizeof(struct sockaddr_in);

/** release socket

	free resources occupied by the socket

	@param sp socket pointer
 */
void socket_rls(socket_t *sp)
{
	close(sp->fd);
	free(sp);
}

/** instantiate socket extended.

    instantiate socket object with other than the default options:
	AF_INET,and IPPROTO_TCP

	@param family socket family
	@param type socket type (stream or dgram)
	@param proto protocol
	@return a pointer to a socket object
 */
socket_t *socket_ist_ext(int family, int type, int proto)
{
	socket_t *sp = (socket_t *)calloc(sizeof(socket_t),1);
	
	sp->type   = type;
	sp->family = family;
	sp->fd     = socket(family, type, proto);
	/* fcntl(sp->fd,F_SETFD,FD_CLOEXEC); */
	/* fcntl(sp->fd,F_SETFD,0); */
	if (sp->fd < 1) perror("socket");

	return sp;
}

/** instantiate socket 

    instantiate socket object with default options:
	AF_INET,and IPPROTO_TCP

	@param type socket type (stream or dgram)

	@return a pointer to a socket object
 */
socket_t *socket_ist(int type)
{
	int proto;
	/* return socket_ist_ext(AF_INET, type, IPPROTO_TCP); */
	if (type == SOCK_DGRAM) proto = IPPROTO_UDP;
	if (type == SOCK_STREAM) proto = IPPROTO_TCP;
	return socket_ist_ext(AF_INET, type, proto);
}

/** make socket reusable

	this prevents the problem of having to wait for a socket to be
	released after a program crash or something,

	@param socket object pointer
	@return 0 on success -1, on error
 */
int socket_reuse(socket_t *sp)
{
	int yes=1;

	if (setsockopt(sp->fd, SOL_SOCKET, SO_REUSEADDR, (void *)&yes, sizeof(int)) == -1) {
		return -1;
    }
	return 0;
}


/** set the opposite socket address

	call this function before you call socket_connect, 
	it specifies the connectee

	@param sp object pointer
	@param name hostname of the peer
	@param port port of the peer
	@return 0 on success, errnum otherwise
 */
int socket_setpeerbyname(socket_t *sp, char *name, short port)
{
	struct hostent *hep;
/* #define TESTING  */
#ifdef TESTING
	int t=0;
#endif

	sp->peer.sa4.sin_family = AF_INET;
	/* bs->peer.sa4.sin_family = sp->family; */
	if (name)
		hep = gethostbyname(name);
	else {
		sp->peer.sa4.sin_addr.s_addr = INADDR_ANY;
		goto wrapup;
	}

	if (!hep) 
		return err_set(ERR_LIBC);

#ifdef TESTING
	while  (hep->h_aliases[t]) {
		lprintf(LOG_DEBUG, "alias %s\n", hep->h_aliases[t]);
		t++;
	}
	t=0;
	while  (hep->h_addr_list[t]) {
		lprintf(LOG_DEBUG, "address %x\n", *(int *)hep->h_addr_list[t]);
		t++;
	}
#endif

	sp->peer.sa4.sin_addr.s_addr = *(int *)hep->h_addr_list[0];
wrapup:
	sp->peer.sa4.sin_port = htons(port);
	return 0;
}

/** set the local socket address

	@param sp object pointer
	@param name hostname of the local machine
	@param port port of the peer (0 for random port)
	@return 0 on success, errnum otherwise
 */
int socket_sethostbyname(socket_t *sp, char *name, short port)
{
	struct hostent *hep;
	char myname[MAXHOSTNAMELEN];

#define TESTING
#ifdef TESTING
	int t=0;
#endif

	if (name == NULL) {
		gethostname(myname, MAXHOSTNAMELEN);
		lprintf(LOG_DEBUG, "This is %s\n", myname);
		name = myname;
	}

	if (name)
		hep = gethostbyname(name);
	else {
		sp->addr.sa4.sin_addr.s_addr = INADDR_ANY;
		goto wrapup;
	}

	if (!hep) 
		return err_set(ERR_LIBC);

#ifdef TESTING
	while  (hep->h_aliases[t]) {
		lprintf(LOG_DEBUG,"alias %s\n", hep->h_aliases[t]);
		t++;
	}
	t=0;
	while  (hep->h_addr_list[t]) {
		lprintf(LOG_DEBUG,"alias %x\n", *(int *)hep->h_addr_list[t]);
		t++;
	}
#endif

	sp->addr.sa4.sin_addr.s_addr = *(int *)hep->h_addr_list[0];
wrapup:
	sp->addr.sa4.sin_port = (int)htons(port);
	return 0;
}

/** bind socket to local socket address

	give socket an address, currently the port overwrites the one 
	set whith socket_sethostbyname.

	@param sp object pointer
	@param port of the socket (0 for random port)
	@return 0 on success, errnum otherwise
 */
int socket_bind(socket_t *sp, short port)
{
	int ret;
	struct sockaddr a;
    struct sockaddr_in *sa4;
#ifdef HAVEIPV6
    struct sockaddr6_in *sa6;
#endif
	unsigned int namelen = sizeof(a);

	/* sp->addr.sa4.sin_addr.s_addr = INADDR_ANY; */
	lprintf(LOG_DEBUG,"-- %x\n", sp->addr.sa4.sin_addr.s_addr);
	sp->addr.sa4.sin_port = (int)htons(port);

	sp->addr.sa4.sin_family = sp->family;
	sp->addr.sa4.sin_port = htons(port);
	ret = bind(sp->fd, (struct sockaddr *)&sp->addr.sa, socklen);

	if (ret != 0)
	{
		perror("could not bind socket");
		return ret;
	}

	/* find out which port  */
	getsockname(sp->fd, &a, &namelen);
	sp->family = a.sa_family;
	if (sp->family == AF_INET) {
		sa4 = (struct sockaddr_in *)&a;
		sp->port = ntohs(sa4->sin_port);
	}
#ifdef HAVEIPV6
	if (sp->family == AF_INET6) {
		sa6 = (struct sockaddr_in *)a;
		sp->port = sa6.sin_port;
	}
#endif

	/* sp->port = ntohs(a.sa4.sin_port); */
	lprintf(LOG_DEBUG, "port %d\n", sp->port);
	return 0;
}

/** give the ipv4 string of this sockaddr_in 

	gives the familiar string "A.B.C.D"

	@param saddr socket address
	@return printable address
 */
char *ip4_string(struct sockaddr_in *saddr) {
    char *hostname;
    struct in_addr *addr = &saddr->sin_addr;

    if (!addr)
        hostname = ego_sprintf("-");
    else
        hostname = ego_sprintf("%u.%u.%u.%u",
        (addr->s_addr & 0x000000FF) >> (8*0),
        (addr->s_addr & 0x0000FF00) >> (8*1),
        (addr->s_addr & 0x00FF0000) >> (8*2),
        (addr->s_addr & 0xFF000000) >> (8*3));

    return hostname;
}

/** connect the socket to the given peer

	currently name and port overwrite any set by socket_setpeerbyname

	@param sp socket object pointer
	@param name peer hostname
	@param port peer port
	@return connect return value
 */
int socket_connect(socket_t *sp, char *name, short port)
{
    socket_setpeerbyname(sp, name, port);

    return connect(sp->fd, (struct sockaddr *)&sp->peer, socklen);
}

/** dump address in readible form

	format is adress:port

	@param sa socket address
 */
void address_dmp(struct sockaddr_in sa)
{
	char *str;

    str = ip4_string(&sa);
	lprintf(LOG_BLAH,  "%s:%d\n", str, ntohs(sa.sin_port));
}

/** dump both local and peer address

	@param sp socket object pointer
 */
void socket_dmp(socket_t *sp)
{
	address_dmp(sp->addr.sa4);
	address_dmp(sp->peer.sa4);
}

/** duplicate socket object

	duplicate socket, only useful for socket_accept

	@param sp socket object pointer
	@return cloned socket object pointer
 */
static socket_t *socket_dup(socket_t *sp)
{
	socket_t *np = (socket_t *)calloc(sizeof(socket_t),1);
	
	memcpy(np,sp,sizeof(socket_t));

	return np;
}

/** accept incoming connection

	accepts socket and assigns the file descriptor

	@param socket object pointer
	@return new cloned socket object pointer
 */
socket_t *socket_accept(socket_t *sp)
{
	socket_t *np;

	// don't accept socket if it's a UDP socket
	if (sp->type == SOCK_DGRAM) return sp;

	np= socket_dup(sp);
	np->fd = accept(sp->fd,&sp->peer.sa,&socklen);
	return np;
}

/** listen on socket

	@param socket object pointer
	@return listen return value
 */
int socket_listen(socket_t *sp)
{
	return listen(sp->fd,10);
}

#define BLEN 100
int socket_test(int len, int num)
{
	socket_t *sp;
	io_t *iop;
	int ret;
	char buf[BLEN];
	char myname[MAXHOSTNAMELEN];

	gethostname(myname, MAXHOSTNAMELEN);
	printf("This is %s\n", myname);

	sp = socket_ist(SOCK_STREAM);

	socket_sethostbyname(sp, NULL, 0);
	socket_bind(sp, 10101);


	/* socket_dmp(sp); */
	printf("connecting\n");

	/* ret = socket_connect(sp, "benmaf", 10000); */
	ret = socket_connect(sp, "217.77.138.65", 4569);
	/* ret = socket_connect(sp, "maakaf", 4569); */

	iop = io_ist(IO_SOCKET, sp, "rw", 0);
	printf("connected %d\n", ret);
	/* time_sleep(1); */
	fflush(stdout);

	ret = 1;
	while( ret > 0) {
		snprintf(buf, BLEN, "this is the client !");
		ret = io_wait(iop, WAIT_WRITE, 0);
		io_write(iop, buf, strlen(buf)+1);
		buf[0] = 0;
		time_sleep(1);
		ret = io_wait(iop, WAIT_READ, 0);
		ret = io_read(iop, buf, BLEN);
		printf("Got %d bytes \"%s\"\n", ret, buf);
		fflush(stdout);
	}
	/* socket_dmp(sp); */
	socket_rls(sp);
	io_rls(iop);
	
	return 1;
}

