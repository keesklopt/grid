#include <ego.h>

/* i split tree's in sorted (stree_t) and unsorted (tree_t) 
	stree_t is built on top of tree_t
*/

/* a node rls function in tnode_fnc_t form */
static void traverse_noderls(tree_t *tp, tnode_t *nd, void *udata,int level)
{
	tnode_rls(nd);
}

static void traverse_tdump(tree_t *tp, tnode_t *nd, void *udata,int level)
{
	dmp_fnc_t dmp = *(dmp_fnc_t *)&udata;
	if (level > 0) printf("\n%*s", level*2, "  ");
	dmp(tp->wid, nd->data, NULL);
}

/* free all user data in the tree (does NOT free the tree itself) */
static void traverse_trls(tree_t *tp, tnode_t *nd, void *udata,int level)
{
	rls_fnc_t rls = *(rls_fnc_t *)&udata;
	rls(nd->data);
}

tnode_t *tnode_ist(void *dta, int wid)
{
    tnode_t *n = (tnode_t *)calloc(sizeof(tnode_t),1);

    n->data = (void *)malloc(wid);
    memcpy(n->data, dta, wid);

    return n;
}

/* beware, NOT recursive: only copies data, not children */
tnode_t *tnode_dup(tnode_t *tp,int wid)
{
	tnode_t *cpy = tnode_ist(tp->data,wid);

	return cpy;
}

void tnode_rls(tnode_t *nd)
{
	if (!nd) return;

	if (nd->children) 
		da_rls(nd->children);	

	/* if this free fails (with valgrind for instance) 
		take a second look at your free function used in tree_strip() */
	if (nd->data) free(nd->data);

	free(nd);
}

tnode_t *tnode_get(tnode_t *parent, int pos)
{
	tnode_t **tp = da_get(parent->children,pos);

	return *tp;
}

int tnode_set(tnode_t *where, tnode_t *what, int pos)
{
	return da_set(where->children,what,pos);
}

int tnode_link(tnode_t **where, tnode_t *what, int pos)
{
    if (!where)
        return -1;

    if (pos < 0) /* not a sibling but the root */
    {
        *where = what;    
        return pos;
    } 

	if (! (*where)->children)
		(*where)->children= da_ist(sizeof(tnode_t *),0);

	da_ins((*where)->children, &what, pos,1);
    return pos;
}

void tree_tnode_rls(tree_t *tp)
{
	/* the fastest way is actually traversing in postorder */
	tree_traverse_post(tp, tp->root, traverse_noderls, NULL, 0);
}

void tree_rls(tree_t *tp)
{
	if (!tp) return;
	tree_tnode_rls(tp);
	free(tp);
}


tree_t *tree_ist(int wid)
{
	tree_t *tp = calloc(sizeof(tree_t),1);

	tp->wid = wid;

	tp->root = NULL;	/* start empty */
	
	return tp;
}

int tree_node_nchildren(tree_t *tp, tnode_t *node)
{
	if (!node) return 0;
	return da_len(node->children);
}

/* as opposed to tree_ins, this function must guarentee 
  that the *where node has 1 more child then before. or error */
int tree_ins_pure(tree_t *tp, tnode_t **where, tnode_t *what, int pos)
{
    if (!where || !(*where)) {
		return err_set(ERR_PARM);
    }
    return tnode_link(where,what,pos);
}

/* to keep tree_ins consistent. use this if root may be null */
int tree_ins(tree_t *tp, tnode_t **where, tnode_t *what, int pos)
{
    if (!where) {
		return err_set(ERR_PARM);
    }
    if (!(*where)) {
        *where = what;
        return 0;
    }

    return tnode_link(where,what,pos);
}


/* ---- traversal stuff ---- */

/* pre-order tree traversal */
int tree_traverse_pre(tree_t *tp, tnode_t *np, tnode_fnc_t fnc,void *udata,int level)
{
	int t, len;
	tnode_t *cur;

	if (!np) return 0;
	if (!np->children) len = 0;
	else len = da_len(np->children);

	fnc(tp,np,udata,level);

	for (t=0; t< len ; t++) {
		cur = tnode_get(np,t);
		tree_traverse_pre(tp,cur,fnc,udata,level+1);
	}
	return 0;
}

/* post-order tree traversal */
int tree_traverse_post(tree_t *tp, tnode_t *np, tnode_fnc_t fnc,void *udata,int level)
{
	int t, len;
	tnode_t *cur;

	if (!np) return 0;
	if (!np->children) len = 0;
	else len = da_len(np->children);

	for (t=0; t< len ; t++) {
		cur = tnode_get(np,t);
		tree_traverse_post(tp,cur,fnc,udata,level+1);
	}

	fnc(tp,np,udata,level);
	return 0;
}

/* pre is the most used order so : */
int tree_traverse(tree_t *tp, tnode_t *np, tnode_fnc_t fnc,void *udata,int level)
{
	return tree_traverse_pre(tp, np, fnc,udata,level);
}

void tree_strip(tree_t *tp, rls_fnc_t rls)
{
	tree_traverse(tp, tp->root, traverse_trls, rls,1);
}

void tree_dmp(tree_t *tp, dmp_fnc_t dmp)
{
	printf("\nPRE:\n");
	tree_traverse(tp, tp->root, traverse_tdump, dmp,0);
	printf("\n");
	/* printf("\nPOST:\n"); */
	/* tree_traverse_post(tp, tp->root, traverse_tdump, dmp,0); */
}

static void traverse_flatten(tree_t *tp, tnode_t *np, void *udata, int level)
{
	da_t *flat = (da_t *)udata;
	da_add(flat, &np);
}

/* allocates */
da_t *tree_flatten(tree_t *tp)
{
	da_t *flat = da_ist(tp->wid,0);

	tree_traverse(tp, tp->root, traverse_flatten, flat,0);
	return flat;
}

tree_t *tree_dup(tree_t *tp)
{	
	/* can we do this with data_t (not imp yet) */

	return NULL;
}

/* ---- sorted  tree --- */
void stree_strip(stree_t *stp, rls_fnc_t rls)
{
	tree_t *tp = stp->basetree;
	tree_traverse(tp, tp->root, traverse_trls, rls,1);
}

void stree_rls(stree_t *tp)
{
	tree_rls(tp->basetree);
	free(tp);
}

void stree_dmp(stree_t *tp, dmp_fnc_t dmp)
{
	tree_dmp(tp->basetree, dmp);
}

stree_t *stree_ist(int wid, cmp_fnc_t cmp, unsigned int max)
{
	stree_t *tp = calloc(sizeof(stree_t),1);
	tp->cmp = cmp; 	/* NULL if !sorted */

	tp->basetree = tree_ist(wid);
	/* this seems bogus, but if you say 1 exactly i presume  */
	/* you realy want a one-legged tree (that's a list) */
	if (max < 1) max =2;  /* otherwise i default to 2 */
	tp->maxbranch = max;

	return tp;
}

int tnode_pos(stree_t *tp, tnode_t **root, tnode_t *nd)
{
    int res, t, pos = -1;
    for (t=0; t< tree_node_nchildren(tp->basetree,*root); t++) 
    {
        tnode_t *n = tnode_get((*root),t);
        res = tp->cmp(nd->data, n->data, NULL);
        if (res == 0) { 
            /* printf("DUPLICATE NODE, appended !!\n"); */
        }  else if ( res < 0) {
            continue; /* return last one that was bigger */
        } else {
            pos = t; /* remember that this was bigger */
        }
    }
    return pos;
}

void tree_swap_data(tree_t *tp, tnode_t *a, tnode_t *b)
{
	void *tmp = (void *)malloc(tp->wid);

	memcpy(tmp,a->data,tp->wid);
	memcpy(a->data,b->data,tp->wid);
	memcpy(b->data,tmp,tp->wid);

	free(tmp);
}

/* in sorted tree there is no _ins, in tree there is no _add */
int stree_add(stree_t *tp, tnode_t **where, tnode_t *what)
{
    int pos, res;
    tnode_t *tmp;

    /* 1. test the given root node */
    if (!where) {
		return err_set(ERR_PARM);
    }
    if (tp->cmp == NULL) {
		return err_set(ERR_TREE_CMP);
	}
    if (!(*where)) {    
        /* printf("No more leaves . Adding node right here !!\n"); */
        *where = what;
        return 0;
    }

    res = tp->cmp(what->data, (*where)->data, NULL);
    /* printf("res is %d \n", res); */
    if (res < 0) {
        /* printf("SWAP (insert root instead of node!!\n"); */
		tree_swap_data(tp->basetree,*where,what);
    }

	/* stree_dmp(tp, str_dmp); */

    /* 2. FIRST find the place, what to do with it we see later */
    pos = tnode_pos(tp, where, what);
    /* printf("FOUND %d\n", pos); */

    /* 3. if there's room link it right there */
    if (tree_node_nchildren(tp->basetree,*where) < tp->maxbranch)
    {
        tnode_link(where,what,pos+1); /* note that this relies on tnode_pos */
									  /* returning exactly -1 on smaller than */
    } else {
    /* 4. otherwise : descend into the tree at pos */
        if (pos < 0) { /* unless it's smaller than root */
            /* printf("Root should be swapped\n"); */
			/* than it should become the root */
			tmp = tnode_get((*where),0);
			/* then add the original root recursively */
            stree_add(tp, &tmp, what);
        } else {
			tnode_t *n = tnode_get((*where),pos);
            stree_add(tp, &n, what);
        }
    }
	return 0;
}

/* test section */
int stree_test(int num,int len,int k)
{
	int t;
	char *data;
	stree_t *tp;
	tnode_t *nd;

	tp = stree_ist(sizeof(char *), strp_cmp, k);

	for (t=0; t< num; t++) {
		/* r = intrand(1,len); */
		data = strrand(len);
		printf("Adding %s\n", data);
		nd = tnode_ist(&data,tp->basetree->wid);
		stree_add(tp, &tp->basetree->root, nd);
	}
	stree_dmp(tp, strp_dmp);
	stree_strip(tp,strp_rls);
	stree_rls(tp);
	
	return 1;
}

int tree_test_data(int num,int len,int k)
{
	int t;
	char *data;
	stree_t *tp;
	tnode_t *nd;
	instance_t *ip;

	data_init(1);

	tp = stree_ist(sizeof(char *), strp_cmp, k);

	for (t=0; t< num; t++) {
		/* r = intrand(1,len); */
		data = strrand(len);
		nd = tnode_ist(&data,tp->basetree->wid);
		stree_add(tp, &tp->basetree->root, nd);
	}

	ip= instance_ist("stree_t *");
	instance_pack(ip, (void *)&tp,1);

	stree_dmp(tp, str_dmp);

	stree_strip(tp,strp_rls);
	stree_rls(tp);

	instance_unpack(ip, (void *)&tp);
	stree_dmp(tp, str_dmp);
	
	return 1;
}

int utree_test(int num,int len)
{
	int r,t;
	char *data;
	tree_t *tp;
	tnode_t *nd, **wlk;

	da_t *flattened;

	tp = tree_ist(sizeof(char *));

	for (t=0; t< num; t++) {
		data = strrand(10);

		r = intrand(0,t);
		flattened = tree_flatten(tp);
		wlk = da_get(flattened, r);
		if (!wlk) wlk = &tp->root;

		/* add new node under the found one  */
		nd = tnode_ist(&data,tp->wid);
		tree_ins(tp, wlk, nd,0);
		/* tree_dmp(tp, strp_dmp); */
		da_rls(flattened);
	}

	/* cleanup  */
	tree_strip(tp,strp_rls);
	tree_rls(tp);
	
	return 1;
}

int tree_test(int num,int len)
{
	int ret = 
		utree_test(10,len);

	ret &= stree_test(100,50,10);

	//ret &= tree_test_data(100,50,10);

	return ret;
}

