/*(* disabled for doxygen because it's not opretational) @file
 * 
	compact data structures

	This file implements a way to compact non-cyclycal data structures into 
	a contigues byte stream and unpack them again into their original state
 */

#include <ego.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* data description objects 
	don't be tempted to use io_t for this because you need random access, jumping 
	back and forth through the buffer, so buf_t is the ideal base for this.
*/

static da_t *typelist;

static type_t *charp_type;
static type_t *short_type;
static type_t *short4_type;
static type_t *basetag_type;

#define TEST_BASE(TP,VAR,FMT)	\
	printf("\n------ %s  ---> ", #TP);	\
	i= instance_ist(#TP); \
	instance_pack(i, (void *)&VAR, 1);	\
	buf_dmp(i->bufp); 	\
	instance_dump(i);	\
	VAR = 0; \
	instance_unpack(i, (void *)&VAR);	\
	printf(FMT, VAR);	\
	instance_rls(i);

#define TEST_ARR(TP,VAR,FMT,COUNT)	\
	printf("\n------ %s  ---> ", #TP);	\
	i= instance_ist(#TP); \
	instance_pack(i, (void *)&VAR, COUNT);	\
	buf_dmp(i->bufp); 	\
	instance_dump(i);	\
	for (xx=0; xx< COUNT; xx++) VAR[xx] = 0; \
	instance_unpack(i, (void *)&VAR);	\
	for (xx=0; xx< COUNT; xx++) printf(FMT,VAR[xx]); \
	instance_rls(i);

#define TEST_PTR(TP,VAR,FMT,COUNT,VAR2)	\
	printf("\n------ %s  ---> ", #TP);	\
	i= instance_ist(#TP); \
	instance_pack(i, (void *)&VAR, COUNT);	\
	buf_dmp(i->bufp); 	\
	instance_dump(i);	\
	instance_unpack(i, (void *)&VAR2);	\
	for (xx=0; xx< COUNT; xx++) printf(FMT,VAR2[xx]); \
	instance_rls(i);	free(VAR2); 


/* count parameter : */
#define UNALLOC -2	// pointer that was definately NOT allocated
#define POINTER -1	// pointer that might be allocated
#define BASE     0

#define NTYPES (typelist ? da_len(typelist) : 0)

// no unions yet, because there's no easy way to now what member to use
// pointers rely on a dirty length check. (prefix 4 bytes)

/* all types should be registered once then use the index in the
 type(def) array as type identifier, type examples : 
	szof < 1 means try to calculate it
	count == -1 means "pointer to"
	count == 0 means it's a base type, rest os array

	idx	name	szof	count	subarray
	0	int		4		1		-
	1	char 	1		1		-
	2	char *	4		0		-
	3	int[10] 40		10		-
	4	member	8		1		{ {"name",2},{"type",0} }
	5 .. etc

 members are always named entries in the type table.
 You may have forward declarations, just make sure the number is
 correct *(or use nametype functions). Example :
	
	idx	name	szof	count	subarray
	.....
	4	first 	-1		1		{ {"name",2},{"forward",7} }
	5	first*	-1		0		{ {NULL,4} }
	6	second	-1		1		{ {"name",2},{"backward",5} }
	7	second*	-1		0		{ {NULL,6} }
	....

	this makes two struct that have a pointer to eachother.

	ASCII-ART:

	base type : we know the exact format, no help fields
    base types ALWAYS need storage, like they do on the stackframe as well

	short : +----+  char : +--+ etc.
			|abcd|         |32|
			+----+         +--+

	array type : we also know the exact type, and need no padding :
	
	short[3] : +----+----+----+	: char[1] : +--+  (so char == char[1])
               |aaaa|bbbb|cccc|             |32|
	           +----+----+----+             +--+

	pointer type : we need to specify the length (4  bytes ?!)

	short *  : +----+----+----+----+	: char* : +--+  (so char == char[1])
               |        2|aaaa|bbbb|           | h|
	           +----+----+----+----+           +--+

	struct type : again you know the exect format, add padding :

	struct { int a, short b } :  +--------+----+----+
                                 |aabbccdd|eeff|----|
	                             +--------+----+----+

	union type, we need a specifier that says which union member is used, i 	
		use the offset in the metadata, so a short is MORE than enough.

	union { int a, short b } :  +----+----+----+
	                            |   1|eeff|----|
	                            +----+----+----+

	Potential pitfalls :
	-----------------------
	1) unions obviously, should be handled in the pack/unpack functions
	2) bitfields : group them into a single integer
	3) byte-order: this could be handled though i don't know what happens to bitfields
	4) pointers into a block: too much knowledge is needed, do them in custom pack functions
	5) 

	loop detection :
	-----------------------
	pointers are normally associated with allocated data, but naturally they 
	can point to ANYTHING!. Loop detection makes sure data structures are not
	traversed again and again, but also not allocated double. 
	And what about a 'traversal pointer' that points anywhere within an already
	allocated block ? All of this could be detected with a pointer table with the real 
	pointer, the offset within the block and the length.

*/

void instance_dmp(instance_t *i, int clr);


#ifdef USED
void align(instance_t *i, char **ptr)
{
	unsigned int test = (unsigned int)*ptr % i->alignment;

	if (test) *ptr += (i->alignment)-test;
}
#endif

intptr_t alignsize(intptr_t a, intptr_t b)
{
	if (a < 1) return 0;
	return (((a-1)/b)+1) * b;
}

type_t *type_get(int pos)
{
	type_t **ret;
	if (pos < 0 || pos > NTYPES) return NULL;
	ret =  da_get(typelist, pos);
	return *ret;
}

type_t *struct_type_get(type_t *tp, intptr_t mem_pos)
{
	int pos;

	if (!tp->struct_mem) return NULL;

	pos = tp->struct_mem[mem_pos].type;
	return type_get(pos);
}

void instance_stretch(instance_t *i, intptr_t offset, intptr_t bytes)
{
	buf_stretch(i->bufp, offset, bytes);
	i->freestore += bytes;
}

/* if your struct does nothing nifty use this function */
intptr_t gen_traverse_struct_d(instance_t * i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc)
{
	intptr_t t;
	type_t *member_type;

	for (t=0; t< tp->count; t++) {
		member_type = struct_type_get(tp, t);
		member_type->wlk_d(i,member_type,pos,data,1,fnc);
		STRUCT_SHIFT(i,pos,data,member_type)
	}
	return 0;
}

int gen_traverse_ptr_d(instance_t * i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc)
{
	type_t *base_type;
	char *deref;

	buf_set_int(i->bufp, pos ,sizeof(REF_TYPE), i->freestore);
instance_dmp(i,LOG_ALOT);
	deref = *(char **)data;
	data = deref;
	if (data == NULL) {
		printf("NULL POINTER HANDLING ??\n");
		buf_set_int(i->bufp,pos,sizeof(REF_TYPE), -1);
		return 0;
	}
	pos = i->freestore;

	instance_stretch(i, pos, sizeof(LEN_TYPE));

	buf_set_int(i->bufp,pos,sizeof(LEN_TYPE), count);
	
	pos += sizeof(LEN_TYPE);
instance_dmp(i, LOG_ALOT);

	base_type = type_get(tp->basetype);
	instance_stretch(i, pos, count*base_type->szof);
	gen_traverse_arr_d(i,base_type,pos,data,count,fnc);
	return 0;
}

int gen_traverse_ptr_b(instance_t * i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc)
{
	intptr_t t;
	type_t *base_type;
	REF_TYPE **ptr = (REF_TYPE **)buf_get(i->bufp, pos);
	LEN_TYPE len;

	if (*ptr == (void *)-1) {
		lprintf(LOG_ALOT, "NULL POINTER ENCOUNTERED\n");
		return 0;
	}
	base_type = type_get(tp->basetype);
	
	pos = *(intptr_t *)ptr;
	/* len = buf_get_int(i->bufp, (int *)*ptr); */
	len = buf_get_int(i->bufp, pos);

	count = len;
	pos += sizeof(LEN_TYPE);

	if (data) { // ALLOCATES !! 
		if (count && base_type->szof) 
			*(char **)data = calloc(count , base_type->szof);
		// AND DEREF
		printf("alloc part %p (%p)\n", data, *(char **)data);
		data = *(char **)data;
	}
	for (t=0; t< count; t++) {
		type_t *base_type = type_get(tp->basetype);
		base_type->wlk_b(i,base_type,pos,data,1,fnc);
		ARR_SHIFT(pos,data,base_type)
	}

	return 0;
}

int gen_traverse_arr_d(instance_t * i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc)
{
	int t;

	/* STRETCH_BUFFER(i,buffer,count*base_type->szof); */
	for (t=0; t< count; t++) {
		
		tp->wlk_d(i,tp,pos,data,1,fnc);
		ARR_SHIFT(pos,data,tp)
instance_dmp(i, LOG_ALOT);
	}
	return 0;
}

/* convenienece routines : */
/* c string - always a pointer */
intptr_t cstr_traverse_d (instance_t * i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc)
{
	size_t len = 0;
	char *str = *(char **)data;

	if (str) len = strlen(str);
	len += 1; // add the null byte, so we don;t need a cstr_traverse_b

	return gen_traverse_d(i,tp,pos,data,len,fnc);
}

/* unallocated pointers, normally they point to or within allocated data. 
	szof is still sizeof(REF_TYPE) but the length = 0!!.
	If the range in which they point cannot be found they will be set to NULL!!!, 
	so it is advisable to order structure members more wisely, for instance :

struct a_tag {
	char *currentpos;
	char *actualdata;
} a;

	will end up in a NULL pointer for current because actualdata is not in the pointer table yet
	simply reversing will sort out this problem :"

struct a_tag {
	char *actualdata;
	char *currentpos;
} a;

*/

/* genericly walk through the tree led by the data */
intptr_t gen_traverse_d(instance_t * i, type_t *tp, intptr_t pos, char *data, intptr_t count, gfunc_t fnc)
{
	type_t *base_type;

	printf("WALKING: \"%s\" size %d count %d\n", tp->name, (int)tp->szof, (int)count);

	if (tp->count == BASE) {
		/* instance_stretch(i, pos, tp->szof); */
		fnc(i,tp,pos,data,tp->szof);
		ARR_SHIFT(pos,data,tp)
		return tp->szof;
	}
	if (tp->struct_mem) {
		instance_stretch(i, pos, tp->szof);
		tp->wlk_d(i,tp,pos,data,1,fnc);
		ARR_SHIFT(pos,data,tp)
instance_dmp(i, LOG_ALOT);
		return 0;
	}

	if (tp->count == POINTER) { // default pointer to 1 (one) member
		gen_traverse_ptr_d(i,tp,pos,data,count,fnc);
		return 0;
	} 

	count = tp->count;

	base_type = type_get(tp->basetype);
	gen_traverse_arr_d(i,base_type,pos,data,count,fnc);

	return 0;
}

/* example struct : data driven function */
int basetag_traverse_d(instance_t * i, type_t *tp, intptr_t pos, char *data, intptr_t dummy, gfunc_t fnc)
{
	size_t len = 0;
	int structcounter= 0;
	type_t *base_type, *member_type;

	/* it is always handy to cast back to your struct */
	struct BASE_tag *bt = (struct BASE_tag *)data;

	/* pointers are the main reason this cannot be done totally generic */
	/* because we do not know the length you allocated */
	/* (char *) bt->name ,  so we need the length */

	if (bt->name) len = strlen(bt->name)+1;
	member_type = struct_type_get(tp, structcounter++);
	gen_traverse_ptr_d(i,member_type,pos,(char*)data,len,fnc);

	STRUCT_SHIFT(i,pos,data,member_type)

	len = 3;
	member_type = struct_type_get(tp, structcounter++);
	base_type = type_get(member_type->basetype);
	gen_traverse_arr_d(i,base_type,pos,(char*)data,len,fnc);
	/* ALWAYS use STRUCT_SHIFT, because it ALIGNS */
	STRUCT_SHIFT(i,pos,data,member_type)

	/* BASE_tag * bt->next , a pointer, so gen can do that */
	len = 1;
	base_type = struct_type_get(tp, structcounter++);
	gen_traverse_ptr_d(i,base_type,pos,(char*)data,len,fnc);
	STRUCT_SHIFT(i,pos,data,base_type)

	return 0;
}

/* genericly walk through the tree led by the buffer */
intptr_t gen_traverse_b(instance_t * i, type_t *tp, intptr_t pos, char *data, intptr_t dummy, gfunc_t fnc)
{
	int t;
	LEN_TYPE count;
	type_t *member_type;

	if (tp->count == BASE) {
		fnc(i,tp,pos,data,dummy);
		ARR_SHIFT(pos,data,tp);
		return tp->szof;
	}
	if (tp->struct_mem) {
		for (t=0; t< tp->count; t++) {
			member_type = struct_type_get(tp, t);
			member_type->wlk_b(i,member_type,pos,data,1,fnc);
			STRUCT_SHIFT(i,pos,data,member_type)
		}
		ARR_SHIFT(pos,data,tp)
		return 0;
	}

	count = tp->count;
	if (tp->count == POINTER) {
#ifndef APART
		gen_traverse_ptr_b(i,tp,pos,data,count,fnc);
// delete this else part if everything tetst OK !!!
#else
		REF_TYPE **ptr = (REF_TYPE **)buf_get(i->bufp, pos);
		LEN_TYPE len;
		/* printf("dat is at : %d from %d\n", *ptr, pos); */
		if (*ptr == (void *)-1) {
			lprintf(LOG_ALOT, "NULL POINTER ENCOUNTERED\n");
			return;
		}
		type_t *base_type = type_get(tp->basetype);
		
		// subtract len from buffer at pos (*ptr)
		pos = *(int *)ptr;
		len = buf_get_int(i->bufp, pos);

		count = len;
		pos += sizeof(LEN_TYPE);

		if (data) { // ALLOCATES !! 
			if (count && base_type->szof) 
				*(char **)data = calloc(count , base_type->szof);
			// AND DEREF
			printf("alloc part %p (%p)\n", data, *(char **)data);
			data = *(char **)data;
		}
	}
	for (t=0; t< count; t++) {
		type_t *base_type = type_get(tp->basetype);
		base_type->wlk_b(i,base_type,pos,data,dummy,fnc);
		ARR_SHIFT(pos,data,base_type)
#endif
	}
	/* return patient-oldptr; */
	return 0;
}

int type_cmp(const void *a,const  void *b, void *c)
{
	type_t *A = *(type_t **)a;
	type_t *B = *(type_t **)b;

	if (!A->name && !B->name) return 0;
	if (!A->name) return -1;
	if (!B->name) return  1;
	/* printf("testing %s %s \n", A->name, B->name); */
	return strcmp(A->name,B->name);
}

int ptr_cmp(const void *a, const void *b, void *c)
{
	const ptr_t *A = (ptr_t *)a;
	const ptr_t *B = (ptr_t *)b;

	lprintf(LOG_ALOT, "%p\n", A->ptr);
	lprintf(LOG_ALOT, "%p\n", B->ptr);
	if (A->ptr < B->ptr) return -1;
	if (A->ptr > B->ptr) return  1;
	lprintf(LOG_ALOT, "DOUBLE POINTER ENTRY !!!!\n");
	return 0;
}

void wlk_rls(instance_t *i, type_t *tp)
{
	/* printf("[%p][%p]\n", ptr,tp); */
	/* tp->dmp(*ptr,tp); */
}

int offset_cmp(const void *a, const void *b, void *c)
{
	const ptr_t *A = (ptr_t *)a;
	const ptr_t *B = (ptr_t *)b;

	if (A->ptr < B->ptr) return -1;
	if (A->ptr > B->ptr) return  1;
	lprintf(LOG_ALOT, "DOUBLE POINTER ENTRY !!!!\n");
	return 0;
}

// loop detection
void add_pointer(instance_t *i, int offset, void *ptr)
{
	ptr_t p;

	p.ptr = ptr;
	p.offset = offset;

	ida_add(i->ptable, (void *)&p);
	/* ida_dmp(pointers); */
}

ptr_t *find_pointer(instance_t *ip, int offset, void *ptr)
{
	ptr_t *p;
	int i;

	if ( (offset < 0 && ptr == NULL) || ip->ptable == NULL) return NULL;

	// search on index 0 (ptrs) or 1 (offsets)
	i = offset < 0 ? 0 : 1;

	p = ida_get(ip->ptable, i);

	return p;
}

int wlk_unpack(instance_t *i, type_t *tp, char **patient, char *donor)
{
	memcpy(patient, donor, tp->szof);
	return 0;
}

/* BEWARE !! : if the buffer gets a new pointer also reset the wlk 
	pointer (done with offset_from) */
type_t *type_ist(char *name, int szof, int count, char *type, wbfunc_t wlk_b, wdfunc_t wlk_d)
{
	type_t *tp = calloc(1,sizeof(type_t));

	if (!tp) return NULL;

	tp->name = strdup(name);
	tp->szof = szof;
	tp->count = count;
	if (type) tp->basename = strdup(type);

	tp->wlk_d = wlk_d;
	tp->wlk_b = wlk_b;

	return tp;
}

void gen_dmp(instance_t *i, char *wlk, type_t *tp)
{

	if (tp->struct_mem) {
		printf("<Structure>");
	} else if (tp->count == BASE) {
		printf("base : ");
	} else if (tp->count == POINTER) {
		printf("<Pointer>");
	} else {
		printf("<Array>");
	}

	/* wlk += tp->szof; */
}

int type_dmp(void)
{
	int t, s;

	for (t=0; t< NTYPES; t++) {
		type_t *tp = type_get(t);
		printf("[%d] \"%s\"", t, tp->name);
		printf("(%d/%d)", (int)tp->szof, (int)tp->count);
		printf("\n");
		if (tp->struct_mem) {
			for (s=0; s < tp->count; s++) {
				printf("	%d", s);
				printf("\"%s\"", tp->struct_mem[s].name);
				printf(" (%s)", tp->struct_mem[s].tagname);
				printf("%d\n", tp->struct_mem[s].type);
			}
		}
	}
	return 0;
}

void instance_dmp(instance_t *i, int level)
{
	lprintf(level, "%x -> fs:%d, bts:%d\n", i->bufp, i->freestore, i->bsize);
	buf_dmp(i->bufp);
}


type_t *type_find(char *name)
{
	int t;
	type_t testtype, *tptr;

	if (!typelist) return NULL;

	testtype.name = name;
	tptr = &testtype;

	t = da_pos(typelist, &tptr, type_cmp, NULL);
	return type_get(t);
}

int type_reg(type_t *tp)
{
//	int t;

	if ( type_find(tp->name)) {
		lprintf(LOG_ALOT, "re-registering type %s\n", tp->name);
		return -1;
	}
		
	if (tp->wlk_d == NULL) tp->wlk_d = gen_traverse_d;
	if (tp->wlk_b == NULL) tp->wlk_b = gen_traverse_b;
	if (!typelist) {
		/* typelist = ArrIst(ARR_WID, sizeof(type_t *), ARR_CMP, type_cmp, NULL, ARR_END); */
		typelist = da_ist(sizeof(type_t *), 0);
	}
	/* ArrAdd(typelist, &tp); */
	da_add(typelist, &tp);
	return 0;
}

type_t *type_reg_arr(char *name, char *base, int count, wbfunc_t wlk_b, wdfunc_t wlk_d)
{
	type_t *tp;

	tp = type_ist(name, -1, count, base, wlk_b, wlk_d);
	if (tp) type_reg(tp);

	return tp;
}

type_t *type_reg_ptr(char *name, char *base, wbfunc_t wlk_b, wdfunc_t wlk_d)
{
	type_t *tp;

	tp = type_ist(name, -1, POINTER, base, wlk_b, wlk_d);

	if (tp) type_reg(tp);

	return tp;
}

/* install and register base types */
type_t *type_reg_base(char *name, int szof, wbfunc_t wlk_b, wdfunc_t wlk_d)
{
	type_t *tp;

	tp = type_ist(name, szof, BASE, NULL, wlk_b, wlk_d);
	if (tp) type_reg(tp);

	return tp;
}


int type_pos(char *name)
{
	int t;
	type_t testtype, *tptr;

	if (!typelist) return -1;

	testtype.name = name;
	tptr = &testtype;

	t = da_pos(typelist, &tptr, type_cmp, NULL);
	return t;
}

/* use this function only  after complete arrays/structs/types
	since it aligns !!!! */
int instance_reindex(instance_t * i)
{
	return 0;
}


/* wlk_b function (buffer steered) */
intptr_t dump(instance_t *i, type_t *tp, intptr_t pos, char *data, intptr_t count)
{
	int t;
	intptr_t sz = tp->szof;
	char *buffer = buf_get(i->bufp, pos);

	lprintf(LOG_ALOT, "DUMP : %d of %d bytes :", sz, tp->szof);

	for (t=0; t< sz; t++) {
		printf(" %x", buffer[t]);
	}
	printf("\n");
	return 0;
}

/* wlk_b function (buffer steered) */
intptr_t unpack(instance_t *i, type_t *tp, intptr_t pos, char *data,intptr_t count)
{
	count *= tp->szof;

	/* lprintf(LOG_ALOT, "UNPACK : %d bytes from %d to %p\n", tp->szof, pos, data); */
	buf_get_str(data,i->bufp, pos, count);
	/* memcpy(data, buffer, tp->szof); */
	return 0;
}

/* wlk_d function (data steered) */
intptr_t pack(instance_t *i, type_t *tp, intptr_t pos, char *data,intptr_t count)
{
	/* lprintf(LOG_ALOT, "PACK : %d (%d)bytes from %p to %d\n", count, tp->szof, data, pos); */
	/* STRETCH_BUFFER(i,buffer,count); */
	printf("%p\n", i->bufp);
	printf("%p\n", data);
	buf_set_str(i->bufp, pos, count, data);

	return 0;
}

intptr_t instance_pos(instance_t *i)
{
	return buf_pos(i->bufp);
}

int instance_pack(instance_t * i, char *donor, int count)
{
	intptr_t pos = instance_pos(i);
//	LEN_TYPE *temp;
	type_t *tp = type_get(i->type);

	// set aside storage for this object itself */
	/* gen_traverse_d(i,tp,pos,donor,count,pack); */
	instance_stretch(i, pos, tp->szof);
	tp->wlk_d(i,tp,pos,donor,count,pack);

	return 0;
}

int instance_unpack(instance_t * i, char *patient)
{
	type_t *tp = type_get(i->type);

	// patient should have room for data !!*/
	gen_traverse_b(i,tp, 0, patient, 1, unpack);
	return 0;
}

instance_t * instance_ist(char *mytype)
{
	idx_t *ptr_idx, *pos_idx;
	instance_t * i = (instance_t *)calloc(1,sizeof(instance_t));
	i->type = type_pos(mytype);
	if (i->type < 0) {
		lprintf(LOG_ALOT, "type_t \"%s\" is not registered, please check the exact spelling !!\n", mytype);
		free(i);
		return NULL;
	}
	
	i->bufp = buf_ist(0);
	
	printf("new buffer %p\n", i->bufp);
	i->bsize = JUMP_SIZE;

	// loop detection needs a pointer table PER INSTANCE !
	i->ptable = ida_ist(sizeof(REF_TYPE), 0);	
	ptr_idx = idx_ist(0, offset_cmp, NULL);
	ida_add_index(i->ptable, ptr_idx);
	pos_idx = idx_ist(0, ptr_cmp, NULL);
	ida_add_index(i->ptable, pos_idx);

	return i;
}

intptr_t type_resolve_size(type_t *tp)
{
	int s;
	intptr_t size=0;

	// resolved already ?
	if (tp->szof > 0) return tp->szof;

	if (tp->struct_mem) {
		for (s=0; s < tp->count; s++) {
			type_t *sub_type;
			int type = type_pos(tp->struct_mem[s].name);
			if (type < 0) {
				lprintf(LOG_ALOT, "UNRESOLVED REFERENCE (%s)\n", tp->struct_mem[s].name);
				exit(0);
			}
			tp->struct_mem[s].type = type;
			sub_type = type_get(type);
			
			size += type_resolve_size(sub_type);
		}
	} else {
		if (tp->count == POINTER) {
			size =  PTR_SIZE;
		} else if (tp->count == BASE) {
			if (tp->szof < 1) {
				lprintf(LOG_ALOT, "BASE type (%s) without sizeof !!\n", tp->name);
	
				exit(0);
			}
			size = tp->szof;
		} else {
			type_t *base_type = type_get(tp->basetype);
			size = type_resolve_size(base_type);
			size *= tp->count;
		}
	}
	return size;
}

/* only call this after registering all types , so we can resolve all 
	type references , either forward or backward */
int type_calc(intptr_t alignment)
{
	int t, s;

	for (t=0; t< NTYPES; t++) {
		type_t *tp = type_get(t);
		intptr_t struct_size = 0;

		// if this is a struct traverse its submembers
		if (tp->struct_mem) {
			for (s=0; s < tp->count; s++) {
				type_t *sub_type;
				int type = type_pos(tp->struct_mem[s].name);
				if (type < 0) {
					lprintf(LOG_ALOT, "UNRESOLVED REFERENCE (%s)\n", tp->struct_mem[s].name);
					exit(0);
				}
				tp->struct_mem[s].type = type;
				sub_type = type_get(type);
				
				struct_size += type_resolve_size(sub_type);
				printf("	%d \"%s\"\n", s, tp->struct_mem[s].name);
			}
			struct_size = alignsize(struct_size, alignment);
			printf("Struct size is %d\n", (int)struct_size);
		} else { 
			// if this is a ptr or array fill in the basetype */
			if (tp->basename) {
				tp->basetype = type_pos(tp->basename);
				if (tp->basetype < 0) {
					lprintf(LOG_ALOT, "UNRESOLVED REFERENCE (%s)\n", tp->basename);
					exit(0);
				} 
			} else { // mark this as a base type
				tp->basetype = -1;
			}
			if (tp->count == POINTER)  {
				printf("POINTER TO\n");
				struct_size += PTR_SIZE;
			} else if (tp->count == BASE)  {
				printf("BASE type_t\n");
				struct_size = alignsize(struct_size, alignment);
			} else {
				printf("ARRAY OF\n");
				struct_size += type_resolve_size(tp);
			}
		}
		if (tp->szof < 1) 
			tp->szof = struct_size;
	}

	return 0;
}

void instance_rls(instance_t * i)
{
	lprintf(LOG_ALOT, "AND FREEING %p\n", i->bufp);
	buf_rls(i->bufp);
	ida_rls(i->ptable);
	free(i);
}

void instance_dump(instance_t * i)
{
	type_t *tp = type_get(i->type);

	gen_traverse_b(i,tp, 0, NULL, 1, dump);
}

// no need for speed, this is before startup
int type_add_member(type_t *tp, member_t memb)
{
	tp->struct_mem = (member_t *) realloc(tp->struct_mem, 
				(tp->count+1)*sizeof(member_t));
	tp->struct_mem[tp->count] = memb;
	tp->count++;

	return 0;
}

int type_add(type_t *tp, char *tagname, char *name)
{
	member_t mem;

	mem.tagname  = strdup(tagname);
	mem.name     = strdup(name);
	mem.type     = -1;
	return type_add_member(tp,mem);
}


void member_rls(member_t m)
{
	if (m.name) free(m.name);
	if (m.tagname) free(m.tagname);
}

void type_rls(type_t *tp)
{
	int t;
	if (!tp) return;

	if (tp->name) free(tp->name);
	if (tp->basename) free(tp->basename);
	if (tp->struct_mem)  {
		for (t=0; t< tp->count; t++) { 
			member_rls(tp->struct_mem[t]);
		}
		free(tp->struct_mem);
	}

	free(tp);
}

void type_unreg(type_t *tp)
{
	int pos;

	if (!typelist) return;

	/* pos = ArrPos(typelist, &tp, ARR_END); */
	pos = da_pos(typelist, &tp, type_cmp, NULL);
	if (pos >= 0) 
		/* ArrDel(typelist, pos); */
		da_del(typelist, pos,1);

	type_rls(tp);

	if (da_len(typelist) == 0) {
		da_rls(typelist);
		typelist = NULL;
	}
}

/*! @brief test struct */
typedef struct TEST_TAG {
	char* othersubname;	/*!< dunno */
	char* othername;	/*!< dunno */
	int othertype;		/*!< dunno */
} TEST;

void cleanup()
{
//	int t;

again:
	while (typelist && da_len(typelist)) {
		/* type_t **tp = ArrGet(typelist, 0, ARR_END); */
		type_t **tp = da_get(typelist, 0);
		type_unreg(*tp);
		goto again;
	}
	/* MemExit(0); */
}

/* assumes the length has laredy been written in the buffer */
LEN_TYPE get_count(char *ptr)
{
	LEN_TYPE *temp;
	if (!ptr)return 0;

	temp = (LEN_TYPE *)ptr;
	temp--;
	return *temp;
}

// use the helper variable
LEN_TYPE std_cnt(char *ptr, instance_t *i, type_t *t)
{
	return i->len;
}

LEN_TYPE str_cnt(char *ptr, instance_t *i, type_t *t)
{
	if (!ptr)return 0;
	return strlen(ptr)+1;
}

// call this method if you want to use basic types 
// it can also initialize the ego data types  (complete = 1)
int data_init(int complete)
{
//	type_t *tp;

	// i presume these tend to get used ;)
	short_type = type_reg_base("short", sizeof(short), gen_traverse_b, gen_traverse_d);
	type_reg_base("float", sizeof(float), gen_traverse_b, gen_traverse_d);
	type_reg_base("double", sizeof(double), gen_traverse_b, gen_traverse_d);
	type_reg_base("int", sizeof(int), gen_traverse_b, gen_traverse_d);
	type_reg_base("char", sizeof(char), gen_traverse_b, gen_traverse_d);

	/* register some real pointers */
	charp_type = type_reg_ptr("char *", "char",gen_traverse_b, gen_traverse_d);
	type_reg_ptr("short *", "short", gen_traverse_b, gen_traverse_d);
	type_reg_ptr("int *", "int", gen_traverse_b, gen_traverse_d);
	type_reg_ptr("float *", "float", gen_traverse_b, gen_traverse_d);
	type_reg_ptr("double *", "double", gen_traverse_b, gen_traverse_d);

	// convenience types, you can use these to avoid making a custom traversal function!!
	// char * is not necesarily a null terminated string !!
	charp_type = type_reg_ptr("cstring", "char", gen_traverse_b, cstr_traverse_d);
	// other possibilities not implemented yet:
	// unionchooser : { int which; union members {} }
	// lenprefix : { int len; anytype * array; }

	if (!complete) return 0;

#ifdef USE_DATABLOCK
	buf_data();
	da_data();
#endif

	type_calc(ALIGNMENT);
	return 0;
}

int data_test_loop(int wid, int len)
{
	return 0;
}

int data_test_default(int wid, int len)
{
	instance_t * i;
	type_t *type;
	/* char *strptr; */

#ifdef UNUSED_WARNING
	/* base types test  */
	char c = 'a';
	short s = 5678;
	int t = 12345678;
	double d = 1.0/3.0;
	float f = 1.0/3.0;

	/* array types test  */
	char ca[3] = { 'a', 'b', 'c' };
#endif
	short sa[3] = { 1111,2222,3333 };
	/* int ta[3] = { 0x11111111,0x22222222,0x33333333}; */
	float fa[3] = { (float)1.0/(float)3.0, (float)1.0/(float)4.0, (float)1.0/(float)5.0} ;
	/* double da[3] = { 1.0/3.0, 1.0/4.0, 1.0/5.0} ; */

	/* pointer types */
	/* char *cp = strdup("lihskjhwekwke"); char *cp2; */
	short *sp = sa; short *sp2;
#ifdef UNUSED_WARNING
	int *tp = ta; int *tp2;
	float *fp = fa; float *fp2;
	double *dp = da; double *dp2;

	/* structs , for alignment tests */
	struct altest_tag at1;
	struct altest_tag at = { {0x3, 0x33, 0x333
#if GRANULE == 4
		, 0x3333
#endif
	} };

	struct BASE_tag third = { "third", {0x3, 0x33, 0x333
#if GRANULE == 4
		, 0x3333
#endif
		}, NULL};
	struct BASE_tag second = { "second", {0x2, 0x22, 0x222
#if GRANULE == 4
		, 0x2222
#endif
	}, &third};
	struct BASE_tag first = { "first", {0x01, 0x11, 0x111
#if GRANULE == 4
		, 0x1111
#endif
	}, &second};
#endif

	int xx;

	printf("%f\n", fa[0]);
	printf("%f\n", fa[1]);
	printf("%f\n", fa[2]);

	/* int *bleerk = 0x2222; */
	/* char *bleerk = "jazekers"; */
	/* short bleerk[55] = { 0 }; */

	/* register some arrays */
	type_reg_arr("char[3]", "char", 3, gen_traverse_b, gen_traverse_d);
	type_reg_arr("short[3]", "short", 3, gen_traverse_b, gen_traverse_d);
	type_reg_arr("int[3]", "int", 3, gen_traverse_b, gen_traverse_d);
	type_reg_arr("float[3]", "float", 3, gen_traverse_b, gen_traverse_d);
	type_reg_arr("double[3]", "double", 3, gen_traverse_b, gen_traverse_d);

	/* register structs */
	type = type_ist("altest_tag", CALC_SIZE, 0, NULL, gen_traverse_b, gen_traverse_struct_d);
	type_add(type, "num", "short[X]");
	type_add(type, "ha",  "int");
	type_reg(type);

	type = type_ist("BASE_tag", CALC_SIZE, 0, NULL, gen_traverse_b, gen_traverse_struct_d);
	type_add(type, "name", "cstring");
	type_add(type, "num", "short[X]");
	type_add(type, "next", "BASE_tag *");
	type_reg(type);

	short4_type = type_reg_arr("short[X]", "short", GRANULE, gen_traverse_b, gen_traverse_d);
	/* type_reg_arr("short[55]", "short", GRANULE, gen_traverse_b, gen_traverse_d); */
	type_dmp();


// data after pack(first) should be:
/* 
*/

	/* test struct  */
	/* and pointer to the struct  */
	basetag_type = type_reg_ptr("BASE_tag *", "BASE_tag",gen_traverse_b, gen_traverse_d);
	type_calc(ALIGNMENT);
	type_dmp();

	/* TEST_BASE(char,c,"( %c )") */
	/* TEST_BASE(short,s,"( %d )") */
	/* TEST_BASE(int,t,"( %d )") */
	/* TEST_BASE(float,f,"( %f )") */
	/* TEST_BASE(double,d,"( %f )") */

	/* TEST_ARR(char[3],ca,"[%c]",3) */
	/* TEST_ARR(short[3],sa,"[%d]",3) */
	/* TEST_ARR(int[3],ta,"[%x]",3) */
	/* TEST_ARR(float[3],fa,"[%f]",3) */
	/* TEST_ARR(double[3],da,"[%f]",3) */

	/* TEST_PTR(cstring,cp,"%c",1,cp2) */
	TEST_PTR(short *,sp,"%d ",3,sp2)
	/* TEST_PTR(int *,tp,"[%x] ",3,tp2) */
	/* TEST_PTR(float *,fp,"%f ",3,fp2) */
	/* TEST_PTR(double *,dp,"%f ",3,dp2) */

exit(0);

#ifdef TESTING_STRUCT
	printf("-- testing struct of size %d\n", sizeof(at));
	i= instance_ist("altest_tag");
	instance_pack(i, (void *)&at,1);
	instance_dump(i);
	instance_unpack(i, (void *)&at1);
	instance_rls(i);

	printf("recvd: %x %x %x\n", at1.num[0], at1.num[1], at1.num[2]);
#endif

#ifdef TESTING
	// note that we need the null termination !!
	i= instance_ist("BASE_tag");
	instance_pack(i, (void *)&first,1);
	/* buf_dmp(i->bufp); */
	/* instance_dump(i); */
	instance_unpack(i, (void *)&receiver);

	printf("unpacked : %s \n", receiver.name);
	printf("unpacked : %s \n", receiver.next->name);
	printf("unpacked : %s \n", receiver.next->next->name);

	printf("FREEING!!\n");
	// since receiver was static it should not be freed
	// its name member was allocated though !!!
	free(receiver.next->next->name);
	free(receiver.next->next);
	free(receiver.next->name);
	free(receiver.next);
	free(receiver.name);
	instance_rls(i);
#endif
	/* free(cp); */

	cleanup();
	return 1;
}

int data_test(int wid, int len)
{
	int ret;

	data_init(0);

	/* ret = data_test_loop(wid,len); */
	ret = data_test_default(wid,len);

	return ret;
}
