/** @file
 * 
	everything not belonging in other files and test routines for ego

 */

#include <ego.h>
#include <time.h>
#ifndef WIN32
#include <unistd.h>
#endif

#include <string.h>

/** @brief dump limit */
#define DMPLIMIT 100

/* -------------------------------------------------------------------- *\
|* non standard
\* -------------------------------------------------------------------- */
/** @brief is c numeric ? 
 * @param c character to test
 * @return 1 : c is numeric, o c is not 
 */
int is_numeric(char c)
{
    if (c >= '0' && c<='9') return 1;
    return 0;
}

/** @brief is c alphanumeric ? 
 * @param c character to test
 * @return 1 : c is alphanumeric, o c is not 
 */
int is_alpha(char c)
{
    if ( (c >= 'a' && c<='z') || (c>='A' && c<='Z') || c=='_') return 1;
    return 0;
}

/** @brief is c white space ? 
 * @param c character to test
 * @return 1 : c is white space, o c is not 
 */
int is_whitespace(char c)
{
    if ( c == ' ' || c== '\t' || c=='\n' || c=='\0') return 1;
    return 0;
}

/* -------------------------------------------------------------------- *\
|* standard dump function, dumps at most DMPLIMIT bytes in dec+char form
\* -------------------------------------------------------------------- */

/** @brief compare plain pointers
 
 * @param L left pointer to compare
 * @param R right pointer to compare
 * @param xtra unused
 * @return negative if L<R , 0 if L == R,  positive otherwise
 */
int std_cmp(const void *L, const void *R, void *xtra)
{
	return memcmp(L,R, sizeof(void *));
}

/** @brief compare string
 * also checks for NULL values, NULL comparing smaller then filled values
 * @param L left string to compare
 * @param R right string to compare
 * @param xtra unused
 * @return negative if L<R , 0 if L == R,  positive otherwise
 */
int str_cmp(const void *L, const void *R, void *xtra)
{
	char *left = (char *)L;
	char *right = (char *)R;

	if (!left && !right) return 0;
	/* null is smaller than any string  */
	if (!left) return -1;
	if (!right) return 1;

	//printf("%s vs %s\n", left, right);

	return strcmp(left, right);
}

/** @brief compare string pointer
 * strcmp version that first dereferences pointer to string 

 * also checks for NULL values, NULL comparing smaller then filled values
 * @param L left string to compare
 * @param R right string to compare
 * @param xtra unused
 * @return negative if L<R , 0 if L == R,  positive otherwise
 */
int strp_cmp(const void *L, const void *R, void *xtra)
{
	char *left = *(char **)L;
	char *right = *(char **)R;

	if (!left && !right) return 0;
	/* null is smaller than any string  */
	if (!left) return -1;
	if (!right) return 1;

	/* printf("%s < %s ?\n", left, right); */
	
	return strcmp(left, right);
}

/** @brief compare int inside a pointer
 * compare a pointer that was abused to contain an integer 

 * also checks for NULL values, NULL comparing smaller then filled values
 * @param left left int to compare
 * @param right right int to compare
 * @param xtra unused
 * @return negative if left<right , 0 if left == right,  positive otherwise
 */
int int_abuse_cmp(const void *left, const void *right, void *xtra)
{
	intptr_t l = (intptr_t)left;
	intptr_t r = (intptr_t)right;

	/* printf("%d %d (%p)\n", l, r, xtra); */

	if (l < r) return -1;
	if (l > r) return  1;
	return 0;
}

/** @brief compare float 
 * compare the content of two pointers to float

 * @param left left float to compare
 * @param right right float to compare
 * @param xtra unused
 * @return negative if left<right , 0 if left == right,  positive otherwise
 */
int float_cmp(const void *left, const void *right, void *xtra)
{
	float l = *(float *)left;
	float r = *(float *)right;

	/* printf(":%d %d (%p)\n", l, r, xtra); */

	/* consecutive values (for iv.c) */
	if (l == r-1) return -1;
	if (l < r) return -2;
	if (l == r+1) return 1;
	if (l > r) return  2;
	return 0;
}

/** @brief compare integer pointer 
 * compare the content of two pointers to int

 * @param left left int to compare
 * @param right right int to compare
 * @param xtra unused
 * @return negative if left<right , 0 if left == right,  positive otherwise
 */
int int_cmp(const void *left, const void *right, void *xtra)
{
	int l = *(int *)left;
	int r = *(int *)right;

	/* printf(":%d %d (%p)\n", l, r, xtra); */

	/* consecutive values (for iv.c) */
	if (l == r-1) return -1;
	if (l < r) return -2;
	if (l == r+1) return 1;
	if (l > r) return  2;
	return 0;
}

/** @brief compare integer pointer 
 * compare the content of two pointers to int

 * @param left left int32_t to compare
 * @param right right int32_t to compare
 * @param xtra unused
 * @return negative if left<right , 0 if left == right,  positive otherwise
 */
int int32_cmp(const void *left, const void *right, void *xtra)
{
	int32_t l = *(int32_t *)left;
	int32_t r = *(int32_t *)right;

	//printf(":%d %d (%p)\n", l, r, xtra);

	/* consecutive values (for iv.c) */
	if (l == r-1) return -1;
	if (l < r) return -2;
	if (l == r+1) return 1;
	if (l > r) return  2;
	return 0;
}

/** @brief compare integer pointer 
 * compare the content of two pointers to int

 * @param left left int64_t to compare
 * @param right right int64_t to compare
 * @param xtra unused
 * @return negative if left<right , 0 if left == right,  positive otherwise
 */
int int64_cmp(const void *left, const void *right, void *xtra)
{
	int64_t l = *(int64_t *)left;
	int64_t r = *(int64_t *)right;

	/* printf(":%d %d (%p)\n", l, r, xtra); */

	/* consecutive values (for iv.c) */
	if (l == r-1) return -1;
	if (l < r) return -2;
	if (l == r+1) return 1;
	if (l > r) return  2;
	return 0;
}

/** @brief compare integer pointer 
 * compare the content of two pointers to int

 * @param left left int to compare
 * @param right right int to compare
 * @param xtra unused
 * @return negative if left<right , 0 if left == right,  positive otherwise
 */
int intp_cmp(const void *left, const void *right, void *xtra)
{
	int l = *(int *)left;
	int r = *(int *)right;

	/* printf("%d %d (%p)\n", l, r, xtra); */

	if (l < r) return -1;
	if (l > r) return  1;
	return 0;
}

/** @brief compare test structure
  * test function  */
int test_cmp(const void *left, const void *right, void *xtra)
{
	test_t l = *(test_t *)left;
	test_t r = *(test_t *)right;

	if (l.i < r.i) return -1;
	if (l.i > r.i) return  1;
	return 0;
}

/** @brief dump int abused in pointer
 *  this used is when you cram an integer into a pointer 

 * @param wid width in bytes of the data
 * @param data content to dump
 * @param nop unused
 */
void int_abuse_dmp(intptr_t wid, const void *data, void *nop)
{
	intptr_t i = (intptr_t)data;

	printf("%d", (int)i);
}

/** @brief dump time_t
 *  dump readable form of time_t 

 * @param wid width in bytes of the data
 * @param a content to dump
 * @param b unused
 */
void time_t_dmp(intptr_t wid, const void *a, void *b)
{
	time_t w = (time_t)a;
	char buf[100];
	
	ctime_s(buf,100,&w);
	printf("%s", buf);
}

/** @brief dump 8 byte integer 
 *  dump content of pointer to 8 byte integer 

 * @param wid width in bytes of the data
 * @param a content to dump
 * @param b unused
 */
void int64_dmp(intptr_t wid, const void *a, void *b)
{
	int w = *(int *)b;
	int64_t i = *(int64_t *)a;
	if (w != 0)
	  printf("%-*lld", w, (long long)i);
	else
	  printf("%lld ", (long long)i);
}

/** @brief dump integer 
 *  dump content of pointer to integer 

 * @param wid width in bytes of the data
 * @param a content to dump
 * @param b unused
 */
void int_dmp(intptr_t wid, const void *a, void *b)
{
	intptr_t w = (intptr_t)b;
	intptr_t i = *(intptr_t *)a;
	if (w != 0)
	  printf("%-*d", (int)w, (int)i);
	else
	  printf("%d ", (int)i);
}

/** @brief dump float
 *  dump content of pointer to float

 * @param wid width in bytes of the data
 * @param a content to dump
 * @param b unused
 */
void float_dmp(intptr_t wid, const void *a, void *b)
{
	float f = *(float *)a;
	printf("%f ", f);
}

/** @brief dump char 
 * dump content of char pointer 

 * @param wid width in bytes of the data
 * @param a content to dump
 * @param b unused
 */
void char_dmp(intptr_t wid, const void *a, void *b)
{
	intptr_t w = (intptr_t)b;
	intptr_t i = *(intptr_t *)a;
	if (w != 0)
	  printf("%-*c", (int)w, (int)i);
	else
		printf("%c ", (int)i);
}

/** @brief dump char in pointer 
 *  dump content of char abused in a pointer 

 * @param wid width in bytes of the data
 * @param a content to dump
 * @param b unused
 */
void char_abuse_dmp(intptr_t wid, const void *a, void *b)
{
	intptr_t i = (intptr_t)a;
	if (wid != 0)
		printf("%-*c", (int)wid, (int)i);
	else
		printf("%c", (int)i);
}

/** @brief dump int 
 *  @brief dump content of int pointer 

 * @param wid width in bytes of the data
 * @param a content to dump
 * @param b unused
 */
void intp_dmp(intptr_t wid, const void *a, void *b)
{
	int i = *(int *)a;
	if (wid != 0)
		printf("%-*d", (int)wid, i);
	else
		printf("%d ", i);
}

/** @brief dump char pointer (string) 

 * @param bytes width in bytes of the data
 * @param a content to dump
 * @param b unused
 */
void str_dmp(intptr_t bytes, const void *a, void *b)
{
	char *str = (char *)a;

	if (!str) printf("null");
	printf("%s", str);
}

/** @brief dump char pointer pointer (pointer to string) 

 * @param bytes width in bytes of the data
 * @param a content to dump
 * @param b unused
 */
void strp_dmp(intptr_t bytes, const void *a, void *b)
{
	char *str = *(char **)a;

	if (!str) printf("null");
	printf("%s", str);
}

/** @brief release pointer to string
 *  release char pointer pointer (pointer to string) 
 * @param a pointer which content to release
 */
void strp_rls(void *a)
{
	char *str = *(char **)a;
	if (!str) return;

	/* printf("freeing %s\n", str); */
	free(str);
}

/** @brief release char pointer (string) 
 *  release char pointer (string) 
 * @param a pointer which content to release
 */
void str_rls(void *a)
{
	char *str = (char *)a;
	if (!str) return;

	/* printf("freeing %s\n", str); */
	free(str);
}

/** @brief free anything 
  * simply call free on the data element if ts is not NULL 
 * @param data pointer which content to release
 */
void anything_rls(void *data)
{
	if (data) free(data);
}

/** @brief dump test structure */
void test_dmp(int bytes, const void *a, const void *b)
{
	test_t c = *(test_t *)a;

	lprintf(LOG_ALOT, "%c,%d,%s,{%c,%c,%c,%c,%c}\n", 
		c.c, 
		c.i,
		c.str, 
		c.carr[0],c.carr[1],c.carr[2],c.carr[3],c.carr[4]);
}

/** @brief dump anything 
  up to DMPLIMIT that is 
 * @param wid width in bytes of the data
 * @param elm content to dump
 * @param usr unused
 */
void stddmpfnc(intptr_t wid, const void *elm, void *usr)
{
	long t;
	char *Elm = (char *)elm;
	long end = wid;
	if (end > DMPLIMIT) end = DMPLIMIT;

	for (t=0; t<end;t++) {
		printf("%3.d ", Elm[t]);
	}
	for (t=0; t<end;t++) {
		if (Elm[t] < 32)
			printf(".");
		else
			printf("%c", Elm[t]);
	}
	printf("\n");
}

/** @brief release ego 
  * release resources initialised by ego_init */
void ego_exit(void)
{
	err_exit();
	log_exit();
}

/** @brief initialse ego
 * init some basic resources used by the ego library, like error messages
 * io structs, logging etc  
 * @param loglevel teh default loglevel to use
 */
int ego_init(int loglevel)
{
	int ret;
	
	/* randist(); */

	ret = log_init(loglevel,NULL,NULL,NULL) |
	io_init() | 
	err_init();

	if (ret)
		lprintf(LOG_ALOT, "failed with %d (%s)\n", err_errno(), err_string());

#ifdef WIN32
	StartSockets();
#endif

	return ret;
}

/** @brief sleep (comf|p)ortable 
  * @param sec seconds to sleep 
  */
void time_sleep(int sec)
{
#ifdef WIN32
	Sleep(sec * 1000);
#else
	sleep(sec);
#endif
}

/** brief random integer 


<pre>
   In Numerical Recipes in C: The Art of Scientific Computing (William H.
   Press,  Brian  P.  Flannery, Saul A. Teukolsky, William T. Vetterling;
   New York: Cambridge University Press, 1992 (2nd  ed.,  p.  277)),  the
   following comments are made:

            "If you want to generate a random integer between 1 and 10, you
             should always do it by using high-order bits, as in

                    j=1+(int) (10.0*rand()/(RAND_MAX+1.0));

              and never by anything resembling

                     j=1+(rand() % 10);

              (which uses lower-order bits)."

	ps: 
		intrand should include both borders lo and hi !!
</pre>
*/
int intrand(int lo, int hi)
{
  double diff = (hi - lo)+1;

  return lo + (int)(diff * rand() / (RAND_MAX + 1.0));
}


/** @brief random string 
   allocate and randomly fill a string with lowercase letters */
char *strrand(int len) 
{
	int t;
	char *c = calloc(len,sizeof(char));

	for (t=0; t< len; t++) {
		c[t] = intrand('a','z');
	}
	c[len-1] = '\0';

	return c;
}

/** @brief custom strsep 
 * mimic strsep behaviour (strsep is not portable !) :
 * extract a token from a string based on the given delinmiters
 *
 * @param stringp base string pointer 
 * @param delim string with delimiter characters
 * @return pointer to the token
 */
char *str_sep(char **stringp, const char *delim)
{
    register char *s;
    register const char *spanp;
    register int c, sc;
    char *tok;

    if ((s = *stringp) == NULL)
        return (NULL);
    for (tok = s;;) {
        c = *s++;
        spanp = delim;
        do {
            if ((sc = *spanp++) == c) {
                if (c == 0)
                    s = NULL;
                else
                    s[-1] = 0;
                *stringp = s;
                return (tok);
            }
        } while (sc != 0);
    }
    /* NOTREACHED */
}

/** @brief randomize once */
void randist(void)
{
	time_t t = time(NULL);

	lprintf(LOG_DEBUG, "'randomizing' on seed %d\n", (int)t);
	srand((int)t);
}

/** @brief instantiate tuple
 * create a new key,value type duplicating the paramaters
 * @param k key for the tuple, it will be duplicated
 * @param v value for the tuple, it will be duplicated
 * @return a new tuple with key and value 
 */
tuple_t *tuple_ist(const char *k, const char *v)
{
	tuple_t *tp = (tuple_t *)calloc(sizeof(tuple_t),1);

	tp->key = strdup(k);
	tp->val = strdup(v);

	return tp;
}

#ifdef USE_THIS
static int randomtest(int num, int lo, int hi)
{
	int ret = 1; /*  1 = ok */
	int t;
	int r;
	int total = 0;
	int *counter;
	float mean;
	int range = (hi-lo)+1;  /* e.g (3-2)+1 =2 -> 2 and 3 */

	/* this needs to have some substance and is not exhaustive  */
	if (num < 10000) num = 10000;

	/* calloc, clearing everything to 0  */
	counter = calloc(sizeof(int), range);

	for (t=0; t < num; t++) {
		r = intrand(lo,hi);
		if (r == hi) {
		}
		/* e.g. : 2-2 = 0, 3-2= 1 */
		counter[r-lo] ++;
	}

	for (t=0; t < range ; t++) {
		total += counter[t];
	}
	mean = (float)total / range; 
	/* second go , get the deviation */
	for (t=0; t < range ; t++) {
		float r,f = (float) counter[t];
		/* i'm not a statistician, but 50 % must spell trouble */
		/* if not done on too small of a set  */
		r = mean-f;
		if (r<0) r*=-1;
		r /= mean;
		if (r > 0.3) {
			printf("way off %f (value %d (%d times))\n", r, t, counter[t]);
			ret=0;
		}
	}
	
	free(counter);

	return ret;
}
#endif

/** @brief create 32-bit string
 * create printable bit representation of a 32 bit integer 
 * @param base int32_t to represent
 * @return static buffer containing the bits
 */
char *bit_string_32(int32_t base)
{
	static char buf[33]={0};
	int t;

	for (t=0; t< 32; t++) {
		if (base & (1 << (31-t))) 
			buf[t] = '1';
		else 
			buf[t] = '0';
	}
	return buf;
}

/** @brief create 16-bit string
 * create printable bit representation of a 16 bit integer 
 * @param base int16_t to represent
 * @return static buffer containing the bits
 */
char *bit_string_16(int16_t base)
{
	static char buf[17]={0};
	int t;

	for (t=0; t< 16; t++) {
		if (base & (1 << (15-t))) 
			buf[t] = '1';
		else 
			buf[t] = '0';
	}
	return buf;
}

/** @brief create 8-bit string
 * create printable bit representation of a 8 bit integer 
 * @param base int8_t to represent
 * @return static buffer containing the bits
 */
char *bit_string_8(int8_t base)
{
	static char buf[9]={0};
	int t;

	for (t=0; t< 8; t++) {
		if (base & (1 << (7-t))) 
			buf[t] = '1';
		else 
			buf[t] = '0';
	}
	return buf;
}

static int generic_test(int len,int wid,char *name, tst_fnc_t fnc)
{
	int ret = fnc(len,wid);
	log_ok(LOG_ALOT, name, ret);
	fflush(stdout);
	return ret;
}

#ifdef USE_THIS
static int mytest(void)
{
	char *str= calloc(100,1);
	char *ptr;
	char *test;
	int len;
	char *fname;
	char *dir="../../dat";
	char *dir2="../../dat";
	char *ibase="uk";

	dir2=ego_strdup(dir);

	fname = b_sprintf("%s/%s/network.vrt", dir2, ibase);
	printf("%s/%s/network.vrt", dir2, ibase);

	//printf("%s\n", bit_string_16(42682));

	exit(0);

	test = b_sprintf("jaj %d ha", 100);
	len = b_strlenf("jaj %d ha", 100);
	
	printf("%s,%d\n", test, len);

	exit(0);

	snprintf(str, 100, "a,b,,__c,d,");

	test = strdup(str);
	printf("string was %s\n", str);

	ptr = str_sep(&str,",_");
	printf("string is %s: %s\n", str, ptr );
	ptr = str_sep(&str,",_");
	printf("string is %s: %s\n", str, ptr );
	ptr = str_sep(&str,",_");
	printf("string is %s: %s\n", str, ptr );
	ptr = str_sep(&str,",_");
	printf("string is %s: %s\n", str, ptr );
	ptr = str_sep(&str,",_");
	printf("string is %s: %s\n", str, ptr );
	ptr = str_sep(&str,",_");
	printf("string is %s: %s\n", str, ptr );
	ptr = str_sep(&str,",_");

	return 1;
}
#endif

/** @brief ego library testing ground
 * internal test function
 */
int ego_test(int len, int wid)
{
	int ret=1;

	/* mytest(); */
	/* exit(0); */

	/* ret = randomtest(len, 10, 90); */

	//ret &= generic_test(len,wid,"current test",    		tree_test);
	ret &= generic_test(len,wid,"current test",    		prm_test);
	//ret &= generic_test(len,wid,"current test",    		eheap_test);

	return ret;
	ret &= generic_test(len,wid,"current test",    		md5_test);
	/* ret &= generic_test(len,wid,"json test",    		json_test); */
	ret &= generic_test(len,wid,"scale test",    		scale_test);
	/* ret &= generic_test(len,wid,"timewindows test",    		tw_test); */
	/* ret &= generic_test(len,wid,"set test",    		set_test); */
	/* ret &= generic_test(len,wid,"at test",    		at_test); */
	ret &= generic_test(len,wid,"current test",    		dlst_test);
	/* ret &= generic_test(len,wid,"current test",    		iv_test); */
	/* ret &= generic_test(len,wid,"current test",    		automaton_test); */
	/* ret &= generic_test(len,wid,"current test",    		data_test); */
	/* ret &= generic_test(len,wid,"current test",    		table_test); */
	/* ret &= generic_test(len,wid,"current test",    		aa_test); */
	/* ret &= generic_test(len,wid,"current test",    		dsp_test); */
	/* ret &= generic_test(len,wid,"current test",    		ida_test); */
	/* ret &= generic_test(len,wid,"current test",    		da_test); */
	/* ret &= generic_test(len,wid,"current test",    		graph_test); */


	/* ret &= generic_test(len,wid,"tlv test",    			tlv_test); */
	/* ret &= generic_test(len,wid,"socket test",    		socket_test); */
	ret &= generic_test(len,wid,"log test",    			log_test);
	ret &= generic_test(len,wid,"table test", 	    	table_test);
	ret &= generic_test(len,wid,"indexed array test", 	ida_test);
	ret &= generic_test(len,wid,"permutation test",		prm_test);
	ret &= generic_test(len,wid,"bufferd io test", 		bio_test);
	ret &= generic_test(len,wid,"io test", 				io_test);
	ret &= generic_test(len,wid,"lifo (stack) test", 	lifo_test);
//	ret &= generic_test(len,wid,"fifo (queue) test", 	fifo_test);
	ret &= generic_test(len,wid,"double list test", 	dlst_test);
	ret &= generic_test(len,wid,"ring buffer test", 	rbuf_test);
	ret &= generic_test(len,wid,"buffer test", 			buf_test);
	ret &= generic_test(len,wid,"dynamic array test", 	da_test);
	ret &= generic_test(len,wid,"heap test", 			eheap_test);
	ret &= generic_test(len,wid,"index test", 			idx_test);
	ret &= generic_test(len,wid,"tree test", 			tree_test);
	/* ret &= generic_test(len,wid,"interval test", 		iv_test); */


	return ret;
}

