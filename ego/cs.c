/** @file

	client server automation

	implementation of a generic server handling requests and a generic client 
	able to post these requests on the server, both udp and tcp connection
	are supported
*/

#include <ego.h>
#include <errno.h>
#include <signal.h>
#include <sys/select.h>
#include <sys/wait.h>


/** 
 * generic server and client
 */

#define BACK_LOG 5

/** @brief instantiate client
  * create a new client object with default settings on the socket provided
  * 
  * @param sp a socket object created by sock_ist
  * @return a newly created client object
  */
client_t *client_ist(socket_t *sp)
{
	client_t *cp = (client_t *)calloc(sizeof(client_t),1);

	/* printf("Adding fd %d\n", sp->fd); */
	cp->sock = sp;
	cp->iop = io_ist(IO_SOCKET, sp, "rw", 0);
	cp->state = -1;
	cp->udata = NULL;

	return cp;
}

/** @ brief set user data hook
 *
 * if needed a user can pass private data through the client_t * structure
 * the client pointer is passed in the handler function so you can retrieve
 * the udata hook there again.
 * 
 * @param cp pointer to the client_t struct
 * @param udata void pointer to users data handle
 */
void client_set_udata(client_t *cp, void *udata)
{
	if (!cp) return;

	cp->udata = udata;
}

/** @ brief release client
 *
 * release a client object created with client_ist
 * 
 * @param cp pointer to the client_t struct
 */
void client_rls(client_t *cp)
{
	if (!cp) return;
	socket_rls(cp->sock);
	io_rls(cp->iop);
	free(cp);
}

/** @ brief dump client info
 *
 * dump information about a client object 
 * 
 * @param cp pointer to the client_t struct
 */
void client_dmp(client_t *cp)
{
	/* char *type = (cp->type == SOCK_STREAM) ? "TCP" : "UDP"; */
	char *type = "?";
	printf("%p (%s) ", cp, type);
}

/** @ brief instantiate server
 *
 * create a new server object
 * 
 * @param backlog backlog parameter used for createing the socket
 */
server_t *server_ist(int backlog)
{
	server_t *sp = (server_t *)calloc(sizeof(server_t),1);
	sp->services = da_ist(sizeof(service_t),0);
	return sp;
}

/** @ brief release server
 *
 * release a server object and free up ressources
 * 
 * @param sp pointer to a server object
 */
void server_rls(server_t *sp)
{
	int t;
	service_t **spp;

	if (!sp) return;

	if (sp->services) {
		for (t=0; t< da_len(sp->services); t++) {
			spp = da_get(sp->services, t);
			service_rls(*spp);
		}
		da_rls((*spp)->clients);
	}

	free(sp);
}

static void client_set_service(client_t *cp, service_t *sp)
{
	if (!sp || !cp )return;

	cp->service = sp;
}

static void ign_handler(int sig)
{
    int pid;
    int stat;

	// might be more than 1 ?
    do
    {
        pid = waitpid(-1, &stat, WNOHANG | WUNTRACED);
    printf("got sig %d on %d , stat is %d\n", sig, pid, stat);
    } while (pid > 0);
}

/** @ brief catch signals
 *
 * mainly, just catch and ignore the signals ,the loop will
 * fall through and fix the clients
 * 
 */
void server_catch_signals(void)
{
	struct sigaction ign_action;
	sigset_t ign_mask;

	sigemptyset(&ign_mask);

	// this is our main concern, client died just while 
	// we want to read/write
	sigaddset(&ign_mask, SIGPIPE);

	ign_action.sa_handler = ign_handler;
    ign_action.sa_mask    = ign_mask;
    ign_action.sa_flags   = 0;

	sigaction(SIGPIPE, &ign_action, NULL);
}


/** @ brief wait for server messages
 *
 * waits for new server messages from a client
 * 
 * @param sp pointer to a server object
 * @return a pointer to the client that sent a request
 */
client_t *server_wait(server_t *sp)
{
	int max=0, t, u;
	int res;
	fd_set read_fd;
	fd_set err_fd;
	client_t **cpp;
	client_t *nc;
	service_t **spp;
	socket_t *ns;
	int sock;

	FD_ZERO(&err_fd);
	FD_ZERO(&read_fd);

	/* first, count which socket has the highest fd (neede for select) */
	for (t=0; t< da_len(sp->services); t++) {
		spp = da_get(sp->services,t); 
		sock = (*spp)->sock->fd;
		if (sock > max) max = sock;
		FD_SET(sock, &err_fd);
		FD_SET(sock, &read_fd);
		for (u=0; u< da_len((*spp)->clients); u++) {
			cpp = da_get((*spp)->clients,u); 
			sock = (*cpp)->sock->fd;
			if (sock > max) max = sock;
			FD_SET(sock, &err_fd);
			FD_SET(sock, &read_fd);
			//printf("added %d %d and %d\n", sock, err_fd, read_fd);
		}
	}
	
again:
	/* printf("waiting %d\n", max); */
    res = select(max + 1, &read_fd, NULL, &err_fd, NULL);
	/* printf("waited %d %d %d\n", res, FD_ISSET(sock, &read_fd), */
	/* FD_ISSET(sock, &err_fd)); */

	if (res < 0) {
		//printf("retrying with res is %d, err_fd = %d\n", res, err_fd);
		// this is probably one of the child signals, just retry
		goto again;
	} 

	for (t=0; t< da_len(sp->services); t++) {
		spp = da_get(sp->services,t); 
		sock = (*spp)->sock->fd;
		if  (FD_ISSET(sock, &err_fd)) {
			printf("heel jammer\n");
		}
		if  (FD_ISSET(sock, &read_fd)) {
			ns = socket_accept((*spp)->sock);
			if (!ns) {
				perror("accept");
				return NULL;
			} else {
				da_len((*spp)->clients);
				nc =  client_ist(ns);
				nc->state = CLT_INIT;
				if ((*spp)->sock->type == SOCK_STREAM) 
					service_add_client(*spp, nc);
				/* printf("now %d clients \n", c); */
				client_set_service(nc,*spp);
				return nc;
			}
		}
		for (u=0; u< da_len((*spp)->clients); u++) {
			cpp = da_get((*spp)->clients,u); 
			(*cpp)->state = CLT_RUNNING;
			sock = (*cpp)->sock->fd;
			if  (FD_ISSET(sock, &read_fd)) {
				return *cpp;
			}
			if  (FD_ISSET(sock, &err_fd)) {
				printf("jammer");
			}
		}
	}
	printf("fell through!\n");
	return NULL;
}

/** @ brief handle a client
 *
 * handle a client request 
 * 
 * @param cp pointer to a client object
 * @return 0 to continue, or 1 to stop the server
 */
int server_handle(client_t *cp)
{
	intptr_t ret;
	int c;

	ret = cp->service->fnc(cp);
	// 0 = just continue connected
	// 1 = disconnect
	// 2 = stop server

	if (ret == CS_DONE && cp->sock->type == SOCK_STREAM) {
		c = service_del_client(cp);
		if (c<0) printf("failed deleting client \n");
		/* else printf("now %d clients \n", c); */
	}

	if (ret == CS_STOP) return 1;
	// return 0 unless you want to STOP THE SERVER !!!!
	return 0;
}

/** @ brief start request loop
 *
 * loop waiting for request and handle them
 * 
 * @param sp pointer to a server object
 * @return 0 to continue, or 1 to stop the server
 */
int server_loop(server_t *sp)
{
	int stop=0;
	client_t *cp;

	lprintf(LOG_DEBUG, "Starting server\n");
	
	while (!stop) {
		/* printf("--- waiting\n"); */
		cp = server_wait(sp);
		/* printf("--- waited\n"); */
		stop = server_handle(cp);
		/* printf("--- handled %d\n", stop); */
	}
	
	return stop;
}

/** @ brief delete a client 
 *
 * delete a client from a servers service 
 * 
 * @param cp pointer to the client object to be removed
 * @return 0 on success, or -1 on error
 */
int service_del_client(client_t *cp)
{
	int u;
	service_t *sp;
	client_t **cpp;

	if (!cp || !cp->service )return 0;

	sp = cp->service;

	for (u=0; u< da_len(sp->clients); u++) {
		cpp = da_get(sp->clients,u); 
		if (*cpp == cp) {
			client_rls(*cpp);
			da_del(sp->clients, u,1);
			return da_len(sp->clients);
		}
	}
	return -1;
}


/** @ brief add a client 
 *
 * add a client to a servers service 
 * 
 * @param sp pointer to a service object
 * @param cp pointer to the client to be added
 * @return the number of clients for this service
 */
int service_add_client(service_t *sp, client_t *cp)
{
	if (!sp || !cp )return 0;

	client_set_service(cp,sp);
	da_add(sp->clients, &cp);
	return da_len(sp->clients);
}

/** @ brief instantiate service
 *
 * instantiate a service to be handled by the server
 * 
 * @param port port on wich to listen for request for this service
 * @param type type of protocol either SOCK_STREAM or SOCK_DGRAM
 * @param handler service handler a function to be started at any request
 * @param udata static user data to be sent along with the handler function
 * @return the number of clients for this service
 */
service_t *service_ist(int port, int type, service_fnc_t handler, void *udata)
{
	service_t *sp = (service_t *)calloc(sizeof(service_t),1);

	sp ->fnc = handler;
	sp ->clients = da_ist(sizeof(client_t),0);

	sp->sock = socket_ist(type);
	socket_reuse(sp->sock);
	socket_sethostbyname(sp->sock,"localhost",0);
	socket_bind(sp->sock, port);

	sp->udata = udata;

	// TODO: get a socket: Socket type not supported here!
	if (type == SOCK_DGRAM) return sp;

	if (socket_listen(sp->sock)) {
		perror("listen");
		return NULL;
	}
	return sp;
}

/** @ brief add a service
 *
 * add a new service to the server
 * 
 * @param sp pointer to server object
 * @param svp pointer to a service object to be handled by the server
 * @return the number of services for this server
 */
int server_add_service(server_t *sp, service_t *svp)
{
	if (!sp || !svp )return 0;

	svp->srv=sp;

	da_add(sp->services, &svp);
	return da_len(sp->services);
}

/** @ brief release a service
 *
 * release a service object and free up the resources used
 * 
 * @param sp pointer to a service object
 */
void service_rls(service_t *sp)
{
	int t;
	client_t **cpp;
	if (!sp) return;

	if (sp->clients) {
		for (t=0; t< da_len(sp->clients); t++) {
			cpp = da_get(sp->clients, t);
			client_rls(*cpp);
		}
		da_rls(sp->clients);
	}

	free(sp);
}

/** @brief test buffer length */
#define BLEN 100
static int service_dmp(service_t *sp)
{
	//return printf("service %p\n", sp);
	return 0;
}

// test section

static intptr_t srv_dmp(client_t *cp)
{
	intptr_t ret;
	char buf[BLEN];

	ret = io_read(cp->iop, buf, BLEN-1);
	buf[ret]='\0';
	printf ("read %d bytes \"%s\"", (int)ret, buf);
	
	printf ("'handling' request on service :");
	service_dmp(cp->service);
	printf (" for client :");
	client_dmp(cp);
	// important!! MUST return 0 if no bytes been read
	return ret;
}

/** @brief create testserver
 *
 * internal test function
 */
void server_testserver(void)
{
	int res;
	server_t *srv = server_ist(5);
	service_t *s1 = service_ist(8811,  SOCK_STREAM, srv_dmp, NULL);
	service_t *s2 = service_ist(8812,  SOCK_STREAM, srv_dmp, NULL);

	server_add_service(srv, s1);
	server_add_service(srv, s2);

	res= server_loop(srv);

	printf("Server ended with code %d\n", res);

	//  valgrind test here ?
	server_rls(srv);
}

