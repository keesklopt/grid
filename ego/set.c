#include <ego.h>

/* A set is a collection of objects, on which you can perform 
	operations like union, intersection etc. The set is stored 
	as a sorted array. 

	NOTE: this code is not monkey proof. :
	1. there is no checking against bad input, though i *TRY* to use
		set = NULL as an empty set
	2. double entries are removed at all actions
	3. no freeing in any of the function except set_rls of course,
		clean up after yourself by using set_rls a lot.
*/

int set_len(set_t * set)
{
	if (!set )
		return -1;

	if (!set->arr)
		return 0;

	return da_len(set->arr);
}

int set_empty(set_t * set)
{
	int ret = set_len(set);

	if (ret == 0)
		return 1;
	if (ret < 0)
		return -1;
	return 0;
}

set_t * set_ist(int wid, cmp_fnc_t cmp)
{
	set_t * set = (set_t *)calloc(1,sizeof(set_t));

	if (!set || !cmp)
		return NULL;

	// NO , why ??
	/* set->arr = da_ist(sizeof(void *),0); */
	set->arr = da_ist(wid,0);
	set->cmp = cmp;
	da_sort(set->arr,cmp,NULL);

	return set;
}

void *set_get(set_t *set, void *key)
{
	int pos;
	pos = set_pos(set, key);

	if (pos<0) return NULL;

	return da_get(set->arr, pos);
}

void set_rls(set_t * set)
{
	if (!set)
		return;

	if (set->arr)
		da_rls(set->arr);
	if (set) free(set);
}

void *set_next(set_t * set)
{
	void *elm;

	if (set->pos > da_len(set->arr))
		return NULL;
	elm = da_get(set->arr, set->pos);
	set->pos++;

	return elm;
}

void * set_first(set_t * set)
{

	if (!set || !set->arr) {
		return NULL;
	}
	set->pos = 0;

	return set_next(set);
}

char *set_string(set_t * set, str_fnc_t dmp)
{
	char *newstr, *oldstr;
	void *elm;

	if (!set || !set->arr) {
		newstr = ego_sprintf("%c", B_EMPTY);
		return newstr;
	}
	
	newstr = ego_sprintf("{");

	if (da_len(set->arr)< 1) {
		oldstr = newstr;
		newstr = ego_sprintf("%s%c", oldstr, B_EMPTY);
		if (oldstr) free(oldstr);
	} else {
		elm = set_first(set); 
		oldstr = newstr;
		if (elm) {
			char *str = dmp(elm);
			newstr = ego_strcat_free(newstr, str);
		}

		for (elm= set_next(set); elm != NULL;
		 	elm = set_next(set)) {
			newstr = ego_strcat(newstr, ",");
			newstr = ego_strcat_free(newstr, dmp(elm));
		}
	}
	newstr = ego_strcat(newstr, "}");
	return newstr;
}

void set_dmp(set_t * set, dmp_fnc_t dmp, void *xtra)
{
	void *elm;

	if (!set || !set->arr) {
		printf("NOT-DEFINED\n");
		return;
	}
	printf("{");
	if (da_len(set->arr)< 1) {
		// not visible on all text terminals 
		/* printf("%c", 248); */
		printf("empty");
	} else {
		elm = set_first(set); 
		if (elm)
			dmp(0, elm, xtra);	

		for (elm= set_next(set); elm != NULL;
		 	elm = set_next(set)) {
			printf(",");
			dmp(0, elm, xtra);	
		}
	}
	printf("}\n");
}

int set_pos(set_t * set, void *a)
{
	int pos;

	pos = da_pos(set->arr, a, set->cmp, NULL);
	return pos;
}

int set_contains(set_t * set, void *a)
{
	int pos;

	pos = set_pos(set, a);

	if (pos >=0) 
		return -1;
	else 
		return 0;
}

int set_add(set_t * set, void *a)
{
	if (set_contains(set,a))
		return -1;

	da_ins_sorted(set->arr, a, set->cmp, NULL);
	return da_len(set->arr);
}

/* you'r a grown boy now, i do NOT check if set_t *'s are compatible */
set_t * sets_combine(set_t * a, set_t * b,int how)
{
	int x=0,y=0;
	void *elma=NULL, *elmb=NULL;
	int ret;
	da_t * use;
	set_t * newset;
	if (!a && !b)
		return NULL;

	if (a) use = a->arr;
	else use = b->arr;

	newset = set_ist(use->wid, a->cmp);
	
	if (a && a->arr) 
		elma = da_get(a->arr, 0);
	if (b && b->arr) 
		elmb = da_get(b->arr, 0);

	while ((a && a->arr && x < da_len(a->arr)) 
		|| (b && b->arr && y < da_len(b->arr))) {
	
		if (!a || !a->arr || !elma) ret = 1;
		else if (!b || !b->arr || !elmb) ret = -1;
		else ret = a->cmp(elma,elmb,NULL);

		if (ret < 0) { // A won
			if (how == UNION || how == SUBTRACT)
				set_add(newset, elma);
			elma = da_get(a->arr, ++x);
		} else if (ret > 0) {
			if (how == UNION)
				set_add(newset, elmb);
			elmb = da_get(b->arr, ++y);
		} else { // elements are the same, only add one
			if (how != SUBTRACT) {
				set_add(newset, elma);
			}
			elma = da_get(a->arr, ++x);
			elmb = da_get(b->arr, ++y);
		}
	}

	return newset;
}

set_t * set_dup(set_t * a)
{
	set_t * newset;
	void *elma;
	int t;
	if (!a) return NULL;
	newset = set_ist(a->arr->wid, a->cmp);

	for (t=0; t < da_len(a->arr); t++) {
		elma = da_get(a->arr, t);
		set_add(newset, elma);
	}
	return newset;
}

int sets_equal(set_t * a, set_t * b)
{
	void *elma, *elmb;
	int t;

	if (!a&&!b) return 1;
	if (!a || !b) return 0;
	if (da_len(a->arr) != da_len(b->arr))
		return 0;

	for (t=0; t < da_len(a->arr); t++) {
		elma = da_get(a->arr, t);
		elmb = da_get(b->arr, t);

		if (a->cmp(elma,elmb,NULL))
			return 0;
	}
	return 1;
}

// subtract b from a, returning all entries that are only in a !
set_t * sets_subtract(set_t * a, set_t * b)
{
	return sets_combine(a,b,SUBTRACT);
}

set_t * sets_union(set_t * a, set_t * b)
{
	return sets_combine(a,b,UNION);
}

set_t * sets_intersection(set_t * a, set_t * b)
{
	return sets_combine(a,b,INTERSECTION);
}

set_t * sets_add(set_t * a, set_t * b)
{
	int t;
	void *elm;
	set_t * newset = set_ist(a->arr->wid, a->cmp);
	
	for (t=0; t< da_len(a->arr); t++) {
		elm = da_get(a->arr, t);
		set_add(newset, elm);
	}
	for (t=0; t< da_len(b->arr); t++) {
		elm = da_get(b->arr, t);
		set_add(newset, elm);
	}

	return newset;
}

// operations
int set_test2(int w, int h)
{
	int t;
	set_t *seta = set_ist(sizeof(int), int_cmp);
	set_t *setb = set_ist(sizeof(int), int_cmp);
	set_t *set2;

	t = 110;
	set_add(seta, &t);
	t = 120;
	set_add(seta, &t);
	t = 140;
	set_add(seta, &t);
	t = 150;
	set_add(seta, &t);

	t = 100;
	set_add(setb, &t);
	t = 120;
	set_add(setb, &t);
	t = 130;
	set_add(setb, &t);
	t = 150;
	set_add(setb, &t);

	set_dmp(seta, int_dmp ,NULL);
	set_dmp(setb, int_dmp ,NULL);

	set2 = sets_intersection(seta,setb);
	set_dmp(set2, int_dmp ,NULL);
	set_rls(set2);

	set2 = sets_union(seta,setb);
	set_dmp(set2, int_dmp ,NULL);
	set_rls(set2);

	set2 = sets_subtract(seta,setb);
	set_dmp(set2, int_dmp ,NULL);
	set_rls(set2);

	set2 = sets_subtract(setb,seta);
	set_dmp(set2, int_dmp ,NULL);
	set_rls(set2);

	set_rls(seta);
	set_dmp(setb, int_dmp ,NULL);
	set_rls(setb);

	return 0;
}

int set_test1(int w, int h)
{
	int t, ret;
	set_t *seta = set_ist(sizeof(int), int_cmp);
	set_t *setb = set_ist(sizeof(int), int_cmp);
	set_t *set2;

	t = 140;
	set_add(seta, &t);
	t = 120;
	set_add(seta, &t);
	t = 130;
	set_add(seta, &t);
	t = 110;
	set_add(seta, &t);
	t = 130;
	set_add(setb, &t);
	t = 120;
	set_add(setb, &t);
	t = 110;
	set_add(setb, &t);
	t = 120;
	set_add(setb, &t);

	set2 = set_dup(setb);
	set_dmp(seta, int_dmp ,NULL);

	ret = sets_equal(seta,set2);
	printf("%d \n", ret);
	ret = sets_equal(seta,set2);
	printf("%d \n", ret);
	ret = sets_equal(setb,set2);
	printf("%d \n", ret);
	
	set_rls(seta);
	set_dmp(setb, int_dmp ,NULL);
	set_rls(setb);

	set_rls(set2);

	return 1;
}


int set_test(int w, int l)
{
	printf ("the set test \n");

	/* set_test1(w,l); */
	set_test2(w,l);

	return 1;
}

