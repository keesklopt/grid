/** @file
 * 
	generic error handling 
 */

#include <ego.h>
#include <stdio.h>
#include <string.h>

/** @brief main/default error space */
errspace_t def_err;

/** @brief default errors
 * errors used throughout ego library
 */
err_t err_glob[] = {
    {  ERR_NONE,        "no error"},
    {  ERR_MEM,         "failed allocating memory"},
    {  ERR_PARM,        "illegal parameter value"},
    {  ERR_NULL,        "unexpected NULL parameter"},
    {  ERR_RANGE,       "array index out of range"},

/* object specific */
    {  ERR_TREE_CMP,    "can't _add a node wit no cmp function, (use _ins)"},

    {  ERR_IO_OPEN,     "couldn't open io instance"},

    {  ERR_SOCK_CONNECT,"couldn't connect to socket"},

    {  ERR_TOK_INIT,    "tokenizer has no begin state (use tok_push)"},

    {  ERR_AUTH_LOGIN,  "login failed "},

    {  ERR_SMTP_LENGTH, "return code was too short"},
    {  ERR_SMTP_UNEXPECTED,  "unexpected return code"},

/* mostly these messages are overwritten by err_set_string */
    {  ERR_CURL,        "error from curl library"},
    {  ERR_XML2,        "error from xml2 library"},
    {  ERR_LIBC,      	"error from libc/system"},

/* must always be last !! */
    {  ERR_LAST,        "error error"},
};

/** @brief initialise error handling
 * once a session initialisation, sets up error messages etc
 * this is automatically called from ego_init()
 * @returns 0 on success, error codes otherwise 
 */
int err_init()
{
	int t;
	
	def_err.err_no = ERR_NONE;
	def_err.errors = da_ist(sizeof(err_t),0);

	if (def_err.errors == NULL) return err_set(ERR_MEM);

	/* register all standard errors, the ones you can 
	rely on being the same number */
	for (t=0; t<= ERR_LAST; t++) {
		if (t != err_register(err_glob[t].msg)) {
			return -1;
		}
		if (t != err_glob[t].err_no) {
			return -2;
		}
	}

	return 0;
}

/** @brief exit error code
 * releases resources used bu err_init()
 */
void err_exit(void)
{
	if (def_err.errors) da_rls(def_err.errors);
}

/* int vsnprintf(char *,size_t, const char *, va_list); */


/** @brief set error string
 * overwrite an error string with a new value
 * @param num error number or code
 * @param fmt format string used to augment the error string
 * @param ... variable arguments list used to fill fmt string
 */
void err_set_string(int num, char *fmt, ...)
{
	err_t *e;
	va_list vl;

	va_start(vl, fmt);

	e = (err_t *)da_get(def_err.errors, num);
	if (!e) return;
#ifdef WIN32
	vsnprintf_s(e->msg, ERRMSG_MAX, ERRMSG_MAX, fmt, vl);
#else
	vsnprintf(e->msg, ERRMSG_MAX, fmt, vl);
#endif
}

/** @brief get error string
 * get the error string associated with currently set default errno
 * @return current default error string
 */
char * err_string(void)
{
	err_t *e = (err_t *)da_get(def_err.errors, def_err.err_no);
	if (!e) return "unknow error !!";
	return e->msg;
}

/** @brief get default error number
 * get the currently set default errno
 * @return current default error number
 */
int err_errno(void)
{
	return def_err.err_no;
}

/** @brief set error number
 * set error number for a certain error space
 * @param esp error space used
 * @param ernum error number to set
 * @return set error number
 */
int err_set_to(errspace_t *esp, int ernum)
{
	if (!esp) return-1;
	return esp->err_no = ernum;
}

/** @brief set default error number
 * set error number for a the default error space
 * @param ernum error number to set
 * @return set error number
 */
int err_set(int ernum)
{
	return err_set_to(&def_err, ernum);
}

/** @brief register error string
 * register a custom error in the default error space
 * @param message error message
 * @return error number associated with the new error string
 */
int err_register(char *message)
{
	err_t nuone;

	memcpy(nuone.msg , message, ERRMSG_MAX);
	nuone.err_no = da_len(def_err.errors);
	da_add(def_err.errors, &nuone);
	return nuone.err_no;
}

/** @brief exit on error
 * exit if an error is set after printing the error string
 * @param what error number to test against
 * @param msg extra message to print
 */
void err_exit_if(int what, char *msg)
{
	if (!what) return;
	if (!msg) msg = "";

	printf("err_exit_if : %s (lasterr:%s)\n", 
			msg , err_string());

	exit(err_errno());
}

