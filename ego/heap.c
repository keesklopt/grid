/** @file 
	K- heap, a tree-in-array data structure, 

	the parent at position X in 
	the array has it's children at (X*K), (X*K)+1 .. etc
	note !! : hand tested with k=2 and k=3, due to the slow 
	sorting, is chose for da_t with kqsort as base for idx
*/
#include <ego.h>

/** @brief swap typed
 * Qsort routine from Bentley & McIlroy's "Engineering a Sort Function".
 */
#define swapcode(TYPE, parmi, parmj, n) {               \
         int32_t i = (n) / sizeof (TYPE);                   \
         TYPE *pi = (TYPE *) (parmi);            \
         TYPE *pj = (TYPE *) (parmj);            \
         do {                                            \
                 TYPE    t = *pi;                \
                 *pi++ = *pj;                            \
                 *pj++ = t;                              \
         } while (--i > 0);                        \
         }

static __inline void
swapfunc(char *a, char *b, int n, int swaptype)
{
        if(swaptype <= 1)
                swapcode(int32_t, a, b, n)
        else
                swapcode(char, a, b, n)
}

/** @brief initialiase swap
 * Qsort routine from Bentley & McIlroy's "Engineering a Sort Function".
 */
#define SWAPINIT(a, es) swaptype = ((char *)a - (char *)0) % sizeof(int32_t) || \
         es % sizeof(int32_t) ? 2 : es == sizeof(int32_t)? 0 : 1;

/** @brief swap two values
 * Qsort routine from Bentley & McIlroy's "Engineering a Sort Function".
 */
#define swap(a, b)                                      \
         if (swaptype == 0) {                            \
                 int32_t t = *(int32_t *)(a);                  \
                 *(int32_t *)(a) = *(int32_t *)(b);            \
                 *(int32_t *)(b) = t;                       \
         } else                                          \
                 swapfunc(a, b, es, swaptype)
 

/** @brief instantiate eheap
 * create a new heap of order K with data elements wid wide
 * @param wid width of each element in the heap
 * @param k order of the heap, 2 is a binary tree, 3 has three children etc
 * @param len length of the heap
 * @param cmp function to compare two items in a heap
 * @return new heap object
 */
eheap_t *eheap_ist(int wid, int k, int len, cmp_fnc_t cmp)
{
	eheap_t *hp =(eheap_t *) calloc(sizeof(eheap_t), 1);

	hp->k = k;
	hp->wid = wid;
	hp->cmp = cmp;

	hp->dap = da_ist(wid,len);

	return hp;
}

/** @brief release heap
 * free up all resources of heap object
 * @param hp heap object pointer
 */
void eheap_rls(eheap_t *hp)
{
	if (!hp) return;

	if (hp->dap) da_rls(hp->dap);
	
	free(hp);
}

/** @brief swap elements
 * swap around two elements in the heap
 * @param hp heap object pointer
 * @param a position of first element
 * @param b position of second element
 * @return 0 always
 */
int eheap_swap(eheap_t *hp, int a, int b)
{
	return da_swap(hp->dap,a,b);
}

/** @brief heap length
 * return number of elements in th eheap
 * @param hp heap object pointer
 * @return length of the heap
 */
/* use heap->dap->len at will, its faster*/
int eheap_len(eheap_t *hp)
{
	return da_len(hp->dap);
}

/** @brief heap size
 * calculate size for a filled heap of certain depth 
 * @param hp heap object pointer
 * @param depth depth to calculate
 * @return heap size at depth
 */
int eheap_size(eheap_t *hp, int depth)
{
	int t=0;
	int ret=1;
	int level=hp->k;

	for (t=0; t< depth; t++) {
		ret += level;
		level *= hp->k;
	}
	
	return ret;
}

/** @brief count parent nodes
 * how many nodes are parent nodes, actually the parent of the last node is
 * also the last parent, so after that all are children
 * @param hp heap object pointer
 * @return
 */
int eheap_nofparents(eheap_t *hp)
{
	int ret;

	/* that's simply the parent of the last element */
	/* everything before that is a parent, and after is not */
	ret = eheap_parent(hp, hp->dap->len-1);
	/* but we want the number of parents, not the last so +1 */
	return ret+1;
}

/** @brief parent node
 * get a nodes parent node
 * @param hp heap object pointer
 * @param node node to get the parent of
 * @return index of the parent node
 */
int eheap_parent(eheap_t *hp, int node)
{
	if (node == 0) return -1;

	return ((node-1)/hp->k);
}

/** @brief get child node
 * get one of the node's child nodes, which is the number of the child
 * @param hp heap object pointer
 * @param node parent node to get the children for
 * @param which which of the children (0..k) is needed
 * @return index of the requested child or -1 if non existent
 */
int eheap_child(eheap_t *hp,int node, unsigned int which)
{
	unsigned int logicalchild = (node * hp->k)+1;

	if (which > hp->k || 
		logicalchild > hp->dap->len)
		return -1;

	return logicalchild+which;
}

/** @brief calculate heap level
 * calculate at which level a node resides
 * @param hp heap object pointer
 * @param node which node to test 
 * @return
 */
int eheap_level(eheap_t *hp, int node)
{
	int t=0;
	int level=1;
	
	while (node > 0) {
		level *= hp->k;
		node -= level;
		t++;
	}
	return t;
}

/** @brief set node 
 * set a node at the given index in the heap, overwriting current data
 * @param hp heap object pointer
 * @param elm element to set
 * @param loc index of element to set
 * @return 0
 */
intptr_t eheap_set(eheap_t *hp, void *elm, int loc)
{
	return da_set(hp->dap, elm, loc);
}

/** @brief add to heap
 * add a new node to a heap and restore partially sortedness
 * @param hp heap object pointer 
 * @param elm data of element to add
 * @return 0
 */
int eheap_add(eheap_t *hp, void *elm)
{
	/* add at the end, then restore partial heap */
	da_add(hp->dap, elm);
	eheap_up(hp, hp->dap->len-1);

	return 0;
}

/** @brief leaf test
 * is this node a leaf
 * @param hp heap object pointer
 * @param pos index of node te test
 * @return 1 true or 0 false
 */
int eheap_isleaf(eheap_t *hp, int pos)
{
	int p = eheap_nofparents(hp);

	if (pos < p) return 0;
	return 1;
}

/** @brief parent test
 * is this node a parent 
 * @param hp heap object pointer
 * @param pos index of node te test
 * @return 1 true or 0 false
 */
int eheap_isparent(eheap_t *hp, int pos)
{
	return !eheap_isleaf(hp,pos);
}

/** @brief delete node
 * delete a node at index from the heap
 * @param hp heap object pointer
 * @param pos index of element to delete
 * @return 0
 */
int eheap_del(eheap_t *hp, int pos)
{
	int lastnode = hp->dap->len-1;
	int p;
	char *child, *parent;

	child = eheap_get(hp, pos);
	/* swap pos with the last one */
	eheap_swap(hp,pos,lastnode);
	/* remove the last one  */
	da_del(hp->dap, lastnode, 1);
	if (pos == lastnode) return 0;

	/* now find out if you need to bubble up or down */
	child = eheap_get(hp, pos);
	p = eheap_parent(hp,pos);
	if (p >= 0) {
		parent = eheap_get(hp,p);
		if (hp->cmp(parent,child,NULL) < 0)  {
			eheap_up(hp,pos);
			return 0;
		}
	}
	/* otherwise bubble down the swapped node */
	eheap_down(hp,pos);

	return 0;
}

/** @brief get from heap
 * get an object from the heap at position loc
 * @param hp heap object pointer
 * @param loc position of element to get
 * @return element data at loc
 */
void * eheap_get(eheap_t *hp, int loc)
{
	return da_get(hp->dap, loc);
}

/** @brief
 * also siftup or bubbleup: propagate an endnode up to the root 
	restoring (or setting) the partially sorted heap property which is:
	parent must be bigger than it's own children
 * @param hp heap object pointer
 * @param pos index of endnode to bubble up
 * @return
 */
void eheap_up(eheap_t *hp, int pos)
{
	int parent;
	int es = hp->wid;
	int swaptype;
	char *elmparent, *elmchild;

	SWAPINIT(hp->dap->bufp->data, hp->wid);

	while (pos >= 0) {
		elmchild = &hp->dap->bufp->data[hp->dap->wid*pos];
		parent = eheap_parent(hp,pos);
		if (parent < 0) return; /* has no parent */
		/* elmparent = heap_get(hp, parent); */
		elmparent = &hp->dap->bufp->data[hp->dap->wid*parent];
		
		if (hp->cmp(elmparent, elmchild,NULL) > 0)
		/* if no swap has to be done we are ready !! */
			return;
			
		/* eheap_swap(pos, parent); */
		swap(elmchild, elmparent);
		pos = parent;
	}
}

/** @brief test sortedness
 * test if this heap is completely sorted
 * @param hp heap object pointer
 * @return 0 for false, 1 for true
 */
int eheap_issorted(eheap_t *hp)
{
	unsigned int t;
	char *last, *next;
	
	last = eheap_get(hp, 0);
	for (t=1; t< hp->dap->len; t++) {
		next = eheap_get(hp, t);
		if (hp->cmp(last, next,NULL) > 0) 
			return 0;
		last=next;
	}

	return 1;
}

/** @brief teast heap
 * test if heap is partially sorted heap (is a parentbigger than all of 
 * its children)
 * @param hp heap object pointer
 * @return 0 for false, 1 for true
 */
int eheap_isheap(eheap_t *hp)
{
	unsigned int t,c,k, nparents;
	char *parent, *child;

	nparents = eheap_nofparents(hp);
	/* printf("testing %d parents \n", nparents); */

	for (t=0; t< nparents; t++) {
		parent = eheap_get(hp, t);

		for (k=0,c=eheap_child(hp,t,0); 
			k < hp->k && c>=0 && c< hp->dap->len; c++,k++)
		{
			child = eheap_get(hp, c);
			if (hp->cmp(parent, child,NULL) < 0) 
				return 0;
		}
	}

	return 1;
}

/** @brief bubble down
 * bubble down, needed to turn any array into a partially sorted heap 
 * but now we have to regard all child nodes
 * @param hp heap object pointer
 * @param pos parent node to bubble down
 */
void eheap_down(eheap_t *hp, unsigned int pos)
{
	unsigned int c,k;
	unsigned int biggest;
	int es = hp->wid;
	char *elmparent, *elmchild;
	int swaptype;

	SWAPINIT(hp->dap->bufp->data, hp->wid);

	while (pos < hp->dap->len) {
		biggest = pos;
		/* elmparent = eheap_get(hp, biggest); too slow (though clearer;)*/
		elmparent = &hp->dap->bufp->data[hp->dap->wid*biggest];
		/* walk all children but mind the empty ones  */
		for (c= (pos * hp->k)+1, k=0; 
			c>=0 && c< hp->dap->len && k<hp->k; c++,k++) {
			elmchild = &hp->dap->bufp->data[hp->dap->wid*c];
			if (hp->cmp(elmparent, elmchild,NULL) < 0) {
				/* with a new biggest to compare !! */
				biggest = c;
				elmparent = &hp->dap->bufp->data[hp->dap->wid*biggest];
			}
		}
		if (biggest == pos) return; /* done  */
		elmchild = &hp->dap->bufp->data[hp->dap->wid*pos];
		/* eheap_swap(pos, biggest); */
		swap(elmchild, elmparent);
		pos = biggest; 
	}
}

/** @brief partially sort heap
 * turn heap into a partially sorted heap
 * @param hp heap object pointer
 */
void eheap_sortpartial(eheap_t *hp)
{
	int np, t;

	/* for all parents, do a eheap_down */
	np = eheap_nofparents(hp);

	for (t=np-1; t>=0; t--) {
		eheap_down(hp, t);
	}
}

/** @brief sort heap
 * sort the heap into a straight sorted array
 * @param hp heap object pointer
 * @param cmp function to compare two gheap elements
 */
void eheap_sort(eheap_t *hp, cmp_fnc_t cmp)
{
	int limit, oldlen=hp->dap->len;
	int swaptype, es=0;
	if (cmp) hp->cmp = cmp;

	SWAPINIT(hp->dap->bufp->data, hp->wid);
	eheap_sortpartial(hp);

	//eheap_dmp(hp,int_dmp);
	//printf("partial test is %d\n", eheap_isheap(hp));

	for (limit = hp->dap->len-1; limit > 0; limit--)
	{
		//eheap_swap(0,limit);
 		swap(hp->dap->bufp->data,&hp->dap->bufp->data[hp->dap->wid*limit]);
		/* abuse of dap's len, so set it back again !!! */
		//eheap_dmp(hp,int_dmp);
		hp->dap->len--;
		eheap_down(hp,0);
		//eheap_dmp(hp,int_dmp);
	}
	hp->dap->len = oldlen;
}

/** @brief recursively dump tree
 * recusrive part of dumping a heap tree hierarchically
 * @param hp heap object pointer
 * @param root at which node t start the dump
 * @param lvl level counter for indentation
 * @param dmp function to dump 1 heap element
 * @return 0
 */
int eheap_treedmp_r(eheap_t *hp, unsigned int root, unsigned int lvl,  dmp_fnc_t dmp)
{
	intptr_t l=5;
	unsigned int t, k=0,x;
	void *elmp;

	elmp = eheap_get(hp, root);
	if (root < hp->dap->len)
		dmp(hp->wid, elmp, (void *)l);
	else 
		lprintf(LOG_ALOT, " - ");

	t = eheap_child(hp,root,0);
	if (t<0) return 0;

	for (k=0; k<hp->k; t++,k++) {
		eheap_treedmp_r(hp,t, lvl+1, dmp);
		if (k==hp->k-1) continue;
		lprintf(LOG_ALOT, "\n");
		for (x=0;x<lvl+1; x++) 
			lprintf(LOG_ALOT, "%*s", l, "");
	}
	return 0;
}

/** @brief dump heap tree
 * top level function for dumping a tree hierarchically
 * @param hp heap object pointer
 * @param dmp function to dump 1 heap element
 * @return 0
 */
int eheap_treedmp(eheap_t *hp, dmp_fnc_t dmp)
{

	eheap_treedmp_r(hp, 0, 0, dmp);
	printf("\n");
	return 0;
}

/** @brief dump heap data
 * dump the contents of a heap object
 * @param hp heap object pointer
 * @param dmp function to dump 1 heap element
 * @return 0
 */
int eheap_dmp(eheap_t *hp, dmp_fnc_t dmp)
{
	unsigned int t;
	void *elmp;

	lprintf(LOG_ALOT, "heap of wid %d, k = %d, %d elements \n", hp->wid, 
		hp->k, hp->dap->len);

	for (t=0; t< hp->dap->len ; t++) {
		elmp = eheap_get(hp, t);
		dmp(hp->wid, elmp, NULL);
		lprintf(LOG_ALOT, "\n");
	}
	lprintf(LOG_ALOT, "\n");

	return 0;
}

static int eheap_test_string(int num)
{
	int t;
	eheap_t *hp = eheap_ist(10, 2, 0, int_cmp);

	for (t=0; t< num ; t++) {
		char *rnd = strrand(10);
		eheap_add(hp, rnd);
	}

	eheap_dmp(hp, str_dmp);
	eheap_sort(hp, str_cmp);
	eheap_dmp(hp, str_dmp);

	printf("is this psorted %d\n", eheap_isheap(hp));
	printf("is this sorted %d\n", eheap_issorted(hp));
	eheap_rls(hp);
	
	return 1;
}

#ifdef USED
static int eheap_test_add(int num)
{
	int t;
	eheap_t *hp = eheap_ist(sizeof(int), 2, 0, int_cmp);

	for (t=0; t< num ; t++) {
		int rnd = intrand(1,1000);
		eheap_add(hp, &rnd);
	}

	//eheap_dmp(hp, int_dmp);
	lprintf(LOG_BLAH, "is this psorted %d\n", eheap_isheap(hp));
	lprintf(LOG_BLAH, "is this sorted %d\n", eheap_issorted(hp));
	printf("is this psorted %d\n", eheap_isheap(hp));
	printf("is this sorted %d\n", eheap_issorted(hp));

	//eheap_dmp(hp, int_dmp);
	eheap_sort(hp, int_cmp);
	//eheap_dmp(hp, int_dmp);

	printf("is this psorted %d\n", eheap_isheap(hp));
	printf("is this sorted %d\n", eheap_issorted(hp));
	eheap_rls(hp);
	
	return 1;
}

static int eheap_test_set_get(eheap_t *hp, int num)
{
	int t;

	for (t=0; t< num ; t++) {
		int rnd = intrand(1,1000);
		eheap_set(hp, &rnd, t);
		/* lprintf(LOG_ALOT, "%d,", rnd); */
	}
	/* lprintf(LOG_ALOT, "\n"); */

	eheap_sort(hp,NULL);
	

	lprintf(LOG_BLAH, "is this psorted %d\n", eheap_isheap(hp)); 
	lprintf(LOG_BLAH, "is this sorted %d\n", eheap_issorted(hp)); 

	while (hp->dap->len) {
		int which = intrand(0,hp->dap->len-1);
		if ( !eheap_isheap(hp)) return 0;
		eheap_del(hp, which);
		/* printf("is this psorted %d\n", eheap_isheap(hp));	 */
	}

	return 1;
}
#endif

/** @brief heap tests
 * internal test function 
 */
int eheap_test(int num, int wid)
{
	//eheap_t *hp = eheap_ist(sizeof(int), 5, num, int_cmp);
	//eheap_test_add(10);
	eheap_test_string(10);
	//eheap_test_set_get(hp, num);
	//eheap_rls(hp);

	return 1;
}

