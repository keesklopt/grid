#include <ego.h>

// APRIL 2008 : altered tlv by removing the value field because it's added
// as result of an arbitrary choice (it was handy for some messages)
// now ego and syncserver use 8 bytes (type,length), ask still uses 12 bytes
// (type,length,value)

// the in-core data structures stay the same, just read/write differ 4 bytes

tlv_t *tlv_ist(int t, int l, void *v)
{
	tlv_t *tp = (tlv_t *)calloc(sizeof(tlv_t),1);

	if (!v) v = (void *)calloc(l,1);

	tp->type = t;
	tp->len = l;
	tp->handle = v;
	
	return tp;
}

void tlv_rls_data(tlv_t *tp)
{
	if (!tp) return;
	if (tp->handle) free(tp->handle);
}

void tlv_rls(tlv_t *tp)
{
	if (tp) free(tp);
}

// ALLOCATES according to L
tlv_t *tlv_recv(io_t *iop, int timeout)
{
	int ret;
	tlv_t *rcvp = (tlv_t *)calloc(sizeof(tlv_t),1);

	ret = io_wait(iop, WAIT_READ, timeout);

	if (ret == 0) {
		free(rcvp);
		return NULL; // timed out !
	}

	ret = io_read_exactly(iop, (void *)rcvp, 8);
	rcvp->type = ntohl(rcvp->type);
	rcvp->len = ntohl(rcvp->len);

	if (ret == 0) {	// orderly shutdown
		free(rcvp);
		return 0;
	}
	if (ret != 8)	// error
	{
		printf("Socket: short read (%d < %d) !!!\n", ret, 8);
		free(rcvp);
		return 0;
	}

	if (rcvp->len)
	{	
		rcvp->handle = (void *)calloc(rcvp->len,1);	
		/* printf("new handle of %d\n", rcvp->len); */
		if ((ret = io_read_exactly(iop, rcvp->handle, rcvp->len)) != rcvp->len)
		{
			printf("No extended Message\n");
			return rcvp;
		}
	}

	return rcvp;
}

int tlv_send(io_t *iop, tlv_t *tp)
{
	int len, size= 8 + tp->len;
	char *both = malloc(size);
	tlv_t tmptlv;

	// in theory the len and the val could be interleaved, so send 
	// both in 1 message so : BUNDLE before sending !!
	tmptlv.type = htonl(tp->type);
	tmptlv.len = htonl(tp->len);

	memcpy(both, &tmptlv, 8);
	if (tp->len > 0) { 
		memcpy(both + 8 , tp->handle, tp->len);
	}

	if ((len = io_write(iop, both, size)) != size)
	{
		printf("could only write %d of %d bytes \n", len, 8);
	}

	free(both);
	return len;
}

int tlv_test(int n, int w)
{
	socket_t *ss =  socket_ist(SOCK_STREAM);
	io_t *iop;
	tlv_t *tp;

	socket_bind(ss, 0);

	socket_connect(ss, "telefonix", 1741);
	printf("connected ??\n");

	iop = io_ist(IO_SOCKET, ss, "rw", 0);

	tp = tlv_recv(iop,-1);
	printf("tlv %d\n", tp->len);

	return 1;
}
