/** @file
 * 
	associative array

	TODO :: net 64 bit ready

	This implements an associative array with a functional interface.
	It is a one way mapping only (key->value), but both key and value can be any type, and are therefore passed as opaque pointers (void *).

	associated array is built upon dynamic array da.c
	A note, in all parameter lists, it's always ordered KEY,VALUE.
 */

#include <ego.h>

static int aa_item_cmp(const void *L, const void *R, void *xtra)
{
	const aa_item_t *iip = L;
	const aa_item_t *iir = R;
	const cmp_fnc_t fnc = xtra;

	/* printf("comp: %s %s \n", iip->key, iir->key); */

	return fnc(iip->key,iir->key,NULL);
}

/** @brief instantiate
 *
 * instantiate an assoiciative array and return an aa_t * pointer
 * data for both key and value are copied accoring to the sizes in 
 * the l and r parameters, so for pointers pass sizeof (yourtype *)
 *
 * @param l size in bytes of the keys
 * @param r size in bytes of the values
 * @param cmp function used to compare two key's
 * @return pointer to aa_t structure
 */
aa_t *aa_ist(int l, int r, cmp_fnc_t cmp)
{
	aa_t *aap = calloc(sizeof(aa_t),1);

	aap->keywid = l;
	aap->valwid = r;
	aap->keycmp = cmp;

	aap->dap = da_ist(sizeof(aa_item_t),0);

	return aap;
}

/** @brief instantiate
 *
 * instantiate an assoiciative array and return an aa_t * pointer
 * data for both key and value are copied accoring to the sizes in 
 * the l and r parameters, so for pointers pass sizeof (yourtype *)
 *
 * @param l size in bytes of the keys
 * @param r size in bytes of the values
 * @param initial initial size of the assoicative arrat
 * @param cmp function used to compare two key's
 * @return pointer to aa_t structure
 */
aa_t *aa_ist_init(int l, int r, int initial, cmp_fnc_t cmp)
{
	aa_t *aap = calloc(sizeof(aa_t),1);

	aap->keywid = l;
	aap->valwid = r;
	aap->keycmp = cmp;

	aap->dap = da_ist(sizeof(aa_item_t),initial);

	return aap;
}

/** @brief associative array length
 *
 *  return number of elements in the associative array
 *
 * @param aap aa instance pointer
 * @return total number of elements
 */
int aa_len(aa_t *aap)
{
	if (!aap || !aap->dap) return 0;
	return da_len(aap->dap);
}

/** @brief retrieve value
 *
 * retrieve stored value associated with given key
 *
 * @param aap pointer to associative array
 * @param key key which value is searched for
 * @return value stored under 'key'
 */
void *aa_val(aa_t *aap, void *key)
{
	int pos;
	aa_item_t ai;
	aa_item_t *aip;
	ai.key = key;

	pos = da_pos(aap->dap,&ai,aa_item_cmp, aap->keycmp);
	if (pos < 0) return NULL;
	aip = da_get(aap->dap,pos);

	return aip->val;
}

/** @brief key position
 *
 * returns the physical position of the element associated with key in the
 * underlying dynamic array.
 *
 * @param aap pointer to associative array
 * @param key key which value is searched for
 * @return position in the array
 */
int aa_pos(aa_t *aap, void *key)
{
	int pos;
	aa_item_t ai;
	ai.key = key;

	pos = da_pos(aap->dap,&ai,aa_item_cmp, aap->keycmp);
	return pos;
}

/** @brief delete element
 *
 * delete array alement associated with key
 *
 * @param aap pointer to associative array
 * @param key key which value is searched for
 * @return 0 on success
 */
int aa_del(aa_t *aap, void *key)
{
	int pos;
	aa_item_t ai;
	ai.key = key;

	pos = da_pos(aap->dap,&ai,aa_item_cmp, aap->keycmp);
	if (pos < 0) return -1;
	da_del(aap->dap,pos,1);

	return 0;
}

/** @brief get array item
 *
 * get the array item, which is a struct with both key and value called aa_item_t,
 * at position pos in the array
 *
 * @param aap pointer to associative array
 * @param pos physical position in the underlying dynamic array
 * @return pointer to an aa_item_t array element structure
 */
aa_item_t *aa_get(aa_t *aap, intptr_t pos)
{
	aa_item_t *aip;

	aip =  da_get(aap->dap,pos);

	return aip;
}

/** @brief strip data
 *
 * free all data inside an associative array without deleteing the array elements.
 * You can use the associative array to store any structure or
 * pointer that you want. So just freeing up the allocated data is mostly not
 * sufficient. For instance both key and value can be pointers or structures 
 * containing pointers. So to prevent memory leakage, you can provide release
 * functions for both key and value, and call 'strip' on all members.
 * after that you can easily call aa_rls()
 *
 * @param aap pointer to associative array
 * @param rlskey function that releases the data in a key
 * @param rlsval function that releases the data in a value
 */
void aa_strip(aa_t *aap,rls_fnc_t rlskey, rls_fnc_t rlsval)
{
	int t;

	aa_item_t *aip;
	for (t=0; t< aa_len(aap); t++) {
		aip = aa_get(aap,t);
		if (rlskey) rlskey(aip->key);
		if (rlsval) rlsval(aip->val);
	}
}

/** @brief release
 *
 * free all structural data of an associative array, this means all storage 
 * of all elements and the aa_t structuire itself. If you have any references
 * in either key are value members, use aa_strip() to free that data.
 *
 * @param aap pointer to associative array
 */
void aa_rls(aa_t *aap)
{
	if (!aap) return;
	if (aap->dap) da_rls(aap->dap);

	free(aap);
}

/** @brief add or overwrite at position
 *
 * set an array element assocated with key or add it if it already exists
 * used to circumvent searching for position, so if you already know use this
 *
 * @param aap pointer to associative array
 * @param pos the physical position to put the key-val pair
 * @param a the key to add or set
 * @param b the value to add or set
 * @return 0 on success
 */
int aa_set_at(aa_t *aap, int32_t p, void *a, void *b)
{
	aa_item_t  ai;

	memcpy(&ai.key,&a,aap->keywid);
	memcpy(&ai.val,&b,aap->valwid);

	da_set(aap->dap,&ai,p);

	return 0;
}

/** @brief add or overwrite
 *
 * set an array element assocated with key or add it if it already exists
 * note his is a potential memory leak if you use references, if so use
 * aa_get() to retrieve and free the current element , then add the new one
 *
 * @param aap pointer to associative array
 * @param a the key to add or set
 * @param b the value to add or set
 * @return 0 on success
 */
int aa_set(aa_t *aap, void *a, void *b)
{
	int p;
	aa_item_t ai;

	memcpy(&ai.key,&a,aap->keywid);
	memcpy(&ai.val,&b,aap->valwid);

	p = da_pos(aap->dap,&ai,aa_item_cmp, aap->keycmp);
	if (p < 0) {
		p = da_insertpos(aap->dap, &ai, aa_item_cmp , aap->keycmp);
		da_ins(aap->dap, &ai, p,1);
		return 0;
	}
	
	da_set(aap->dap,&ai,p);

	return 0;
}

/** @brief add regardless
 *
 * add a key value pair even it if the key already exists
 * please note this function allowes double entries, and several functions
 * will only retrieve the first element found
 *
 * @param aap pointer to associative array
 * @param a the key to add or set
 * @param b the value to add or set
 * @return 0 on success
 */
int aa_add(aa_t *aap, void *a, void *b)
{
	int p;
	aa_item_t aip;

	memcpy(&aip.key,&a,aap->keywid);
	memcpy(&aip.val,&b,aap->valwid);

	p = da_insertpos(aap->dap, &aip, aa_item_cmp , aap->keycmp);
		
	da_ins(aap->dap, &aip, p,1);
	return 0;
}

/*! brief dump an item from the associative array 
 *
 */
void aa_item_dmp(intptr_t wid, const void *data, void *udata)
{
	const aa_item_t *iip = data;
	const dmp_fnc_t *fnc = udata;

	if (!fnc) {
		printf("no dump function !");
		return ;
	}

	fnc[0](0,iip->key,NULL);
	printf("=");
	fnc[1](0,iip->val,NULL);
	printf("\n");
}

/** @brief dump 
 *
 * dump all members of an associative array
 *
 * @param aap pointer to associative array
 * @param dmpa function that dumps a key
 * @param dmpb function that dumps a value
 */
void aa_dmp(aa_t *aap, dmp_fnc_t dmpa, dmp_fnc_t dmpb)
{
	dmp_fnc_t fnc[2];

	fnc[0] = dmpa;
	fnc[1] = dmpb;
	da_dmp(aap->dap, aa_item_dmp, fnc);
}

static int aa_test_del(int len, int wid)
{
	intptr_t t, ret=1;
	aa_t *aap;
	aa_item_t *aip;

	char *left; 
	char *fp;
	intptr_t right;

	aap = aa_ist(sizeof(char *), sizeof(int), str_cmp);

	for (t=0; t< len; t ++) {
		left = strrand(wid);
		right = intrand(0,wid);
		printf("Adding %s (%p) with %d\n", left, left, (int)right);
		aa_set(aap, left, (void *)right);
		aa_dmp(aap,str_dmp,int_abuse_dmp);
	}

	ret = (intptr_t)aa_val(aap,"jhebeywte");
	printf("found %d\n", (int)ret);

	ret = aa_pos(aap,"jhebeywte");
	aip = aa_get(aap, ret);
	fp = aip->key;
	printf("freeing %s (%p)\n", fp, fp);
	aa_del(aap,"jhebeywte");

	aa_dmp(aap,str_dmp,int_abuse_dmp);
	ret = (intptr_t)aa_val(aap,"jhebeywte");
	printf("found %d\n", (int)ret);

	printf("freeing %s (%p)\n", fp, fp);
	free(fp);
	aa_strip(aap,str_rls, NULL);
	aa_rls(aap);

	return 1;
}

static int aa_test_str_int(int len, int wid)
{
	intptr_t t,ret=1;
	aa_t *aap;

	char *left; 
	intptr_t right;

	aap = aa_ist(sizeof(char *), sizeof(int), str_cmp);

	for (t=0; t< len; t ++) {
		left = strrand(wid);
		right = intrand(0,wid);
		printf("Adding %s with %d\n", left, (int)right);
		aa_set(aap, (void *)left, (void *)right);
		/* aa_set(aap, left, (void *)right); */
		aa_dmp(aap,str_dmp,int_abuse_dmp);
	}

	ret = (intptr_t)aa_val(aap,"jhebeywte");
	printf("found %d\n", (int)ret);

	aa_strip(aap,str_rls, NULL);
	aa_rls(aap);

	return 1;
}

static int aa_test_plain(void)
{
	aa_t *aap;

	aap = aa_ist(sizeof(char *), sizeof(int), str_cmp);

	aa_add(aap, "deze string", (void *)11);
	aa_dmp(aap,str_dmp,int_abuse_dmp);

	aa_rls(aap);

	return 1;
}

/** test entry
  */
int aa_test(int len, int wid)
{
	int ret=1;

	len = 10;
	wid = 10;

	ret = aa_test_plain();
	ret &= aa_test_str_int(len,wid);
	ret &= aa_test_del(len,wid);

	return ret;
}
