#include <ego.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#include <sys/unistd.h>
#include <sys/param.h>
#endif

/* this was made to send simple mails through an installed smtp 
	server without the need for a mailer daemon
	so it does NOT implement the whole smtp protocol
	additions are simple though
*/

smtp_t *smtp_ist(char *server, short port)
{
	smtp_t *sp = (smtp_t *)calloc(sizeof(smtp_t),1);

	if (server) sp->server = strdup(server);
	if (port < 1) port = 25;
	sp->port = port;
	sp->rcpts = da_ist(sizeof(char *), 0);
	sp->hdrs = da_ist(sizeof(tuple_t), 0);
	sp->msgbuf = buf_ist(0);

	return sp;
}

void smtp_rls(smtp_t *sp)
{
	if (!sp) return;

	if (sp->rcpts) da_rls(sp->rcpts);
	if (sp->hdrs) smtp_hdrs_rls(sp);
	if (sp->server) free(sp->server);
	if (sp->msgbuf) buf_rls(sp->msgbuf);

	free(sp);
}

static int tuple_cmp(const void *a, const void *b, void *dummy)
{
	tuple_t *A = (tuple_t *)a;
	tuple_t *B = (tuple_t *)b;

	return strcmp(A->key, B->key);
}

void smtp_hdrs_rls(smtp_t *sp)
{
	int i;

	if (!sp) return;
	if (!sp->hdrs) return;

	for (i=0; i< da_len(sp->hdrs); i++) {
		tuple_t *hdr;
		hdr = da_get(sp->hdrs, i);
		free ((*hdr).key);
		free ((*hdr).val);
	}
	da_rls(sp->hdrs);
}

// yes, you can put any header you want, its up to you 
// overwrites same headers 
int smtp_add_hdr(smtp_t *sp, char *hdr, char *val)
{
	int l, p;
	tuple_t tuple, *tp;

	if (!sp || !hdr || !val) return err_set(ERR_PARM);

	l = strlen(hdr)-1;

	tuple.key = strdup(hdr);
	// very thin check for ":"
	if (tuple.key[l] == ':') tuple.key[l] = '\0';
	tuple.val = strdup(val);

	p = da_pos(sp->hdrs, &tuple, tuple_cmp,NULL);
	if (p < 0) {
		da_add(sp->hdrs, &tuple);
	} else {
		tp = da_get(sp->hdrs, p);
		if (tp->val) free(tp->val);
		tp->val = strdup(val);
	}
	return 0;
}

int smtp_add_rcpt(smtp_t *sp, char *rcpt)
{
	if (!sp || !sp->rcpts || !rcpt) return err_set(ERR_PARM);

	da_add(sp->rcpts, &rcpt);	return 0;
}

// add one line to the message buffer
void smtp_add_msg(smtp_t *sp, char *msg)
{
	buf_add(sp->msgbuf, msg);
}

void smtp_set_msg(smtp_t *sp, char *msg)
{
	buf_clear(sp->msgbuf);
	smtp_add_msg(sp,msg);
}


#define BLEN 256

int smtp_expect(smtp_t *sp, io_t *iop, buf_t *bp, char *expected, char *altexpect)
{
	size_t ret;
	char *tok;
	char *buf;
	char readbuf[BLEN];

	if (!bp) return err_set(ERR_PARM);

	buf = buf_strdup(bp);
	//printf("put: %s\n", buf);
	io_write(iop, buf, strlen(buf));
	free(buf);

	ret = io_wait(iop, WAIT_READ, 0);
	memset(readbuf, '\0', BLEN);
	ret = io_read(iop, &readbuf[0], BLEN);

	if (ret < strlen(expected)) {
		return err_set(ERR_SMTP_LENGTH);
	}
	readbuf[ret] = '\0';

	//printf("got: %s\n", readbuf);
	tok = strtok(readbuf, " ");
	if (strcmp(tok, expected) !=0 && 
		(!altexpect || strcmp(tok,altexpect) !=0) ) {
		err_set_string(ERR_SMTP_UNEXPECTED, "Did not receive expected %s code\n", expected);
		return err_set(ERR_SMTP_UNEXPECTED);
	}

	return 0;
}

int smtp_send(smtp_t *smp)
{
	int t, rval=0;
	char myname[MAXHOSTNAMELEN];
	char *msgp;
	buf_t *buf;
	socket_t *sp;
	io_t *iop;
	char from[1024];

	gethostname(myname, MAXHOSTNAMELEN);

	sp = socket_ist(SOCK_STREAM);
	socket_sethostbyname(sp, NULL, 0);
	socket_connect(sp, smp->server, smp->port);
	iop = io_ist(IO_SOCKET, sp, "rw", 0);
	buf = buf_ist(0);
	
	// this seems to be needed to avoid 554 error messages
	sleep(1);

	//  simple smtp session  
	// should be EHLO, BUT older servers use HELO, AND 
	// newer server are forced to support HELO, so HELO 
	// has an overall higher success percentage !
	buf_add(buf, "HELO ");
	buf_add(buf, myname);
	buf_add(buf, "\n");
	if (smtp_expect(smp, iop, buf, "220", NULL)) goto cleanup;
	buf_clear(buf);

	buf_add(buf, "MAIL FROM: ");
	sprintf(from, " <no-reply@%s>", myname);
	buf_add(buf, from);
	buf_add(buf, "\n");
	if (smtp_expect(smp, iop, buf, "250", NULL)) goto cleanup;
	buf_clear(buf);
	
	for (t=0; t< da_len(smp->rcpts); t++) {
		char **to;
		to = da_get(smp->rcpts, t);
		buf_add(buf, "RCPT TO:");
		buf_add(buf, *to);
		buf_add(buf, "\n");
		if (smtp_expect(smp, iop, buf, "250", NULL)) {
			printf("Skipping recipient %s", err_string());
		}
		buf_clear(buf);
	}

	buf_add(buf,"DATA\n");
	if (smtp_expect(smp, iop, buf, "354", "250")) goto cleanup;
	buf_clear(buf);

	for (t=0; t< da_len(smp->hdrs); t++) {
		tuple_t *hdr;
		hdr = da_get(smp->hdrs, t);
		buf_add(buf, (*hdr).key);
		buf_add(buf, ":");
		buf_add(buf, (*hdr).val);
		buf_add(buf, "\n");
	}

	msgp = buf_strdup(smp->msgbuf);
	buf_add(buf, msgp);
	buf_add(buf, "\n.\n");
	free(msgp);
	if (smtp_expect(smp, iop, buf, "250", NULL)) goto cleanup;
	buf_clear(buf);

	buf_add(buf,"QUIT\n");
	if (smtp_expect(smp, iop, buf, "250", NULL)) goto cleanup;
	buf_clear(buf);

	fflush(stdout);

	rval = -1;
cleanup:
	if (iop) io_rls(iop);
	if (buf) buf_rls(buf);
	if (sp) socket_rls(sp);
	return rval;
}

int smtp_test(int w, int h)
{
	smtp_t *sp;
	//sp = smtp_ist("smtp.luna.net", 25);
	sp = smtp_ist("localhost", 25);
	//sp = smtp_ist("dynamix-177.almende.net", 25);

	smtp_add_rcpt(sp, "kees@almende.org");
	//smtp_add_rcpt(sp, "kees@dynamix-177.almende.net");
	/* smtp_add_rcpt(sp, "hareuh@hotmail.com"); */
	smtp_add_hdr(sp, "Subject:", "iets");
	smtp_add_hdr(sp, "Subject:", "twiets");
	smtp_add_hdr(sp, "From", "kees@almende.org");
	smtp_set_msg(sp,"start new message\n");
	smtp_add_msg(sp, "Ok das weer een nieuwe regel\n");
	smtp_send(sp);
	/* smtp_add_msg(sp,"nog een regel erbij\n"); */
	/* smtp_send(sp); */
	/* smtp_set_msg(sp,"verse message\n"); */
	/* smtp_send(sp); */
	smtp_rls(sp);

	return 1;
}
