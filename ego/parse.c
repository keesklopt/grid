#include <ego.h>

/* DON'T try to make a real parser (NFA/DFA.. etc) for one it will just be 
	SLOWER, so there is no reason left. 
*/

void parse_rls(parse_t *pp)
{
	if (!pp) return;

	io_rls(pp->inp);
	free(pp);
}

parse_t * parse_ist(io_t *iop)
{
	parse_t *pp = (parse_t *)calloc(sizeof(parse_t),1);

	pp->inp = iop;

	return pp;
}

void parse_add_context(parse_t *pp, pcontext_t *parent, pcontext_t *child)
{
	/* printf("Jo \n"); */
}

void context_rls (pcontext_t *ctp)
{
	if (!ctp) return;

	free(ctp);
}

pcontext_t *context_ist(char *before, char *start, char *end, char *after)
{
	pcontext_t *cp = (pcontext_t *)calloc(sizeof(pcontext_t),1);

	return cp;
}

int parse_it(parse_t *pp)
{
	char k[2] = {0};
	io_rewind(pp->inp);

	while (io_read_exactly(pp->inp, &k[0], 1)) {
		/* printf("->%s\n", k); */;
	}

	return 0;
}

int parse_test(int len, int wid)
{
	io_t *iop;
	parse_t *pp;
	pcontext_t *filectxt, *section;
	pcontext_t *comment, *string, *anything;

	iop = io_ist(IO_FP, "parsefile.txt", "r", 0);

	pp = parse_ist(iop);	

	// first instantiate all contexts, so you can build the 
	// hierarchy top-down, and to enable recursion

	filectxt = context_ist(NULL, NULL, NULL, "eof");
	comment = context_ist("\n", ";", NULL, "\n");
	section = context_ist(NULL, "[", NULL, "\n[");
	string = context_ist( NULL, "\"", "\"", NULL);
	anything = context_ist(NULL, NULL, NULL, NULL);

	// build the hierarchy
	parse_add_context(pp, NULL, filectxt);

	parse_it(pp);

	parse_rls(pp);

	context_rls(filectxt);
	context_rls(comment);
	context_rls(section);
	context_rls(string);
	context_rls(anything);

	return 1;
}
