/** @file 
	index and inverse index 
	
<pre>
	this is an implementation of a generic index, a double redirection index
	to use with an array of any data.
	indices consist of two dynamic arrays (da.c). 
	one array contains the forward mapping (positions) the 
	other one the reverse (indices)

	positions[x] = y;
	indices[y] = x;

	idx.c on it's own wil probably not be of much use to you, use indexed array
	ida.c for maintaining indices on actual data
</pre>	
*/

#include <ego.h>
#include <sys/timeb.h>
#include <stdlib.h>

/** @brief (re)number the index
 * number or renumber an error setting entry idx[x] to x
 *
 * @param ip pointer to the index object
 */
void idx_number(idx_t *ip)
{
	unsigned int t;

	for (t=0; t< ip->pos->len; t++) {
		da_set(ip->pos, (void *)&t, t);
	}
}

/** @brief duplicate the index
 * make a copy of an index and duplicate all it's data
 *
 * @param orig pointer to the source index object
 * @param udata user data to be set on the index
 */
idx_t *idx_dup(idx_t *orig, void *udata)
{
	idx_t *ip = (idx_t *)calloc(sizeof(idx_t),1);

	memcpy(ip,orig,sizeof(idx_t));
	ip->idx = da_dup(orig->idx);
	ip->pos = da_dup(orig->pos);
	ip->udata = udata;

	return ip;
}

/** @brief instantiate index
 * create a new index object and set up some substructures
 *
 * @param len length of the indices
 * @param cmp compare function to use when sorting
 * @param udata optional user data to pass to compare function
 * @return pointer to the new index object
 */
idx_t *idx_ist(int len,cmp_fnc_t cmp, void *udata)
{
	idx_t *ip = (idx_t *)calloc(sizeof(idx_t),1);

	ip->idx = da_ist(sizeof(int),len);
	ip->pos = da_ist(sizeof(int),len);
	ip->cmp = cmp;
	ip->udata = udata;

	idx_number(ip);

	return ip;
}

/** @brief test sortedness
 * test and set if this index is sorted with it's compare function
 *
 * @param ip pointer to the source index object
 * @return 1 if sorted, 0 if not
 */
int idx_issorted(idx_t *ip)
{
	ip->sorting = 1;
	return da_issorted(ip->pos,ip->cmp,ip);
}

/** @brief invert index
 * invert the index according to the pos index
 * OKT 2009 Kees, changed this to void function since it's return value
 * was not used, old prototype was :
  idx_t *idx_invert(idx_t *ip) 
 * 
 * @param ip pointer to the source index object
 */
void idx_invert(idx_t *ip)
{
	int t;
	int p;

	for (t=0; t< da_len(ip->pos); t++) {
		p = *(int *)da_get(ip->pos, t);
		da_set(ip->idx, (void *)&t, p);
	}
}

#ifdef USE_SWAP
static void swap(char *a, char *b, sort_t *sp)
{ 
	/*printf("swap %d and %d\n", *(int *)a, *(int*)b); */
	memcpy(sp->tmp, a, sp->wid);
	memcpy(a, b, sp->wid);
	memcpy(b, sp->tmp, sp->wid);
}
#endif

/** @brief release index
 *
 * release the index object and all it's substructures
 *
 * @param ip pointer to an index object
 */
void idx_rls(idx_t *ip)
{
	if (!ip)return;
	if (ip->idx) da_rls(ip->idx);
	if (ip->pos) da_rls(ip->pos);

	/* printf("IP FREE: %p\n", ip); */
	free(ip);
}

/** @brief sort ascending
 *
 * sort an index lowest first
 *
 * @param left left object to compare
 * @param right right object to compare
 * @param udata user data pointer passed 
 * @return -1 if left smaller than right, 1 if right < left, 0 if equal
 */
int ascending_srt(const void *left, const void *right, void *udata)
{
	idx_t *idp = (idx_t *)udata;
	da_t *dap= (da_t *)idp->udata;
	int a= *(int *)left;
	int b= *(int *)right;
	int ra,rb;

	/* when bsearching, the key is NOT redirected !! */
	if (idp->sorting)  
		ra = *(int *)da_get(dap, a);
	else 
		ra = a;
	
	rb = *(int *)da_get(dap, b);

	/* printf("test %d against %d\n", ra, rb); */

	if (ra < rb) return -1;
	if (ra > rb) return  1;

	return 0;
}

/** @brief sort descending
 *
 * sort an index highest first
 *
 * @param left left object to compare
 * @param right right object to compare
 * @param udata user data pointer passed 
 * @return -1 if left bigger than right, 1 if right > left, 0 if equal
 */
int descending_srt(const void *left, const void *right, void *udata)
{
	idx_t *idp = (idx_t *)udata;
	da_t *dap= (da_t *)idp->udata;
	int a= *(int *)left;
	int b= *(int *)right;
	int ra,rb;

	/* when bsearching, the key is NOT redirected !! */
	if (idp->sorting)  
		ra = *(int *)da_get(dap, a);
	else 
		ra = a;
	
	rb = *(int *)da_get(dap, b);

	/* printf("test %d against %d\n", ra, rb); */

	if (ra < rb) return  1;
	if (ra > rb) return -1;

	return 0;
}

/** @brief find insert position
 *
 * find the position where the given element should be inserted in the index
 *
 * @param ip pointer to an index object
 * @param data pointer to an int containing the value to be inserted
 * @return the place where the item would be inserted 
 */
int idx_insertpos(idx_t *ip, void *data)
{
	ip->sorting = 0;
	return da_insertpos(ip->pos, data, ip->cmp, ip);
}

/** @brief find position
 *
 * find the position where the given element resides in the index
 *
 * @param ip pointer to an index object
 * @param data pointer to an int containing the value to be found
 * @return the place where the item is in the index
 */
int idx_pos(idx_t *ip, void *data)
{
	ip->sorting = 0;
	return da_pos(ip->pos, data, ip->cmp, ip);
}

/** @brief sort index
 *
 * sort the index according to it's own compare function (given in idx_ist)
 *
 * @param ip pointer to an index object
 */
void idx_sort(idx_t *ip)
{
	ip->sorting = 1;
	da_sort(ip->pos,ip->cmp, ip);
	ip->sorted = 1;
}

/** @brief get index
 *
 * get the value of the index at position pos
 *
 * @param ip pointer to an index object
 * @param pos the position in the index to retrieve
 * @return what is at position pos
 */
int idx_index(idx_t *ip, int pos)
{
	char *p = da_get(ip->pos, pos);
	return *(int *)p;
}

/** @brief get inverse
 *
 * get the value of the inverse index at position pos
 *
 * @param ip pointer to an index object
 * @param pos the position in the inverse index to retrieve
 * @return at which position is pos
 */
int idx_inv(idx_t *ip, int pos)
{
	char *p = da_get(ip->idx, pos);
	if (!p) return 0;
	return *(int *)p;
}

/** @brief dump with index
 *
 * dump data in the indexed order
 *
 * @param ip pointer to an index object
 * @param dmp function to dump one element
 * @param dap ppinter to a dynamic array containg the data
 * @return ERR_NONE
 */
int idx_dmp_da(idx_t *ip, dmp_fnc_t dmp, da_t *dap)
{
	unsigned int t;
	int *p;
	char *dp;

	lprintf(LOG_ALOT, "{");
	for (t=0; t< ip->pos->len; t++) {
		if (t != 0) lprintf(LOG_ALOT, ",");
		p = (int *)da_get(ip->pos, t);

		dp = da_get(dap, *p);
		dmp(dap->wid,dp,NULL);
	}
	lprintf(LOG_ALOT, "}\n");

	return ERR_NONE;
}

/** @brief update after addition
 * when an item is added , no complete reindex is needed, just :
	step 1: shift all values behind pos
	step 2: up all values higher than pos
 * @param ip pointer to the index object
 * @param pos position of the addition
 * @return ERR_NONE
 */
int idx_recalc_added(idx_t *ip, unsigned int pos)
{
	unsigned int t;
	unsigned int tmp=pos; 

	da_ins(ip->pos,&tmp,ip->npos,1);
	da_ins(ip->idx,&tmp,ip->npos,1);

	for (t=0;t<ip->pos->len;t++) {
		tmp = *(int *)da_get(ip->pos,t);
		if (tmp>=pos && t!=ip->npos) tmp++;
		da_set(ip->pos,&tmp,t);
		da_set(ip->idx,&t,tmp);
	}

	return ERR_NONE;
}

/** @brief update after deletion
 * when an item is deleted , no complete reindex is needed, just :
 * step 1 shift values behind pos nbackwards 
   step 2 : lower all values higher than pos

 * @param ip pointer to the index object
 * @param pos position of the deletion
 * @return ERR_NONE
 */
int idx_recalc_deleting(idx_t *ip, int pos)
{
	unsigned int t;
	int tmp=pos; 

	da_del(ip->pos,ip->npos,1);
	t = idx_inv(ip,ip->npos);
	da_del(ip->idx,t,1);

	for (t=0;t<ip->pos->len;t++) {
		tmp = *(int *)da_get(ip->pos,t);
		if (tmp>=pos) tmp--;
		da_set(ip->pos,&tmp,t);
		da_set(ip->idx,&t,tmp);
	}

	return ERR_NONE;
}

/** @brief reindex
 * completely reindex the index
 * @param ip pointer to the index object
 * @return ERR_NONE
 */
int idx_reindex(idx_t *ip)
{
	int t;
	int last,tmp;
	int step;
	int io, in;

	io = idx_inv(ip, ip->opos);
	/* in = idx_inv(ip, ip->npos); */
	in = ip->npos;

	/* printf("--- %d to %d\n", ip->opos, ip->npos); */
	/* printf("--- %d to %d\n", io,in); */
	if (io>in) step=1;
	else step=-1;

	last = ip->opos;
	for (t=in;t!=io;t+=step) {
		tmp = *(int *)da_get(ip->pos,t);
		da_set(ip->pos,&last,t);
		/* printf("walkie %d (%d -> %d\n", t, tmp, last); */
		last = tmp;
	}
	da_set(ip->pos,&last,t);

	return ERR_NONE;
}

/** @brief dump index
 * dump the values in the index and the reverse index
 * @param ip pointer to the index object
 * @return ERR_NONE
 */
int idx_dmp(idx_t *ip)
{
	unsigned int t;
	int *p;

	lprintf(LOG_ALOT, "{");
	for (t=0; t< ip->pos->len; t++) {
		if (t != 0) lprintf(LOG_ALOT, ",");
		p = (int *)da_get(ip->pos, t);
		lprintf(LOG_ALOT, "%d", *p);
	}
	lprintf(LOG_ALOT, "}\n");
	lprintf(LOG_ALOT, "inverse : \n");
	lprintf(LOG_ALOT, "{");
	for (t=0; t< ip->idx->len; t++) {
		if (t != 0) lprintf(LOG_ALOT, ",");
		p = (int *)da_get(ip->idx, t);
		lprintf(LOG_ALOT, "%d", *p);
	}
	lprintf(LOG_ALOT, "}\n");

	return ERR_NONE;
}

static int idx_test_sort(int len)
{
	int t;
	da_t *dap =da_ist(sizeof(int),len);
	idx_t *idx = idx_ist(len, ascending_srt, dap);
	idx_t *idx2 = idx_ist(len, descending_srt, dap);

	for (t=0; t< len; t++) {
		int n = intrand(1,100);
		da_set(dap, (void *)&n, t);
	}

	idx_sort(idx);
	idx_sort(idx2);
	idx_dmp_da(idx,int_dmp,dap);
	idx_dmp_da(idx2,int_dmp,dap);

	idx_rls(idx);
	idx_rls(idx2);
	da_rls(dap);

	return ERR_NONE;
}

/* test section */
static int idx_test_dup(int num, int len)
{
	int t;
	da_t *dap =da_ist(sizeof(int),len);
	idx_t *ix = idx_ist(0,ascending_srt, dap);
	idx_t *c;


	for (t=0; t< num; t++) {
		c = idx_dup(ix,dap);
		idx_rls(ix);
		ix = c;
	}

	idx_rls(ix);
	da_rls(dap);

	return 1;
}

static int idx_test_speed(int len)
{
	int t;
	time_t tm1, tm2, tm3;
	int *arr=calloc(sizeof(int),len);
	da_t *dap =da_ist(sizeof(int),len);

	for (t=0; t< len; t++) {
		int n = intrand(1,100000);
		arr[t] = n;
		da_set(dap, (void *)&n, t);
	}
	tm1 = time(NULL);
	da_sort(dap, int_cmp,NULL);
	tm2 = time(NULL);
	da_sort(dap, int_cmp,NULL);
	tm3 = time(NULL);

	printf("qsort is %d, sort is %d\n", (int)tm2-(int)tm1, (int)tm3-(int)tm2);

	free(arr);
	da_rls(dap);

	return ERR_NONE;
}

/** @brief test function 
 * internal testing
 */
int idx_test(int num, int wid)
{
	idx_test_speed(100);
	idx_test_sort(100);
	idx_test_dup(num,wid);

	return 1;
}
