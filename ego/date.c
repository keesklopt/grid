#include <ego.h>

/* NOT threadsafe: localtime uses static memory and localtime_r is 
	not supported on windows i tried to mark critical sections with
	BLOCK and UNBLOCK if you want to make it threadsafe
*/

/* just because time_t allready exists! */
/* this is purely a set of convenience functions  */
date_t *date_ist(time_t stamp)
{
	date_t *dp = (date_t *)calloc(sizeof(date_t),1);

	dp->time = stamp;
	date_tm(dp);

	return dp;
}

void date_set_now(date_t *dp)
{
	dp->time = time(NULL);
	date_tm(dp);
}

date_t *date_ist_now(void)
{
	return date_ist(time(NULL));
}

struct tm *date_tm(date_t *dp)
{
	struct tm tmp;

	localtime_s(&tmp,&dp->time);
	memcpy(&dp->tm, &tmp,sizeof(struct tm));

	return &dp->tm;
}

time_t date_time(date_t *dp)
{
	dp->time = mktime(&dp->tm);
	return dp->time;
}

void date_add(date_t *which, date_t *what)
{
	
}
