#ifdef __cplusplus
extern "C" {
#endif
/*! @mainpage libego

Libego is a library of general purpose utilities and data structures, mainly used by it's author, hence the name 'ego'. Another way of explaining the name is the way it appears on the gcc command line: -lego, because it is built up of small blocks built upon eachother.
*/
/** @file ego.h
  * data structures and constants
  */
#ifndef _EGO_H_INCLUDED_
#define _EGO_H_INCLUDED_

#include <stdio.h>
#ifndef WIN32
#include <stdint.h>

#include <hmac.h>
#include <memxor.h>
#include <sha1.h>
#include <base64.h>

/*! @brief microsoft compatible strtok */
#define strtok_s(d,l,c) strtok(d,l)

/*! @brief microsoft compatible ctime */
#define ctime_s(a,b,c) ctime_r(c,a)
/*! @brief microsoft compatible fopen */
#define fopen_r(a,b,c) (a = fopen(b,c))
/*! @brief microsoft compatible open */
#define _open open
/*! @brief microsoft compatible close */
#define _close close
/*! @brief microsoft compatible read */
#define _read read
/*! @brief microsoft compatible write */
#define _write write
/*! @brief microsoft compatible lseek */
#define _lseek lseek
/*! @brief microsoft compatible localtime_r */
#define localtime_s(a,b) localtime_r(b,a)

#else

int snprintf (char *str, int n, char *fmt, ...);
//#define snprintf sprintf_s
// MS also redifines this, but complains about strncat, i'm fed up with this 
// im using _CRT_SECURE_NO_WARNINGS

//#define strncat(s,d,l) strcat_s(s,l,d)
#define fopen_r(a,b,c) ( fopen_s(&a,b,c) == 0 )
//#define strncpy(d,s,l) strcpy_s(d,l,s)
#define sscanf sscanf_s
//#define snprintf sprintf_s


void StartSockets(void);
#endif
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <limits.h>
#include <time.h>


#ifndef BUILDING_PROTO
#ifdef WIN32
#include <winsock2.h>
#include <wincrypt.h>

#define MAXHOSTNAMELEN 64
#else
#include <sys/socket.h>
#include <arpa/inet.h>

#endif
#endif

/*! @brief maximum */
#define EGO_MAX(A,B) (A>B)?A:B
/*! @brief minimum */
#define EGO_MIN(A,B) (A<B)?A:B

/* just a struct to use for testing */
/*! @brief generic test structure */
typedef struct test_tag {
	char c;			/*!< charretje */
	int i;			/*!< intje  */
	char *str;		/*!< stringetje */
	char carr[5];	/*!< stringetje */
} test_t;

/* ok, why do you allow this then? */
/*! @brief some is C workaround */
typedef struct iso_void_tag {
	void *iso_sucks;	/*!< but what for ? */
} iso_void;

/*! time pattern */
typedef struct tpat_tag tpat_t;

/* standard compare and dump function prototypes */
/*! compare function prototype */
typedef int (* cmp_fnc_t)(const void *, const void *, void *);
/*! dump function prototype */
typedef void (* dmp_fnc_t)(intptr_t, const void *, void *);
/*! tostring function prototype */
typedef char *(* str_fnc_t)(void *);
/*! release function prototype */
typedef void (* rls_fnc_t)(void *data);
/*! void function prototype */
typedef void (* void_fnc_t)(void *data);
/*! generic function prototype */
typedef void *(* generic_fnc_t)(void *data);
/*! unit test function prototype */
typedef int (* tst_fnc_t)(int,int);
/*! thread function prototype */
typedef void *(* thread_fnc_t)(void *);
/*! permutation function prototype */
typedef int (*prm_fnc_t)(void **, int, void *);
/*! has function prototype */
typedef int (* hash_fnc_t)(const void *);

/* ---- object.c ---- */
/*! @brief generic object */
typedef struct object_tag {
	char *name;		/*!< object name */
	rls_fnc_t rls;	/*!< how to realease object */
	cmp_fnc_t cmp;	/*!< how to compare two objects */
	dmp_fnc_t dmp;	/*!< dump object */
	void *udata;	/*!< xtra user data */
} object_t;

/*! null object */
extern object_t null_object;
/*! unsorted object */
extern object_t unsorted_object;
/*! integer object */
extern object_t int_object;
/*! integer character object */
extern object_t intchar_object;
/*! integer pointer object */
extern object_t intp_object;
/*! interval object */
extern object_t iv_object;
/*! interval set object */
extern object_t ivset_int_object;
/*! string object */
extern object_t str_object;


/* ---- buf.c ---- */
/*! @brief buffer full flag */
#define BUF_FULL 0x0001
/*! @brief dynamic byte buffer */
typedef struct buf_tag {
    intptr_t bytes;	/*!< number of bytes in buffer */
	char *cur; 		/*!< current position, MIND REALLOCS !! */
    char *data;		/*!< base data pointer, can change through reallocs !!*/
} buf_t;

/* da.c */
/*! @brief dynamic array */
typedef struct da_tag {
	/*! number of elements currently in the array */
	unsigned int len;
	/*! is array sorted ?*/
	unsigned int sorted : 1;	
	/*! width in bytes of each array element */
	unsigned int wid 	: 31;
	/*! underlying dynamic buffer */
	buf_t *bufp;
} da_t;

/*! @brief sorting helper struct */
typedef struct sort_tag
{
	void *handle;	/*!< helper data */
	int wid;		/*!< with of an element */
	char *tmp;		/*!< temporary pointer */
} sort_t;

/* set.c */
/*! set operations */
enum operations { 
	NOOP, 
	UNION, 
	INTERSECTION, 
	REPEAT, 
	SUBTRACT,
	NOT 
};

/*! @brief data set */
typedef struct set_tag
{
	/*! internal position index */
	int  pos;
	/*! base dynamic array */
	da_t *arr;
	/*! element compare function */
	cmp_fnc_t cmp;
} set_t;

/*! @brief empty sign */
#define B_EMPTY 248
/*! @brief epsilon sign */
#define EPS 162

/* ---- permute.c ---- */

/*! permutation operations */
enum perm_e { PERM_NUMBER, PERM_PERMUTE, PERM_COMBINE };

/*! @brief permutation strycture */
typedef struct prm_tag {
	/*! width of 1 element in vals */
	int wid;
	/*! permutation type see perm_e */
	int type;
	/*! array of all values */
	da_t *vals;
	/*! sequence array */
	int *seq;
} prm_t;

/* ---- list.c ---- */
/*! @brief first position in list */
#define LST_FIRST 	0 
/*! @brief before item in list */
#define LST_BEFORE  -1
/*! @brief after item in list */
#define LST_AFTER   -2
/*! @brief last position in list */
#define LST_LAST	-3
/*! @brief this position in list  */
#define LST_THIS    -4

/*! single list type */
typedef struct litem_tag litem_t;
/*! single list element */
struct litem_tag {
	void *data;		/*!< list element content */
	litem_t *next;	/*!< next list element */
}; 

/*! @brief single linked list */
typedef struct lst_tag {
	/*! total number of elements */
	int len;
	/*! first element */
	litem_t head;
} lst_t;


/*! double linked list type */
typedef struct ditem_tag ditem_t;
/*! doubly linked list item */
struct ditem_tag {
	void *data;			/*!< item data */
	ditem_t * next;		/*!< next item */
	ditem_t * prev;		/*!< previous item */
} ;

/*! @brief double linked list */
typedef struct dlst_tag {
	/*! length administration */
	int len;
	/*! first element in the list */
	ditem_t head;
	/*! last element in the list */
	ditem_t tail;
	/*! object stored in each list element */
	object_t *op; 
} dlst_t;

/*! doubly linked list item function prototype */
typedef void (* ditem_fnc_t)(dlst_t *, ditem_t *, void *udata);

/*! node type */
typedef struct enode_tag enode_t;
/*! edge type */
typedef struct eedge_tag eedge_t;

/* ---- graph.c ---- */
/*! @brief graph edge or connection */
struct eedge_tag {
	void *udata;	/*!< edge data */
	/* enode_t *from; */
	enode_t *to;		/*!< to node */
};

/*! @brief graph node */
struct enode_tag {
	/*! dynamic array of edges leaving this node */
	da_t *edges;
	/*! status of node */
	unsigned int status : 5;
	/*! for loop detection */
	unsigned int visited : 1;
	/*! future use or padding */
	unsigned int rest   : 26;
	/*! user data */
	void *udata;
};

/*! @brief network graph */
typedef struct graph_tag {
	dlst_t *nodes;	/*!< list o nodes */
	dlst_t *edges;	/*!< list o edges */
	enode_t *entry;	/*!< entrance */
	enode_t *exit;	/*!< exit */
	object_t *nob;	/*!< node object description */
	object_t *eob;	/*!< edge objecv description */
} graph_t;

/* ---- fifo.c ---- */
/*! @brief fifo or queue */
typedef struct fifo_tag
{
	dlst_t *lst;	/*!< underlying list */
	ditem_t *cur;	/*!< current item */
} fifo_t;

/* ---- lifo.c ---- */
/*! @brief lifo or stack */
typedef struct lifo_tag
{
	dlst_t *lst;	/*!< stack is based on a doubly linked list */
	ditem_t *cur;	/*!< current dlist item */
} lifo_t;

/* ---- rbuf.c ---- */
/*! @brief ring buffer */
typedef struct rbuf_tag {
	buf_t *bufp;	/*!< underlying dynamic buffer */
	int lo;			/*!< lo water */
	int hi;			/*!< hi water */
	int eof; 		/*!< end of file */
} rbuf_t;

/* ---- err.c ---- */
/*! @brief max error message length */
#define ERRMSG_MAX 128

/*! @brief tuple error number and message */
typedef struct err_tag {
	/*! error number */
    int err_no;
	/*! error message */
    char msg[ERRMSG_MAX];
} err_t;

/* use if you need more than the global error space def_err
	for instance, when you are threading */
/*! error space (set of new errors) */
typedef struct errspace_tag {
	/*! errno for this errorspace */
    int err_no; 
	/*! list of err_tag tuples */
	da_t *errors;
} errspace_t;

/*! always positive!!, reserve negative numbers for errors 
in the error handling itself ! */
enum err_num_e {
/* general internal program errors */
    ERR_NONE=0x00,
    ERR_MEM,
    ERR_PARM,
    ERR_NULL,
    ERR_RANGE,

/* object errors */
    ERR_TREE_CMP,

    ERR_IO_OPEN,

    ERR_SOCK_CONNECT,

	ERR_TOK_INIT,

	ERR_AUTH_LOGIN,

    ERR_SMTP_LENGTH, 
    ERR_SMTP_UNEXPECTED, 

/* external program errors */
    ERR_CURL,
    ERR_XML2,
    ERR_LIBC,

/* always leave this last !! */
    ERR_LAST
};

/*! default error space */
extern errspace_t def_err;

/* ---- io.c ---- */
/*! color value */
enum colortns_e { IO_DARKGREY=0, IO_RED, IO_GREEN, IO_YELLOW, IO_BLUE, IO_ORANGE, IO_CYAN, IO_WHITE, IO_PURPLE, IO_LILA, IO_NORMAL};
/*! terminal escape code for coloring */
#define COLOR "\033[%d;%dm"


/*! @brief is this a resizable io medium */
#define IO_RESIZABLE    0xFFFFFFFF

/*! @brief file pointer */
#define IO_FP 			0x00
/*! @brief file descriptor */
#define IO_FD 			0x01
/*! @brief dynamic buffer (buf.c) */
#define IO_BUF   		0x02 
/*! @brief plain memory */
#define IO_MEM   		0x03 
/*! @brief socket  */
#define IO_SOCKET		0x04
/*! @brief digital sound processor */
#define IO_DSP 			0x05

/*! @brief current number of io types */
#define IO_NTYPES 		0x06

/* logsetlevel(LOG_DEBUG) if you want to force the system to be verbose
	lprintf(LOG_ALOT, ...) if you want the lprintf function to be verbose !! 
	i removed LOG_VERBOSE because it's counter intuitive that 
	lprintf(LOG_VERBOSE) actually prints LESS often !!!
	level 0 is still available if i find a better keyword for it ?
*/

/* loglevel=LOG_SILENT wins over lprintf(LOG_BLAH) */

/*! @brief very verbose log level */
#define LOG_BLAH    0	/* 0+1+2+3 */
/*! @brief debug log level */
#define LOG_DEBUG   1	/* 1+2+3 */
/*! @brief warning log level */
#define LOG_WARN    2	/* 2+3 */
/*! @brief error log level */
#define LOG_ERR     3	/* 3 */
/*! @brief always be quiet log level */
#define LOG_SILENT  4	/*  */

/*! @brief for use in lprintf, force to be verbose */
#define LOG_ALOT    3	/* be verbose */

/*! @brief max number of log levels */
#define LOG_MAX    	5

/* helper struct(s) for io_t */
/*! @brief memory buffer */
typedef struct mem_tag
{
	/*! total size in bytes of membuf */
	int size;
	/*! base of buffer data */
	char *base;
	/*! current pointer inside base */
	char *cur;
} mem_t; 

/* socket.c */
/*! wait for data available on socket */
#define WAIT_READ  0x01
/*! wait for output socket */
#define WAIT_WRITE 0x02
/*! wait error on  socket */
#define WAIT_ERROR 0x04

/*! @brief realm */
typedef struct rlm_tag {
	short type;		/*!< type first so you get generic messages with realm 0 */
	short realm;	/*!< realm second */
} rlm_t;

/*! @brief construct type from realm and type */
#define RLM_INT(realm,type) (int) (realm<<16)+type
/*! @brief realm part of type */
#define TAG_REALM(tag)      (tag >> 16) & 0x00FF
/*! @brief type part of type */
#define TAG_TYPE(tag)       (tag & 0x00FF)

/* always register new realms here, the types may be in client source */
/* also .. check in right after registering to avoid clashes */
/*! don't use realm 0 */
#define REALM_SPECIAL 0		/* tlv spinoff okt 2004 */
/*! don't use realm 0 */
#define REALM_ERROR   0		/* tlv spinoff okt 2004 */
/* keep REALM 0 apart for detection of ancient messages */

/*! @brief obsolete box library, could reuse realm ? */
#define REALM_BOX     1
/*! @brief ask realm */
#define REALM_ASK     2
/*! @brief mysql */
#define REALM_MYSQL   3
/*! @brief utility suite 9 nov 2004 */
#define REALM_UTIL    4 
/*! @brief socket server : 10 march 2005 */
#define REALM_SOCKS   5 	
/*! @brief generic messages  moved from realm 0 to keep 0 clean */
#define REALM_GENERIC 6		
/*! @brief lib ego */
#define REALM_EGO     7		
/*! @brief sync session server april 2008 */
#define REALM_SSS     8		

/* 6:REALM_GENERIC : */
#define MSG_CON      0 		/*!< connect */
#define	MSG_REJ      1 		/*!< reject (put in len what )*/
#define	MSG_ACK      2 		/*!< acknowledge(put in len what ) */
#define	MSG_HUP      3		/*!< hangup */
#define	MSG_INT      4		/*!< general integer in len */
#define	MSG_END      5 		/*!< end */
#define	MSG_STOP     6 		/*!< end */
#define	MSG_ERRNO    7 		/*!< error */

#define GEN_MAX      8 		/*!< max number of GENERIC messages */

/* 1:REALM_BOX : */
#define	MSG_KICK     1 		/*!< give the server a lifesign */
#define	MSG_ACKICK   2		/*!< give the server an acknowledged lifesign */
#define	MSG_STR      3		/*!< string */
#define	MSG_CLT_RPRT 4		/*!< client report */
#define	MSG_CLT      5		/*!< report single client */
#define	MSG_REG      6		/*!< register  */
#define	MSG_IDENT    7		/*!< register  */
#define MSG_PORT_PUT 8      /*!< port offer */
#define MSG_PORT_GET 9      /*!< port request */

#define	BOX_MAX      10		/*!< max nof BOX messages */

// tlv is type-length-value
/*! tamas' format (tl) */
#define TLV_8  8
/*! ego / ask format (tlv) */
#define TLV_12 12

/*! @brief type-length-value message encapsulation */
typedef struct tlv_tag {
	/*! type of message */
	int32_t type;
	/*! length of value */
	int32_t len;
	/*! value of lentght len */
	char *handle;
} tlv_t;

/*! @brief generic socket */
typedef struct socket_tag
{
	/*! wadayathink? */
	int port;
	/*! socket family (AF_INET, etc) */
	int family;
	/*! udp or tcp */
	int type;
	/*! file descriptor */
	int fd;
	/*! address information */
	union {
        struct sockaddr sa;
        struct sockaddr_in sa4;
#ifdef HAS_IPV6
        struct sockaddr_in6 sa6;
#endif
    } addr;
    union {
        struct sockaddr sa;
        struct sockaddr_in sa4;
#ifdef HAS_IPV6
        struct sockaddr_in6 sa6;
#endif
    } peer;	/*!< peder */

} socket_t;

/*! @brief generic in/out objects */
typedef struct io_tag
{
    unsigned int type      : 8;		/*!< io type */
    unsigned int colorable : 1;		/*!< console is colorable */
    unsigned int options   : 23;	/*!< option flags */
	unsigned int size;				/*!< for instance : memory */
    union {							/*!< union */
        FILE *fp;					/*!< file pointer */
        int fd;						/*!< file descriptor */
		buf_t *buf;					/*!< dynamic buffer */
		mem_t *mem;					/*!< memory */
		socket_t *sock;				/*!< socket */
    } handle;						/*!< union */
} io_t;

/* cs.c */
#define CS_CONT 0 /*!< continue */
#define CS_DONE 1 /*!< done with message */
#define CS_STOP 2 /*!< done with server ! */

/*! client type */
typedef struct client_tag client_t;
/*! service type */
typedef struct service_tag service_t;

/*! service handler function prototype */
typedef intptr_t (*service_fnc_t)(client_t *);

/*! client states, used to decide if data is to be read, only CLT_RUNNING 
 *  means there is a message waiting, the others are used for server push
 *  messages if needed (for instance CLT_INIT to print a welcome message)
 */
enum clt_states { 
	/*! just after connection, use when server has to msg first */
	CLT_INIT, 
	/*! normal running, client should msg now */
	CLT_RUNNING,
	/*! serverside message, not a reaction to client message */
	CLT_PUSH,
	/*! serverside stopping of client, for instance when server stops */
	CLT_EXIT,
};

/*! @brief connected client
 *
 * information of clinets that are connected to a server instance
 */
struct client_tag {
	/*! socket connection */
	socket_t *sock; 	
	/*! service this client will receive */
	service_t *service;
	/*! stream to read from/write to (based on sock)  */
	io_t *iop;
	/*! state of client, see clt_states */
	int state;
	/*! login or connect time */
	time_t stamp;
	/*! users data hook, assigned in client_set_udata() */
	void *udata;
};

/*! @brief server data */
typedef struct server_tag {
	/*! list of services this server provides */
	da_t *services;
	/*! default backlog of server sockets */
	int backlog;
} server_t;

/*! @brief server service */
struct service_tag  {
	server_t *srv;	/*!< server supplying this service */
	socket_t *sock;	/*!< socket using this service */
	service_fnc_t fnc;	/*!< service handler */
	da_t *clients;	/*!< clients using this service */
	void *udata;	/*!< user data hook */
};

/*! @brief buffered io*/
typedef struct bio_tag 
{
	io_t *iop;			/*!< io pointer */
	int lead;			/*!< lead position */
	int trail;			/*!< trail position */
	buf_t *cache;		/*!< cache buffer */
} bio_t;

/*! @brief buffer io */
typedef struct bio_functions_tag
{
    /* int (*fopen)(io_t *, const void *handle, const char *mode); */
    /* int (*fclose)(io_t *iop); */
	/*! read function with fread like format */
	size_t (*fread)(void *ptr, size_t size, size_t nmemb, const bio_t *iop);
	/*! write function with fwrite like format */
	size_t (*fwrite)(void *ptr, size_t size, size_t nmemb, const bio_t *iop);

	/* positioning (restricted !) */
	/*! function with fseek like format */
    int (*fseek)(bio_t *iop, long pos, int whence);
	/*! function with ftell like format */
    intptr_t (*ftell)(bio_t *iop);
    /* void (*rewind)(io_t *iop); */
	/*! function with wait like format */
	int (*wait)(bio_t *fp, int which, int to);

	/*! discard data */
	int (*discard)(bio_t *biop, int pos);
	/*! read a complete line */
	char (*readline)(bio_t *biop);
} bio_functions_t;

/*! stdout io type */
extern io_t io_stdout;
/*! stdin io type */
extern io_t io_stdin;

/*! @brief io function pointers 
 *
 * a set of functions an io_t type object must implement 
 */
typedef struct io_functions_tag
{
	/*! open io object */
    int (*open)(io_t *, const void *handle, int mode);
	/*! close io object */
    int (*close)(io_t *iop);
	/*! read from io object */
	size_t (*read)(io_t *iop,void *ptr, size_t size);
	/*! write to io object */
	size_t (*write)(io_t *iop,void *ptr, size_t size);

	/* positioning  */
	/*! lseek into io */
    int (*lseek)(io_t *iop, long pos, int whence);
	/*! ftell position in io */
    intptr_t (*ftell)(io_t *iop);
	/*! rewind position in io */
    void (*rewind)(io_t *iop);
	/*! wait for io */
	int (*wait)(io_t *fp, int which, int to);

	/* int (*feof)(const io_t *ptr) */
	/* int (*ferror)(const io_t *ptr) */
} io_functions_t;

/* ---- heap.c ---- */
/*! @brief heap data structure */
typedef struct eheap_tag {
	/*! size of data kept in 1 heape element */
	unsigned int wid       : 16;
	/*! k heap order */
	unsigned int k         : 14;
	/*! is completely sorted */
	unsigned int is_sorted : 1; /* totally sorted */
	/*! qualifies as heap */
	unsigned int is_heap   : 1; /* partially sorted */
	/*! underlying dynamic array */
	da_t *dap; 	
	/*! compare function for array elements */
	cmp_fnc_t cmp;
} eheap_t;

/* ---- log.c ---- */
/*! @brief log context */
typedef struct log_tag {
	/*! destinatiuon stream for logging */
	io_t *iop;
	/*! log verbosity level */
	int level;
} log_t;

/* ---- idx.c ---- */
/*! @brief array index 
 *
 * as opposed to indexed array, these are the indices of an indexed array 
 *
 */
typedef struct idx_tag
{
	/*! element compare function */
	cmp_fnc_t cmp;
	/*! position array or forward array */
	da_t *pos; 	/* index (positions) */
	/*! index array or inverse array */
	da_t *idx; 	/* inverse (indices) */
	/*! optional user data handle */
	void *udata;
	/*! old position, used in stretching/shrinking */
	unsigned int opos;
	/*! new position, used in stretching/shrinking  */
	unsigned int npos;
	/*! is in process of sorting (bitfield : 1) */
	unsigned int sorting : 1;
	/*! is completely sorted (bitfield : 1) */
	unsigned int sorted  : 1; 
	/*! future use/padding  (bitfield : 30)*/
	unsigned int therest : 30;
} idx_t;

/* indexed dynamic array !! */
/*! @brief indexed array */
typedef struct ida_tag
{
	/*! array of indices */
	da_t *idxs; 
	/*! actual data */
	da_t *dap; 
} ida_t;


/* ---- aa.c ---- */
/*! @brief associated array element
 *
 *  Detailed description starts here.
 */
typedef struct aa_item_tag
{
	void *key;	/*!< key value */
	void *val;	/*!< data value */
} aa_item_t;

/*! @brief associative array */
typedef struct aa_tag {
	unsigned int sorted : 1;	/*!< set if array is sorted */
	unsigned int unused : 1;	/*!< not used */
	unsigned int keywid : 15;	/*!< wid in bytes of the key */
	unsigned int valwid : 15;	/*!< wid in bytes of the value */
	cmp_fnc_t keycmp;			/*!< key comparison function */
	da_t *dap;					/*!< underlying dynamic array */
	void *udata;				/*!< user data hook */
} aa_t;

/* ---- ada.c ---- */
/*! @brief generic string tuple */
typedef struct tuple_tag
{
	char *key;		/*!< key value */
	char *val;		/*!< data value */
} tuple_t;

/* ---- iv.c ---- */

/*! @brief interval */
typedef struct iv_tag {
	/*! left interval border */
	void *left;
	/*! right interval border */
	void *right;
	/*! user data */
	void *udata;
} iv_t;

/*! @brief interval set */
typedef struct ivset_tag {
	/*! list of intervals */
	dlst_t *ivs;
	/*! object in each list member */
	object_t op;
} ivset_t;

#define TOK_ERROR -1 /*!< token error */

/* in tokenizer contexts you can make (in sequence) any combination of  */
/* + : push state */
/* - : pop state */
/* r : report (return current token) */
/* s : shift add current character to the current token (and read the next) */
/* if you add a rule with "r" report in it you should priovde a type as well */
/* these are some standard sequences  */

#define TOK_PUSH  "+"		/*!< push  (non terminals) */
#define TOK_TERM  "r-"		/*!< report, then pop (end of terminals) */
#define TOK_SHIFT "s"		/*!< just shift (terminals) */
#define TOK_SEP   "sr"		/*!< shift, then report (single-char separators) */
#define TOK_POP   "-"		/*!< just pop	(ends, handled in a higher context) */

#define DEFBUFSIZE 100	/*!< default buffer size */
#define DEFJUMP    100	/*!< default jump */

typedef struct mat_tag
{
	int rows;
	int cols;
	int32_t *data;
} mat_t;

/* autom.c */
/*! @brief non deterministic finite automaton */
typedef struct nfa_tag {
	char *base;	/*!< base pointer */
	char *ptr;	/*!< current pointer */

	lifo_t *stack;	/*!< parser stack */
	graph_t *gp;	/*!< stat graph */
} nfa_t;

/*! @brief deterministic finite automaton */
typedef struct dfa_tag {
	char *base;	/*!< base pointer */
	char *ptr;	/*!< walk pointer */

	lifo_t *stack;	/*!< parser stack */
	graph_t *gp;	/*!< state graph */
} dfa_t;

/* ---- tok.c ---- */

/*! tokenizer entry type */
typedef struct tokentry_tag tokentry_t;
/*! tokenizer context type */
typedef struct tokcontext_tag tokcontext_t;

/*! @brief tokenizer entry */
struct tokentry_tag {
	char *what;				/*!< data */
	tokcontext_t *where;	/*!< position */
	int type;				/*!< type */
};

/*! @brief tokenizer context */
struct tokcontext_tag {
	tokentry_t *da_rulez[256]; 		/*!< token context rules */
};

/*! @brief token */
typedef struct tok_tag {
	char *name;		/*!< token name */
	char *workbuf;	/*!< work buffer */
	int bufsize;	/*!< buffer size */
	int jumpsize;   /*!< enlargment size when bufsize is exceeded */
	int len;   		/*!< token length */
	int type;		/*!< current token type */

	lifo_t *stack;	/*!< state stack */
	tokcontext_t *entry;/*!< entry */
	da_t *contexts;	/*!< context array */
	
	void *udata;	/*!< user data hook */
} tok_t;

/*! tokenizer handler function prototype */
typedef int (* tok_fnc_t)(tok_t *, void *);
/* ---- tree.c ---- */

/*! probably need enode_t later on, tnode_t is a treenode */
typedef struct tnode_tag tnode_t;
/*! tree node element */
struct tnode_tag {
	void  *data;	/*!< nodes data */
	da_t *children;	/*!< nodes chilrdem */
}; 

/*! @brief tree structure */
typedef struct tree_tag {
	/*! width in bytes of each tree element */
	int wid;
	/*! root element */
	tnode_t *root;
	/*! user data */
	void *udata;
} tree_t;

/*! sorted tree structure */
typedef struct stree_tag {
	tree_t *basetree;	/*!< base tree */
	int maxbranch;		/*!< max branch */
	cmp_fnc_t cmp;		/*!< compare function */
} stree_t;

/*! tree node function prototype */
typedef void (* tnode_fnc_t)(tree_t *tp, tnode_t *np, void *udata,int level);

/* ---- parse.c --- */
/*! @brief parse tree */
typedef struct parse_tag {
	io_t *inp;	/*!< input stream */
	tree_t *tp;	/*!< parse tree */
} parse_t;

/*! @brief parser context */
typedef struct pcontext_tag {
	parse_t *pp;	/*!< parser */
} pcontext_t;

#ifdef WIN32
typedef __int64 ts_t;
typedef __int64 micro_t;
#else
/*! time stamp */
typedef int64_t ts_t;
/*! micro seconds time */
typedef int64_t micro_t;
#endif

#ifdef OLDER
typedef void *(* tpat_fnc_t)(timestamp_t, tpat_t *,void *data);
typedef struct twin_tag {
	timestamp_t start;
	timestamp_t end;
	timestamp_t freq;
} twin_t;

struct tpat_tag {
	int operation;	
	void *handle; 
	twin_t tw;
	da_t *subptrns;
} ;


/* database layout */
typedef struct tpat_db_tag {
	char *tag;
	timestamp_t start;
	timestamp_t end;
	timestamp_t freq;
	char *group;
} tpat_db_t;
#endif


/* ---- date.c ---- */
/*! @brief date calculations  */
typedef struct date_tag {
	time_t time;	/*!< ye olde time_t */
	struct tm tm; 	/*!< ye olde struct tm */
} date_t;

/* If you use no ALIGNMENT (1) be sure to __attribute__((pack)) your */
/* structs  */
#define ALIGNMENT 4		/*!< byte alignment */

/* ---- data.c ---- */
/* 1 as a testing value */
#define JUMP_SIZE 1		/*!< must be set bigger */

#define CALC_SIZE -1		/*!< calulate the size again */
#define SIZE_UNKNOWN -1		/*!< unknown size */
#define PARSE_TYPE NULL		/*!< uhmm */

#define IS_BASETYPE(tp) tp->count==BASE		/*!< is this a base type */
#define IS_POINTER(tp) tp->count==POINTER		/*!< is this a pointer */
#define IS_STRUCTURE(tp) (tp->struct_mem != NULL)		/*!< is struct */

#define PTR_SIZE sizeof(void *)		/*!< pointer size */
#define LEN_TYPE intptr_t		/*!< pointer size */
#define REF_TYPE int		/*!< refernce type */

/*! shift ptr ahead for certain type */
#define SHIFT(PTR,TP) (PTR) += TP->szof;
/*! shift ptr ahead for certain type aligning */
#define SHIFT_ALIGNED(I,PTR,TP) SHIFT(PTR,TP); DO_ALIGN(I,PTR);
/*! shift ptr ahead for certain struct */
#define STRUCT_SHIFT(I,PATIENT,DONOR,TP) SHIFT(PATIENT,TP); if (DONOR) SHIFT_ALIGNED(I,DONOR,TP)
/*! shift ptr ahead for array */
#define ARR_SHIFT(PATIENT,DONOR,TP) SHIFT(PATIENT,TP); if (DONOR) SHIFT(DONOR,TP)

/* if needed, alignment per instance: */
/* #define DO_ALIGN(I,PTR) PTR = (char *)((((unsigned int)PTR +(I->alignment-1)) / I->alignment) * I->alignment) */
/*! realign */
#define DO_ALIGN(I,PTR) PTR = (char *)((((intptr_t)PTR +(ALIGNMENT-1)) / ALIGNMENT) * ALIGNMENT)

/*! struct member representation */
typedef struct member_tag member_t;
/*! data type representation */
typedef struct type_tag type_t;
/*! data instance representation */
typedef struct instance_tag instance_t;
/*! generic function prototype  */
typedef intptr_t  (* gfunc_t)(instance_t *, type_t *, intptr_t, char *, intptr_t);
/*! walk buffer function prototype  */
typedef intptr_t  (* wbfunc_t)(instance_t *, type_t *, intptr_t, char *,intptr_t, gfunc_t);
/*! walk data function prototype  */
typedef intptr_t  (* wdfunc_t)(instance_t *, type_t *, intptr_t, char *,intptr_t, gfunc_t);


/*! @brief generic data type test */
struct type_tag 
{
	char *name;				/*!< own name of this type */
	intptr_t   szof; 		/*!< size of type */
	intptr_t   count;	    /*!<  arrays (<1 is pointer) */
	int   basetype;		    /*!< base type (-1 for structs /basetypes) */
	char *basename;			/*!< base name of this type */
	member_t  *struct_mem;	/*!< submember array (types) */
	wbfunc_t   wlk_b;		/*!< walk over the buffer */
	wdfunc_t   wlk_d;		/*!< walk over the data */
};

/*! test padding, you can only use 3 or 4 because of  */
/* the declarations in data.c */
/* at this point GRANULE 3 gives valgrind errors (nasty ones) */
#define GRANULE 4

/* NOTE THE PACKING !!!!! */
/*! @brief uhmmm */
struct BASE_tag {
	char *name;	/*!< uhmmm */
	short  num[GRANULE];	/*!< uhmmm */
	struct BASE_tag *next;	/*!< uhmmm */
/* } __attribute__((packed)); */
};

/*! @brief alignment test struct */
struct altest_tag {
	short  num[GRANULE];	/*!< uhmmm */
	int ha;	/*!< uhmmm */
/* } __attribute__((packed)); */
};

/*! @brief representation of a struct member 
 *
 */
struct member_tag  
{
	/*! optional tag name */
	char *tagname;
	/*! name of the member */
	char *name;
	/*! enumerated type of member */
	int   type;
};

/* offset from packed data, and pointer allocated to it 
	used for detecting loops */
/*! @brief generic pointer */
typedef struct ptr_tag {
	intptr_t offset;/*!< offset within packed data */
	void *ptr;		/*!< base data */
} ptr_t;

/* static da_t *typelist; */

/* static type_t *charp_type; */
/* static type_t *short_type; */
/* static type_t *short4_type; */
/* static type_t *basetag_type; */

/*! @brief generic variable instance */
struct instance_tag 
{
	int type;	/*!< type index in typedef array */
	int tid;	/*!< thread id */
	intptr_t freestore;	/*!< index of the first free byte */
	int alignment;	/*!< std is 4 */
	intptr_t bsize;		/*!< physical buffer size */
	buf_t *bufp;	/*!< base buffer */
	ida_t *ptable;	/*!< pointer table */
	LEN_TYPE len;	/*!< help variable */
};

/* ---- bit.c ---- */
/*! dump all bits */
#define BITROW_DUMPALL 0x1
/*! row o bits */
typedef struct bitrow_tag {
    int shift;	/*!< offset in buffer */
    int bits;	/*!< number of bits from offset */
    buf_t *buf;	/*!< byte buffer */
} bitrow_t;

/*! @brief bit sequence */
typedef struct bitseq_tag {
	/*! byte where the first bit if this row is */
    int byte; 
	/*! underlying row of bits */
    bitrow_t *bitrow;
} bitseq_t;

#define COMP_NONE 0	/*!< no compression */
#define COMP_HUFF 1	/*!< huffman code compression */

#define MEM_SIZE 8	/*!< memory size */

#define OFF 8	/*!< offset */
#define BITS 64	/*!< nof bits */
#define UNSIGNED 0	/*!< unsigned */
#define SIGNED 1	/*!< signed */

#define NORESULT 1	/*!< no result */
#define DEF 0	/*!< default */

/* ---- persist.c ---- */

#ifdef WIN32
#define strdup _strdup
#endif

/* ---- table.c ---- */
/*! @brief table data structure */
typedef struct table_tag
{
	/*! number of columns in each row */
	int cols;
	/*! array of rows */
	da_t *rows;
} table_t;

/* ---- json.c ---- */
/*! json parser types */
enum JSON_TYPES { json_type_int, json_type_double, json_type_string, json_type_bool, json_type_null, json_type_array, json_type_object, json_type_error };

/*! @brief java script object notation entry */
typedef struct json_tag
{
	int type;	/*!< json data type */
	char *key;  /*!< for objects */
	union {
		da_t *da;		/*!< dynamic array */
		char *s;		/*!< character string */
		//unsigned int i;	/*!< integer */
		long long ll;		/*!< long long integer */
		double d;		/*!< double */
		int  b;			/*!< boolean */
 	} val;				/*!< union of different possible types */
} json_t;

/* ---- scale.c ---- */
/*! @brief scaling context */
typedef struct scale_tag
{
	/*! world width */
	int ww;
	/*! world left */
	int wl;
	/*! world right */
	int wr;
	/*! model width */
	int mw;
	/*! model left */
	int ml;
	/*! model right */
	int mr;

	/*! world snap left offset */
	int wo; 
	/*! world snap step */
	int ws; 

	/*! scale measure */
	float scale;
} scale_t;

/* ---- smtp.c ---- */
/*! @brief simple mail transport protocol */
typedef struct smtp_tag {
	/*! server url */
	char  *server;
	/*! server port */
	short  port;
	/*! recepients array */
	da_t  *rcpts;
	/*! headers array */
	da_t  *hdrs; 
	/*! message transfer buffer */
	buf_t *msgbuf;
} smtp_t;

/*! make timespec from timeval */
# define TIMEVAL_2_TIMESPEC(tv, ts) {                                   \
        (ts)->tv_sec = (tv)->tv_sec;                                    \
        (ts)->tv_nsec = (tv)->tv_usec * 1000;                           \
}
/*! make timeval from timespec */
# define TIMESPEC_2_TIMEVAL(tv, ts) {                                   \
        (tv)->tv_sec = (ts)->tv_sec;                                    \
        (tv)->tv_usec = (ts)->tv_nsec / 1000;                           \
}

/* ---- md5.c ---- */
/*! typedef a 32 bit type */
typedef unsigned long UINT4;

/*! @brief md5 context
 * 
 *  Data structure for MD5 (Message Digest) computation 
 */
typedef struct {
	/*! number of _bits_ handled mod 2^64 */
  UINT4 i[2];                   
	/*! scratch buffer */
  UINT4 buf[4];                                    
	/*! input buffer */
  unsigned char in[64];                              
	/*! actual digest after MD5Final call */
  unsigned char digest[16];     
} MD5_CTX;

/* ---- dsp.c ---- */
/*! @brief digital sound processing */
typedef struct dsp_tag
{
	/*! audio file descriptor */
	int fd;
	/*! audio format (PCM,ALAW,ULAW etc) */
	int fmt;
	/*! frame rate */
	int rate;
	/*! bits per sample */
	int bits;
	/*! nof channels (stereo is 2)*/
	int channels;
	/*! blocksize per read/write */
	int blocksize;
	/*! format capabilities */
	int formats;
} dsp_t;

enum { PROGRAM_USAGE_START, PROGRAM_USAGE_STATUS, PROGRAM_USAGE_STOP, PROGRAM_USAGE_KILL, };

typedef struct tokenizer_state_tag {
	char c;
	int16_t fromstate;
	int16_t tostate;
	int16_t token;
} tokenizer_state_t;

typedef struct tokenizer_transition_tag
{
	int16_t tostate[256];
	int16_t token[256];
} tokenizer_transition_t;

typedef struct xml_tag {
	io_t *input;
	tree_t *parsedtree; 
	int16_t stat;
	tokenizer_transition_t *stats;
} xml_t;

typedef struct ego_list_tag ego_list_t;
struct ego_list_tag
{
	void *data;
	ego_list_t *next;
};


/* leave this one here, generated by cproto, so it needs all types */
/* this file is NOT in svn, generate it with 'make proto' and be 
	sure to have cproto installed !! */
#ifndef BUILDING_PROTO
#include "ego_proto.h"
#endif


#endif /* include wrapper */
#ifdef __cplusplus
}
#endif
