#include <ego.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>


int writePid_currentPid(char *filename) {
 	FILE *fp;
	struct stat fileInfo;
	int result;
	int oldPid = -1;
	char buffer[256] = {0};

	if(stat(filename, &fileInfo) == 0) {
		//printf("writePid(): File %s exist.\n", filename);
		fp = fopen(filename, "r+");
		if(fp == NULL) {
			printf("writePid(): Couldn't open %s in r+ mode.\n", filename);
			exit(-1);
		}
		result = fread(buffer, sizeof(char), 256-1, fp);
		if(result < 0) {
			printf("writePid(): Failed to read from file %s.\n", filename);
			exit(-1);
		}
		if(result == 0) {
			printf("writePid(): File %s is empty.\n", filename);
			exit(-1);
		}
		fclose(fp);
		oldPid = atoi(buffer);
		if(oldPid < 1) {
			printf("writePid(): Can't read pid from string: %s.\n", buffer);
			exit(-1);
		}
		result = kill(oldPid, 0);
		if(result == 0) {
			//printf("writePid(): The process %d specified in %s is still alive.\n", oldPid, filename);
			return oldPid;
		}	
		else {
			if(errno == ESRCH) {
				//printf("writePid(): Process does not exists.\n");
				return -1;
			}
			else {
				printf("writePid(): Failed with kill(): %s\n", strerror(errno));
				exit(-1);
			}
		}
	}
	else {
		//printf("writePid_currentPid(): File does not exist.\n");
		return -1;
	}
}

void writePid_write(char *filename) {
	char myPid[10] = {0};
	FILE *fp;

	snprintf(myPid, 10-1, "%d", getpid());

	fp = fopen(filename, "w");
	if(fp == NULL) {
		printf("writePid(): Couldn't open %s.\n", filename);
		exit(-1);
	}
	fwrite(myPid, strlen(myPid), 1, fp);
	fclose(fp);

	return;
}

void writePid_sendsignal(int pid, int signal) {
	int result;
	
	result = kill(pid, signal);
	if(result != 0) {
		printf("writePid_kill(): Failed to kill %d. %s\n", pid, strerror(errno));
	}
}

void writePid_init(char *programName, int action) {
	int oldPid, c, stopAttempts = 2;
	char filename[FILENAME_MAX];

	snprintf(filename, FILENAME_MAX-1, "%s/%s.pid", "/var/run", programName);
	oldPid = writePid_currentPid(filename);
	
	if(action == PROGRAM_USAGE_START) {
		if(oldPid < 0) {
			printf("writePid(): %10s: Writing pid to file....\n", programName);
			writePid_write(filename);
			return;
		}
		else {
			printf("writePid(): %10s: Process %d is still alive.\n", programName, oldPid);
			exit(-1);
		}
	}
	else if(action == PROGRAM_USAGE_STATUS) {
		if(oldPid < 0) {
			printf("writePid(): %10s: No process alive.\n", programName);
			exit(0);
		}
		else {
			printf("writePid(): %10s: Process %d is alive.\n", programName, oldPid);
			exit(0);
		}
	}
	else if(action == PROGRAM_USAGE_KILL) {
		if(oldPid < 0) {
			printf("writePid(): %10s: No process to kill.\n", programName);
			exit(0);
		}
		else {
			printf("writePid(): %10s: Killing process.\n", programName);
			writePid_sendsignal(oldPid, SIGKILL);
			exit(0);
		}
	}
	else { // action == PROGRAM_USAGE_STOP
		if(oldPid < 0) {
			printf("writePid(): %10s: No process to stop.\n", programName);
			exit(0);
		}
		else {
			printf("writePid(): %10s: Stopping process.\n", programName);
			for(c = 0; c < stopAttempts; c++) {
				writePid_sendsignal(oldPid, SIGTERM);
				usleep(100 * 1000); // Sleep 0.2 secs
				oldPid = writePid_currentPid(filename);
				if(oldPid < 0) break;
				usleep(300 * 1000); // Sleep 0.2 secs
				oldPid = writePid_currentPid(filename);
				if(oldPid < 0) break;
				printf("writePid(): %10s: Attempt %d\n", programName, c + 2);
			}
			if(c == stopAttempts) {
				printf("writePid(): %10s: Now kill it.\n", programName);
				writePid_sendsignal(oldPid, SIGKILL);
			}
			exit(0);
		}
	}
}
