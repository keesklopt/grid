#include <ego.h>

/* add one list item just after the given item, since this could be
	the list head, it could be altered */
void ego_list_add(ego_list_t **position, void *item)
{
	ego_list_t *new = calloc(sizeof(ego_list_t), 1);
	new->data=item;
	new->next = (*position);
	*position = new;
}

void ego_list_del(ego_list_t **position)
{
	ego_list_t *old;
	if (!position) return;
	old = *position;
	*position = old->next;
	free(old);
}

void ego_list_dmp(ego_list_t *lp,dmp_fnc_t how)
{
	ego_list_t *wlk;
	int count=0;

	for (wlk=lp; wlk!= NULL; wlk=wlk->next) {
		printf("[%d] ", count++);
		if (how) how(0, wlk->data, NULL);
		printf("\n");
	}
}

/** find insertpos means the item just before cmp get's bigger
*/
ego_list_t **ego_list_find_insertpos(ego_list_t **lp,void *item, cmp_fnc_t where)
{
	int ret;
	static ego_list_t *wlk;

	wlk = *lp;
	if (!wlk) return lp; // if empty list 
	ret = where(item, wlk->data, NULL); // or if smaller then first item
	if (ret < 0) return lp; // insert as first

	for (wlk=*lp; wlk!= NULL; wlk=wlk->next) {
		if ((wlk)->next==NULL) return &wlk->next; // insert at end
		ret = where(item, wlk->next->data, NULL);
		if (ret < 0) return &wlk->next;
	}
	return lp;
}

/** find deletepos means the first item where cmp is 0
*/
ego_list_t **ego_list_find_deletepos(ego_list_t **lp,void *item, cmp_fnc_t where)
{
	int ret;
	static ego_list_t *wlk;

	wlk = *lp;
	if (!wlk) return NULL; // if empty list no deletion

	ret = where(item, wlk->data, NULL); // or if smaller then first item
	if (ret == 0) return lp; // delete first item

	for (wlk=*lp; wlk!= NULL; wlk=wlk->next) {
		if ((wlk)->next==NULL) return NULL; // nothin to delete
		ret = where(item, wlk->next->data, NULL);
		if (ret == 0) return &wlk->next;
	}
	return NULL; // not found
}

int ego_list_test(int wid, int len)
{
	int t=0;
	ego_list_t *lp=NULL;
	ego_list_t **pos;
	long item;

	for (t=0; t< 100; t++) {
		item=intrand(1,10);
		pos = ego_list_find_insertpos(&lp,(void *)item,int_abuse_cmp);
		ego_list_add(pos, (void *)item);
		ego_list_dmp(lp,int_abuse_dmp);
	}

	// delete as well
	for (t=0; t< 100; t++) {
		item=intrand(1,10);
		pos = ego_list_find_deletepos(&lp,(void *)item,int_abuse_cmp);
		//printf("trying to find %d\n", item);
		ego_list_del(pos);
		ego_list_dmp(lp,int_abuse_dmp);
	}

	return 0;
}
