/** @file  (SORRY function unimplented, disabled in Doxyfile)
  * implementation of a directed graph datastructure
  */

#include <ego.h>

/*! @brief graph helper struct */
typedef struct graph_hlp_tag {
	/*! node object */
	object_t *nob;
	/*! edge object */
	object_t *eob;
} graph_hlp_t;

/** @brief node template (see object.c) */
object_t enode_template = { "node object", node_rls, node_cmp, node_dmp, NULL};
/** @brief edge template (see object.c) */
object_t eedge_template = { "edge object", edge_rls, edge_cmp, edge_dmp, NULL};

/** @brief compare edges
 * NOT IMPLEMENTED YET
 * @param
 * @return
 */
int edge_cmp(const void *left, const void *right, void *udata)
{
	return 0;
}

/** @brief
 * 
 * @param
 * @return
 */
void edge_rls(void *opaque)
{
	eedge_t *ep = opaque;
	if (!ep) return;

	free(ep->udata);
	free(ep);
}

/** @brief
 * 
 * @param
 * @return
 */
void node_dmp(intptr_t wid, const void *opaq, void *xtra)
{
	printf ("node");
}

/** @brief
 * 
 * @param
 * @return
 */
void edge_dmp(intptr_t wid, const void *opaq, void *xtra)
{
	const eedge_t *ep = opaq;
	const object_t *op = xtra;
	graph_hlp_t *ghp = op->udata;

	ghp->eob->dmp(0,ep->udata,ghp->eob->udata);
	printf(":");
	ghp->nob->dmp(0,ep->to->udata,ghp->eob);
}

/** @brief
 * 
 * @param
 * @return
 */
int node_cmp(const void *left, const void *right, void *udata)
{
	const enode_t *l = left;
	const enode_t *r = right;
	object_t *op = udata;
	object_t *sop = op->udata;

	return sop->cmp(l->udata, r->udata, NULL);
}

/** @brief
 * 
 * @param
 * @return
 */
void node_rls(void *data)
{
	enode_t *np = data;
	if (!np) return;

	da_rls(np->edges);
	free(np->udata);
	free(np);
}

// list traversal functions 

// this function overwrites list data, so it assumes it is already a copy
/** @brief
 * 
 * @param
 * @return
 */
void nodedup(dlst_t *dp, ditem_t *dip, void *udata)
{
	aa_t *tmp = udata;
	enode_t *np = dip->data;
	enode_t *dup;

	dup = node_dup(np,tmp);
	aa_add(tmp,np,dup);
	dip->data = dup;
}

/** @brief
 * 
 * @param
 * @return
 */
void edgefill(dlst_t *gp, ditem_t *dip, void *udata)
{
	aa_t *tmp = udata;
	eedge_t *ep = dip->data;
	
	printf("settink %p, %p ni is to %p\n", dip->data, ep, ep->to);
	ep->to = aa_val(tmp, ep->to);
	printf("settink %p, %p ni is to %p\n", dip->data, ep, ep->to);
}

// this function overwrites list data, so it assumes it is already a copy
/** @brief
 * 
 * @param
 * @return
 */
void edgedup(dlst_t *dp, ditem_t *dip, void *udata)
{
	aa_t *tmp = udata;
	eedge_t *ep = dip->data;
	eedge_t *dup;

	dup = edge_dup(ep);
	aa_add(tmp, ep, dup);
	dup->to = ep->to;
	printf("%p is %p\n", dup, dup->to);
	dip->data = dup;
}

/** @brief
 * 
 * @param
 * @return
 */
void erls(void *data)
{
	str_rls(data);
}

/** @brief
 * 
 * @param
 * @return
 */
void nrls(void *data)
{
	str_rls(data);
}

/** @brief
 * 
 * @param
 * @return
 */
graph_t *graph_ist(object_t *nodes, object_t *edges)
{
	graph_t *gp = calloc(sizeof(graph_t),1);

	gp->nodes = dlst_ist(NULL);
	gp->edges = dlst_ist(NULL);

	return gp;
}

/** @brief
 * 
 * @param
 * @return
 */
void graph_gut(graph_t *gp, object_t *nop, object_t *eop)
{
	// copy the template for thread-safety
	object_t *op = object_dup(&enode_template);
	op->udata = nop;

	// free node content
	dlst_gut(gp->nodes, op);
	object_rls(op);
	
	// free edge content
	op = object_dup(&eedge_template);
	op->udata = eop;
	dlst_gut(gp->edges, op);
	object_rls(op);
}

/** @brief
 * 
 * @param
 * @return
 */
// grap release (only release graph structural form) 
void graph_rls(graph_t *gp)
{

	if (!gp) return;

	printf("still %d and %d to release\n", dlst_len(gp->nodes), dlst_len(gp->edges));

	dlst_rls(gp->nodes);
	dlst_rls(gp->edges);

	free(gp);
}

/** @brief
 * 
 * @param
 * @return
 */
void graph_dmp(graph_t *gp)
{
	/* graph_hlp_t gpd; */

	/* gpd.ndmp = ndmp; */
	/* gpd.edmp = edmp; */

	printf("Graph \n");
	printf("%d Nodes -------------\n", dlst_len(gp->nodes));
	dlst_dmp(gp->nodes);
	printf("%d Edges -------------\n", dlst_len(gp->edges));
	/* dlst_traverse(gp->edges, edgedmp, &gpd); */
	dlst_dmp(gp->edges);
}

/** @brief
 * 
 * @param
 * @return
 */
eedge_t *edge_ist(void *udata)
{
	eedge_t *ep = calloc(sizeof(eedge_t),1);

	ep->udata = udata;

	return ep;
}

/** @brief
 * 
 * @param
 * @return
 */
void e_dmp(eedge_t *np, dmp_fnc_t dmp)
{
	printf(" - ");
	if (dmp) {
		dmp(0,np->udata,NULL);
	}
	printf("\n");
}

/** @brief
 * 
 * @param
 * @return
 */
enode_t *node_ist(void *udata)
{
	enode_t *np = calloc(sizeof(enode_t),1);

	np->udata = udata;
	np->edges = da_ist(sizeof(eedge_t *),0);

	return np;
}

/** @brief
 * 
 * @param
 * @return
 */
eedge_t *edge_dup(eedge_t *orig)
{
	eedge_t *ep = calloc(sizeof(eedge_t),1);

	printf("ep is %p, orig is %p\n", ep, orig);

	ep->udata = orig->udata;
	printf("==== %p %p\n", ep->to, orig->to);
	ep->to = orig->to;
	

	return ep;
}

/** @brief
 * 
 * @param
 * @return
 */
enode_t *node_dup(enode_t *orig, aa_t *tmp)
{
	int t;
	eedge_t **ep;
	eedge_t *e2p;
	enode_t *np = calloc(sizeof(enode_t),1);

	/* printf("np is %p, orig is %p\n", np, orig); */

	np->udata = orig->udata;
	np->edges = da_ist(sizeof(eedge_t *),da_len(orig->edges));

	for (t=0; t< da_len(orig->edges); t++) {
		ep = da_get(orig->edges, t);
		e2p = aa_val(tmp,(*ep));
		/* printf("%p is %p\n", *epp, e2p); */
		da_set(np->edges, &e2p, t);
	}

	return np;
}

/** @brief
 * 
 * @param
 * @return
 */
enode_t *graph_get_node(graph_t *gp, int t)
{
	enode_t *nd;

	nd = dlst_get(gp->nodes, t);
	return nd;
}

/** @brief
 * 
 * @param
 * @return
 */
void graph_add_node(graph_t *gp, enode_t *np)
{
	dlst_add(gp->nodes, np);
}

/** @brief
 * 
 * @param
 * @return
 */
void graph_add_edge(graph_t *gp, enode_t *from, enode_t *to, void *udata)
{
	eedge_t *ep = edge_ist(udata);

	ep->to = to;
	da_add(from->edges, &ep);
	dlst_add(gp->edges, ep);
}

/** @brief
 * 
 * @param
 * @return
 */
graph_t *graph_dup(graph_t *orig)
{
	aa_t *tmp;
	graph_t *gp = graph_ist(orig->nob, orig->eob);

	// graph_ist installs too much 
	dlst_rls(gp->nodes);
	dlst_rls(gp->edges);

	tmp = aa_ist(sizeof(void *), sizeof(void *), std_cmp);

	gp->nodes = dlst_dup(orig->nodes);
	gp->edges = dlst_dup(orig->edges);

	dlst_traverse(gp->edges, edgedup, tmp);
	dlst_traverse(gp->nodes, nodedup, tmp);
	dlst_traverse(gp->edges, edgefill, tmp);

	aa_rls(tmp);

	return gp;
}

// this fills a and empties b (empty'ing is NOT freeing)
/** @brief
 * 
 * @param
 * @return
 */
int graph_join(graph_t *a, graph_t *b)
{
	dlst_join(a->nodes, b->nodes);
	dlst_join(a->edges, b->edges);

	b->entry = b->exit = NULL; // unusable

	return 0;
}

/** @brief
 * 
 * @param
 * @return
 */
enode_t * graph_get_entry(graph_t *gp)
{
	return gp->entry;
}

/** @brief
 * 
 * @param
 * @return
 */
enode_t * graph_get_exit(graph_t *gp)
{
	return gp->exit;
}

/** @brief
 * 
 * @param
 * @return
 */
void graph_set_entry(graph_t *gp, enode_t *np)
{
	gp->entry = np;
}

/** @brief
 * 
 * @param
 * @return
 */
void graph_set_exit(graph_t *gp, enode_t *np)
{
	gp->exit = np;
}

/** @brief
 * 
 * @param
 * @return
 */
int graph_cat(graph_t *a, graph_t *b, void *data)
{
	enode_t *from = a->exit;
	enode_t *to = b->entry;

	if (!from || !to) return -1;

	graph_join(a,b);
	graph_add_edge(a, from, to, data);

	return 0;
}

/** @brief
 * 
 * @param
 * @return
 */
int graph_test(int wid, int len)
{
	int t;
	int from,to;
	char *str;
	graph_t *gp;
	graph_t *gp2;
	enode_t *np, *np2;

	// test values
	wid = 10;
	len = 4;

	gp = graph_ist(&str_object, &str_object);
	gp2 = graph_ist(&str_object, &str_object);
	/* gp2 = graph_ist(&int_object, &int_object); */
	
	for (t=0; t< len; t++) {
		str = strrand(wid);
		np = node_ist(str);
		graph_add_node(gp, np);
		graph_set_exit(gp,np);
		if (t ==0) graph_set_entry(gp,np);

		str = strrand(wid);
		np = node_ist(str);
		graph_add_node(gp2, np);
		graph_set_exit(gp2,np);
		if (t ==0) graph_set_entry(gp2,np);
	}
	
	for (t=0; t< len; t++) {
		from = intrand(0,len-2);
		to = intrand(0,len-2);
		str = strrand(wid/2);
		np = graph_get_node(gp, from);
		np2 = graph_get_node(gp, to);
		graph_add_edge(gp, np, np2, str);

		from = intrand(0,len-2);
		to = intrand(0,len-2);
		str = strrand(wid/2);
		np = graph_get_node(gp2, from);
		np2 = graph_get_node(gp2, to);
		graph_add_edge(gp2, np, np2, str);	
	}

	/* graph_dmp(gp); */
	/* graph_dmp(gp2); */

	graph_rls(gp2);
	gp2 = graph_dup(gp);
	/* graph_dmp(gp2,str_dmp,str_dmp); */

	/* graph_join(gp, gp2); */
	/* graph_cat(gp, gp2, strdup("nothn")); */
	graph_dmp(gp);
	graph_gut(gp2, &str_object, &str_object);
	graph_dmp(gp2);

	graph_gut(gp, &str_object, &str_object);
	graph_rls(gp);
	graph_rls(gp2);
	/* graph_rls(gp,NULL,NULL); */
	/* graph_rls(gp2,NULL,NULL); */
	return 1;
}
