#include <ego.h>

int main(int argc, char *argv[])
{

	int ret=0;
	ego_init(LOG_ALOT);

	fflush(stdout);

	if (argc > 1) {
		ret='s';
	}

	/* mytest(); */

	switch (ret) {
		case 's':	/* 'server' mode */
			server_testserver();
		break;
		default:
		break;
	}

	if (argc > 1) { 
		printf("Argc is %d\n", ret);
	}

	/* mind that some tests are exhaustive, which amounts to  */
	/* o2 or even o! behaviour */
	/* keep these values modest, in the source files i will note  */
	/* how deep the tests have been applied without error */
	ret = ego_test(20, 20);
	log_ok(LOG_ALOT, "lego test", ret);

	printf("for a memory leak test type 'scons valgrind'\n");

	ego_exit();

	return 0;
}
