import java.awt.event.*;
import java.applet.*;
import java.awt.*;
import java.util.TreeMap;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.JComponent;
import java.text.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.*;
import javax.imageio.*;
import java.nio.*;

import java.io.*;

class BitSource {

  private final byte[] bytes;
  private int byteOffset;
  private int bitOffset;

  /**
   * @param bytes bytes from which this will read bits. Bits will be read from the first byte first.
   * Bits are read within a byte from most-significant to least-significant bit.
   */
  public BitSource(byte[] bytes) {
    this.bytes = bytes;
  }

  /**
   * @return index of next bit in current byte which would be read by the next call to {@link #readBits(int)}.
   */
  public int getBitOffset() {
    return bitOffset;
  }

  /**
   * @return index of next byte in input byte array which would be read by the next call to {@link #readBits(int)}.
   */
  public int getByteOffset() {
    return byteOffset;
  }

  /**
   * @param numBits number of bits to read
   * @return int representing the bits read. The bits will appear as the least-significant
   *         bits of the int
   * @throws IllegalArgumentException if numBits isn't in [1,32] or more than is available
   */
  public int readBits(int numBits) {
    if (numBits < 1 || numBits > 32 || numBits > available()) {
      throw new IllegalArgumentException(String.valueOf(numBits));
    }

    int result = 0;

    // First, read remainder from current byte
    if (bitOffset > 0) {
      int bitsLeft = 8 - bitOffset;
      int toRead = numBits < bitsLeft ? numBits : bitsLeft;
      int bitsToNotRead = bitsLeft - toRead;
      int mask = (0xFF >> (8 - toRead)) << bitsToNotRead;
      result = (bytes[byteOffset] & mask) >> bitsToNotRead;
      numBits -= toRead;
      bitOffset += toRead;
      if (bitOffset == 8) {
        bitOffset = 0;
        byteOffset++;
      }
    }

    // Next read whole bytes
    if (numBits > 0) {
      while (numBits >= 8) {
        result = (result << 8) | (bytes[byteOffset] & 0xFF);
        byteOffset++;
        numBits -= 8;
      }

      // Finally read a partial byte
      if (numBits > 0) {
        int bitsToNotRead = 8 - numBits;
        int mask = (0xFF >> bitsToNotRead) << bitsToNotRead;
        result = (result << numBits) | ((bytes[byteOffset] & mask) >> bitsToNotRead);
        bitOffset += numBits;
      }
    }

    return result;
  }

  /**
   * @return number of bits that can be read successfully
   */
  public int available() {
    return 8 * (bytes.length - byteOffset) - bitOffset;
  }

}

class ImageSize
{
	//static int wid=1024;
	//static int hei=1024;
	static int wid=800;
	static int hei=800;
}

class DrawObject
{
	boolean isnode;
	int tile;
	int offset;
	int type;
}

class Offsets
{
	int nodes;
	int roads;
	int noffset;
	int roffset;

	Offsets(int n, int r,int no, int ro)
	{
		nodes=n;
		roads=r;
		noffset=no;
		roffset=ro;
	}
}

class RoadMap extends JComponent implements MouseListener, MouseMotionListener
{
	String filebase;
	long toptile;
	long toplevel;
	static final long serialVersionUID=1;
	Map<Object,Object> idx = new TreeMap<Object,Object>();
	BufferedImage bi;
	TileCache tc;
	TileCache tc2;
	int nodes, roads, tiles;
	Scale xscale;
	Scale yscale;
	Point ll, ur;
	boolean drawdirs=false;
	boolean drawup=false;
	boolean drawnodes=false;
	boolean drawdists=false;
	boolean drawnums=false;
	boolean drawtileno=true;
	boolean drawgrid=false;
	boolean drawannotation=false;
	boolean drawlevel[] = new boolean[17];
	boolean drawtween[] = new boolean[17];
	ArrayList annotations=null;

	int rule = AlphaComposite.SRC_OVER;
    float alpha = 0.85F;

	public void paint(Graphics g)
	{
		Graphics2D g2d = bi.createGraphics();
		//System.out.println("Paint !!");
		g.drawImage(bi,0,0,ImageSize.wid,ImageSize.hei,null);
	}

	String[] getTiles()
	{
		return null;
	}

	public void SetBound()
	{
	}

	public void SetTile(long tile)
	{
		LevelInfo levelinfo = new LevelInfo();
		toptile=tile;
		if (tile > 0) 
			toplevel = levelinfo.level(tile);
	}

	public RoadMap(String filebase)
	{
		this.filebase = filebase;
		addMouseListener(this);
		//addMouseMotionListener(this);
		bi = new BufferedImage(ImageSize.wid,ImageSize.hei,BufferedImage.TYPE_INT_RGB);
	}
	
	void DrawTile(Tile tile,TileCache tc,Color color)
	{
		Graphics g = bi.getGraphics();
		Color clr = new Color(255,255,255);
		g.setColor(clr);
		g.fillRect(1,1,ImageSize.wid,ImageSize.hei);
		tile.DrawTileOld(g,this,tc,color);
		repaint();
	}

	void DrawAnnotations (RoadMap map, TileCache tc)
	{
		Graphics g = bi.getGraphics();

		ArrayList<Color> colors = new ArrayList<Color>();
		colors.add(new Color(0,0,0));  // 15 black
		colors.add(new Color(255,0,0));	// 14 red
		colors.add(new Color(0,255,0));	// 13 green
		colors.add(new Color(0,0,255));	// 12 blue
		colors.add(new Color(255,0,255));	// 11 purple
		colors.add(new Color(0,255,255));	// 10 cyan
		colors.add(new Color(255,255,0));	// 9 yellow
		colors.add(new Color(127,127,255)); // 8 easy blue
		colors.add(new Color(128,255,128));	// 7 easy green
		colors.add(new Color(255,128,128)); // 6 red 2
		colors.add(new Color(255,255,128)); // 5 yellow 2
		colors.add(new Color(255,128,255)); // 4 ?

		if (map.annotations == null) return;
		DrawObject drobj;
		LevelInfo levelinfo = new LevelInfo();

		for (int i=0; i< map.annotations.size(); i++) {
			drobj = (DrawObject) map.annotations.get(i);
		//System.out.println(drobj.type);
		//System.out.println(drobj.tile);
		//System.out.println(drobj.offset);
			Tile tile = tc.GetTile(drobj.tile);
			if (drobj.isnode) {
				Node to = tile.GetNode(drobj.offset);
				long x1=to.x; long y1=to.y; 
				x1 = map.xscale.get_model(x1);
				y1 = map.yscale.get_model(y1);
	
       			Graphics2D g2 = (Graphics2D)g;
				Color clr = new Color(128,128,128);
				if (drobj.type <11) 
					clr = colors.get(drobj.type);
				g.setColor(clr);
				int wid=6;
				if (drobj.type ==12) wid=12;
				if (drobj.type ==13) wid=9;
				if (drobj.type ==4) wid=8;
				//if (drobj.type ==5) wid=10;
				//if (drobj.type ==6) wid=9;
				g.fillArc((int)x1-(wid/2),(int)y1-(wid/2),wid,wid,0,360);
			} else {
				Road r = tile.roads[drobj.offset];
				Node fr = tile.GetNode(r.from);
				Tile tileto = tc.GetTile(r.totile);
				Node to = tileto.GetNode(r.to);
				long x1=fr.x; long y1=fr.y; 
				long x2=to.x; long y2=to.y;
				x1 = map.xscale.get_model(x1);
				y1 = map.yscale.get_model(y1);
				x2 = map.xscale.get_model(x2);
				y2 = map.yscale.get_model(y2);
	
       			Graphics2D g2 = (Graphics2D)g;
				
				Color clr = new Color(255,0,0);
				if (drobj.type <4) 
					clr = colors.get(drobj.type);
				g.setColor(clr);
				g.drawLine((int)x1,(int)y1,(int)x2,(int)y2);
			}
		}
		repaint();
	}

	void loadannotation()
	{
		String fname = "grid_draw";
		FileInputStream is;
		DataInputStream ios;
		try {
			is= new FileInputStream(fname);
			ios = new DataInputStream(is);
		} catch (IOException e) {
			System.out.println("Could not load file : " + e);

			return;
		} 

		annotations = new ArrayList<DrawObject>();

		boolean EOF=false;
		while (!EOF) {
			try {
    			// retrieve image
				DrawObject dobj = new DrawObject();

				byte key=ios.readByte();
				if (key == 'n') 
					dobj.isnode=true;
				if (key == 'r') 
					dobj.isnode=false;

				dobj.tile=ios.readInt();
				dobj.offset=ios.readInt();
				dobj.type=ios.readByte();

				annotations.add(dobj);

			} catch (EOFException e) {
				EOF=true;
			} catch (IOException e) {
				System.out.println("Could not load file : " + e);
				return;
			}
		}
	}

	void Save() 
	{
		try {
    		// retrieve image
    		File outputfile = new File("map.png");
    		ImageIO.write(bi, "png", outputfile);
		} catch (IOException e) {
			System.out.println("Could not save file : " + e);
		}
	}

	void DrawNet(long toplevel) 
	{
		Graphics g = bi.getGraphics();
		Colors clrs = new Colors();
		Color clr = new Color(255,255,255);
		//SetBound();
		g.fillRect(0,0,ImageSize.wid,ImageSize.hei);
		LevelInfo li = new LevelInfo();

		for (int l=0; l< 16; l++) { 
			System.out.println("level " + l);
			if (drawlevel[l]==true) { 
				System.out.println("Getting level " + l);
				ArrayList al = tc.getTiles((int)l);
				if (al==null) continue;
				for(int i=0; i< al.size(); i++) {
					Long tnum = (Long)al.get(i);
					//System.out.println(tnum);
					Tile tile = this.tc.GetTile(tnum);
		
					Rect bb = tile.Dimensions();
					clr = new Color(200,200,0);
					g.setColor(clr);
		
					long x1 = (long)xscale.get_model(bb.ll.x);
					long y1 = (long)yscale.get_model(bb.ll.y);
					long x2 = (long)xscale.get_model(bb.ur.x);
					long y2 = (long)yscale.get_model(bb.ur.y);
		
					if (drawgrid == true) {
						g.drawRect((int)x1,(int)y2,(int)(x2-x1),(int)(y1-y2));
						g.drawString(tnum + "",(int)x1,(int)y2);
					}
		
					Color roadcolor = clrs.get(l);
					tile.DrawTile(g,this,tc,roadcolor);
					repaint();
					tile=null;
				}
			} 
			if (drawtween[l]==true) { 
				System.out.println("Getting level " + l);
				ArrayList al = tc2.getTiles((int)l);
				if (al==null) continue;
				for(int i=0; i< al.size(); i++) {
					Long tnum = (Long)al.get(i);
					Tile tile = this.tc2.GetTile(tnum);
		
					Rect bb = tile.Dimensions();
					clr = new Color(200,200,0);
					g.setColor(clr);
		
					long x1 = (long)xscale.get_model(bb.ll.x);
					long y1 = (long)yscale.get_model(bb.ll.y);
					long x2 = (long)xscale.get_model(bb.ur.x);
					long y2 = (long)yscale.get_model(bb.ur.y);
		
					if (drawgrid == true) {
						g.drawRect((int)x1,(int)y2,(int)(x2-x1),(int)(y1-y2));
						g.drawString(tnum + "",(int)x1,(int)y2);
					}
					//g.drawRect((int)x1,(int)y1,20,20);
		
					Color roadcolor = clrs.get(l);
					tile.DrawTile(g,this,tc2,roadcolor);
					repaint();
					tile=null;
				}
			} 
		}
	}

	void Draw()
	{
		Object key;
		Graphics g = this.getGraphics();
		int count=0;

		loadannotation();

		if (this.toptile < 0) 
			this.DrawNet(this.toplevel);
		else 
			this.DrawTile(this.GetTile(this.toptile,g),tc,null);

		if (drawannotation==true) DrawAnnotations(this,tc);
	}

	void Dijk()
	{
		Graphics g = this.getGraphics();

		TileNode from = new TileNode(this,21,0,g);
	//	TileNode to = new TileNode(this,33,2059,g);
		TileNode to = null;

		// TESTNET
		//TileNode from = new TileNode(this,1164618414,0,g);
		//TileNode to = new TileNode(this,1164618414,4,g);

		//TileNode to = new TileNode(this,1210493832,0,g);

		//TileNode from = new TileNode(this,1120310618, 0);
		//TileNode to = new TileNode(this,1125647026, 0);
		/* TileNode from = new TileNode(this,880140293, 5); */
		/* TileNode to = new TileNode(this,879635335, 0); */
		Dijkstra d = new Dijkstra(this, from, to, g);
	}

	void ResetEval()
	{
		// reset the tilecache NOT everything
		if (this.tc != null)
			this.tc.ResetEval();
	}

	void Reset()
	{
		// reset the tilecache NOT everything
		if (this.tc != null)
			this.tc.Reset();
	}

	void Reset1(Graphics g)
	{
		Object key;
		int count=0;
		Iterator it = this.idx.keySet().iterator();
		
		for (;it.hasNext();)  {
			key= it.next();
			Tile tile = this.GetTile(count,g);
			tile.Reset();
			count++;
		}
	}

	Tile GetTile(long num,Graphics g)
	{
		//System.out.println(num);
		return tc.GetTile(num);
	}

	long getTileIndex(long tile) {
		//System.out.println("Duh !!\n");
		return 0;
	}

	void SetScale(Scale xs, Scale ys)
	{
		this.xscale=xs;
		this.yscale=ys;

		//xs.dmp();
		//ys.dmp();
	}

	// interface MouseListener
	public void mouseExited(MouseEvent e)
	{
	}

	public void mouseEntered(MouseEvent e)
	{
	}

	public void mouseMoved(MouseEvent e)
	{
		//System.out.println(e);
	}
	
	public void mouseDragged(MouseEvent e)
	{
        Graphics2D g2 = (Graphics2D)getGraphics();
		System.out.println(g2);

		Rectangle2D prostokat = new Rectangle2D.Double();
 
        prostokat.setFrameFromDiagonal(e.getPoint().x, e.getPoint().y,this.ll.x, this.ll.y);
        g2.setComposite(AlphaComposite.getInstance(rule, alpha));
        g2.draw(prostokat);
		Color trans = new Color(100,100,100,5);
        g2.setColor(trans);
		//g2.setXORMode(Color.gray);
        g2.fill(prostokat);
        paintComponent(g2);
	}
	
	public void mouseReleased(MouseEvent e)
	{
		this.ur = new Point(e.getX(), e.getY());
		long z1= this.ur.x;
		long z2= this.ll.x;

		//System.out.println(e);

		//if (z1>z2) {
			//long tmp = z1;
			//z1=z2;
			//z2=tmp;
		//}

		long newleft = this.xscale.get_world(z2);
		long newright = this.xscale.get_world(z1);
		//System.out.println(newleft);
		//System.out.println(newright);
		//this.xscale = new Scale(newleft,newright,0,ImageSize.hei);
		this.xscale.set_world(newleft,newright);
		z1= this.ur.y;
		z2= this.ll.y;

		//if (z1<z2) {
			//long tmp = z1;
			//z1=z2;
			//z2=tmp;
		//}
		//System.out.println("y was : " + z2 +"-"+ z1);
		newleft = this.yscale.get_world(z2);
		newright = this.yscale.get_world(z1);
		//System.out.println(newleft);
		//System.out.println(newright);
		//this.yscale = new Scale(newleft,newright,0,ImageSize.hei);
		this.yscale.set_world(newleft,newright);
		//this.Draw();
	}
	
	public void mousePressed(MouseEvent e)
	{
		this.ll = new Point(e.getX(), e.getY());
		//startPoint=e.getPoint();
        setOpaque(true);

        Graphics2D g2 = (Graphics2D)getGraphics();
 
        Rectangle2D prostokat = new Rectangle2D.Double();
        prostokat.setFrameFromDiagonal(e.getPoint().x, e.getPoint().y,this.ll.x, this.ll.y);
        g2.setComposite(AlphaComposite.getInstance(rule, alpha));
        g2.draw(prostokat);
		//g2.setXORMode(Color.gray);
		Color trans = new Color(100,100,100,5);
        g2.setColor(trans);
        g2.fill(prostokat);
	}

	public void mouseClicked(MouseEvent e)
	{
	}
}

class OsrmMap extends RoadMap
{
	//DataInputStream ios;
	RandomAccessFile ios;
	String fname;
	RandomAccessFile raf[];
	RandomAccessFile raf2[];

	Tile tile1 = new Tile(0);
	//Tile tile2 = new Tile(1);

	public OsrmMap(String filebase)
	{
		super(filebase);

		long level;

		//read_osrm();
		read_nodes();
		read_edges();
		read_hsgr();
	}

	void read_hsgr()
	{
		try {
			FileInputStream is;
			int x = this.getWidth();
			int y = this.getHeight();
			x=ImageSize.wid;
			y=ImageSize.hei;
			raf= new RandomAccessFile[4];
	
			Map<Long,Offsets> idx = new TreeMap<Long,Offsets>();
			//fname = filebase + "/netherlands.osrm.hsgr";
			fname = filebase + "/luxembourg.osrm.hsgr";

			ios = new RandomAccessFile(fname,"r");
	
			int i=0;
			boolean EOF=false;
			int ni=0;
			int ri=0;
			int t,n;

			int last=-1;
			try {
				int key=0;
				int nodes=ios.readInt(); // CRC 
				nodes=ios.readInt();
				nodes = Integer.reverseBytes(nodes);

				System.out.println(nodes);
				tile1.n2nodes = nodes;
				tile1.tile = new Long(0);

				int index=0;
				tile1.hnodes = new HashMap();
				for (n=0; n< nodes; n++) {
					byte c;
					Node node = new Node();
					int id = Integer.reverseBytes(ios.readInt());
					node.first= index;
					//c =  ios.readByte();
					//c =  ios.readByte();
					node.offset = id;
					node.first=id;
					node.last=last;
					last = node.first-1;
					//node.offset=-1;
					//System.out.println("its " + node.offset);
					tile1.hnodes.put(new Integer(id),node);
					if (id == 301920347) System.out.println("Yep!");
					//System.out.println("------");
					//System.out.println(id);
				}

				//System.out.println("nodes : " + nodes);
				//ios.seek((nodes * 16) + 4);
				int roads=ios.readInt();
				roads = Integer.reverseBytes(roads);
				System.out.println("roads : " + roads);
				tile1.n2roads = roads;
			tile1.hroads = 
				new Road[(int)tile1.n2roads];
			for (t=0; t< tile1.n2roads; t++) {
				Road r = new Road();
				tile1.hroads[t] = new Road();
				tile1.hroads[t].from= Integer.reverseBytes(ios.readInt());
				tile1.hroads[t].to= Integer.reverseBytes(ios.readInt());
				tile1.hroads[t].len= Integer.reverseBytes(ios.readInt());
				int excess = Integer.reverseBytes(ios.readInt());
				tile1.hroads[t].to &= 0x7fffffff;


				//System.out.println(tile1.hroads[t].from);
				System.out.println(tile1.hroads[t].to);

				ByteBuffer bb = ByteBuffer.allocate(8);
				bb.putInt((int)tile1.hroads[t].to);
				bb.putInt((int)tile1.hroads[t].len);
				byte[] bytes = bb.array();
				BitSource bs = new BitSource(bytes);

				long to = bs.readBits(31);
				System.out.println(to);
				to <<=1;
				System.out.println("shifted");
				System.out.println(to);
			}

				idx.put(new Long(key),new Offsets(nodes,roads,ni,ri));
				ni+=nodes;
				ri+=roads;
				i++;
			} catch (EOFException e) {
				EOF=true;
			}
		
			ios.close();
			//is.close();

			/*
			fname= filebase + "/grid_" + level + "_nodes";
			raf[Tile.NODES] = new RandomAccessFile(fname,"r");
			fname= filebase + "/grid_" + level + "_roads";
			raf[Tile.ROADS]= new RandomAccessFile(fname,"r");
			fname= filebase + "/grid_" + level + "_coord";
			raf[Tile.COORD]= new RandomAccessFile(fname,"r");
			fname= filebase + "/grid_" + level + "_up";
			raf[Tile.LINKS]= new RandomAccessFile(fname,"r");
			this.tc.add(level,raf,idx);
			*/

			//System.out.println(al);
			//System.out.println(level);
		} catch (EOFException ef) {
			System.out.println("short read !!");
		} catch (Exception e) {
			System.out.println(e);
			//System.out.println("no level " + level + " files found");
		}
	}

	void read_osrm()
	{

		try {
			FileInputStream is;
			int x = this.getWidth();
			int y = this.getHeight();
			x=ImageSize.wid;
			y=ImageSize.hei;
			//Scale sx = new Scale(minx,maxx, 0, x);
			//Scale sy = new Scale(maxy,miny, 0, y);
			//System.out.println(sx.scale);
			//this.SetScale(sx,sy);
			raf= new RandomAccessFile[4];
	
			Map<Long,Offsets> idx = new TreeMap<Long,Offsets>();
			//fname = filebase + "/netherlands.osrm";
			fname = filebase + "/luxembourg.osrm";
			//is= new FileInputStream(fname);
			//ios = new DataInputStream(is);

			ios = new RandomAccessFile(fname,"r");
	
			int i=0;
			boolean EOF=false;
			int ni=0;
			int ri=0;
			int t,n;
			try {
				int key=0;
				int nodes=ios.readInt();
				nodes = Integer.reverseBytes(nodes);

				System.out.println(nodes);
				tile1.nnodes = nodes;
				tile1.tile = new Long(0);

				int index=0;
				tile1.nodes = //new HashMap();
					new Node[(int)tile1.nnodes+1];
				for (n=0; n< nodes; n++) {
					byte c;
					Node node = new Node();
					node.y= Integer.reverseBytes(ios.readInt());
					node.x= Integer.reverseBytes(ios.readInt());
					int id = Integer.reverseBytes(ios.readInt());
					node.first= index;
					c =  ios.readByte();
					c =  ios.readByte();
					node.rank = 11;
					//System.out.println("Reading link ");
					node.offset = n;
					//node.offset=-1;
					//System.out.println("its " + node.offset);
					index += c;
					tile1.nodes[n]=node;
					//System.out.println(id);
					c =  ios.readByte();
					c =  ios.readByte();
				}

				System.out.println("nodes : " + nodes);
				ios.seek((nodes * 16) + 4);
				int roads=ios.readInt();
				roads = Integer.reverseBytes(roads);
				System.out.println("roads : " + roads);
				tile1.nroads = roads;
			tile1.roads = 
				new Road[(int)tile1.nroads];
			for (t=0; t< tile1.nroads; t++) {
				Road r = new Road();
				tile1.roads[t] = new Road();
				tile1.roads[t].from= Integer.reverseBytes(ios.readInt());
				tile1.roads[t].totile= 0;
				tile1.roads[t].to= Integer.reverseBytes(ios.readInt());
				tile1.roads[t].len= Integer.reverseBytes(ios.readInt());

				tile1.roads[t].dir= Short.reverseBytes(ios.readShort());
				tile1.roads[t].type= Short.reverseBytes(ios.readShort());
				Byte c =  ios.readByte();
				c =  ios.readByte();
				c =  ios.readByte();
				c =  ios.readByte();
				c =  ios.readByte();
				c =  ios.readByte();
				c =  ios.readByte();
				c =  ios.readByte();
				c =  ios.readByte();
				c =  ios.readByte();
				c =  ios.readByte();
				//System.out.println(tile1.roads[t].from);
			}

				idx.put(new Long(key),new Offsets(nodes,roads,ni,ri));
				ni+=nodes;
				ri+=roads;
				i++;
			} catch (EOFException e) {
				EOF=true;
			}
		
			ios.close();
			//is.close();

			/*
			fname= filebase + "/grid_" + level + "_nodes";
			raf[Tile.NODES] = new RandomAccessFile(fname,"r");
			fname= filebase + "/grid_" + level + "_roads";
			raf[Tile.ROADS]= new RandomAccessFile(fname,"r");
			fname= filebase + "/grid_" + level + "_coord";
			raf[Tile.COORD]= new RandomAccessFile(fname,"r");
			fname= filebase + "/grid_" + level + "_up";
			raf[Tile.LINKS]= new RandomAccessFile(fname,"r");
			this.tc.add(level,raf,idx);
			*/

			//System.out.println(al);
			//System.out.println(level);
		} catch (EOFException ef) {
			System.out.println("short read !!");
		} catch (Exception e) {
			System.out.println(e);
			//System.out.println("no level " + level + " files found");
		}
	}

	void read_edges()
	{

		try {
			FileInputStream is;
			int x = this.getWidth();
			int y = this.getHeight();
			x=ImageSize.wid;
			y=ImageSize.hei;
			raf= new RandomAccessFile[4];
	
			Map<Long,Offsets> idx = new TreeMap<Long,Offsets>();
			//fname = filebase + "/netherlands.osrm";
			fname = filebase + "/luxembourg.osrm.edges";
			//is= new FileInputStream(fname);
			//ios = new DataInputStream(is);

			ios = new RandomAccessFile(fname,"r");
	
			int i=0;
			boolean EOF=false;
			int ni=0;
			int ri=0;
			int t,n;
			try {
				int key=0;
				int roads=ios.readInt();
				roads = Integer.reverseBytes(roads);
				System.out.println("roads : " + roads);
				tile1.nroads = roads;
			tile1.roads = 
				new Road[(int)tile1.nroads];
				for (t=0; t< tile1.nroads; t++) {
					Road r = new Road();
					tile1.roads[t] = new Road();
					tile1.roads[t].to= Integer.reverseBytes(ios.readInt());
					tile1.roads[t].len= Integer.reverseBytes(ios.readInt());
					tile1.roads[t].len= Integer.reverseBytes(ios.readInt());
	
					//Byte c =  ios.readByte();
					//if (t< 100) {
						//System.out.println(tile1.roads[t].to);
						//System.out.println(tile1.roads[t].len);
					//}
				}

			} catch (EOFException e) {
				EOF=true;
			}
		
			ios.close();

			//System.out.println(al);
			//System.out.println(level);
		} catch (EOFException ef) {
			System.out.println("short read !!");
		} catch (Exception e) {
			System.out.println(e);
			//System.out.println("no level " + level + " files found");
		}
	}

	void read_nodes()
	{
		try {
			FileInputStream is;
			int x = this.getWidth();
			int y = this.getHeight();
			x=ImageSize.wid;
			y=ImageSize.hei;
			raf= new RandomAccessFile[4];
	
			Map<Long,Offsets> idx = new TreeMap<Long,Offsets>();
			//fname = filebase + "/netherlands.osrm";
			fname = filebase + "/luxembourg.osrm.nodes";
			//is= new FileInputStream(fname);
			//ios = new DataInputStream(is);

			ios = new RandomAccessFile(fname,"r");
	
			int i=0;
			boolean EOF=false;
			int ni=0;
			int ri=0;
			int t,n;
			//Node lastnode=null;
			try {
				int key=0;
				int nodes=0;

				tile1.tile = new Long(0);

				try { 
					Node node = new Node();
					while (true) {
						byte c;
						node.y= Integer.reverseBytes(ios.readInt());
						node.x= Integer.reverseBytes(ios.readInt());
						int id = Integer.reverseBytes(ios.readInt());
						nodes++;
					}
				} catch (Exception e) {
					System.out.println("nodes read : " + nodes);
				}

				ios.seek(0);
				tile1.nodes = new Node[nodes];
				tile1.nnodes = nodes;
				nodes=0;
				try { 
					while (true) {
						byte c;
						Node node = new Node();
						node.y= Integer.reverseBytes(ios.readInt());
						node.x= Integer.reverseBytes(ios.readInt());
						int id = Integer.reverseBytes(ios.readInt());
						//node.first= id;
						node.offset = nodes;
						tile1.nodes[nodes]=node;
						nodes++;
					}
				} catch (Exception e) {
					System.out.println("nodes read : " + nodes);
				}

				System.out.println("this was nodes : " + nodes);
				ios.seek((nodes * 16) + 4);
			} catch (EOFException e) {
				EOF=true;
			}
		
			ios.close();

			//System.out.println(al);
			//System.out.println(level);
		} catch (EOFException ef) {
			System.out.println("short read !!");
		} catch (Exception e) {
			System.out.println(e);
			//System.out.println("no level " + level + " files found");
		}
	}

	public void SetBound()
	{
		this.toptile=toptile;
		int minx,miny,maxx,maxy;
		Graphics g = this.getGraphics();
	
		int x = this.getWidth();
		int y = this.getHeight();
		x=ImageSize.wid;
		y=ImageSize.hei;

		Rect bb = new Rect(460775292, 711587265,-31261471, 326357100 );
		//System.out.println(bb.ur.x);
		Scale sx = new Scale(bb.ur.x+1,bb.ll.x-0, 20, x-20);
		Scale sy = new Scale(bb.ll.y+1,bb.ur.y-0, 20, y-20);
		this.SetScale(sx,sy);
	}

	public void SetTile(long tile)
	{
		super.SetTile(tile);

		this.toptile=toptile;
		int minx,miny,maxx,maxy;
		Graphics g = this.getGraphics();
		Tile t = this.tile1;

		int x = this.getWidth();
		int y = this.getHeight();
		x=ImageSize.wid;
		y=ImageSize.hei;
	
		Rect bb = t.BoundBox();
		//System.out.println(bb);
		//System.out.println(bb.ur.x);
		Scale sx = new Scale(bb.ur.x+1,bb.ll.x-0, 20, x-20);
		Scale sy = new Scale(bb.ll.y+1,bb.ur.y-0, 20, y-20);
		this.SetScale(sx,sy);
	}

	String[] getTiles()
	{
		String[] sarr = new String[2];
		sarr[0] = "tile_0";
		sarr[1] = "tile_1";

		//for(int i=0; i< al.size(); i++) {
			//sarr[i] = (String) al.get(i);
		//}
	
		return sarr;
	}

	Tile GetTile(long num,Graphics g)
	{
		if (num == 0) return tile1;
		return tile1;
	}


	long getTileIndex(long tile)
	{
		return tile;
	}
}

class LevelMap extends RoadMap
{
	DataInputStream ios;
	String fname;
	RandomAccessFile raf[];
	RandomAccessFile raf2[];
	
	public LevelMap(String filebase)
	{
		super(filebase);

		long level;
		this.tc = new TileCache(this);
		this.tc2 = new TileCache(this);

		for (level=0; level < 16; level ++) {
			try {
				FileInputStream is;
				int x = this.getWidth();
				int y = this.getHeight();
				x=ImageSize.wid;
				y=ImageSize.hei;
				//Scale sx = new Scale(minx,maxx, 0, x);
				//Scale sy = new Scale(maxy,miny, 0, y);
				//System.out.println(sx.scale);
				//this.SetScale(sx,sy);
				raf= new RandomAccessFile[4];
	
				Map<Long,Offsets> idx = new TreeMap<Long,Offsets>();
				fname= filebase + "/grid_" + level + "_index";
				is= new FileInputStream(fname);
				ios = new DataInputStream(is);
	
				int i=0;
				boolean EOF=false;
				int ni=0;
				int ri=0;
				while (!EOF) {
					try {
						int key=ios.readInt();
						//System.out.println("key : " + key);
						int nodes=ios.readInt();
						int roads=ios.readInt();
						idx.put(new Long(key),new Offsets(nodes,roads,ni,ri));
						ni+=nodes;
						ri+=roads;
						i++;
					} catch (EOFException e) {
						EOF=true;
					}
				
				}
				ios.close();
				is.close();

				fname= filebase + "/grid_" + level + "_nodes";
				raf[Tile.NODES] = new RandomAccessFile(fname,"r");
				fname= filebase + "/grid_" + level + "_roads";
				raf[Tile.ROADS]= new RandomAccessFile(fname,"r");
				fname= filebase + "/grid_" + level + "_coord";
				raf[Tile.COORD]= new RandomAccessFile(fname,"r");
				fname= filebase + "/grid_" + level + "_up";
				raf[Tile.LINKS]= new RandomAccessFile(fname,"r");
				this.tc.add(level,raf,idx);

				Map<Long,Offsets> idx2 = new TreeMap<Long,Offsets>();
				raf2= new RandomAccessFile[4];
				fname= filebase + "/grid_1" + level + "_index";
				is= new FileInputStream(fname);
				ios = new DataInputStream(is);
	
				i=0;
				EOF=false;
				ni=0;
				ri=0;
				while (!EOF) {
					try {
						int key=ios.readInt();
						//System.out.println("key : " + key);
						int nodes=ios.readInt();
						int roads=ios.readInt();
						idx2.put(new Long(key),new Offsets(nodes,roads,ni,ri));
						ni+=nodes;
						ri+=roads;
						i++;
					} catch (EOFException e) {
						EOF=true;
					}
				
				}
				ios.close();
				is.close();
				fname= filebase + "/grid_1" + level + "_nodes";
				raf2[Tile.NODES] = new RandomAccessFile(fname,"r");
		fname= filebase + "/grid_1" + level + "_roads";
		raf2[Tile.ROADS]= new RandomAccessFile(fname,"r");
		fname= filebase + "/grid_1" + level + "_coord";
		raf2[Tile.COORD]= new RandomAccessFile(fname,"r");
		fname= filebase + "/grid_1" + level + "_up";
		raf2[Tile.LINKS]= new RandomAccessFile(fname,"r");
		//System.out.println("Pushing " + fname + " to " + level);
		this.tc2.add(level,raf2,idx2);
		//System.out.println(idx2);
		ArrayList al = tc2.getTiles((int)level);
		//System.out.println(al);
		//System.out.println(level);
	} catch (EOFException ef) {
		System.out.println("short read !!");
	} catch (Exception e) {
		System.out.println(e);
		System.out.println("no level " + level + " files found");
	}
}
}

public void SetBound()
{
this.toptile=toptile;
int minx,miny,maxx,maxy;
Graphics g = this.getGraphics();
//Tile t = this.tc.GetTile(toptile);

int x = this.getWidth();
int y = this.getHeight();
x=ImageSize.wid;
y=ImageSize.hei;

Rect bb = new Rect(460775292, 711587265,-31261471, 326357100 );
//System.out.println(bb.ur.x);
Scale sx = new Scale(bb.ur.x+1,bb.ll.x-0, 20, x-20);
Scale sy = new Scale(bb.ll.y+1,bb.ur.y-0, 20, y-20);
this.SetScale(sx,sy);
}

public void SetTile(long tile)
{
super.SetTile(tile);

this.toptile=toptile;
int minx,miny,maxx,maxy;
Graphics g = this.getGraphics();
Tile t = this.tc.GetTile(toptile);

int x = this.getWidth();
int y = this.getHeight();
x=ImageSize.wid;
y=ImageSize.hei;

Rect bb = t.BoundBox();
//System.out.println(bb.ur.x);
Scale sx = new Scale(bb.ur.x+1,bb.ll.x-0, 20, x-20);
Scale sy = new Scale(bb.ll.y+1,bb.ur.y-0, 20, y-20);
this.SetScale(sx,sy);
}

String[] getTiles()
{
ArrayList al = tc.getTiles();

String[] sarr = new String[al.size()];
//String[] sarr = new String[0];
for(int i=0; i< al.size(); i++) {
	sarr[i] = (String) al.get(i);
}

return sarr;
}

long getTileIndex(long tile)
{
ArrayList al = tc.getTiles();
String test = "tile_" + Long.toString(tile);

String[] sarr = new String[al.size()];
for(int i=0; i< al.size(); i++) {
	//System.out.println(i);
	//System.out.println(test);
	//System.out.println(al.get(i));
	if (((String) al.get(i)).equals(test)) return i;
}
return 0;
}
}

/*
class SingleMap extends RoadMap
{
DataInputStream ios;
public SingleMap(String filebase)
{
super(filebase);

String fname= filebase + "/network.par";
FileInputStream is;
int num;
int minx,miny,maxx,maxy;
int cities, streets, pcs, countries;
int i=0;

addMouseListener(this);

try {
	is= new FileInputStream(fname);
	ios = new DataInputStream(is);

	tiles= ios.readInt();
	nodes= ios.readInt();
	roads= ios.readInt();
			minx= ios.readInt();
			miny= ios.readInt();
			maxx= ios.readInt();
			maxy= ios.readInt();
			cities= ios.readInt();
			streets= ios.readInt();
			pcs= ios.readInt();
			countries= ios.readInt();

			System.out.println("tiles : " + tiles);
			System.out.println("nodes : " + nodes);
			System.out.println("roads : " + roads);
			System.out.println("minx : " + minx);
			System.out.println("miny : " + miny);
			System.out.println("maxx : " + maxx);
			System.out.println("maxy : " + maxy);

			int x = this.getWidth();
			int y = this.getHeight();
			x=600;
			y=650;
			Scale sx = new Scale(minx,maxx, 0, x);
			Scale sy = new Scale(maxy,miny, 0, y);
			//System.out.println(sx.scale);
			this.SetScale(sx,sy);

			ios.close();
			is.close();

			fname= filebase + "/network.idx";
			is= new FileInputStream(fname);
			ios = new DataInputStream(is);

			for (i=0; i<tiles;i++) {
			//System.out.println("i" + i);
				int key=ios.readInt();
				int val=ios.readInt();
				idx.put(key,val);
				//System.out.println(idx);
			}
			ios.close();
			is.close();
			fname= filebase + "/network.dat";
			RandomAccessFile raf= new RandomAccessFile(fname,"r");
			this.tc = new TileCache(raf,idx,this);
		} catch (EOFException ef) {
			System.out.println("short read !!");
		} catch (Exception e) {
			System.out.println(e);
			System.out.println(fname);
		}
	}
}

class TileMap extends RoadMap
{
	DataInputStream ios;
	public TileMap(String dir,int toptile) 
	{
		super(dir);

		this.toptile=toptile;
		int num;
		int cities, streets, pcs, countries;
		int i=0;
	
		this.tc = new TileCache(dir,idx,this);
	
		int x = this.getWidth();
		int y = this.getHeight();
		x=600;
		y=650;
	
		Graphics g = this.getGraphics();
		Tile t = this.tc.GetTile(toptile,g);
	
		Rect bb = t.BoundBox();
		Scale sx = new Scale(bb.ur.x,bb.ll.x, 0, x);
		Scale sy = new Scale(bb.ll.y,bb.ur.y, 0, y);
		this.SetScale(sx,sy);
	}

	public void SetTile(int tile)
	{
		super.SetTile(tile);

		this.toptile=toptile;
		int minx,miny,maxx,maxy;
		Graphics g = this.getGraphics();
		Tile t = this.tc.GetTile(toptile,g);
	
		int x = this.getWidth();
		int y = this.getHeight();
		x=600;
		y=650;
	
		Rect bb = t.BoundBox();
		Scale sx = new Scale(bb.ur.x,bb.ll.x, 0, x);
		Scale sy = new Scale(bb.ll.y,bb.ur.y, 0, y);
		this.SetScale(sx,sy);
	}

	@Override
	String[] getTiles()
	{
		String []entries;
		File dir = new File(filebase);

		FilenameFilter fnf = new FilenameFilter() {
			public boolean accept(File dir, String name)
			{
				return name.startsWith("tile_");
			}
		};
		//System.out.println(fnf);
		entries = dir.list(fnf);

		Arrays.sort(entries, new Comparator<String>(){
    		public int compare(String f1, String f2)
    		{
        		return f1.compareTo(f2);
    		} });

		if (entries==null) return null;
		for (int i=0; i< entries.length; i++) {
			//System.out.println(entries[i]);
		}
		
		return entries;

	}

	public TileMap(String dir) 
	{
		super(dir);

		int num;
		int minx,miny,maxx,maxy;
		int cities, streets, pcs, countries;
		int i=0;
	
		this.tc = new TileCache(dir,idx,this);
	
	}
}
*/

public class MapApplet extends JApplet implements ActionListener,ListSelectionListener
{	
	JSplitPane mainpane;
	JList tiles;
	RoadMap map;
	long toptile;
	static final long serialVersionUID=1;
	String []entries;
	JScrollPane jsp;
	JButton save, ldown, lup, up,down,left,right;
	JButton zoom, total;
	JToggleButton uplevel, dists, nodes;
	JToggleButton nums, grid, annotate;
	JToggleButton dirs;
	JToggleButton lbut[] = new JToggleButton[16];;
	JToggleButton tbut[] = new JToggleButton[16];;

	MapApplet(long tile)
	{
		//this.level=level;
		this.toptile = tile;
	}

	public JList MakeList(String dirname)
	{
		//map = new TileMap(".");
		//int tile = Integer.parseInt(entries[0].substring(5));
		map = new LevelMap(dirname);
		//map = new OsrmMap(dirname);
		entries = map.getTiles();


		JList rval = new JList(entries);
		return rval;
	}

	public void start_instance()
	{
    	JFrame frame = new JFrame();

		JLabel menu=new JLabel("placeholder for future JMenuBar!!");
		JLabel stat=new JLabel("placeholder for status bar");

		this.add(menu,BorderLayout.NORTH);
		this.add(stat,BorderLayout.SOUTH);

		//Dimension buttondim= new Dimension(5,5);
		//Dimension buttondim= new Dimension(Short.MAX_VALUE,30);

		tiles = MakeList("hoek");
		tiles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		// first the choices 
		//JButton zoom = new JButton("\u2328");	// keyboard
		//JButton zoom = new JButton("\ue210"); 	// checker
		
		ImageIcon image = new ImageIcon("zoom-fit.png");  
		zoom = new JButton(image);  

		image = new ImageIcon("zoom-out.png");  
		lup = new JButton(image);  

		//zoom.setMaximumSize(buttondim);
		image = new ImageIcon("zoom-in.png");  
		ldown = new JButton(image);  
		up = new JButton("\u21E7");
		left = new JButton("\u21E6");
		down = new JButton("\u21e9");
		right = new JButton("\u21e8");

		image = new ImageIcon("view-fullscreen.png");  
		total = new JButton(image);

		image = new ImageIcon("stock_save.png");  
		save = new JButton(image);

		//Box choicepanel = new Box(BoxLayout.PAGE_AXIS);
		JPanel choicepanel = new JPanel();
		BorderLayout bl = new BorderLayout();
		JPanel dialpanel = new JPanel();
		JPanel togglepanel = new JPanel();
		GridLayout gl = new GridLayout(0,3);
		GridLayout gl2 = new GridLayout(0,5);
		dialpanel.setLayout(gl);
		togglepanel.setLayout(gl2);
		choicepanel.setLayout(bl);
		choicepanel.add(BorderLayout.PAGE_START,dialpanel);
		choicepanel.add(BorderLayout.PAGE_END,togglepanel);

		dialpanel.add(ldown);
		dialpanel.add(up);
		dialpanel.add(lup);
		dialpanel.add(left);
		dialpanel.add(zoom);
		dialpanel.add(right);
		dialpanel.add(total);
		dialpanel.add(down);
		dialpanel.add(save);

		image = new ImageIcon("uplink.png");  
		uplevel = new JToggleButton(image);  
		image = new ImageIcon("dists.png");  
		dists = new JToggleButton(image);  
		image = new ImageIcon("nodes.png");  
		nodes = new JToggleButton(image);  
		image = new ImageIcon("nums.png");  
		nums = new JToggleButton(image);  
		image = new ImageIcon("lines.png");  
		grid = new JToggleButton(image);  
		image = new ImageIcon("annotate.png");  
		annotate = new JToggleButton(image);  
		image = new ImageIcon("dirs.png");  
		dirs = new JToggleButton(image);  

		dialpanel.add(uplevel);
		dialpanel.add(dists);
		dialpanel.add(nodes);
		dialpanel.add(nums);
		dialpanel.add(grid);
		dialpanel.add(annotate);
		dialpanel.add(dirs);
		//choicepanel.add(folderlist);
		//choicepanel.add(shortcuts);
		//choicepanel.add(jtree);
		Colors clrs = new Colors();

		for (int l=1; l< 16; l++) {
			String s = "" + l;
			Color c = clrs.get(l);
			lbut[l] = new JToggleButton(s);
			lbut[l].setForeground(c);
			togglepanel.add(lbut[l]);
			lbut[l].addActionListener(this);
			if (l==15) break;
			s = "\u2b0c";
			c = clrs.get(l+15);
			tbut[l] = new JToggleButton(s);
			tbut[l].setForeground(c);
			togglepanel.add(tbut[l]);
			tbut[l].addActionListener(this);
		}

		jsp = new JScrollPane(tiles);
		choicepanel.add(BorderLayout.CENTER,jsp);

		JPanel radiopanel = new JPanel();
		BoxLayout bl2 = new BoxLayout(radiopanel,BoxLayout.PAGE_AXIS);
		radiopanel.setLayout(bl2);

		JLabel lbl = new JLabel("");
		JSplitPane choicepane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,radiopanel,choicepanel);
		mainpane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,choicepane,lbl);

		this.add(mainpane,BorderLayout.CENTER);

		zoom.addActionListener(this);
		lup.addActionListener(this);
		total.addActionListener(this);
		ldown.addActionListener(this);
		save.addActionListener(this);
		up.addActionListener(this);
		down.addActionListener(this);
		left.addActionListener(this);
		right.addActionListener(this);

		uplevel.addActionListener(this);
		dists.addActionListener(this);
		nodes.addActionListener(this);
		nums.addActionListener(this);
		grid.addActionListener(this);
		annotate.addActionListener(this);
		dirs.addActionListener(this);

		tiles.addListSelectionListener(this);


		//applet.add(t,BorderLayout.NORTH);
    	frame.addWindowListener(new WindowAdapter()
    	{
      		public void windowClosing(WindowEvent e)
      		{
        		System.exit(0);
      		}
    	});

    	frame.add(this);
    	frame.setSize(1100,900);
    	frame.setVisible(true);
		this.init();
		if (toptile > 0) 
		SetSelection(toptile);
	}

	void SetSelection(long tile)
	{
		long i = map.getTileIndex(tile);
		//long i=0;
		tiles.setSelectedIndex((int)i);
		map.SetTile(tile);
		map.Draw();
		JScrollBar jb = jsp.getVerticalScrollBar();

		long min = jb.getMinimum();
		long max = jb.getMaximum();
		long num = map.tc.numTiles();
		int top = (int)(i*max/num);
		jb.setValue(top);
	}

	public static final void main(String args[])
	{
		long tile=0;
		if (args.length > 0) 
			tile = Integer.parseInt(args[0]);
		
		MapApplet app = new MapApplet(tile);
		app.start_instance();
		app.go();
	}


	public void go() {
		Graphics g;

		//System.out.println(map);
		mainpane.setRightComponent(map);
	}

	public void actionPerformed(ActionEvent e) {
		
    	String a = e.getActionCommand();
		for (int l=0; l<16; l++) {
			if (e.getSource() == lbut[l]) {
				JToggleButton jt = (JToggleButton)e.getSource();
				System.out.println("Level button " + l + " pushed");
				map.drawlevel[l] = jt.getModel().isSelected();
			}
			if (e.getSource() == tbut[l]) {
				JToggleButton jt = (JToggleButton)e.getSource();
				System.out.println("Tween button " + l + " pushed");
				map.drawtween[l] = jt.getModel().isSelected();
			}
		}
		if (e.getSource() == uplevel) {
			JToggleButton jt = (JToggleButton)e.getSource();
			map.drawup = jt.getModel().isSelected();
		}
		if (e.getSource() == dists) {
			JToggleButton jt = (JToggleButton)e.getSource();
			map.drawdists = jt.getModel().isSelected();
		}
		if (e.getSource() == nodes) {
			JToggleButton jt = (JToggleButton)e.getSource();
			map.drawnodes = jt.getModel().isSelected();
		}
		if (e.getSource() == nums) {
			JToggleButton jt = (JToggleButton)e.getSource();
			map.drawnums = jt.getModel().isSelected();
		}
		if (e.getSource() == grid) {
			JToggleButton jt = (JToggleButton)e.getSource();
			map.drawgrid = jt.getModel().isSelected();
		}
		if (e.getSource() == annotate) {
			JToggleButton jt = (JToggleButton)e.getSource();
			// do some annotation here
			map.drawannotation = jt.getModel().isSelected();
		}
		if (e.getSource() == dirs) {
			JToggleButton jt = (JToggleButton)e.getSource();
			// do some annotation here
			map.drawdirs = jt.getModel().isSelected();
		}
		if (e.getSource() == lup) {
			//LevelInfo levelinfo = new LevelInfo();
			//long[] children= levelinfo.children(map.toptile);

			map.xscale.zoomout(1.0f);
			map.yscale.zoomout(1.0f);
			//Scale sx = new Scale(bb.ur.x+1,bb.ll.x-0, 20, x-20);
			//Scale sy = new Scale(bb.ll.y+1,bb.ur.y-0, 20, y-20);
	
			//SetSelection(map.toptile);
			//map.Dijk();
			map.Draw();
		}

		if (e.getSource() == up) {
			LevelInfo levelinfo = new LevelInfo();
			long[] n= levelinfo.neighbours(map.toptile);
		// neighbour order is 
        //   123
        //   0 4
        //   765 so 
			if (n != null && n[2] >0 ) 
				map.toptile= n[2];
			SetSelection(map.toptile);
		}
		if (e.getSource() == down) {
			LevelInfo levelinfo = new LevelInfo();
			long[] n= levelinfo.neighbours(map.toptile);
		// neighbour order is 
        //   123
        //   0 4
        //   765
			if (n != null && n[6] >0 ) 
				map.toptile= n[6];
			SetSelection(map.toptile);
		}
		if (e.getSource() == left) {
			LevelInfo levelinfo = new LevelInfo();
			long[] n= levelinfo.neighbours(map.toptile);
		// neighbour order is 
        //   123
        //   0 4
        //   765
			if (n != null && n[0] >0 ) 
				map.toptile= n[0];
			SetSelection(map.toptile);
		}
		if (e.getSource() == right) {
			LevelInfo levelinfo = new LevelInfo();
			long[] n= levelinfo.neighbours(map.toptile);
		// neighbour order is 
        //   123
        //   0 4
        //   765
			if (n != null && n[4] >0 ) 
				map.toptile= n[4];
			SetSelection(map.toptile);
		}
		if (e.getSource() == total) {
			map.toptile= -1;
			int x = this.getWidth();
			int y = this.getHeight();
			x=ImageSize.wid;
			y=ImageSize.hei;
		
			Rect bb = map.tc.BoundBox((int)map.toplevel);
			//System.out.println(bb.ur.x);
			//System.out.println(bb.ll.x);
			//System.out.println(bb.ur.y);
			//System.out.println(bb.ll.y);
			Scale sx = new Scale(bb.ur.x+1,bb.ll.x-0, 20, x-20);
			Scale sy = new Scale(bb.ll.y+1,bb.ur.y-0, 20, y-20);
			map.SetScale(sx,sy);

			map.Draw();
		}
		if (e.getSource() == save) {
			System.out.println("Save pressed");
			map.Save();
		}
		if (e.getSource() == zoom) {
			map.Draw();
			//map.Dijk();
		}
	}

	public void valueChanged(ListSelectionEvent e) {
		ListSelectionModel lsm = tiles.getSelectionModel();
		int i;
		if (e.getValueIsAdjusting()==true) return;

		//System.out.println(e);
		if (lsm.isSelectionEmpty()) {
            //output.append(" <none>");
			return;
        } else {
            // Find out which indexes are selected.
            int minIndex = lsm.getMinSelectionIndex();
            int maxIndex = lsm.getMaxSelectionIndex();
            for (i = minIndex; i <= maxIndex; i++) {
                if (lsm.isSelectedIndex(i)) {
					break;
                }
            }
        }
		//System.out.println(s);
		String s= entries[i];
		int tile = Integer.parseInt(s.substring(5));
		map.SetTile(tile);
		map.Draw();
	}
}

