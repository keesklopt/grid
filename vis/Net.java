import java.awt.*;
import javax.swing.*;
import java.util.TreeMap;
import java.util.*;
import java.io.*;
import java.lang.Math;
import java.text.*;

class Slice 
{
	long x;
	long y;
	int level;
}

class Colors
{
	//String colors[] = { "#2971a5", "#06497a", "#576b7a", "#c9e6fc", "#663366", "#003333", "#000033", "#9999cc", "#339999", "#99cccc", "#cccccc", "#333366", "#0066ff", "#996699", "#cc99cc", "#597a7b", "#5c5570", "#36364a" };

	String colors[] = { "#52cc52", "#52cccc", "#6666ff", "#cc66ff", "#cc5252", "#ffbf40", "#bfff40", "#40ffff", "#ff40ff", "#ff4040", "#b3b36b", "#5c995c", "#7ab1cc", "#967acc", "#b36b6b", "#7f0000", "#7f5500", "#2a7f00", "#006666", "#003399", "#660099", "#990000", "#ff6699", "#7f4c33", "#8ae55c", "#0000ff", "#ff00ff", "#00ff00", "#00ffff", "#ffff00", "#ff0000" }; 

	String get_string (int n)
	{
		if (n<0 || n>= colors.length) return "#000000";
		return colors[n];
	}

	Color get(int n)
	{
		String c = get_string(n);

		int r = Integer.parseInt(c.substring(1,3),16);
		int g = Integer.parseInt(c.substring(3,5),16);
		int b = Integer.parseInt(c.substring(5,7),16);

		return new Color(r,g,b);
	}
}

class Rect
{
	Point ll;
	Point ur;

	Rect(long x1, long y1, long x2, long y2)
	{
		this.ll = new Point(x1,y1);
		this.ur = new Point(x2,y2);
	}

	Rect(Point ll,Point ur)
	{
		this.ll=ll;
		this.ur=ur;
	}
}

class Point
{
	long x;
	long y;
	Point(long x,long y)
	{
		this.x=x;
		this.y=y;
	}
}

// node inside a tile
class Node
{
	long x;
	long y;
	int first;
	int last;
	int cost;
	int cost2;
	int offset;
	int rank;
	boolean heap;
	boolean eval1;
	boolean eval2;
}

class TileNode implements Comparable
{
	Tile tile;
	long  node;

	public int compareTo(Object o)
	{
		TileNode tn=(TileNode)o;
		Node n1=tn.tile.GetNode(tn.node);
		Node n2=this.tile.GetNode(node);

		// if it's the same node, don;t compare costs
		if (n1.cost + n1.cost2 <n2.cost + n2.cost2 ) return  1;
		if (n1.cost + n1.cost2 >n2.cost + n2.cost2 ) return -1;
		return 0;
	}

	TileNode(RoadMap rm, long tile, int node, Graphics g)
	{
		this.tile=rm.GetTile(tile,g);
		this.node=node;
	}

	TileNode(RoadMap rm, Tile tile, long node)
	{
		this.tile=tile;
		this.node=node;
	}

}

class Road
{
	long from;
	long totile;
	long to;
	int len;
	int dir;
	int type;
	int shortcut;
}

class TileAlgo
{
	static final int ORDER_HEAP=0;
	static final int ORDER_TOPDOWN=1;
	static final int ORDER_BOTTOMUP=2;
	int order;
}

class Heap
{
	int nr;
	Vector v = new Vector();
	
	Heap(int nr)
	{
		this.nr=nr;
	}

	void Reset()
	{
		// just empty the list i guess
		this.v.clear();
	}

	TileNode top()
	{
		if (this.v.size() < 1) return null;
		TileNode entry = (TileNode)this.v.get(0);
		return entry;
	}

	int find(Node srcnode)
	{
		int lo=0;
		int hi= this.v.size();
		int mid=hi/2;

		while(hi>lo) {
			TileNode tnode  = (TileNode)this.v.get(mid);
			Node test = tnode.tile.GetNode(tnode.node);
			
			if (srcnode.cost + srcnode.cost2 < test.cost + test.cost2) 
			{
				hi=mid;
			} else {
				lo=mid+1;
			}
			mid=(hi-lo)/2 + lo;
		}
		return mid;
	}

	int find(TileNode tn)
	{
		Node srcnode = tn.tile.GetNode(tn.node);
		return find(srcnode);
	}

	void del(TileNode n)
	{
		int index = this.find(n);
		index--;
		this.v.remove(index);
	}

	void resort(TileNode n)
	{
		TileNode last=null, current=null;

		if (n==null) {
			Collections.sort(this.v);
		} else {
			this.del(n);
			this.add(n);
		}
	}

	void pop()
	{
		this.v.remove(0);
	}

	void add(TileNode node)
	{
		int pos = this.find(node);
		this.v.insertElementAt(node,pos);
		this.dump();
	}

	void dump()
	{
		int i;

		for (i=0; i< this.v.size(); i++) {
			TileNode tn = (TileNode)this.v.get(i);
			Node n = tn.tile.GetNode(tn.node);
			//System.out.println(n.cost + " in node " + tn.tile.tile + ":" + tn.node + " eval = " + n.eval);
		}
	}
}

class Dijkstra
{
	Graphics g;
	TileNode from;
	TileNode to;
	RoadMap  rm;

	Heap heap1 = new Heap(1);
	Heap heap2 = new Heap(2);

	Dijkstra(RoadMap rm, TileNode from, TileNode to, Graphics g)
	{
		SingleDijkstra(rm, from, to, g);
	}

	void DoubleDijkstra(RoadMap rm, TileNode from, TileNode to, Graphics g)
	{
		this.g=g;
		this.rm=rm;
		this.from=from;
		this.to=to;

		this.rm.Reset();
		//System.out.println(from);
		//System.out.println(to);
		heap1.Reset();
		heap2.Reset();

		// add from to the heap
		heap1.add(from);
		heap2.add(to);

		Node fromnode = from.tile.GetNode(from.node);
		Node tonode = to.tile.GetNode(to.node);
		//f.cost=0;
		//f.heap=true;
		boolean found=false;
		TileNode tn;
		Node node;
		while (!found) {
			tn = this.Step(heap1,tonode);
			node=tn.tile.GetNode(tn.node);
			if (node.eval2) found=true;
			tn = this.Step(heap2,fromnode);
			node=tn.tile.GetNode(tn.node);
			if (node.eval1) found=true;
			//if (tn.tile == to.tile && tn.node == to.node)
				//found=true;
		};
		//System.out.println("Found node " + to);
		//DrawRoute(from,to);
	}

	void SingleDijkstra(RoadMap rm, TileNode from, TileNode to, Graphics g)
	{
		this.g=g;
		this.rm=rm;
		this.from=from;
		this.to=to;

		this.rm.Reset();
		//System.out.println(from);
		//System.out.println(to);
		heap1.Reset();

		// add from to the heap
		heap1.add(from);
		Node tonode;

		Node fromnode = from.tile.GetNode(from.node);
		if (to != null)
			tonode = to.tile.GetNode(to.node);
		else 
			tonode = null;
		//f.cost=0;
		//f.heap=true;
		boolean found=false;
		TileNode tn;
		Node node;
		while (!found) {
			tn = this.Step(heap1,tonode);
			node=tn.tile.GetNode(tn.node);
			//if (node.eval2) found=true;
			if (to != null && tn.tile == to.tile && tn.node == to.node)
				found=true;
		};
		//System.out.println("Found node " + to);
		//DrawRoute(from,to);
	}

	// NOT WORKING YET !!!
	/*
	void DrawRoute(TileNode from,TileNode endpoint)
	{
		g.setColor(new Color(0,255,0));
		Node fromnode = endpoint.tile.GetNode(endpoint.node);
		Node tonode = from.tile.GetNode(from.node);
		Node to = tonode;
		Node f = tonode;
		Node l = tonode;
		int t;

		while (from.tile != null && to != tonode) {
			int cnode=0;
			Road rds[] = from.tile.getFwdStar(to.first);
			Tile ctile=null;
			if (rds!=null)  {
				for (t=0; t< rds.length; t++) {
					Road r = rds[t];
					//System.out.println(r.from + ":" + r.to);
					f = from.tile.GetNode(r.from);
					Tile tile = rm.GetTile(r.totile,g);
					Node next = tile.GetNode(r.to);
					System.out.println("cost" + next.cost);
					System.out.println("dir" + r.dir);
					if (from.cost == next.cost + r.cost) {
						cnode = r.to;
						ctile= tile;
						to = next;
					}
					next.eval1=true;
				}
				from= new TileNode(rm, ctile, cnode);

				long x1=l.x; long y1=l.y; long x2=to.x; long y2=to.y;
				x1 = rm.xscale.get_model(x1);
				y1 = rm.yscale.get_model(y1);
				x2 = rm.xscale.get_model(x2);
				y2 = rm.yscale.get_model(y2);
				g.drawLine((int)x1,(int)y1,(int)x2,(int)y2);
				l = to;
			}
			//f = to;
		}
	}
*/

	void Sleep(int seconds)
	{
		int stoptime = seconds* 1;
		try {
		Thread.sleep(stoptime);
		} catch (InterruptedException e) {
		e.printStackTrace();
		}
	}

	int distance(Node a,Node b)
	{
		double R = 6371.0; // km
		if (a==null || b==null) return 100000000;
		double  dLat = Math.toRadians(b.x-a.x);
		double  dLon = Math.toRadians(b.y-a.y);

		double aa = Math.sin(dLat/2) * Math.sin(dLat/2) +
        		Math.cos(Math.toRadians(a.x)) * Math.cos(Math.toRadians(b.x)) * 
        		Math.sin(dLon/2) * Math.sin(dLon/2); 
		double c = 2 * Math.atan2(Math.sqrt(aa), Math.sqrt(1-aa)); 
		double d = R * c;

		return (int)Math.round(d*10);
	}

	void AddDijkPoints(Heap heap, TileNode tn, Road r, Node endpoint)
	{
		int Cp1; // cost point 1 (from point)
		int Cr;
		int Cp2; // Cost point 2 (to point)
		Tile tile;

		Node p1 = tn.tile.GetNode(tn.node);
		tile = this.rm.GetTile(r.totile,g);
		TileNode tn2 = new TileNode(this.rm, tile, r.to);
		Node p2 = tile.GetNode(r.to);

		Cp1 = p1.cost;
		
		//System.out.println("p2 eval: " + p2.eval + " = " + p2.cost);
		boolean eval = (heap.nr==1) ? p2.eval1 : p2.eval2;
		if (!eval) {
			Cr = r.len;
			if (p2.heap) {
				Cp2=p2.cost;
				if (Cp1+Cr < Cp2) {
					p2.cost = Cp1+Cr;
					//System.out.println("resort, cost is " + p2.cost);
					p2.cost2= distance(p2,endpoint);
					//System.out.println("resort, cost is " + p2.cost + ":" + p2.cost2);
					heap.resort(tn2);
				}
			} else {
				p2.cost = Cp1+Cr;
				//System.out.println("add, cost is " + p2.cost);
				heap.add(tn2);
				p2.heap=true;
			}
		}
	}

	TileNode Step(Heap heap, Node tonode) 
	{
		int t;
		long x1,x2,y1,y2;
		TileNode from;

		from = heap.top();
		if (from==null) return null;

		g.setColor(new Color(255,0,0));
		Tile ftile = from.tile;
		Node f = from.tile.GetNode(from.node);

		heap.pop();
		if (heap.nr==1) 
			f.eval1=true;
		else
			f.eval2=true;
		f.heap=false;

		//System.out.println("node :" + ftile.tile );
		Road rds[] = ftile.getFwdStar(from.node);
		if (rds!=null)  
		for (t=0; t< rds.length; t++) {
			Road r = rds[t];
			//System.out.println(r.from + ":" + r.to);
			f = ftile.GetNode(r.from);

			Tile tile = rm.GetTile(r.totile,g);
			Node to = tile.GetNode(r.to);
			x1=f.x; y1=f.y; x2=to.x; y2=to.y;
			x1 = rm.xscale.get_model(x1);
			y1 = rm.yscale.get_model(y1);
			x2 = rm.xscale.get_model(x2);
			y2 = rm.yscale.get_model(y2);

			g.drawLine((int)x1,(int)y1,(int)x2,(int)y2);

			AddDijkPoints(heap,from,r,tonode);
		} else {
			System.out.println("no fwd roads");
		}
		return from;
	}
}

class Tile 
{
	long nnodes;
	long n2nodes;
	long nroads;
	long n2roads;
	Road []roads;
	Road []hroads;
	Node []nodes;
	//HashMap<Integer, Node> nodes;
	HashMap<Integer, Node> hnodes;
	
	Long tile;
	DataInputStream ios;
	DataInputStream nios;
	DataInputStream rios;
	DataInputStream cios;
	FileInputStream is;

	public static final int NODES=0;
	public static final int ROADS=1;
	public static final int COORD=2;
	public static final int LINKS=3;

	Tile (long num)
	{
		this.tile=new Long(num);
	}

	void readparts(TileCache tc, RandomAccessFile raf[], Map<Long, Offsets>idx)
	{
		int t=0;

		try {
			int l = (int)tc.levelinfo.level(this.tile);
			Offsets o = idx.get(this.tile);
			//System.out.println(o);
			if (o==null) return;
			this.nroads = o.roads;
			this.roads = 
				new Road[(int)this.nroads];
			raf[ROADS].seek(o.roffset * 18);
			for (t=0; t< this.nroads; t++) {
				Road r = new Road();
				this.roads[t] = new Road();
				this.roads[t].from= raf[ROADS].readInt();
				this.roads[t].totile= raf[ROADS].readInt();
				this.roads[t].to= raf[ROADS].readInt();
				this.roads[t].len= raf[ROADS].readInt();
				this.roads[t].dir= raf[ROADS].readByte();
				this.roads[t].type= raf[ROADS].readByte();
			}
			//System.out.println(this.nnodes);
			int index=0;
	
			this.nnodes = o.nodes;
			this.nodes = new Node[(int)this.nnodes+1];
			//this.nodes = new HashMap();
			
			raf[COORD].seek(o.noffset * 8);
			raf[NODES].seek(o.noffset * 5);
			raf[LINKS].seek(o.noffset * 4);
			for (t=0; t< this.nnodes; t++) {
				byte c;
				Node node = new Node();
				node.x= raf[COORD].readInt();
				node.y= raf[COORD].readInt();
				node.first= index;
				c =  raf[NODES].readByte();
				node.rank = raf[NODES].readInt();
				//System.out.println("Reading link ");
				node.offset =  raf[LINKS].readInt();
				//node.offset=-1;
				//System.out.println("its " + node.offset);
				index += c;
				this.nodes[t]=node;
			}
			//System.out.println("Nodes : " + t);
			// provide 1 extra node for calculating last road of last node
			Node extra = new Node();
			extra.first=index;
			this.nodes[t]=extra;

			//is.close();
			//ios.close();
		} catch( IOException e)
		{
			System.out.println("Warning !!!! you have an incomplete tileset, missing tile : " + this.tile);
			System.out.println(e);
		}
	}

	// th ereal dimensions oif this tile 
	Rect Dimensions()
	{
		LevelInfo li = new LevelInfo();
		return li.boundbox(this.tile);
	}

	// a rectangle around the outer coordinates, so <= Dimensions
	Rect BoundBox()	
	{
		Rect r = new Rect(-1800000000,-900000000,1800000000,900000000);

		/* Iterator it = this.nodes.values().iterator(); */
		/* for (;it.hasNext();)  { */
		for (int t=0; t< this.nnodes; t++) {

		//Set<Integer> set = this.nodes.keySet();
		//Iterator<Integer> ki = set.iterator();
		//while(ki.hasNext()) {
			//Integer t = ki.next();
			Node n = (Node) this.nodes[t];
			//System.out.println(r.ll.x);
			if (n.x > r.ll.x) r.ll.x=n.x;
			if (n.x < r.ur.x) r.ur.x=n.x;
			if (n.y > r.ll.y) r.ll.y=n.y;
			if (n.y < r.ur.y) r.ur.y=n.y;
			//if (count > 1000) break;
		}
		return r;
	}

	Road [] getFwdStar(long n)
	{
		long r;
		long t;

		if (n>= this.nnodes) return null;
		Node nd = this.nodes[(int)n];
		long f= nd.first;
		nd = this.nodes[(int)(n+1)];
		long l= nd.first;

		Road []ra = new Road[(int)l-(int)f];
		for(r=f;r<l;r++) {
			ra[(int)r-(int)f]= this.roads[(int)r];
		}
		return ra;
	}

	Node GetNode(long n)
	{
		//if (this.nodes == null || n>=this.nodes.size()) return null;
		Node node = this.nodes[(int)n];
		return node;
	}

	Slice tile_2_xy(TileAlgo a, int n, int depth)
	{
		int quad;
		int l;
		int qn;
		int order=TileAlgo.ORDER_TOPDOWN;
		int d=depth-2;
		Slice s= new Slice();

		if (a != null) order = a.order;
		return s;
	}

	void ResetEval()
	{
		int t;
		for (t=0; t< this.nnodes; t++) {
			Node n = this.nodes[t];
			n.heap=false;
			n.eval1=false;
			n.eval2=false;
		}
	}

	void Reset()
	{
		int t;
		//System.out.println(this.nnodes);
		for (t=0; t< this.nnodes; t++) {
			Node n = this.nodes[t];
			n.heap=false;
			n.eval1=false;
			n.eval2=false;
			n.cost=0;
			n.cost2=0;
		}
	}

	void DrawTileOld(Graphics g,RoadMap map, TileCache tc,Color color)
	{
		int t;
		long x1,x2,y1,y2;
		int wid=4;
		int wid2=2;

		if (map.drawnodes==false) {
			wid=1;
			wid2=1;
		}

		int thick=0;
		Node lastnode=null;
		for (t=0; t< this.nroads; t++) {
			Road r = this.roads[t];
			System.out.println(r.from);
			Node f = this.GetNode(r.from);
			System.out.println(this.tile);
			System.out.println(f);

			if (f != lastnode) thick=0;
			lastnode=f;
			//System.out.println("TO road is " + r);
			//System.out.println(t);
			//System.out.println(r.totile);
			Tile tile = this;
			if (tc != null) 
				tile = tc.GetTile(r.totile);
			//System.out.println(tile);
			Node to = tile.GetNode(r.to);
			if (to == null) continue;
			//System.out.print(f.x);
			//System.out.print(" ");
			//System.out.println(f.y);
			//System.out.print(to.x);
			//System.out.print(" ");
			//System.out.println(to.y);
			x1=f.x; y1=f.y; x2=to.x; y2=to.y;
			x1 = map.xscale.get_model(x1);
			y1 = map.yscale.get_model(y1);
			x2 = map.xscale.get_model(x2);
			y2 = map.yscale.get_model(y2);

       		Graphics2D g2 = (Graphics2D)g;
			
			Color clr = new Color(25,25,55);
			Color green = new Color(0,255,0);
			Color red = new Color(255,0,0);
			Font font = new Font("Courier", Font.PLAIN,10);
			boolean addstring=map.drawdists;
			//if (full==true) 
				//g2.setStroke(new BasicStroke(3.0f));
			//else
				g2.setStroke(new BasicStroke(1.0f));

			if (r.totile != this.tile) {

				//addstring=false; // for none of the external links
				if (map.tc.levelinfo.level(r.totile) == map.tc.levelinfo.level(this.tile)) {
					clr = new Color(200,200,255); // green-ish
				} else 
				if (map.tc.levelinfo.level(r.totile) > map.tc.levelinfo.level(this.tile)) {
					clr = new Color(200,200,255); // cold links : blue-ish
				} else {
					clr = new Color(255,200,200); // going up is red
				}
				//if (full==false) clr = new Color(0,0,0);
				g.setColor(clr);
				g.fillArc((int)x2-(wid2/2),(int)y2-(wid2/2),wid2,wid2,0,360);
				g2.setStroke(new BasicStroke(1.0f));
			}
			if (color!=null) clr=color;
			if (r.type >= 20) clr=red;
			//Colors c = new Colors();
			//clr = c.get(thick);
			g.setColor(clr);
			g.setFont(font);
			thick++;
		    //g2.setStroke(new BasicStroke(thick));
			g.drawLine((int)x1,(int)y1,(int)x2,(int)y2);
			g.setColor(green);
			if (addstring) {
				g.drawString(r.len + "",(int)((x1+x2)/2)+10,(int)(y1+y2)/2);
			}
			// 1 : from -> to
			if (r.dir == 2 && map.drawdirs==true) {
				//long xx=x2-(x2-x1)/8;
				//long yy=y2-(y2-y1)/8;
				long xx=x1+(x2-x1)/8;
				long yy=y1+(y2-y1)/8;
				clr = new Color(0,255,0);
				g.setColor(clr);
				g.fillRect((int)xx-2,(int)yy-2,4,4);
			}
		}

		//Iterator it = this.nodes.entrySet().iterator();
		//for (;it.hasNext();)  {
		for (t=0; t< this.nnodes; t++) {
			//Map.Entry pairs = (Map.Entry)it.next();
			//Node n = (Node)pairs.getValue();
			Node n = this.nodes[t];
			long x=n.x;
			long y=n.y;
			//System.out.println(x);
			x = map.xscale.get_model(x);
			y = map.yscale.get_model(y);
			//System.out.println(x);
			/* g.setColor(clr); */
			Color clr = new Color(0,0,255);
			g.setColor(clr);
			//g.fillRect((int)x-(wid/2),(int)y-(wid/2),wid,wid);
			g.fillArc((int)x-(wid/2),(int)y-(wid/2),wid,wid,0,360);
			/* this was for overlapping textst, but its 
				is not neede at this moment */	
			int red = (int) (Math.random() * 250);
			int green = (int) (Math.random() * 250);
			int blue = (int) (Math.random() * 250);
			// so .... color them like the nodes
			//g.setColor(new Color(red,green,blue));
			String s = t + ":" + n.rank;
			if (map.drawnums==true) {
				//g.drawString(t + "",(int)x+10,(int)y+10);
				g.drawString(s ,(int)x+10,(int)y+10);
			}

			// highlight nodes :
			//System.out.println(n.x);
			if ((n.x == 88570757 && n.y == 491956509) ||
			    (n.x == 88574704 && n.y == 491956951) ||
			    (n.x == 60885097 && n.y == 495924496)) {
				System.out.println("YEP !!");
				g.setColor(new Color(255,0,255));
				g.drawArc((int)x-(80/2),(int)y-(80/2),80,80,0,360);
			}

			g.setColor(new Color(255,0,255));
			if (map.drawup==true) {
				if (n.offset != -1) 
					g.drawArc((int)x-(18/2),(int)y-(18/2),18,18,0,360);
			}
		}
	}
		
	void DrawTile(Graphics g,RoadMap map, TileCache tc,Color color)
	{
		int t=11;
		long x1,x2,y1,y2;
		int wid=4;
		int wid2=2;

		if (map.drawnodes==false) {
			wid=1;
			wid2=1;
		}

		for(t=0; t< this.nnodes; t++) {
		//Iterator it = this.nodes.entrySet().iterator();
		//for (;it.hasNext();)  {
			//Map.Entry pairs = (Map.Entry)it.next();
			//Node n = (Node)pairs.getValue();
			Node n = this.nodes[t];
			long x=n.x;
			long y=n.y;
			x = map.xscale.get_model(x);
			y = map.yscale.get_model(y);
			Color clr = new Color(0,0,255);
			g.setColor(clr);
			//g.fillRect((int)x-(wid/2),(int)y-(wid/2),wid,wid);
			g.fillArc((int)x-(wid/2),(int)y-(wid/2),wid,wid,0,360);
			/* this was for overlapping textst, but its 
				is not neede at this moment */	
			int red = (int) (Math.random() * 250);
			int green = (int) (Math.random() * 250);
			int blue = (int) (Math.random() * 250);
			// so .... color them like the nodes
			//g.setColor(new Color(red,green,blue));
			String s = t + ":" + n.rank;
			if (map.drawnums==true) {
				g.drawString(s ,(int)x+10,(int)y+10);
			}

			g.setColor(new Color(255,0,255));
			if (map.drawup==true) {
				if (n.offset != -1) 
					g.drawArc((int)x-(18/2),(int)y-(18/2),18,18,0,360);
			}
			//System.out.println(n.first);

			for (int f = n.first; f<= n.last; f++) {
				//System.out.println(f);
			}
		}
		System.out.println(this.n2roads);
		for (t=0; t< this.n2roads; t++) {
			//System.out.println("from");
			//System.out.println(this.hroads[t].from);
			Road rl = this.roads[(int)this.hroads[t].from];
			//System.out.println(rl.to);
			Node l = this.nodes[(int)rl.to];
			//System.out.println("to");
			//System.out.println(this.hroads[t].to);
			Road rto = this.roads[(int)this.hroads[t].to];
			//System.out.println(rto.to);
			Node to = this.nodes[(int)rto.to];
			//System.out.println(to);

			x1=l.x; y1=l.y; x2=to.x; y2=to.y;
			x1 = map.xscale.get_model(x1);
			y1 = map.yscale.get_model(y1);
			x2 = map.xscale.get_model(x2);
			y2 = map.yscale.get_model(y2);
			System.out.println(x1);
			System.out.println(x2);
			System.out.println(y1);
			System.out.println(y2);
			g.drawLine((int)x1,(int)y1,(int)x2,(int)y2);
		}
	}
}

class TileCacheLevel
{
	RandomAccessFile raf[];
	Map <Long,Offsets>idx;
	Map <Object,Object>cache=new TreeMap<Object,Object>();

	TileCacheLevel(RandomAccessFile raf[], Map <Long,Offsets> i)
	{
		this.raf=raf;
		this.idx=i;
	}
}

class TileCache
{
	RoadMap rm;
	String base;
	//Object[] offsets;
	TileCacheLevel []levels = new TileCacheLevel[20];
	LevelInfo levelinfo = new LevelInfo();

	TileCache(RoadMap rm)
	{
		this.rm=rm;
	}

	void add(long level, RandomAccessFile raf[], Map <Long,Offsets> i)
	{
		levels[(int)level] = new TileCacheLevel(raf,i);
	}
	
	long numTiles()
	{
		int l;
		int num=0;
		ArrayList <String>entries = new ArrayList<String>();

		for (l=0; l< 20; l++) {
			if (levels[l] == null) continue;
			Iterator it = levels[l].idx.keySet().iterator();

			//System.out.println(l);
	
			for (;it.hasNext();)  {
				Object key= it.next();
				num++;
			}
		}
		
		return num;
	}

	ArrayList getTiles(int level)
	{
		int l;
		ArrayList <Long>entries = new ArrayList<Long>();

		if (levels[level] == null) return entries;
		Iterator it = levels[level].idx.keySet().iterator();

		int i=0;
		for (;it.hasNext();)  {
			Object key= it.next();
			entries.add((Long)key);
		}
		
		return entries;
	}

	ArrayList getTiles()
	{
		int l;
		ArrayList <String>entries = new ArrayList<String>();

		for (l=0; l< 20; l++) {
			if (levels[l] == null) continue;
			Iterator it = levels[l].idx.keySet().iterator();

			//System.out.println(l);
	
			int i=0;
			for (;it.hasNext();)  {
				Object key= it.next();
				String entry = "tile_" + key;
				entries.add(entry);
			}
		}
		
		return entries;
	}

	Rect BoundBox(int level)
	{
		int t;
		Rect r = new Rect(-1800000000,-900000000,1800000000,900000000);

		if (levels[level] == null) return r;
		Iterator it = levels[level].idx.keySet().iterator();

		int i=0;
		for (;it.hasNext();)  {
			Object key= it.next();
			Tile tile = GetTile((Long)key);
			for (t=0; t< tile.nnodes; t++) {
				Node n = tile.nodes[t];
				//System.out.println(r.ll.x);
				if (n.x > r.ll.x) r.ll.x=n.x;
				if (n.x < r.ur.x) r.ur.x=n.x;
				if (n.y > r.ll.y) r.ll.y=n.y;
				if (n.y < r.ur.y) r.ur.y=n.y;
				//if (count > 1000) break;
			}
		}
		return r;
	}

	TileCache(RandomAccessFile raf[], Map <Long,Offsets> i, RoadMap rm)
	{
		this.rm=rm;
		//this.raf=raf;
		//this.idx=i;
		//this.offsets=this.idx.values().toArray();
	}

	TileCache(String base, Map <Long,Offsets> i, RoadMap rm)
	{
		this.rm=rm;
		//this.base=base;
		//this.idx=i;
		//this.offsets=this.idx.values().toArray();
	}

	// reset all cached tiles
	void Reset()
	{
		Tile tile;
		int l;
		for (l=0;l<20;l++) {
			if (levels[l] == null) continue;
			Iterator it = this.levels[l].cache.values().iterator();
			
			for (;it.hasNext();)  {
				tile = (Tile) it.next();
				tile.Reset();
			}
		}
	}

	// reset evaluation bits only
	void ResetEval()
	{
		Tile tile;
		int l;

		for (l=0;l<20;l++) {
			if (levels[l] == null) continue;
			Iterator it = this.levels[l].cache.values().iterator();
		
			for (;it.hasNext();)  {
				tile = (Tile) it.next();
				tile.ResetEval();
			}
		}
	
	}

	Tile GetTile(long num)
	{
		int ret=111;
		int t;
		//System.out.println(num);
		Long n = new Long(num);
		int l = (int)this.levelinfo.level(n);
		Tile tile = (Tile)this.levels[l].cache.get(n);
		if (tile!= null) return tile;

		tile = new Tile(num);

		//System.out.println(this.base);
		tile.readparts(this,this.levels[l].raf,this.levels[l].idx);

		// always Reset tile when it comes from disk
		tile.Reset();
		/* System.out.println(this.cache.size()); */
		this.levels[l].cache.put(n,tile);

		return tile;
	}

}
