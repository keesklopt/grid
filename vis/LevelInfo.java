class Xyz
{
	long x;
    long y;
    long l;
};

public class LevelInfo
{
	public static int depth=18;
	long tiles[];
	long offsets[];
	long parts[];

	public long TEST_BIT_32(long data,long pos) {
		 return (data & (1<<(pos)));
	}

	LevelInfo()
	{
		long step=0;
        long quad=1;
        long wide=1;
        int t;

        tiles = new long[depth];
        offsets = new long[depth];
        parts = new long[depth];
        for (t=0; t< this.depth; t++) {
            this.tiles[t] = (long) Math.pow(4,t);
            this.offsets[t]=step;
            step += quad;
            this.parts[t]=wide;
            wide *=2;
            quad *=4;
        }
	}

	Xyz xyz(long tile)
    {
        Xyz xyz=new Xyz();

		long level = this.level(tile);
        long rest = tile - this.offsets[(int)level];

		xyz.x=0;
        xyz.y=0;
        xyz.l = level;

		while (level > 0) {
			long check = this.tiles[(int)level-1];
	
			if (rest < check) {
			} else 
			if (rest < check*2) {
				xyz.x |= 1;
                rest -= check*1;
			} else 
			if (rest < check*3) {
				xyz.y |= 1;
                rest -= check*2;
			} else 
			if (rest < check*4) {
                xyz.x |= 1;
                xyz.y |= 1;
                rest -= check*3;
			} else {
				xyz.l=-1;
                xyz.x=-1;
                xyz.y=-1;
                return xyz;
			}
			xyz.x <<= 1;
            xyz.y <<= 1;
            level--;
		}
	    // shifted one too far
        xyz.x >>= 1;
        xyz.y >>= 1;


        return xyz;
    }

	long xy_2_tile(Xyz xyz)
    {
        long level=xyz.l;
        //System.out.println(xyz.x + ":" + xyz.y + ":" + level);

        if (xyz.l==0) {
            if (xyz.x==0 && xyz.y==0) return 0;
            return -1;
        }

        long parts = this.parts[(int)level];

        // sanity checks 
        if (xyz.x< 0 || xyz.y< 0) return -1;
        if (xyz.x>=parts || xyz.y>= parts) return -1;

        long tile = offsets[(int)xyz.l];
        long factor=1;
        long subtile;
        long x=xyz.x;
        long y=xyz.y;

	    while (level>0) {
            subtile=0;
            subtile = (TEST_BIT_32(y,0)!=0 ? 2 : 0);
            subtile += (TEST_BIT_32(x,0)!=0 ? 1 : 0);
            tile += subtile * factor;
            level--; x>>=1; y>>=1;
            factor *=4;
        }
        //printf("tile is %d\n", tile);
        return tile;

	}

	Rect boundbox(Xyz xyz)
    {
        // we take pacific and antarctic as 0,0 
        // to keep everything positive numbered
        long bx=3600000000L;
        long by=1800000000;
        long tilefx=xyz.x; 
		long tilefy=xyz.y;
        long fax=0; 
		long fay=0;
        Rect r= new Rect(0,0,0,0);
        long mask;
        long level = xyz.l;

        if (xyz.l < 0) return r;

        mask = 1 << (level);
        //printf("mask is %d (level %d)\n", mask, level);

        while (level >0) {
            /* printf("level %d %d %d\n", level, lx, bx); */
            bx/=2;
            by/=2;      tilefx <<=1;
            tilefy <<=1;
            if ((tilefx & mask) != 0) {
                fax += bx;
            }
            if ((tilefy & mask) != 0) {
                fay += by;
            }
            level--;
        }

        // here we restore the 0,0 center again , and the negative coords
        r.ll.x = fax - 1800000000;
        r.ll.y = fay - 900000000;
        r.ur.x = r.ll.x + bx;
        r.ur.y = r.ll.y + by;

        return r;
    }

	Rect boundbox(long tilenum)
	{
		Xyz xyz = xyz(tilenum);
		return boundbox(xyz);
	}


	long []neighbours(long tile)
    {
		long n[] = new long[8];
        int x=0;

        //   123
        //   0 4
        //   765

        Xyz xyz = this.xyz(tile);
        xyz.x-=1; n[x++] = xy_2_tile(xyz);
        xyz.y+=1; n[x++] = xy_2_tile(xyz);
        xyz.x+=1; n[x++] = xy_2_tile(xyz);
        xyz.x+=1; n[x++] = xy_2_tile(xyz);
        xyz.y-=1; n[x++] = xy_2_tile(xyz);
        xyz.y-=1; n[x++] = xy_2_tile(xyz);
        xyz.x-=1; n[x++] = xy_2_tile(xyz);
        xyz.x-=1; n[x++] = xy_2_tile(xyz);
        return n;
	}

	long []children(long tile)
    {
        //int32_t *children = (int32_t *)calloc(4,sizeof(int32_t));
        long children[]= new long[4];
        Xyz xyz = this.xyz(tile);

        //cout << xyz << endl;
        long x=xyz.x;
        long y=xyz.y;
        long l=xyz.l;

        if (l==15) return null;

        x *= 2;
        y *= 2;
        //cout << tile << " " << x << " " << y << " " << l << " " << endl;

        xyz.l = l+1;
		int t=0;
        for (xyz.y=y; xyz.y<y+2; xyz.y++)
            for (xyz.x=x; xyz.x<x+2; xyz.x++)
                children[t++] = xy_2_tile(xyz);

        return children;
	}



	public long parent(long tile)
    {
        Xyz xyz = this.xyz(tile);

		//System.out.println("parent");
		//System.out.println(tile);
		//System.out.println(xyz.x);
		//System.out.println(xyz.y);
		//System.out.println(xyz.l);
        long x=xyz.x;
        long y=xyz.y;
        long l=xyz.l;

        if (l==0) return -1;

        x /= 2;
        y /= 2;

        xyz.x = x;
        xyz.y = y;
        xyz.l = l-1;

        return xy_2_tile(xyz);
    }

	public long level(long tile)
    {
        int l;

        for (l=0; l< this.depth; l++) {
            if (tile < this.offsets[l]) return l-1;
        }
        return l;
    }

}
