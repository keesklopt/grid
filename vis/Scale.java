// port from the scale object in ego.c and Scale.js in Feyenoord
// please keep those in sync !
class Scale
{
	// (w)orld, (m)odel, (l)eft , (r)ight (w)idth (s)nap, (o)ffset
	long ww,mw;
	long wo,ws;
	long wl,wr,ml,mr;
	float scale;

	Scale(long wl, long wr, long ml, long mr)
	{
		this.wl=wl;
		this.ml=ml;
		this.wr=wr;
		this.mr=mr;

		this.recalc();
	}

	void set_world_snap(long offset, long step)
	{
		this.wo=offset;
		this.ws=step;
	}

	long snap_world(long wx)
	{
		long left;
    	long absl = wx - this.wo; // calc from first snap point
    	long  nsnaps = (long)((float)(absl/this.ws) + 0.5);      // which snapslot is it in
    	left = nsnaps * this.ws;    // get left side of slot
    	left += this.wo;        // restore offset
    	return left;
	}

	void recalc()
	{
		this.ww = this.wr-this.wl;
    	this.mw = this.mr-this.ml;
    	this.scale = (float)this.mw/(float)this.ww;
	}

	void set_world(long start, long end)
	{
		this.wl = start;
		this.wr = end;
		this.recalc();
	}

	void zoomout(float howmuch)
	{
		this.wl -= this.ww;
		this.wr += this.ww;

		System.out.println(this.wl);
		System.out.println(this.wr);
		System.out.println(this.ww);
		System.out.println(this.scale);
		
		this.recalc();

		System.out.println(this.wl);
		System.out.println(this.wr);
		System.out.println(this.ww);
		System.out.println(this.scale);
	}

	void set_model(long start, long end)
	{
		this.ml = start;
		this.mr = end;
		this.recalc();
	}

	long get_world(long m)
	{
		float wr = (((float)(m - this.ml)+(float)0.5) / this.scale) + (float)this.wl;
		return (long)wr;
	}

	long get_model (long w)
	{
		float mr = (((float)(w - this.wl)+(float)0.5) * this.scale) + (float)this.ml;
		return (long)mr;
	}

	void dmp()
	{
		System.out.println("world " + this.wl + "," + this.wr + " " + this.ww);
		System.out.println("model " + this.ml + "," + this.mr + " " + this.mw);
		System.out.println("snap " + this.wo + " " + this.ws + " (" + this.scale + ")");
	}

	long scale_snap_model (long mr)
	{
		// get world equivalent
		long wrld = this.get_world(mr);
		// snap world !!
		long val = this.snap_world(wrld);
		// return model equivalent
		return this.get_model(val);
	}
}
