#include <gen.h>
#include <level.h>
#include <stdio.h>
#include <stdlib.h>

/* 
	node reference for general renumbering purposes, simply holds :

	prevtile:prevnode
	newtile:newnode

	indexed on prevtile:prevnode 
*/
linkref_t *linkref_ist()
{
	linkref_t *cp = (linkref_t *)calloc(sizeof(linkref_t),1);
	if (!cp) return NULL;

	cp->elms=NULL;

	return cp;
}

void linkref_elm_rls(const void *resultp, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	linkrefelm_t *cep = *(linkrefelm_t **)resultp;
	free(cep);
}

void linkref_walk(linkref_t *cp, void (*fnc)(const void *,const VISIT,const int))
{
	if (!cp) return;
	twalk(cp->elms,fnc);
}

void linkref_free(void *data)
{
	free(data);
}

void linkref_rls(linkref_t *cp)
{
	if (!cp) return;

	//twalk(cp->elms,linkref_elm_rls);

	if (cp->elms) tdestroy(cp->elms,linkref_free);
	free(cp);
}

int linkelm_cmp(const void *a, const void *b)
{
	const linkrefelm_t *A=(const linkrefelm_t *)a;
	const linkrefelm_t *B=(const linkrefelm_t *)b;

	//printf("Testing %d:%d and %d:%d\n", A->tile, A->id, B->tile, B->id);
	// if tile -1 it means we are serching on openstreetmap id
	//if (A->tile >=0 && B->tile >=0) {
	//	if (A->tile < B->tile) return -1;
		//if (A->tile > B->tile) return  1;	
	//}
	if (A->id < B->id) return -1;
	if (A->id > B->id) return  1;
	return 0;
}

int linkref_len(linkref_t *cp)
{
	if (!cp || !cp->elms) return 0;
	return cp->nelm;
}

linkrefelm_t *linkref_find(linkref_t *cp, int32_t tile, int32_t id)
{
	linkrefelm_t tce={0};
	linkrefelm_t *tcep=NULL;
	linkrefelm_t *thelper;

	tce.tile=tile;
	tce.id=id;
	thelper = &tce;

	tcep = (linkrefelm_t *)
	tfind(thelper,(void *const*)&cp->elms,linkelm_cmp);
	return tcep;
}

linkrefelm_t *linkref_get(linkref_t *cp, int32_t tile, int32_t id)
{
	linkrefelm_t tce={0};
	linkrefelm_t *tcep;
	linkrefelm_t *thelper;

	tce.tile=tile;
	tce.id=id;
	thelper = &tce;
	tcep = (linkrefelm_t *)tfind((void *)thelper,(void * const *)&cp->elms,linkelm_cmp);

	if (tcep) {
		//printf("Found %d:%d\n", tile, id);
		return (*(linkrefelm_t **)tcep);
	} 

	return NULL;
}

void link_ref_dmp(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	linkrefelm_t *tp = *(linkrefelm_t **)nodep;
	printf("%d:%d pointer -> %p \n", tp->tile, tp->id, tp->nd );
	//wlk_tile_dmp(0, tp->node,NULL);
}

void linkref_dmp(linkref_t *cp)
{
	printf("--------- linkref --------\n");
	twalk(cp->elms,link_ref_dmp);
}

void linkref_del(linkref_t *cp, linkrefelm_t *te)
{
	// Sorted !
	tdelete ((void *) te, &cp->elms, linkelm_cmp);
	cp->nelm--;

	//printf("Deleted --------- %d\n", te->tileno);
	//linkref_dmp(cp);
	//printf("Done --------- %d\n", te->tileno);
}

void linkref_set(linkref_t *cp, int32_t tile, int32_t id, node_t *nd)
{
	// Sorted !
	linkrefelm_t *tcep;

	tcep = linkref_get(cp,tile,id);

	if (!tcep) 
		tcep = (linkrefelm_t *)calloc(sizeof(linkrefelm_t),1);

	//printf("Adding %d:%d to linkref\n", prevtile,prevnode);
	tcep->tile = tile;
	tcep->id = id;
	tcep->nd = nd;
	//tcep->cost = cost;
	tcep = *(linkrefelm_t **)tsearch((void *)tcep,&cp->elms,linkelm_cmp);
	cp->nelm++;

	//printf("Added --------- %d\n", tcep->cost);
	//linkref_dmp(cp);
	//printf("Done --------- %d\n", tcep->cost);
}
