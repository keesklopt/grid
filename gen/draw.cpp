#include <gen.h>
#include <draw.h>

int Draw::AddNode(int x, int y, void *hdl)
{
	//printf("Called Base AddNode\n");
	//nds.push_back(DrawNode(x,y,hdl));
	DrawNode dn = DrawNode(x,y,hdl);
	nds[dn] = dn;
	
	return nds.size();
}

int Draw::AddEdge(int x1, int y1, int x2, int y2, unsigned int len, void *hdl)
{
	DrawEdge de = DrawEdge(x1,y1,x2,y2, len,hdl);
	eds[de] = de;
	
	return eds.size();
}
