#include <gen.h>
#include <level.h>
#include <fcntl.h>

bigref_t *bigref_ist(gen_t * gp)
{
	//char fname[100];
	bigref_t *ni = (bigref_t *)calloc(sizeof(bigref_t),1);
	if (!ni) return NULL;

	ni->gen=gp;
	ni->tree = treemap_ist(bigrefelm_cmp);
	ni->nelm=0;
	//sprintf(fname, "%s/%s/network.nix",ni->gen->outdir,ni->gen->ibase);
	//ni->fp = fopen(fname,"w");

	return ni;
}

void bigref_rls(bigref_t *ni)
{
	if (!ni) return;
	if (ni->fp) fclose(ni->fp);
	if (ni->tree) {
		treemap_rls(ni->tree,1);
	}

	// no sorting yet, since all nodes are read before the roads !
	// so we can sort after reading all nodes
	
	free(ni);
}

void bigref_del(bigref_t *cp, int id)
{
	bigrefelm_t ne;

	ne.id=id;

	treemap_del(cp->tree,(void *)&ne);
	cp->nelm--;

	printf("Deleted --------- %d %d left\n", ne.id, cp->nelm);
	//noderef_dmp(cp);
	//printf("Done --------- %d\n", te->tileno);
}

void bigref_set_offset(bigref_t *ni, int32_t id, int offset)
{
	bigrefelm_t *nep;
	nep = (bigrefelm_t *)bigref_get(ni,id);
	if (!nep) return ;
	nep->offset = offset;
}

int bigref_put(bigref_t *ni, node_t node,int tile)
{
	bigrefelm_t *nep;

	nep = bigref_get(ni,node.id);

	if (!nep) 
		nep = (bigrefelm_t *)calloc(sizeof(bigrefelm_t),1);

	nep->id=node.id;
	nep->tile = tile;
	nep->x= node.x;
	nep->y= node.y;
	nep->offset = node.offset;
	nep->keep = 0;
	nep->done = 0;

	nep = (bigrefelm_t *)treemap_add(ni->tree, (void *)nep);

	//printf("Putting %d %d %d\n", tile, nep->x,nep->y);

	ni->nelm++;
	//ni->arr[n] = ne;

	//bigref_dump(ni,0);
	return 0;
}

void node_dmp(node_t n)
{
	printf("%d:%d (%d,%d) stat %d roads[%d]\n", 
		n.tile->num, n.id, n.x, n.y, n.keep, n.nroads);
}

int bigrefelm_cmp(const void *a, const void *b)
{
	bigrefelm_t *A=(bigrefelm_t *)a;
	bigrefelm_t *B=(bigrefelm_t *)b;

	if (A->id > B->id) return -1;
	if (A->id < B->id) return  1;
	return 0;
}

bigrefelm_t *bigref_get(bigref_t *ni, int id)
{
	bigrefelm_t ne, *nep;

	ne.id=id;

	nep = (bigrefelm_t *)treemap_get(ni->tree,(void *)&ne);
	return nep;
}

int bigref_gettile(bigref_t *ni, int32_t id)
{
	bigrefelm_t *nep;
	nep = bigref_get(ni,id);
	if (!nep) return -1;
	return nep->tile;
}

int bigref_write(int i, bigref_t *ni, vrt_t vrt,int tile)
{
	//write_comment(i, ni->fp, "idx: ");
	io.write_uint32(OUT_DEC, ni->fp, vrt.id);
	io.write_comment(OUT_DEC, ni->fp, ",");
	//write_uint32(i, ni->fd, vrt.tile);
	//write_comment(i, ni->fd, ",");
	io.write_uint32(OUT_DEC, ni->fp, tile);
	io.write_comment(OUT_DEC, ni->fp, "\n");

	return 0;
}
