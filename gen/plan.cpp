#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <plan.h>
#include <math.h>

#define MAXLINE 10000
truck_t *trucks;
int ntrucks=0;

order_t *orders;
int norders=0;

trip_t *trips;
int ntrips=0;

location_t *locs;
int nlocs=0;

order_t *comp_order=NULL;

int pcmp(const void *a, const void *b)
{
	location_t *A=(location_t *)a;
	location_t *B=(location_t *)b;

	return strcmp(A->pc, B->pc);
}

int ocmp(const void *a, const void *b)
{
	order_t *A=(order_t *)a;
	order_t *B=(order_t *)b;

	//printf("Testing %d against %d\n", A->id, B->id);

	return A->id - B->id;
}

int tcmp(const void *a, const void *b)
{
	order_t *A=(order_t *)a;
	order_t *B=(order_t *)b;

	// just take euclidian dist
	double deltaLat = A->dLat-comp_order->dLat;
	double deltaLon = A->dLon-comp_order->dLon;

	double da= sqrt((deltaLat * deltaLat) + (deltaLon * deltaLon));

	deltaLat = B->dLat-comp_order->dLat;
	deltaLon = B->dLon-comp_order->dLon;
	double db= sqrt((deltaLat * deltaLat) + (deltaLon * deltaLon));

	printf("Testing %f against %f\n", da, db);
	return A->id - B->id;
}

void sort_trips(order_t *subject)
{
	comp_order = subject;
	qsort(trips,ntrips,sizeof(trip_t),tcmp);
}

location_t *location_find(char *pc)
{
	location_t *record=NULL;
	location_t  src;

	strcpy(src.pc, pc);

	record = (location_t *)bsearch(&src,locs,nlocs,sizeof(location_t),pcmp);
	return record;
}

order_t *order_find(char *id)
{
	order_t *record=NULL;
	order_t  src;

	src.id = atoi(id);

	record = (order_t *)bsearch(&src,orders,norders,sizeof(order_t),ocmp);
	return record;
}

void add_trip(char *line)
{
	int n = norders;
	struct tm Tm={0};
	
	char part[100]={0};

	ntrips ++;
	if (ntrips==1) {
		trips = (trip_t *)calloc(sizeof(trip_t),ntrips);
	} else {
		trips = (trip_t *)realloc(trips,sizeof(trip_t)*ntrips);
	}
	
	strncpy(&part[0], &line[13], 10);
	part[10]='\0';
	//printf("%s\n", part);
	trips[n].oid = atoi(part);
	strncpy(&part[0], &line[24], 3);
	part[3]='\0';
	trips[n].tripnr = atoi(part);
	//printf("%s\n", part);
	strncpy(&part[0], &line[27], 3);
	part[3]='\0';
	//printf("%s\n", part);
	trips[n].seqnr = atoi(part);

	// construct date 
	strncpy(&part[0], &line[56], 2);
	part[2]='\0';
	Tm.tm_mday = atoi(part);
	strncpy(&part[0], &line[58], 2);
	part[2]='\0';
	Tm.tm_mon = atoi(part)-1;
	strncpy(&part[0], &line[60], 4);
	part[4]='\0';
	Tm.tm_year = atoi(part)-1900;

	strncpy(&part[0], &line[30], 2);
	part[2]='\0';
	Tm.tm_hour = atoi(part);
	strncpy(&part[0], &line[32], 2);
	part[2]='\0';
	Tm.tm_min = atoi(part);
	//printf("%s\n", part);

	trips[n].when = mktime(&Tm);
	//printf("das %ld\n", trips[n].when);
	//trips[n].date = atoi(part);
}

void add_order(char *line)
{
	int n = norders;
	char *puPC;
	double puX, puY;
	
	location_t *lp;
	char part[100]={0};
	norders ++;
	if (norders==1) {
		orders = (order_t *)calloc(sizeof(order_t),norders);
	} else {
		orders = (order_t *)realloc(orders,sizeof(order_t)*norders);
	}
	
	strncpy(&part[0], &line[0], 10);
	part[10]='\0';
	orders[n].client = atoi(part);
	strncpy(&part[0], &line[30], 10);
	part[10]='\0';
	//printf("oid is %s\n", part);
	orders[n].id = atoi(part);

	strncpy(&part[0], &line[282], 7);
	part[7]='\0';
	//printf("depot is %s\n", part);

	if (strcmp(part, (char *)"SP-SH01") == 0) {
		puPC = (char *)"1119NT";
    	puY =52.279757;
    	puX =4.763550;
	} else 
	if (strcmp(part, (char *)"SP-SH02") == 0) {
    	puPC = (char *)"5683CV";
    	puY =51.498919;
    	puX =5.402613;
	} else {
		printf("Unknown depot at line %d (FIX NOW !!)\n", norders);
		exit(0);
	}

	strncpy(&part[0], &line[372], 6);
	part[6]='\0';
	//printf("pc is %s\n", part);

	lp = location_find(part);
	if (!lp) {
		printf("could not find postcode %s at line %d (FIX NOW !!)\n", part, norders);
		exit(0);
	}

	

	//orders[n].  pLat;
	//pLon;
	//dLat;
	//dLon;
}


void add_postcode(char *line)
{
	int n = nlocs;
	char *tok;
	nlocs ++;
	if (nlocs==1) {
		locs = (location_t *)calloc(sizeof(location_t),nlocs);
	} else {
		locs = (location_t *)realloc(locs,sizeof(location_t)*nlocs);
	}

	tok = strtok(line, ",");
	tok = strtok(NULL, ",");
	strncpy(locs[n].pc, tok,10);
	tok = strtok(NULL, ",");
	locs[n].lat= atof(tok);
	tok = strtok(NULL, "\n");
	locs[n].lon= atof(tok);
}

void add_truck(char *line)
{
	int n = ntrucks;
	char *tok;
	ntrucks ++;
	if (ntrucks==1) {
		trucks = (truck_t *)calloc(sizeof(truck_t),ntrucks);
	} else {
		trucks = (truck_t *)realloc(trucks,sizeof(truck_t)*ntrucks);
	}

	tok = strtok(line, "\"");
	strncpy(trucks[n].id, tok,100);
	tok = strtok(NULL, "\"");
	tok = strtok(NULL, "\"");
	strncpy(trucks[n].code, tok,100);
	tok = strtok(NULL, "\"");
	tok = strtok(NULL, "\"");
}

void read_orders(void)
{
	char line[MAXLINE];
	FILE *fp;
	fp = fopen("order.txt", "r");
	if (!fp) {
		printf("Could not open vehicle file\n");
		exit(1);
	}
	while (fgets(line,MAXLINE,fp)) {
		//printf("%s\n", line);
		add_order(line);
	}

	fclose (fp);

	qsort(orders,norders,sizeof(order_t),ocmp);
}

void read_assignments(void)
{
	char line[MAXLINE];
	FILE *fp;
	fp = fopen("trip.txt", "r");
	if (!fp) {
		printf("Could not open trip file\n");
		exit(1);
	}
	while (fgets(line,MAXLINE,fp)) {
		//printf("%s\n", line);
		add_trip(line);
	}

	fclose (fp);
}

void read_trucks(void)
{
	char line[MAXLINE];
	FILE *fp;
	fp = fopen("truck.txt", "r");
	if (!fp) {
		printf("Could not open vehicle file\n");
		exit(1);
	}
	while (fgets(line,MAXLINE,fp)) {
		//printf("%s\n", line);
		add_truck(line);
	}

	fclose (fp);
}

void read_postcodes(void)
{
	char line[MAXLINE];
	FILE *fp;
	fp = fopen("postcode.txt", "r");
	if (!fp) {
		printf("Could not open vehicle file\n");
		exit(1);
	}
	while (fgets(line,MAXLINE,fp)) {
		//printf("%s\n", line);
		add_postcode(line);
	}

	qsort(locs,nlocs,sizeof(location_t),pcmp);

	fclose (fp);
}


void plan_init(void )
{
	read_postcodes();
	read_trucks();
	read_orders();
	read_assignments();
}

#ifdef DEBUG

int main()
{
	location_t *lp;

	plan_init();

	char *pc = "5625LN";
//51.4738952,5.4740795

	lp= location_find(pc);
	if (!lp) {
		printf("No such pc found\n");
	} else {
		printf("That's %f and %f\n", lp->lat, lp->lon);
	}

	printf("Locations %d\n", nlocs);
	printf("Trucks %d\n", ntrucks);
	printf("Orders %d\n", norders);
	printf("Trips %d\n", ntrips);

	return 0;
}

#endif
