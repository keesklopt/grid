#include <grid.h>

#include <search.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <expat.h>
#include <plan.h>
#include <treemap.h>
#ifdef USING_MONGO
#include "client/dbclient.h"
#endif

using namespace std;

#define BLAH 1
#define STRINGS 1

//#define VERBOSE 0
#define _FILE_OFFSET_BITS 64

//#define TESTTILES(num) (num == 1298358098 || num == 1298358099 || num == 324589524)
//#define TESTTILES(num) (num == 298358096 || num == 324589602)
//#define TESTTILES(num) (num == 301950959)
#define TESTTILES(num) (0)

extern int speeds[];

/* to silence all logging : */
void lprintf(const char *format, ...);
//#define printf lprintf

//#define DEFAULTNETWORK "hvh"

// pure reading (gen -c) : 9m55 
// split to level 8 files : 39m
//#define DEFAULTNETWORK "europe-121121"
//#define NODES 856640000
//#define ROADS 229823586

// round jan 2013 updated
//#define DEFAULTNETWORK "europe" 
//#define NODES 856640000
//#define ROADS 229823586

//#define DEFAULTNETWORK "belarus"
//#define NODES 776891414
//#define ROADS 229823586

//#define DEFAULTNETWORK "lux"
//#define NODES 50021591
//#define ROADS 5854425

//#define DEFAULTNETWORK "netherlands"
//#define NODES 50021591
//#define ROADS 5854425

//#define DEFAULTNETWORK "great_britain"
//#define NODES 42377015
//#define ROADS 18709916

// pure reading : 33 sec
// split to level 8 files : 2m18
//#define DEFAULTNETWORK "n9"
//#define NODES 49944637
//#define ROADS 5783547

//#define DEFAULTNETWORK "bretagne"
//#define NODES 18739229
//#define ROADS 2218029

//#define DEFAULTNETWORK "baden"
//#define NODES 16283383
//#define ROADS 2643400

//#define DEFAULTNETWORK "sweden"
//#define NODES 15533077
//#define ROADS 3889512

// pure reading : 6.6 seconds
// deleting : 19 seconds
//#define DEFAULTNETWORK "belgium"
//#define NODES 10409644
//#define ROADS 3561831

// pure reading : 1.7 seconds
//#define DEFAULTNETWORK "latvia"
//#define NODES 3497558
//#define ROADS 606401

// pure reading : 0.6 sec
// split into bottom tiles : 2m30
// no writing : 3 sec
//#define DEFAULTNETWORK "luxembourg-latest"
//#define NODES 1024724
//#define ROADS 429802

// split to level 8 files : 
//#define DEFAULTNETWORK "andorra"
//#define NODES 42492
//#define ROADS 23986

// split to level 8 files : 
#define DEFAULTNETWORK "hoek"
#define NODES 17
#define ROADS 13

//#define DEFAULTNETWORK "map"
//#define NODES 17
//#define ROADS 13

//#define DEFAULTNETWORK "europe-latest"
//#define NODES 17
//#define ROADS 13

//#define DEFAULTNETWORK "small"
//#define NODES 17
//#define ROADS 13

//#define DEFAULTNETWORK "haarlem"
//#define NODES 17
//#define ROADS 13

//#define DEFAULTNETWORK "ch"
//#define NODES 6
//#define ROADS 5

//#define DEFAULTNETWORK "test1"
//#define NODES 6
//#define ROADS 5

#define RALLOCSIZE 10
//#define DAT_INDIR "."
//#define DAT_INDIR "/home/kees/projects/grid/3pty/osrm"
#define DAT_INDIR "/data/projects/osrm"
#define DAT_OUTDIR "."
//#define DAT_INDIR "/usr/local/share/klopt"
//#define DAT_OUTDIR "/usr/local/share/klopt"

//#define ROADCOST(road) road.cost
//#define ROADCOST(road) road.len
//#define ROADCOST(road) ((road.len)/speeds[(road.type)])

#define ROADCOST(road) roadcost(road)

typedef struct tileindex_tag
{
	uint32_t tile;
	uint32_t roffset;
	uint32_t noffset;
} tileindex_t;

typedef struct bigrefelm_tag
{
	int32_t id;
	int32_t tile; // could be retrieved from x/y ?
	int32_t x;
	int32_t y;
	uint32_t offset; 
	uint32_t keep : 1;
	uint32_t done : 2;
	uint32_t nroads : 29;
} bigrefelm_t;

typedef struct edgedata_tag {
   unsigned int id:31;
   unsigned int shortcut:1;
   unsigned int distance:30;
   unsigned int forward:1;
   unsigned int backward:1;
} edgedata;

typedef struct edge_tag
{
   unsigned int fromnode;
   unsigned int tonode;
   edgedata ed;
} edge;

typedef struct osrm_node_tag {
	int lat;
	int lon;
	uint32_t id;
	char bollard;
	char trafficlight;
} osrm_node;

typedef struct nodeinfo_tag {
	int lat;
	int lon;
	uint32_t id;
} nodeinfo;

typedef struct roadinfo_tag
{
	int vianode;
	unsigned nameid;
	char turn;
} roadinfo;

typedef struct restriction_tag
{
	int viaNode;
	int fromNode;
	int toNode;
	char isonly : 1;
	char u1 : 1;
	char u2 : 1;
	char u3 : 1;
	char u4 : 1;
	char u5 : 1;
	char u6 : 1;
	char u7 : 1;
} restriction_t;

// since the 4 chars in a row (2013) packing is not needed 
// but we never know when they add another field again
// still cannot figure out why they packed the roads and nnot the nodes ?
#pragma pack(push)
#pragma pack (1)
typedef struct osrm_road_tag
{
	uint32_t from;
	uint32_t to;
	unsigned int dist;
	short dir;
	int weight;
	short type;
	int nameid;
	char roundabout;
	char ignoreingrid;
	char accesrestricted;
	char contraflow;
} osrm_road;
#pragma pack (pop)

/*
typedef struct query_edge_tag
{
	int source;
	int dest;
	struct EdgeData {
        unsigned int id:31;
        unsigned int shortcut:1;
        unsigned int distance:30;
        unsigned int forward:1;
        unsigned int backward:1;
   } data;
} query_edge_t;
*/

typedef struct scale_tag
{
	int wl;
	int wr;
	int ml;
	int mr;
	double scale;
} scale_t;

//scale_t xs;
//scale_t ys;

typedef struct world_tag
{
	int l;
	int r;
	int t;
	int b;
} world_t;

typedef struct worknode_tag
{
	unsigned int offset;
	int orignode;
	int cost;
	char eval;
} worknode_t;

/* reference used in splitting the tiles into usable
  blocks, it needs to include ALL nodes, so this needs 
	to be a minimal struct */
typedef struct splitrefelm_tag
{
	int32_t id;
	int32_t tile;
} splitrefelm_t;

/* reference for renumbering nodes */
typedef struct noderefelm_tag
{
	int prevtile;
	int prevnode;
	int tile;
	int node;
} noderefelm_t;

typedef struct noderef_tag
{
	void *elms;
	int nelm;
} noderef_t;

typedef struct oref_tag
{
	int id;
	int offset;
} oref_t;

typedef struct draw_node_tag
{
	unsigned int id;
	char *name;
	int32_t x;
	int32_t y; 
	int color;
} draw_node_t;

typedef struct draw_edge_tag
{
	draw_node_t from;
	draw_node_t to;
	unsigned int  len;
	short type;
	short dir;
	short shortcut;
	char *name;
	int color;
} draw_edge_t;

typedef struct draw_context_tag
{
	unsigned int nnodes;
	unsigned int nedges;
	unsigned int nodenames;
	unsigned int roadlength;
	draw_node_t (* get_node)(draw_context_tag *,int);
	draw_edge_t (* get_edge)(draw_context_tag *,int);
	int (* get_nfwd)(void *);
	draw_edge_t (* get_fwd)(void *, int);
	//node_coord_t (* get_node_coord)(int);
	void *handle;
} draw_context_t;

// IMPORTANT !! 
// maximum level is 14, NOT 15, since tiles are numbered absolutely
// so level 0 takes up 1 integer, level 1 takes 4, etc
// that means there is no room to number level 15 because that alone
// would take up all positive integers in a long, no room for the other
// levels
// also because we only use 14 levels, we don't need unsigned longs, 
// wich is handy for languages without unsigned's like java 

// additional : the tile cache can be built using the negative numbers
// positive indices will mean hat the tile is not in memory, negative
// means the offset from the bottom of the cache

#define MAXLEVEL 14
// make a partition at level 8 because linux cannot handle millions of files
#define PARTLEVEL 8

enum genstats { GEN_EMPTY, GEN_READ, GEN_RENUMBERED, GEN_SPLIT };

enum cost_sort { SORT_COST , SORT_NODE };
// changed 10 sep 2012, drop,keep,join is a more memorable order
enum stats { STAT_DROP, STAT_KEEP, STAT_JOIN };
extern int output;

typedef struct routenoderef_tag
{
	int32_t meters;
	int32_t minutes;	
} routenoderef_t;

// ORDER_HEAP : quad-heap ordering (not implemented yet)
// ORDER_TOPDOWN : 
enum orders { ORDER_HEAP, ORDER_TOPDOWN, ORDER_BOTTOMUP };
class TileAlgo 
{                  
	public:        // 23  This is the default ordering
	int tiling[4]; // 01  fill tiling to have that changed
	int order; // see orders enum

	TileAlgo() {
		this->tiling[0]=0;
		this->tiling[1]=3;
		this->tiling[2]=1;
		this->tiling[3]=2;
		this->order=ORDER_TOPDOWN;
	}

	TileAlgo(int t[4], int o) {
		this->tiling[0]=t[0];
		this->tiling[1]=t[1];
		this->tiling[2]=t[2];
		this->tiling[3]=t[3];
		this->order=o;
	}
};

typedef struct indexer_tag
{
	unsigned int id;
	int offset;
} indexer_t;

typedef struct slice_tag
{
	int32_t x;
	int32_t y;
	int32_t level;
} slice_t;

typedef struct ref_tag
{
	uint32_t id;
	int32_t tile;
	int32_t offset;
} ref_t;

typedef struct fileparam_tag
{
	int max_line;
	int lines;
	int chars;
	char count[256];
} fileparam_t;

typedef struct vrtparam_tag
{
	//int8_t id_sorted;
	//int8_t id_double;
	//int8_t xy_sorted;
	//int8_t xy_double;
	//fileparam_t fp;
} vrtparam_t;

typedef struct sgmparam_tag
{
	int32_t max_from;
	int32_t max_to;
	int32_t id_sorted;
	int32_t id_double;
	int32_t max_len;
	int32_t nbounds;
	fileparam_t fp;
} sgmparam_t;

typedef struct vrt_tag
{
	int32_t id;
	int32_t x;
	int32_t y;
	int32_t s;
	int32_t roadcount;
	uint16_t country;
	int8_t  level;
	int8_t  type;
	int8_t  stat;
	int32_t offset;
	int32_t tile;
	char *pc;
	char *name;
} vrt_t;

typedef struct ll_tag
{
	int32_t  lat;
	int32_t  lon;
	int32_t  tile;
	int32_t  node;
} ll_t;

typedef struct pc_tag
{
	char     *name;
	int32_t  tile;
	int32_t  node;
	uint16_t country;
} pc_t;

typedef struct vrt_nm_tag
{
	char   *name;
	int32_t tile;
	int32_t offset;
} vrt_nm_t;

typedef struct way_tag
{
	unsigned int      id;
	int8_t   level;
	int8_t   type;
	int8_t   stat;
	int8_t   dir;
	int32_t  len;
	int32_t  nn; 	// number of nodes
	int32_t  *nodes;
} way_t;

typedef struct sgm_tag
{
	int32_t  from;
	int32_t  to;
	int32_t  fromtile;
	int32_t  totile;
	uint16_t country;
	int8_t   level;
	int8_t   type;
	int8_t   stat;
	int8_t   dir;
	int32_t  len;
	int32_t  offset;
	int32_t  tile;
} sgm_t;

typedef struct vrt_idx_tag
{
	int32_t id; // original
	int32_t n; // new
} vrt_idx_t;

typedef struct node_tag node_t;
typedef struct road_tag road_t;
typedef struct tile_tag tile_t;

/* struct used to uniquely identify a node */
struct node_tag
{
	uint32_t id;
	tile_t *tile;
	int32_t offset; // node index 
	/*
	int32_t firstroad;
	int32_t lastroad;
	*/
	int b; 	// border distance
	int a; // reference distance
	int p; // is a parent
	int nroads;
	int fradius;
	int bradius;
	int slack;
	road_t **rds;
	int32_t x;
	int32_t y;
	int32_t cost; 
	int32_t cost2; // for neighbourhood search
	int32_t contract_cost;
	uint8_t eval;
	uint8_t eval2;
	uint8_t keep;
	uint8_t join;
	uint8_t rise;
	uint8_t hop;
	uint8_t active;
	char depth;
	//char a;
	//char d;
	
/*
	uint32_t eval : 1;
	uint32_t keep : 1;
	uint32_t join : 1;
	uint32_t rise : 1;
	uint32_t hop  : 4;
	uint32_t del  : 16;
*/
	int32_t rank;
	treemap_t *parents;
	//int nparents;
	//node_t **parents;
	node_t *pnode;
};

typedef struct linkrefelm_tag
{
	uint32_t id;
	int tile;
	node_t *nd;
} linkrefelm_t;

typedef struct linkref_tag
{
	void *elms;
	int nelm;
} linkref_t;

/* bare essentials for nodes (used for splitting)  */
typedef struct barenode_tag
{
	uint32_t id;
	int32_t x;
	int32_t y;
} barenode_t;

/* bare essentials for calculation */
typedef struct calc_node_tag
{
	int32_t firstroad;
	int32_t lastroad;
	int32_t tile;
} calc_node_t;

typedef struct pqentry_tag
{
	node_t *nd;
	int32_t cost;
} pqentry_t;

/* struct used to uniquely identify a node */
struct road_tag
{
	node_t *nodefrom; 
	//int32_t fromid;
	//tile_t *tileto;
	//int32_t nodeto;
	node_t *nodeto;
	//int32_t toid;
	int32_t cost;
	// earth radius is 6371000 so a road around the world would be
	// 6371000 * 2 * 3.14 = 40028993 km 
	// largest value in int 4294967295 using meters that would be
	// 4294967.295 km as largest road (ferry)
	// dm would be 429496 km
	// cm would be 42949 km  
	// maybe even 16 bits and split some more ?
#define ROAD_MAX 8388607
	uint32_t len: 23; // in dm length max is 8388607 (838 km) or meters 8388 km
	uint32_t type: 4;
	uint32_t dir: 2;
	uint32_t done: 1;
	uint32_t keep: 1;
	uint32_t rise: 1;
};

/* bare essentials for roads (used for splitting)  */
typedef struct bareroad_tag
{
	int32_t tile;
	int32_t from;
	int32_t tileto;
	int32_t nodeto;
	//int32_t len; // len can be generated 
	int8_t  dir;
	int8_t  type;
} bareroad_t;

typedef struct tileref_tag tileref_t;

typedef struct tilerefelm_tag
{
	int32_t tile;
	barenode_t *nds;
	int nnodes;
	bareroad_t *rds;
	int nroads;
	tileref_t *tr;
} tilerefelm_t;

extern road_t road_dontmatch;
extern node_t node_dontmatch;

typedef struct pathelm_tag {
	node_t *n;
	road_t *r;
} pathelm_t;

typedef struct path_tag
{
	int nelms;
	pathelm_t *elms;
} path_t;
	
typedef struct tile_tag
{
	int8_t  level;
	int8_t  done;
	int8_t  dirty;
	int32_t num;
	int32_t parent;
	int32_t nnodes;
	int32_t nroads;
	node_t **nds;
	road_t **rds;
	int32_t offset; // for renumbering on higher level
	void *hook;
} tile_t;

enum cache_stat { CACHE_CLEAN, CACHE_DIRTY };

typedef struct tcache_tag tcache_t;

typedef struct tcentry_tag
{
	tcache_t *tc;
	int32_t tileno;
	tile_t *tile;

	uint32_t stamp : 29;
	uint32_t clean : 1;
	uint32_t done : 1;
	uint32_t incore : 1;
} tcentry_t;

typedef struct tcarray_tag
{
	int len;
	tcentry_t **items;
} tcarray_t;

#define TCACHE_INMEM  10
#define TCACHE_ONDISK 11

typedef int (* path_fnc_t)(int level,node_t*, road_t *,void *);

typedef struct td_tag
{
	int32_t meters;
	int32_t minutes;
	int32_t cost;
} td_t;

// country codes
typedef struct ccc_tag {
	uint16_t  telcode;
	char     *name;
	char     *carcode;
} ccc_t;

typedef struct gen_tag gen_t;

extern gen_t gen;

enum tcache_type { TC_RDONLY, TC_WRONLY, TC_RDWR };

#define NFILES 8

struct tcache_tag 
{
	int max;
	int incore;
	int stamp; // 'time' stamp for the tiles
	int level;
	tcache_type type;
	void *tcs; // gnu binary tree (tiles)
	void *idx; // gnu binary tree (index)
	gen_t *gen;
	int nmarks;
	tcentry_t **marks; // seperately mark tiles, for deletion etc
	int worklevel;
	tcarray_t arr;
	node_coord_t *coords;
	// level files 
	uint32_t noffset;
	uint32_t roffset;
	FILE *fps[NFILES]; // index, nodes,roads,coords,revcoord,params,uplinks,downlink
	void *hook; // general purpose

	stat_t stat;
} ;

struct tileref_tag
{
	int nelm;
	treemap_t *tree;
	gen_t *gen;
	int max; // 
	int count; // flush if above max
};

typedef struct bigref_tag
{
	FILE *fp;
	int nelm;
#ifndef TREE_VERSION
	treemap_t *tree;
#else
	bigrefelm_t *array;
#endif
	gen_t *gen;
} bigref_t;

typedef struct splitref_tag
{
	FILE * fp;
	int nelm;
#ifdef TREE_VERSION
	treemap_t *tree;
#else
	splitrefelm_t *array;
#endif
	gen_t *gen;
} splitref_t;

struct gen_tag
{
	int32_t output;
	int32_t max_len;

	bigref_t *br;
	splitref_t *sr;
	tileref_t *tr;
	noderef_t *nr;
	linkref_t *lr;

	char *indir;
	char *outdir;
	char *ibase;
	char *obase;

	FILE *pbfp;
	FILE *xmlp;
	// OR :
	FILE *osmp;

	stat_t stat;

	int32_t nnodes;
	int32_t nways;
	int32_t nroads;
	int32_t ntiles;
	int32_t ncountries;
	//int32_t po;
	// int levels (don't bother, always 16)
	// since empty tiles take no space and level 5 is almost always used
	
	vrt_t *vrt_arr;
	way_t *way_arr;
	sgm_t *sgm_arr;
	ccc_t *ccc_arr;
	vrt_idx_t *vrt_idx;
	vrtparam_t vp;
	sgmparam_t sp;
};

typedef struct heapentry_tag heapentry_t;

struct heapentry_tag
{
	void *node;
	heapentry_t *next;
} ;

typedef int (*heap_cmp_fnc)(const void *,const void *);
typedef int (*heap_getcost_fnc)(const void *);
typedef void (*heap_setcost_fnc)(const void *,int);
typedef int (*heap_getflag_fnc)(const void *, int);
typedef void (*heap_setflag_fnc)(const void *,int, int );

typedef struct heap_tag heap_t;

struct heap_tag
{
	int32_t part;
	int32_t bot;
	int32_t top;
	int32_t phys;
	heapentry_t **data;
	heap_cmp_fnc cmp;
	heap_getcost_fnc getcost;
	heap_setcost_fnc setcost;
	heap_setflag_fnc setflag;
	heap_getflag_fnc getflag;
} ;

// convenience struct for passing the heap functions
typedef struct heapfnc_tag
{
	heap_cmp_fnc cmp;
	heap_getcost_fnc getcost;
	heap_setcost_fnc setcost;
	heap_setflag_fnc setflag;
	heap_getflag_fnc getflag;
} heapfnc_t;

typedef struct costentry_tag
{
	node_t *node;
	int32_t cost;
} costentry_t;

typedef struct cost_tag
{
	treemap_t *tm;
} cost_t;

//typedef int (* path_fnc_t)(net_t *, node_t*, road_t *,void *);
//#ifndef GENERATING_PROTO
//#include <net_proto.h>
//#endif

/* linkref.cpp */
linkref_t *linkref_ist();
void linkref_elm_rls(const void *resultp, const VISIT which, const int depth);
void linkref_walk(linkref_t *cp, void (*fnc)(const void *,const VISIT,const int));
void linkref_free(void *data);
void linkref_rls(linkref_t *cp);
int resultelm_cmp(const void *a, const void *b);
int linkref_len(linkref_t *cp);
linkrefelm_t *linkref_find(linkref_t *cp, int32_t tile, int32_t id);
linkrefelm_t *linkref_get(linkref_t *cp, int32_t tile, int32_t id);
void linkref_dmp(linkref_t *cp);
void linkref_del(linkref_t *cp, linkrefelm_t *te);
void linkref_set(linkref_t *cp, int32_t tile, int32_t id, node_t *nd);

/* gen.cpp */
void double_roads(const void *nodep, const VISIT which, const int depth);
int node_contract(tcache_t *tc,node_t *np,int L);
void join(const void *nodep, const VISIT which, const int depth);
void renum(const void *nodep, const VISIT which, const int depth);
void selective_dump(tile_t *tp);
void gen_types(gen_t *g);
int combine_level(gen_t *g, int level, tcarray_t *lc);
int hh_step_1(gen_t *g, int level, tcarray_t *lc);
int combine_level_new(gen_t *g, int level, tcarray_t *lc);
int combine_level_old(gen_t *g, int level, tcarray_t *lc);
void renumber_nodes(bigref_t *br, tcache_t *tc);
void renumber_roads(bigref_t *br, tcache_t *tc);
void write_indices(bigref_t *br, tcache_t *tc);
void gen_dice_splitset(gen_t *g);
void gen_fix(gen_t *g);
void gen_dice_splitset2(gen_t *g);
int init_mongodb();
int8_t classify_highway(const char *name);
void gen_rls(gen_t *g);
int vrt_id_cmp(const void *a, const void *b);
int vrt_xy_cmp(const void *a, const void *b);
int vrt_tile_cmp(const void *a, const void *b);
int vrt_cmp(const void *a, const void *b);
int vrt_idx_cmp_noisy(const void *a, const void *b);
int vrt_idx_cmp(const void *a, const void *b);
int rtype_cmp(const void *a, const void *b);
void init_highways();
int gen_test_inpoly(int32_t nvert, float *vertx, float *verty, float testx, float testy);
uint32_t *calcsteps(int depth);
void gen_set_output(gen_t *g, int val);
void gen_set_ibase(gen_t *g, char *base);
void gen_set_obase(gen_t *g, char *base);
void gen_set_dir(gen_t *g, char *dir,char *);
int gen_scandir(gen_t *g);
void gen_reduce(gen_t *g, int32_t tile);
void gen_bypass(gen_t *g, int32_t tile);
void fp_dmp(fileparam_t fp);
void renumber_nodes(bigref_t *ni, tcache_t *tc);
void renum_rds(tile_t *tp);
void renum_nds(tile_t *tp);
void recount_roads(bigref_t *ni, tcache_t *tc);
void starthandler(void *udata, const XML_Char *name, const XML_Char **atts);
void endhandler(void *udata, const XML_Char *name);
void gen_analyze_fmt_osm(gen_t *gp);
void gen_read_fmt_osm(gen_t *gp);
int check_sgm_and(sgmparam_t *sp, char *buf);
int gen_init();
int gen_analyze_osm(gen_t *g);
int gen_check_connected(gen_t *g);
int32_t float_2_int(const char *num);
int32_t float_2_int_orig(char *num);
rect_t boundbox(int tile);
int vrt_sort(gen_t *g, int (*cmp)(const void *,const void *) );
int gen_mark_unused(gen_t *g);
void gen_clean(gen_t *g);
void gen_prune(gen_t *g);
int gen_renumber(gen_t *g);
int sgm_tile_cmp(const void *a, const void *b);
int sgm_cmp(const void *a, const void *b);
int sgm_sort(gen_t *g, int (*cmp)(const void *,const void *) );
int tile_2_level(unsigned int tile);
uint32_t calctile(uint16_t x, uint16_t y, int level);
int pcvrt_cmp(const void *left, const void *right, void *xtra);
int pcvrt_cmp2(const void *left, const void *right);
int gen_calc_lengths(gen_t *g);
int gen_read_osm(gen_t *g);
int gen_read_pbf(gen_t *g);
int gen_read(gen_t *g);
int gen_minmax(gen_t *g);
int gen_names(gen_t *g);
int gen_read_hh(gen_t *g);
int gen_hh(gen_t *g);
void gen_hh(gen_t *g, int32_t tile);
void gen_reduce_slow(gen_t *g);
void gen_dmp(gen_t *g);
void gen_statistics(gen_t *g);
void gen_dice(gen_t *g);
void gen_split(gen_t *g);
tile_t *gen_random_tile(gen_t *g, rect_t bbox, int nds, int rds);
void gen_random(gen_t *g, char *name);
void fix_links(const void *nodep, const VISIT which, const int depth);

/* tile.cpp */
int overlap(path_t *P, cost_t *Ns1, cost_t *Np);
road_t *road_back_road(road_t *fwd, node_t *orig);
//road_t *node_back_road(node_t *nodeto, node_t *orig);
void tile_del_road(tile_t *tp, road_t *rd);
void tile_renum(tile_t *tp);
void tile_join(tile_t *tp);
void tcache_reduce(tcache_t *tc);
void tcache_hh_1(tcache_t *tc);
node_t *node_dup(node_t *orig);
void tile_relink (tile_t *tnew, tile_t *orig);
void tile_link_roads(tile_t *tp);
int32_t level_traverse_path(int level, node_t *from, node_t *to,path_fnc_t what, void*, int);
void tile_reset(tile_t *tp);
void tile_reset2(tile_t *tp);
void tile_link(tile_t *tp);
void tile_check_reset(tile_t *tp);
int32_t level_edsger(int level, node_t *from, node_t *to,int costlimit, int hoplimit);
int32_t level_promote(int level, cost_t *cost, node_t *from, int h);
int32_t level_reset_edsger(int level, node_t *to);
void level_reset_keep(int l);
void tile_contract(tile_t *tp);
void tile_reverse_peers(tile_t *tp);
int node_get_keep_roads(tile_t *tp, node_t *nd);
void draw_open(void);
void draw_close(void);
void tile_draw(tile_t *tp);
void tile_null_peers(tile_t *tp);
void tile_set_peers(tile_t *tp);
void tile_dmp_peers(tile_t *tp);
void tile_sort(tile_t *tp);
void tile_create_links(noderef_t *,tile_t *tp);
void tile_double_roads(tile_t *tp);
tile_t *tile_dup (tile_t *orig,int newnum);
tile_t *tile_dup_peered (tile_t *orig,int newnum);
int tile_remove_doubles(tile_t *tp);
void tile_prune(tile_t *tp);
void tile_prune2(tile_t *tp);
void tile_fix(tile_t *tp);
void tile_reset_links(noderef_t *nr,tile_t *tp);
void tile_create_links(noderef_t *nr,tile_t *tp);
void tile_reset_keep(tile_t *tp,int keep);
tile_t *tile_read_parts(tcache_t *tc,int tileno);
int tile_check(tile_t *tp);
void tile_fix_externals(noderef_t *nr, tile_t *tp);
tile_t *tiles_add(gen_t *nr, tile_t **tiles, int,int32_t);
tile_t *tiles_add_into_parent(noderef_t *nr, tile_t **tiles, int,int32_t);
void tile_add(tile_t *dst, tile_t *src);
int find_endpoints(tile_t *tp, node_t *from, node_t *endnodes[2], int endroads[2]);
int whipe_deadends(tile_t *tp);
cost_t *tile_edsger_cost(tile_t *tp, node_t *from, node_t *to);
cost_t *level_edsger_cost_bwd(int level, node_t *from,int H);
cost_t *level_edsger_cost_fwd(int level, node_t *from,int H);
cost_t *level_edsger_cost(int level, node_t *from,int H,int testdir);
cost_t *level_edsger_cost_HH(int level, node_t *from, int h);
int32_t level_edsger_radius(int level, node_t *from, int h, int dir);
int32_t tile_edsger(tile_t *tp, node_t *from, node_t *to);
int32_t tile_mark_path_cost(tile_t *tp, cost_t *,node_t *from, node_t *to);
int32_t tile_mark_path(tile_t *tp, node_t *from, node_t *to);
int32_t tile_test_path_cost(tile_t *tp, cost_t *cost, node_t *from, node_t *to);
int32_t tile_test_path(tile_t *tp, node_t *from, node_t *to);
road_t tile_nth_road(tile_t *tp, node_t nd, int count);
void tile_reindex_roads(tile_t *tp);
void tile_fixrefs(tile_t *tp);
void road_dmp(road_t r);
uint32_t xy_2_tile(TileAlgo *ta, point_t xy, int level);
slice_t tile_2_xy(TileAlgo *ta, unsigned int n, int depth);
void tile_node_shrink(tile_t *tp,int offset);
tcentry_t *tcache_find(tcache_t *tc, uint32_t tileno);
void tile_node_sort(tile_t *tp);
void tile_road_sort(tile_t *tp);
rect_t xy_2_ll(point_t xy, int level);
point_t ll_2_xy(uint32_t lat, uint32_t lon, int level);
int32_t ll_2_tile(int32_t lat, int32_t lon, int level);
tile_t *tile_ist(int32_t tnum);
void tile_rls(tile_t *tp);
void wlk_tile_dmp(intptr_t size, const void *elm, void *extra);
void tile_dmp(tile_t *tp,int);
void tile_draw(tile_t *tp,int);
int write_road(int i, FILE * fp, road_t rp);
int write_tile(int i, FILE * fp, tile_t *tp);
int tcache_write_parts(tcache_t *tc, tile_t *tp);
int node_cmp(const void *a, const void *b);
int road_cmp(const void *a, const void *b);
node_t tile_add_vrt(tile_t *tp, vrt_t vrt);
int32_t gen_tile_add_node(gen_t *,tile_t *tp, node_t nd);
int32_t tile_add_node(tile_t *tp, node_t *nd);
void tile_add_sgm(tile_t *tp, sgm_t sgm);
int tile_add_road(tile_t *tp, road_t *rd);
void tile_add_bareroad(gen_t *gp, tile_t *tp, bareroad_t rd, double);
void tile_reduce(tile_t *tp);
void tile_simplify(tile_t *tp, tile_t *parent);
void tile_rise_nodes(tile_t *tp, tile_t *parent);
void tile_pass_nodes(tile_t *tp);
void tile_pass_nodes_4(tile_t *tp);
void tile_pass_nodes_9(tile_t *tp);

/* bigref.cpp */
bigref_t *bigref_ist(gen_t * gp);
void bigref_rls(bigref_t *ni);
void bigref_set_offset(bigref_t *ni, int32_t id, int offset);
int bigref_put(bigref_t *ni, node_t node,int tile);
int bigref_put_vrt(bigref_t *ni, vrt_t vrt,int tile);
int bigrefelm_cmp(const void *a, const void *b);
void bigref_sort(bigref_t *ni);
bigrefelm_t *bigref_get(bigref_t *ni, int id);
void bigref_dmp(bigref_t *nt, int limit);
int bigref_gettile(bigref_t *ni, int32_t id);
int bigref_write(int i, bigref_t *ni, vrt_t vrt,int tile);
void bigref_del(bigref_t *cp, int id);

/* splitref.cpp */
splitref_t *splitref_ist(gen_t * gp);
void splitref_rls(splitref_t *ni);
void splitref_set_offset(splitref_t *ni, int32_t id, int offset);
int splitref_put(splitref_t *ni, node_t node,int tile);
int splitref_put_vrt(splitref_t *ni, vrt_t vrt,int tile);
int splitrefelm_cmp(const void *a, const void *b);
void splitref_sort(splitref_t *ni);
splitrefelm_t *splitref_get(splitref_t *ni, int32_t id);
void splitref_dmp(splitref_t *nt, int limit);
int splitref_gettile(splitref_t *ni, int32_t id);
int splitref_write(int i, splitref_t *ni, vrt_t vrt,int tile);

/* tcache.cpp */
int net_intrand(int lo, int hi);
int net_rand(double lo, double hi);
tcache_t *tcache_elevate(tcache_t *tc);
void tcache_check_reset(tcache_t *tc);
void tcache_reset(tcache_t *tc);
void tcache_reset_keep(tcache_t *tc, int k);
void tcache_reset2(tcache_t *tc);
tcache_t *tcache_dup(tcache_t *tc);
tcache_t *tcache_dup_peered(tcache_t *tc);
int tcache_write_links(tcache_t *tc, tile_t *tp);
void tcache_put_links(tcache_t *tc,int level);
void dump_stats(stat_t *stat);
void reset_stats(stat_t *stat);
void add_stats(stat_t *dst, stat_t *src);
int tcache_stats(tcache_t *tc, tile_t *tp);
void tcache_write_node(tcache_t *tc, node_t nd);
void tcache_write_road(tcache_t *tc, road_t rd);
void tcache_write_index(tcache_t *tc, tile_t *tp);
void tcache_empty(tcache_t *tc);
void tcache_put_level(tcache_t *tc, uint32_t tileno, tile_t *tp);
void tcache_put_all(tcache_t *tc,int level);
void tcache_add_offset(tcache_t *tc, offsets_t *te);
void tcache_exit_level(tcache_t *tc);
void tcache_init_level_read(tcache_t *tc,int level);
void tcache_init_level_write(tcache_t *tc,int level);
void tcache_check(tcache_t *tc);
void tcache_del_level(tcache_t *tc);
tcarray_t tcache_get_array(tcache_t *tc);
void tcache_reset(tcache_t *tc);
void tcache_reset_shortest(tcache_t *tc);
void tcache_reset_fastest(tcache_t *tc);
void reset_cost(const void *elm, const VISIT which, const int depth);
void tcache_walk(tcache_t *tc, void (*)(const void *,const VISIT,const int));
void tcache_walk_index(tcache_t *tc, void (*fnc)(const void *,const VISIT,const int));
void tcache_rls(tcache_t *tc);
tcache_t *tcache_ist(gen_t *gp,int level, int max,tcache_type);
int tc_cmp(const void *a, const void *b, void *dummy);
int tcache_len(tcache_t *tc);
void tcache_put(tcache_t *tc, uint32_t tileno, tile_t *tp);
tile_t *tcache_get(tcache_t *tc, uint32_t tileno);
tile_t *tcache_get_at(tcache_t *tc, uint32_t pos);
void tce_dmp(intptr_t size, const void *elm, void *extra);
void tcache_dmp(tcache_t *tc);
void tcache_prune(tcache_t *tc);
void tcache_add(tcache_t *tc, tcentry_t *te);
void tcache_add_tile(tcache_t *tc, tile_t *te);
void tcache_del(tcache_t *tc, tcentry_t *te);

/* grid.cpp */

road_t *read_road(tile_t *tp,tcache_t *,FILE * fp);
node_t *read_node(tcache_t *,FILE * fp);

/* heap.cpp */
void *heapnode_ist(int cost, void *nd);
heap_t *heap_ist(int part, heapfnc_t fncs);
void heap_del(heap_t *hp, void *hd);
void heap_dmp(heap_t *hp);
void heap_add(heap_t *hp, void *hn);
void heap_prune(heap_t *hp);
void *heap_get(heap_t *hp, int num);
int heap_empty(heap_t *hp);
void heap_rls(heap_t *hp);

/* cost.cpp */
void cost_rls(cost_t *cp);
costentry_t *cost_get(cost_t *cp, node_t *nd);
void cost_dmp(cost_t *cp);
costentry_t *cost_find(cost_t *cp, node_t* nd);
cost_t *cost_ist(int);
void cost_add(cost_t *cp, node_t *nd, int cost);
void cost_set(cost_t *cp, node_t *nd, int cost);
void cost_walk(cost_t *cp, void (*fnc)(const void *,void *), void *udata);

/* result.cpp */
noderef_t *noderef_ist();
void noderef_elm_rls(const void *resultp, const VISIT which, const int depth);
void noderef_walk(noderef_t *cp, void (*fnc)(const void *,const VISIT,const int));
void noderef_rls(noderef_t *cp);
int noderef_elm_cmp(const void *a, const void *b);
int noderef_len(noderef_t *cp);
noderefelm_t *noderef_find(noderef_t *cp, int prevtile, int prevnode);
noderefelm_t *noderef_get(noderef_t *cp, int prevtile, int prevnode);
void node_ref_dmp(const void *nodep, const VISIT which, const int depth);
void noderef_dmp(noderef_t *cp);
void noderef_del(noderef_t *cp, noderefelm_t *te);
void noderef_set(noderef_t *cp, int prevtile, int prevnode, int tile, int node);

/* tileref.cpp */
void tileref_flush(tileref_t *ni);
void tileref_put_node(tileref_t *ni, int tile, barenode_t node);
void tileref_put_road(tileref_t *ni, int tile, bareroad_t road);
tileref_t *tileref_ist(gen_t * gp,int);
void tileref_rls(tileref_t *ni);
tilerefelm_t *tileref_put(tileref_t *ni, int tile);
int tileref_put_vrt(tileref_t *ni, vrt_t vrt,int tile);
int tilerefelm_cmp(const void *a, const void *b);
tilerefelm_t *tileref_get(tileref_t *ni, int32_t id);
int tileref_write(int i, tileref_t *ni, vrt_t vrt,int tile);
tilerefelm_t *tileref_get(tileref_t *ni, int32_t tile);
void tile_null_peers(tile_t *tp);

/* scale.cpp */
scale_t scale_set(int wl, int wr, int ml, int mr);
scale_t scale_set_world(scale_t s,int wl, int wr);
int scale_get_model(scale_t s,int world);
int scale_get_world(scale_t s,int model);
scale_t scale_recalc_scale(scale_t s);

/* gtk.cpp */
void gtk_draw_node(draw_node_t c);
void gtk_draw_edge(draw_edge_t e);
void gtk_draw_set_context(draw_context_t drawcontext);

int nodecmp(const void *a, const void *b);
void gtk_draw_init_stack(double lox, double loy, double hix, double hiy);
void gtk_draw_init(int argc, char *argv[]);
void gtk_draw_run();
draw_node_t osrm_get_node(int n);

/* opengl.cpp */
void ogl_draw_run();
void ogl_draw_init_stack(double lox, double loy, double hix, double hiy);
void ogl_draw_init(int argc, char *argv[]);
draw_node_t ogl_osrm_get_node(int n);
void ogl_draw_set_context(draw_context_t drawcontext);

/* draw.cpp */
enum draw_types { DRAW_GTK, DRAW_OPENGL } ;

/* osrm.cpp */
nodeinfo *find_node(int id);
void dump_parents(node_t *nd);
