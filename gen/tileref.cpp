#include <gen.h>
#include <level.h>
#define ARRAY_VERSION 1


#ifdef USING_MONGO
extern mongo::DBClientConnection c;
#endif

tileref_t *tileref_ist(gen_t * gp,int max)
{
	tileref_t *tr = (tileref_t *)calloc(sizeof(tileref_t),1);
	if (!tr) return NULL;

	tr->tree = treemap_ist(bigrefelm_cmp);
	tr->nelm=0;
	tr->gen=gp;
	tr->max=max;

	return tr;
}

static void flushtiles(const void *data, void *udata)
{
	char fname[256];
	int t;
	tilerefelm_t *tre=(tilerefelm_t *)data;
	gen_t *gen = tre->tr->gen;

	//printf("Flushing tile %d %d and %d\n", tre->tile, tre->nroads, tre->nnodes);

	if (tre->nroads) {
		sprintf(fname, "%s/%s/road_%10.10d",gen->outdir,gen->ibase,tre->tile);
		FILE *fp= fopen(fname,"a");
		for (t=0;t<tre->nroads; t++) {
			bareroad_t br = {0};
			// sorry, valgrind wil complain if you assign directly
			br.dir=tre->rds[t].dir;
			br.tile=tre->rds[t].tile;
			br.tileto=tre->rds[t].tileto;
			br.type=tre->rds[t].type;
			br.from=tre->rds[t].from;
			br.nodeto=tre->rds[t].nodeto;
			fwrite(&br,sizeof(bareroad_t),1,fp);
			//if (br.tile == 1304342789)
			//printf("%d:%d to %d:%d\n", br.tile, br.from, br.tileto, br.nodeto);
		}
		fclose(fp);
		free(tre->rds);
	}

	if (tre->nnodes) {
		sprintf(fname, "%s/%s/node_%10.10d",gen->outdir,gen->ibase,tre->tile);
		FILE *fp= fopen(fname,"a");
		for (t=0;t<tre->nnodes; t++) {
			barenode_t bn=tre->nds[t];
			fwrite(&bn,sizeof(barenode_t),1,fp);
		}
		fclose(fp);
		free(tre->nds);
	}
}

void tileref_flush(tileref_t *trp)
{
	treemap_walk(trp->tree, flushtiles,NULL);
	treemap_rls(trp->tree,1);
	trp->count=0;
	trp->tree=treemap_ist(bigrefelm_cmp);
}

static void tilerefelm_add_road(tilerefelm_t *trp, bareroad_t road)
{
	int n = trp->nroads++;

#define RSIZE 10

	if ((n % RSIZE) == 0) {
		//printf("New chunck\n");
		if (trp->rds)  {
			trp->rds=(bareroad_t *)realloc(trp->rds, sizeof(bareroad_t)*(trp->nroads+RSIZE));
			//memset(&trp->rds[n],'\0', sizeof(bareroad_t) * RSIZE);
		} 
		else
			trp->rds=(bareroad_t *)calloc(sizeof(bareroad_t),trp->nroads+RSIZE);
	}

	trp->rds[n] = road;
}

static void tilerefelm_add_node(tilerefelm_t *trp, barenode_t node)
{
	int n = trp->nnodes++;

#define RSIZE 10

	if ((n % RSIZE) == 0) {
		//printf("New chunck\n");
		if (trp->nds) 
			trp->nds=(barenode_t *)realloc(trp->nds, sizeof(barenode_t)*(trp->nnodes+RSIZE));
		else
			trp->nds=(barenode_t *)calloc(sizeof(barenode_t),trp->nnodes+RSIZE);
	}

	trp->nds[n] = node;
}

void tileref_rls(tileref_t *tr)
{
	if (!tr) return;
	if (tr->tree) {
		treemap_rls(tr->tree,0);
		//free(tr->tree);
	}

	free(tr);
}

void tileref_put_node(tileref_t *tr, int tile, barenode_t node)
{
	tilerefelm_t *nep;
	int hitile = levelinfo.ancestorat(tile,PARTLEVEL);
	nep = tileref_get(tr,hitile);
	if (!nep) {
		tr->nelm++;
		nep = (tilerefelm_t *)calloc(sizeof(tilerefelm_t),1);
		nep->tile = hitile;
		nep->tr = tr;
		treemap_add(tr->tree, (void *)nep);
	}

	tilerefelm_add_node(nep,node);
	if (tr->count++ > tr->max) tileref_flush(tr);
}

void tileref_put_road(tileref_t *tr, int tile, bareroad_t road)
{
	//char fname[1024];
	tilerefelm_t *nep;
	int hitile = levelinfo.ancestorat(tile,PARTLEVEL);
	nep = tileref_get(tr,hitile);
	if (!nep) {
		tr->nelm++;
		nep = (tilerefelm_t *)calloc(sizeof(tilerefelm_t),1);
		nep->tile = hitile;
		nep->tr = tr;
		treemap_add(tr->tree, (void *)nep);
	}

	tilerefelm_add_road(nep,road);
	if (tr->count++ > tr->max) tileref_flush(tr);
}

int tilerefelm_cmp(const void *a, const void *b)
{
	tilerefelm_t *A=(tilerefelm_t *)a;
	tilerefelm_t *B=(tilerefelm_t *)b;

	if (A->tile > B->tile) return -1;
	if (A->tile < B->tile) return  1;
	return 0;
}

tilerefelm_t *tileref_get(tileref_t *tr, int32_t tile)
{
	tilerefelm_t ne, *nep;

	ne.tile=tile;

	nep = (tilerefelm_t *)treemap_get(tr->tree,(void *)&ne);
	return nep;
}

