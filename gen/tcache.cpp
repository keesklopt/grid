#include <gen.h>
#include <level.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

int net_intrand(int lo, int hi)
{
  double diff = (hi - lo)+1;

  return lo + (int)(diff * rand() / (RAND_MAX + 1.0));
}

int net_rand(double lo, double hi)
{
  double diff = (hi - lo)+1;

  return lo + (int)(diff * rand() / (RAND_MAX + 1.0))+0.5;
}

tcache_t *tcache_ist(gen_t *gp,int level, int max,tcache_type tp)
{
	tcache_t *tc = (tcache_t *)calloc(sizeof(tcache_t),1);
	if (!tc) return NULL;

	tc->max=max;
	// we need at least 5 tiles for the aggregation of 4 tiles into 1
	if (tc->max < 5) tc->max=5; 
	tc->stat.ntiles=0;
	tc->stamp=0;
	tc->type=tp;
	tc->gen=gp;
	tc->tcs = NULL;
	tc->idx = NULL;
	tc->marks=NULL;
	tc->level = level;
	if (tp == TC_RDWR || tp== TC_RDONLY) {
		tcache_init_level_read(tc,level);
	}

	return tc;
}

void tce_rls(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)nodep;
	tile_rls(tcep->tile);

	//printf("Releasing tile\n");
	//free(tcep);
}

void tcache_walk(tcache_t *tc, void (*fnc)(const void *,const VISIT,const int))
{
	if (!tc) return;
	twalk(tc->tcs,fnc);
}

/* walk according to index, not loaded tiles */
void tcache_walk_index(tcache_t *tc, void (*fnc)(const void *,const VISIT,const int))
{
	if (!tc) return;
	twalk(tc->idx,fnc);
}

static void put_links(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tcache_write_links(tcep->tc, tcep->tile);
}

static void put_all(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	selective_dump(tcep->tile);
	tcache_write_parts(tcep->tc, tcep->tile);
}

static void statistics(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tcache_stats(tcep->tc, tcep->tile);
}

static void check_reset(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tile_check_reset(tcep->tile);
}

static void reset_keep_1(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tile_reset_keep(tcep->tile, 1);
}

static void reset_keep_0(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tile_reset_keep(tcep->tile, 0);
}

static void reset(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tile_reset(tcep->tile);
}

static void reset2(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tile_reset2(tcep->tile);
}

tcache_t *newtc= NULL;

#ifdef NOT_USED
static void tc_dup_idx(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	offsets_t *op = *(offsets_t **)elm;
	//int level = levelinfo->level(op->tile);

// TODO : global!! (make own tree)
	tcache_add_offset(newtc,op);
}
#endif

/*
static void tc_dup(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tile_t *tn = tile_dup(tcep->tile, tcep->tileno);
	tcache_add_tile(newtc,tn);
}
*/

void freeit(void *d)
{
	free(d);
}

void tcache_rls(tcache_t *tc)
{
	if (!tc) return;
	printf("Release cache\n");

	twalk(tc->tcs,tce_rls);

	if (tc->tcs) tdestroy(tc->tcs,free);
	if (tc->marks) free(tc->marks);
	//if (tc->tcs) free(tc->tcs);
	if (tc->idx) tdestroy(tc->idx,freeit);
	free(tc);
}

void markoldest(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	tcache_t *tc=tp->tc;

	tcentry_t *tcep = tc->marks[0];

	// mark oldest timestamp that is in-core
	if (tp->incore && (!tcep || tp->stamp < tcep->stamp)) {
		//printf("Oldest is now %d\n", tp->stamp);
		tc->marks[0] = tp;
	} 
}

int rev_cmp(const void *a, const void *b)
{
	const node_coord_t *A=(const node_coord_t *)a;
	const node_coord_t *B=(const node_coord_t *)b;

	//printf("Testing %d and %d\n", A->tileno, B->tileno);
	if (A->y < B->y) return -1;
	if (A->y > B->y) return  1;
	if (A->x < B->x) return -1;
	if (A->x > B->x) return  1;
	return 0;
}

int op_cmp(const void *a, const void *b)
{
	const offsets_t *A=(const offsets_t *)a;
	const offsets_t *B=(const offsets_t *)b;

	//printf("Testing %d and %d\n", A->tileno, B->tileno);
	if (A->tile < B->tile) return -1;
	if (A->tile > B->tile) return  1;
	return 0;
}

int tc_cmp(const void *a, const void *b)
{
	const tcentry_t *A=(const tcentry_t *)a;
	const tcentry_t *B=(const tcentry_t *)b;

	//printf("Testing %d and %d\n", A->tileno, B->tileno);
	if (A->tileno < B->tileno) return -1;
	if (A->tileno > B->tileno) return  1;
	return 0;
}

int tcache_len(tcache_t *tc)
{
	if (!tc || !tc->tcs) return 0;
	return tc->stat.ntiles;
}

tcentry_t *tcache_find(tcache_t *tc, uint32_t tileno)
{
	tcentry_t tce={0};
	tcentry_t *tcep;
	tcentry_t *thelper;

	tce.tileno=tileno;
	thelper = &tce;

	tcep = (tcentry_t *)tfind(thelper,(void *const*)&tc->tcs,tc_cmp);

	if (tcep == NULL) return NULL;
	tcep = *(tcentry_t **)tcep;
	return tcep;
}

void tcache_mark(tcache_t *tc, tcentry_t *tcep)
{
	tc->nmarks++;
	if (tc->marks==NULL) 
		tc->marks = (tcentry_t **)calloc(1,sizeof(tcentry_t *));
	else
		tc->marks = (tcentry_t **)realloc(tc->marks,sizeof(tcentry_t *)*tc->nmarks);

	tc->marks[tc->nmarks-1] = tcep;
}

void tce_fill(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	tcache_t *tc=tp->tc;

	if (tp->done) return;
	if (tc->worklevel > 0) {
		if (tp->tile->level != tc->worklevel) 
			return;
	}
	int curitem = tc->arr.len++;
	tc->arr.items= (tcentry_t **)realloc(tc->arr.items, tc->arr.len * sizeof(tcentry_t *));
	tc->arr.items[curitem] = tp;
}

/* allocates !!, clean the data up by just 'free'ing */
tcarray_t tcache_get_array(tcache_t *tc)
{
	//tc->worklevel=level;
	// allocate one item just to be able to always realloc, 
	tc->arr.items = (tcentry_t **)calloc(1,sizeof(tcentry_t *));
	tc->arr.len=0;

	twalk(tc->tcs,tce_fill);
	return tc->arr;
}

offsets_t *tcache_get_offsets(tcache_t *tc, uint32_t tileno)
{
	offsets_t tce={0};
	offsets_t *tcep;
	offsets_t *thelper;

	tce.tile=tileno;
	thelper = &tce;
	tcep = (offsets_t *)tfind((void *)thelper,(void * const *)&tc->idx,op_cmp);
	if (tcep) tcep = *(offsets_t **)tcep;

	return tcep;
}

tile_t *tile_read_parts(tcache_t *tc,int tileno)
{
	int t;
	tile_t *tp = tile_ist(tileno);

	offsets_t *op = tcache_get_offsets(tc,tileno);
	if (!op) {
		printf("Could not find tile %d\n", tileno);
	}
	//printf("reading tile %d from %d/%d\n", tp->num, op->noffset, op->roffset);

	tp->nroads = op->nroads;

	uint64_t seek = (uint64_t)op->roffset * 18ULL;
	//if (op->roffset > 238609290) {
		//printf("Seeking %d * 18 is %lu\n", op->roffset, seek);
	//}

	fseeko(tc->fps[ORD_ROADS],seek,SEEK_SET);

	if (tp->nroads) {
		tp->rds = (road_t **)calloc(tp->nroads,sizeof(road_t *));
		for (t=0;t< tp->nroads; t++) {
			tp->rds[t] = read_road(tp,tc,tc->fps[ORD_ROADS]);
			tp->rds[t]->keep=1;
			tp->rds[t]->done=0;
		}
	}

	seek = (uint64_t)op->noffset * 1ULL;
	fseeko(tc->fps[ORD_NODES],seek,SEEK_SET);
	seek = (uint64_t)op->noffset * 4ULL;
	fseeko(tc->fps[ORD_UPLINKS],seek,SEEK_SET);
	seek = (uint64_t)op->noffset * 8ULL;
	fseeko(tc->fps[ORD_COORDS],seek,SEEK_SET);
	tp->nnodes = op->nnodes;
	if (TESTTILES(tp->num)) { 
		printf("Starting\n");
	}
	int count=0;
	if(tp->nnodes) {
		tp->nds = (node_t **)calloc(sizeof(node_t *),tp->nnodes);
		for (t=0;t< tp->nnodes; t++) {
			tp->nds[t] = read_node(tc,tc->fps[ORD_NODES]);
			tp->nds[t]->x = io.read_uint32(tc->fps[ORD_COORDS]);
			tp->nds[t]->y = io.read_uint32(tc->fps[ORD_COORDS]);
			// initialise both id and offset as sequence number
			tp->nds[t]->tile=tcache_get(tc,tileno);
			tp->nds[t]->id=
			tp->nds[t]->offset=t;
			//int32_t pn = io->read_uint32(tc->fps[ORD_UPLINKS]);
			//if (pn>=0) printf("Got %d\n", pn);
			//tp->nds[t].pnode=NULL;
			tp->nds[t]->nroads=count;
			//count += tp->nds[t]->lastroad;
			//tp->nds[t]->lastroad=(tp->nds[t]->firstroad+tp->nds[t]->lastroad)-1;
			tp->nds[t]->keep = 1;
			tp->nds[t]->rise = 0;
		}
	}
	if (TESTTILES(tp->num)) tile_dmp(tp,1);
	if (count != tp->nroads) {
		// can occur when reading stage 3 so quit this
		//printf("No : %d %d\n", count, tp->nroads);
	}
	return tp;
}

tile_t *tcache_get(tcache_t *tc, uint32_t tileno)
{
	tcentry_t tce={0};
	tcentry_t *tcep;
	tcentry_t *thelper;
	tile_t *tp;

	tce.tileno=tileno;
	thelper = &tce;
	tcep = (tcentry_t *)tfind((void *)thelper,(void * const *)&tc->tcs,tc_cmp);

	if (tcep) { // was in cache
		tcep = *(tcentry_t **)tcep;
		if (tcep->incore) 
			return tcep->tile;
		// else read from disk
		tp = tile_read_parts(tc,tileno);
		tc->incore=1;
		return tp;
	} else {
		//printf("tile %d not found\n", tileno);
		//tcache_dmp(tc);
		tcep = (tcentry_t *)calloc(sizeof(tcentry_t),1);
		tcep->incore=0;
		tcep->done=0;
		//printf("Created %p\n", tcep);
		tcep->tileno = tileno;
		tcep->incore = 1;
		if (tc->type != TC_WRONLY) {
			tp = tile_read_parts(tc,tileno);
		} else 
			tp = tile_ist(tileno);
		tcep->tile= tp;
		tcache_add(tc, tcep);
	}

	tcep->tile = tp;
	return tp;
}

void tce_dmp(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	printf("Tile : %d (%d/%d) stamp %d (%s)\n", tp->tileno, tp->tile->nnodes, tp->tile->nroads, tp->stamp, 
			tp->incore ? "in mem": "on disk");
	wlk_tile_dmp(0, tp->tile,NULL);
}

void tce_prunelevel(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	tcache_t *tc=tp->tc;

	if (tc->worklevel == tp->tile->level) {
		tcache_mark(tp->tc,tp); // for deletion
	}
}

void tce_prune(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	if (tp->tile->nnodes == 0 || tp->tile->nroads==0) {
		tcache_mark(tp->tc,tp); // for deletion
	}
}

void tce_all(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	tcache_mark(tp->tc,tp); // for deletion
}

void tce_check(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	int done = tile_check(tp->tile);
	if (done) {
		tcache_mark(tp->tc,tp); // for deletion
	}
}

void tcache_clear_marks(tcache_t *tc)
{
	if (tc->marks) free(tc->marks);
	tc->marks=NULL;
	tc->nmarks=0;
}

/* unused since we have a cache per level
void tcache_del_level(tcache_t *tc)
{
	int t;
	tcache_clear_marks(tc); // just in case
	tc->worklevel=level;
	twalk(tc->tcs,tce_prunelevel);
	for (t =0; t< tc->nmarks; t++) {
		tcache_del(tc,tc->marks[t]);
	}
	tcache_clear_marks(tc); // and again
}
*/

void tcache_prune(tcache_t *tc)
{
	int t;
	tcache_clear_marks(tc); // just in case
	twalk(tc->tcs,tce_prune);
	for (t =0; t< tc->nmarks; t++) {
		tcache_del(tc,tc->marks[t]);
	}
	tcache_clear_marks(tc); // and again
}

void tcache_exit_level(tcache_t *tc)
{
	int t;
	for (t=0; t< NFILES; t++) {
		if (tc->fps[t]) fclose(tc->fps[t]);
		tc->fps[t]=NULL;
	}
	if (tc->coords) free(tc->coords);
	tc->coords=NULL;
	tc->noffset=0;
	tc->roffset=0;
}

void tcache_init_level_read(tcache_t *tc,int level)
{
	char fname[1024];
	uint32_t nitems=0;
	int no=0;
	int ro=0;

	tcache_exit_level(tc);
	sprintf(fname, "%s/%s/grid_%d_index",tc->gen->outdir,tc->gen->ibase,level);
	if ((tc->fps[ORD_INDEX] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_nodes",tc->gen->outdir,tc->gen->ibase,level);
	if ((tc->fps[ORD_NODES] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_roads",tc->gen->outdir,tc->gen->ibase,level);
	if ((tc->fps[ORD_ROADS] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_coord",tc->gen->outdir,tc->gen->ibase,level);
	if ((tc->fps[ORD_COORDS] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_drooc",tc->gen->outdir,tc->gen->ibase,level);
	if ((tc->fps[ORD_REVCOORDS] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_parms",tc->gen->outdir,tc->gen->ibase,level);
	if ((tc->fps[ORD_PARMS] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_up",tc->gen->outdir,tc->gen->ibase,level);
	if ((tc->fps[ORD_UPLINKS] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_down",tc->gen->outdir,tc->gen->ibase,level);
	if ((tc->fps[ORD_DOWNLINKS] = fopen(fname,"r"))==NULL) goto error;

	tc->stat.nindices=0;
	while (1) {
		offsets_t *op = (offsets_t *)calloc(sizeof(offsets_t),1);
		//printf("alloc %p\n", op);
		// fread needed to detect EOF
		nitems = fread(&op->tile,sizeof(uint32_t),1,tc->fps[ORD_INDEX]);
		//printf("%d %d %d\n", op->tile, op->nnodes, op->nroads);
		op->tile = ntohl(op->tile);
		if (nitems != 1) { 
			free(op);
			break;
		}
		op->nnodes = io.read_uint32(tc->fps[ORD_INDEX]);
		op->nroads = io.read_uint32(tc->fps[ORD_INDEX]);
		op->noffset=no;
		op->roffset=ro;
		no += op->nnodes;
		ro += op->nroads;
		tcache_add_offset(tc,op);
	}

	return;
error:
	printf("Could not open %s\n", fname);
	exit(-1);
}

void tcache_init_level_write(tcache_t *tc,int level)
{
	char fname[1024];
	
	tcache_exit_level(tc);
	sprintf(fname, "%s/%s/grid_%d_index",tc->gen->outdir,tc->gen->ibase,level);
	tc->fps[ORD_INDEX] = fopen(fname,"w");
	sprintf(fname, "%s/%s/grid_%d_nodes",tc->gen->outdir,tc->gen->ibase,level);
	tc->fps[ORD_NODES] = fopen(fname,"w");
	sprintf(fname, "%s/%s/grid_%d_roads",tc->gen->outdir,tc->gen->ibase,level);
	tc->fps[ORD_ROADS] = fopen(fname,"w");
	sprintf(fname, "%s/%s/grid_%d_coord",tc->gen->outdir,tc->gen->ibase,level);
	tc->fps[ORD_COORDS] = fopen(fname,"w");
	sprintf(fname, "%s/%s/grid_%d_drooc",tc->gen->outdir,tc->gen->ibase,level);
	tc->fps[ORD_REVCOORDS] = fopen(fname,"w");
	sprintf(fname, "%s/%s/grid_%d_parms",tc->gen->outdir,tc->gen->ibase,level);
	tc->fps[ORD_PARMS] = fopen(fname,"w");
	sprintf(fname, "%s/%s/grid_%d_up",tc->gen->outdir,tc->gen->ibase,level);
	tc->fps[ORD_UPLINKS] = fopen(fname,"w");
	sprintf(fname, "%s/%s/grid_%d_down",tc->gen->outdir,tc->gen->ibase,level);
	tc->fps[ORD_DOWNLINKS] = fopen(fname,"w");

	tc->coords = (node_coord_t *)calloc(tc->stat.nnodes,sizeof(node_coord_t));
}

void tcache_write_pnode(tcache_t *tc, node_t nd)
{
	int o= tc->gen->output;
	int off=-1;
	if (nd.pnode) off = nd.pnode->offset;

	//printf("link %d:%d is %p\n", nd.tile, nd.offset, nd.pnode);

	int diff = nd.nroads;

	if (diff < 0) {
		//printf("pnode fail !! %d \n", nd.lastroad,nd.firstroad);
	}

	io.write_int32(o,tc->fps[ORD_UPLINKS], off);
	io.write_comment(o, tc->fps[ORD_UPLINKS], "\n");
}

void tcache_write_node(tcache_t *tc, node_t nd)
{
	int o= tc->gen->output;
	int off=-1;
	if (nd.pnode) {
		//printf ("Pnode of %d:%d is %d:%d and join is %d\n", 
			//nd.tile->num, nd.offset, 
			//nd.pnode->tile->num, nd.pnode->offset, nd.join);
		off = nd.pnode->offset;
	}

	if (nd.nroads==0) { 
		//printf("Skipping on roads \n");
		return;
	}

	//if (nd.pnode) 
	//printf("link %d:%d is %d:%d\n", nd.tile, nd.offset, nd.pnode->tile,nd.pnode->offset);

	//printf("writing %d\n", off);

	//int diff = nd.nroads;

	io.write_uint8(o,tc->fps[ORD_NODES], (nd.nroads));
	io.write_comment(o, tc->fps[ORD_NODES], "\n");
	io.write_int32(o,tc->fps[ORD_NODES], nd.rank);
	io.write_comment(o, tc->fps[ORD_NODES], "\n");
	io.write_int32(o,tc->fps[ORD_UPLINKS], off);
	io.write_comment(o, tc->fps[ORD_UPLINKS], "\n");
	// 1 * N
	io.write_int32(o,tc->fps[ORD_COORDS], nd.x);
	io.write_comment(o, tc->fps[ORD_COORDS], ",");
	io.write_int32(o,tc->fps[ORD_COORDS], nd.y);
	io.write_comment(o, tc->fps[ORD_COORDS], "\n");
}

void tcache_write_coords(tcache_t *tc)
{
	int o= tc->gen->output;
	int i;

	qsort(tc->coords,tc->stat.nnodes,sizeof(node_coord_t),rev_cmp);

	for (i=0; i< tc->stat.nnodes; i++) {
		node_coord_t nd = tc->coords[i];
		io.write_int32(o,tc->fps[ORD_REVCOORDS], nd.tile);
		io.write_comment(o, tc->fps[ORD_REVCOORDS], ",");
		io.write_int32(o,tc->fps[ORD_REVCOORDS], nd.node);
		io.write_comment(o, tc->fps[ORD_REVCOORDS], ",");
		io.write_int32(o,tc->fps[ORD_REVCOORDS], nd.x);
		io.write_comment(o, tc->fps[ORD_REVCOORDS], ",");
		io.write_int32(o,tc->fps[ORD_REVCOORDS], nd.y);
		io.write_comment(o, tc->fps[ORD_REVCOORDS], "\n");
	}
}


void tcache_write_road(tcache_t *tc, road_t rd)
{
	int o= tc->gen->output;

	io.write_uint32(o, tc->fps[ORD_ROADS], rd.nodefrom->offset);
	io.write_comment(o, tc->fps[ORD_ROADS], ",");
	io.write_uint32(o, tc->fps[ORD_ROADS], rd.nodeto->tile->num);
	io.write_comment(o, tc->fps[ORD_ROADS], ":");
	io.write_uint32(o, tc->fps[ORD_ROADS], rd.nodeto->offset);
	io.write_comment(o, tc->fps[ORD_ROADS], ",");
	io.write_uint32(o, tc->fps[ORD_ROADS], rd.len);
	io.write_comment(o, tc->fps[ORD_ROADS], ",");
	io.write_uint8(o, tc->fps[ORD_ROADS], rd.dir);
	io.write_comment(o, tc->fps[ORD_ROADS], ",");
	int extra=0;
	if (rd.rise) { 
		extra = 20;
	}
	io.write_uint8(o, tc->fps[ORD_ROADS], rd.type);
	io.write_comment(o, tc->fps[ORD_ROADS], "\n");

	tc->stat.nroads ++;
}

void tcache_write_index(tcache_t *tc, tile_t *tp)
{
	int o= tc->gen->output;
	io.write_uint32(o, tc->fps[ORD_INDEX], tp->num);
	io.write_comment(o, tc->fps[ORD_INDEX], ",");
	io.write_uint32(o, tc->fps[ORD_INDEX], tp->nnodes);
	io.write_comment(o, tc->fps[ORD_INDEX], ",");
	io.write_uint32(o, tc->fps[ORD_INDEX], tp->nroads);
	io.write_comment(o, tc->fps[ORD_INDEX], "\n");
}

void tcache_write_parms(tcache_t *tc)
{
	int o= tc->gen->output;

	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.ntiles);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.nnodes);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.nroads);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.miny);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.maxy);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.minx);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.maxx);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.minlen);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.maxlen);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.maxnode);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.maxroad);
	io.write_comment(o, tc->fps[ORD_PARMS], ",");
	io.write_int32(o, tc->fps[ORD_PARMS], tc->stat.maxforward);
	io.write_comment(o, tc->fps[ORD_PARMS], "\n");
}

int tcache_stats(tcache_t *tc, tile_t *tp)
{
	int n=0;
	int r=0;

	tc->stat.ntiles++;
	for (n=0; n< tp->nnodes; n++) {
		node_t nd = *tp->nds[n];
		tc->stat.nnodes++;
		if (nd.x < tc->stat.minx) tc->stat.minx=nd.x;
		if (nd.x > tc->stat.maxx) tc->stat.maxx=nd.x;
		if (nd.y < tc->stat.miny) tc->stat.miny=nd.y;
		if (nd.y > tc->stat.maxy) tc->stat.maxy=nd.y;
		if (nd.nroads > tc->stat.maxforward) tc->stat.maxforward=nd.nroads;
		if (n > tc->stat.maxnode) tc->stat.maxnode=n;
	}
	for (r=0; r< tp->nroads; r++) {
		road_t rd = *tp->rds[r];
		tc->stat.nroads++;
		if (rd.len < tc->stat.minlen) tc->stat.minlen=rd.len;
		if (rd.len > tc->stat.maxlen) tc->stat.maxlen=rd.len;
		if (r > tc->stat.maxroad) tc->stat.maxroad=r;
	}

	return 0;
}

int tcache_write_links(tcache_t *tc, tile_t *tp)
{
	int n=0;

	for (n=0; n< tp->nnodes; n++) {
		node_t nd = *tp->nds[n];
		tcache_write_pnode(tc,nd);
	}
	
	return 0;
}

int tcache_write_parts(tcache_t *tc, tile_t *tp)
{
	int n=0;
	int r=0;
	//int p= TESTTILES(tp->num);

	//printf("T %d (%d/%d)\n", tp->num, tp->nnodes, tp->nroads);
	//tile_dmp(tp,1);

	//if (p) tile_draw(tp);
	if (tp->nnodes ==0 || tp->nroads==0) return 0;

	for (n=0; n< tp->nnodes; n++) {
		node_t nd = *tp->nds[n];
		tcache_write_node(tc,nd);
		tc->coords[tc->stat.nnodes].tile = nd.tile->num;
		tc->coords[tc->stat.nnodes].node = nd.offset;
		tc->coords[tc->stat.nnodes].x = nd.x;
		tc->coords[tc->stat.nnodes].y = nd.y;
		tc->stat.nnodes++;
	}
	for (r=0; r< tp->nroads; r++) {
		road_t rd = *tp->rds[r];
		tcache_write_road(tc,rd);
	}

	tcache_write_index(tc,tp);
	
	return 0;
}

void dump_stats(stat_t *stat)
{
	printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n", 
	stat->ntiles,
	stat->nnodes,
	stat->nroads,
	stat->miny,
	stat->maxy,
	stat->minx,
	stat->maxx,
	stat->minlen,
	stat->maxlen,
	stat->maxroad,
	stat->maxnode,
	stat->maxforward);
}

void reset_stats(stat_t *stat)
{
	stat->ntiles=0;
	stat->nnodes=0;
	stat->nroads=0;
	stat->miny= 900000000;
	stat->maxy= -900000000;
	stat->minx= 1800000000;
	stat->maxx= -1800000000;
	stat->minlen=1000000000;
	stat->maxlen=0;
	stat->maxroad=0;
	stat->maxnode=0;
	stat->maxforward=0;
}

void add_stats(stat_t *dst, stat_t *src)
{
	if (src->ntiles == 0) return;
	dst->ntiles += src->ntiles;
	dst->nnodes += src->nnodes;
	dst->nroads += src->nroads;

	if (src->miny < dst->miny) dst->miny = src->miny;
	if (src->maxy > dst->maxy) dst->maxy = src->maxy;
	if (src->minx < dst->minx) dst->minx = src->minx;
	if (src->maxx > dst->maxx) dst->maxx = src->maxx;
	if (src->minlen < dst->minlen) dst->minlen = src->minlen;
	if (src->maxlen > dst->maxlen) dst->maxlen = src->maxlen;

	if (src->maxroad > dst->maxroad) dst->maxroad = src->maxroad;
	if (src->maxnode > dst->maxnode) dst->maxnode = src->maxnode;
	if (src->maxforward > dst->maxforward) dst->maxforward = src->maxforward;
}

void tcache_reset2(tcache_t *tc)
{
	tcache_walk(tc,reset2);
}

void tcache_reset_keep(tcache_t *tc, int k)
{
	if (k) 
		tcache_walk(tc,reset_keep_1);
	else
		tcache_walk(tc,reset_keep_0);
}

void tcache_reset(tcache_t *tc)
{
	tcache_walk(tc,reset);
}

void tcache_check_reset(tcache_t *tc)
{
	tcache_walk(tc,check_reset);
}

void tcache_statistics(tcache_t *tc)
{
	reset_stats(&tc->stat);
	tcache_walk(tc,statistics);
	dump_stats(&tc->stat);
}

/* elevate complete tcache level 1 stage */
/* in short just copy all roads to their parent */
tcache_t *tcache_elevate(tcache_t *tc)
{
	tcache_t *n = tcache_ist(tc->gen, tc->level, tc->max, tc->type);
	tcarray_t orig = tcache_get_array(tc);
	tile_t *newtile;
	tile_t *oldtile;

	// 1. first add all nodes, creating the tiles along the way
	// that way when 2. adding roads, both totile and node will exist !
	for (int t=0; t< orig.len; t++) {
		//tnew [t] = tile_dup(orig.items[t]->tile,orig.items[t]->tileno);
		int parent = levelinfo.parent(orig.items[t]->tile->num);
		newtile = tcache_get(n,parent);
		oldtile = orig.items[t]->tile;
		for (int n=0; n< oldtile->nnodes; n++) {
			//printf("node %d:%d into parent %d\n", oldtile->num, n, parent);
			node_t *nd = node_dup(oldtile->nds[n]);
			// BUT reset road count to 0 to reinit them !!
			nd->nroads=0;
			tile_add_node(newtile,nd);
			oldtile->nds[n]->pnode = nd; // (ab)used in restoring nodes
			nd->pnode = oldtile->nds[n];
		}	

	}

	// 2. add roads and fix the links 
	for (int t=0; t< orig.len; t++) {
		int parent = levelinfo.parent(orig.items[t]->tile->num);
		tile_t *newtile = tcache_get(n,parent);
		tile_t *oldtile = orig.items[t]->tile;
		
		for (int r=0; r< oldtile->nroads; r++) {
			//printf("node %d:%d into parent %d\n", oldtile->num, n, parent);
			road_t *oldrd = oldtile->rds[r];
			road_t *newrd = (road_t *)calloc(sizeof(road_t),1);
			*newrd = *oldrd;
			int toparent = levelinfo.parent(oldrd->nodeto->tile->num);

			newrd->nodefrom = oldrd->nodefrom->pnode;
			newrd->nodeto = oldrd->nodeto->pnode;
			newrd->nodeto->tile = tcache_get(n,toparent);
		
			//int prevn = oldrd->nodefrom->pnode->nroads++;
			//if (oldrd->nodefrom->pnode->rds) 
				//oldrd->nodefrom->pnode->rds = (road_t **)realloc(oldrd->nodefrom->pnode->rds, sizeof(road_t *)* (oldrd->nodefrom->pnode->nroads));
			//else
				//oldrd->nodefrom->pnode->rds = (road_t **)calloc(sizeof(road_t *), (oldrd->nodefrom->pnode->nroads));
			//oldrd->nodefrom->pnode ->rds[prevn] = newrd;
			tile_add_road(newtile,newrd);
		}	
	}

	n->level--;
	return n;
}

tcache_t *tcache_dup(tcache_t *tc)
{
	tcache_t *n = tcache_ist(tc->gen, tc->level, tc->max, tc->type);

	// ole way, won't work anymore 
	//tc->hook = (void *)n;
	//tcache_walk(tc,tc_dup);

	tcarray_t orig = tcache_get_array(tc);
	tile_t **tnew = (tile_t **)calloc(sizeof(tile_t *),orig.len);

	for (int t=0; t< orig.len; t++) {
		tnew [t] = tile_dup(orig.items[t]->tile,orig.items[t]->tileno);
		tcache_add_tile(n,tnew[t]);
	}
	// tnew now contains tiles with new pointers but old references
	// relink them
	for (int t=0; t< orig.len; t++) {
		tile_relink(tnew[t], orig.items[t]->tile);
	}
	return n;
}

void tcache_put_links(tcache_t *tc,int level)
{
	tcache_prune(tc);

	//printf("Level %d has %d tiles\n", level, tc->stat.ntiles);
	//printf("Writing link\n");
	if (tc->stat.ntiles == 0) return;
	tcache_walk(tc,put_links);
}

void tcache_put_all(tcache_t *tc,int level)
{
	printf("Level %d has %d tiles\n", level, tc->stat.ntiles);
	tcache_prune(tc);

	printf("Level %d has %d tiles\n", level, tc->stat.ntiles);
	if (tc->stat.ntiles == 0) return;
	tcache_statistics(tc);
	tcache_init_level_write(tc,level);
	tc->stat.nnodes=0;
	tcache_walk(tc,put_all);
	tcache_write_parms(tc);
	tcache_write_coords(tc);
	tcache_exit_level(tc);
}

void tcache_empty(tcache_t *tc)
{
	int t;
	tcache_clear_marks(tc); // just in case
	twalk(tc->tcs,tce_all);
	for (t =0; t< tc->nmarks; t++) {
		tcache_del(tc,tc->marks[t]);
	}
	tcache_clear_marks(tc); // and again

	//printf("%d tiles left in cache\n", tc->ntiles);
}

void tcache_dmp(tcache_t *tc)
{
	twalk(tc->tcs,tce_dmp);
}

void tcache_del(tcache_t *tc, tcentry_t *te)
{
	tcentry_t *tep;
	//printf("Bye %d\n", te->tile->num);
	tile_rls(te->tile);
	tep = (tcentry_t *)tdelete((void *)te,&tc->tcs,tc_cmp);
	free(te);
	tc->stat.ntiles--;

	//tcache_dmp(tc);
}

void tcache_put(tcache_t *tc, uint32_t tileno, tile_t *tp)
{
	char fname[1024];
	//tcentry_t tce={0};
	tcentry_t *tcep;

	tcep = tcache_find(tc,tileno);

	if (tcep) {
		//printf("Found type %s\n", rp->name);
		//tcep->stat=CACHE_DIRTY;
	} else {
		printf("Did not find tile %d\n", tileno);
		tcep = (tcentry_t *)calloc(sizeof(tcentry_t),1);
		tcep->tile = tp;
		tcep->stamp=tc->stamp++;
		//tcep->stat=CACHE_DIRTY;
		tcache_add(tc, tcep);
	}

	if (tp->nroads == 0 && tp->nnodes==0) {
		//printf("Skipping empty tile %d\n", tp->num);
		return;
	}
	sprintf(fname, "%s/%s/tile_%10.10d",tc->gen->outdir,tc->gen->ibase,tileno);
	FILE *fp = fopen(fname,"w");
	write_tile(tc->gen->output, fp, tp);
	fclose(fp);
}

void tcache_todisk(tcache_t *tc, tcentry_t *tcep)
{
	tcache_put(tc,tcep->tile->num, tcep->tile);
	tile_rls(tcep->tile);
	tcep->tile=NULL;
	tc->incore--;
	tcep->incore = 0;
}

void tcache_add_offset(tcache_t *tc, offsets_t *te)
{
	// Sorted !
	offsets_t *op;
	op = *(offsets_t **)tsearch((void *)te,&tc->idx,op_cmp);

	tc->stat.nindices++;
}

void tcache_add(tcache_t *tc, tcentry_t *te)
{
	// Sorted !
	tcentry_t *tcep;
	tcep = *(tcentry_t **)tsearch((void *)te,&tc->tcs,tc_cmp);
	tcep->tc=tc;
	tcep->stamp=tc->stamp++;

	if (tc->incore > tc->max) {
		tcache_clear_marks(tc);
		tcache_mark(tc,NULL); // mark one item for keeping the oldest
		twalk(tc->tcs,markoldest);
		tcep = tc->marks[0];
		//printf("Deleting one tile %d\n",tcep->tile->num);
		if (tcep) tcache_todisk(tc,tcep);
	}
	//printf("Adding one tile\n");
	tc->stat.ntiles++;
	tc->incore++;
	tcep->incore=1;

	//printf("Added --------- %d\n", te->tileno);
	//tcache_dmp(tc);
	//printf("Done --------- %d\n", te->tileno);
}

void tcache_add_tile(tcache_t *tc, tile_t *tp)
{
	tcentry_t *tcep = (tcentry_t *)calloc(1,sizeof(tcentry_t));

	tcep->tile= tp;
	tcep->tileno= tp->num;
	tcache_add(tc, tcep);
}
