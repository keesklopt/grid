#include <stdio.h>
#include <rds.h>

#define x0  155000.0
#define y0  463000.0
#define k  .9999079
#define bigr  6382644.571
#define m  .003773953832
#define n  1.00047585668
#define lambda0  M_PI * .029931327161111111
#define phi0  M_PI * .28975644753333335
#define l0  M_PI * .029931327161111111
#define b0  M_PI * .28956165138333334
#define e .08169683122
#define a 6377397.155

/*
RDSPoint *makeRDSPoint(GPSPoint gpspoint)
{
  double lat = gpspoint.y;
  double lng = gpspoint.x;

  // GPS to bessel
  double dlat   = lat - 52.0;
  double dlng   = lng - 5.0;
  double latcor = (-96.862 - dlat * 11.714 - dlng * .125) * 1e-5;
  double lngcor = (dlat * .329 - 37.902 - dlng * 14.667) * 1e-5;
  double latbes = lat - latcor;
  double lngbes = lng - lngcor;

  // deg to rad
  double phi = latbes / 180.0 * M_PI;
  double lambda = lngbes / 180.0 * M_PI;

  // bessel to RDS
  double qprime = log(tan(phi / 2.0 + (M_PI / 4)));
  double dq = e / 2.0 * log((e * sin(phi) + 1.0) / (1.0 - e * sin(phi)));
  double q = qprime - dq;
  double w = n * q + m;
  double b = atan(exp(w)) * 2.0 - (M_PI / 2);
  double dl = n * (lambda - lambda0);
  // Computing 2nd power
  double d__1 = sin((b - b0) / 2.0);
  // Computing 2nd power
  double d__2 = sin(dl / 2.0);
  double s2psihalf = d__1 * d__1 + d__2 * d__2 * cos(b) * cos(b0);
  double cpsihalf = sqrt(1.0 - s2psihalf);
  double spsihalf = sqrt(s2psihalf);
  double tpsihalf = spsihalf / cpsihalf;
  double spsi = spsihalf * 2.0 * cpsihalf;
  double cpsi = 1.0 - s2psihalf * 2.0;
  double sa = sin(dl) * cos(b) / spsi;
  double ca = (sin(b) - sin(b0) * cpsi) / (cos(b0) * spsi);
  double r = k * 2.0 * bigr * tpsihalf;
  x = r * sa + x0;
  y = r * ca + y0;
}
*/

GPSPoint makeGPSPoint(RDSPoint rdspoint)
{
	GPSPoint p;
  int i;
  double x = rdspoint.x;
  double y = rdspoint.y;

  // RDS to bessel
  double d__1 = x - x0;
  double d__2 = y - y0;
  double r = sqrt(d__1 * d__1 + d__2 * d__2);
  double sa, ca;
  if (r) {
    sa = (x - x0) / r;
    ca = (y - y0) / r;
  } else {
    sa = 0.0;
    ca = 0.0;
  }
  double psi = atan2(r, k * 2. * bigr) * 2.;
  double cpsi = cos(psi);
  double spsi = sin(psi);
  double sb = ca * cos(b0) * spsi + sin(b0) * cpsi;
  double cb = sqrt(1. - sb * sb);
  double b = acos(cb);
  double sdl = sa * spsi / cb;
  double dl = asin(sdl);
  double lambda = dl / n + lambda0;
  double w = log(tan(b / 2. + (M_PI / 4)));
  double q = (w - m) / n;
  double phiprime = atan(exp(q)) * 2. - (M_PI / 2);
  double phi;
  for (i = 0; i < 4; ++i) {
    double dq = e / 2. * log((e * sin(phiprime) + 1.) / (1. - e * sin(phiprime)));
    phi = atan(exp(q + dq)) * 2. - (M_PI/ 2);
    phiprime = phi;
  }

  // rad to deg
  double latbes = phi / M_PI * 180;
  double lngbes = lambda / M_PI * 180;

  // bessel to GPS
  double dlat   = latbes - 52.0;
  double dlng   = lngbes - 5.0;
  double latcor = (-96.862 - dlat * 11.714 - dlng * .125) * 1e-5;
  double lngcor = (dlat * .329 - 37.902 - dlng * 14.667) * 1e-5;
  p.lat = latbes + latcor;
  p.lng = lngbes + lngcor;

	return p;
}

/*
int main()
{
	GPSPoint ll;
	RDSPoint rp = { 68682.95, 443911.84 };

	ll = makeGPSPoint(rp);

	printf("%f,%f\n", ll.lat, ll.lng);
	
	return 0;
}
*/
