#ifndef GRID_H_INCLUDED
#define GRID_H_INCLUDED

#include <search.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <expat.h>
#include <iostream>
#include <cmath>
#include <unistd.h>
#include <cstring>
#include <arpa/inet.h>

using namespace std;

#define DIR_BOTH 0
#define DIR_FROMTO 1
#define DIR_TOFROM 2
#define DIR_NONE 3 // use to remove roads 

// clear, never been in the heap before
#define HEAP_CLEAR -1

#define LOG_BLAH    0   /* 0+1+2+3 */
/*! @brief debug log level */
#define LOG_DEBUG   1   /* 1+2+3 */
/*! @brief warning log level */
#define LOG_WARN    2   /* 2+3 */
/*! @brief error log level */
#define LOG_ERR     3   /* 3 */
/*! @brief always be quiet log level */
#define LOG_SILENT  4   /*  */

/*! @brief for use in lprintf, force to be verbose */
#define LOG_ALOT    3   /* be verbose */

/*! @brief max number of log levels */
#define LOG_MAX     5

extern int loglevel;

int lprintf(int level, char *fmt, ...);

enum outputs { OUT_BINARY, OUT_HEX, OUT_DEC };
enum methods { METHOD_LENGTH, METHOD_SPEED };
enum ord { ORD_PARMS, ORD_INDEX, ORD_NODES, ORD_ROADS, ORD_COORDS, ORD_REVCOORDS, ORD_UPLINKS, ORD_DOWNLINKS };

class Stats
{
	public:
	int ntiles;
	int nindices;
	int nnodes;
	int nroads;
	int minx;
	int miny;
	int maxx;
	int maxy;
	int minlen;
	int maxlen;
	int maxroad;
	int maxnode;
	int maxforward;

	Stats()
	{
		this->reset();
	}
	
	void dump(bool verbose)
	{
		if (verbose==false) {
			printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n", 
			this->ntiles, this->nnodes, this->nroads,
			this->miny, this->maxy, this->minx, this->maxx,
			this->minlen, this->maxlen,
			this->maxroad, this->maxnode, this->maxforward);
		} else {
			printf("ntiles : %d\nnnodes :%d\nnroads : %d\nminy : %d\nmaxy : %d\n"
			"minx : %d\nmaxx : %d\nminlen : %d\nmaxlen : %d\n"
			"maxroad : %d\nmaxnode : %d\nmaxforward : %d\n",
			this->ntiles, this->nnodes, this->nroads,
			this->miny, this->maxy, this->minx, this->maxx,
			this->minlen, this->maxlen,
			this->maxroad, this->maxnode, this->maxforward); 
		}
	}
	
	void reset()
	{
		this->ntiles=0;
		this->nnodes=0;
		this->nroads=0;
		this->miny= 900000000;
		this->maxy= -900000000;
		this->minx= 1800000000;
		this->maxx= -1800000000;
		this->minlen=1000000000;
		this->maxlen=0;
		this->maxroad=0;
		this->maxnode=0;
		this->maxforward=0;
	}
	
	void add(Stats *src)
	{
		if (src->ntiles == 0) return;
		this->ntiles += src->ntiles;
		this->nnodes += src->nnodes;
		this->nroads += src->nroads;
	
		if (src->miny < this->miny) this->miny = src->miny;
		if (src->maxy > this->maxy) this->maxy = src->maxy;
		if (src->minx < this->minx) this->minx = src->minx;
		if (src->maxx > this->maxx) this->maxx = src->maxx;
		if (src->minlen < this->minlen) this->minlen = src->minlen;
		if (src->maxlen > this->maxlen) this->maxlen = src->maxlen;
	
		if (src->maxroad > this->maxroad) this->maxroad = src->maxroad;
		if (src->maxnode > this->maxnode) this->maxnode = src->maxnode;
		if (src->maxforward > this->maxforward) this->maxforward = src->maxforward;
	}
};


class IO
{
	public:
	int32_t read_int32(FILE *fp)
	{
		int32_t swapped;
		fread(&swapped,sizeof(int32_t),1,fp);
	
		return ntohl(swapped);
	}
	
	int32_t read_int32(int fd)
	{
		int32_t swapped;
		read(fd,&swapped,sizeof(int32_t));
	
		return ntohl(swapped);
	}
	
	uint32_t read_uint32(FILE *fp) 
	{
    	uint32_t ret;
   	 
    	fread(&ret,sizeof(uint32_t),1,fp);
    	int32_t swapped= ntohl(ret);
    	return swapped;
	}

	uint32_t read_uint32(int fd) 
	{
    	uint32_t ret;
   	 
    	read(fd,&ret,sizeof(uint32_t));
    	int32_t swapped= ntohl(ret);
    	return swapped;
	}

	uint16_t read_uint16(FILE *fp)
	{
		uint16_t ret;

		fread(&ret,sizeof(uint16_t),1,fp);
		int16_t swapped= ntohs(ret);
		return swapped;
	}
	int16_t read_int16(FILE *fp)
	{
		int16_t swapped;
		fread(&swapped,sizeof(int16_t),1,fp);
		return ntohs(swapped);
	}

	uint8_t read_uint8(FILE *fp)
	{
		uint8_t ret;

		fread(&ret,sizeof(uint8_t),1,fp);
		return ret;
	}

	uint8_t read_uint8(int fd)
	{
		uint8_t ret;

		read(fd,&ret,sizeof(uint8_t));
		return ret;
	}

	int8_t read_int8(FILE *fp)
	{
		int8_t swapped;
		fread(&swapped,sizeof(int8_t),1,fp);
		return swapped;
	}


	char *read_string(FILE *fp)
	{	
		int t=0;
		char *str = (char *)calloc(sizeof(char),1);
		int c='x';

		int l = fread(&c,sizeof(char),1,fp);
		while (c!='\0' && l > 0) {
			str[t]=c;
			t++;
			l = fread(&c,sizeof(char),1,fp);
			str = (char *)realloc(str, sizeof(char)*(t+1));
		}
		str[t]='\0';

		return str;
	}

	void write_comment(int32_t output, int fd, const char *comment)
	{
		switch (output) {
			case OUT_DEC: 
			case OUT_HEX: write(fd, comment,strlen(comment)); break;
			default : break; // no comments in binary form
		}
	}

	void write_comment(int32_t output, FILE *fp, const char *comment)
	{
		switch (output) {
			case OUT_DEC: 
			case OUT_HEX: fprintf(fp, "%s", comment); break;
			default : break; // no comments in binary form
		}
	}

	void write_string(int32_t output,FILE *fp, const char *i)
	{
		switch (output) {
			case OUT_DEC: fprintf(fp, "%s", i); break;
			case OUT_HEX: fprintf(fp, "%s", i); break;
			default : 
				fwrite((void *)i,strlen(i)+1,1,fp); break;
		}
	}

	void write_int32(int32_t output,FILE *fp, int32_t i)
	{
		int32_t swapped= htonl(i);
		switch (output) {
			case OUT_DEC: fprintf(fp, "%d", i); break;
			case OUT_HEX: fprintf(fp, "%x", i); break;
			default : fwrite(&swapped,sizeof(int32_t),1,fp); break;
		}
	}

	void write_int32(int32_t output, int fd, int32_t i)
	{
		//char s[10]={0};
		int32_t swapped= htonl(i);
		switch (output) {
			//case OUT_DEC: sprintf(s, "%d", swapped); 
				//write(fd, &swapped,strlen(s)); 
			//break;
			//case OUT_HEX: sprintf(s, "%x", swapped); 
				//write(fd, &swapped,strlen(s)); 
			//break;
			default : write(fd,&swapped,sizeof(int32_t)); break;
		}
	}

	void write_uint32(int32_t output, FILE *fp, uint32_t i)
	{
		uint32_t swapped= htonl(i);
		switch (output) {
			case OUT_DEC: fprintf(fp, "%u", i); break;
			case OUT_HEX: fprintf(fp, "%x", i); break;
			default : fwrite(&swapped,sizeof(uint32_t),1,fp); break;
		}
	}

	void write_uint32(uint32_t output, int fd, uint32_t i)
	{
		//char s[10]={0};
		uint32_t swapped= htonl(i);
		switch (output) {
			//case OUT_DEC: sprintf(s, "%u", swapped); 
				//write(fd, &swapped,strlen(s)); 
			//break;
			//case OUT_HEX: sprintf(s, "%x", swapped); 
				//write(fd, &swapped,strlen(s)); 
			//break;
			default : write(fd,&swapped,sizeof(uint32_t)); break;
		}
	}

	void write_uint16(int32_t output, FILE *fp, uint16_t i)
	{
		uint16_t swapped= htons(i);
		switch (output) {
			case OUT_DEC: fprintf(fp, "%hu", i); break;
			case OUT_HEX: fprintf(fp, "%hx", i); break;
			default : fwrite(&swapped,sizeof(uint16_t),1,fp); break;
		}
	}

	void write_int16(int32_t output, FILE *fp, int16_t i)
	{
		int16_t swapped= htons(i);
		switch (output) {
			case OUT_DEC: fprintf(fp, "%hd", i); break;
			case OUT_HEX: fprintf(fp, "%hx", i); break;
			default : fwrite(&swapped,sizeof(int16_t),1,fp); break;
		}
	}

	void write_uint8(int32_t output, int fd, uint8_t i)
	{
		//char s[10]={0};
		uint8_t swapped= i;
		switch (output) {
		//	case OUT_DEC: sprintf(s, "%hu", swapped); 
				//write(fd, &swapped,strlen(s)); 
			//break;
			//case OUT_HEX: sprintf(s, "%hx", swapped); 
				//write(fd, &swapped,strlen(s)); 
			//break;
			default : write(fd,&swapped,sizeof(uint8_t)); break;
		}
	}

	void write_uint8(int32_t output, FILE *fp, uint8_t i)
	{
		uint8_t swapped= i;
		switch (output) {
			case OUT_DEC: fprintf(fp, "%hu", i); break;
			case OUT_HEX: fprintf(fp, "%hx", i); break;
			default : fwrite(&swapped,sizeof(uint8_t),1,fp); break;
		}
	}

	void write_int8(int32_t output, FILE *fp, int8_t i)
	{
		switch (output) {
			case OUT_DEC: fprintf(fp, "%hd", i); break;
			case OUT_HEX: fprintf(fp, "%hx", i); break;
			default : fwrite(&i,sizeof(int8_t),1,fp); break;
		}
	}

	void write_coordinate(int32_t output, FILE *fp, int32_t i)
	{
		write_int32(output,fp,i);
	}

};


extern IO io;

class Haversine
{
	public:
	// Convert our passed value to Radians
	double ToRad( double nVal )
	{
    	return nVal * (M_PI/180);
	}

	double distance( double nLat1, double nLon1, double nLat2, double nLon2 )
	{       
    	double nRadius = 6371000.0; // Earth's radius in Kilometers
    	// Get the difference between our two points
    	// then convert the difference into radians
    	//printf("%f,%f,%f,%f\n", nLat1, nLon1, nLat2, nLon2);
	
    	double nDLat = ToRad(nLat2 - nLat1);
    	double nDLon = ToRad(nLon2 - nLon1);
       	 
    	// Here is the new line
    	nLat1 =  ToRad(nLat1);
    	nLat2 =  ToRad(nLat2);
       	 
    	double nA = pow ( sin(nDLat/2), 2 ) + cos(nLat1) * cos(nLat2) * pow ( sin(nDLon/2), 2 );
	
	
    	double nC = 2 * atan2( sqrt(nA), sqrt( 1 - nA ));
    	double nD = nRadius * nC;
	
    	//printf(" = %f\n", nD);
	
    	return nD; // Return our calculated distance in METERS !!
	}

	double distance( int32_t nLat1, int32_t nLon1, int32_t nLat2, int32_t nLon2 )
	{
		double x1 = (double)nLon1 / 10000000.0;
		double y1 = (double)nLat1 / 10000000.0;
		double x2 = (double)nLon2 / 10000000.0;
		double y2 = (double)nLat2 / 10000000.0;
			//printf("Trying %d (%d,%d)\n", n1->id,n1->x,n1->y);
		return distance(y1,x1,y2,x2);
	}

};

extern Haversine hav;

struct point_t
{
	int32_t x;
	int32_t y;

	friend ostream &operator<<(ostream &stream,point_t &pt)
	{
		cout << pt.y * 0.0000001;
		cout << " ";
		cout << pt.x * 0.0000001;
		cout << " ";
		return stream;
	}
};

// coords from lowerleft (-,-) to upperright(+,+)
struct rect_t
{
	point_t ll;
	point_t ur;

	friend ostream &operator<<(ostream &stream, rect_t &o)
	{
		cout << o.ll << o.ur << endl;
		return stream;
	}
};

typedef struct level_tag
{
	FILE *fps[4]; // index, nodes,roads,coords
} level_t;

typedef struct offsets_tag
{
	uint32_t tile;
	uint32_t nnodes;
	uint32_t nroads;
	uint32_t noffset;
	uint32_t roffset;
} offsets_t;

typedef struct stat_tag
{
	int ntiles;
	int nindices;
	int nnodes;
	int nroads;
	int minx;
	int miny;
	int maxx;
	int maxy;
	int minlen;
	int maxlen;
	int maxroad;
	int maxnode;
	int maxforward;
} stat_t;

typedef struct coord_tag
{   
    double x;
    double y; 
} coord_t;

typedef struct node_coord_tag
{
	uint32_t tile;
	uint32_t node;
	int32_t x;
	int32_t y; 
} node_coord_t;

#endif // GRID_H_INCLUDED
