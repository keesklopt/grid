#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

#include <math.h>
#include <gen.h>

#define GTK 1

int NO,EO;
unsigned int NCH,ECH;
uint64_t NMS;

draw_context_t osrm_context = {0};

//int ORIGEDG;

typedef struct check_tag
{
	uint64_t loname;
	uint64_t hiname;
	int lovia;
	int hivia;
	unsigned int loto;
	unsigned int hito;
	unsigned int lofrom;
	unsigned int hifrom;
	int loedge;
	int hiedge;
	int lowestedgeid;
	int higestedgeid;
} check_t;

check_t c = {0};

//#define DIRECTORY "./"
//#define DIRECTORY "/projects/gen/"
#define DIRECTORY "/data/projects/osrm/"

//#define NET "luxembourg"
//#define NET "network"
//#define NET "example"
//#define NET "vierkant"
//#define NET "net1"
#define NET "hoek"
//#define NET "map"
//#define NET "netherlands"
//#define NET "europe"
char **names;

const char *ts[] = {
"NoTurn", 
"GoStraight",
"TurnSlightRight",
"TurnRight",
"TurnSharpRight",
"UTurn",
"TurnSharpLeft",
"TurnLeft",
"TurnSlightLeft",
"ReachViaPoint",
"HeadOn",
"EnterRoundAbout",
"LeaveRoundAbout",
"StayOnRoundAbout",
"StartAtEndOfStreet",
"ReachedYourDestination"
};


nodeinfo *no_arr = NULL;
roadinfo *ro_arr = NULL;
worknode_t *nh_offsets = NULL;
edge *rh_arr = NULL;

osrm_road *origedge = NULL;

int osrm_nodecmp(const void *a, const void *b)
{
	worknode_t *A=(worknode_t *)a;
	worknode_t *B=(worknode_t *)b;

	return A->offset-B->offset;
}

int osrm_node_getcost(const void *nd)
{
	worknode_t *node = (worknode_t *)nd;
	return node->cost;
}

void osrm_node_setcost(const void *nd, int cost)
{
	worknode_t *node = (worknode_t *)nd;
	node->cost = cost;
}

heapfnc_t osrm_heap_fncs = { osrm_nodecmp, osrm_node_getcost, osrm_node_setcost } ;

draw_node_t osrm_get_node(draw_context_t *dc, int n)
{
	nodeinfo nd;
	nd = no_arr[n];
	
	draw_node_t nc={ nd.id,NULL, nd.lon , nd.lat } ;

	return nc;
}

/*
void draw_nodes()
{
	unsigned int n;

	for (n=0; n< osrm_context.nnodes; n++) {
		draw_node_t c = osrm_context.get_node(&osrm_context,n);

		gtk_draw_node(c);
	} 
}

void draw_edges()
{
	unsigned int n;

	for (n=0; n< dc.nedges; n++) {
		draw_edge_t e = dc.get_edge(&dc,n);

		gtk_draw_edge(e);
	} 
}
*/

draw_edge_t osrm_get_edge(draw_context_t *dc, int r)
{
	osrm_road ed = origedge[r];
	nodeinfo *from = find_node(ed.from);
	nodeinfo *to = find_node(ed.to);

	draw_node_t f = { from->id, NULL, from->lon, from->lat} ;
	draw_node_t t = { to->id, NULL, to->lon, to->lat} ;

	draw_edge_t de={ f, t, ed.dist, ed.type, ed.dir, 0} ;

	return de;
}

draw_edge_t osrm_get_hsgr_edge(draw_context_t *dc, int r)
{
	edge ed = rh_arr[r];

	worknode_t wn1 = nh_offsets[ed.fromnode];
	worknode_t wn2 = nh_offsets[ed.tonode];
	nodeinfo nd1 = no_arr[wn1.orignode];
	nodeinfo nd2 = no_arr[wn2.orignode];

	if (wn1.orignode<0) nd1.id=-1;
	if (wn2.orignode<0) nd2.id=-1;

	draw_node_t f = { nd1.id, NULL, nd1.lon, nd1.lat} ;
	draw_node_t t = { nd2.id, NULL, nd2.lon, nd2.lat} ;

	short dir = ed.ed.forward == 1;
	short sc = ed.ed.shortcut != 0;
	draw_edge_t de={ f, t, ed.ed.distance, 0, dir,sc} ;

	return de;
}

nodeinfo *find_node(int id)
{
	nodeinfo ni, *nip=NULL;
	ni.id=id;
	nip = (nodeinfo *)bsearch((void *)&ni,no_arr,NO,sizeof(nodeinfo),nodecmp);
	return nip;
}

void read_osrm(int *N, int *R)
{
	int n,r;
	
	char buf[1000];
	osrm_node nd;
	osrm_road rd;

	int loid=INT_MAX;
	int hiid= 0;

	sprintf(buf, "%s%s.osrm", DIRECTORY, NET);

	FILE *fp = fopen(buf, "r");

	fread(N, sizeof(int),1,fp);
	printf("nodes : %d\n", *N);
	for (n=0; n< *N; n++) {
		if (fread(&nd, sizeof(osrm_node), 1, fp) < 1) printf("S\n");
		if (BLAH) printf("%u,%d,%d,%d,%d\n", nd.id, nd.lat, nd.lon, nd.bollard, nd.trafficlight);
	}
	fread(R, sizeof(unsigned),1,fp);

	//ORIGEDG=*R;

	origedge = (osrm_road *)calloc(sizeof(osrm_road),*R);

	//printf("size : %ld R is %d\n", sizeof(rd), *R);
	for (r=0; r< *R; r++) {
		if (fread(&rd, sizeof(osrm_road), 1, fp) ==0) printf("short read!!\n");
		origedge[r] = rd;
		if (rd.nameid < loid) loid=rd.nameid;
		if (rd.nameid > hiid) hiid=rd.nameid;
		if (BLAH) printf("R: %d,%u,%u,%d,%d\n", rd.nameid, rd.from, rd.to, rd.dist, rd.weight);
	}
	int other;
	while (fread(&other, sizeof(int), 1, fp) > 0) printf("long read %d!!\n", other);

	printf("OSRM Edges : %d - %d\n", loid, hiid);
	fclose(fp);
}

/*
scale_t scale_stack(scale_t *scales, int num)
{
	int s;
	scale_t s2;
	//int x,y,w,h;

	for (s=0; s< num; s++) {
		//s2 = scale_set();
	}
}
*/

void read_osrm_hsgr(void)
{
	unsigned int r;
	int CRC;

	char buf[1000];
	//roadinfo rd;

	sprintf(buf, "%s%s.osrm.hsgr", DIRECTORY, NET);

	FILE *fp = fopen(buf, "r");

	printf("------ roads ------\n");
	fread(&CRC, sizeof(int),1,fp); // CRC 320
	fread(&NCH, sizeof(int),1,fp);
	printf("%d nodes\n", NCH);

	nh_offsets=(worknode_t *)calloc(sizeof(worknode_t),NCH);

	for (r=0; r< NCH; r++) {
		int wot;
		fread(&wot, sizeof(int), 1, fp);
		nh_offsets[r].offset=wot;
		nh_offsets[r].orignode = -1;
		if (BLAH) printf("%d\n", wot);
	}
	fread(&ECH, sizeof(int),1,fp);
	printf("roads %d\n", ECH);

	rh_arr = (edge *)calloc(sizeof(edge),ECH*2);

	c.hiedge=0;
	c.hito=0;
	c.hifrom=0;
	c.loedge=100000000;
	c.loto=100000000;
	c.lofrom=100000000;
	int sc=0;
	int orig=0;
	int fromnode=-1;
	for (r=0; r< ECH; r++) {
		edge ed;
		//if (fread(&to, sizeof(int), 1, fp) != 1) printf("Short\n");

		while (r==nh_offsets[fromnode+1].offset) fromnode++;

		ed.fromnode = fromnode;
		if (fread(&ed.tonode, sizeof(int), 1, fp) != 1) printf("Short\n");
		if (fread(&ed.ed, sizeof(edgedata), 1, fp) != 1) printf("Short\n");
		if (BLAH) printf("%ld %d -> from %d to %d %d %d %d %d\n", sizeof(edgedata), ed.ed.id, ed.fromnode, ed.tonode, ed.ed.distance, ed.ed.shortcut, ed.ed.forward, ed.ed.backward);
		if (ed.ed.id > c.hiedge) c.hiedge=ed.ed.id;
		if (ed.ed.id < c.loedge) c.loedge=ed.ed.id;
		if (ed.tonode > c.hito) c.hito = ed.tonode;
		if (ed.tonode < c.loto) c.loto = ed.tonode;
		if (ed.fromnode > c.hifrom) c.hifrom = ed.fromnode;
		if (ed.fromnode < c.lofrom) c.lofrom = ed.fromnode;
		if (ed.ed.shortcut==1) sc++; else orig++;
		rh_arr[r]= ed;
	}

	printf("ID %d-%d, TO %d=%d\n", c.loedge, c.hiedge, c.loto, c.hito);
	printf("shortcuts %d originals %d\n", sc, orig);
	char excess;
	if (fread(&excess, 1, 1, fp) != 0) printf("Long\n");

	fclose(fp);
}

int high=0;

void read_osrm_edges()
{
	int r;
	
	char buf[1000];
	roadinfo rd;

	sprintf(buf, "%s%s.osrm.edges", DIRECTORY, NET);

	FILE *fp = fopen(buf, "r");

	//printf("------ osrm.edges ------\n");
	fread(&EO, sizeof(int),1,fp);
	printf("%d roads %ld size \n", EO, sizeof(roadinfo));
	
	ro_arr = (roadinfo *)calloc(sizeof(roadinfo),EO);
	
	c.lowestedgeid=INT_MAX;
	c.higestedgeid=0;

	c.lovia=INT_MAX;
	c.hivia=0;
	c.loname=INT_MAX;
	c.hiname=0;
	for (r=0; r< EO; r++) {
		if (fread(&rd, sizeof(roadinfo), 1, fp) == 0) printf("Short read!!\n");
		char *name = names[rd.nameid];
		if (rd.vianode > c.hivia) c.hivia = rd.vianode;
		if (rd.vianode < c.lovia) c.lovia = rd.vianode;
		if (rd.nameid > c.hiname) c.hiname = rd.nameid;
		if (rd.nameid < c.loname) c.loname = rd.nameid;
		if (BLAH) printf("%d at node %d %s, onto road %s\n", r, rd.vianode, ts[(int)rd.turn], name);
		ro_arr[r]= rd;
	}
	char excess;
	printf("via is %d-%d\n", c.lovia, c.hivia);
	printf("names is %ld-%ld\n", c.loname, c.hiname);
	if (fread(&excess, 1, 1, fp) != 0) printf("Longue\n");
	fclose(fp);
}

void read_osrm_names(void)
{
	unsigned int r;
	
	char buf[1000];

	sprintf(buf, "%s%s.osrm.names", DIRECTORY, NET);

	FILE *fp = fopen(buf, "r");

	printf("------ names ------\n");
	fread(&NMS, sizeof(uint64_t),1,fp);
	printf("%ld names \n", NMS);

	names = (char **)calloc(sizeof(char **),NMS);
	
	for (r=0; r< NMS-1; r++) {
		int len;
		//char name[1024] = {0};
		fread(&len, sizeof(int), 1, fp);
		//printf("%d string of %d expected : ", r, len);
		names[r+1] = (char *)calloc(sizeof(char *),len+1);
		if (fread(names[r+1], len, 1, fp) == 0) printf("Sjordt %d %s\n", len, names[r+1]);
		if (BLAH) printf("\"%s\"\n", names[r]);
	}
	if (BLAH) printf("\"%s\"\n", names[r]);
	fclose(fp);
}


void read_osrm_nodes()
{
	char buf[1000];
	nodeinfo nd;

	sprintf(buf, "%s%s.osrm.nodes", DIRECTORY, NET);

	FILE *fp = fopen(buf, "r");

	//printf("------ nodes ------\n");
	//fread(&N, sizeof(int),1,fp);
	//printf("%d nodes \n", N);

	int lox=180000000;
	int hix=-180000000;
	int loy=90000000;
	int hiy=-90000000;

	NO=0;
	while (fread(&nd, sizeof(nodeinfo), 1, fp) >0) {
		NO++;
		//printf("%d,%d,%d\n", nd.id, nd.lat, nd.lon);
	}
	fseek(fp,0,SEEK_SET);
	no_arr = (nodeinfo *) calloc(sizeof(nodeinfo),NO);
	NO=0;
	while (fread(&nd, sizeof(nodeinfo), 1, fp) >0) {
		no_arr[NO++] = nd;
		if (BLAH) printf("%u,%d,%d\n", nd.id, nd.lat, nd.lon);
		if (nd.lat < loy) loy = nd.lat;
		if (nd.lat > hiy) hiy = nd.lat;
		if (nd.lon < lox) lox = nd.lon;
		if (nd.lon > hix) hix = nd.lon;
	}
	printf("Counted %d nodes\n", NO);

	printf("Boundbox is %d,%d,%d,%d\n", lox, loy, hix, hiy);
#ifdef GTK
	gtk_draw_init_stack(lox,loy,hix,hiy);
#else
	ogl_draw_init_stack(lox,loy,hix,hiy);
#endif

	char excess;
	if (fread(&excess, 1, 1, fp) != 0) printf("Longue\n");
	fclose(fp);
}

void read_osrm_restrictions()
{
	int r;
	
	char buf[1000];
	//nodeinfo nd;

	sprintf(buf, "%s%s.osrm.restrictions", DIRECTORY, NET);

	FILE *fp = fopen(buf, "r");

	printf("------ restrictions ------\n");
	fread(&r, sizeof(int),1,fp);
	//printf("%d restrictions \n", r);

	restriction_t rest[r];

	int num = fread(&rest, sizeof(restriction_t),r,fp);
	
	if (num != r) {
		printf("Short read !!\n");
	}
	char excess;
	printf("Read %d\n", num);
	if (fread(&excess, 1, 1, fp) != 0) printf("Longue\n");
	fclose(fp);
}

int check_all(check_t c)
{
	int x=0;
	if (NMS-1 != c.hiname) { 
		printf("there are %ld names (plus the empty name \"\" but the highest reference is %ld\n", NMS-1, c.hiname);
		x++;
	}
	//if (1 != c.loname) { // NOPE.. no name has index 0 !!
		//printf("lowest name reference should be 1, it is %ld\n", c.loname);
		//x++;
	//}
	if (NO-1 != c.hivia) { 
		printf("there are %d (0-%d) original nodes but the highest reference is %d\n", NO, NO-1, c.hivia);
		x++;
	}
	if (0 != c.lovia) {
		printf("lowest original node reference should be 0, it is %d\n", c.lovia);
		x++;
	}
	// hierarchy : there is an extra node to hold a firstedge value to calculate
	// the last edge of the real last node
	if (NCH-2 != c.hito && NCH-2 != c.hifrom) {  
		printf("there are %d (0-%d) nodes but the highest to node reference is %d and the highest from ref is %d\n", NCH-1, NCH-2, c.hito, c.hifrom);
		x++;
	}
	if (0 != c.loto && 0 != c.lofrom) {
		printf("lowest to node reference should be 0, it is %d (from) and %d (to)\n", c.lofrom, c.loto);
		x++;
	}

	// fill orignode
	unsigned int r;
	for (r=0; r<ECH; r++) {
		edge ed = rh_arr[r];
		if (ed.ed.shortcut!=0) continue;
		roadinfo rd = ro_arr[ed.ed.id];
		if (ed.ed.backward==1) {
			nh_offsets[ed.fromnode].orignode = rd.vianode;
		} 
		if (ed.ed.forward==1) {
			nh_offsets[ed.tonode].orignode = rd.vianode;
		} 
		// add reversed edge
		//rh_arr[r+ECH].fromnode = rh_arr[r].tonode;
		//rh_arr[r+ECH].tonode = rh_arr[r].fromnode;
		//rh_arr[r+ECH].ed = rh_arr[r].ed;
		//rh_arr[r+ECH].ed.forward = rh_arr[r].ed.backward;
		//rh_arr[r+ECH].ed.backward = rh_arr[r].ed.forward;
	}

	return x;
}

void traverse_network(void)
{
	unsigned int r;

	heap_t *heap = heap_ist(1000,osrm_heap_fncs);

	for (r=0; r< NCH; r++) {
		nh_offsets[r].eval=0;
		nh_offsets[r].cost=HEAP_CLEAR;
		nh_offsets[r].orignode = -1;
	} 
	worknode_t *start = &nh_offsets[0];
	worknode_t *next;
	worknode_t *nd;
	//nodeinfo *from=NULL;

	unsigned int fr = nh_offsets[start->offset].offset;
	unsigned int lr = nh_offsets[start->offset+1].offset-1;
	printf("Start at %d [%d-%d]\n", start->offset, fr, lr);

	//start.eval=1; // NOPE !! not drawn yet !!

	start->cost = 0;
	heap_add(heap, start);
	//heap_dmp(heap);

//int stop=100;

	while (1) {

		if (heap_empty(heap)) {
			//printf("No endpoint found\n");
			goto cleanup;
		}
		next = (worknode_t *)heap_get(heap, 0);
		int rank = (next-nh_offsets);
		fr = nh_offsets[rank].offset;
		lr = nh_offsets[rank+1].offset-1;

		printf("Next at %d [%d-%d]\n", rank, fr, lr);

		for (r=fr; r<=lr; r++) {
//if (stop-- < 0) return;
			edge rd = rh_arr[r];

			if (rd.ed.shortcut) {
				printf("Skipping shortcut\n");
				continue;
			} 
			roadinfo ri = ro_arr[rd.ed.id];
			nd = &nh_offsets[rd.tonode];

			if (rd.ed.backward) {
				//from = &no_arr[ri.vianode];
				//nd->orignode = ri.vianode;
				continue;
			} 

			//draw_edge_t e;
			printf("Road from %d to %d %d long via is %d\n", rd.fromnode, rd.tonode, rd.ed.distance, ri.vianode);
			//gtk_draw_edge(rd);
			if (nd->eval) continue; // evaluated
			if (heap->getcost(nd) == HEAP_CLEAR) { // new to the heap
				//printf("cost of %d was %d\n", nd->offset, nd->cost);
				//nd->cost = ROADCOST(r);
				heap->setcost(nd, heap->getcost(next)+rd.ed.distance);
				// insert on cost
				//printf("setting %d to %d\n", nd->offset, nd->cost);
				nd->eval=1;
				heap_add(heap, nd);

				nd->orignode = ri.vianode;
				if (rd.ed.backward) 
					nh_offsets[rd.fromnode].orignode = ri.vianode;
				else 
					nh_offsets[rd.tonode].orignode = ri.vianode;

				draw_node_t c = {0};
				nodeinfo node = no_arr[ri.vianode];
				c.id= node.id;
				c.x = node.lon;
				c.y = node.lat;
#ifdef GTK
				//gtk_draw_node(c);
#else
				//ogl_draw_node(c);
#endif

				/*
				if (from) {
					draw_edge_t e = {0};
					e.from = f;
					e.to = c;
					e.len = rd.ed.distance;
					e.shortcut = rd.ed.shortcut;
					gtk_draw_edge(e);
				} 
				*/

			} else { // already in the heap
				// only set cost if lower
				if (heap->getcost(next)+rd.ed.distance < heap->getcost(nd)) {
					heap_del(heap,nd);
					//printf("Deleting %d ---\n", nd->cost);
					//heap_dmp(heap);
					heap->setcost(nd, heap->getcost(next)+rd.ed.distance);
					//printf("And adding as %d ---\n", nd->cost);
					//heap_dmp(heap);
					nd->eval=1;
					heap_add(heap,nd);
					//cost_set(cost,nd,nd->cost);
				}
			}
		} 
		heap_del(heap,next);
		//cost_add(cost,next,next->cost2);
		next->eval = 1;
	}
cleanup:
	heap_rls(heap);
}

int main(int a, char *v[])
{
	printf("Osrm reader\n");
	int N,R;

	read_osrm(&N,&R);
	//read_osrm_restrictions();

#ifdef GTK
	gtk_draw_init(a,v);
#else
	ogl_draw_init(a, v);
#endif

	read_osrm_names();
	read_osrm_nodes();
	read_osrm_edges();
	read_osrm_hsgr();

	check_all(c);

	osrm_context.nnodes = N;
	osrm_context.get_node=osrm_get_node;
	//osrm_context.nedges = R;
	//osrm_context.get_edge=osrm_get_edge;
	osrm_context.nedges = ECH;
	osrm_context.get_edge=osrm_get_hsgr_edge;
#ifdef GTK
	gtk_draw_set_context(osrm_context);
#else
	ogl_draw_set_context(osrm_context);
#endif

	//draw_nodes();
	//draw_edges();

	//traverse_network();
#ifdef GTK
	gtk_draw_run();
#else
	ogl_draw_run();
#endif

	//printf("%d\n", sizeof(edgedata));
	return 0;
}
