#include <gen.h>
#include <level.h>
#include <iostream>
#include <cmath>
#include <cstring>

using namespace std;

int main(int c, char *v[])
{	
	int t;
	int start=0,stop=10;
	int x;
	LevelSquare levelinfo;
	rect_t boundbox;

	Xyz xyz;

	levelinfo.dump();

	if (c > 1) {
		start = atol(v[1]);
		stop = start+1;
	}

	/*
	for (t=start; t< 20000000000; t++) {
		if (levelinfo.parent(t) != levelinfo.parentold(t)) {
			cout << " fail " << t;
			exit (0);
		}
	}
	*/
	for (t=start; t< stop; t++) {
		//int level = levelinfo.level(t);
		xyz = levelinfo.xyz(t);
		cout << "zyx: : " << xyz << endl;
		cout << "t    : " << t << endl;
		cout << "xy2t : " << levelinfo.xy_2_tile(xyz) << endl;
		boundbox= levelinfo.boundbox(xyz);
		cout << "bb   : " << boundbox;
		cout << "parent : " << levelinfo.parent(t)  << "\n";
		cout << "neighbours : ";
		int *n = levelinfo.neighbours(t);
		for (x=0;x< 8; x++) 
			cout << n[x] << ",";
		cout << "\nchildren : ";
		n = levelinfo.children(t);
		if (n==NULL) cout << "none";
		else
			for (x=0;x< 4; x++) 
				cout << n[x] << ",";
		cout << "\n\n";
	}

	return 0;
}
