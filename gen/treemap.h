#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include <iostream>
#include <zlib.h>
#include <iostream>
#include <cstring>
#include <search.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <expat.h>

typedef struct treemap_tag treemap_t;

// sorry we, need this to convert into an array and remain reentrant
typedef struct treeelm_tag
{
	treemap_t *tm;
	void *data;
} treeelm_t;

struct treemap_tag
{
	int n; // num items
	treeelm_t *tree; // gnu binary tree
	void **array;
	int arrlen; // same as n, but needed for filling
	int (*cmp)(const void *, const void *);
	void *fncptr; 
	void *udata;
} ;

/* treemap.cpp */
treemap_t *treemap_ist(int (*)(const void *, const void *));
void treemap_elm_rls(const void *treemapp, const VISIT which, const int depth);
void treemap_walk(treemap_t *tm, void (*fnc)(const void *, void *), void *udata);
void treemap_rls(treemap_t *cp,int);
int treemap_len(treemap_t *cp);
void *treemap_find(treemap_t *cp, void *);
void *treemap_get(treemap_t *cp, void *item);
void treemap_del(treemap_t *cp, void *te);
void *treemap_add(treemap_t *cp, void *);
void *treemap_insert(treemap_t *cp, void *item);
void *treemap_array(treemap_t *tm);
void treemap_array_rls(treemap_t *tm);
