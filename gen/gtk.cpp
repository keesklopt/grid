#include <gen.h>
#include <draw.h>
#include <stdlib.h>

#include <gtk/gtk.h>

extern GtkDraw d;

static GtkWidget *da;

static cairo_surface_t *surface = NULL;

static gdouble downx=-1.0;
static gdouble downy=-1.0;

#define MAX_STACK 40
static int stackpointer=0;

static world_t stack[MAX_STACK];

static scale_t ys;
static scale_t xs;

void redraw_all();

typedef struct colors_tag
{
	double r;
	double g;
	double b;
	double a;
}  colors_t;

colors_t clrs[] = {
	{ 0.0, 0.0, 0.0, 1.0 },
	{ 14.0, 0.0, 0.0, 1.0 },
	{ 0.0, 14.0, 0.0, 1.0 },
	{ 0.0, 0.0, 14.0, 1.0 },
	{ 14.0, 14.0, 14.0, 1.0 },
};
 

#define SW 1000 // screen width
#define W 800
#define RW 200 // rest width
#define H 800

#define MARGIN 80

static draw_context_t dc = {0};

void gtk_draw_init_stack(double lox, double loy, double hix, double hiy)
{
	stack[0].l = lox;
	stack[0].r = hix;
	stack[0].t = loy;
	stack[0].b = hiy;

	xs = scale_set(lox,hix,MARGIN,W-MARGIN);
	ys = scale_set(hiy,loy,MARGIN,H-MARGIN);
}

static void close_window (void)
{
  if (surface)
    cairo_surface_destroy (surface);

  gtk_main_quit ();
}

static void clear_surface (void)
{
  cairo_t *cr;

  cr = cairo_create (surface);

  cairo_set_source_rgb (cr, 1, 1, 1);
  cairo_paint (cr);

  cairo_destroy (cr);
}

/* Create a new surface of the appropriate size to store our scribbles */
static gboolean
configure_event_cb (GtkWidget         *widget,
            GdkEventConfigure *event,
            gpointer           data)
{
  if (surface)
    cairo_surface_destroy (surface);

  surface = gdk_window_create_similar_surface (gtk_widget_get_window (widget),
                                       CAIRO_CONTENT_COLOR,
                                       gtk_widget_get_allocated_width (widget),
                                       gtk_widget_get_allocated_height (widget));

  /* Initialize the surface to white */
  clear_surface ();

  /* We've handled the configure event, no need for further processing. */
  return TRUE;
}

	/* for rubberbanding (later)
static void
draw_brush (GtkWidget *widget,
    gdouble    x,
    gdouble    y)
{
  cairo_t *cr;

  cr = cairo_create (surface);

  //cairo_rectangle (cr, x - 3, y - 3, 6, 6);
  cairo_rectangle (cr, downx, downy , x , y);
  cairo_fill (cr);

  cairo_destroy (cr);

  // Now invalidate the affected region of the drawing area.
  //gtk_widget_queue_draw_area (widget, x - 3, y - 3, 6, 6);
}
*/

static gboolean draw_cb (GtkWidget *widget,
 cairo_t   *cr,
 gpointer   data)
{
  cairo_set_source_surface (cr, surface, 0, 0);
  cairo_paint (cr);

  return FALSE;
}

// overwrote plugin context for drawing networks
void gtk_draw_set_context(draw_context_t drawcontext)
{
	dc = drawcontext;
}

void gtk_draw_node(draw_node_t c)
{
	int x,y;
  	cairo_t *cr;

	cr = cairo_create (surface);

	x = scale_get_model(xs,c.x);
	y = scale_get_model(ys,c.y);

	//printf("NODE at %d,%d\n", x, y);

	cairo_set_source_rgba (cr, 
		clrs[c.color].r, 
		clrs[c.color].g,
		clrs[c.color].b, 
		clrs[c.color].a);

  	//cairo_rectangle (cr, x - 3, y - 3, 6, 6);
	cairo_arc(cr,x,y,3.0,0.0,2 * M_PI);
  	cairo_fill (cr);

	if (dc.nodenames) {
		string text;
		//char text[400]= {0};
		//snprintf(text, 400, "%d", c.id);
		if (c.name) text = c.name;
		else text = "";
		cairo_text_extents_t te;
		cairo_set_source_rgb (cr, 0.6, 0.0, 0.0);
		cairo_select_font_face (cr, "Georgia",
    CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
		cairo_set_font_size (cr, 9.0);
		cairo_text_extents (cr, text.c_str(), &te);
		cairo_move_to (cr, x, y);
		cairo_show_text (cr, text.c_str());
	} 
    cairo_destroy (cr);
}

void gtk_draw_nodes()
{
	unsigned int n;
	int x,y;
  	cairo_t *cr;

	cr = cairo_create (surface);

	printf("nnodes %d\n", dc.nnodes);
	
	for (n=0; n< dc.nnodes; n++) {

		draw_node_t c = dc.get_node(&dc,n);
		x = scale_get_model(xs,c.x);
		y = scale_get_model(ys,c.y);

		printf("%d,%d\n", c.x, c.y);

  		//cairo_rectangle (cr, x - 3, y - 3, 6, 6);
		cairo_arc(cr,x,y,1.0,0.0,2 * M_PI);
  		cairo_fill (cr);

		char text[400]= {0};
		snprintf(text, 400, "%d(%d)", n, c.id);
		if (dc.nodenames) {
			cairo_text_extents_t te;
			cairo_set_source_rgb (cr, 0.6, 0.0, 0.0);
			cairo_select_font_face (cr, "Georgia",
    CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
			cairo_set_font_size (cr, 9.0);
			cairo_text_extents (cr, text, &te);
			cairo_move_to (cr, x, y);
			cairo_show_text (cr, text);
		} 
	}
  cairo_destroy (cr);
}

void gtk_draw_edge(draw_edge_t e)
{
  	cairo_t *cr;

	cr = cairo_create (surface);

	int x1 = scale_get_model(xs,e.from.x);
	int y1 = scale_get_model(ys,e.from.y);

	int x2 = scale_get_model(xs,e.to.x);
	int y2 = scale_get_model(ys,e.to.y);

/*
	if (e.shortcut!=0) {
		cairo_set_source_rgba (cr, 14, 0, 0,0.3);
	} else {
		cairo_set_source_rgba (cr, 0, 0, 14, 1.0);
	} 
*/
	cairo_set_source_rgba (cr, 
		clrs[e.color].r, 
		clrs[e.color].g,
		clrs[e.color].b, 
		clrs[e.color].a);

	cairo_set_line_width (cr, 0.5);
	cairo_move_to(cr, x1, y1);
	cairo_line_to(cr, x2, y2);

	printf("%d,%d %d,%d, %d\n", x1,y1,x2,y2, dc.roadlength);

	if (dc.roadlength) {
		char text[400];
		sprintf(text, "%d", e.len);
		cairo_text_extents_t te;
		cairo_set_source_rgb (cr, 0.0, 0.6, 0.0);
		cairo_select_font_face (cr, "Georgia",
    CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
		cairo_set_font_size (cr, 9.0);
		cairo_text_extents (cr, text, &te);
		cairo_move_to (cr, x1+((x2-x1)/2), y1+((y2-y1)/2));
		cairo_show_text (cr, text);
	} 

	cairo_stroke(cr);
  	cairo_destroy (cr);
}

void gtk_draw_edges()
{
	unsigned int r;
  	cairo_t *cr;

	cr = cairo_create (surface);

	printf("nedges %d\n", dc.nedges);

	for (r=0; r< dc.nedges; r++) {
		//osrm_road ed = origedge[r];


		draw_edge_t e = dc.get_edge(&dc,r);
		gtk_draw_edge(e);
	}
  	cairo_destroy (cr);
}

void redraw_all()
{
    clear_surface ();

	d.Fill();
	
	gtk_draw_nodes();
	gtk_draw_edges();

	//draw_origedges_gtk();
	//draw_edges_gtk();
  	gtk_widget_queue_draw_area (da, 0, 0, W, H);
}

static gboolean
button_press_event_cb (GtkWidget      *widget,
               GdkEventButton *event,
               gpointer        data)
{

	printf("button is %d\n", event->button);
  /* paranoia check, in case we haven't gotten a configure event */
  if (surface == NULL)
    return FALSE;

  if (event->button == 1)
    {
		downx = event->x;
		downy = event->y;
		//printf("down %d,%d\n", downx, downy);
    }
  else if (event->button == 3)
    {

			stackpointer--;
			if (stackpointer<0) stackpointer=0;
			xs = scale_set_world(xs,stack[stackpointer].l,stack[stackpointer].r);
			ys = scale_set_world(ys,stack[stackpointer].b,stack[stackpointer].t);
			redraw_all();
  			gtk_widget_queue_draw_area (da, 0, 0, W, H);
    }

  /* We've handled the event, stop processing */
  return TRUE;
}

static gboolean
button_release_event_cb (GtkWidget      *widget,
               GdkEventButton *event,
               gpointer        data)
{

  /* paranoia check, in case we haven't gotten a configure event */
  if (surface == NULL)
    return FALSE;

printf("Up : button is %d\n", event->button);
  if (event->button == 1)
    {
      clear_surface ();
      //gtk_widget_queue_draw (widget);
			int x=event->x,
			    y=event->y;

			printf("Release on %d,%d\n", x, y);
			//printf("Selected on %d,%d\n", downx, downy);

			printf("new world X would be %d,%d\n", scale_get_world(xs,downx), scale_get_world(xs,x));

			stackpointer++;
			if (x > downx) {
				stack[stackpointer].l = scale_get_world(xs,downx);
				stack[stackpointer].r = scale_get_world(xs,x);
			} else {
				stack[stackpointer].l = scale_get_world(xs,x);
				stack[stackpointer].r = scale_get_world(xs,downx);
			} 
			if (y > downy) {
				stack[stackpointer].t = scale_get_world(ys,y);
				stack[stackpointer].b = scale_get_world(ys,downy);
			} else {
				stack[stackpointer].t = scale_get_world(ys,downy);
				stack[stackpointer].b = scale_get_world(ys,y);
			} 
			xs = scale_set_world(xs,stack[stackpointer].l,stack[stackpointer].r);
			ys = scale_set_world(ys,stack[stackpointer].b,stack[stackpointer].t);
			//redraw();
			redraw_all();
  			gtk_widget_queue_draw_area (da, 0, 0, W, H);
    }

  /* We've handled the event, stop processing */
  return TRUE;
}

/* Handle motion events by continuing to draw if button 1 is
 * still held down. The ::motion-notify signal handler receives
 * a GdkEventMotion struct which contains this information.
 */
static gboolean
motion_notify_event_cb (GtkWidget      *widget,
                GdkEventMotion *event,
                gpointer        data)
{
  int x, y;
  GdkModifierType state=GDK_BUTTON1_MASK;
	GdkDeviceManager *device_manager;
	GdkDevice *pointer;

   /* paranoia check, in case we haven't gotten a configure event */
   if (surface == NULL)
       return FALSE;

	//device_manager = gdk_display_get_device_manager (gtk_widget_get_display (widget));
  	//pointer = gdk_device_manager_get_client_pointer (device_manager);
    //gdk_window_get_device_position (gtk_widget_get_window (widget), pointer, &x, &y, &state);

	//printf("%d and %d\n", x, y);


  //if (state & GDK_BUTTON1_MASK)
    //draw_brush (widget, x, y);

  /* We've handled it, stop processing */
  return TRUE;
}

void toggle_rl(GtkWidget *widget, gpointer window) {
  	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
		dc.roadlength = 1;
  	} else {
		dc.roadlength = 0;
 	}
	redraw_all();
}

void toggle_nn(GtkWidget *widget, gpointer window) {
  	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
		dc.nodenames = 1;
  	} else {
		dc.nodenames = 0;
 	}
	redraw_all();
}

void gtk_draw_init(int argc, char *argv[]) {
	GtkWidget *fixed;
	GtkWidget *buttonbox;
    GtkWidget *window;

    gtk_init (&argc, &argv);
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

	// add close event
	g_signal_connect (window, "destroy", G_CALLBACK (close_window), NULL);
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);
    gtk_window_set_title (GTK_WINDOW (window), "Map, left button zooms in, right button zooms out.");
	
	fixed = gtk_fixed_new ();
	gtk_widget_set_size_request (fixed, SW, H);
	gtk_container_add (GTK_CONTAINER (window), fixed);

	da = gtk_drawing_area_new ();
	gtk_fixed_put(GTK_FIXED(fixed), da, 0,0);
	gtk_widget_set_size_request(da, W,H);

	buttonbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);

	// gimme some check buttons : 
	GtkWidget *check_nn = gtk_check_button_new_with_label("nodenames");
	gtk_container_add(GTK_CONTAINER(buttonbox),check_nn);
	g_signal_connect(check_nn, "clicked", G_CALLBACK(toggle_nn), (gpointer) window);

	GtkWidget *check_rl = gtk_check_button_new_with_label("road lengths");
	gtk_container_add(GTK_CONTAINER(buttonbox),check_rl);
	g_signal_connect(check_rl, "clicked", G_CALLBACK(toggle_rl), (gpointer) window);

	gtk_fixed_put(GTK_FIXED(fixed), buttonbox, W,0);
	gtk_widget_set_size_request(buttonbox, RW,H);

	g_signal_connect (da, "draw", G_CALLBACK (draw_cb), NULL);
	g_signal_connect (da,"configure-event", G_CALLBACK (configure_event_cb), NULL);

	 /* Event signals */
    g_signal_connect (da, "motion-notify-event", G_CALLBACK (motion_notify_event_cb), NULL);
    g_signal_connect (da, "button-press-event", G_CALLBACK (button_press_event_cb), NULL);
    g_signal_connect (da, "button-release-event", G_CALLBACK (button_release_event_cb), NULL);

	gtk_widget_set_events (da, gtk_widget_get_events (da)
                     | GDK_BUTTON_PRESS_MASK
                     | GDK_BUTTON_RELEASE_MASK
                     | GDK_POINTER_MOTION_MASK
                     | GDK_POINTER_MOTION_HINT_MASK);

    gtk_widget_show_all (window);
}

void gtk_draw_run()
{
	//redraw_all();
	gtk_main();
}
