#include <gen.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

static draw_context_t dc = {0};

static float rotX = 0.0, rotY = 0.0;

#define SW 1000 // screen width
#define W 800
#define RW 200 // rest width
#define H 800

void init(void)
{
    glClearColor (0.7, 0.7, 0.7, 0.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	glOrtho(0.0, W, 0.0, H, -1.0, 1.0);
}

#define MAX_STACK 400
//static int stackpointer=0;

static world_t stack[MAX_STACK];

static scale_t ys;
static scale_t xs;

//static draw_context_t dc = {0};

void ogl_draw_init_stack(double lox, double loy, double hix, double hiy)
{
	stack[0].l = lox;
	stack[0].r = hix;
	stack[0].t = loy;
	stack[0].b = hiy;

	xs = scale_set(lox,hix,40,W-40);
	ys = scale_set(hiy,loy,40,H-40);
}

// overwrote plugin context for drawing networks
void ogl_draw_set_context(draw_context_t drawcontext)
{
	dc = drawcontext;
}

void ogl_draw_nodes()
{
	unsigned int n;
	int x,y;

	//printf("nnodes %d\n", dc.nnodes);
	
	for (n=0; n< dc.nnodes; n++) {

		draw_node_t c = dc.get_node(&dc,n);

		x = scale_get_model(xs,c.x);
		y = scale_get_model(ys,c.y);

        glVertex3f (x, y, 0.0);
		//printf("%d,%d\n", x, y);

  		//cairo_rectangle (cr, x - 3, y - 3, 6, 6);
		//cairo_arc(cr,x,y,1.0,0.0,2 * M_PI);
  		//cairo_fill (cr);
	} 
}

void draw_node(draw_node_t c)
{
	int x,y;

	x = scale_get_model(xs,c.x);
	y = scale_get_model(ys,c.y);

	printf("NODE at %d,%d\n", x, y);

  	//cairo_rectangle (cr, x - 3, y - 3, 6, 6);

	glClear (GL_COLOR_BUFFER_BIT);

	//cairo_arc(cr,x,y,3.0,0.0,2 * M_PI);
  	//cairo_fill (cr);
    //cairo_destroy (cr);
}

#ifdef LATER
void draw_edge(draw_edge_t e)
{
  	cairo_t *cr;

	cr = cairo_create (surface);

	int x1 = scale_get_model(xs,e.from.x);
	int y1 = scale_get_model(ys,e.from.y);

	int x2 = scale_get_model(xs,e.to.x);
	int y2 = scale_get_model(ys,e.to.y);

	if (e.shortcut!=0) {
		//sprintf(text, "%d", ed.ed.distance);
		//XSetForeground(dis,gc,green.pixel);
		cairo_set_source_rgba (cr, 14, 0, 0,0.3);
	} else {
		//sprintf(text, "%d", ed.ed.distance);
	    //sprintf(text, "%s (%d)", names[ed.ed.id], ed.ed.distance);
		//XSetForeground(dis,gc,black);
		cairo_set_source_rgba (cr, 0, 0, 14, 1.0);
	} 
	cairo_set_line_width (cr, 0.5);
	cairo_move_to(cr, x1, y1);
	cairo_line_to(cr, x2, y2);

	cairo_stroke(cr);
  	cairo_destroy (cr);
}

void draw_edges()
{
	unsigned int r;
  	cairo_t *cr;

	cr = cairo_create (surface);

	printf("nedges %d\n", dc.nedges);

	for (r=0; r< dc.nedges; r++) {
		//osrm_road ed = origedge[r];

		draw_edge_t e = dc.get_edge(r);

		int x1 = scale_get_model(xs,e.from.x);
		int y1 = scale_get_model(ys,e.from.y);

		int x2 = scale_get_model(xs,e.to.x);
		int y2 = scale_get_model(ys,e.to.y);

	//	printf("%d to %d\n", e.from.id, e.to.id);
		// IF an edge is not present (one way, etc) the from 
		// or to will not contain a valid id, skip those edges
		if (e.from.id <0 || e.to.id < 0) continue;

		if (e.shortcut!=0) {
			//sprintf(text, "%d", ed.ed.distance);
			//XSetForeground(dis,gc,green.pixel);
			cairo_set_source_rgba (cr, 14, 0, 0,0.3);
		} else {
			//sprintf(text, "%d", ed.ed.distance);
		    //sprintf(text, "%s (%d)", names[ed.ed.id], ed.ed.distance);
			//XSetForeground(dis,gc,black);
			cairo_set_source_rgba (cr, 0, 0, 14, 1.0);
		} 
		cairo_set_line_width (cr, 0.5);
		cairo_move_to(cr, x1, y1);
		cairo_line_to(cr, x2, y2);

		cairo_stroke(cr);

		//printf("%d,%d to %d,%d\n", x1, y1, x2, y2);

		//if (ed.id > 0) {
		//	char text[100];
	//		sprintf(text, "%s (%d)", names[ed.id], ed.weight);
		//	if (STRINGS) XDrawString(dis,win,gc,(int)((x1+x2)/2)+10,(int)(y1+y2)/2, text, strlen(text));
		//} 
	}
  cairo_destroy (cr);
}

void redraw_all()
{
    clear_surface ();
	draw_nodes();
	draw_edges();

	draw_origedges_gtk();
	//draw_edges_gtk();
  	gtk_widget_queue_draw_area (da, 0, 0, W, H);
}

static gboolean
button_press_event_cb (GtkWidget      *widget,
               GdkEventButton *event,
               gpointer        data)
{

	printf("button is %d\n", event->button);
  /* paranoia check, in case we haven't gotten a configure event */
  if (surface == NULL)
    return FALSE;

  if (event->button == 1)
    {
		downx = event->x;
		downy = event->y;
		//printf("down %d,%d\n", downx, downy);
    }
  else if (event->button == 3)
    {

			stackpointer--;
			if (stackpointer<0) stackpointer=0;
			xs = scale_set_world(xs,stack[stackpointer].l,stack[stackpointer].r);
			ys = scale_set_world(ys,stack[stackpointer].b,stack[stackpointer].t);
			redraw_all();
  			gtk_widget_queue_draw_area (da, 0, 0, W, H);
    }

  /* We've handled the event, stop processing */
  return TRUE;
}

static gboolean
button_release_event_cb (GtkWidget      *widget,
               GdkEventButton *event,
               gpointer        data)
{

  /* paranoia check, in case we haven't gotten a configure event */
  if (surface == NULL)
    return FALSE;

printf("Up : button is %d\n", event->button);
  if (event->button == 1)
    {
      clear_surface ();
      //gtk_widget_queue_draw (widget);
			int x=event->x,
			    y=event->y;

			printf("Release on %d,%d\n", x, y);
			//printf("Selected on %d,%d\n", downx, downy);

			printf("new world X would be %d,%d\n", scale_get_world(xs,downx), scale_get_world(xs,x));

			stackpointer++;
			if (x > downx) {
				stack[stackpointer].l = scale_get_world(xs,downx);
				stack[stackpointer].r = scale_get_world(xs,x);
			} else {
				stack[stackpointer].l = scale_get_world(xs,x);
				stack[stackpointer].r = scale_get_world(xs,downx);
			} 
			if (y > downy) {
				stack[stackpointer].t = scale_get_world(ys,y);
				stack[stackpointer].b = scale_get_world(ys,downy);
			} else {
				stack[stackpointer].t = scale_get_world(ys,downy);
				stack[stackpointer].b = scale_get_world(ys,y);
			} 
			xs = scale_set_world(xs,stack[stackpointer].l,stack[stackpointer].r);
			ys = scale_set_world(ys,stack[stackpointer].b,stack[stackpointer].t);
			//redraw();
			redraw_all();
  			gtk_widget_queue_draw_area (da, 0, 0, W, H);
    }

  /* We've handled the event, stop processing */
  return TRUE;
}

/* Handle motion events by continuing to draw if button 1 is
 * still held down. The ::motion-notify signal handler receives
 * a GdkEventMotion struct which contains this information.
 */
static gboolean
motion_notify_event_cb (GtkWidget      *widget,
                GdkEventMotion *event,
                gpointer        data)
{
  int x, y;
  GdkModifierType state=GDK_BUTTON1_MASK;
	GdkDeviceManager *device_manager;
	GdkDevice *pointer;

   /* paranoia check, in case we haven't gotten a configure event */
   if (surface == NULL)
       return FALSE;

	device_manager = gdk_display_get_device_manager (gtk_widget_get_display (widget));
  	pointer = gdk_device_manager_get_client_pointer (device_manager);
    gdk_window_get_device_position (gtk_widget_get_window (widget), pointer, &x, &y, &state);

	//printf("%d and %d\n", x, y);


  //if (state & GDK_BUTTON1_MASK)
    //draw_brush (widget, x, y);

  /* We've handled it, stop processing */
  return TRUE;
}

void toggle_nn(GtkWidget *widget, gpointer window) {
  	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
		dc.nodenames = 1;
  	} else {
		dc.nodenames = 0;
 	}
	redraw_all();
}

#endif

static GLfloat spin = 0.0;

void spinDisplay(void)
{
   spin = spin + 2.0;
   if (spin > 360.0)
      spin = spin - 360.0;
   glutPostRedisplay();
}

void mouse(int button, int state, int x, int y) 
{
   switch (button) {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(spinDisplay);
         break;
      case GLUT_MIDDLE_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(NULL);
         break;
      default:
         break;
   }
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	//ogl_draw_nodes();
	//return;

	//printf("display\n");
/*  draw white polygon (rectangle) with corners at
 *  (0.25, 0.25, 0.0) and (0.75, 0.75, 0.0)  
 */
	//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	glPushMatrix();
    glColor3f (1.0, 0.1, 1.0);
	//glRotatef(spin, 0.5, 0.5, 0.0);
    glRotatef(rotY, W,H,0.0);
    glRotatef(rotX, W,H,0.0);

    glBegin(GL_POINTS);


	ogl_draw_nodes();
        //glVertex3f (0.15, 0.85, 0.0);
        //glVertex3f (0.25, 0.25, 0.0);
        //glVertex3f (0.75, 0.25, 0.0);
        //glVertex3f (0.75, 0.75, 0.0);
        //glVertex3f (0.25, 0.75, 0.0);
    glEnd();
	glPopMatrix();
    glutSwapBuffers();
}

void reshape(int w, int h)
{
   glViewport (0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(0.0, W, 0.0, H, -1.0, 1.0);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

static void Key(unsigned char key, int x, int y)
{

    switch (key) {
      case 27:
    exit(0);
    }
}

static void SpecialKey(int key, int x, int y)
{

    switch (key) {
      case GLUT_KEY_UP:
    rotX -= 2.0;
    glutPostRedisplay();
    break;
      case GLUT_KEY_DOWN:
    rotX += 2.0;
    glutPostRedisplay();
    break;
      case GLUT_KEY_LEFT:
    rotY -= 2.0;
    glutPostRedisplay();
    break;
      case GLUT_KEY_RIGHT:
    rotY += 2.0;
    glutPostRedisplay();
    break;
    }
}

void ogl_draw_init(int argc, char *argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize (850, 850);
	glutInitWindowPosition (100, 100);
	glutCreateWindow ("ogl");
	init ();
	glutDisplayFunc(display);
	//glutReshapeFunc(reshape);
	//glutMouseFunc(mouse);
    glutKeyboardFunc(Key);
    glutSpecialFunc(SpecialKey);

}

void ogl_draw_run()
{
	//redraw_all();
	glutMainLoop();
}

