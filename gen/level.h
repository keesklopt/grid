#include <iostream>
#include <cmath>
#include <cstring>
#include <stdint.h>
#include <stdio.h>

using namespace std;

//#define LEVEL 6
#define TEST_BIT_32(data,pos) (data & (1<<(pos)))
#define TEST_BIT_16(data,pos) (data & (1<<(pos)))

struct Xyz
{
	int32_t x;
	int32_t y;
	int16_t l;

	friend ostream &operator<<(ostream &stream,Xyz &pt)
	{
		cout << pt.l;
		cout << " (";
		cout << pt.x;
		cout << ",";
		cout << pt.y;
		cout << ")";
		return stream;
	}
};

class LevelInfo
{
	public:
	const static int32_t depth = 17; // max with int32_t
	int32_t *tiles;
	int32_t *offsets;
	int32_t *parts;
	int32_t *masks;
	//double  *w;
	//double *h;

	public:
	LevelInfo() {
		int32_t step=0;
		int32_t quad=1;
		int32_t wide=1;
		int32_t mask=3; // 11 binary
		int32_t t;

		tiles = new int32_t[depth]();
		offsets = new int32_t[depth]();
		parts = new int32_t[depth]();
		masks = new int32_t[depth]();
		//w = new double[depth]();
		//h = new double[depth]();
		//w[0] = 40075160.0*2.0; // meters at equator 
		//h[0] = 40008000.0; // meters round the poles
		for (t=0; t< this->depth; t++) {
			//w[t] /= 2.0;
			//h[t] /= 2.0;
			this->tiles[t] = pow(4,t);
			this->offsets[t]=step;
			step += quad;
			this->parts[t]=wide;
			this->masks[t]=mask;
			wide *=2;
			quad *=4;
			mask <<= 2;

			//if (t+1<this->depth) {
				//w[t+1]= w[t];
				//h[t+1]= h[t];
			//}
			//cout << t << " " << offsets[t] << "\n";
		}
	}

	virtual ~LevelInfo() {
		delete []tiles;
		delete []offsets;
		delete []parts;
		delete []masks;
	}

	virtual int32_t xy_2_tile(Xyz xyz) 
	{
		cout << "no generic xy_2_tile available" << endl;
		return 0;
	}

	int32_t calctile(int32_t x, int32_t y, int16_t level)
	{
		Xyz xyz = {x,y,level};
		return xy_2_tile(xyz);
	}

	int level(int32_t tile)
	{
		int16_t l;

		for (l=0; l< this->depth; l++) {
			if (tile < this->offsets[l]) return l-1;
		}
		return l;
	}

	int32_t *children(int32_t tile)
	{
		//int32_t *children = (int32_t *)calloc(4,sizeof(int32_t));
		static int32_t children[4];
		Xyz xyz = this->xyz(tile);

		//cout << xyz << endl;
		int32_t x=xyz.x;
		int32_t y=xyz.y;
		int32_t l=xyz.l;

		if (l==15) return NULL;

		x *= 2;
		y *= 2;
		//cout << tile << " " << x << " " << y << " " << l << " " << endl;
	
		xyz.l = l+1;

		int t=0;
		for (xyz.y=y; xyz.y<y+2; xyz.y++)
			for (xyz.x=x; xyz.x<x+2; xyz.x++)
				children[t++] = xy_2_tile(xyz);

		return children;
	}

	int32_t parentold(int32_t tile)
	{
		Xyz xyz = this->xyz(tile);

		//cout << xyz << endl;
		int32_t x=xyz.x;
		int32_t y=xyz.y;
		int32_t l=xyz.l;

		if (l==0) return -1;

		x /= 2;
		y /= 2;
		//cout << tile << " " << x << " " << y << " " << l << " " << endl;
	
		xyz.x = x;
		xyz.y = y;
		xyz.l = l-1;

		return xy_2_tile(xyz);
	}

	int32_t parent(int32_t tile)
	{
		int l = this->level(tile);
		if (l==0) return -1;
		int rest = tile - offsets[l];
		int p = rest & ~masks[l];
		p >>= 2;
		return p+offsets[l-1];
	}

	int32_t ancestorat(int32_t tile, int level)
	{
		int p=tile;
		int l = this->level(tile);

		while (l > level) {
			p = parent(p);
			l--;
		}
		return p;
	}

	// get all neighbour cells, starting left,up,right,down,left,up
	int32_t *neighbours(int32_t center)
	{
		static int32_t n[8] = {0};
		int x=0;

		//   123
		//   0 4
		//   765

		Xyz xyz = this->xyz(center);
		xyz.x-=1; n[x++] = xy_2_tile(xyz);
		xyz.y+=1; n[x++] = xy_2_tile(xyz);
		xyz.x+=1; n[x++] = xy_2_tile(xyz);
		xyz.x+=1; n[x++] = xy_2_tile(xyz);
		xyz.y-=1; n[x++] = xy_2_tile(xyz);
		xyz.y-=1; n[x++] = xy_2_tile(xyz);
		xyz.x-=1; n[x++] = xy_2_tile(xyz);
		xyz.x-=1; n[x++] = xy_2_tile(xyz);
		return n;
	}

	// get all neighbour cells + self from leftunder to upperright
	int32_t *tictactoe(int32_t center)
	{
		static int32_t n[9] = {0};

		//   678
		//   345
		//   012

		Xyz xyz = this->xyz(center);

		n[4] = center;
		xyz.x-=1; n[3] = xy_2_tile(xyz);
		xyz.y+=1; n[6] = xy_2_tile(xyz);
		xyz.x+=1; n[7] = xy_2_tile(xyz);
		xyz.x+=1; n[8] = xy_2_tile(xyz);
		xyz.y-=1; n[5] = xy_2_tile(xyz);
		xyz.y-=1; n[2] = xy_2_tile(xyz);
		xyz.x-=1; n[1] = xy_2_tile(xyz);
		xyz.x-=1; n[0] = xy_2_tile(xyz);

		return n;
	}

	rect_t boundbox(Xyz xyz)
	{
		// we take pacific and antarctic as 0,0 
		// to keep everything positive numbered
		uint32_t bx=3600000000UL; 
		uint32_t by=1800000000UL; 
		uint32_t tilefx=xyz.x; uint32_t tilefy=xyz.y; 
		uint32_t fax=0; uint32_t fay=0;
		rect_t r={{0,0},{0,0}};
		int mask;
		int level = xyz.l;

		if (xyz.l < 0) return r;

		mask = 1 << (level);
		//printf("mask is %d (level %d)\n", mask, level);

		while (level >0) {
			/* printf("level %d %d %d\n", level, lx, bx); */
			bx/=2;
			by/=2;
			tilefx <<=1;
			tilefy <<=1;
			if (tilefx & mask) {
				fax += bx;
			}
			if (tilefy & mask) {
				fay += by;
			}
			level--;
		}

		// here we restore the 0,0 center again , and the negative coords
		r.ll.x = fax - 1800000000; 
		r.ll.y = fay - 900000000;
		r.ur.x = r.ll.x + bx;
		r.ur.y = r.ll.y + by;

		return r;
	}

	point_t ll_2_xy(uint32_t lat, uint32_t lon, int level)
	{
		point_t pt={0};
		uint32_t x,y;
		uint32_t bx,by; // borders
		int tilex=0; int tiley=0; 
		int ax, ay;
		int t;
	
		// shift all negatives out of view 
		x=lon+1800000000;
		y=lat+900000000;
	
		//printf("analyzing %d,%d to %d,%d\n", fx, fy, tx, ty);
		int l=0;
		bx=3600000000UL; by=1800000000; 
		ax=0; ay=0;
		tilex=0; tiley=0;
	
		while (l< level) {
			bx/=2;
		by/=2;
			l++;
			tilex <<=1;
			tiley <<=1;
			if (x > ax+bx) {
				ax += bx;
				tilex |=1;
			}
			if (y > ay+by) {
				ay += by;
				tiley |=1;
			}
		}
	
		l= level;
		//level -= from.level;
	
		int tilesx = tilex;
		int tilesy = tiley;
		for (t=l; t< level; t++) {
			tilesx >>= 1;
			tilesy >>= 1;
		}
	
		//printf("level %d\n", level);
		//printf("x %d\n", tilesx);
		//printf("y %d\n", tilesy);
		pt.x = tilesx;
		pt.y = tilesy;
		return pt;
	}

	virtual Xyz xyz(int32_t tile) {
		Xyz xyz={0};
		cout << "no generic xyz available" << endl;
		return xyz;
	}

	void dump()
	{
		int32_t t;
		for (t=0; t< this->depth; t++) {
			cout << t << ": " << this->tiles[t] << " " ;
			cout << this->offsets[t] << "-";
			cout << this->offsets[t+1]-1 << " ";
			cout << this->parts[t] << " ";
			cout << this->masks[t] << " ";
			//cout << this->w[t] << " ";
			//cout << this->h[t] << " ";
			cout << "\n";
		}
	}
};

// cut op in squares like  :  a b e f
// (hex)                      8 9 c d
//                            2 3 6 7
//                            0 1 4 5
class LevelSquare : public LevelInfo
{
	public:

	int32_t xy_2_tile(Xyz xyz)
	{
		int level=xyz.l;
		//printf("(%d,%d) on level %d\n", xyz.x, xyz.y, level);
	
		if (xyz.l==0) {
			if (xyz.x==0 && xyz.y==0) return 0; 
			return -1;
		}

		int parts = this->parts[level];
	
		// sanity checks 
		if (xyz.x< 0 || xyz.y< 0) return -1;
		if (xyz.x>=parts || xyz.y>= parts) return -1;

		int32_t tile = offsets[xyz.l];
		int32_t factor=1;
		int32_t subtile;
		int32_t x=xyz.x;
		int32_t y=xyz.y;
	
		//printf("Base is %d\n", tile);
	
		while (level>0) {
			subtile=0;
			subtile = (TEST_BIT_32(y,0)!=0 ? 2 : 0);
			subtile += (TEST_BIT_32(x,0)!=0 ? 1 : 0);
			tile += subtile * factor;
			level--; x>>=1; y>>=1;
			factor *=4;
		}
		//printf("tile is %d\n", tile);
		return tile;
	}

	Xyz xyz(int32_t tile)
	{
		Xyz xyz={0};

		int level = this->level(tile);
		int32_t rest = tile - this->offsets[level];

	//	cout << "Starting at level : " << level << " rest : " << rest << endl;

		xyz.x=0;
		xyz.y=0;
		xyz.l = level;
		while (level > 0) {
			//cout << "Walking level " << level <<endl ;
			int32_t check = this->tiles[level-1];
			//cout << "Using " << check <<endl ;

			if (rest < check) {
				//cout << "⇙" << endl;
			} else 
			if (rest < check*2) {
				//cout << "⇘" << endl;
				xyz.x |= 1;
				rest -= check*1;
			} else 
			if (rest < check*3) {
				//cout << "⇖" << endl;
				xyz.y |= 1;
				rest -= check*2;
			} else 
			if (rest < check*4) {
				//cout << "⇗" << endl;
				xyz.x |= 1;
				xyz.y |= 1;
				rest -= check*3;
			} else {
				xyz.l=-1;
				xyz.x=-1;
				xyz.y=-1;
				return xyz;
			}
			xyz.x <<= 1;
			xyz.y <<= 1;
			level--;
			//cout << xyz << endl;
		}
	 	// shifted one too far
		xyz.x >>= 1;
		xyz.y >>= 1;

		return xyz;
	}

};

extern LevelSquare levelinfo;
