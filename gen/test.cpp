#include <level.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <float.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <ftw.h>
#include <dirent.h>

//#define DEFAULTNETWORK "network"
//#define DEFAULTNETWORK "../dat/network"

LevelSquare *levelinfo = new LevelSquare();
#ifdef USING_MONGO
mongo::DBClientConnection c;
#endif

int nnodes;
gen_t gen;
ref_t *ref;
// a tile cache per level works better
tcache_t *tc[16];
noderef_t *nr;
typedef struct clt_tag
{
	string code;
	double lat;
	double lon;
} clt_t;

clt_t rit1[] = {
 { "ORIG",52.2560000,4.7960000},
 { "09LO",52.2927377,5.2324384},
 { "10ZZ",52.3020494,5.2551977},
 { "12HU",52.3672309,5.2094591},
 { "29XU",52.371353,5.222124},
 { "12CC",52.3138844,5.0347502},
 { "14KM",52.52302,5.487309},
 { "16WP",52.5029964,5.4969436},
 { "12IX",52.4562372,5.6910016},
 { "11OD",52.5207617,5.7263693},
 { "11ZL",52.5119006,5.7230311},
 { "23UT",52.667,5.6039307},
};

/*rit_t din1[] = {
 20OJ,52.2219963,5.9514701
 06ZL,51.9254038,6.6028777
 17MZ,52.2151307,5.9715468
 17FK,52.2317436,5.9952825
 17CJ,52.2239121,6.0033479
 17YJ,52.193795,6.0149098
 17IW,52.1980287,5.9854375
 17TW,52.1980287,5.9854375
 17UB,52.2042246,5.9936398
 17SP,52.201914,5.9710893
 17RQ,52.1919695,5.9510022
 17OM,52.2548012,5.9629144
 05MG,52.1636913,5.9842315
 04UV,52.1226743,6.0211526
 17ZQ0,52.1158193,6.0205881
}
*/

typedef struct td_cache_tag
{
	int a;
	int b;
	int dist;
} td_cache_t;

int ptv()
{
	//char *url = "http://10.2.2.20/";
	return 0;
}

int test_hoek(net_t *np)
{
	int d;
	node_t *from, *to;
	printf("-- location nodes --\n");
    from = net_get_node_by_ll(np, 519779100, 41289500,NULL);
	to = net_get_node_by_ll(np, 514428480, 54663364,NULL);

	d=0;
	printf("tile %d is level %d\n", d, tile_2_level(d));
	d=1;
	printf("tile %d is level %d\n", d, tile_2_level(d));
	d=4;
	printf("tile %d is level %d\n", d, tile_2_level(d));
	d=5;
	printf("tile %d is level %d\n", d, tile_2_level(d));

	//meters = net_shortest(np, from, to);
	//printf("Shortest is %d meters\n", meters);

	//meters = net_minutes(np, to);
	//printf("Shortest is %d seconds\n", meters);
	
	//meters = net_minutes(np, to);
	//printf("Shortest is %d minutes\n", meters);
	return 0;
}

int test_nl(net_t *np)
{
	int meters=0;
	//int d;
	uint32_t t;
	node_t *from, *to;
	//tsp_t *tsp;

#define DISTANCE 100

	printf("-- get matrix --\n");
	grid_t *grd= grid_ist(NULL);
	for (t=0; t< (sizeof(rit1)/sizeof(clt_t)); t++) {
		int d=0;
		node_t *nd = net_get_node_by_ll(np,rit1[t].lat*10000000, rit1[t].lon*10000000, &d);
		if (d> DISTANCE) printf("node more than %d meters away (%d)\n", DISTANCE, d);
		//printf("Node is %d:%d (%d,%d)\n", nd->tile, nd->node, nd->y, nd->x);
		grid_set(grd, t, nd);
	}
	//node_t *origin = net_get_node_by_ll(np,rit1[t].lat*10000000, rit1[t].lon*10000000, &d);

	grid_calc_complete(grd,np);
	grid_dmp(grd);
	grid_write(grd, "gridfile");

	//grd = grid_read(np, "gridfile");
	//grid_dmp(grd);

	//tsp = tsp_ist(origin);

	printf("-- location nodes --\n");
    // hendrikstraat hvh Albert Heyn
    	from = net_get_node_by_ll(np, 519779100, 41289500,NULL);
	// Eindhoven 
	to = net_get_node_by_ll(np, 514428480, 54663364,NULL);

	printf("-- get fastest --\n");
	// force complete dijkstra with node_dontmatch
	meters = net_edsger(np, from, &node_dontmatch);
	printf("Fastest is %d meters\n", meters);
	meters = net_minutes(np, to);
	printf("Fastest is %d minutes\n", (meters*60/1000000) );
	meters = net_meters(np, to);
	printf("Fastest is %d meters\n", meters);

	printf("-- get shortest --\n");
	// force complete dijkstra with node_dontmatch
	meters = net_edsger(np, from, &node_dontmatch);
	printf("Shortest is %d meters\n", meters);
	meters = net_minutes(np, to);
	printf(" %d minutes\n", meters);
	printf("Shortest is %d minutes\n", (meters*60/1000000));
	meters = net_meters(np, to);
	printf("Shortest is %d meters\n", meters);

	
	return 0;
}

int test_test(net_t *np)
{
	int meters=0;
	node_t *from, *to;
	printf("-- location nodes --\n");
	// testnet node 10 
    from = net_get_node_by_ll(np, 510000000, 41000000, NULL);
	// testnet node 60
    to = net_get_node_by_ll(np, 516000000, 40000000, NULL);

	meters = net_edsger(np, from, to);
	printf("Shortest is %d meters\n", meters);

	meters = net_minutes(np, to);
	printf("Shortest is %d minutes\n", meters);
	
	//meters = net_minutes(np, to);
	//printf("Shortest is %d minutes\n", meters);
	return 0;
}

int main()
{
	net_t *np;
	int meters=0;
	node_t *from, *to;
	//path_t *path;

	printf("-- Opening network -- \n");

	np = net_open((char *)DAT_INDIR, (char *)DEFAULTNETWORK);

	if (!np) {
		printf("could not open network %s\n", DEFAULTNETWORK);
		return -1;
	}

	net_statistics(np);

	//net_dump(np);

	//return test_hoek(np);
	//return test_test(np);
	return test_nl(np);

	//from = net_get_node_by_pc(np, 31, "2691");
	//from = net_get_node_by_ll(np, 523746200, 46777800, NULL);
	//to = net_get_node_by_ll(np, 523746200, 46777800, NULL);
    // 2011 SZ  Grote Houtstraat Haarlem
    //from = net_get_node_by_ll(np, 523763069, 46310220, NULL);
    // 2011 PX Kruisstraat Haarlem
    //to = net_get_node_by_ll(np, 523844214, 46356790, NULL);
    // hendrikstraat hvh Albert Heyn
    from = net_get_node_by_ll(np, 519779100, 41289500, NULL);
    // 2011 PX Kruisstraat Haarlem
    //to = net_get_node_by_ll(np, 523844214, 46356790, NULL);

	// Eindhoven 
	to = net_get_node_by_ll(np, 514428480, 54663364, NULL);
    // texel !!!
    to = net_get_node_by_ll(np, 532277100, 6554220, NULL);

	//printf("From node is %d,%d\n", from->x, from->y);
	//printf("To node is %d,%d\n", to->x, to->y);

	//to = net_get_node_by_pc(np, 32, "6990");
	//to = net_get_node_by_pc(np, 49, "80799");
	//to = net_get_node_by_pc(np, );
	//to = net_get_node_bypc(np, 33, "22950");
	//to = net_get_node_bypc(np, 422, "99124");
	//to = net_get_node_bypc(np, 31, "3016");

	// testnet node 10 
    //from = net_get_node_by_ll(np, 510000000, 41000000);
	// testnet node 60
    //from = net_get_node_by_ll(np, 516000000, 40000000);
	// testnet node 80 
    //to = net_get_node_by_ll(np, 517000004, 50000010);
	// testnet node 40
    //to = net_get_node_by_ll(np, 513000000, 44000000);

	// PTV test points (luxembourg)
    //from = net_get_node_by_ll(np, 496067600, 61289400);
    //to = net_get_node_by_ll(np, 496084100, 60538100);

	printf("node found : %d:%d\n", from->tile, from->offset);
	printf("node found : %d:%d\n", to->tile, to->offset);

	//path = net_path(np, from, to, &len);
	//for (t=0; t< len ; t++) {
		//printf("%d:%d %d \n", path[t].n.tile, path[t].n.node, path[t].r.len);
	//}

	meters = net_shortest(np, from, to);
	printf("Shortest is %d meters", meters);
	//meters = net_get_distance(np, to);
	//printf("Shortest is %d meters", meters);
	

	//meters = net_fastest(np, from, to);
	//printf("Thats' %d meters and\n", meters);
	//printf(" %d minutes\n", (meters/1000000) *60);

	//printf(" bird flight : %f\n", CalculateDistance(from->y/10000000.0, from->x/10000000.0, to->y/10000000.0, to->x/10000000.0));

	//printf("country code for %s is %d\n", (char *)"NL", net_country_get_bycarcode(np, "NL"));

	exit(0);

	node_t one, two;
	// one way test :
	//s:5090,357913942:5084,106,0
	//s:5090,357913942:5093,79,2
	//s:5093,357913942:5090,79,1
	one.tile= 357913942;
	one.offset= 5090;
	two.tile= 357913942;
	two.offset= 5093;

	meters = net_shortest(np, &one, &two);
	printf("Thats' %d m\n", meters);
	meters = net_shortest(np, &two, &one);
	printf("Thats' %d m\n", meters);

	return 0;
}
