#include <math.h>
#include <float.h>

typedef struct RDSPoint_tag
{
	double x;
	double y;
} RDSPoint;

typedef struct GPSPoint_tag
{
	double lat;
	double lng;
} GPSPoint;

GPSPoint makeGPSPoint(RDSPoint rdspoint);
