#include <treemap.h>
#include <stdio.h>
#include <stdlib.h>

treemap_t *treemap_ist(int (*cmp)(const void *, const void *))
{
	treemap_t *tm = (treemap_t *)calloc(sizeof(treemap_t),1);
	if (!tm || !cmp) return NULL;

	tm->cmp =cmp;
	tm->tree =NULL;
	tm->n=0;
	tm->array =NULL;

	return tm;
}

void tm_walk(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	treeelm_t *tp = *(treeelm_t **)nodep;

	void (*fnc)(const void *, void *)= (void (*)(const void *, void *))tp->tm->fncptr;

	if (fnc) fnc(tp->data, tp->tm->udata);
}

void tm_free(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	treeelm_t *tp = *(treeelm_t **)nodep;

	//printf("Freeing %p\n", tp->data);
	if (tp->data) free(tp->data);
}

/* release a treemap, and if cleardata != 0 also it's content (with plain free())
	if cleardata == 0 you are expected to free it yourself */
void treemap_rls(treemap_t *tm, int cleardata)
{
	if (!tm) return;
	
	if (cleardata) 
		twalk(tm->tree,tm_free);

	if (tm->tree) tdestroy(tm->tree,free);
	free(tm);
}

void treemap_walk(treemap_t *tm, void (*fnc)(const void *, void *), void *udata)
{
	if (!tm) return;
	
	tm->fncptr = (void *)fnc;
	tm->udata = udata;
	twalk(tm->tree,tm_walk);
}

int treemap_len(treemap_t *tm)
{
	if (!tm || !tm->tree) return 0;
	return tm->n;
}

int tmcmp(const void *a, const void *b)
{
	treeelm_t *A=(treeelm_t *)a;
	treeelm_t *B=(treeelm_t *)b;

	// item B is the one already in the list
	return B->tm->cmp(A->data,B->data);
}

/* returns NULL if not found */
void *treemap_find(treemap_t *tm, void *item)
{
	treeelm_t *tcep=NULL;
	treeelm_t src;

	src.data = item;
	src.tm=tm; // just to be sure treemap is filled correctly in compare function

	tcep = (treeelm_t *)
	tfind(&src,(void * const *)&tm->tree,tmcmp);
	if (!tcep) return NULL;
	return tcep->data;
}

/* insert an item if you are sure it does not exist
	otherwise the item will be returned and the count will be WRONG !! */
void *treemap_insert(treemap_t *tm, void *item)
{
	treeelm_t *tcep=NULL;
	treeelm_t *test=NULL;
	tcep = (treeelm_t *)calloc(1,sizeof(treeelm_t));

	tcep->data = item;
	tcep->tm=tm;

	test = *(treeelm_t **)
	tsearch(tcep,(void **)&tm->tree,tmcmp);
	tm->n++;
	if (test != tcep) free(tcep);
	return test->data;
}

/* add an item if it does not exists, or return the value if it does 
	maintain the correct count */
void *treemap_add(treemap_t *tm, void *item)
{
	treeelm_t *tp = (treeelm_t *)
	treemap_get(tm,item);

	if (tp) return tp->data;

	return treemap_insert(tm,item);
}

void *treemap_get(treemap_t *tm, void *item)
{
	treeelm_t *tcep=NULL;
	treeelm_t src;

	if (!tm) return NULL;

	src.data = item;
	src.tm=tm; // just to be sure tm is filled correctly 

	tcep = (treeelm_t *)
	tfind(&src,(void * const *)&tm->tree,tmcmp);
	if (tcep) {
		tcep = *(treeelm_t **)tcep;
		return (tcep->data);
	} 

	return NULL;
}

void tm_fill(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	treeelm_t *tp = *(treeelm_t **)nodep;
	treemap_t *tm=tp->tm;


	int curitem = tm->arrlen++;
	//printf("Setting %d to %p\n", curitem, tp->data);
	tm->array[curitem] = tp->data;
}

/* allocates !!, clean the data up by just 'free'ing */
void *treemap_array(treemap_t *tm)
{
	if (!tm) return NULL;

	tm->array = (void **)calloc(tm->n,sizeof(void *));
	tm->arrlen=0;

	twalk(tm->tree,tm_fill);
	return tm->array;
}

void treemap_array_rls(treemap_t *tm)
{
	if (!tm) return;
	if (tm->array) free(tm->array);
	tm->array=NULL;
}

void treemap_del(treemap_t *tm, void *item)
{
	// Sorted !
	treeelm_t *tcep=NULL;
	treeelm_t src;

	src.data = item;
	src.tm=tm; // just to be sure tm is filled correctly 

	tcep = *(treeelm_t **) tdelete ((void *) &src, (void **)&tm->tree, tmcmp);
	tm->n--;

	//printf("Deleted --------- %d\n", te->tileno);
	//treemap_dmp(tm);
	//printf("Done --------- %d\n", te->tileno);
}
