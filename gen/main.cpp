#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <signal.h>

#include <gen.h>

//#define DAT_DIR "/media/759e9496-07fe-41a0-bd13-0d9475626269/home/kees/Downloads/planet"

void usage(char *pname)
{
	printf("Usage : %s [-c] [-v] [-r] [-d <datadir>] -i <inputnetwork>\n", pname);
	exit (1);
}

rect_t bbox_au={ { 0, 460000 } , { 97396, 471020 } };
rect_t bbox_uk={ { -180000,0 } , { 40000, 900000 } };
//rect_t bbox_uk={ { -40000,525000 } , { -35000, 530000 } };

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
    printf("Caught segfault at address %p\n", si->si_addr);
    exit(0);
}

/* this will help complete the output of redirection 
 when encountering a sigfault */
void setup_sig(void)
{
	struct sigaction sa;

	memset(&sa, 0, sizeof(struct sigaction));
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = segfault_sigaction;
    sa.sa_flags   = SA_SIGINFO;

	sigaction(SIGSEGV, &sa, NULL);
}

int main(int c, char *v[])
{
	int ret=0;
	int only_count=0;
	int32_t tile=-1;

	// 4 stages :
	int perform_once=0;
	int perform_split=0;
	int perform_dice=0;
	int perform_names=0;
	int perform_fix=0;
	int perform_reduce=0;
	int perform_bypass=0;
	int perform_random=0;
	int perform_hh=1;
	int dump_types=0;

	setup_sig();

	// or 1 run (hi memory usage)
	int one_run=0;
	char *name;

	printf("Network generation tool\n");

	gen.output=OUT_BINARY;
	gen_init();
	gen_set_dir(&gen, (char *)DAT_INDIR, (char *)DAT_OUTDIR);

	ret = getopt(c,v, "gt:cvs:rhni1d:o:b:x:");
	while (ret>=0) {
		switch (ret) {
			case 'v':
				usage(v[0]);
			break;
			case 'c':
				only_count=1;
			break;
			case 'n':
				perform_names=1;
			break;
			case 'g':
				dump_types=1;
			break;
			case 's':
				switch(optarg[0]) {
					case '1': perform_split=1; break;
					case '2': perform_dice=1; break;
					case '3': perform_fix=1; break;
					case '4': perform_reduce=1; break;
					case '5': perform_bypass=1; break;
					//case '4': perform_reduce=1; break;
				} 
			break;
			case '1':
				one_run=1;
			break;
			case 'r':
				gen.output=OUT_DEC;
			break;
			case 'i':
				printf("input base is %s\n", optarg);
				gen_set_ibase(&gen, optarg);
			break;
			case 'o':
				printf("output base is %s\n", optarg);
				gen_set_obase(&gen, optarg);
			break;
			case 'h':
				printf("output base is %s\n", optarg);
				gen_set_obase(&gen, optarg);
			break;
			case 't':
				tile = atol(optarg);
				printf("Doing tile %d\n", tile);
				// only for pass 4 :
				perform_reduce=1;
			break;
			case 'x':
				name = optarg;
				if (!name) name = (char *)"random";
				perform_random=1;
				perform_once=0;
			break;
		}
		ret = getopt(c,v, "gt:cvs:rhni1d:o:b:x:");
	}
	if (!gen.ibase) gen.ibase= (char *)DEFAULTNETWORK;

	if (only_count) { gen_statistics(&gen); return -1; }
	if (perform_once) { gen_read(&gen); }
	if (perform_split) { gen_split(&gen); }
	//if (perform_dice) { gen_dice_splitset2(&gen); }
	//if (perform_fix) { gen_fix(&gen); }
	if (perform_reduce) { gen_reduce(&gen,tile); }
	if (perform_bypass) { gen_bypass(&gen,tile); }
	if (perform_names) { gen_names(&gen); }
	if (perform_hh) { gen_read_hh(&gen); }
	if (perform_random) { gen_random(&gen,name); }
	if (dump_types) { gen_types(&gen); }
	//gen_dice(&gen);
	//gen_random(&gen);

	gen_rls(&gen);

	return 0;
}

