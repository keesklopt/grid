#include <map>

class BoundBox 
{
public:
	int min_x;
	int max_x;
	int min_y;
	int max_y;
};

class DrawNode
{
public:
	int x; 
	int y;
	void *hdl;
	std::string id;

	bool operator< (DrawNode rhs) const
	{
		if (this->y<rhs.y) return false;
		if (this->y>rhs.y) return true;
		if (this->x<rhs.x) return false;
		if (this->x>rhs.x) return true;
		return false;
	} 

	DrawNode()
	{
	}

	DrawNode(int x, int y, void *hdl)
	{
		this->x=x;
		this->y=y;
		this->hdl=hdl;
	} 
};

class DrawEdge
{
public:
	DrawNode a;
	DrawNode b;
	std::string id;
	unsigned int len;
	void *hdl;

	bool operator< (DrawEdge rhs) const
	{
		if (this->a<rhs.a) return false;
		if (rhs.a<this->a) return true;
		if (this->b<rhs.b) return false;
		if (rhs.b<this->b) return true;
		return false;
	} 

	DrawEdge(int x1, int y1,int x2, int y2, int len, void *hdl)
	{
		a = DrawNode(x1,y1,hdl);
		b = DrawNode(x2,y2,hdl);
		this->len=len;
		this->hdl=hdl;
	} 

	DrawEdge(DrawNode a, DrawNode b, int len, void *hdl)
	{
		a = a;
		b = b;
		this->len=len;
		this->hdl=hdl;
	} 

	DrawEdge()
	{
	}

};

class Draw
{
public:
	int type;
	map<DrawNode,DrawNode> nds;
	map<DrawEdge,DrawEdge> eds;

	int AddNode(int x, int y, void *);
	int AddEdge(int x1, int y1, int x2, int y2, unsigned int len, void *hdl);

	void Dump(void)
	{
		printf("node list is %ld long\n", nds.size());
		
/*
		for(unsigned int t=0; t< nds.size(); t++) {
			printf("%d,%d\n", nds[t].x, nds[t].y);
		} 
*/
		for (map<DrawNode,DrawNode>::iterator t = nds.begin(); 
			t != nds.end(); t++) {
			DrawNode dn = (*t).second;
			printf("%d,%d\n", dn.x, dn.y);
		} 
	}

	BoundBox CalcBounds(void)
	{
		BoundBox bb;

		bb.min_x=1800000000;
		bb.max_x=-1800000000;
		bb.min_y=900000000;
		bb.max_y=-900000000;

		for (map<DrawNode,DrawNode>::iterator t = nds.begin(); 
			t != nds.end(); t++) {
			DrawNode dn = (*t).second;
			int x = dn.x;
			int y = dn.y;
			if (y<bb.min_y)bb.min_y=y;
			if (y>bb.max_y)bb.max_y=y;
			if (x<bb.min_x)bb.min_x=x;
			if (x>bb.max_x)bb.max_x=x;
		} 
		//printf("%d,%d,%d,%d\n", bb.min_x, bb.min_y, bb.max_x, bb.max_y);
		return bb;
	}
};

class GtkDraw : public Draw
{
public:
	void Init()
	{
		gtk_draw_init(0,NULL);
		BoundBox bb = CalcBounds();
		gtk_draw_init_stack(bb.min_x, bb.min_y, bb.max_x, bb.max_y);
	} 

	void Fill(void)
	{ 
		for (map<DrawNode,DrawNode>::iterator t = nds.begin(); 
			t != nds.end(); t++) {
			DrawNode dn = (*t).second;
			int x = dn.x;
			int y = dn.y;
			int clr;
			node_t *nd = (node_t *) dn.hdl;
			// okay, red is 1, black is zore let's treat red as 'on'
#define BLACK 0
#define RED 1
#define GREEN 2
#define BLUE 3
#define WHITE 4
			if (nd->active==1) clr = RED;
			else { 
				clr=BLACK;
			} 

			draw_node_t c = {0, NULL, x, y, clr};
			asprintf(&c.name, "%d:%d", nd->tile->num, nd->offset);
			gtk_draw_node(c);
		} 
	
		for (map<DrawEdge,DrawEdge>::iterator t = eds.begin(); 
			t != eds.end(); t++) {
			DrawEdge de = (*t).second;
			int clr;
			draw_node_t a = {0, NULL, de.a.x, de.a.y };
			draw_node_t b = {0, NULL, de.b.x, de.b.y };
			road_t *rd = (road_t *) de.hdl;
			if (rd->rise==1) clr = GREEN;
			else { 
				clr=BLUE;
			} 
			draw_edge_t c = {a, b, de.len,1,1,1, NULL, clr} ;
			gtk_draw_edge(c);
		} 
	
	} 

	void Run()
	{
		gtk_draw_run();
	} 
};

