#include <gen.h>
#include <level.h>
#include <fcntl.h>

#define ARRAY_VERSION 1

#ifdef USING_MONGO
extern mongo::DBClientConnection c;
#endif

splitref_t *splitref_ist(gen_t * gp)
{
	char fname[100];
	splitref_t *ni = (splitref_t *)calloc(sizeof(splitref_t),1);
	if (!ni) return NULL;

	ni->gen=gp;
#ifdef TREE_VERSION
	ni->tree = treemap_ist(splitrefelm_cmp);
#elif ARRAY_VERSION
	ni->array = NULL;
#else
	// how to F' ing do this ?
	c.remove("grid.index", mongo::BSONObj( ));
#endif
	ni->nelm=0;
	sprintf(fname, "%s/%s/network.nix",ni->gen->outdir,ni->gen->ibase);
	ni->fp = fopen(fname,"w");

	return ni;
}

void splitref_rls(splitref_t *ni)
{
	if (!ni) return;
	if (ni->fp) fclose(ni->fp);
#ifdef TREE_VERSION
	if (ni->tree) free(ni->tree);
#elif ARRAY_VERSION
	if (ni->array) free(ni->array);
#else
#endif

	// no sorting yet, since all nodes are read before the roads !
	// so we can sort after reading all nodes
	
	free(ni);
}

int splitref_put(splitref_t *ni, node_t node,int tile)
{
#ifdef ARRAY_VERSION
	int n = ni->nelm;
#endif

	ni->nelm++;


#ifdef TREE_VERSION
	splitrefelm_t *nep;
	nep = splitref_get(ni,node.node);
	if (!nep) 
		nep = (splitrefelm_t *)calloc(sizeof(splitrefelm_t),1);

	nep->id=node.node;
	nep->tile = tile;
	nep = (splitrefelm_t *)treemap_add(ni->tree, (void *)nep);
	//printf("Putting %d %d %d\n", tile, ne.x,ne.y);
#elif ARRAY_VERSION
#define RSIZE 1000000
	splitrefelm_t ne;

	ne.id=node.id;
	ne.tile=tile;
	if ((n % RSIZE) == 0) {
		//printf("New chunck\n");
		if (ni->array) 
			ni->array=(splitrefelm_t *)realloc(ni->array, sizeof(splitrefelm_t)*(ni->nelm+RSIZE));
		else
			ni->array=(splitrefelm_t *)calloc(sizeof(splitrefelm_t),ni->nelm+RSIZE);
	}

	ni->array[n] = ne;
#else
	mongo::BSONObjBuilder b;
	b.append("node", node.node);
	b.append("tile", tile);
	mongo::BSONObj p = b.obj();
	c.insert("grid.index",p);
	c.ensureIndex("grid.index", mongo::fromjson("{node:1}"));
#endif

	//splitref_dump(ni,0);
	return 0;
}

void splitref_sort(splitref_t *sr)
{
#ifdef ARRAY_VERSION
	qsort(sr->array,sr->nelm,sizeof(splitrefelm_t),splitrefelm_cmp);
#endif
}

int splitrefelm_cmp(const void *a, const void *b)
{
	splitrefelm_t *A=(splitrefelm_t *)a;
	splitrefelm_t *B=(splitrefelm_t *)b;

	if (A->id > B->id) return -1;
	if (A->id < B->id) return  1;
	return 0;
}

splitrefelm_t *splitref_get(splitref_t *ni, int32_t id)
{
	splitrefelm_t ne, *nep;

	ne.id=id;

#ifdef TREE_VERSION
	nep = (splitrefelm_t *)treemap_get(ni->tree,(void *)&ne);
#elif ARRAY_VERSION
	ne.id=id;
	nep = (splitrefelm_t *)bsearch(&ne,ni->array,ni->nelm,sizeof(splitrefelm_t),splitrefelm_cmp);
#else
	auto_ptr<mongo::DBClientCursor> cursor = 
	c.query("grid.index", QUERY( "node" << id));
	mongo::BSONObj p;
	while (cursor->more()) {
		p = cursor->next();
	}
	mongo::BSONElement be;
	be= p.getField("node");
	ne.id=be.numberInt();
	be= p.getField("tile");
	ne.tile=be.numberInt();
	//printf("Das : %d:%d\n", ne.id,ne.tile);
	return &ne;
#endif
	return nep;
}

int splitref_gettile(splitref_t *ni, int32_t id)
{
	splitrefelm_t *nep;
	nep = splitref_get(ni,id);
	if (!nep) return -1;
	return nep->tile;
}

int splitref_write(int i, splitref_t *ni, vrt_t vrt,int tile)
{
	//write_comment(i, ni->fp, "idx: ");
	io.write_uint32(OUT_DEC, ni->fp, vrt.id);
	io.write_comment(OUT_DEC, ni->fp, ",");
	//write_uint32(i, ni->fp, vrt.tile);
	//write_comment(i, ni->fp, ",");
	io.write_uint32(OUT_DEC, ni->fp, tile);
	io.write_comment(OUT_DEC, ni->fp, "\n");

	return 0;
}
