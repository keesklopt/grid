#include <gen.h>
#include <level.h>
#include <stdio.h>
#include <stdlib.h>

/* 
	node reference for general renumbering purposes, simply holds :

	prevtile:prevnode
	newtile:newnode

	indexed on prevtile:prevnode 
*/
noderef_t *noderef_ist()
{
	noderef_t *cp = (noderef_t *)calloc(sizeof(noderef_t),1);
	if (!cp) return NULL;

	cp->elms=NULL;

	return cp;
}

void noderef_elm_rls(const void *resultp, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	noderefelm_t *cep = *(noderefelm_t **)resultp;
	free(cep);
}

void noderef_walk(noderef_t *cp, void (*fnc)(const void *,const VISIT,const int))
{
	if (!cp) return;
	twalk(cp->elms,fnc);
}

void noderef_free(void *data)
{
	free(data);
}

void noderef_rls(noderef_t *cp)
{
	if (!cp) return;

	//twalk(cp->elms,noderef_elm_rls);

	if (cp->elms) tdestroy(cp->elms,noderef_free);
	free(cp);
}

int resultelm_cmp(const void *a, const void *b)
{
	const noderefelm_t *A=(const noderefelm_t *)a;
	const noderefelm_t *B=(const noderefelm_t *)b;

	//printf("Testing %d and %d\n", A->tileno, B->tileno);
	if (A->prevtile < B->prevtile) return -1;
	if (A->prevtile > B->prevtile) return  1;
	if (A->prevnode < B->prevnode) return -1;
	if (A->prevnode > B->prevnode) return  1;
	return 0;
}

int noderef_len(noderef_t *cp)
{
	if (!cp || !cp->elms) return 0;
	return cp->nelm;
}

noderefelm_t *noderef_find(noderef_t *cp, int prevtile, int prevnode)
{
	noderefelm_t tce={0};
	noderefelm_t *tcep=NULL;
	noderefelm_t *thelper;

	tce.prevtile=prevtile;
	tce.prevnode=prevnode;
	thelper = &tce;

	tcep = (noderefelm_t *)
	tfind(thelper,(void *const*)&cp->elms,resultelm_cmp);
	return tcep;
}

noderefelm_t *noderef_get(noderef_t *cp, int prevtile, int prevnode)
{
	noderefelm_t tce={0};
	noderefelm_t *tcep;
	noderefelm_t *thelper;

	tce.prevtile=prevtile;
	tce.prevnode=prevnode;
	thelper = &tce;
	tcep = (noderefelm_t *)tfind((void *)thelper,(void * const *)&cp->elms,resultelm_cmp);

	if (tcep) {
		return (*(noderefelm_t **)tcep);
	} 

	return NULL;
}

void node_ref_dmp(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	noderefelm_t *tp = *(noderefelm_t **)nodep;
	printf("nr %d:%d has become %d:%d \n", tp->prevtile, tp->prevnode, tp->tile, tp->node);
	//wlk_tile_dmp(0, tp->node,NULL);
}

void noderef_dmp(noderef_t *cp)
{
	printf("--------- noderef --------\n");
	twalk(cp->elms,node_ref_dmp);
}

void noderef_del(noderef_t *cp, noderefelm_t *te)
{
	// Sorted !
	tdelete ((void *) te, &cp->elms, resultelm_cmp);
	cp->nelm--;

	//printf("Deleted --------- %d\n", te->tileno);
	//noderef_dmp(cp);
	//printf("Done --------- %d\n", te->tileno);
}

void noderef_set(noderef_t *cp, int prevtile, int prevnode, int tile, int node)
{
	// Sorted !
	noderefelm_t *tcep;

	tcep = noderef_get(cp,prevtile,prevnode);

	if (!tcep) 
		tcep = (noderefelm_t *)calloc(sizeof(noderefelm_t),1);

	//printf("Adding %d:%d to noderef\n", prevtile,prevnode);
	tcep->prevtile = prevtile;
	tcep->prevnode = prevnode;
	tcep->tile = tile;
	tcep->node = node;
	//tcep->cost = cost;
	tcep = *(noderefelm_t **)tsearch((void *)tcep,&cp->elms,resultelm_cmp);
	cp->nelm++;

	//printf("Added --------- %d\n", tcep->cost);
	//noderef_dmp(cp);
	//printf("Done --------- %d\n", tcep->cost);
}
