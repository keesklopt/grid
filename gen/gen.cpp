#include <gen.h>
#include <draw.h>
#include <level.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <float.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <ftw.h>
#include <dirent.h>

#include <map>

#include <expat.h>
#include <map>

#include <osmpbf/osmpbf.h>
#include <iostream>
#include <zlib.h>

#define COUNTLIMIT 10000

using namespace std;

GtkDraw d;

//LevelSquare levelinfo = new LevelSquare();
Haversine haversine;
IO io;
LevelSquare levelinfo;

class KeyVal {
public:
	string key;
	string val;

	KeyVal(string k, string v)
	{
		key=k;
		val=v;
	}
};

int nodecmp(const void *a, const void *b)
{
	nodeinfo *A=(nodeinfo *)a;
	nodeinfo *B=(nodeinfo *)b;

	if (A->id < B->id) return -1;
	if (A->id > B->id) return 1;
	return 0;
}

#ifdef USING_MONGO
mongo::DBClientConnection c;
#endif

// in dm/ms, a single segment can hold an int32_t, that's 
// 2147483647 / (3600 * 1000) = 596 hours, this must be enough to cross the world a 
// couple of times over
/*
float speeds[16] = { 
90/360.0, //#define AS_MOTORWAY     	0
75/360.0, //#define AS_MOTORWAY_LINK  1
85/360.0, //#define AS_TRUNK			2
70/360.0, //#define AS_TRUNK_LINK		3
65/360.0, //#define AS_PRIMARY      	4
60/360.0, //#define AS_PRIMARY_LINK  	5
55/360.0, //#define AS_SECONDARY    	6
50/360.0, //#define AS_SECONDARY_LINK	7
40/360.0, //#define AS_TERTIARY       8
30/360.0, //#define AS_TERTIARY_LINK  9
25/360.0, //#define AS_UNCLASSIFIED   10 
25/360.0, //#define AS_RESIDENTIAL    11
10/360.0, //#define AS_LIVINGSTREET 	12
15/360.0, //#define AS_SERVICE	 	13
5/360.0,  //#define AS_FERRY        	14
50/360.0  //#define AS_DEFAULT		15
};
*/

int speeds[16] = { 
90, //#define AS_MOTORWAY     	0
75, //#define AS_MOTORWAY_LINK  1
85, //#define AS_TRUNK			2
70, //#define AS_TRUNK_LINK		3
65, //#define AS_PRIMARY      	4
60, //#define AS_PRIMARY_LINK  	5
55, //#define AS_SECONDARY    	6
50, //#define AS_SECONDARY_LINK	7
40, //#define AS_TERTIARY       8
30, //#define AS_TERTIARY_LINK  9
25, //#define AS_UNCLASSIFIED   10 
25, //#define AS_RESIDENTIAL    11
10, //#define AS_LIVINGSTREET 	12
15, //#define AS_SERVICE	 	13
5,  //#define AS_FERRY        	14
50  //#define AS_DEFAULT		15
};


// choose yer tiles here 
void selective_dump(tile_t *tp)
{
	if (TESTTILES(tp->num)) 
		tile_dmp(tp,1);
}

int nnodes;
gen_t gen;
ref_t *ref;
// a tile cache per level works better
tcache_t *tc[16];
noderef_t *nr;
//noderef_t *reverse;

// 0 is one tile only: how deep do we divide ?
// 15 is maximum level, can't fit level 16 in a uint32_t
#define LEVEL 15
#define ISLE_LIMIT 1000
	
//#define JUNCTIONS 1
//#define ONLY_JUNCTIONS 1
#define INTERMEDIATES 1

enum enforce { UNKNOWN=0, YES, NO };

enum handling {HANDLING_NONE=0, HANDLING_NODES, HANDLING_ROADS, HANDLING_BOUNDS};

static char buffer[OSMPBF::max_uncompressed_blob_size];
char unpack_buffer[OSMPBF::max_uncompressed_blob_size];
OSMPBF::BlobHeader blobheader; // pbf struct of a BlobHeader
OSMPBF::Blob blob; // pbf struct of a Blob
OSMPBF::HeaderBlock headerblock; // pbf struct of an OSM HeaderBlock
OSMPBF::PrimitiveBlock primblock; // pbf struct of an OSM PrimitiveBlock

//char *formatcomment = "tile tructure : \nroads: <number of roadsn this tile>\n"
	//"s:<offset of from node (this tile of course)>,<to tile>:<to node offset>,<length>\n"
	//"s: ... road 2 ... etc sorted on from node, then to node\n"
	//"nodes:<number of nodes in this tile>\n"
	//"v:<x coordinate>,<y cordinate>,<number of outgoing roads>\n"
	//"v: ... node 2 ... etc sorted on y, then x\n";
	
typedef struct roadtype_tag {
	char *name;
	int  type;
} roadtype_t;

// -1 undrivable so skipped altogether
// nl motorcar settings 
// 7 motorway max 120 (100)
// 6 main trunk max 100 (90)
// 5 primary roads max 80 (70)
// 4 secondary roads max 80 (60)
// 3 local roads max 50 (40)
// 2 livin streets max 15 (10)
// 1 .. 
// 0 .. 
// 0-7 would mean 3 bits
// nope, take 16 bits and use this division :

#define AS_MOTORWAY     	0
#define AS_MOTORWAY_LINK    1
#define AS_TRUNK			2
#define AS_TRUNK_LINK		3
#define AS_PRIMARY      	4
#define AS_PRIMARY_LINK   	5
#define AS_SECONDARY    	6
#define AS_SECONDARY_LINK  	7
#define AS_TERTIARY         8
#define AS_TERTIARY_LINK    9
#define AS_UNCLASSIFIED     10
#define AS_RESIDENTIAL      11
#define AS_LIVINGSTREET 	12
#define AS_SERVICE          13
#define AS_FERRY        	14
#define AS_DEFAULT			15

roadtype_t types[] = {
{ (char *)"motorway", AS_MOTORWAY },

{ (char *)"trunk", AS_TRUNK },

{ (char *)"motorway_junction", AS_SECONDARY },
{ (char *)"motorway_link", AS_SECONDARY },

{ (char *)"primary", AS_PRIMARY },

{ (char *)"secondary", AS_SECONDARY },

{ (char *)"secondary_junction", AS_SECONDARY_LINK },
{ (char *)"secondary_link", AS_SECONDARY_LINK },
{ (char *)"primary_junction", AS_PRIMARY_LINK },
{ (char *)"primary_link", AS_PRIMARY_LINK },
{ (char *)"road", AS_UNCLASSIFIED },
{ (char *)"ford", AS_UNCLASSIFIED },  // crosses some water, gets tires wet 
{ (char *)"passing_place", AS_UNCLASSIFIED }, // isles on roads to small to for two-way
{ (char *)"trunk_link", AS_TRUNK_LINK },
{ (char *)"unclassified", AS_UNCLASSIFIED }, // somewhere in the middle
{ (char *)"residential;unclassified", AS_UNCLASSIFIED }, // somewhere in the middle
{ (char *)"tertiary", AS_TERTIARY },
{ (char *)"tertiary_link", AS_TERTIARY_LINK },

{ (char *)"residential", AS_RESIDENTIAL },
{ (char *)"roundabout", AS_UNCLASSIFIED },
{ (char *)"living_street", AS_LIVINGSTREET },
{ (char *)"mini_roundabout", AS_UNCLASSIFIED },

// some accessible but slow types 
{ (char *)"services", AS_UNCLASSIFIED },
{ (char *)"parking", AS_UNCLASSIFIED },
{ (char *)"service", AS_UNCLASSIFIED },
{ (char *)"driveway", AS_UNCLASSIFIED },
{ (char *)"rest_area", AS_UNCLASSIFIED },
{ (char *)"turning_circle", AS_UNCLASSIFIED },
{ (char *)"access_ramp", AS_UNCLASSIFIED },
{ (char *)"minor", AS_UNCLASSIFIED },
{ (char *)"local", AS_UNCLASSIFIED },
{ (char *)"lierdererf", AS_UNCLASSIFIED },
{ (char *)"strack", AS_UNCLASSIFIED },

// marked as unaccessible
{ (char *)"byway", -1 },
{ (char *)"track;unclassified", -1 },
{ (char *)"quay", -1 },
{ (char *)"trail", -1 },
{ (char *)"dog track", -1 },
{ (char *)"virtual", -1 },
{ (char *)"no_track_anymore", -1 },
{ (char *)"private", -1 },
{ (char *)"cycleway;footway", -1 },
{ (char *)"bicycle", -1 },
{ (char *)"undefined", -1 },
{ (char *)"tram_stop", -1 },

{ (char *)"traffic_island", -1 },
{ (char *)"abandoned", -1 },
{ (char *)"forbidden_passage", -1 },
{ (char *)"emergency_bay", -1 },
{ (char *)"noway", -1 },
{ (char *)"dog tracks", -1 },
{ (char *)"raceway", -1 },
{ (char *)"race_track", -1 },
{ (char *)"racewayroads", -1 },
{ (char *)"privateroads", -1 },
{ (char *)"disusedroads", -1 },
{ (char *)"disused", -1 },
{ (char *)"unpaved", -1 },
{ (char *)"unsurfaced", -1 },
{ (char *)"gate", -1 },
{ (char *)"give_way", -1 },
{ (char *)"proposed", -1 }, // planned 
{ (char *)"planned", -1 }, // planned 
{ (char *)"path", -1 },
{ (char *)"path;track", -1 },
{ (char *)"pedestrian", -1 },
{ (char *)"platform", -1 }, // bus platform for passengers
{ (char *)"steps", -1 },
{ (char *)"track", -1 },
{ (char *)"bridleway", -1 },
{ (char *)"bus_guideway", -1 },
{ (char *)"bus_sluice", -1 },
{ (char *)"bus_stop", -1 },
{ (char *)"construction", -1 },
{ (char *)"crossing", -1 },
{ (char *)"cy", -1 },
{ (char *)"cycleway", -1 },
{ (char *)"ditch", -1 },
{ (char *)"stream", -1 },
{ (char *)"closed", -1 },
{ (char *)"no", -1 },
{ (char *)"access", -1 },
{ (char *)"traffic_signals", -1 },
{ (char *)"cycleway;footpath", -1 },
{ (char *)"unclassified;cycleway", -1 },
{ (char *)"elevator", -1 },
{ (char *)"emergency_signals", -1 },
{ (char *)"fo", -1 },
{ (char *)"footway", -1 },
};

#define NUMTYPES sizeof(types)/sizeof(roadtype_t)

int vrt_id_cmp(const void *a, const void *b)
{
	const vrt_t *A=(const vrt_t *)a;
	const vrt_t *B=(const vrt_t *)b;

	// y, then x
	if (A->id < B->id) return -1;
	if (A->id > B->id) return  1;
	return 0;
}

int vrt_xy_cmp(const void *a, const void *b)
{
	const vrt_t *A=(const vrt_t *)a;
	const vrt_t *B=(const vrt_t *)b;

	// y, then x
	if (A->y < B->y) return -1;
	if (A->y > B->y) return  1;
	if (A->x < B->x) return -1;
	if (A->x > B->x) return  1;
	return 0;
}

int vrt_tile_cmp(const void *a, const void *b)
{
	const vrt_t *A=(const vrt_t *)a;
	const vrt_t *B=(const vrt_t *)b;

	if (A->tile < B->tile) return -1;
	if (A->tile > B->tile) return  1;
	// y, then x
	if (A->y < B->y) return -1;
	if (A->y > B->y) return  1;
	if (A->x < B->x) return -1;
	if (A->x > B->x) return  1;
	return 0;
}

int vrt_cmp(const void *a, const void *b)
{
	const vrt_t *A=(const vrt_t *)a;
	const vrt_t *B=(const vrt_t *)b;

	// y, then x
	if (A->y < B->y) return -1;
	if (A->y > B->y) return  1;
	if (A->x < B->x) return -1;
	if (A->x > B->x) return  1;
	return 0;
}

int vrt_idx_cmp_noisy(const void *a, const void *b)
{
	const vrt_idx_t *A=(const vrt_idx_t *)a;
	const vrt_idx_t *B=(const vrt_idx_t *)b;

	printf("%d gainst %d\n", A->id, B->id);
	if (A->id < B->id) return -1;
	if (A->id > B->id) return  1;
	return 0;
}

int vrt_idx_cmp(const void *a, const void *b)
{
	const vrt_idx_t *A=(const vrt_idx_t *)a;
	const vrt_idx_t *B=(const vrt_idx_t *)b;

	if (A->id < B->id) return -1;
	if (A->id > B->id) return  1;
	return 0;
}

int rtype_cmp(const void *a, const void *b)
{
	roadtype_t *A = (roadtype_t *)a;
	roadtype_t *B = (roadtype_t *)b;

	//printf("%s against %s\n", A->name, B->name);
	return strcmp(A->name, B->name);
}

void init_highways()
{
	//printf("numtyps %d\n", NUMTYPES);
	// in order for bsearch to find names :
	qsort(types,NUMTYPES,sizeof(roadtype_t),rtype_cmp);
}

road_t road_fill(road_t rd,std::string k, std::string v)
{
	// exclude attributes only 
	if (k == "oneway") { 
		//printf("oneway %s %d\n", v.c_str(), v.compare("no"));
		if (!v.compare("-1") || !v.compare("no; -1")) 
			rd.dir = DIR_TOFROM;
		else if (!v.compare("no") || !v.compare("undefined") || !v.compare("fix"))  // thank you butthead
			rd.dir = DIR_BOTH;
		else if (!v.compare("yes") || !v.compare("true") || !v.compare("1"))  // thank you butthead
			rd.dir = DIR_FROMTO;
		else {
			//printf("Handle oneway %s\n", v.c_str());
			rd.dir = DIR_FROMTO;
		}
		//printf("dir is %d\n", rd.dir);
	}
	if (k == "route") { 
		if (v == "ferry") {
			//printf("Handle route %s\n", v.c_str());
		} 
	}
	if (k == "highway") { 
		int8_t type = classify_highway(v.c_str());

		//printf("type of %s is %d\n", v.c_str(), rd.type);
		if (type >= 0) {
			rd.keep=1;
			rd.type = (uint32_t) type;
		} else
			rd.keep=0;
		//printf("keep is %d\n", rd.keep);
	}
	return rd;
}

int node_keyfail(std::string k, std::string v)
{
	// exclude attributes only 
	if (k == "barrier") { 
		//printf ("handle %s %s\n", k.c_str(),v.c_str());
	}
	return 0;
}

int8_t classify_highway(const char *name)
{
	roadtype_t r;
	roadtype_t *rp;

	r.name=(char *)name;

	rp = (roadtype_t *)bsearch(&r,types,NUMTYPES,sizeof(roadtype_t),rtype_cmp);
	if (rp) {
		//printf("Found type %s with %d\n", rp->name, rp->type);
	} else {
		printf("\nFound no %s take it as undrivable\n", name);
		return -1;
	}

	return rp->type;
}

#define STAT_NONE 0
#define STAT_BUSY 1
#define TEST_WITHIN(V,BB) \
	((V)->x > (BB).ll.x && \
	 (V)->x < (BB).ur.x && 	\
	 (V)->y > (BB).ll.y && \
	 (V)->y < (BB).ur.y)

int gen_test_inpoly(int32_t nvert, float *vertx, float *verty, float testx, float testy)
{
  int32_t i, j, c = 0;
  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>testy) != (verty[j]>testy)) &&
	 (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       c = !c;
  }
  return c;
}

void gen_set_output(gen_t *g, int val)
{
	g->output = val;
}

void gen_set_ibase(gen_t *g, char *base)
{
	g->ibase = strdup(base);
}

void gen_set_obase(gen_t *g, char *base)
{
	g->obase = strdup(base);
}

void gen_set_dir(gen_t *g, char *indir, char *outdir)
{
	g->indir = strdup(indir);
	g->outdir = strdup(outdir);
}

void gen_rls(gen_t *g)
{
	if (!g) return;
	if (g->indir) free(g->indir);
	if (g->outdir) free(g->outdir);
	if (g->pbfp) fclose(g->pbfp);
	if (g->osmp) fclose(g->osmp);
	if (g->tr) tileref_rls(g->tr);
	//if (g->dir) free(g->dir);

	for (int l=0; l< 16; l++) {
		if (tc[l]) 
			tcache_rls(tc[l]);
	}
	// valgrind will like this
  	google::protobuf::ShutdownProtobufLibrary();
}

/*
void tile_fix_roads(tile_t*tp)
{
	int t;

	//printf("Fixing %d\n", tp->num);

	//tile_dmp(tp,1);
	for (t=0; t< tp->nroads; t++) 
	{
		road_t *road = tp->rds[t];
		//printf("Done %d len %d\n", road->done, road->len);
		if (road->done) continue;
		bigrefelm_t *n1 = bigref_get(gen.br,road->nodefrom->offset);
		bigrefelm_t *n2 = bigref_get(gen.br,road->nodeto->offset);
		
		if (n1 && n2) {
			// common attributes
			double x1 = (double)n1->x / 10000000.0;
			double y1 = (double)n1->y / 10000000.0;
			double x2 = (double)n2->x / 10000000.0;
			double y2 = (double)n2->y / 10000000.0;
			//printf("Trying %d (%d,%d)\n", n1->id,n1->x,n1->y);
			double len  = haversine.distance(y1,x1,y2,x2);
			road->len  = (int) ((len*10.0)+0.5); // meters to decimeters
			if (road->len==0) road->len=1; // minimum cost
			//road->done=n1->done && n2->done; // both refs and length found !!
			//printf("Set 4 for %d %d\n", road->toid, road->done);
			if (len > (double) ROAD_MAX) {
				printf("Segment bigger than %d dm : %f\n",ROAD_MAX, len);
				exit(-1); // FIX !!
			}
			//printf("set len to %d %f\n", road->len, len);
		} else if (n2) {
			// TODO : length is missing here !!!
			//printf("Set 1 %d\n", road->nodeto);
			road->done=1;
		} else {
			//printf("Missing nodes %p (%d) or %p (%d)\n", n1, road->from, n2, road->nodeto);
		}

	 	// enable the linking nodes
		if (n1) n1->keep= 1;
		if (n2) n2->keep= 1;
	}
}
*/

#define WORKBUF 1024

void dmp_peers(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	tile_dmp_peers(tp->tile);
}

void reverse_peers(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	tile_reverse_peers(tp->tile);
}

void set_peers(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	tile_set_peers(tp->tile);
}

void null_peers(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	tile_null_peers(tp->tile);
}

void link_tiles(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	tile_link(tp->tile);
}

int removetiles1(const char *name, const struct stat *st, int flag)
{
	// a small safety test ;) 
	if (strstr(name,"tile_") != NULL ||
	    strstr(name,"node_") != NULL ||
		strstr(name,"road_") != NULL) {
		unlink(name);
		//printf("Deleting %s\r", name);
		//fflush(stdout);
	} else 
		printf("Not deleting %s\n", name);
	return 0;
}

int removetiles3(const char *name, const struct stat *st, int flag)
{
	int t;
	// to keep level 16 intact 
	for (t=0; t< 16; t++) {
		char teststring[1000];
		snprintf(teststring, 1000, "grid_%d_", t);
		
		if (strstr(name,teststring) != NULL) {
			unlink(name);
			//printf("Deleting %s\r", name);
			//fflush(stdout);
		}
	}
	return 0;
}

int gen_scandir(gen_t *g,int cleanup)
{
	char fname[WORKBUF];
	g->osmp=g->pbfp=NULL;

	snprintf(fname,WORKBUF,"%s/%s", g->outdir, g->ibase);
	mkdir(fname, S_IRWXU);

	if (cleanup==1) 
		ftw(fname, removetiles1, FTW_D);
	if (cleanup==2) 
		ftw(fname, removetiles1, FTW_D);
	if (cleanup==3) 
		ftw(fname, removetiles3, FTW_D);

	// first try pbf file 
	snprintf(fname, WORKBUF, "%s/%s.osm.pbf", g->indir, g->ibase);
	g->pbfp = fopen(fname, "r");
	if (g->pbfp) return 0;

	// try xml file 
	snprintf(fname, WORKBUF, "%s/%s.xml", g->indir, g->ibase);
	g->xmlp = fopen(fname, "r");
	if (g->xmlp) return 0;

	// then try osm file 
	snprintf(fname,WORKBUF,"%s/%s.osm", g->indir, g->ibase);
	g->osmp = fopen(fname, "r");
	if (!g->osmp) {
		printf("This is not an openstreetmap network (no %s)\n", fname);
	} else  
		return 0;

	
	return 0;
}

#define MAXLINE 256

void fp_dmp(fileparam_t fp)
{
	printf("%d characters, %d lines, max line is %d\n", fp.chars, fp.lines, fp.max_line);
}

typedef struct xml_help_bound_tag 
{
} xml_help_bound_t;

typedef struct xml_help_node_tag
{
	vrt_t vrt;
	int8_t closed;
} xml_help_node_t;

typedef struct osm_help_road_tag
{
	way_t way;
	int8_t dontuse;
	int8_t motorcar; 
} osm_help_road_t;

typedef struct xml_help_road_tag
{
	int32_t from;
	int32_t to;
	int32_t len;
} xml_help_road_t;

typedef struct xml_help_tag
{
	gen_t *gp;
	tcache_t *tc;
	bigref_t *br;
	int current;
	xml_help_node_t xn;
	osm_help_road_t xw;
	xml_help_road_t xr;
	xml_help_bound_t xb;
} xml_help_t;

void (*xml_handle_node)(xml_help_t *);
void (*xml_handle_road)(xml_help_t *);

static void handle_road(xml_help_t *xp)
{
	linkrefelm_t *n1=NULL;
	linkrefelm_t *n2=NULL;
	bareroad_t road= {0};

	n1 = linkref_get(gen.lr, -1, xp->xr.from);
	n2 = linkref_get(gen.lr, -1, xp->xr.to);

	tile_t *tp = n1->nd->tile;
	road.from= n1->id;
	road.tileto = n2->nd->tile->num;
	road.nodeto = n2->id;
	tile_add_bareroad(&gen,tp,road,xp->xr.len);
	road.from = n2->id;
	road.tileto = n1->nd->tile->num;
	road.nodeto = n1->id;
	// reverse if needed 
	//if (road.dir == DIR_TOFROM) road.dir=DIR_FROMTO;
	//else if (road.dir == DIR_FROMTO) road.dir=DIR_TOFROM;
	tp = n2->nd->tile;
	tile_add_bareroad(&gen,tp,road,xp->xr.len);
}

static void handle_node(xml_help_t *xp)
{
	node_t node = {0};
	node.y=xp->xn.vrt.y;
	node.x=xp->xn.vrt.x;
	node.id=xp->xn.vrt.id;
	node.keep=0;
	uint32_t tile = ll_2_tile(node.y, node.x, LEVEL);
	node.tile=tcache_get(tc[LEVEL],tile);
	// only reached when no keys fail :
	gen.nnodes++; 
	tile_t *tp = tcache_get(tc[LEVEL],tile);
	gen_tile_add_node(&gen,tp,node);
}

static int check_order(xml_help_t *xp, int what)
{
	if (xp->current < what) xp->current = what;
	if (xp->current > what) {
		printf("Aiiii, i presumed osm files are sorted nodes,roads,relations, please resort or implement a 3-pass solution \n");
		exit(-1);
	}
	return xp->current;
}

static osm_help_road_t xml_road_nd (osm_help_road_t xw, const XML_Char **atts)
{
	int t;

	for (t=0; atts[t] != NULL; t+=2)
	{
		if (strcmp(atts[t],"ref")) {
			printf("Expecting only ref (was %s %s) (%d)!!!\n", atts[t], atts[t+1], xw.way.id);
			exit(0);
		} 
		if (xw.way.nn==0) {
			xw.way.nodes = (int32_t *)calloc(sizeof(int32_t), (xw.way.nn+1));
		} else {
			xw.way.nodes = (int32_t *)realloc(xw.way.nodes, sizeof(int32_t)* (xw.way.nn+1));
		}
		xw.way.nodes[xw.way.nn] = atol(atts[1]);
		xw.way.nn++;
	}
	return xw;
}

static osm_help_road_t osm_road_tag (osm_help_road_t xn, const XML_Char **atts)
{
	int t;

	int highway=0;
	for (t=0; atts[t] != NULL; t+=4)
	{
		if (strcmp(atts[t],"k")) {
			printf("Expecting only k (was %s)!!!\n", atts[t]);
			exit(0);
		} 
		if (strcmp(atts[t+2],"v")) {
			printf("Expecting only v (was %s)!!!\n", atts[t+2]);
			exit(0);
		}
		if (! strcmp(atts[t+1],"highway") ) {
			int type = classify_highway((const char *)atts[t+3]);

			highway=1;
			if (xn.way.type>= 0) {
				xn.dontuse=0;
				xn.way.type = (uint32_t) type;
			}

		} else
		if (! strcmp(atts[t+1],"building")) {
			if (strcmp(atts[t+3], "no"))  
				xn.way.dir = DIR_FROMTO;
		} else
		if (! strcmp(atts[t+1],"oneway")) {
			if (strcmp(atts[t+3], "no"))  
				xn.way.dir = DIR_BOTH;
		} else
		if (! strcmp(atts[t+1],"route")) {
			if (!strcmp(atts[t+3], "ferry")) {
				xn.way.type=AS_FERRY;
				xn.dontuse=0;
			//printf("Added ferry\n");
			}
		} else 
		if (! strcmp(atts[t+1],"motorcar")) {
			xn.motorcar=YES; 
			if (!strcmp(atts[t+3], "no"))  // or disable is no
				xn.motorcar=NO; 
		} else {
			//printf("Unknown node tag %s\n", atts[t+1]);
		}
	}
	return xn;
}

static xml_help_node_t xml_node_tag (xml_help_node_t xn, const XML_Char **atts)
{
	int t;

	for (t=0; atts[t] != NULL; t+=4)
	{
		if (strcmp(atts[t],"k")) {
			printf("Expecting only k (was %s)!!!\n", atts[t]);
			exit(0);
		} 
		if (strcmp(atts[t+2],"v")) {
			printf("Expecting only v (was %s)!!!\n", atts[t+2]);
			exit(0);
		}
		if (! strcmp(atts[t+1],"name")) {
		} else
		if (! strcmp(atts[t+1],"barrier")) {
			xn.closed=1;
		} else
		if (! strcmp(atts[t+1],"addr:city")) {
			// TODO : interesting
		} else
		if (! strcmp(atts[t+1],"addr:street")) {
			// TODO : interesting
		} else
		if (! strcmp(atts[t+1],"addr:postcode")) {
			// TODO : interesting
		} else
		if (! strcmp(atts[t+1],"addr:housenumber")) {
			// TODO : interesting
		} else
		if (! strcmp(atts[t+1],"addr:country")) {
			// TODO : interesting
		} else
		if (! strcmp(atts[t+1],"highway")) {
			// TODO : interesting (traffic_lights for instance)
		} else
		if (! strcmp(atts[t+1],"zone")) {
		} else
		if (! strcmp(atts[t+1],"description")) {
		} else
		if (! strcmp(atts[t+1],"zone")) {
		} else
		if (! strcmp(atts[t+1],"tourism")) {
		} else
		if (! strcmp(atts[t+1],"source")) {
		} else
		if (! strcmp(atts[t+1],"railway")) {
		} else
		if (! strcmp(atts[t+1],"AND_nodes")) {
		} else
		if (! strcmp(atts[t+1],"foot")) {
		} else
		if (! strcmp(atts[t+1],"operator")) {
		} else
		if (! strcmp(atts[t+1],"bicycle")) {
		} else
		if (! strcmp(atts[t+1],"atm")) {
		} else
		if (! strcmp(atts[t+1],"amenity")) {
		} else {
			//printf("Unknown node tags %s\n", atts[t+1]);
		}
	}
	return xn;
}

static xml_help_node_t xml_help_node_ist(const XML_Char **atts)
{
	int t;
	vrt_t vrt;
	xml_help_node_t nd;

	for (t=0; atts[t] != NULL; t+=2)
	{
		if (! strcmp(atts[t],"id")) {
			vrt.id = atol(atts[t+1]);
		} else
		if (! strcmp(atts[t],"y")) {
			vrt.y = float_2_int(atts[t+1]);
		} else
		if (! strcmp(atts[t],"x")) {
			vrt.x = float_2_int(atts[t+1]);
		}
	}
	nd.vrt=vrt;
	nd.vrt.stat=0;
	nd.vrt.level=0;
	return nd;
}

static osm_help_road_t osm_help_road_ist(const XML_Char **atts)
{
	int t;
	osm_help_road_t rd;

	for (t=0; atts[t] != NULL; t+=2)
	{
		if (! strcmp(atts[t],"id")) {
			rd.way.id = atol(atts[t+1]);
		}
	}
	rd.way.nn=0;
	rd.dontuse=1;
	rd.motorcar = UNKNOWN;
	rd.way.dir=DIR_BOTH;
	rd.way.nodes=NULL;
	return rd;
}

static xml_help_road_t xml_help_road_ist(const XML_Char **atts)
{
	int t;
	xml_help_road_t rd;

	for (t=0; atts[t] != NULL; t+=2)
	{
		if (! strcmp(atts[t],"v1")) {
			rd.from = atol(atts[t+1]);
		}
		if (! strcmp(atts[t],"v2")) {
			rd.to = atol(atts[t+1]);
		}
		if (! strcmp(atts[t],"length")) {
			rd.len = atol(atts[t+1]);
		}
	}
	return rd;
}

/* renumber the roads in a tile using the reference table (bigref)
	1 change the node id's with the tile offsets for the nodes
		do that for from as well as to 
	2 sort the roads on 'from' node so all roads of the same node
		will be contigues
*/
/*
void renum_rds(tile_t *tp)
{
	bigrefelm_t *nep=NULL;
	road_t *rp=NULL;
	bigref_t *br = gen.br;
	int r;

	//printf("Doing tile %d %p (%d)\n", t, tp, tp->num);
	for (r=0; r< tp->nroads; r++) {
		rp = tp->rds[r];
		if (rp->done) continue;
		//nep = bigref_get(br, rp->fromid);
		nep = bigref_get(br, rp->nodefrom->offset);
		int found=0;

		//printf("Doing x road %d %d \n", r, rp->fromid);
		if (nep && nep->done) {
			//printf("offset %d (last %d)\n", nep->offset, last);
			rp->nodefrom=tp->nds[nep->offset];
			found++;
		} else { 
			printf("index not found %d setting -1\n", rp->nodefrom->offset);
			rp->nodefrom=NULL;
			rp->done=1;
			//exit(-1);
		} 

		//nep = bigref_get(br, rp->toid);
		nep = bigref_get(br, rp->nodeto->offset);
		//printf("Doing road %d %d \n", r, rp->nodeto);
		if (nep && nep->done) {
			//printf("offset %d in %d done %d\n", nep->offset, nep->tile, nep->done);
			tile_t *tileto = tcache_get(tc[LEVEL],nep->tile);
			rp->nodeto=tileto->nds[nep->offset];
			//rp->done=nep->done;
			found++;
		} else { 
			//printf("index not found %d\n", rp->toid);
			rp->done=0;
			//exit(-1);
		} 
		if (found == 2) {
			rp->done= 1;
			//printf("Set 2 %d:%d (%d)\n", rp->tileto,rp->nodeto, rp->toid);
		}
	}
	tile_road_sort(tp);
}
*/

void wlk_renum_rds(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	//tcentry_t *tcep = *(tcentry_t **)elm;

	//tile_t *tp=tcep->tile;
	//tile_t *tp=tcache_get(tcep->tc, tcep->tileno);

	//return renum_rds(tp);
}

void renumber_roads(bigref_t *br, tcache_t *tc)
{
	tcache_walk(tc,wlk_renum_rds);
}

void write_pc(int32_t output, FILE *fp, vrt_t *i, indexer_t *nodearray)
{
	io.write_string(output,fp, i->pc);
	io.write_comment(output,fp, ",");
	io.write_int32(output,fp, i->tile);
	io.write_comment(output,fp, ":");
	io.write_int32(output,fp, nodearray[i->id].id);
	io.write_comment(output,fp, ":");
	//write_uint16(fp, i->country);
	//write_comment(fp, "\n");
}

int write_node(int i, FILE *fp, node_t np)
{	
	// comment out (not needed yet ?)
	//write_uint32(i, fp, np.node);
	//write_comment(i, fp, ",");
#ifndef SKIP_COORDS
// because no strict need (last speed up)
	io.write_uint32(i, fp, np.x);
	io.write_comment(i, fp, ",");
	io.write_uint32(i, fp, np.y);
	io.write_comment(i, fp, ",");
#endif
	//io.write_uint8(i, fp, (np.lastroad-np.firstroad)+1);
	io.write_uint8(i, fp, np.nroads);
	io.write_comment(i, fp, "\n");

	return 0;
}

int write_road(int i, FILE *fp, road_t rp)
{	
	io.write_uint32(i, fp, rp.nodefrom->offset);
	io.write_comment(i, fp, ",");
	io.write_uint32(i, fp, rp.nodeto->tile->num);
	io.write_comment(i, fp, ":");
	io.write_uint32(i, fp, rp.nodeto->offset);
	io.write_comment(i, fp, ",");
	io.write_uint32(i, fp, rp.len);
	io.write_comment(i, fp, ",");
	io.write_uint8(i, fp, rp.dir);
	io.write_comment(i, fp, ",");
	io.write_uint8(i, fp, rp.type);
	io.write_comment(i, fp, "\n");

	return 0;
}

int write_tile(int i, FILE *fp, tile_t *tp)
{	
	int t;
	int nroads = tp->nroads;
	int nnodes = tp->nnodes;

	printf("writing tile %d\n", tp->num);
	//tile_dmp(tp,1);

	io.write_comment(i, fp, "r: ");
	io.write_uint32(i, fp, nroads);
	io.write_comment(i, fp, "\n");
	for (t=0;t< nroads; t++) {
		road_t rdp=*tp->rds[t];
		write_road(i,fp,rdp);
	}
	io.write_comment(i, fp, "n: ");
	io.write_uint32(i, fp, nnodes);
	io.write_comment(i, fp, "\n");
	for (t=0;t< nnodes; t++) {
		node_t ndp=*tp->nds[t];
		write_node(i,fp,ndp);
	}

	return 0;
}

void write_idcs(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;

	tile_t *tp=tcache_get(tcep->tc, tcep->tileno);

	tile_reindex_roads(tp);
	// do this in aggregate !!
	//tcache_put(tc,tp->num,tp); 
}

void write_indices(bigref_t *br, tcache_t *tc)
{
	tcache_walk(tc,write_idcs);
}

/* renumber the nodes in a tile using the reference table (bigref)
	1 sort on y/x
	2 remove all nodes marked as STAT_DROP in the reference
	3 fill the node offset in the reference table
    4 resize the node array down (shrink)
	5 refill all node id's with the physical offset within the tile
*/
void renum_nds(tile_t *tp)
{
	int n;
	node_t *np=NULL;
	bigref_t *br = gen.br;
	bigrefelm_t *nep;
	tile_node_sort(tp);
	//printf("Doing tile %d %p\n", t, tp);
	int offset=0;
	for (n=0; n< tp->nnodes; n++) {
		np = tp->nds[n];
		//printf("Doing node %d %d\n", n, np->node);
		nep = (bigrefelm_t *)bigref_get(br,np->offset);
		if (!nep) return;
		//printf("found %d\n", nep->stat);
		np->keep = nep->keep;
		if (nep->keep == 0) continue;
		nep->offset = offset++;
		nep->done=1;
	}
	//printf("Counted %d of %d\n", offset, n);
	tile_node_shrink(tp,offset);
	// set some more data 
	for (n=0; n< tp->nnodes; n++) {
		np = tp->nds[n];
		np->offset=n;
		np->tile = tp;
	}
}

//renumber all nodes and store in the nodeindex for the road part to reference
void wlk_renum_nds(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;

	tile_t *tp = tcache_get(tcep->tc,tcep->tileno);
	renum_nds(tp);
}

void renumber_nodes(bigref_t *br, tcache_t *tc)
{
	tcache_walk(tc,wlk_renum_nds);
}

// recount all roads, and write the number into each node (outging roads)
void recount_rds(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	road_t *rp=NULL;
	//node_t *np=NULL;
	int r;

	tile_t *tp=tcache_get(tcep->tc, tcep->tileno);

	//printf("Doing tile %d %p\n", t, tp);
	for (r=0; r< tp->nroads; r++) {
		rp = tp->rds[r];
		//printf("Doing road %d %d\n", r, rp->from);
		//np = tp->nds[rp->nodefrom];
		//np->lastroad++;
	}
}

void recount_roads(bigref_t *br, tcache_t *tc)
{
	tcache_walk(tc,recount_rds);
}

void starthandler_xml(void *udata, const XML_Char *name, const XML_Char **atts)
{
	xml_help_t *xp = (xml_help_t *)udata;

	if (!strcmp(name, "osm") ){
		// ignore
	} else 
	if (!strcmp(name, "bound") ||
	    !strcmp(name, "bounds") ){
		// ignore
	} else 
	if (!strcmp(name, "vertex") ){
		check_order(xp, HANDLING_NODES);
		xp->xn = xml_help_node_ist(atts);
	} else 
	if (!strcmp(name, "edge") ){
		if (xp->current != HANDLING_ROADS) {
			printf("Encountered first road\n");
			//printf("Sorting node index\n");
			//bigref_sort(xp->br);
			//printf("Renumbering/indexing nodes\n");
			//renumber_nodes(xp->br, tc[LEVEL]);
		}

		check_order(xp, HANDLING_ROADS);
		xp->xr = xml_help_road_ist(atts);
	} else 
	if (!strcmp(name, "relation") ){
		if (xp->current != HANDLING_BOUNDS) {
			printf("Encountered bounds, wrap up roads !!\n");
		// TODO : wrap up roads !! 
		}
		check_order(xp, HANDLING_BOUNDS);
	} else 
	if (!strcmp(name, "member") ){
	} else {
		printf("Skipping unkown tag \"%s\"\n", name);
	}
}

void starthandler(void *udata, const XML_Char *name, const XML_Char **atts)
{
	xml_help_t *xp = (xml_help_t *)udata;

	if (!strcmp(name, "osm") ){
		// ignore
	} else 
	if (!strcmp(name, "bound") ||
	    !strcmp(name, "bounds") ){
		// ignore
	} else 
	if (!strcmp(name, "node") ){
		check_order(xp, HANDLING_NODES);
		xp->xn = xml_help_node_ist(atts);
	} else 
	if (!strcmp(name, "way") ){
		if (xp->current != HANDLING_ROADS) {
			printf("Encountered first road\n");
			//printf("Sorting node index\n");
			//bigref_sort(xp->br);
			printf("Renumbering/indexing nodes\n");
			renumber_nodes(xp->br, tc[LEVEL]);
		}

		check_order(xp, HANDLING_ROADS);
		xp->xw = osm_help_road_ist(atts);
	} else 
	if (!strcmp(name, "tag") ){
		if (xp->current == HANDLING_NODES) {
			xp->xn = xml_node_tag (xp->xn, atts);
		} else 
		if (xp->current == HANDLING_ROADS) {
			xp->xw = osm_road_tag (xp->xw, atts);
		} else 
		if (xp->current == HANDLING_BOUNDS) {
			//xml_node_tag (xp->xn, atts);
		} 
	} else 
	if (!strcmp(name, "nd") ){
		xp->xw = xml_road_nd (xp->xw, atts);
	} else 
	if (!strcmp(name, "relation") ){
		if (xp->current != HANDLING_BOUNDS) {
			printf("Encountered bounds, wrap up roads !!\n");
		// TODO : wrap up roads !! 
		}
		check_order(xp, HANDLING_BOUNDS);
	} else 
	if (!strcmp(name, "member") ){
	} else {
		printf("Skipping unkown tag \"%s\"\n", name);
	}
}

void endhandler_xml(void *udata, const XML_Char *name)
{
	xml_help_t *xp = (xml_help_t *)udata;
	//gen_t *gp=xp->gp;

	if (!strcmp(name, "osm") ){
	} else 
	if (!strcmp(name, "bound") ||
	    !strcmp(name, "bounds") ){
	} else 
	if (!strcmp(name, "vertex") ){
		xml_handle_node(xp);
	} else 
	if (!strcmp(name, "edge") ){
		//printf("Found %d nodes\n", xp->xr.sgm.nn);
		xml_handle_road(xp);
	} else 
	if (!strcmp(name, "tag") ){
	} else 
	if (!strcmp(name, "nd") ){
		// ignore
	} else 
	if (!strcmp(name, "relation") ){
	} else 
	if (!strcmp(name, "member") ){
	} else {
		printf("Skipping unkown tag \"%s\"\n", name);
	}
}

void endhandler(void *udata, const XML_Char *name)
{
	xml_help_t *xp = (xml_help_t *)udata;
	//gen_t *gp=xp->gp;

	if (!strcmp(name, "osm") ){
	} else 
	if (!strcmp(name, "bound") ||
	    !strcmp(name, "bounds") ){
	} else 
	if (!strcmp(name, "node") ){
		xml_handle_node(xp);
	} else 
	if (!strcmp(name, "way") ){
		//printf("Found %d nodes\n", xp->xr.sgm.nn);
		xml_handle_road(xp);
	} else 
	if (!strcmp(name, "tag") ){
	} else 
	if (!strcmp(name, "nd") ){
		// ignore
	} else 
	if (!strcmp(name, "relation") ){
	} else 
	if (!strcmp(name, "member") ){
	} else {
		printf("Skipping unkown tag \"%s\"\n", name);
	}
}

void gen_analyze_fmt_osm(gen_t *gp)
{
	XML_Parser p;
	//xml_help_t help = {0};
#define BUFF_SIZE 1024

	FILE *filep = gp->osmp;
	p = XML_ParserCreate("UTF-8");

	rewind(filep);

	//XML_SetElementHandler(p, startcounter, endcounter);
	//help.gp=gp;
	//XML_SetUserData(p, &help);

	for (;;) {
		int bytes_read;
  		char *buff = (char *)XML_GetBuffer(p, BUFF_SIZE);
  		if (buff == NULL) {
			printf("Encounterd memory error\n");
			exit(-2);
  		}

  		bytes_read = fread(buff, 1, BUFF_SIZE, filep);
  		if (bytes_read < 0) {
			printf("Encounterd read error\n");
			exit(-2);
  		}
		
  		if (! XML_ParseBuffer(p, bytes_read, bytes_read == 0)) {
			printf("Encounterd parse error\n");
			exit(-2);
  		}
		
  		if (bytes_read == 0)
   	 	break;
	}

	//XML_ParserFree(p);
}

void gen_read_fmt_osm(gen_t *gp)
{
	XML_Parser p;
	xml_help_t help = {0};
#define BUFF_SIZE 1024

	p = XML_ParserCreate("UTF-8");
	FILE *filep = gp->osmp;
	rewind(filep);

	help.current = HANDLING_NONE;
	help.br = bigref_ist(gp);

	XML_SetElementHandler(p, starthandler, endhandler);
	help.gp=gp;
	XML_SetUserData(p, &help);

	for (;;) {
		int bytes_read;
  		char *buff = (char *)XML_GetBuffer(p, BUFF_SIZE);
  		if (buff == NULL) {
			printf("Encounterd memory error\n");
			exit(-2);
  		}

  		bytes_read = fread(buff, 1, BUFF_SIZE, filep);
  		if (bytes_read < 0) {
			printf("Encounterd read error\n");
			exit(-2);
  		}
		
  		if (! XML_ParseBuffer(p, bytes_read, bytes_read == 0)) {
			printf("Encounterd parse error\n");
			exit(-2);
  		}
		
  		if (bytes_read == 0)
   	 	break;
	}
	//tcache_dmp(help.tc);

	recount_roads(help.br, tc[LEVEL]);
	bigref_rls(help.br);

	XML_ParserFree(p);

	printf("Gathered %d nodes and %d ways\n", gp->nnodes,gp->nways);
}

fileparam_t gen_analyze_fmt_and(FILE *filep, int (*check)(void *,char *), void *parms)
{
	int max=0;
	int c;
	fileparam_t fp={0};
	char *buf=(char *)malloc(max+1);
	// stub: analyze file to find buffer limits and certain counts
	
	rewind(filep);
	
	while ((c=fgetc(filep)) != EOF) {
		fp.chars ++;
		max++;
		if (max > fp.max_line){
			fp.max_line=max;
			buf = (char *)realloc(buf,max+1);
		}
		buf[max-1] = c;
		if (c == '\n') {
			fp.lines++;
			buf[max]='\0';
			max=0;
			check(parms,buf);
		}
		fp.count[c]++;
	}
	return fp;
}

#ifdef USING_MONGO
int init_mongodb()
{

	try {
  		c.connect("localhost");
    	cout << "connected ok" << endl;
  	} catch( mongo::DBException &e ) {
    	cout << "caught " << e.what() << endl;
  	}

  return 0;
}
#endif

void set_limits(void)
{
	int r;
    struct rlimit rlim;

	rlim.rlim_max = rlim.rlim_cur = RLIM_INFINITY;
    setrlimit(RLIMIT_NOFILE, &rlim);

    r = getrlimit(RLIMIT_NOFILE, &rlim);
	printf("# files (%ld,%ld)\n", rlim.rlim_cur, rlim.rlim_max);
}

int gen_init()
{
	init_highways();
#ifdef USING_MONGO
	init_mongodb();
#endif
	set_limits();
	return 0;
}

#ifdef NOT_USED
int gen_analyze_osm(gen_t *g)
{
	//sgmparam_t sp={0};
	//vrtparam_t vp={0};

	//sp.id_sorted=1;
	//sp.id_double=0;
	
	g->min_x=1800000000;
	g->max_x=-1800000000;
	g->min_y=900000000;
	g->max_y=-900000000;
	g->nstreets=0;
	g->ncities=0;
	g->npostcodes=0;
	
	//gen_analyze_fmt_osm(g);
	return 0;
}

gen_t *gen_cut_boundbox(gen_t *g, rect_t boundbox)
{
	// cut all nodes outside boundbox, and all roads reaching them
	// no connectivity check !, do that in another function
	gen_t *g2;
	int n,r;

	if (g->stat == GEN_SPLIT) {
		printf("Network already tiled, cannot perform operation!\n");
		return NULL;
	}

	for (n=0; n< g->nnodes; n++) {
		// make sure you are altering the real thing
		vrt_t *v = &(g->vrt_arr[n]); 

		if ( ! TEST_WITHIN(v, boundbox))
			v->stat = STAT_DROP;
		else  {
			v->stat=STAT_KEEP;
			/* printf("node : %d\n", v->id); */
		}
	}
	for (r=0; r< g->nroads; r++) {
		sgm_t *s = &(g->sgm_arr[r]); 
		vrt_t from = g->vrt_arr[s->from]; 
		vrt_t to = g->vrt_arr[s->to]; 
		if (from.stat == STAT_DROP || to.stat == STAT_DROP)
			s->stat=STAT_DROP;
		else {
			s->stat=STAT_KEEP;
			/* printf("road : %d,%d\n", s->from,s->to); */
		}
	}

	g2 = gen_prune(g);
	printf("%d wordt %d and %d wordt %d\n", g->nnodes, g2->nnodes, g->nroads, g2->nroads);
	return g2;
}
#endif

// dont bother, what if you have indonesia, it just should be possible
// to plan on each seperate island, handle it in the net code
int gen_check_connected(gen_t *g)
{
	int connected = 1;

	int v,s;
	int biggest=-1;
	int vl=g->nnodes;
	int sl=g->nroads;
	vrt_t *vp;
	sgm_t *sp;
	vrt_t *start=NULL;
	vrt_t *va=g->vrt_arr;
	sgm_t *sa=g->sgm_arr;
	int label= 0;

	// label all nodes as 0
	
 	for (v=0; v< vl; v++) {
		vp = &va[v];
		vp = &va[v];
		vp->stat=0;
	}
	
	while (1) {
		start = NULL;
		// find the first 'clean' node
 		for (v=0; v< vl; v++) {
			vp = &va[v];
			if (vp->stat==0) {
				start = vp;
				break;
			}
		}
		if (!start) break;

		label++;
		//printf("Label is %d\n", label);
		start->stat=label;
		int labeled=1;
		//printf("start is %d,%d\n", start->y, start->x);

		vrt_t *from;
		vrt_t *to;
		int island=1;
		while (labeled>0) {
			//printf("%d nodes\n", labeled);
			labeled=0;
			for (s=0; s< sl; s++) {
				sp = &sa[s];
				from = &va[sp->from];
				to = &va[sp->to];
				if (sp->dir != DIR_TOFROM && from->stat==label ) {
					if (to->stat == 0) {
						labeled++;
						island++;
						to->stat=label;
						sp->stat=label;
					} else {
						if (to->stat != label) {
							printf("%d leavable not reachable  %d ?\n", to->stat, to->x);
						}
					}
				}
				if (sp->dir != DIR_FROMTO && to->stat==label) {
					if (from->stat == 0 ) {
						labeled++;
						island++;
						from->stat=label;
						sp->stat=label;
					} else {
						if (from->stat != label) {
							printf("%d reachable not leavable %d ?\n", from->stat, from->id);
						}
					}
				}
			}
		}
		if (island > ISLE_LIMIT) {
			if (biggest != -1) {
				printf("Got multiple networks bigger than %d: %d and %d\n", ISLE_LIMIT, biggest, island);
				printf("node : %d,%d\n", start->y, start->x);
			}
			biggest = label;
		}
	}

	printf("Chosen island %d\n", biggest);

	int vd=0;
	int sd=0;
	// just whipe all small islands
 	for (v=0; v< vl; v++) {
		vp = &va[v];
		if (vp->stat==biggest) {
			vp->stat=STAT_KEEP;
		} else {
			vp->stat=STAT_DROP;
			vd++;
		}
	}
	printf("Deleting %d unreachable/unleavable vertices\n", vd);

 	for (s=0; s< sl; s++) {
		sp = &sa[s];
		if (sp->stat==biggest) {
			sp->stat=STAT_KEEP;
		} else {
			sp->stat=STAT_DROP;
			sd++;
		}
	}
	printf("Deleting %d unreachable/unleavable segments\n", sd);

	return connected;
}

// osm always has 7 digits after the comma
int32_t float_2_int(const char *num)
{
	return round( atof(num) *10000000);
}

int32_t float_2_int_orig(char *num)
{
	int32_t c=0;
	int32_t dgr=0;
	int32_t sign=1; // 1000 for and data ?
	int32_t precision=4;

	if (num[c]== '-') {
		sign = -1;
		c++;
	}
	while(num[c] != '.' && num[c] != '\0' ) {
		dgr *=10;
		dgr+=num[c] -'0';
		c++;
	}
	if(num[c] != '\0' ) 
		c++; 	// 52 is not entered as 52.0
	while(num[c]!= '\0') {
		dgr *=10;
		dgr+=num[c] -'0';
		precision--;
		c++;
	}
	while (precision > 0) {
		dgr *=10;
		precision--;
	}
	dgr*=sign;
	/* printf("%s wordt %d\n", num, dgr); */

	return dgr;
}


rect_t boundbox(int tile)
{
	rect_t r={{0,0},{0,0}};
	return r;
}

int vrt_sort(gen_t *g, int (*cmp)(const void *,const void *) )
{
	if (!cmp)
		cmp=vrt_cmp;

	qsort(g->vrt_arr, g->nnodes, sizeof(vrt_t), cmp);
	return 0;
}

int gen_mark_unused(gen_t *g)
{
	vrt_t *vp;
	sgm_t *sp;
	int marked=0;
	int vl=g->nnodes;
	int sl=g->nroads;
	vrt_t *va=g->vrt_arr;
	sgm_t *sa=g->sgm_arr;
	int v,s;

	// initialize on DROP
	for (v=0; v< vl; v++) {
		vp = &va[v];
		vp->stat=STAT_DROP;
		marked++;
	}
	// reset all used nodes
	for (s=0; s< sl; s++) {
		sp = &sa[s];
		if (sp->stat == STAT_KEEP) {
			if (sp->from >=0) {
				if ( va[sp->from].stat != STAT_KEEP) {
					marked--;
				}
				va[sp->from].stat = STAT_KEEP;
			}
			if (sp->to >=0) { 
				if ( va[sp->to].stat != STAT_KEEP) {
					marked--;
				}
				va[sp->to].stat = STAT_KEEP;
			}
		}
	}

	return marked;
}

void gen_clean(gen_t *g)
{
	vrt_t *va=g->vrt_arr;
	sgm_t *sa=g->sgm_arr;
	vrt_t *vp;
	sgm_t *sp;
	int vl=g->nnodes;
	int sl=g->nroads;

	int v,s;

	// roads
	for (v=0; v< vl; v++) {
		vp = &va[v];
		vp->stat = 0;
	}
	for (s=0; s< sl; s++) {
		sp = &sa[s];
		sp->stat = 0;
	}
}

//copy a network, skipping all nodes/roads marked STAT_DROP
// do it in-place, dont worry about the access array indices
void gen_prune(gen_t *g)
{
	vrt_t *va=g->vrt_arr;
	sgm_t *sa=g->sgm_arr;
	vrt_t *vp;
	sgm_t *sp;
	int vl=g->nnodes;
	int sl=g->nroads;

	int v,s;
	int vv=0, ss=0;

	// roads
	for (s=0; s< sl; s++) {
		sp = &sa[s];
		if (sp->from >= 0 &&  va[sp->from].stat != STAT_DROP &&
			sp->to >= 0 && va[sp->to].stat != STAT_DROP &&
			sp->stat != STAT_DROP) {
			sa[ss] = sa[s];
			ss++;
		}
	}
	g->nroads= ss;
	for (v=0; v< vl; v++) {
		vp = &va[v];
		if (vp->stat != STAT_DROP) {
			va[vv] = va[v];
			vv++;
		}
	}
	g->nnodes= vv;

	printf("Pruned to %d nodes %d roads\n", g->nnodes, g->nroads);
}

int gen_renumber(gen_t *g)
{
	int v;
	int s;
	int marked=0;
	vrt_idx_t *vip;

	vrt_sort(g,NULL);

	vrt_t *va=g->vrt_arr;
	sgm_t *sa=g->sgm_arr;
	int vl=g->nnodes;
	int sl=g->nroads;

	// create helper array
	vrt_idx_t *hlp = (vrt_idx_t *)calloc(sizeof(vrt_idx_t),vl);
	if (!hlp) return -1;
	for (v=0; v< vl; v++) {
		hlp[v].id=va[v].id;
		hlp[v].n=v;
	}
	qsort(hlp, vl, sizeof(vrt_idx_t), vrt_idx_cmp);

	// renumber the segments physically
	for (s=0; s< sl; s++) {
		vip = (vrt_idx_t *)bsearch(&sa[s].from,hlp,vl,sizeof(vrt_idx_t),vrt_idx_cmp);
		if (!vip ) { 
		//if (!vip || va[vip->n].stat == STAT_DROP) { 
			printf("From node %d is not present (deleting road)\n", sa[s].from);
			//ret = -1; 
			sa[s].stat = STAT_DROP;
			marked++;
			//exit(-1);
			//goto cleanup; 
			sa[s].from=-1;
		} else
			sa[s].from=vip->n;
		vip = (vrt_idx_t *)bsearch(&sa[s].to,hlp,vl,sizeof(vrt_idx_t),vrt_idx_cmp);
		//if (!vip || va[vip->n].stat == STAT_DROP) { 
		if (!vip ) { 
			printf("To node %d is not present\n", sa[s].to);
			sa[s].stat = STAT_DROP;
			marked++;
			//exit(-1);
			//goto cleanup; 
			sa[s].to=-1;
		} else
			sa[s].to=vip->n;
		//printf("to : %d %d\n", s, sa[s].to);
	}

	// renumber vertices 
	for (v=0; v< vl; v++) {
		va[v].id=v;
	}

	sgm_sort(g,NULL);

	if (hlp) free(hlp);
	
	return marked;
}

int sgm_tile_cmp(const void *a, const void *b)
{
	const sgm_t *A=(const sgm_t *)a;
	const sgm_t *B=(const sgm_t *)b;

	if (A->tile < B->tile) return -1;
	if (A->tile > B->tile) return  1;

	if (A->from < B->from) return -1;
	if (A->from > B->from) return  1;
	if (A->to < B->to) return -1;
	if (A->to > B->to) return  1;
	return 0;
}

int sgm_cmp(const void *a, const void *b)
{
	const sgm_t *A=(const sgm_t *)a;
	const sgm_t *B=(const sgm_t *)b;

	// to, then from 
	if (A->to < B->to) return -1;
	if (A->to > B->to) return  1;
	if (A->from < B->from) return -1;
	if (A->from > B->from) return  1;
	return 0;
}

int sgm_sort(gen_t *g, int (*cmp)(const void *,const void *) )
{
	if (!cmp)
		cmp=sgm_cmp;

	qsort(g->sgm_arr, g->nroads, sizeof(sgm_t), cmp);
	return 0;
}


int gen_calc_lengths(gen_t *g)
{
	int s;

	sgm_t *sa = g->sgm_arr;
	vrt_t *va = g->vrt_arr;
	sgm_t sgm;
	int32_t max=0;

	for (s=0; s < g->nroads; s++) {
		sgm = sa[s];
		double x1, x2, y1, y2, len;
		x1 = (double)va[sgm.from].x / 10000000.0;
		y1 = (double)va[sgm.from].y / 10000000.0;
		x2 = (double)va[sgm.to].x / 10000000.0;
		y2 = (double)va[sgm.to].y / 10000000.0;
		//printf("x1 : %f %d \n", x1, va[sgm.from].x);
		//printf("y1 : %f %d \n", y1, va[sgm.from].y);
		len = haversine.distance(y1,x1,y2,x2);
		sa[s].len = (int32_t) round(len + 0.5);
		if (sa[s].len == 0) {
			printf("Heyyyyy 0 \n");
		//printf("x1 : %f %f \n", y1, x1);
		//printf("x2 : %f %f \n", y2, x2);
		//printf("x2 : %f \n", len);
		}
		//printf("%f %d \n", len, sa[s].len);
		if (sa[s].len > max) max = sa[s].len;
	}
	g->max_len = max;
	return 0;
}

#ifdef USED
// analyze the file data 
int gen_analyze_file(gen_t *g)
{
	int ret;
	ret = gen_scandir(g);
	if (ret != 0) return ret;

	if (g->osmp) return gen_analyze_osm(g);

	// otherwise do and checks
	printf("analyzing vertices\n");
	g->vp= gen_analyze_vertices(g->vrtp);
	printf("analyzing segments\n");
	g->sp= gen_analyze_segments(g->sgmp);
	g->nnodes = g->vp.fp.lines;
	g->nroads = g->sp.fp.lines;

	printf("%d streets from %d\n", g->vp.nstreets, g->nnodes);
	printf("%d postcode nodes from %d\n", g->vp.npostcodes, g->nnodes);
	return 0;
}

// reanalyse
int gen_analyze_net(gen_t *g)
{
	int v,s;
	static int32_t lastfrom;
	sgm_t sgm;
	vrt_t vrt;

	g->min_x=1800000000;
	g->max_x=-1800000000;
	g->min_y=900000000;
	g->max_y=-900000000;
	
	for (v=0; v< g->nnodes; v++) {
		vrt=g->vrt_arr[v];

		if (vrt.id == g->vp.max_id) g->vp.id_double=1;
		if (vrt.id < g->vp.max_id) g->vp.id_sorted=0;
		else 
			g->vp.max_id=vrt.id;
		if (vrt.y == g->vp.max_y) {
			if (vrt.x == g->vp.max_x) 
				g->vp.xy_double=1;
			else if (vrt.x > g->vp.max_x) {
				g->vp.xy_sorted=0;
			}
		} else 
		if (vrt.y < g->vp.max_y) {
			g->vp.xy_sorted=0;
		}
		if (vrt.y<g->vp.min_y)g->vp.min_y=vrt.y;
		if (vrt.y>g->vp.max_y)g->vp.max_y=vrt.y;
		if (vrt.x<g->vp.min_x)g->vp.min_x=vrt.x;
		if (vrt.x>g->vp.max_x)g->vp.max_x=vrt.x;
	}

	g->sp.id_sorted=1;
	g->sp.id_double=0;

	// check sorted
	for (s=0; s< g->nroads; s++) {
		sgm=g->sgm_arr[s];
		if (sgm.to == g->sp.max_to) {
			if (sgm.from == lastfrom) 
				g->sp.id_double=1;
			else if (sgm.from > lastfrom) {
				g->sp.id_sorted=0;
			}
		} else 
		if (sgm.to < g->sp.max_to) {
			g->sp.id_sorted=0;
		}
	}

	g->vp.fp.lines=g->nnodes;
	g->sp.fp.lines=g->nroads;
	return 0;
}
#endif

int gen_read_osm(gen_t *g)
{
	gen_read_fmt_osm(g);
	return 0;
}


bool usecolor = false;
// prints a formatted message to stdout, optionally color coded
void msg(const char* format, int color, va_list args) {
    if(usecolor) {
        fprintf(stdout, "\x1b[0;%dm", color);
    }
    vfprintf(stdout, format, args);
    if(usecolor) {
        fprintf(stdout, "\x1b[0m\n");
    } else {
        fprintf(stdout, "\n");
    }
}

// prints a formatted message to stderr, color coded to red
void err(const char* format, ...) {
    va_list args;
    va_start(args, format);
    msg(format, 31, args);
    va_end(args);
    exit(1);
}

// prints a formatted message to stderr, color coded to yellow
void warn(const char* format, ...) {
    va_list args;
    va_start(args, format);
    msg(format, 33, args);
    va_end(args);
}

// prints a formatted message to stderr, color coded to green
void info(const char* format, ...) {
    va_list args;
    va_start(args, format);
    msg(format, 32, args);
    va_end(args);
}

// prints a formatted message to stderr, color coded to white
void debug(const char* format, ...) {
    va_list args;
    va_start(args, format);
    msg(format, 37, args);
    va_end(args);
}

void pbf_handle_node_count(gen_t *g,node_t node)
{
}

void pbf_handle_road_count(gen_t *g,OSMPBF::PrimitiveGroup pg)
{
}

void pbf_handle_node_dice(gen_t *g, node_t node)
{
	uint32_t tile = ll_2_tile(node.y, node.x, LEVEL);
	tile_t *tp;
	tp = tcache_get(tc[LEVEL],tile);
	gen_tile_add_node(g,tp,node);
	bigref_put(g->br,node,tile);
}

int gen_count_pbf(gen_t *g, int dumptypes)
{
	FILE *fp = g->pbfp;
	int n;
    int32_t sz;
	g->br = bigref_ist(g);
	handling stage=HANDLING_NONE;
	//printf("----- tilecache is %p\n", tc[LEVEL]);

	while (!feof(fp)) {
    	if(fread(&sz, sizeof(sz), 1, fp) != 1) break;
    	sz = ntohl(sz);
    	if(sz > OSMPBF::max_blob_header_size)
        	cerr << "blob header too large";
    	if(fread(buffer, sz, 1, fp) != 1)
        	cerr << "unable to read blob-header from file";

        if(!blobheader.ParseFromArray(buffer, sz))
            cerr << "unable to parse blob header";
        sz = blobheader.datasize();

        if(blobheader.has_indexdata())
            debug("  indexdata = %u bytes", blobheader.indexdata().size());

        if(sz > OSMPBF::max_uncompressed_blob_size)
            err("blob-size is bigger then allowed (%u > %u)", sz, OSMPBF::max_uncompressed_blob_size);

        if(fread(buffer, sz, 1, fp) != 1)
            err("unable to read blob from file");
        if(!blob.ParseFromArray(buffer, sz))
            err("unable to parse blob");
        bool found_data = false;
        if(blob.has_raw()) {
            found_data = true;
            sz = blob.raw().size();
            if(sz != blob.raw_size())
                warn("  reports wrong raw_size: %u bytes", blob.raw_size());
            memcpy(unpack_buffer, buffer, sz);
        }

        if(blob.has_zlib_data()) {
            if(found_data)
                warn("  contains several data streams");
            found_data = true;
            sz = blob.zlib_data().size();
            z_stream z;
            z.next_in   = (unsigned char*) blob.zlib_data().c_str();
            z.avail_in  = sz;
            z.next_out  = (unsigned char*) unpack_buffer;
            z.avail_out = blob.raw_size();
            z.zalloc    = Z_NULL;
            z.zfree     = Z_NULL;
            z.opaque    = Z_NULL;

            if(inflateInit(&z) != Z_OK) {
                err("  failed to init zlib stream");
            }
            if(inflate(&z, Z_FINISH) != Z_STREAM_END) {
                err("  failed to inflate zlib stream");
            }
            if(inflateEnd(&z) != Z_OK) {
                err("  failed to deinit zlib stream");
            }

            sz = z.total_out;
        }

        if(blob.has_lzma_data()) {
            if(found_data)
                warn("  contains several data streams");
            found_data = true;
            err("  lzma-decompression is not supported");
        }

        if(!found_data)
            err("  does not contain any known data stream");

        if(blobheader.type() == "OSMHeader") {
            if(!headerblock.ParseFromArray(unpack_buffer, sz))
                err("unable to parse header block");
            if(headerblock.has_bbox()) {
                OSMPBF::HeaderBBox bbox = headerblock.bbox();
                debug("    bbox: %.7f,%.7f,%.7f,%.7f",
                    (double)bbox.left() / OSMPBF::lonlat_resolution,
                    (double)bbox.bottom() / OSMPBF::lonlat_resolution,
                    (double)bbox.right() / OSMPBF::lonlat_resolution,
                    (double)bbox.top() / OSMPBF::lonlat_resolution);
            }

            if(headerblock.has_writingprogram());
                debug("    writingprogram: %s", headerblock.writingprogram().c_str());
        }

        else if(blobheader.type() == "OSMData") {
            if(!primblock.ParseFromArray(unpack_buffer, sz))
                err("unable to parse primitive block");
            for(int i = 0, l = primblock.primitivegroup_size(); i < l; i++) {
                OSMPBF::PrimitiveGroup pg = primblock.primitivegroup(i);
                bool found_items=false;
                if(pg.nodes_size() > 0) {
                    found_items = true;

                    debug("      nodes: %d", pg.nodes_size());
					debug(" No handler for plain nodes yet !!");
					debug(" Implement ?!!");
					exit(-2);
                    //if(pg.nodes(0).has_info())
                        //debug("        with meta-info");
                }
                if(pg.has_dense()) {
                    found_items = true;
					if (stage == HANDLING_NONE) {
						printf("Started handling nodes\n");
						stage = HANDLING_NODES;
					} else if (stage != HANDLING_NODES) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expected nodes\n");
						exit(-3);
					}
					node_t node= {0};
					int64_t id=0,lat=0,lon=0,keys=0;
					int keycount=0;
		// NODE LOOP :
					for (int i=0;i< pg.dense().id_size(); i++) {
						id  += pg.dense().id(i);
						lat += pg.dense().lat(i);
						lon += pg.dense().lon(i);

						keys = pg.dense().keys_vals_size();
						while (keys && pg.dense().keys_vals(keycount) != 0) {
							std::string key = primblock.stringtable().s(pg.dense().keys_vals(keycount++));
							std::string val = primblock.stringtable().s(pg.dense().keys_vals(keycount++));
							if (node_keyfail(key,val)) goto nextnode;
							//cout << "k: " << key << " v: " << val;
						}
						node.y=lat;
						node.x=lon;
						node.offset=id;
						node.keep=0;
						// only reached when no keys fail :
						g->nnodes++; 
					nextnode:

						keycount++;
					}
					// these should match up !				
					if (keycount != keys) printf("Fail %ld and %d\n", keys, keycount);
                 //   if(pg.dense().has_denseinfo())
                        //debug("        with meta-info");

                }

                // tell about ways
                if(pg.ways_size() > 0) {
                    found_items = true;

					if (stage == HANDLING_NODES) {
						printf("Started handling roads\n");
						stage = HANDLING_ROADS;
						//printf("Sorting node index\n");
						//bigref_sort(g->br);
					} else if (stage != HANDLING_ROADS) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expecting roads\n");
						exit(-3);
					}

					OSMPBF::Way way;
					int64_t id=0,keys=0,vals=0,refs=0;
					for (int i=0;i< pg.ways_size(); i++) {
						road_t wayroad= {0};
						way = pg.ways(i);
						id = way.id();
						keys = way.keys_size();
						vals = way.vals_size();
						wayroad.keep=0;
						for (int v=0; v< vals; v++) {
							// FOR TRUCKS !!! heavy goods vehicles
							//if (key=="hgv" && val=="yes") ferrytest++;
							std::string key = primblock.stringtable().s(way.keys(v));
							std::string val = primblock.stringtable().s(way.vals(v));
							wayroad = road_fill(wayroad,key,val);
							if (key == "highway" && dumptypes) 
								cout << key << " : " << val << "\n";
						}
						refs = way.refs_size();
						int64_t node=0;

						g->nways++;

						for (n=0; n< refs; n++) {
							node += way.refs(n);
							//debug("node %d", node);

							if (wayroad.keep == 1) {
								g->nroads++;
							}
					}
                }

                // tell about relations
                if(pg.relations_size() > 0) {
                    found_items = true;
					if (stage == HANDLING_ROADS) {
						printf("Nothing to do for relations\n");
						stage = HANDLING_BOUNDS;
						break;
					} else if (stage != HANDLING_BOUNDS) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expecting bounds\n");
						exit(-3);
					}

                    debug("      relations: %d", pg.relations_size());
                    if(pg.relations(0).has_info())
                        debug("        with meta-info");
                }

                if(!found_items)
                    warn("      contains no items");
            }
        	}

        } else {
            // unknown blob type
            warn("  unknown blob type: %s", blobheader.type().c_str());
        }
	}
	printf("\n");

	//tcache_dmp(tc[LEVEL]);
	printf("Renumbering nodes\n");
	renumber_nodes(g->br, tc[LEVEL]);
	printf("Renumbering roads\n");
	renumber_roads(g->br,tc[LEVEL]);
	printf("writing indices\n");
	write_indices(g->br,tc[LEVEL]);
	bigref_rls(g->br);
	splitref_rls(g->sr);

	return 0;
}

#ifdef USING_MONGO
void append_road_mongo(gen_t *gen, int tile, int from, int totile, int tonode,int dir,int type)
{
	char mongoname[100] = "grid.tiles";
	mongo::BSONObjBuilder b;
	b.append("tile", tile);
	b.append("from", from);
	b.append("totile", totile);
	b.append("tonode", tonode);
	b.append("dir", dir);
	b.append("type", type);
	mongo::BSONObj p = b.obj();
	//sprintf(mongoname,"grid.%10.10d\n", tile);
	c.insert(mongoname,p);
	//c.ensureIndex("grid.split", mongo::fromjson("{tile:1}"));
	c.ensureIndex(mongoname, mongo::fromjson("{tile:1}"));
}

void append_node_mongo(gen_t *gen, int tile, int id, int y, int x)
{
	char mongoname[100] = "grid.tiles";
	mongo::BSONObjBuilder b;
	b.append("tile", tile);
	b.append("node", id);
	b.append("y", y);
	b.append("x", x);
	mongo::BSONObj p = b.obj();
	//sprintf(mongoname,"grid.%10.10d\n", tile);
	c.insert(mongoname,p);
	c.ensureIndex("grid.tiles", mongo::fromjson("{tile:1}"));
}
#endif

void append_road_file(gen_t *gen, int tile, int from, int totile, int tonode,int dir,int type)
{
	char fname[300];
	bareroad_t br;

	br.from=from;
	br.tileto=totile;
	br.nodeto=tonode;
	br.dir=dir;
	br.type=type;

	sprintf(fname, "%s/%s/road_%10.10d",gen->outdir,gen->ibase,tile);
	FILE *fp= fopen(fname,"a");
	fwrite(&br,sizeof(bareroad_t),1,fp);
	fclose(fp);
}

void append_node_file(gen_t *gen, int tile, int id, int y, int x)
{
	char fname[300];
	barenode_t bn;

	bn.id=id;
	bn.x=x;
	bn.y=y;

	sprintf(fname, "%s/%s/node_%10.10d",gen->outdir,gen->ibase,tile);
	FILE *fp= fopen(fname,"a");
	fwrite(&bn,sizeof(barenode_t),1,fp);
	fclose(fp);
}

void append_road_cache(gen_t *gen, int tile, int from, int totile, int tonode,int dir,int type)
{
	bareroad_t br;

	br.tile=tile;
	br.from=from;
	br.tileto=totile;
	br.nodeto=tonode;
	br.dir=dir;
	br.type=type;

	tileref_put_road(gen->tr,tile,br);
}

void append_node_cache(gen_t *gen, int tile, int id, int y, int x)
{
	barenode_t bn;

	bn.id=id;
	bn.x=x;
	bn.y=y;
	//printf("App node %d:%d\n", tile,id);

	tileref_put_node(gen->tr,tile,bn);
}

void gen_read_xml(gen_t *gp)
{
	XML_Parser p;
	xml_help_t help = {0};
#define BUFF_SIZE 1024

	gp->lr = linkref_ist();
	p = XML_ParserCreate("UTF-8");
	FILE *filep = gp->xmlp;
	rewind(filep);

	help.current = HANDLING_NONE;
	help.br = bigref_ist(gp);

	xml_handle_node = handle_node;
	xml_handle_road = handle_road;

	XML_SetElementHandler(p, starthandler_xml, endhandler_xml);
	help.gp=gp;
	XML_SetUserData(p, &help);

	for (;;) {
		int bytes_read;
  		char *buff = (char *)XML_GetBuffer(p, BUFF_SIZE);
  		if (buff == NULL) {
			printf("Encounterd memory error\n");
			exit(-2);
  		}

  		bytes_read = fread(buff, 1, BUFF_SIZE, filep);
  		if (bytes_read < 0) {
			printf("Encounterd read error\n");
			exit(-2);
  		}
		
  		if (! XML_ParseBuffer(p, bytes_read, bytes_read == 0)) {
			printf("Encounterd parse error\n");
			exit(-2);
  		}
		
  		if (bytes_read == 0)
   	 	break;
	}
	//tcache_dmp(help.tc);

	recount_roads(help.br, tc[LEVEL]);
	bigref_rls(help.br);

	XML_ParserFree(p);

	printf("Gathered %d nodes and %d ways\n", gp->nnodes,gp->nways);
}

int gen_read_names_pbf(gen_t *g)
{
	FILE *fp = g->pbfp;
	//g->tr = tileref_ist(g,10000000);
	int nodeid,n;
    int32_t sz;
	tile_t *tp;
	g->lr = linkref_ist();
	handling stage=HANDLING_NONE;
	double x1,y1,x2,y2,len;

	tc[LEVEL] = tcache_ist(g,LEVEL,100000000,TC_WRONLY);

	while (!feof(fp)) {
    	if(fread(&sz, sizeof(sz), 1, fp) != 1) break;
    	sz = ntohl(sz);
    	if(sz > OSMPBF::max_blob_header_size)
        	cerr << "blob header too large";
    	if(fread(buffer, sz, 1, fp) != 1)
        	cerr << "unable to read blob-header from file";

        if(!blobheader.ParseFromArray(buffer, sz))
            cerr << "unable to parse blob header";
        sz = blobheader.datasize();

        if(blobheader.has_indexdata())
            debug("  indexdata = %u bytes", blobheader.indexdata().size());

        if(sz > OSMPBF::max_uncompressed_blob_size)
            err("blob-size is bigger then allowed (%u > %u)", sz, OSMPBF::max_uncompressed_blob_size);

        if(fread(buffer, sz, 1, fp) != 1)
            err("unable to read blob from file");
        if(!blob.ParseFromArray(buffer, sz))
            err("unable to parse blob");
        bool found_data = false;
        if(blob.has_raw()) {
            found_data = true;
            sz = blob.raw().size();
            if(sz != blob.raw_size())
                warn("  reports wrong raw_size: %u bytes", blob.raw_size());
            memcpy(unpack_buffer, buffer, sz);
        }

        if(blob.has_zlib_data()) {
            if(found_data)
                warn("  contains several data streams");
            found_data = true;
            sz = blob.zlib_data().size();
            z_stream z;
            z.next_in   = (unsigned char*) blob.zlib_data().c_str();
            z.avail_in  = sz;
            z.next_out  = (unsigned char*) unpack_buffer;
            z.avail_out = blob.raw_size();
            z.zalloc    = Z_NULL;
            z.zfree     = Z_NULL;
            z.opaque    = Z_NULL;

            if(inflateInit(&z) != Z_OK) {
                err("  failed to init zlib stream");
            }
            if(inflate(&z, Z_FINISH) != Z_STREAM_END) {
                err("  failed to inflate zlib stream");
            }
            if(inflateEnd(&z) != Z_OK) {
                err("  failed to deinit zlib stream");
            }

            sz = z.total_out;
        }

        if(blob.has_lzma_data()) {
            if(found_data)
                warn("  contains several data streams");
            found_data = true;
            err("  lzma-decompression is not supported");
        }

        if(!found_data)
            err("  does not contain any known data stream");

        if(blobheader.type() == "OSMHeader") {
            if(!headerblock.ParseFromArray(unpack_buffer, sz))
                err("unable to parse header block");
            if(headerblock.has_bbox()) {
                OSMPBF::HeaderBBox bbox = headerblock.bbox();
                debug("    bbox: %.7f,%.7f,%.7f,%.7f",
                    (double)bbox.left() / OSMPBF::lonlat_resolution,
                    (double)bbox.bottom() / OSMPBF::lonlat_resolution,
                    (double)bbox.right() / OSMPBF::lonlat_resolution,
                    (double)bbox.top() / OSMPBF::lonlat_resolution);
            }

            if(headerblock.has_writingprogram());
                debug("    writingprogram: %s", headerblock.writingprogram().c_str());
        }

        else if(blobheader.type() == "OSMData") {
            if(!primblock.ParseFromArray(unpack_buffer, sz))
                err("unable to parse primitive block");
            for(int i = 0, l = primblock.primitivegroup_size(); i < l; i++) {
                OSMPBF::PrimitiveGroup pg = primblock.primitivegroup(i);
                bool found_items=false;
                if(pg.nodes_size() > 0) {
                    found_items = true;

                    debug("      nodes: %d", pg.nodes_size());
					debug(" No handler for plain nodes yet !!");
					debug(" Implement ?!!");
					exit(-2);
                    //if(pg.nodes(0).has_info())
                        //debug("        with meta-info");
                }
                if(pg.has_dense()) {
                    found_items = true;
					if (stage == HANDLING_NONE) {
						printf("Started handling nodes\n");
						stage = HANDLING_NODES;
					} else if (stage != HANDLING_NODES) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expected nodes\n");
						exit(-3);
					}
					node_t node= {0};
					int64_t id=0,lat=0,lon=0,keys=0;
					int keycount=0;
		// NODE LOOP :
					for (int i=0;i< pg.dense().id_size(); i++) {
						id  += pg.dense().id(i);
						lat += pg.dense().lat(i);
						lon += pg.dense().lon(i);

						uint32_t tile = ll_2_tile(lat, lon, LEVEL);
						keys = pg.dense().keys_vals_size();
	// make sure keys exist, and since every key/val sequence is terminated 
	// with key value '0' loop through them using keycount
	// format example : kvkvkv0kv000kvkvkvk0
						while (keys && pg.dense().keys_vals(keycount) != 0) {
							std::string key = primblock.stringtable().s(pg.dense().keys_vals(keycount++));
							std::string val = primblock.stringtable().s(pg.dense().keys_vals(keycount++));
							if (node_keyfail(key,val)) goto nextnode;
							//cout << "k: " << key << " v: " << val;
						}
						node.y=lat;
						node.x=lon;
						node.id=id;
						node.keep=0;
						node.tile=tcache_get(tc[LEVEL],tile);
						// only reached when no keys fail :
						g->nnodes++; 
						if (g->nnodes % COUNTLIMIT == 0 || g->nnodes == NODES) {
							printf("%d of approximately %d nodes (latest gen -c) read (%f %%)\r", 
							g->nnodes, NODES, (g->nnodes*100.0)/NODES);
							fflush(stdout);
						}
			tp = tcache_get(tc[LEVEL],tile);
			gen_tile_add_node(g,tp,node);
						//splitref_put(g->sr,node,tile);
					nextnode:

						keycount++;
					}
					// these should match up !				
					if (keycount != keys) printf("Fail %ld and %d\n", keys, keycount);
                 //   if(pg.dense().has_denseinfo())
                        //debug("        with meta-info");

                }

                // tell about ways
                if(pg.ways_size() > 0) {
                    found_items = true;

					if (stage == HANDLING_NODES) {
						printf("\nStarted handling roads\n");
						stage = HANDLING_ROADS;
						printf("Sorting node index\n");
						//splitref_sort(g->sr);
					} else if (stage != HANDLING_ROADS) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expecting roads\n");
						exit(-3);
					}

                    //debug("      ways: %d", pg.ways_size());
                    //if(pg.ways(0).has_info())
                        //debug("        with meta-info");
					int handle=0;

					OSMPBF::Way way;
					int64_t id=0,keys=0,vals=0,refs=0;
					for (int i=0;i< pg.ways_size(); i++) {
						road_t wayroad= {0};
						way = pg.ways(i);
						id = way.id();
						keys = way.keys_size();
						vals = way.vals_size();
						wayroad.keep=0;
#ifdef FERRIES
						int ferrytest = 0;
#endif
						for (int v=0; v< vals; v++) {
							std::string key = primblock.stringtable().s(way.keys(v));
							std::string val = primblock.stringtable().s(way.vals(v));
							wayroad = road_fill(wayroad,key,val);
#ifdef FERRIES
							//if (key=="route" && val=="ferry") ferrytest++;
							//if (key=="motorcar" && val=="yes") ferrytest++;
							//if (key=="motor_vehicle" && val=="yes") ferrytest++;
#endif
							//if (key=="addr:street") printf("street : %s\n", val.c_str());
							if (key=="boundary") handle=1;
							//cout << key << " and " << val << "\n";
						}
						if (handle) {
							map<string,KeyVal *> values;
							for (int v=0; v< vals; v++) {
								string key = primblock.stringtable().s(way.keys(v));
								string val = primblock.stringtable().s(way.vals(v));
								KeyVal *kv = new KeyVal(key,val);
								values[key] = kv;
								cout << key << " and " << val << "\n";
							}
						}
#ifdef FERRIES
						if (ferrytest > 1) { 
							wayroad.type=AS_FERRY;
							wayroad.keep=1;
							printf("Set Ferry !!\n");
						}
#endif
						refs = way.refs_size();
						int64_t node=0;
						linkrefelm_t *n1=NULL;
						linkrefelm_t *n2=NULL;
						bareroad_t road= {0};
						g->nways++;

						if (handle) {
							for (n=0; n< refs; n++) {
								node += way.refs(n);
								//debug("node %d", node);
	
								nodeid = node;
								n2 = linkref_get(g->lr, -1, nodeid);
								if (wayroad.keep == 0) {
									goto nextref;
								}
								//printf("Setting %d to Keep\n", nodeid);
								g->nroads++;
								if (g->nroads % COUNTLIMIT == 0 || g->nroads == ROADS) {
									printf("%d of approximately %d (latest gen -c) roads read (%f %%)\r", 
										g->nroads, ROADS, (g->nroads*100.0)/ROADS);
										fflush(stdout);
								}
					
								if (!n2) {
									printf("Processing Error node %d not found!!\n", nodeid);
									exit(-1);
								}
					
								if (n==0) {  // initialize first node
									goto nextref;
								}
					
								// reversable attributes
								road.from= n1->id;
								//n1->nd->nroads++;
								//n1->nd->keep=1;
								road.tileto = n2->nd->tile->num;
								//road.toid = n2->id;
								road.nodeto = n2->id;
								road.dir = wayroad.dir;
								road.type=wayroad.type;
								//road.keep = 1;

							x1 = (double)n1->nd->x / 10000000.0;
							y1 = (double)n1->nd->y / 10000000.0;
							x2 = (double)n2->nd->x / 10000000.0;
							y2 = (double)n2->nd->y / 10000000.0;
							//printf("Trying %d (%d,%d)\n", n1->id,n1->x,n1->y);
							len  = haversine.distance(y1,x1,y2,x2);
							printf("%f,%f,", y1,x1);
	
								//tp = tcache_get(tc[LEVEL],n1->nd->tile);
								tp = n1->nd->tile;
								tile_add_bareroad(&gen,tp,road,len);
								road.from = n2->id;
								//n2->nd->nroads++;
								//n2->nd->keep=1;
								road.tileto = n1->nd->tile->num;
								road.nodeto = n1->id;
								// reverse if needed 
								if (road.dir == DIR_TOFROM) road.dir=DIR_FROMTO;
								else if (road.dir == DIR_FROMTO) road.dir=DIR_TOFROM;
								tp = n2->nd->tile;
								tile_add_bareroad(&gen,tp,road,len);
						nextref:
								n1=n2;
						}
					}
                }

	//tcache_dmp(tc[LEVEL]);
                // tell about relations
                if(pg.relations_size() > 0) {
                    found_items = true;
					if (stage == HANDLING_ROADS) {
						printf("Nothing to do for relations\n");
						stage = HANDLING_BOUNDS;
						break;
					} else if (stage != HANDLING_BOUNDS) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expecting bounds\n");
						exit(-3);
					}

                    debug("      relations: %d", pg.relations_size());
                    if(pg.relations(0).has_info())
                        debug("        with meta-info");
                }

                if(!found_items)
                    warn("      contains no items");
            }
        	}

        } else {
            // unknown blob type
            warn("  unknown blob type: %s", blobheader.type().c_str());
        }
	}
	printf("\n");

	//tcache_dmp(tc[LEVEL]);

	linkref_rls(g->lr);
	//tileref_flush(g->tr);

	return 0;
}

int gen_read_pbf(gen_t *g)
{
	FILE *fp = g->pbfp;
	//g->tr = tileref_ist(g,10000000);
	int nodeid,n;
    int32_t sz;
	tile_t *tp;
	g->lr = linkref_ist();
	handling stage=HANDLING_NONE;
	double x1,y1,x2,y2,len;

	tc[LEVEL] = tcache_ist(g,LEVEL,100000000,TC_WRONLY);

	while (!feof(fp)) {
    	if(fread(&sz, sizeof(sz), 1, fp) != 1) break;
    	sz = ntohl(sz);
    	if(sz > OSMPBF::max_blob_header_size)
        	cerr << "blob header too large";
    	if(fread(buffer, sz, 1, fp) != 1)
        	cerr << "unable to read blob-header from file";

        if(!blobheader.ParseFromArray(buffer, sz))
            cerr << "unable to parse blob header";
        sz = blobheader.datasize();

        if(blobheader.has_indexdata())
            debug("  indexdata = %u bytes", blobheader.indexdata().size());

        if(sz > OSMPBF::max_uncompressed_blob_size)
            err("blob-size is bigger then allowed (%u > %u)", sz, OSMPBF::max_uncompressed_blob_size);

        if(fread(buffer, sz, 1, fp) != 1)
            err("unable to read blob from file");
        if(!blob.ParseFromArray(buffer, sz))
            err("unable to parse blob");
        bool found_data = false;
        if(blob.has_raw()) {
            found_data = true;
            sz = blob.raw().size();
            if(sz != blob.raw_size())
                warn("  reports wrong raw_size: %u bytes", blob.raw_size());
            memcpy(unpack_buffer, buffer, sz);
        }

        if(blob.has_zlib_data()) {
            if(found_data)
                warn("  contains several data streams");
            found_data = true;
            sz = blob.zlib_data().size();
            z_stream z;
            z.next_in   = (unsigned char*) blob.zlib_data().c_str();
            z.avail_in  = sz;
            z.next_out  = (unsigned char*) unpack_buffer;
            z.avail_out = blob.raw_size();
            z.zalloc    = Z_NULL;
            z.zfree     = Z_NULL;
            z.opaque    = Z_NULL;

            if(inflateInit(&z) != Z_OK) {
                err("  failed to init zlib stream");
            }
            if(inflate(&z, Z_FINISH) != Z_STREAM_END) {
                err("  failed to inflate zlib stream");
            }
            if(inflateEnd(&z) != Z_OK) {
                err("  failed to deinit zlib stream");
            }

            sz = z.total_out;
        }

        if(blob.has_lzma_data()) {
            if(found_data)
                warn("  contains several data streams");
            found_data = true;
            err("  lzma-decompression is not supported");
        }

        if(!found_data)
            err("  does not contain any known data stream");

        if(blobheader.type() == "OSMHeader") {
            if(!headerblock.ParseFromArray(unpack_buffer, sz))
                err("unable to parse header block");
            if(headerblock.has_bbox()) {
                OSMPBF::HeaderBBox bbox = headerblock.bbox();
                debug("    bbox: %.7f,%.7f,%.7f,%.7f",
                    (double)bbox.left() / OSMPBF::lonlat_resolution,
                    (double)bbox.bottom() / OSMPBF::lonlat_resolution,
                    (double)bbox.right() / OSMPBF::lonlat_resolution,
                    (double)bbox.top() / OSMPBF::lonlat_resolution);
            }

            if(headerblock.has_writingprogram());
                debug("    writingprogram: %s", headerblock.writingprogram().c_str());
        }

        else if(blobheader.type() == "OSMData") {
            if(!primblock.ParseFromArray(unpack_buffer, sz))
                err("unable to parse primitive block");
            for(int i = 0, l = primblock.primitivegroup_size(); i < l; i++) {
                OSMPBF::PrimitiveGroup pg = primblock.primitivegroup(i);
                bool found_items=false;
                if(pg.nodes_size() > 0) {
                    found_items = true;

                    debug("      nodes: %d", pg.nodes_size());
					debug(" No handler for plain nodes yet !!");
					debug(" Implement ?!!");
					exit(-2);
                    //if(pg.nodes(0).has_info())
                        //debug("        with meta-info");
                }
                if(pg.has_dense()) {
                    found_items = true;
					if (stage == HANDLING_NONE) {
						printf("Started handling nodes\n");
						stage = HANDLING_NODES;
					} else if (stage != HANDLING_NODES) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expected nodes\n");
						exit(-3);
					}
					node_t node= {0};
					int64_t id=0,lat=0,lon=0,keys=0;
					int keycount=0;
		// NODE LOOP :
					for (int i=0;i< pg.dense().id_size(); i++) {
						id  += pg.dense().id(i);
						lat += pg.dense().lat(i);
						lon += pg.dense().lon(i);

						uint32_t tile = ll_2_tile(lat, lon, LEVEL);
						keys = pg.dense().keys_vals_size();
	// make sure keys exist, and since every key/val sequence is terminated 
	// with key value '0' loop through them using keycount
	// format example : kvkvkv0kv000kvkvkvk0
						while (keys && pg.dense().keys_vals(keycount) != 0) {
							std::string key = primblock.stringtable().s(pg.dense().keys_vals(keycount++));
							std::string val = primblock.stringtable().s(pg.dense().keys_vals(keycount++));
							if (node_keyfail(key,val)) goto nextnode;
							//cout << "k: " << key << " v: " << val;
						}
						node.y=lat;
						node.x=lon;
						node.id=id;
						node.keep=0;
						node.tile=tcache_get(tc[LEVEL],tile);
						// only reached when no keys fail :
						g->nnodes++; 
						if (g->nnodes % COUNTLIMIT == 0 || g->nnodes == NODES) {
							printf("%d of approximately %d nodes (latest gen -c) read (%f %%)\r", 
							g->nnodes, NODES, (g->nnodes*100.0)/NODES);
							fflush(stdout);
						}
			tp = tcache_get(tc[LEVEL],tile);
			gen_tile_add_node(g,tp,node);
						//splitref_put(g->sr,node,tile);
					nextnode:

						keycount++;
					}
					// these should match up !				
					if (keycount != keys) printf("Fail %ld and %d\n", keys, keycount);
                 //   if(pg.dense().has_denseinfo())
                        //debug("        with meta-info");

                }

                // tell about ways
                if(pg.ways_size() > 0) {
                    found_items = true;

					if (stage == HANDLING_NODES) {
						printf("\nStarted handling roads\n");
						stage = HANDLING_ROADS;
						printf("Sorting node index\n");
						//splitref_sort(g->sr);
					} else if (stage != HANDLING_ROADS) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expecting roads\n");
						exit(-3);
					}

                    //debug("      ways: %d", pg.ways_size());
                    //if(pg.ways(0).has_info())
                        //debug("        with meta-info");

					OSMPBF::Way way;
					int64_t id=0,keys=0,vals=0,refs=0;
					for (int i=0;i< pg.ways_size(); i++) {
						road_t wayroad= {0};
						way = pg.ways(i);
						id = way.id();
						keys = way.keys_size();
						vals = way.vals_size();
						wayroad.keep=0;
#ifdef FERRIES
						int ferrytest = 0;
#endif
						for (int v=0; v< vals; v++) {
							std::string key = primblock.stringtable().s(way.keys(v));
							std::string val = primblock.stringtable().s(way.vals(v));
							wayroad = road_fill(wayroad,key,val);
#ifdef FERRIES
							if (key=="route" && val=="ferry") ferrytest++;
							if (key=="motorcar" && val=="yes") ferrytest++;
							if (key=="motor_vehicle" && val=="yes") ferrytest++;
#endif
							//cout << key << " and " << val << "\n";
						}
#ifdef FERRIES
						if (ferrytest > 1) { 
							wayroad.type=AS_FERRY;
							wayroad.keep=1;
							printf("Set Ferry !!\n");
						}
#endif
						refs = way.refs_size();
						int64_t node=0;
						linkrefelm_t *n1=NULL;
						linkrefelm_t *n2=NULL;
						bareroad_t road= {0};
						g->nways++;

						for (n=0; n< refs; n++) {
							node += way.refs(n);
							//debug("node %d", node);

							nodeid = node;
							n2 = linkref_get(g->lr, -1, nodeid);
							if (wayroad.keep == 0) {
								goto nextref;
							}
							//printf("Setting %d to Keep\n", nodeid);
							g->nroads++;
							if (g->nroads % COUNTLIMIT == 0 || g->nroads == ROADS) {
								printf("%d of approximately %d (latest gen -c) roads read (%f %%)\r", 
									g->nroads, ROADS, (g->nroads*100.0)/ROADS);
									fflush(stdout);
							}
				
							if (!n2) {
								printf("Processing Error node %d not found!!\n", nodeid);
								exit(-1);
							}
				
							if (n==0) {  // initialize first node
								goto nextref;
							}
				
							// reversable attributes
							road.from= n1->id;
							//n1->nd->nroads++;
							//n1->nd->keep=1;
							road.tileto = n2->nd->tile->num;
							//road.toid = n2->id;
							road.nodeto = n2->id;
							road.dir = wayroad.dir;
							road.type=wayroad.type;
							//road.keep = 1;

						x1 = (double)n1->nd->x / 10000000.0;
						y1 = (double)n1->nd->y / 10000000.0;
						x2 = (double)n2->nd->x / 10000000.0;
						y2 = (double)n2->nd->y / 10000000.0;
						//printf("Trying %d (%d,%d)\n", n1->id,n1->x,n1->y);
						len  = haversine.distance(y1,x1,y2,x2);
	
							//tp = tcache_get(tc[LEVEL],n1->nd->tile);
							tp = n1->nd->tile;
							tile_add_bareroad(&gen,tp,road,len);
							road.from = n2->id;
							//n2->nd->nroads++;
							//n2->nd->keep=1;
							road.tileto = n1->nd->tile->num;
							road.nodeto = n1->id;
							// reverse if needed 
							if (road.dir == DIR_TOFROM) road.dir=DIR_FROMTO;
							else if (road.dir == DIR_FROMTO) road.dir=DIR_TOFROM;
							tp = n2->nd->tile;
							tile_add_bareroad(&gen,tp,road,len);
					nextref:
							n1=n2;
					}
                }

	//tcache_dmp(tc[LEVEL]);
                // tell about relations
                if(pg.relations_size() > 0) {
                    found_items = true;
					if (stage == HANDLING_ROADS) {
						printf("Nothing to do for relations\n");
						stage = HANDLING_BOUNDS;
						break;
					} else if (stage != HANDLING_BOUNDS) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expecting bounds\n");
						exit(-3);
					}

                    debug("      relations: %d", pg.relations_size());
                    if(pg.relations(0).has_info())
                        debug("        with meta-info");
                }

                if(!found_items)
                    warn("      contains no items");
            }
        	}

        } else {
            // unknown blob type
            warn("  unknown blob type: %s", blobheader.type().c_str());
        }
	}
	printf("\n");

	//tcache_dmp(tc[LEVEL]);

	linkref_rls(g->lr);
	//tileref_flush(g->tr);

	return 0;
}

int gen_split_pbf(gen_t *g)
{
	FILE *fp = g->pbfp;
	g->tr = tileref_ist(g,10000000);
	int nodeid,n;
    int32_t sz;
	g->sr = splitref_ist(g);
	handling stage=HANDLING_NONE;

	while (!feof(fp)) {
    	if(fread(&sz, sizeof(sz), 1, fp) != 1) break;
    	sz = ntohl(sz);
    	if(sz > OSMPBF::max_blob_header_size)
        	cerr << "blob header too large";
    	if(fread(buffer, sz, 1, fp) != 1)
        	cerr << "unable to read blob-header from file";

        if(!blobheader.ParseFromArray(buffer, sz))
            cerr << "unable to parse blob header";
        sz = blobheader.datasize();

        if(blobheader.has_indexdata())
            debug("  indexdata = %u bytes", blobheader.indexdata().size());

        if(sz > OSMPBF::max_uncompressed_blob_size)
            err("blob-size is bigger then allowed (%u > %u)", sz, OSMPBF::max_uncompressed_blob_size);

        if(fread(buffer, sz, 1, fp) != 1)
            err("unable to read blob from file");
        if(!blob.ParseFromArray(buffer, sz))
            err("unable to parse blob");
        bool found_data = false;
        if(blob.has_raw()) {
            found_data = true;
            sz = blob.raw().size();
            if(sz != blob.raw_size())
                warn("  reports wrong raw_size: %u bytes", blob.raw_size());
            memcpy(unpack_buffer, buffer, sz);
        }

        if(blob.has_zlib_data()) {
            if(found_data)
                warn("  contains several data streams");
            found_data = true;
            sz = blob.zlib_data().size();
            z_stream z;
            z.next_in   = (unsigned char*) blob.zlib_data().c_str();
            z.avail_in  = sz;
            z.next_out  = (unsigned char*) unpack_buffer;
            z.avail_out = blob.raw_size();
            z.zalloc    = Z_NULL;
            z.zfree     = Z_NULL;
            z.opaque    = Z_NULL;

            if(inflateInit(&z) != Z_OK) {
                err("  failed to init zlib stream");
            }
            if(inflate(&z, Z_FINISH) != Z_STREAM_END) {
                err("  failed to inflate zlib stream");
            }
            if(inflateEnd(&z) != Z_OK) {
                err("  failed to deinit zlib stream");
            }

            sz = z.total_out;
        }

        if(blob.has_lzma_data()) {
            if(found_data)
                warn("  contains several data streams");
            found_data = true;
            err("  lzma-decompression is not supported");
        }

        if(!found_data)
            err("  does not contain any known data stream");

        if(blobheader.type() == "OSMHeader") {
            if(!headerblock.ParseFromArray(unpack_buffer, sz))
                err("unable to parse header block");
            if(headerblock.has_bbox()) {
                OSMPBF::HeaderBBox bbox = headerblock.bbox();
                debug("    bbox: %.7f,%.7f,%.7f,%.7f",
                    (double)bbox.left() / OSMPBF::lonlat_resolution,
                    (double)bbox.bottom() / OSMPBF::lonlat_resolution,
                    (double)bbox.right() / OSMPBF::lonlat_resolution,
                    (double)bbox.top() / OSMPBF::lonlat_resolution);
            }

            if(headerblock.has_writingprogram());
                debug("    writingprogram: %s", headerblock.writingprogram().c_str());
        }

        else if(blobheader.type() == "OSMData") {
            if(!primblock.ParseFromArray(unpack_buffer, sz))
                err("unable to parse primitive block");
            for(int i = 0, l = primblock.primitivegroup_size(); i < l; i++) {
                OSMPBF::PrimitiveGroup pg = primblock.primitivegroup(i);
                bool found_items=false;
                if(pg.nodes_size() > 0) {
                    found_items = true;

                    debug("      nodes: %d", pg.nodes_size());
					debug(" No handler for plain nodes yet !!");
					debug(" Implement ?!!");
					exit(-2);
                    //if(pg.nodes(0).has_info())
                        //debug("        with meta-info");
                }
                if(pg.has_dense()) {
                    found_items = true;
					if (stage == HANDLING_NONE) {
						printf("Started handling nodes\n");
						stage = HANDLING_NODES;
					} else if (stage != HANDLING_NODES) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expected nodes\n");
						exit(-3);
					}
					node_t node= {0};
					int64_t id=0,lat=0,lon=0,keys=0;
					int keycount=0;
		// NODE LOOP :
					for (int i=0;i< pg.dense().id_size(); i++) {
						id  += pg.dense().id(i);
						lat += pg.dense().lat(i);
						lon += pg.dense().lon(i);

						uint32_t tile = ll_2_tile(lat, lon, LEVEL);
						keys = pg.dense().keys_vals_size();
	// make sure keys exist, and since every key/val sequence is terminated 
	// with key value '0' loop through them using keycount
	// format example : kvkvkv0kv000kvkvkvk0
						while (keys && pg.dense().keys_vals(keycount) != 0) {
							std::string key = primblock.stringtable().s(pg.dense().keys_vals(keycount++));
							std::string val = primblock.stringtable().s(pg.dense().keys_vals(keycount++));
							if (node_keyfail(key,val)) goto nextnode;
							//cout << "k: " << key << " v: " << val;
						}
						node.y=lat;
						node.x=lon;
						node.id=id;
						node.keep=0;
						// only reached when no keys fail :
						g->nnodes++; 
						if (g->nnodes % COUNTLIMIT == 0 || g->nnodes == NODES) {
							printf("%d of approximately %d nodes (latest gen -c) read (%f %%)\r", 
							g->nnodes, NODES, (g->nnodes*100.0)/NODES);
							fflush(stdout);
						}
#ifdef TILEREF
						trp = tileref_put(tr,tile);
						sprintf(buf, "node %ld %ld %ld\n", id, lat,lon);
						fwrite(buf,strlen(buf),1,trp->nodes);
#else
		append_node_cache(g,tile,id,lat,lon);
#endif
						splitref_put(g->sr,node,tile);
					nextnode:

						keycount++;
					}
					// these should match up !				
					if (keycount != keys) printf("Fail %ld and %d\n", keys, keycount);
                 //   if(pg.dense().has_denseinfo())
                        //debug("        with meta-info");

                }

                // tell about ways
                if(pg.ways_size() > 0) {
                    found_items = true;

					if (stage == HANDLING_NODES) {
						printf("\nStarted handling roads\n");
						stage = HANDLING_ROADS;
						printf("Sorting node index\n");
						splitref_sort(g->sr);
					} else if (stage != HANDLING_ROADS) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expecting roads\n");
						exit(-3);
					}

                    //debug("      ways: %d", pg.ways_size());
                    //if(pg.ways(0).has_info())
                        //debug("        with meta-info");

					OSMPBF::Way way;
					int64_t id=0,keys=0,vals=0,refs=0;
					for (int i=0;i< pg.ways_size(); i++) {
						road_t wayroad= {0};
						way = pg.ways(i);
						id = way.id();
						keys = way.keys_size();
						vals = way.vals_size();
						wayroad.keep=0;
#ifdef FERRIES
						int ferrytest = 0;
#endif
						for (int v=0; v< vals; v++) {
							std::string key = primblock.stringtable().s(way.keys(v));
							std::string val = primblock.stringtable().s(way.vals(v));
							wayroad = road_fill(wayroad,key,val);
#ifdef FERRIES
							if (key=="route" && val=="ferry") ferrytest++;
							if (key=="motorcar" && val=="yes") ferrytest++;
							if (key=="motor_vehicle" && val=="yes") ferrytest++;
#endif
							//cout << key << " and " << val << "\n";
						}
#ifdef FERRIES
						if (ferrytest > 1) { 
							wayroad.type=AS_FERRY;
							wayroad.keep=1;
							printf("Set Ferry !!\n");
						}
#endif
						refs = way.refs_size();
						int64_t node=0;
						splitrefelm_t *n1=NULL;
						splitrefelm_t *n2=NULL;
						bareroad_t road= {0};
						g->nways++;

						for (n=0; n< refs; n++) {
							node += way.refs(n);
							//debug("node %d", node);

							nodeid = node;
							n2 = splitref_get(g->sr, nodeid);
							if (wayroad.keep == 0) {
								goto nextref;
							}
							//printf("Setting %d to Keep\n", nodeid);
							g->nroads++;
							if (g->nroads % COUNTLIMIT == 0 || g->nroads == ROADS) {
								printf("%d of approximately %d (latest gen -c) roads read (%f %%)\r", 
									g->nroads, ROADS, (g->nroads*100.0)/ROADS);
									fflush(stdout);
							}
				
							if (!n2) {
								printf("Processing Error node %d not found!!\n", nodeid);
								exit(-1);
							}
							if (n2->tile < 0) {
								//free(varr);
								printf("Fail !!, node from unkown tile (id = %d) FIX NOW !!\n", nodeid);
								exit(-1); // fix now !!
							}
				
							if (n==0) {  // initialize first node
								goto nextref;
							}
				
							// reversable attributes
							road.from= n1->id;
							road.tileto = n2->tile;
							//road.toid = n2->id;
							road.nodeto = n2->id;
							road.dir = wayroad.dir;
							//road.keep = 1;
	
					append_road_cache(g,	n1->tile,n1->id,n2->tile,n2->id,wayroad.dir,wayroad.type);
							road.from = n2->id;
							road.tileto = n1->tile;
							road.nodeto = n1->id;
							// reverse if needed 
							if (road.dir == DIR_TOFROM) road.dir=DIR_FROMTO;
							else if (road.dir == DIR_FROMTO) road.dir=DIR_TOFROM;
#ifdef TILEREF
						trp = tileref_put(tr,n2->tile);
						sprintf(buf, "road2 %d to %d:%d \n", road.from, road.tileto,road.nodeto);
						fwrite(buf,strlen(buf),1,trp->roads);
#else
		append_road_cache(g,n2->tile,n2->id,n1->tile,n1->id,road.dir,wayroad.type);
#endif
					nextref:
							n1=n2;
					}
                }

                // tell about relations
                if(pg.relations_size() > 0) {
                    found_items = true;
					if (stage == HANDLING_ROADS) {
						printf("Nothing to do for relations\n");
						stage = HANDLING_BOUNDS;
						break;
					} else if (stage != HANDLING_BOUNDS) {
						printf("Order problem : ! (nodes < segments < relations)\n");
						printf("expecting bounds\n");
						exit(-3);
					}

                    debug("      relations: %d", pg.relations_size());
                    if(pg.relations(0).has_info())
                        debug("        with meta-info");
                }

                if(!found_items)
                    warn("      contains no items");
            }
        	}

        } else {
            // unknown blob type
            warn("  unknown blob type: %s", blobheader.type().c_str());
        }
	}
	printf("\n");

	splitref_rls(g->sr);
	tileref_flush(g->tr);

	return 0;
}


#ifdef NOT_USED
// should be recalculated whenever a network is edited
int gen_minmax(gen_t *g)
{
	int v;
	int32_t x, y;

	g->min_x=1800000000;
	g->max_x=-1800000000;
	g->min_y=900000000;
	g->max_y=-900000000;

	for (v=0; v< g->nnodes; v++) {
		x = g->vrt_arr[v].x;
		y = g->vrt_arr[v].y;

		if (y<g->min_y)g->min_y=y;
		if (y>g->max_y)g->max_y=y;
		if (x<g->min_x)g->min_x=x;
		if (x>g->max_x)g->max_x=x;
	}
	return 0;
}
#endif

int gen_names(gen_t *g)
{
	gen_scandir(g,1);
	tc[LEVEL] = tcache_ist(g,LEVEL,1000000000,TC_WRONLY);

	if (g->pbfp) gen_read_names_pbf(g);
	//if (g->xmlp) gen_read_xml(g);

	return 0;

	return 0;
}

void gen_statistics(gen_t *g)
{
	gen_scandir(g,0);

	if (g->pbfp) gen_count_pbf(g,0);
	else if (g->osmp) gen_read_fmt_osm(g);

	printf("Counted %d nodes\n", g->nnodes);
	printf("Counted %d roads\n", g->nroads);
}

void gen_types(gen_t *g)
{
	gen_scandir(g,0);

	if (g->pbfp) gen_count_pbf(g,1);
	else if (g->osmp) gen_read_fmt_osm(g);
}

int worklevel;

void read_all(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	offsets_t *op = *(offsets_t **)nodep;
	int level = levelinfo.level(op->tile);

	tcache_get(tc[level],op->tile);
}

int refcmp2(const void *a, const void *b)
{
	ref_t A=*(ref_t *)a;
	ref_t B=*(ref_t *)b;

	//printf("ref : %lu against %lu\n", A.id, B.id);

	if (A.id<B.id) return -1;
	if (A.id>B.id) return  1;
	return 0;
}

int refcmp(const void *a, const void *b)
{
	ref_t A=*(ref_t *)a;
	ref_t B=*(ref_t *)b;

	//printf("ref : %u against %u\n", A.id, B.id);

	if (A.id<B.id) return -1;
	if (A.id>B.id) return  1;
	return 0;
}

void fix_links(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	//int level = levelinfo.level(op->tile);
	//tile_t *tp = tcache_get(tc[level],op->tile);
	tile_create_links(nr,tp->tile);
}


void sort(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	tile_sort(tp->tile);
}

/*
void fix_externals(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	//int level = levelinfo.level(op->tile);
	//tile_t *tp = tcache_get(tc[level],op->tile);
	tile_fix_externals(nr,tp->tile);
}
*/

void fix_reindex(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	offsets_t *op = *(offsets_t **)nodep;
	int level = levelinfo.level(op->tile);
	tile_t *tp = tcache_get(tc[level],op->tile);

	//tile_dmp(tp,1);
	tile_road_sort(tp);
	if (nr) noderef_rls(nr);
	nr = noderef_ist();
	
	tile_prune(tp);
}

void pass(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	//offsets_t *op = *(offsets_t **)nodep;
	tcentry_t *op = *(tcentry_t **)nodep;

	tile_t *tp = op->tile;

	//int level = levelinfo.level(tp->num);
	//int parent = levelinfo.parent(tp->num);
	//printf("Get Tile %u on level %d parent is %d\n", op->tile, level, parent);

	//tile_t *pp = tcache_get(tc[level-1],parent);
	//printf("Found tile at %p\n", tp);
	//tile_dmp(tp,1);

	tile_pass_nodes(tp);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}

void rise(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	//offsets_t *op = *(offsets_t **)nodep;
	//tcentry_t *op = *(tcentry_t **)nodep;

	//tile_t *tp = op->tile;

	//int level = levelinfo.level(tp->num);
	//int parent = levelinfo.parent(tp->num);
	//printf("Get Tile %u on level %d parent is %d\n", op->tile, level, parent);

	//tile_t *pp = tcache_get(tc[level-1],parent);
	//printf("Found tile at %p\n", tp);
	//tile_dmp(tp,1);

	//tile_rise_nodes(tp,pp);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}

void fix(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	//printf("Found tile %d at %p\n", tp->num, tp);
	//tile_dmp(tp->tile,1);

	// until fix works :::
	tile_fix(tp->tile);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}

void nodesort(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)nodep;

	tile_node_sort(tcep->tile);
}

void link_roads(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)nodep;

	tile_link_roads(tcep->tile);
}

void roadsort(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)nodep;

	tile_road_sort(tcep->tile);
}

void prune2(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)nodep;

	tile_prune2(tcep->tile);
}

void prune(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	//printf("Found tile %d at %p\n", tp->tile->num, tp);
	//tile_dmp(tp->tile,1);

	tile_prune(tp->tile);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}

/*
void reset_links(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	//printf("Found tile %d at %p\n", tp->num, tp);
	//tile_dmp(tp->tile,1);

	tile_reset_links(tp->tile);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}
*/

void simplify(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	//offsets_t *op = *(offsets_t **)nodep;
	//int level = levelinfo.level(op->tile);
	//int parent = levelinfo.parent(op->tile);
	//printf("Get Tile %u on level %d parent is %d\n", op->tile, level, parent);

	//tile_t *tp = tcache_get(tc[level],op->tile);
	//tile_t *pp = tcache_get(tc[level-1],parent);
	//printf("Found tile at %p\n", tp);
	//tile_dmp(tp,1);

	//tile_rise_nodes(tp,pp);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}

void double_roads(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	tile_double_roads(tp->tile);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}

void reduce(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	tile_reduce(tp->tile);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}

void renum(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	tile_renum(tp->tile);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}

void join(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	tile_join(tp->tile);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}

/*
void contract(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	tile_contract(tp->tile);
	//tcache_put(tc[LEVEL], tp->tile->num, tp->tile);
}
*/

typedef struct combine_tag
{
	int32_t parent;
	int32_t children[4];
} combine_t;

int cmb_cmp(const void *a, const void *b)
{
	const combine_t *A=(const combine_t *)a;
	const combine_t *B=(const combine_t *)b;

	//printf("Testing %d against %d\n", A->parent, B->parent);
	
	if (A->parent > B->parent) return  1;
	if (A->parent < B->parent) return -1;
	return 0;
}

treemap_t *fill_combinations(tcarray_t levelcache)
{
	int t,x;
	combine_t *entry;
	combine_t *newentry;
	combine_t test={0};
	treemap_t *tm = treemap_ist(cmb_cmp);

	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		test.parent = levelinfo.parent(tp->num);
		entry = (combine_t *)treemap_get(tm,(void *)&test);
		if (entry==NULL) {
			newentry = (combine_t *)calloc(1,sizeof(combine_t));
			newentry->parent=test.parent;
			entry = (combine_t *)treemap_insert(tm,newentry);
		} else  // dereference first 
			entry = (combine_t *)entry;
		for (x=0;x<4; x++) {
			if (entry->children[x] == 0) {
				entry->children[x] = tp->num;
				break;
			}
		}
	}
	return tm;
}

int hh_step_1(gen_t *g, int level, tcarray_t *lc)
{
	tcarray_t levelcache={0};
	combine_t **combinations=NULL;
	int ncombinations= 0;
	
	tcache_walk(tc[level], nodesort);
	tcache_walk(tc[level], prune2);
	tcache_walk(tc[level], roadsort);
	tcache_prune(tc[level]); // if needed 

	if (lc) levelcache=*lc;
	else {
		if (levelcache.items) free(levelcache.items);
		levelcache = tcache_get_array(tc[level]);
	}

	treemap_t * combmap = fill_combinations(levelcache);
	ncombinations = combmap->n;
	combinations = (combine_t **)treemap_array(combmap);
	printf("Made %d combinations\n", ncombinations);

	// elevate complete chache to (be) their parents
	tc[level-1] = tcache_elevate(tc[level]);
	treemap_rls(combmap,1);
	free(combinations);

	if (levelcache.items) free(levelcache.items);
	levelcache = tcache_get_array(tc[level-1]);
	//tcache_dmp(tc[level-1]);
	tcache_hh_1(tc[level-1]);

	tcache_put_all(tc[level],level);
	tcache_put_all(tc[level-1],level-1);
	//tcache_dmp(tc[level-1]);

	return 1;
}

int combine_level(gen_t *g, int level, tcarray_t *lc)
{
	tcarray_t levelcache={0};

	//tc[level-1] = tcache_ist(g,level-1,100000000,TC_WRONLY);

	combine_t **combinations=NULL;
	int ncombinations= 0;
	
	tcache_walk(tc[level], nodesort);
	tcache_walk(tc[level], prune2);
	tcache_walk(tc[level], roadsort);

	tcache_prune(tc[level]); // if needed 

	if (lc) levelcache=*lc;
	else {
		if (levelcache.items) free(levelcache.items);
		levelcache = tcache_get_array(tc[level]);
	}

	treemap_t * combmap = fill_combinations(levelcache);
	ncombinations = combmap->n;
	combinations = (combine_t **)treemap_array(combmap);
	printf("Made %d combinations\n", ncombinations);

	tc[level-1] = tcache_elevate(tc[level]);

	//tcache_dmp(tc[level]);
	//tcache_dmp(tc[level-1]);

	//tcache_walk(tc[level-1], link_roads);
	//tcache_put_all(tc[level-1],level+99);
	//tcache_put_all(tc[level-1],level-1);

	treemap_rls(combmap,1);
	free(combinations);

	if (levelcache.items) free(levelcache.items);
	levelcache = tcache_get_array(tc[level-1]);
	tcache_dmp(tc[level-1]);
	tcache_reduce(tc[level-1]);

	tcache_put_all(tc[level],level);
	tcache_put_all(tc[level-1],level-1);
	tcache_dmp(tc[level-1]);

#ifdef NOX

	// CH generation
	if (nr) noderef_rls(nr);
	nr = noderef_ist();
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		printf("Reducing level %d %d/%d (%d)\r", l, t, levelcache.len, 
			(t*100)/levelcache.len);
		tile_contract(tp);
		selective_dump(tp);
	}
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		tile_prune(tp);
		//selective_dump(tp);
	} 
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		tile_fix_externals(nr,tp);
		selective_dump(tp);
	}
#endif
	//printf("Stage 4\n");
	//tcache_put_all(tc[level],level);

	return 1;
}

int bypass_level_new(gen_t *g, int level, tcarray_t *lc)
{
	int l=level;
	int parentid=-1;
	tile_t *tpnew=NULL;
	int t,x;
	tcarray_t levelcache={0};

	tc[level-1] = tcache_ist(g,level-1,100000000,TC_WRONLY);

	combine_t **combinations=NULL;
	int ncombinations= 0;
	if (nr) noderef_rls(nr);
	nr = noderef_ist();
	
	tcache_prune(tc[level]); // if needed 

	if (lc) levelcache=*lc;
	else {
		if (levelcache.items) free(levelcache.items);
		levelcache = tcache_get_array(tc[level]);
	}

	if (nr) noderef_rls(nr);
	nr = noderef_ist();
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		tile_double_roads(tp);
		selective_dump(tp);
	}
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		//tile_fix_externals(nr,tp);
		selective_dump(tp);
	}
	printf("Stage 1\n");
	//tcache_put_all(tc[l],l);
	tcache_t *savetc = NULL; // ayy removed (unused) tcache_dup_peered(tc[l]);

	if (nr) noderef_rls(nr);
	nr = noderef_ist();
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		//printf("Reducing level %d %d/%d (%d)\r", l, t, levelcache.len, 
			//(t*100)/levelcache.len);
		tile_reduce(tp);
		selective_dump(tp);
	}
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		//tile_fix_externals(nr,tp);
		selective_dump(tp);
	}
	printf("Stage 4\n");
	if (nr) noderef_rls(nr);
	nr = noderef_ist();
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		tile_double_roads(tp);
		selective_dump(tp);
	}
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		//tile_fix_externals(nr,tp);
		selective_dump(tp);
	}
	tcache_walk(tc[l], fix_links);
	//printf("Stage 5\n");
	//tcache_put_all(tc[l],l+99);
	//tcache_walk(savetc, dmp_peers);
	//printf("Stage 6\n");
	//tcache_walk(tc[l], dmp_peers);

	if (nr) noderef_rls(nr);
	nr = noderef_ist();
	// FIRST !! set all tiles to keep=0
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		//printf("Tile : %d\n", tp->num);
		tile_reset_keep(tp,0);
		selective_dump(tp);
	}
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		printf("Calculate pass-by level %d %d/%d (%d %%) tile %d           \r", l, t+1, levelcache.len, 
			((t+1)*100)/levelcache.len, tp->num);

		//tile_pass_nodes_9(tp);
		//tile_pass_nodes_4(tp);
		selective_dump(tp);
	}
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		tile_prune(tp);
		selective_dump(tp);
	}
	//noderef_dmp(nr);
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		//tile_fix_externals(nr,tp);
		selective_dump(tp);
	}
	tcache_walk(tc[l], fix_links);
	printf("Stage 6\n");
	if (nr) noderef_rls(nr);
	nr = noderef_ist();

	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		tile_double_roads(tp);
	}
	for (t=0; t< levelcache.len; t++) {
		//tile_t *tp = levelcache.items[t]->tile;
		//tile_fix_externals(nr,tp);
	}
	printf("Stage 7\n");
	if (nr) noderef_rls(nr);
	nr = noderef_ist();
//printf("Now shrinking \n");
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		tile_prune(tp);
	}
	for (t=0; t< levelcache.len; t++) {
		//tile_t *tp = levelcache.items[t]->tile;
		//tile_fix_externals(nr,tp);
	} 

	if (nr) noderef_rls(nr);
	nr = noderef_ist();
	tcache_walk(tc[l-1], sort);
#ifdef EFFENIET
	tcache_walk(tc[l-1],fix_externals);
#endif

	//tcache_walk(tc[l], null_peers);
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		//printf ("Fixing links for %d\n", tp->num);
		for (int n=0; n< tp->nnodes; n++) {
			node_t *low = tp->nds[n];
			node_t *peer = low->pnode;
			//printf("Peer %p\n", peer);
			if (peer) { 
			//printf("Node %d:%d has peer %d:%d\n", 
				//low->tile,low->offset, peer->tile, peer->offset);
				peer->rise=1;
				//peer->pnode=low;
				low->pnode=peer;
				if (low->x != peer->x) {
					printf("Nodes do not match between levels\n");
					exit(0);
				}
			} else 
				low->pnode=NULL;
			
		}
	}
	treemap_t * combmap = fill_combinations(levelcache);
	ncombinations = combmap->n;
	combinations = (combine_t **)treemap_array(combmap);
	printf("Made %d combinations\n", ncombinations);
	printf("Stage 6\n");
	//tcache_walk(tc[l], dmp_peers);

	for (t=0; t< ncombinations; t++) {
		//printf("%d <- [", combinations[t]->parent);
		//for (x=0; x< 4; x++) {
			//printf("%s%d", x==0?"":",",combinations[t]->children[x]);
		//}
		//printf("]\n");
		tile_t *worktiles[4];
		int T=0;
		for (x=0; x< 4; x++) {
			int32_t tileno= combinations[t]->children[x];
			if (tileno != 0) { 
				worktiles[x] = tcache_get(tc[level],tileno);
				parentid = levelinfo.parent(tileno);
				//tile_remove_doubles(worktiles[x]);
				T++;
			}
			else
				worktiles[x] = NULL;
		}
		//tpnew = tiles_add_into_parent(nr,worktiles,T,parentid);
		//tpnew = tiles_add(g,worktiles,T,parentid);
		tcache_add_tile(tc[level-1], tpnew);
	}
	treemap_rls(combmap,1);
	free(combinations);

#ifdef TRY
#error nope
	if (nr) noderef_rls(nr);
	nr = noderef_ist();
	tcache_walk(tc[l-1], reduce);
	tcache_walk(tc[l-1], fix_externals);
	tcache_walk(tc[l-1], double_roads);
	tcache_walk(tc[l-1], fix_externals);
	tcache_walk(tc[l-1], fix_links);
#endif

	//tcache_put_links(tc[l],l);
	//printf("-----1-----\n");
	//tcache_walk(tc[l], dmp_peers);
	//printf("-----2-----\n");
	//tcache_walk(tc[l-1], dmp_peers);
	//tcache_walk(tc[l], set_peers);
	//printf("-----3-----\n");
	tcache_walk(tc[l-1], reverse_peers);
	//printf("-----------\n");
	//tcache_walk(savetc, dmp_peers);

	//tcache_dmp(tc[level-1]);

	//noderef_dmp(nr);
	// first fix all externals at this level
	if (levelcache.items) free(levelcache.items);
	levelcache = tcache_get_array(tc[l-1]);
	for (t=0; t< levelcache.len; t++) {
		tile_t *tp = levelcache.items[t]->tile;
		//tile_fix_externals(nr,tp);
		selective_dump(tp);
	}
	tcache_put_all(savetc,l);
	//tcache_put_links(tc[l],l);
	//tcache_put_all(tc[l-1],l-1);
	add_stats(&g->stat, &tc[l]->stat);
	if (levelcache.items) free(levelcache.items);
	//tcache_rls(savetc);
	tc[l] = NULL;

	noderef_rls(nr);
	nr=NULL;
	if (ncombinations < 1) 
		return 0;

	return 1;
}

int bypass_level(gen_t *g, int level, tcarray_t *lc)
{
	return bypass_level_new(g,level,lc);
}

void gen_bypass(gen_t *g, int32_t tile)
{
	draw_open();

	int level = LEVEL;
	tcarray_t lc={0};
	tcarray_t *lcp=NULL;
	//g->br = bigref_ist(g);

	gen_scandir(g,3);

	// input level is falsely named 16
	tc[level] = tcache_ist(g,level+1,100000000,TC_RDWR);
	// read everything in top level
	tcache_walk_index(tc[level], read_all);

	if (tile>0) {
		//int lvl = levelinfo.level(tile);
		int32_t *children=levelinfo.children(tile);
		lc.len=1;
		lc.items = (tcentry_t **)calloc(4,sizeof(tcentry_t *));
		int i;
		for (i=0; i< 4; i++) {
			lc.items[i] = (tcentry_t *)calloc(1,sizeof(tcentry_t));
			lc.items[i]->tile = tcache_get(tc[level],children[i]);
		}
		lcp=&lc;
	}

	reset_stats(&g->stat);
	int ret=1;
	lcp=NULL;
	while (level > -1 && ret==1) {
		ret = bypass_level(g,level,lcp);
		level--;

		//break;
		//combine_level(g,level);
		//level--;
		//pass_level(g,level);
		//level--;
		//printf("Added\n");
		//dump_stats(&g->stat);
	}

	draw_close();
}

int gen_read_hh(gen_t *g)
{
	gen_scandir(g,1);
	tc[LEVEL] = tcache_ist(g,LEVEL,1000000000,TC_WRONLY);

	if (g->pbfp) gen_read_pbf(g);
	if (g->xmlp) gen_read_xml(g);

	gen_hh(g,-1);

	tcache_rls(tc[LEVEL]);
	tc[LEVEL]=NULL;
	return 0;
}

void gen_reduce(gen_t *g, int32_t tile)
{
	draw_open();

	int level = LEVEL;
	tcarray_t lc={0};
	tcarray_t *lcp=NULL;

	if (tile>0) {
		int32_t *children=levelinfo.children(tile);
		lc.len=1;
		lc.items = (tcentry_t **)calloc(4,sizeof(tcentry_t *));
		int i;
		for (i=0; i< 4; i++) {
			lc.items[i] = (tcentry_t *)calloc(1,sizeof(tcentry_t));
			lc.items[i]->tile = tcache_get(tc[level],children[i]);
		}
		lcp=&lc;
	}

	reset_stats(&g->stat);
	int ret=1;
	lcp=NULL;
	while (level > 14 && ret==1) {
		g->lr = linkref_ist();
		ret = combine_level(g,level,lcp);
		linkref_rls(g->lr);
		level--;
		//printf("Level %d\n", level);
	}

	draw_close();
}

/* generate highway hierarchy */
void gen_hh(gen_t *g, int32_t tile)
{
//	draw_open();

	int level = LEVEL;
	tcarray_t lc={0};
	tcarray_t *lcp=NULL;

	if (tile>0) { // do 1 tile
		int32_t *children=levelinfo.children(tile);
		lc.len=1;
		lc.items = (tcentry_t **)calloc(4,sizeof(tcentry_t *));
		int i;
		for (i=0; i< 4; i++) {
			lc.items[i] = (tcentry_t *)calloc(1,sizeof(tcentry_t));
			lc.items[i]->tile = tcache_get(tc[level],children[i]);
		}
		lcp=&lc;
	}

	reset_stats(&g->stat);
	int ret=1;
	lcp=NULL;
	while (level > 14 && ret==1) {
		g->lr = linkref_ist();
		ret = hh_step_1(g,level,lcp);
		linkref_rls(g->lr);
		level--;
		printf("Level %d\n", level);
	}

	//draw_close();
}

void gen_reduce_old(gen_t *g)
{
	printf("%d tiles \n", tc[LEVEL]->stat.ntiles);

	tcache_prune(tc[LEVEL]);
	//int tn = 1207803410;

	//tile_t *test = tcache_get(tc[LEVEL],tn);
	//tile_reduce(test);
	//tcache_put(tc[LEVEL], tn, test);

	tcache_walk(tc[LEVEL],reduce);
	renumber_nodes(g->br,tc[LEVEL]);
	renumber_roads(g->br,tc[LEVEL]);
	//tcache_dmp(tc[LEVEL]);
	
}

int gen_read(gen_t *g)
{
	gen_scandir(g,1);
	tc[LEVEL] = tcache_ist(g,LEVEL,1000000000,TC_WRONLY);

	if (g->pbfp) gen_read_pbf(g);
	if (g->xmlp) gen_read_xml(g);

	tcache_dmp(tc[LEVEL]);

	//tcache_walk(tc[LEVEL], nodesort);
	//tcache_walk(tc[LEVEL], prune2);
	//tcache_walk(tc[LEVEL], roadsort);

	// dump as level 17 : 
	gen_reduce(g,-1);

	tcache_rls(tc[LEVEL]);
	tc[LEVEL]=NULL;
	return 0;
}

void gen_split(gen_t *g)
{
	gen_scandir(g,1);
	tc[LEVEL] = tcache_ist(g,LEVEL,1000000000,TC_WRONLY);

	if (g->pbfp) { 
		gen_split_pbf(g);
	} else if (g->osmp) {
		printf("Split only implemented on osm.pbf files \n");
	}
	tcache_rls(tc[LEVEL]);
		tc[LEVEL]=NULL;
}

int intcmp(const void *a, const void *b)
{
	int A=*(int *)a;
	int B=*(int *)b;

	if (A<B) return -1;
	if (A>B) return  1;
	return 0;
}

tile_t *gen_random_tile(gen_t *g, rect_t bbox, int nds, int rds)
{
	//tile_t *tp = tile_ist(0);
	//tile_dmp(tp);
	return NULL;
}

rect_t *random_rect(void)
{
	rect_t *r = (rect_t *)calloc(sizeof(rect_t),1);

	// within the world : -90,-180,90,180
	// make it 'square' and minimal 10 degrees
 
	int size = net_intrand(100000000,1700000000); 
	int rest = 1800000000 - size;

	// y is 180 in total 
	r->ll.y = net_intrand(0,rest);
	r->ur.y = r->ll.y + rest;
	// x = double that : 360 
	size *=2; rest *=2;
	r->ll.x = net_intrand(0,rest);
	r->ur.x = r->ll.x + rest;

	cout << "random rect is (ll.y ll.x ur.y ur.x): " << *r << endl;

	return r;
}

void gen_random(gen_t *g, char *name)
{
// nodes will be placed on corners of a GRID (GRIDxGRID) inside the rect
// for easier viewing, if you want more detail increase GRID
#define GRID 10			
#define MINNODE 10
#define MAXNODE 20
// average roads per node
#define MINROADS 1.4
#define MAXROADS 3.5
// what level will this be on, level 1 wil be 1 tile only
#define GENLEVEL 1 

	rect_t *rect = random_rect();
	tcache_t *gtc;

	g->ibase = name;
	gen_scandir(g,3);

	int n;
	int nnodes = net_intrand(MINNODE,MAXNODE);

	node_t **nds = (node_t **) calloc(sizeof(node_t *),nnodes);

	gtc = tcache_ist(g,GENLEVEL,100000000,TC_WRONLY);

	for (n=0; n< nnodes; n++) {
		node_t *nd = (node_t *) calloc(sizeof(node_t),1);
		int x = net_intrand(1,GRID-1); // no points on the borders
		int y = net_intrand(1,GRID-1); // no points on the borders
		nd->x = (((rect->ll.x - rect->ur.x)/GRID) * x ) + rect->ll.x;
		nd->y = (((rect->ll.y - rect->ur.y)/GRID) * y ) + rect->ll.y;
		int32_t tnum = ll_2_tile(nd->y, nd->x, GENLEVEL);
		tile_t *tp = tcache_get(gtc, tnum);
		tile_add_node(tp,nd);
		nds[n] = nd;
	}

	for (n=0; n< nnodes; n++) {
		node_t *nd = nds[n];
		tile_t *tp = nd->tile;
		int nroads = net_rand(MINROADS,MAXROADS);
		printf("Trying to add %d roads\n", nroads);
		for (int r=0; r< nroads; r++) {
			road_t *rd = (road_t *)calloc(sizeof(road_t),1);
			int x = net_intrand(0,nnodes-1);
			rd->nodefrom= nd;
			rd->nodeto= nds[x];
			//rd->tileto = rd->nodeto->tile;
			tile_add_road(tp, rd);
		}
	}

	tcache_put_all(gtc,GENLEVEL);

	//gen_segments(g);
	//gen_renumber(g);
	
	//gen_erate(g);
	//gen_dump(g);
}

road_t *read_road(tile_t *tp,tcache_t *tc, FILE *fp)
{	
	road_t *r = (road_t *)calloc(sizeof(road_t),1);

	int32_t offset= io.read_uint32(fp);
	r->nodefrom = tp->nds[offset];
	int32_t tile;
	tile = io.read_uint32(fp);
	offset= io.read_uint32(fp);
	tile_t *tpto = tcache_get(tc,tile);
	r->nodeto = tpto->nds[offset];
	r->nodeto->tile = tpto;
	r->len = io.read_uint32(fp);
	r->dir = io.read_uint8(fp);
	r->type = io.read_uint8(fp);

	return r;
}

node_t *read_node(tcache_t *tc,FILE *fp)
{	
	node_t *n = (node_t *)calloc(sizeof(node_t),1);
#if !defined(SKIP_COORDS) && defined (PER_TILE)
	n->x = 0;
	n->y = 0;
#error "not using coord at this moment"
	n->x = read_uint32(fp);
	n->y = read_uint32(fp);
#endif
	//n->lastroad = io.read_uint8(fp);
	n->nroads = io.read_uint8(fp);
	//io->read_int32(fp);
	//printf("road read %d \n", n.lastroad);

	return n;
}

