#include <gen.h>

int scale_get_model(scale_t s,int world)
{
	//printf("world is %d, scale is %f\n", world, s.scale);
	int model = ((world-s.wl) / s.scale) + s.ml;

	return model;
}

int scale_get_world(scale_t s,int model)
{
	int world = ((model-s.ml) * s.scale) + s.wl;

	return world;
}

scale_t scale_recalc_scale(scale_t s)
{
	s.scale = (double)(s.wr-s.wl)/(double)(s.mr-s.ml);
	//printf("Scale is now %f\n", s.scale);

	return s;
}

scale_t scale_set(int wl, int wr, int ml, int mr)
{
	scale_t s = { wl, wr, ml, mr, 0.0 };

	s = scale_recalc_scale(s);

	return s;
}

scale_t scale_set_world(scale_t s,int wl, int wr)
{
	s.wl=wl;
	s.wr=wr;

	s = scale_recalc_scale(s);

	return s;
}

