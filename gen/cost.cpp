#include <gen.h>
#include <level.h>
#include <stdio.h>
#include <stdlib.h>

int cost_cost_cmp(const void *a, const void *b)
{
	const costentry_t *A=(const costentry_t *)a;
	const costentry_t *B=(const costentry_t *)b;

	// we need this for traverseing the DAG backwards
	// so reverse the compare 
	if (A->node->cost > B->node->cost) return -1;
	if (A->node->cost < B->node->cost) return  1;
	return 0;
}

int costlm_cmp(const void *a, const void *b)
{
	const costentry_t *A=(const costentry_t *)a;
	const costentry_t *B=(const costentry_t *)b;

	//printf("Testing %p and %p\n", A->node, B->node);
	//printf("Testing %d:%d and %d:%d\n", A->node->tile->num, A->node->offset, B->node->tile->num, B->node->offset);
	if (A->node < B->node) return -1;
	if (A->node > B->node) return  1;
	return 0;
}

/* implements the result of a edsger/dijkstra algorithm which is
	shaped as a cost if you view the points as x,y and the cost as z position
*/
cost_t *cost_ist(int sorttype)
{
	cost_t *cp = (cost_t *)calloc(sizeof(cost_t),1);
	if (!cp) return NULL;

	if (sorttype == SORT_NODE)
		cp->tm = treemap_ist(costlm_cmp);
	else if (sorttype == SORT_COST)
		cp->tm = treemap_ist(cost_cost_cmp);
	else { 
		printf("Unknown sort type\n");
		exit(-1);
	} 

	return cp;
}

void cost_walk(cost_t *cp, void (*fnc)(const void *,void *), void *udata)
{
	if (!cp) return;
	treemap_walk(cp->tm,fnc,udata);
}


void cost_free(void *nodep)
{
	free(nodep);
}

void cost_rls(cost_t *cp)
{
	if (!cp) return;

	//if (cp->elms) tdestroy(cp->elms,cost_free);
	if (cp->tm) treemap_rls(cp->tm,1);
	free(cp);
}

int cost_len(cost_t *cp)
{
	if (!cp || !cp->tm) return 0;
	return treemap_len(cp->tm);
}

costentry_t *cost_find(cost_t *cp, node_t* nd)
{
	costentry_t tce={0};
	costentry_t *tcep=NULL;
	costentry_t *thelper;

	tce.node=nd;
	thelper = &tce;

	tcep = (costentry_t *)
	treemap_find(cp->tm,thelper);
	return tcep;
}

costentry_t *cost_get(cost_t *cp, node_t *nd)
{
	costentry_t tce={0};
	costentry_t *thelper;

	tce.node=nd;
	thelper = &tce;
	return (costentry_t *)treemap_get(cp->tm,(void *) thelper);
}

void costntry_dmp(const void *nodep, void *udata)
{
	costentry_t *tp = (costentry_t *)nodep;
	printf("ce [%d] %d:%d \n", tp->cost, tp->node->tile->num, tp->node->offset);
	//wlk_tile_dmp(0, tp->node,NULL);
}

void cost_dmp(cost_t *cp)
{
	treemap_walk(cp->tm,costntry_dmp,NULL);
}

void cost_del(cost_t *cp, costentry_t *te)
{
	// Sorted !

	return treemap_del(cp->tm, te);

	//printf("Deleted --------- %d\n", te->tileno);
	//cost_dmp(cp);
	//printf("Done --------- %d\n", te->tileno);
}

void cost_set(cost_t *cp, node_t *nd, int cost)
{
	// Sorted !
	costentry_t *tcep;
	costentry_t *newp;
	newp = (costentry_t *)calloc(sizeof(costentry_t),1);

	newp->node = nd;
	newp->cost = cost;

	tcep = (costentry_t *)treemap_insert(cp->tm, newp);
	if (tcep != newp) free(newp);
}

costentry_t **cost_array(cost_t *c)
{
	costentry_t **a = (costentry_t **)treemap_array(c->tm);
	return a;
}

node_t **pathnodes(path_t *P)
{
	int n;
	node_t **nodes = (node_t **)calloc(sizeof(node_t *),P->nelms);

	for (n=0; n< P->nelms; n++) {
		nodes[n] = P->elms[n].n;
	} 
	qsort(nodes, P->nelms,sizeof(node_t *),node_cmp);
	return nodes;
}

// this tests the union between the path, the neighbourhood of s1 and
// the neighbourhood of p. It's a 3-way merge algorithm but i only count 
// the number of overlap, where a node is in all three groups
int overlap(path_t *P, cost_t *Ns1, cost_t *Np)
{
	int count;
	int l=0, r=0, p=0;
	costentry_t left, right;
	costentry_t **a1 = cost_array(Ns1);
	costentry_t **a2 = cost_array(Np);
	node_t *middle;
	int NL = cost_len(Ns1);
	int NR = cost_len(Np);
	node_t **path = pathnodes(P);
	int NP = P->nelms;

	// if any of these reached the end, they won't be equal anymore
	while (l < NL && r < NR && p < NP) {
		left = *(a1[l]);
		middle = path[p];
		right = *(a2[r]);

	//	printf("%d , %d and %d\n", left.node->offset, right.node->offset, middle->offset);

		if (costlm_cmp(&left,&right) < 0) l++;
		if (costlm_cmp(&left,&right) > 0) r++;
		if (costlm_cmp(&left,&right) == 0) {  // now test path 
			// does'nt matter which, they tested equal ill take left
			if (left.node < middle) {  l++; r++; }
			if (left.node > middle) p++;
			if (left.node == middle) p++; 
			{ 
				//all three equal , up count 
				count++;
				// and all three indices
				l++; r++; p++;
			} 
		} 
	} 
	return count;
}

void cost_add(cost_t *cp, node_t *nd, int cost)
{
	// Sorted !

	// it's actually the same 
	return cost_set(cp,nd,cost);

/* 
	costentry_t *tcep;
	tcep = (costentry_t *)calloc(sizeof(costentry_t),1);

	//printf("allocated pointer %p\n", tcep);

	tcep->node = nd;
	tcep->cost = cost;
	tcep = *(costentry_t **)tsearch((void *)tcep,&cp->elms,costlm_cmp);
	cp->nelm++;
*/

}
