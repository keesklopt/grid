#include "fcgi_stdio.h"

#include <stdlib.h>
#include <unistd.h>

#include <ego.h>
#include <curl/curl.h>
//#include <mysql/mysql.h>

// prevent from accidentily checking in the private key 
// DON'T ADD privatekey.h to the repository !!!
enum methods { TD_SINGLE, TD_MATRIX, TD_ROW, TD_COL };

#define MAX_MESSAGE_LENGTH 4096
#define BUFSIZE 4096
#define goog_url "/maps/api/geocode/json?sensor=false"

// tweaking :
// normally : all should be #undef 
#undef LEAVE_DB_OPEN 
#define MEMTESTING   

MYSQL *gconn = NULL;

typedef struct pc_entry {
	char pc[5];
	double x;
	double y;
} pc_t;

typedef struct coord_tag
{
	double lat;
	double lon;
	char *iso;
	char *fmt;
} coord_t;

typedef struct work_tag
{
	int t;
	int d;
	int c;
} work_t;

void to_urlsafe(char *orig){
	while (*orig) {
		if (*orig == '+') *orig='-';
		if (*orig == '/') *orig='_';
		orig++;
	}
}

void from_urlsafe(char *orig){
	while (*orig) {
		if (*orig == '-') *orig='+';
		if (*orig == '_') *orig='/';
		orig++;
	}
}

void web_error(char *id , int num, char *msg)
{
	char *str;
	//json_t *out = json_object();
	//json_t *cod = json_int(num);
	//json_t *mes = json_string(msg);

	json_t *out;
	json_t *err;

	//json_object_add(res, "code", cod);
	//json_object_add(res, "message", mes);
	//json_object_add(out, "error", res);
	//json_object_add(out, "result", json_null);
	//json_object_add(out, "id", json_null);

	err = json_rpc_err_ist(num, msg);
	out = json_rpc_rcv_ist(json_string(id), NULL, err);

	str = json_stringify(out);
	printf("%s", str);

	if (str) free(str);
	if (out) json_rls(out);
}

MYSQL *db_init(void)
{
	int tries = 3;
	int sleeptime=1;
	MYSQL *conn = NULL;

#ifdef LEAVE_DB_OPEN
	if (gconn) return gconn;
#endif

	while (tries > 0) {
		conn = mysql_init(NULL);
#ifdef LEAVE_DB_OPEN
		gconn = conn;
#endif
		if (conn != NULL) {
      		//printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
	
			if (mysql_real_connect(conn, "217.77.130.135", "deal", 
          		"deal", "deal", 0, NULL, 0) != NULL) {
				return conn;
    		}
			sleep(sleeptime);
			sleeptime*=2;
			tries --;
        	printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
  		}
	}

	return NULL;
}

void db_exit(MYSQL *conn)
{
#ifndef LEAVE_DB_OPEN
	mysql_close(conn);
	conn=NULL;
#endif
}

static size_t handle_coord(void *ptr, size_t memberSize, size_t nofMembers, void *userdata) {
	buf_t *stretchbuf=(buf_t *)userdata;

	buf_fwrite(ptr, memberSize, nofMembers, stretchbuf);

	//coord_t *ret= (coord_t *)userdata;
	//printf("Got %d and %d \n", (int)memberSize, (int)nofMembers);
	return nofMembers*memberSize;
}

json_t * ptv_get_tsp(coord_t *locs, int nlocs)
{
	CURL *curlHandle;
	CURLcode res;
	char url[BUFSIZE]={0};
	//char *data;
	buf_t *buf =buf_ist(0);
	int l;
	json_t *route = json_array();

	snprintf(url, BUFSIZE, "http://217.77.130.48:50050/xsequence/pages/deal_tsp.jsp?");

	if (nlocs<1) return NULL;
	char subbuf[100];
	
	sprintf(subbuf, "%f,%f", locs[0].lon, locs[0].lat);
	strcat(url, subbuf);
	for (l=1; l< nlocs; l++) {
		sprintf(subbuf, "%f,%f", locs[l].lon, locs[l].lat);
		strcat(url, ",");
		strcat(url, subbuf);
	}

	curlHandle = curl_easy_init();

	curl_easy_setopt(curlHandle, CURLOPT_URL, url);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, handle_coord);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, buf);
	res = curl_easy_perform(curlHandle);

	if (res) {
		goto cleanup;
	}
	//data = buf_strdup(buf);
	buf_rls(buf);

cleanup:
	curl_easy_cleanup(curlHandle); 

	return route;
}

work_t ptv_get_td(double x1, double y1, double x2, double y2)
{
	CURL *curlHandle;
	CURLcode res;
	char url[BUFSIZE]={0};
	char *data;
	buf_t *buf =buf_ist(0);
	work_t ret= {-1,-1,-1};

	snprintf(url, BUFSIZE, "http://217.77.130.135:50030/xroute/samples/td.jsp?%f,%f,%f,%f", x1,y1,x2,y2);

	curlHandle = curl_easy_init();

	curl_easy_setopt(curlHandle, CURLOPT_URL, url);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, handle_coord);
	curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, buf);
	res = curl_easy_perform(curlHandle);

	if (res) {
		//web_error(id, -1,"Could not read google coordinates");
		return ret;
	}
	data = buf_strdup(buf);
	ego_strip(data);
	buf_rls(buf);

	curl_easy_cleanup(curlHandle); 

	char *tok = strtok(data, ",");
	if (tok) ret.d = atof(tok);
	tok = strtok(NULL, ",");
	if (tok) ret.t = atof(tok);

	return ret;
}

char *db_get_countrycode(char *cc)
{
	char qry[BUFSIZE];
	char *ret=NULL;
	
	int nf=0;
	MYSQL_ROW row;
	MYSQL_RES *result;
	CURL *curlHandle;

	MYSQL *conn = db_init();

	if (!conn) return ego_strdup("faulty_code"); // make the call fail 

	curlHandle = curl_easy_init();
	char *escdata=NULL;
	escdata = curl_easy_unescape(curlHandle, cc,0,NULL);
	snprintf(qry, BUFSIZE, "SELECT iso from synonyms WHERE name='%s'", escdata);
	curl_free(escdata);
	curl_easy_cleanup(curlHandle); 

	mysql_query(conn,qry);
	result = mysql_store_result(conn);
	nf=mysql_num_fields(result);

	while ((row = mysql_fetch_row(result)))
  	{
		ret = ego_strdup(row[0]);
		break;
  	}

	if (result) 
		mysql_free_result(result);

	db_exit(conn);
	return ret;
}

void db_free_coord(coord_t coord)
{
	if (coord.fmt) free(coord.fmt);
	if (coord.iso) free(coord.iso);
}

coord_t db_get_coord(char *cc, char *pc)
{
	char qry[BUFSIZE];
	int nf=0;
	coord_t ret={400.0,400.0}; /// biggest is 180.0 90.0
	MYSQL_ROW row;
	MYSQL_RES *result;

	MYSQL *conn = db_init();

	if (!conn) return ret;

	snprintf(qry, BUFSIZE, "SELECT lat,lon,matched,country from postcode WHERE country='%s' AND pc='%s'", cc, pc);

	mysql_query(conn,qry);
	result = mysql_store_result(conn);
	nf=mysql_num_fields(result);

	while ((row = mysql_fetch_row(result)))
  	{
		ret.lat = atof(row[0]);
		ret.lon = atof(row[1]);
		ret.fmt = ego_strdup(row[2]);
		ret.iso = ego_strdup(row[3]);
		break;
  	}

	mysql_free_result(result);
	
	db_exit(conn);
	return ret;
}
	
work_t db_get_work(coord_t from, coord_t to)
{
	char qry[BUFSIZE];
	int nf=0;
	work_t ret={-1,-1,-1};
	MYSQL *conn;
	MYSQL_ROW row;
	MYSQL_RES *result;

	conn = db_init();

	snprintf(qry, BUFSIZE, "SELECT meters,seconds from work WHERE fromx='%f' AND fromy='%f' AND tox='%f' AND toy='%f'", from.lon, from.lat, to.lon, to.lat);

	mysql_query(conn,qry);
	result = mysql_store_result(conn);
	nf=mysql_num_fields(result);
	while ((row = mysql_fetch_row(result)))
  	{
		ret.d = atoi(row[0]);
		ret.t = atoi(row[1]);
		break;
  	}
	
	if (result) 
		mysql_free_result(result);
	db_exit(conn);
	return ret;
}

void db_put_coord(char *cc, char *pc, coord_t coord)
{
	char qry[BUFSIZE]={0};
	char quot[BUFSIZE]={0};
	//MYSQL_ROW row;
	//MYSQL_RES *result;

	MYSQL *conn = db_init();

	if (!conn) return; // sorry no database

	mysql_real_escape_string(conn, quot, coord.fmt, strlen(coord.fmt));

	snprintf(qry, BUFSIZE, "INSERT INTO postcode (country,pc,lat,lon,matched) VALUES ('%s','%s','%f','%f','%s')", cc,pc,coord.lat, coord.lon,quot );

	mysql_query(conn,qry);
	db_exit(conn);
	// i tried !!
}


void db_put_work(coord_t from, coord_t to, work_t work)
{
	char qry[BUFSIZE];
	MYSQL *conn = db_init();

	snprintf(qry, BUFSIZE, "INSERT INTO work (fromx,fromy,tox,toy,meters,seconds) VALUES ('%f','%f','%f','%f','%d','%d')", from.lon,from.lat,to.lon,to.lat,work.d,work.t);

	mysql_query(conn,qry);
	db_exit(conn);
	// i tried !!
}

void read_matrix(void)
{
	FILE *pcs = fopen("postcode.csv", "r");
	char line[BUFSIZE];

	while (fgets(line,BUFSIZE,pcs) != NULL) {
		printf("read %s\n", line);
	}

	fclose(pcs);
}

void init(void)
{
	// do one time init here 
	mysql_library_init(0,NULL,NULL);
#ifdef LEAVE_DB_OPEN
#error No
#else
	gconn = db_init();
#endif
	//read_matrix();
}

void web_put_coord(char *id, coord_t c)
{
	char *str;
	json_t *out;
	//json_t err = json_object();
	json_t *res = json_object();
	json_t *lat = json_double(c.lat);
	json_t *lon = json_double(c.lon);
	json_t *fmt = json_string(c.fmt);
	json_t *iso = json_string(c.iso);

	json_object_add(res, "lat", lat);
	json_object_add(res, "lon", lon);
	json_object_add(res, "matched", fmt);
	json_object_add(res, "iso2", iso);

	out = json_rpc_rcv_ist(json_string(id),res,NULL);

	str = json_stringify(out);
	printf("%s", str);
	free(str);
	json_rls(out);
}

void web_put_tdmatrix(char *id, json_t *matrix)
{
	char *str;
	json_t *out;
	json_t *res = json_object();

	json_object_add(res, "matrix", matrix);

	out = json_rpc_rcv_ist(json_string(id),res,NULL);
	str = json_stringify(out);
	json_rls(out);
	printf("%s", str);
	free(str);
}

void web_put_td(char *id, work_t c)
{
	char *str;
	json_t *out;
	json_t *res = json_object();
	json_t *time = json_int(c.t);
	json_t *dist = json_int(c.d);

	json_object_add(res, "seconds", time);
	json_object_add(res, "meters", dist);

	out = json_rpc_rcv_ist(json_string(id),res,NULL);

	str = json_stringify(out);
	printf("%s", str);
}

void sorting(char *qry)
{
	static char erbuf[2048];
	json_t *content;
	json_t *ident;
	json_t *orders;
	json_t *method;
	json_t *params;

	char *subdir;
	subdir=strtok(NULL, "=");
	if (!subdir || strcmp(subdir, "json")) {
		web_error(NULL, -2,"only json parameter expected");
		//web_error(NULL, -2,subdir);
		return;
	}
	subdir=strtok(NULL, "\n");
	//printf("Content is %s\n", subdir);
	
	content = json_parse(subdir);
	if (!content) {
		sprintf(erbuf, "Could not parse json : %s", subdir);
		web_error(NULL, -12, erbuf);
		return;
	}

	ident = json_object_get(content, "id");
	if (!ident) { // version 2 also allows this 
		ident= json_null();
	}
	char *id = ident->val.s;
	method = json_object_get(content, "method");
	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a params member , json was : %s", subdir);
		web_error(id, -10,erbuf);
		return;
	}

	orders = json_object_get(params, "orders");
	if (!orders) {
		web_error(id, -11,"missing parameter : orders");
		return;
	}

	int t;
	int len = json_array_len(orders);
	for (t=0; t< len; t++) {
		;
	}

	//web_put_coord(id,coord);
}

json_t * make_cluster(coord_t *tlocs, int tnlocs, coord_t *olocs, int onlocs)
{
	//char *data;
	//buf_t *buf =buf_ist(0);
	int l;
	json_t *route = json_array();

	for (l=1; l< tnlocs; l++) {
		printf("%f,%f", tlocs[l].lon, tlocs[l].lat);
	}

	return route;
}

void cluster(char *qry)
{
	static char erbuf[2048];
	json_t *content;
	json_t *ident;
	json_t *tloc;
	json_t *oloc;
	json_t *lon;
	json_t *lat;

	json_t *params;

	char *subdir;
	subdir=strtok(NULL, "=");
	if (!subdir || strcmp(subdir, "json")) {
		web_error(NULL, -2,"only json parameter expected");
		goto cleanup;
	}
	subdir=strtok(NULL, "\n");
	printf("Content is %s\n", subdir);
	
	content = json_parse(subdir);
	if (!content) {
		sprintf(erbuf, "Could not parse json \"%s\"", subdir);
		web_error(NULL, -12, erbuf);
		goto cleanup;
	}

	ident = json_object_get(content, "id");
	if (!ident) { // version 2 also allows this 
		ident= json_null();
	}
	char *id = ident->val.s;

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a 'params' array member , json was : \"%s\"", subdir);
		web_error(id, -10,erbuf);
		goto cleanup;
	}

	coord_t *trucklocations = (coord_t *)calloc(sizeof(coord_t),1);
	int tlocs=0;
	tloc  = json_array_get(params, tlocs);
	while (tloc) {
		lat = json_object_get(tloc, "lat");
		lon = json_object_get(tloc, "lon");
		if (!lon || !lat) {
			web_error(id, -21,"missing parameter: at least provide lat and long ");
			goto cleanup;
		}
		trucklocations[tlocs].lat = lat->val.d;
		trucklocations[tlocs].lon = lon->val.d;

		tlocs++;
		trucklocations = (coord_t *)realloc(trucklocations,sizeof(coord_t)*(tlocs+1));
		tloc  = json_array_get(params, tlocs);
	}

	coord_t *orderlocations = (coord_t *)calloc(sizeof(coord_t),1);
	int olocs=0;
	oloc  = json_array_get(params, tlocs);
	while (oloc) {
		lat = json_object_get(oloc, "lat");
		lon = json_object_get(oloc, "lon");
		if (!lon || !lat) {
			web_error(id, -22,"missing parameter: at least provide lat and long ");
			goto cleanup;
		}
		orderlocations[olocs].lat = lat->val.d;
		orderlocations[olocs].lon = lon->val.d;

		olocs++;
		orderlocations = (coord_t *)realloc(orderlocations,sizeof(coord_t)*(olocs+1));
		oloc  = json_array_get(params, olocs);
	}

	json_t *cluster=NULL;
	cluster = make_cluster(trucklocations,tlocs,orderlocations,olocs);
	web_put_tdmatrix(id,cluster);

cleanup:
	if (content) json_rls(content);
}

void matrixentry(char *qry)
{
	static char erbuf[2048];
	json_t *content;
	json_t *ident;
	json_t *loc;
	json_t *p;
	json_t *c;
	json_t *lon;
	json_t *lat;

	json_t *method;
	json_t *params;

	char *subdir;
	subdir=strtok(NULL, "=");
	if (!subdir || strcmp(subdir, "json")) {
		web_error(NULL, -2,"only json parameter expected");
		//web_error(NULL, -2,subdir);
		return;
	}
	subdir=strtok(NULL, "\n");
	//printf("Content is %s\n", subdir);
	
	content = json_parse(subdir);
	if (!content) {
		sprintf(erbuf, "Could not parse json \"%s\"", subdir);
		web_error(NULL, -12, erbuf);
		return;
	}

	ident = json_object_get(content, "id");
	if (!ident) { // version 2 also allows this 
		ident= json_null();
	}
	char *id = ident->val.s;
	method = json_object_get(content, "method");

	int m = TD_SINGLE; // fallback
	if (method) {
		if (strcmp(method->val.s,"single") == 0) m=TD_SINGLE;
		if (strcmp(method->val.s,"matrix") == 0) m=TD_MATRIX;
		if (strcmp(method->val.s,"row") == 0) m=TD_ROW;
		if (strcmp(method->val.s,"column") == 0) m=TD_COL;
		if (strcmp(method->val.s,"col") == 0) m=TD_COL;
	}

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a 'params' array member , json was : \"%s\"", subdir);
		web_error(id, -10,erbuf);
		return;
	}

	// let's start with 2
	coord_t *locations = (coord_t *)calloc(sizeof(coord_t),1);
	int locs=0;

	loc  = json_array_get(params, locs);
	while (loc) {
		c = json_object_get(loc, "country");
		p = json_object_get(loc, "postcode");
		lat = json_object_get(loc, "lat");
		lon = json_object_get(loc, "lon");
		if (!lon || !lat) {
			if (!c || !p) {
				web_error(id, -21,"missing parameter: at least provide lat and long or country and postcode");
				return;
			}
			char *pc = p->val.s;
			ego_strip(pc);
	
			//locations[locs] = pc_2_coord(id,cc,pc);
		} else {
			locations[locs].lat = lat->val.d;
			locations[locs].lon = lon->val.d;
		}
		locs++;
		locations = (coord_t *)realloc(locations,sizeof(coord_t)*(locs+1));
		loc  = json_array_get(params, locs);
	}
	if (locs < 2) {
		web_error(id, -22,"param: need at leas two locations");
		return;
	}

	json_t *route=NULL;
	route = ptv_get_tsp(locations,locs);

	web_put_tdmatrix(id,route);
}
void tsp(char *qry)
{
	static char erbuf[2048];
	json_t *content;
	json_t *ident;
	json_t *loc;
	json_t *p;
	json_t *c;
	json_t *lon;
	json_t *lat;

	json_t *method;
	json_t *params;

	char *subdir;
	subdir=strtok(NULL, "=");
	if (!subdir || strcmp(subdir, "json")) {
		web_error(NULL, -2,"only json parameter expected");
		//web_error(NULL, -2,subdir);
		return;
	}
	subdir=strtok(NULL, "\n");
	//printf("Content is %s\n", subdir);
	
	content = json_parse(subdir);
	if (!content) {
		sprintf(erbuf, "Could not parse json \"%s\"", subdir);
		web_error(NULL, -12, erbuf);
		return;
	}

	ident = json_object_get(content, "id");
	if (!ident) { // version 2 also allows this 
		ident= json_null();
	}
	char *id = ident->val.s;
	method = json_object_get(content, "method");

	int m = TD_SINGLE; // fallback
	if (method) {
		if (strcmp(method->val.s,"single") == 0) m=TD_SINGLE;
		if (strcmp(method->val.s,"matrix") == 0) m=TD_MATRIX;
		if (strcmp(method->val.s,"row") == 0) m=TD_ROW;
		if (strcmp(method->val.s,"column") == 0) m=TD_COL;
		if (strcmp(method->val.s,"col") == 0) m=TD_COL;
	}

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a 'params' array member , json was : \"%s\"", subdir);
		web_error(id, -10,erbuf);
		return;
	}

	// let's start with 2
	coord_t *locations = (coord_t *)calloc(sizeof(coord_t),1);
	int locs=0;

	loc  = json_array_get(params, locs);
	while (loc) {
		c = json_object_get(loc, "country");
		p = json_object_get(loc, "postcode");
		lat = json_object_get(loc, "lat");
		lon = json_object_get(loc, "lon");
		if (!lon || !lat) {
			if (!c || !p) {
				web_error(id, -21,"missing parameter: at least provide lat and long or country and postcode");
				return;
			}
			//char *cc = c->val.s;
			char *pc = p->val.s;
			ego_strip(pc);
	
			//locations[locs] = pc_2_coord(id,cc,pc);
		} else {
			locations[locs].lat = lat->val.d;
			locations[locs].lon = lon->val.d;
		}
		locs++;
		locations = (coord_t *)realloc(locations,sizeof(coord_t)*(locs+1));
		loc  = json_array_get(params, locs);
	}
	if (locs < 2) {
		web_error(id, -22,"param: need at leas two locations");
		return;
	}

	json_t *route=NULL;
	route = ptv_get_tsp(locations,locs);

	web_put_tdmatrix(id,route);
}

void td(char *qry)
{
	static char erbuf[2048];
	json_t *content;
	json_t *ident;
	json_t *loc;
	json_t *p;
	json_t *c;
	json_t *lon;
	json_t *lat;
	int x, y;

	json_t *method;
	json_t *params;

	char *subdir;
	subdir=strtok(NULL, "=");
	if (!subdir || strcmp(subdir, "json")) {
		web_error(NULL, -2,"only json parameter expected");
		//web_error(NULL, -2,subdir);
		return;
	}
	subdir=strtok(NULL, "\n");
	//printf("Content is %s\n", subdir);
	
	content = json_parse(subdir);
	if (!content) {
		sprintf(erbuf, "Could not parse json \"%s\"", subdir);
		web_error(NULL, -12, erbuf);
		return;
	}

	ident = json_object_get(content, "id");
	if (!ident) { // version 2 also allows this 
		ident= json_null();
	}
	char *id = ident->val.s;
	json_rls(ident);
	method = json_object_get(content, "method");

	int m = TD_SINGLE; // fallback
	if (method) {
		if (strcmp(method->val.s,"single") == 0) m=TD_SINGLE;
		if (strcmp(method->val.s,"matrix") == 0) m=TD_MATRIX;
		if (strcmp(method->val.s,"row") == 0) m=TD_ROW;
		if (strcmp(method->val.s,"column") == 0) m=TD_COL;
		if (strcmp(method->val.s,"col") == 0) m=TD_COL;
	}

	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a 'params' array member , json was : \"%s\"", subdir);
		web_error(id, -10,erbuf);
		goto cleanup;
	}

	// let's start with 2
	coord_t *locations = (coord_t *)calloc(sizeof(coord_t),1);
	int locs=0;

	loc  = json_array_get(params, locs);
	while (loc) {
		c = json_object_get(loc, "country");
		p = json_object_get(loc, "postcode");
		lat = json_object_get(loc, "lat");
		lon = json_object_get(loc, "lon");
		if (!lon || !lat) {
			if (!c || !p) {
				web_error(id, -21,"missing parameter: at least provide lat and long or country and postcode");
				return;
			}
			//char *cc = c->val.s;
			char *pc = p->val.s;
			ego_strip(pc);
	
			//locations[locs] = pc_2_coord(id,cc,pc);
		} else {
			locations[locs].lat = lat->val.d;
			locations[locs].lon = lon->val.d;
		}
		locs++;
		locations = (coord_t *)realloc(locations,sizeof(coord_t)*(locs+1));
		loc  = json_array_get(params, locs);
	}
	if (locs < 2) {
		web_error(id, -22,"param: need at leas two locations");
		return;
	}

	int yl, xl, skipempties=1;
	y=0;
	if (m==TD_SINGLE) {
		yl=1;
		xl=2;
	} else 
	if (m==TD_MATRIX) {
		yl=locs;
		xl=locs;
		skipempties=0; // keep matrix 'square'
	} else 
	if (m==TD_ROW) {
		yl=1;
		xl=locs;
	} else 
	if (m==TD_COL) {
		yl=locs;
		xl=1;
		y=1;
	}
	json_t *matrix = json_array();
	for (; y< yl; y++) {
		coord_t from = locations[y];
		json_t *row = json_array();
		for (x=0; x< xl; x++) {
			if (skipempties && x==y) continue;
			coord_t to = locations[x];
			work_t work = db_get_work(from,to);
			json_t *col = json_object();
			if (work.t< 0) {
				work = ptv_get_td(from.lon,from.lat,to.lon,to.lat);
				if (work.t< 0) {
					//printf("Could not resolve coordinate\n");
					char msg[1024];
					sprintf(msg, "Could not calculate combination : (%f,%f) to (%f,%f)", from.lon, from.lat, to.lon, to.lat);
					web_error(id, -26,msg);
					return;
				} else {
					db_put_work(from,to,work);
				}
			}
			json_t *time = json_int(work.t);
			json_t *dist = json_int(work.d);
			json_object_add(col, "seconds", time);
			json_object_add(col, "meters", dist);
			json_array_add(row,col);
		}
		json_array_add(matrix,row);
	}
	web_put_tdmatrix(id,matrix);

cleanup:
	if (locations) free(locations);
	if (content) json_rls(content);
}

void web_get(char *qry)
{
	printf("only rest supported right now\n");
	return;
}

void dispatch_post(char *qry)
{
	static char erbuf[2048];
	json_t *content;
	json_t *ident;
	json_t *method;
	json_t *params;

	content = json_parse(qry);
	if (!content) {
		sprintf(erbuf, "Could not parse json : %s", qry);
		web_error(NULL, -12, erbuf);
		return;
	}

	printf("Succeeded parsing !!");

	ident = json_object_get(content, "id");
	if (!ident) { // version 2 also allows this 
		ident= json_null();
	}
	char *id = ident->val.s;
	method = json_object_get(content, "method");
	params = json_object_get(content, "params");
	if (!params) {
		sprintf(erbuf, "json rpc message needs a params member , json was : %s", qry);
		web_error(id, -10,erbuf);
		return;
	}
}

void web_post(char *qry)
{
	CURL *curlHandle;
	printf("received a post request: %s \n", qry);

	curlHandle = curl_easy_init();
	char *escdata=NULL;
	escdata = curl_easy_unescape(curlHandle, qry,0,NULL);
	ego_plus_2_space(escdata);
	printf("escaped post data: \"%s\"\n", escdata);

	curl_free(escdata);
	curl_easy_cleanup(curlHandle); 

	return dispatch_post(escdata);
}

void web_agents(char *qry)
{
	char *subdir;
	subdir=strtok(NULL, "/");
	if (subdir && !strcmp(subdir, "matrixentry")) {
		matrixentry(qry);
	} else
	if (subdir && !strcmp(subdir, "sorting")) {
		sorting(qry);
	} else
	if (subdir && !strcmp(subdir, "tw")) {
		printf("TW");
	} else {
		printf("unknown command %s\n", subdir);
	}
}

void web_data(char *qry)
{
	char *subdir;
	subdir=strtok(NULL, "/");
	if (subdir && !strcmp(subdir, "geocode")) {
		printf("TW");
	} else 
	{
		printf("unknown command %s\n", subdir);
	}
}

void web_rest(char *dir, char *qry)
{
	char *subdir;
	subdir=strtok(dir, "/");
	//printf("sub found %s\n", subdir);
	if (subdir && !strcmp(subdir, "agents")) {
		web_agents(qry);
	} else {
			if (subdir && !strcmp(subdir, "data")) {
				web_data(qry);
			} else 
			{
				printf("unknown command %s\n", subdir);
			}
	}
}

int main(int c, char *v[])
{
	init();

	while (FCGI_Accept () >= 0) {
		printf ("Content-type: text/html\r\n\r\n");
		char *qry = getenv("QUERY_STRING");
		char *pi = getenv("PATH_INFO");
		// test for GET or POST or REST
		if (pi!=NULL && pi[0] != '\0') {
			web_rest(pi,qry);
		} else {
			if (qry!=NULL && qry[0] == '\0') { // POST
            	printf ("method is POST<br>");
            	char *cl = getenv ("CONTENT_LENGTH");
				printf("Content length is %s\n", cl);
            	int sz = atoi (cl); 
            	qry = calloc (sz,1);
            	fread (qry, sz, 1, stdin);
				if (!qry) web_error(NULL,-7,"no POST command found");
				else web_post(qry);
        	} else { // GET
            	printf ("method is GET<br>");
				if (!qry) web_error(NULL,-8,"no GET command found");
				else web_get(qry);
        	}
		}
#ifdef MEMTESTING
	// mysql tempts to leave stuff open internally
	// only enable this on testing
		mysql_library_end();
		printf("closed mysql library !");
#endif
	}

	return 0;
}
