#include <stdio.h>
#include <stdlib.h>
#include <net.h>

int main(int argc, char *argv[])
{
	slice_t s;

	calcsteps(16);

	if (argc < 2 || argc > 4) {
		printf("Usage : %s tilenumber | level lat lon\n", argv[0]);
	}
	
	if (argc ==2) {
		s = tile_2_xy(NULL,atol(argv[1]),15);
		printf("level %d, %d, %d\n", s.level, s.x, s.y);
		return 0;
	}

	//s = xy_2_tile(NULL,atol(argv[1]),1,1);
	//printf("level %d, %d, %d\n", s.level, s.x, s.y);
	return 0;
}
