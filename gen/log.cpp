#include <grid.h>
#include <stdarg.h>

int loglevel;

int lprintf(int level, char *fmt, ...)
{
    int ret=0;
    va_list vl;

    va_start(vl, fmt);

    /* this overrules all, even lprintf(LOG_ALL, xx) won't pass */
    if (loglevel == LOG_SILENT) return ret;

    if (level >= loglevel) {
        if (level < 1 || level > 3) return ret;
        ret = vprintf(fmt, vl);
    }

    return ret;
}
