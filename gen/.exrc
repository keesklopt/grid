if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
inoremap <F5> :%!xxd
nmap gx <Plug>NetrwBrowseX
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#NetrwBrowseX(expand("<cWORD>"),0)
noremap <F4> :make^M
vnoremap <F5> :%!xxd
nnoremap <F5> :%!xxd
let &cpo=s:cpo_save
unlet s:cpo_save
set backspace=indent,eol,start
set completeopt=preview
set fileencodings=ucs-bom,utf-8,latin1
set helplang=en
set makeprg=scons
set printoptions=paper:a4
set ruler
set runtimepath=~/.vim,/var/lib/vim/addons,/usr/share/vim/vimfiles,/usr/share/vim/vim73,/usr/share/vim/vimfiles/after,/var/lib/vim/addons/after,~/.vim/after
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc
set tabstop=4
set termencoding=utf-8
" vim: set ft=vim :
