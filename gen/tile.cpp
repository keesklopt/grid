#include <gen.h>
#include <draw.h>
#include <level.h>
#include <map>
#include <vector>
#include <limits.h>

extern tcache_t *tc[16];
extern noderef_t *nr;
extern noderef_t *reverse;

extern GtkDraw d;

#define MAXHOP 10

#define H 10

FILE *drawfile=NULL;
typedef struct contract_tag
{
	node_t *nd;
	road_t *to;
	road_t *back;
	int32_t score;
	int32_t len;
} contract_t;


// NL
//#define TESTTILES(num) (num == 324946878 || num == 1299787486)

#define USE_TYPES 1

#define opposite_dir(dir) (dir==DIR_TOFROM) ? DIR_FROMTO : (dir == DIR_TOFROM) ? DIR_FROMTO : DIR_BOTH
#define is_reverse_dir(rd1,rd2) ((rd1->dir==DIR_TOFROM && rd2->dir == DIR_FROMTO) || (rd1->dir == DIR_FROMTO && rd2->dir == DIR_TOFROM) || (rd1->dir == DIR_BOTH && rd2->dir == DIR_BOTH ))

#define is_backroad(tp,rd1,rd2,nd) (rd2->nodeto->offset==nd->offset && rd2->nodeto->tile==tp&& is_reverse_dir(rd1,rd2))

#define SINGLE_MIDPOINT(nd) (nd->nroads==2 && is_reverse_dir(nd->rds[0],nd->rds[1]))

#define BACKROAD()

int gen_nodecmp(const void * a, const void *b)
{
	node_t *A= (node_t *)a;
	node_t *B= (node_t *)b;
	if (A->tile == B->tile && A->offset == B->offset)  return 0;
	return -1;
} 

int gen_node_getcost(const void *nd)
{
	node_t *node = (node_t *)nd;
	return node->cost;
}

void gen_node_setcost(const void *nd, int cost)
{
	node_t *node = (node_t *)nd;
	node->cost = cost;
}

int gen_node_getcost2(const void *nd)
{
	node_t *node = (node_t *)nd;
	return node->cost2;
}

void gen_node_setcost2(const void *nd, int cost)
{
	node_t *node = (node_t *)nd;
	node->cost2 = cost;
}

heapfnc_t gen_heap_fncs = { gen_nodecmp, gen_node_getcost, gen_node_setcost } ;
heapfnc_t gen_heap_fncs2 = { gen_nodecmp, gen_node_getcost2, gen_node_setcost2 } ;

int roadcost(road_t road)
{
	//int cost = (road.len*360)/speeds[(road.type)];
	int cost = road.len;

	//cost = road.len;
	//cost = road.len*speeds[(road.type)];
	//cost = road.len*100;

	//printf("cost : %d and %d is %d\n", road.len, speeds[road.type], cost);
	if (cost < 1) { 
		printf("GOT 0 cost !!\n");
		cost = 1;
	}
	return cost;
}

rect_t xy_2_ll(point_t xy, int level)
{
	int bx=3600000000UL; 
	int by=1800000000; 
	int tilefx=xy.x; int tilefy=xy.y; 
	int fax=0; int fay=0;
	rect_t r={{0,0},{0,0}};
	int mask;

	mask = 1 << (level);
	printf("mask is %d (level %d)\n", mask, level);

	while (level >0) {
		/* printf("level %d %d %d\n", level, lx, bx); */
		bx/=2;
		by/=2;
		tilefx <<=1;
		tilefy <<=1;
		if (tilefx & mask) {
			fax += bx;
		}
		if (tilefy & mask) {
			fay += by;
		}
		level--;
	}

	r.ll.x = fax - 1800000000; 
	r.ll.y = fay - 900000000;
	r.ur.x = r.ll.x + bx;
	r.ur.y = r.ll.y + by;

	return r;
}

int32_t ll_2_tile(int32_t lat, int32_t lon, int32_t level)
{
	point_t pt;

	pt = levelinfo.ll_2_xy(lat, lon, level);
	int num = levelinfo.calctile(pt.x,pt.y,level);
	return num;
}

node_t *node_dup(node_t *orig)
{
	node_t *nn = (node_t *)calloc(sizeof(node_t),1);

	// copy all static content 
	*nn=*orig;

	// do not copy roads array, refill it 
	nn->rds = NULL;

	// resty should be done manually , keep old values for now
	return nn;
}

tile_t *tile_ist(int32_t tnum)
{
	tile_t *tp = (tile_t *)calloc(sizeof(tile_t),1);

	//printf("Creating %d %p\n", tnum);

	tp->nds= NULL;
	tp->rds= NULL;
	tp->nroads=0;
	tp->nnodes=0;
	tp->num = tnum;
	tp->dirty = 1;
	tp->level = levelinfo.level(tnum);
	tp->parent = levelinfo.parent(tnum);

	return tp;
}

void tile_rls(tile_t *tp)
{
	if (!tp) return;
	//printf("Deleting tile %d with %p %p %p %p\n", tp->num, tp, tp->nds, tp->rds, tp);

	if (tp->nds) {
		for (int n=0; n< tp->nnodes; n++) {
			node_t *nd = tp->nds[n];
			if (nd->rds) free(nd->rds);
			free(nd);
		}
		free(tp->nds);
	}
	if (tp->rds) {
		free(tp->rds);
	}
	free(tp);
}

// promote all references to links
void tile_link (tile_t *tp)
{
	for (int n=0; n< tp->nnodes; n++) {
		printf("Now handling node %d, %d\n", n, tp->nds[n]->offset);
	}
	for (int r=0; r< tp->nroads; r++) {
	}
}

tile_t *tile_dup (tile_t *orig,int newnum)
{
	tile_t *dup = tile_ist(orig->num);

	memcpy(dup,orig,sizeof(tile_t));
	dup->num=newnum;
	// duplicate all nodes
	dup->nds = (node_t **)calloc(orig->nnodes,sizeof(node_t *));
	for (int n=0; n< orig->nnodes; n++) {
		dup->nds[n]= (node_t *)calloc(sizeof(node_t),1);
		memcpy(dup->nds[n],orig->nds[n],sizeof(node_t));
		dup->nds[n]->rds = (road_t **)calloc(sizeof(road_t *),orig->nds[n]->nroads);
	}
	// duplicate all roads
	dup->rds = (road_t **)calloc(orig->nroads,sizeof(road_t *));
	for (int r=0; r< orig->nroads; r++) {
		dup->rds[r]= (road_t *)calloc(sizeof(road_t),1);
		memcpy(dup->rds[r],orig->rds[r],sizeof(road_t));
	}
	// nodes rds array and roads nodefrom and nodeto now contain
	// pointers to the original tile, replace those 
	// these are only the internal tiles
	for (int r=0; r< orig->nroads; r++) {
		if (orig->rds[r]->nodeto->tile==orig) 
			dup->rds[r]->nodeto->tile = dup;
	}

	return dup;
}

void tile_relink (tile_t *tnew, tile_t *orig)
{
	//tile_dmp(orig, 1);
	for (int n=0; n< orig->nnodes; n++) {
		node_t *o = orig->nds[n];
		for (int r=0; r< o->nroads; r++) {
			for (int x=0; x< orig->nroads; x++) {
				if (orig->rds[x] == orig->nds[n]->rds[r]) {
					//printf("Match %d == %d\n", x, r); 
					tnew->nds[n]->rds[r]= tnew->rds[x];
					break;
				}
			}
		}
	}
	for (int r=0; r< orig->nroads; r++) {
		road_t *o=orig->rds[r];
		for (int n=0; n< orig->nnodes; n++) {
			if (orig->nds[n] == o->nodefrom) 
				tnew->rds[r]->nodefrom = tnew->nds[n];
		}
		
	}
}

int tile_remove_doubles(tile_t *tp)
{
	int t;
	int fix=0;
	road_t prev={NULL,NULL};

	for (t=0; t< tp->nroads; t++) {
		road_t *rd = tp->rds[t];
		if (rd->nodefrom==prev.nodefrom &&
		    rd->nodeto==prev.nodeto &&
		    rd->nodeto->tile==prev.nodeto->tile && 
			rd->keep==1) {
			printf("Double road %d:%d to %d:%d detected and deleted\n", tp->num,
				rd->nodefrom->offset, rd->nodeto->tile->num, rd->nodeto->offset);
			rd->keep=0;
			fix=1;
		}
		prev=*rd;
	}
	if (fix) tile_prune(tp);
	return 1;
}

int tile_check(tile_t *tp)
{
	int t;

	for (t=0; t< tp->nroads; t++) {
		road_t rd = *tp->rds[t];
		if (!rd.done) return 0;
		if (rd.len == 0)  {	 
			printf("Unresolved length %d:%d to %d:%d\n", tp->num,rd.nodefrom->offset,rd.nodeto->tile->num,rd.nodeto->offset);
			rd.len=1;
			// could be data error : 
			/*  luxembourg.osm example :
			    <nd ref="311169866"/>
        		<nd ref="311169846"/>
        		<nd ref="311169846"/>
			*/
		}
	}
	return 1;
}


void node_draw(int o, node_t nd,int8_t extra)
{
	if (!nd.keep) return;
	io.write_uint8(o, drawfile, 'n');
	io.write_comment(o, drawfile, ",");
	io.write_uint32(o, drawfile, nd.tile->num);
	io.write_comment(o, drawfile, ",");
	io.write_uint32(o, drawfile, nd.offset);
	io.write_comment(o, drawfile, ",");
	io.write_uint8(o, drawfile, extra);

}

void road_draw(int o, tile_t *tp, int r,int8_t extra)
{
	io.write_uint8(o, drawfile, 'r');
	io.write_comment(o, drawfile, ",");
	io.write_uint32(o, drawfile, tp->num);
	io.write_comment(o, drawfile, ",");
	io.write_uint32(o, drawfile, r);
	io.write_comment(o, drawfile, ",");
	io.write_uint8(o, drawfile, extra);
}

void tile_draw(tile_t *tp)
{
	int t;
	int o= OUT_BINARY;

	printf("Tile dmp %d: (%d nodes/%d roads)\n", tp->num, tp->nnodes, tp->nroads);

	for (t=0; t< tp->nnodes; t++) {
		node_t nd = *tp->nds[t];
		node_draw(o,nd,10);
	}
	for (t=0; t< tp->nroads; t++) {
		road_t rd = *tp->rds[t];
		if (!rd.keep) continue;
		road_draw(o,tp,t,10);
	}
}

void draw_open(void)
{
	if (drawfile) return;
	drawfile = fopen("grid_draw", "w");
} 

void draw_close(void)
{
	if (drawfile)
		fclose(drawfile);
}

void tile_dmp(tile_t *tp, int verbose)
{
	int t;
	printf("Tile dmp %d: (%d nodes/%d roads)\n", tp->num, tp->nnodes, tp->nroads);
	if (!verbose) return;
	for (t=0; t< tp->nnodes; t++) {
		node_t *nd = tp->nds[t];
		printf("[%d] %p %d:%d [%d], (%d,%d) k%d p %p \n   ", nd->offset, nd, nd->tile->num,nd->offset, nd->nroads, nd->x, nd->y, nd->keep, nd->pnode);
		for (int r=0; r< nd->nroads; r++) 
			printf("[%d-%d]", nd->rds[r]->nodefrom->offset,nd->rds[r]->nodeto->offset);
		printf("\n");
	}
	printf("(%d roads)\n", tp->nroads);
	for (t=0; t< tp->nroads; t++) {
		road_t *rd = tp->rds[t];
		const char *dir = "<->";
		if (rd->dir == DIR_FROMTO) dir=" ->";
		if (rd->dir == DIR_TOFROM) dir="<- ";
		printf("[%d] %p %d %s %d:%d, type %d, keep %d, len %d done (%d)\n", t, rd->nodefrom,rd->nodefrom->offset, dir, rd->nodeto->tile->num,rd->nodeto->offset, rd->type, rd->keep, rd->len,rd->done);
	}
}

void wlk_tile_dmp(intptr_t size, const void *elm, void *extra)
{
	tile_t *tp = (tile_t *)elm;
	int p = TESTTILES(tp->num);
	if (p) tile_dmp(tp,1);
	//tile_dmp(tp,1);
}

/*
void tile_fix_externals(noderef_t *nr, tile_t *tp)
{
	int r,n;

	//printf("Fixing %d\n", tp->num);
	//tile_dmp(tp,1);
	for (n=0; n< tp->nnodes; n++) {
		tp->nds[n]->tile=tp;
		tp->nds[n]->offset=n;
	}
	for (r=0; r< tp->nroads; r++) {
		road_t *rd = tp->rds[r];	
		if (rd->nodeto->tile->num != tp->num) {
			//printf("Fixing external reference\n");
			noderefelm_t *found = noderef_get(nr,rd->nodeto->tile->num,rd->nodeto->offset);
			if (!found) {
				// NOTE, this is NOT an error, it could be a correct external
				// to the same level, it will not be in the noderef then

				printf("Could not find %d:%d from tile %d in node reference\n", rd->nodeto->tile->num, rd->nodeto->offset, tp->num);
				tile_dmp(tp,1);
				noderef_dmp(nr);
				exit(-2);
			} else {
			//printf("altering %d:%d to %d:%d\n", found->prevtile, found->prevnode, found->tile, found->node);
				rd->nodeto->tile->num = found->tile;
				tile_t *tileto = tcache_get(tc[tp->level],rd->nodeto->tile->num);
				rd->nodeto = tileto->nds[found->node];
			}
		}
	}
	tile_reindex_roads(tp);

	//printf ("Fixed !!???\n");
	//tile_dmp(tp,1);
}
*/

// sort linked nodes first
/* 
int pqentry_cmp(const void *a, const void *b)
{
	pqentry_t *A=(pqentry_t *)a;
	pqentry_t *B=(pqentry_t *)b;

	if (A->cost > B->cost) return  1;
	if (A->cost < B->cost) return -1;
	//if (A->nd->id > B->nd->id) return  1;
	//if (A->nd->id < B->nd->id) return -1;
	return 0;
}
*/

int contract_cmp(const void *a, const void *b)
{
	node_t *A=*(node_t **)a;
	node_t *B=*(node_t **)b;

	//printf("%d against %d\n",  A->contract_cost, B->contract_cost);
	if (A->contract_cost < B->contract_cost) return  1;
	if (A->contract_cost > B->contract_cost) return -1;
	//if (A->nd->id > B->nd->id) return  1;
	//if (A->nd->id < B->nd->id) return -1;
	return 0;
}

int linknode_cmp(const void *a, const void *b)
{
	node_t *A=(node_t *)a;
	node_t *B=(node_t *)b;

	if (A->keep == 0 && B->keep == 1) return 1;
	if (A->keep == 1 && B->keep == 0) return -1;
	if (A->pnode == NULL && B->pnode != NULL) return 1;
	if (A->pnode != NULL && B->pnode == NULL) return -1;
	if (A->y > B->y) return  1;
	if (A->y < B->y) return -1;
	if (A->x > B->x) return  1;
	if (A->x < B->x) return -1;
	return 0;
}

int node_cmp(const void *a, const void *b)
{
	node_t *A=*(node_t **)a;
	node_t *B=*(node_t **)b;

	if (A->keep == 0 && B->keep == 1) return 1;
	if (A->keep == 1 && B->keep == 0) return -1;
	if (A->y > B->y) return  1;
	if (A->y < B->y) return -1;
	if (A->x > B->x) return  1;
	if (A->x < B->x) return -1;
	return 0;
}

int road_cmp(const void *a, const void *b)
{
	road_t *A=*(road_t **)a;
	road_t *B=*(road_t **)b;

	//printf("%d against %d\n", A->nodefrom->offset, B->nodefrom->offset);
	//printf("%d:%d against %d:%d\n", A->nodeto->tile, A->nodeto->offset,  B->nodeto->tile, B->nodeto->offset);
	if (A->nodefrom->offset > B->nodefrom->offset) return  1;
	if (A->nodefrom->offset < B->nodefrom->offset) return -1;

	if (A->nodeto->tile > B->nodeto->tile) return  1;
	if (A->nodeto->tile < B->nodeto->tile) return -1;
	if (A->nodeto->offset > B->nodeto->offset) return  1;
	if (A->nodeto->offset < B->nodeto->offset) return -1;
	return 0;
}

void tile_dmp_peers(tile_t *tp)
{
	int n;
	
	for (n=0; n< tp->nnodes; n++) {
		node_t *peer = tp->nds[n]->pnode;
		if (peer) {
		printf("Dmp %d:%d thats %d:%d %d %d\n", tp->nds[n]->tile->num, tp->nds[n]->offset, peer->tile->num, peer->offset, tp->nds[n]->keep, peer->keep);
			peer = peer->pnode;
			if (peer) 
				printf("Dmp %d:%d %d:%d %d %d\n", tp->nds[n]->tile->num, tp->nds[n]->offset, peer->tile->num, peer->offset, tp->nds[n]->keep, peer->keep);
		} else 
			printf("Dmp %d:%d no peer \n", tp->nds[n]->tile->num, tp->nds[n]->offset);
	}
}

/* combined tile now has a plink to the higher original tile, but we need 
   the reverse link for writing uplink, so that's waht this function does 
*/
void tile_reverse_peers(tile_t *tp)
{
	int n;
	
	for (n=0; n< tp->nnodes; n++) {
		if (tp->nds[n]->pnode == NULL) {
			printf("PROBLEM \n");
			exit(0);
		}
		tp->nds[n]->pnode->pnode=tp->nds[n];
		//printf("Set to %p thats %d:%d\n", tp->nds[n].pnode, tp->nds[n].tile, tp->nds[n].offset);
	}
}

void tile_set_peers(tile_t *tp)
{
	int n;
	
	for (n=0; n< tp->nnodes; n++) {
		tp->nds[n]->pnode=tp->nds[n]->pnode->pnode;
		printf("Set to %p thats %d:%d\n", tp->nds[n]->pnode, tp->nds[n]->tile->num, tp->nds[n]->offset);
	}
}

void tile_null_peers(tile_t *tp)
{
	int n;
	
	for (n=0; n< tp->nnodes; n++) {
		tp->nds[n]->pnode=NULL;
	}
}

void tile_reset_join(tile_t *tp)
{
	int n;
	
	for (n=0; n< tp->nnodes; n++) {
		node_t *nd = tp->nds[n];
		if (SINGLE_MIDPOINT(nd)) {
			nd->join=1;
		} else 
			nd->join=0;
	}
}

void tile_reset_keep(tile_t *tp,int keep)
{
	int r;
	int n;
	
	for (r=0; r< tp->nroads; r++) {
		tp->rds[r]->keep=keep;
		//tp->rds[r].cost=0;
	}
	for (n=0; n< tp->nnodes; n++) {
		tp->nds[n]->keep=keep;
		tp->nds[n]->cost=HEAP_CLEAR;
		tp->nds[n]->eval=0;
		tp->nds[n]->slack=-1;
		tp->nds[n]->active=0;
		tp->nds[n]->p=0;
	}
	tp->offset = 0;
	//tp->dirty=0;
}

void tile_check_reset(tile_t *tp)
{
	int r;
	int n;

	//if (tp->dirty==0) return;

	//printf("checking %d\n", tp->num);
	
	for (r=0; r< tp->nroads; r++) {
	//	tp->rds[r].keep=1;
		if (tp->rds[r]->cost != roadcost(*tp->rds[r]))
			printf("Wrong road cost %d:%d %d\n", tp->num, r, tp->rds[r]->cost);
//#define ROADCOST(road) ((road.len)/speeds[road.type])
	}
	for (n=0; n< tp->nnodes; n++) {
		//tp->nds[n].keep=1;
		if ( tp->nds[n]->cost != HEAP_CLEAR)
			printf("Wrong node cost %d:%d\n", tp->num, n);
		if (tp->nds[n]->hop!=0)
			printf("Wrong hop %d:%d\n", tp->num, n);
		if (tp->nds[n]->eval!=0)
			printf("Wrong eval %d:%d\n", tp->num, n);
	}
	tp->offset = 0;
	//tp->dirty=0;
}

void tile_reset(tile_t *tp)
{
	int r;
	int n;

	if (tp->dirty==0) { 
		//printf("Skipping tile %d\n", tp->num);
		return;
	} else {
		//printf("Really cleaning tile %d\n", tp->num);
	}
	
	for (r=0; r< tp->nroads; r++) {
	//	tp->rds[r].keep=1;
		tp->rds[r]->cost=roadcost(*tp->rds[r]);
//#define ROADCOST(road) ((road.len)/speeds[road.type])
	}
	for (n=0; n< tp->nnodes; n++) {
		//tp->nds[n].keep=1;
		tp->nds[n]->cost=HEAP_CLEAR;
		tp->nds[n]->slack=-1;
		tp->nds[n]->eval=0;
		tp->nds[n]->hop=0;
		tp->nds[n]->active=0;
		tp->nds[n]->p=0;
		//tp->nds[n]->a=0;
		//tp->nds[n]->d=0;
		tp->nds[n]->depth=0;
	}
	tp->offset = 0;
	tp->dirty=0;
}

void tile_reset2(tile_t *tp)
{
	int r;
	int n;

	if (tp->dirty==0) { 
		//printf("Skipping tile %d\n", tp->num);
		return;
	} else {
		//printf("Really cleaning tile %d\n", tp->num);
	}
	
	for (r=0; r< tp->nroads; r++) {
	//	tp->rds[r].keep=1;
		tp->rds[r]->cost=roadcost(*tp->rds[r]);
//#define ROADCOST(road) ((road.len)/speeds[road.type])
	}
	for (n=0; n< tp->nnodes; n++) {
		//tp->nds[n].keep=1;
		tp->nds[n]->cost2=HEAP_CLEAR;
		tp->nds[n]->eval2=0;
		tp->nds[n]->hop=0;
		//tp->nds[n]->active=0;
		//tp->nds[n]->a=0;
		//tp->nds[n]->d=0;
		tp->nds[n]->depth=0;
	}
	tp->offset = 0;
	tp->dirty=0;
}

void level_reset2(int l)
{
	tcache_reset2(tc[l]);
}

void level_reset_keep(int l, int k)
{
	tcache_reset_keep(tc[l],k);
}

void level_reset(int l)
{
	tcache_reset(tc[l]);
}

void level_check_reset(int l)
{
	printf("Doing check\n");
	tcache_check_reset(tc[l]);
}

/* ye olde
void tile_pruneold(tile_t *tp)
{
	int n,r;
	int offset=0;

	// renumber all nodes 'in place' 
	for (n=0; n< tp->nnodes; n++) {
		node_t *nd = &tp->nds[n];

		if (nd->keep == 1) {
			//noderefelm_t *ref = noderef_get(nr, nd->tile, nd->node);
			//printf("Altering result %d:%d to %d:%d\n", 
				//nd->tile, nd->node, nd->tile, offset);
			noderef_set(nr,nd->tile, nd->offset, nd->tile, offset);
			nd->offset = offset++;
		} else 
			nd->offset = -11;
	}

	printf("--------- 1 -------\n");
	tile_dmp(tp,1);
	// use the new index in each node to renumber roads 
	// and shrink roads 
	offset=0;
	for (r=0; r< tp->nroads; r++) {
		road_t *rd = &tp->rds[r];
		if (rd->keep == 0) continue;
		rd->nodefrom = tp->nds[rd->nodefrom].offset;
		if (rd->nodeto->tile == tp->num) 
			rd->nodeto = tp->nds[rd->nodeto].offset;
		else { // also change references in externally connected tiles
			//tile_t *external = tcache_get(tc,rd->nodeto->tile);
			//int er;
			//for (er=0; er<external->nroads; er++) {
				//road_t *extroad= &external->rds[er];
				//if (extroad->nodeto->tile == tp->num) 
					//extroad->nodeto = tp->nds[extroad->nodeto].node;
			//tile_dmp(external,1);
		}
		tp->rds[offset++] = *rd;
	}
	tp->rds = (road_t *)realloc(tp->rds,offset*sizeof(road_t));
	tp->nroads = offset;

	// then shrink nodes and renumber roads 
	offset=0;
	for (n=0; n< tp->nnodes; n++) {
		node_t nd = tp->nds[n];
		if (nd.keep == 1) 
			tp->nds[offset++] = nd;
	}
	tp->nds = (node_t *)realloc(tp->nds,offset*sizeof(node_t));
	tp->nnodes = offset;
	printf("--------- 2 -------\n");
	tile_dmp(tp,1);
}
*/

/* make sure there are no nodes without roads etc */
void tile_validate(tile_t *tp)
{
	int r,n;
	int fixed=0;
	
	//printf("--------- -1 -------\n");
	//tile_dmp(tp,1);
	while(!fixed) {
		fixed=1; // what a positive attitude
		// first mark 'empty' nodes
		for (n=0; n< tp->nnodes; n++) {
			node_t *nd = tp->nds[n];
			nd->eval=0;
		}
		for (r=0; r< tp->nroads; r++) {
			road_t *rd = tp->rds[r];
			if (rd->keep==0) continue;
			node_t *from = rd->nodefrom;
			from->eval=1;
			node_t *to = NULL;
			if (from->keep==0) { 
				//printf("Fixing road %d , it's from node is deleted\n", rd->nodefrom);
				if (rd->keep==1) fixed=0;
				rd->keep=0;
			}
			if (rd->nodeto->tile->num == tp->num) {
				to = rd->nodeto;
				to->eval=1;
				if (to->keep==0) {
				//printf("Fixing road %d , it's to node is deleted\n", rd->nodeto);
					if (rd->keep==1) fixed=0;
					rd->keep=0;
				}
			}
		}
		// this just lets the roads's keepstate dictate what to keep
		for (n=0; n< tp->nnodes; n++) {
			node_t *nd = tp->nds[n];
			if (nd->keep!=nd->eval) fixed=0;
			nd->keep=nd->eval;
			//printf("Fixing node %d to %d, it has no roads\n", nd->offset, nd->eval);
		}
	}
}

void tile_prune2(tile_t *tp)
{
	int n,r;
	int offset=0;

	int p = TESTTILES(tp->num);

	tile_validate(tp);
	if (p) printf("--------- 0 -------\n");
	if (p) tile_dmp(tp,1);
	/* renumber all nodes 'in place' */
	for (n=0; n< tp->nnodes; n++) {
		node_t *nd = tp->nds[n];

		if (nd->keep == 1) {
			nd->offset = offset++;
		} else 
			nd->offset = -11;
	}

	if (p) printf("--------- 1 -------\n");
	if (p) tile_dmp(tp,1);
	/* use the new index in each node to renumber roads */
	/* and shrink roads */
	offset=0;
	for (r=0; r< tp->nroads; r++) {
		road_t *rd = tp->rds[r];
		if (rd->keep == 0) continue;
		if (rd->nodefrom->offset < 0 || 
			(rd->nodeto->tile->num == tp->num && 
		    rd->nodeto->offset < 0)) { 
			printf("Wrong reference nodefrom %d or nodeto %d error\n", 	rd->nodefrom->offset, rd->nodeto->offset);
			tile_dmp(tp,1);
			exit(-11); // i regard this as fatal error at current
			rd->keep=0;
		}
		tp->rds[offset++] = rd;
	}
	//tp->rds = (road_t *)realloc(tp->rds,offset*sizeof(road_t));
	tp->nroads = offset;

	/* then shrink nodes and renumber roads */
	offset=0;
	for (n=0; n< tp->nnodes; n++) {
		node_t *nd = tp->nds[n];
		if (nd->keep == 1) 
			tp->nds[offset++] = nd;
	}
	tp->nds = (node_t **)realloc(tp->nds,offset*sizeof(node_t *));
	tp->nnodes = offset;
	if (p) printf("--------- 2 -------\n");
	if (p) tile_dmp(tp,1);
	if (p) printf("--------- 3 -------\n");
	if (p) tile_dmp(tp,1);
	//tile_reindex_roads(tp);
	if (p) printf("--------- 4 -------\n");
	if (p) tile_dmp(tp,1);
}

void tile_prune(tile_t *tp)
{
	int n,r;
	int offset=0;

	int p = TESTTILES(tp->num);

	tile_validate(tp);
	if (p) printf("--------- 0 -------\n");
	if (p) tile_dmp(tp,1);
	/* renumber all nodes 'in place' */
	for (n=0; n< tp->nnodes; n++) {
		node_t *nd = tp->nds[n];

		if (nd->keep == 1) {
			//noderef_set(nr,nd->tile, nd->offset, nd->tile, offset);
			nd->offset = offset++;
		} else 
			nd->offset = -11;
	}

	if (p) printf("--------- 1 -------\n");
	if (p) tile_dmp(tp,1);
	/* use the new index in each node to renumber roads */
	/* and shrink roads */
	offset=0;
	for (r=0; r< tp->nroads; r++) {
		road_t *rd = tp->rds[r];
		if (rd->keep == 0) continue;
		if (rd->nodefrom->offset < 0 || 
			(rd->nodeto->tile->num == tp->num && 
		    rd->nodeto->offset < 0)) { 
			printf("Wrong reference nodefrom %d or nodeto %d error\n", 	rd->nodefrom->offset, rd->nodeto->offset);
			tile_dmp(tp,1);
			exit(-11); // i regard this as fatal error at current
			rd->keep=0;
		}
		tp->rds[offset++] = rd;
	}
	//tp->rds = (road_t *)realloc(tp->rds,offset*sizeof(road_t));
	tp->nroads = offset;

	/* then shrink nodes and renumber roads */
	offset=0;
	for (n=0; n< tp->nnodes; n++) {
		node_t nd = *tp->nds[n];
		if (nd.keep == 1) 
			*tp->nds[offset++] = nd;
	}
	tp->nds = (node_t **)realloc(tp->nds,offset*sizeof(node_t *));
	tp->nnodes = offset;
	if (p) tile_dmp(tp,1);
	if (p) printf("--------- 2 -------\n");
	if (p) tile_dmp(tp,1);
	tile_reindex_roads(tp);
	if (p) printf("--------- 3 -------\n");
	if (p) tile_dmp(tp,1);
}

/* AFTER this function always renumber all external referenceing tiles
	using the generated noderef !!!
*/
void tile_fix(tile_t *tp)
{
	int n,r;
	int offset=0;

	/* resort nodes on xy */
	//printf("--------- fix 1 -------\n");
	//tile_dmp(tp,1);

	/* renumber all nodes 'in place' */
	for (n=0; n< tp->nnodes; n++) {
		node_t *nd = tp->nds[n];

		if (nd->keep == 1) {
			noderef_set(nr,nd->tile->num, nd->offset, nd->tile->num, offset);
			nd->offset = offset++;
		} else 
			nd->offset = -11;
	}

	// LATER until then this just tile_prune !!!
	//printf("--------- fix 0 -------\n");
	//tile_dmp(tp,1);
	//tile_node_sort(tp);

	//printf("--------- fix 2 -------\n");
	//tile_dmp(tp,1);

	/* use the new index in each node to renumber roads */
	/* and shrink roads */
	offset=0;
	for (r=0; r< tp->nroads; r++) {
		road_t *rd = tp->rds[r];
		if (rd->keep == 0) continue;
		//rd->nodefrom = tp->nds[rd->nodefrom]->offset;
		//if (rd->nodeto->tile == tp->num)  {
			//rd->nodeto = tp->nds[rd->nodeto]->offset;
		//}
		tp->rds[offset++] = rd;
	}
	tp->rds = (road_t **)realloc(tp->rds,offset*sizeof(road_t *));
	tp->nroads = offset;

	tile_road_sort(tp);
	//printf("--------- fix 3 -------\n");
	//tile_dmp(tp,1);

	/* then shrink nodes and renumber roads */
	offset=0;
	for (n=0; n< tp->nnodes; n++) {
		node_t nd = *tp->nds[n];
		if (nd.keep == 1) 
			*tp->nds[offset++] = nd;
	}
	tp->nds = (node_t **)realloc(tp->nds,offset*sizeof(node_t *));
	tp->nnodes = offset;
	//printf("--------- fix 4 -------\n");
	//tile_dmp(tp,1);
	tile_reindex_roads(tp);
	//printf("--------- fix 5 -------\n");
	//tile_dmp(tp,1);
}
/*
void tile_reset_links(noderef_t *nr,tile_t *tp)
{
	int r;
	noderefelm_t *nep;

	//printf("reset tile %d\n", tp->num);

	for (r=0; r<tp->nroads; r++) {
		road_t *rp = tp->rds[r];

		nep = noderef_get(nr, rp->nodeto->tile->num, rp->nodeto->offset);
		if (nep) {
			//printf("Changing %d:%d into %d:%d\n", 
				//rp->nodeto->tile, rp->nodeto, nep->tile, nep->node);
			rp->nodeto->tile->num=nep->tile;
			tile_t *tileto = tcache_get(tc[tp->level],rp->nodeto->tile->num);
			rp->nodeto=tileto->nds[nep->node];
		}
	}
}
*/

void tile_create_links(noderef_t *nr, tile_t *tp)
{
	//printf("%d of %d (%d %%) nodes and %d of %d (%d %%) roads kept\n", 
			//nkeep, tp->nnodes, (nkeep*100)/tp->nnodes, 
			//rkeep, tp->nroads, (rkeep*100)/tp->nroads);
	//printf("Finally --- \n");
	//tile_dmp(tp,1);

	//noderef_dmp(nr);
}

/* 
	calculate which nodes/roads in a tile can be bypassed 
	altogether
*/
void tile_pass_nodes(tile_t *tp)
{
	int r;
	int n;
	int extnodes=0;
	int introads=0;
	int lowroads=0;
	
	int thislevel=tp->level;
	// can't realloc NULL, start with 1
	node_t **exts= (node_t **)calloc(1,sizeof(node_t*));

	//printf("Contracting %d \n", tp->num);
	tile_reset_keep(tp,1);
	//tile_dmp(tp,1);

	for (n=0; n< tp->nnodes; n++) {
		node_t *nd = tp->nds[n];
		for (r=0; r< nd->nroads; r++) {
			road_t *rp = nd->rds[r];
			int tolevel = levelinfo.level(rp->nodeto->tile->num);
			if (rp->nodeto->tile != tp) {
				if (tolevel != thislevel) lowroads++;
				else { 	
					extnodes++;
					exts = (node_t **)realloc(exts, sizeof(node_t *)*extnodes);
					exts[extnodes-1] = nd;
				}
			} else {
				introads++;
			}
		}
	}
	//printf("Tile %d , %d internal %d external and %d low roads\n", 
			//tp->num, introads, extnodes, lowroads);

	int x,y;
	// alternate reset, we need keep to begin as 0
	for (n=0; n< tp->nnodes; n++) {
		tp->nds[n]->keep=0;
		tp->nds[n]->cost=HEAP_CLEAR;
		tp->nds[n]->eval=0;
	}

	for (r=0; r< tp->nroads; r++) {
		if (tp->rds[r]->nodeto->tile == tp) {
			tp->rds[r]->keep=0;
		} else {
			tp->rds[r]->keep=1; // save external roads
			tp->rds[r]->nodefrom->keep=1;
		}
		//tp->rds[r].cost=0;
	}
	for (x=0; x< extnodes; x++) {
		//tile_dmp(tp,1);
		// reset eval only !
		for (n=0; n< tp->nnodes; n++) {
			tp->nds[n]->eval=0;
			tp->nds[n]->cost=HEAP_CLEAR;
		}

		cost_t *res = tile_edsger_cost(tp, exts[x], NULL);
		//printf("External road from %d\n", exts[x]->offset);
		
		for (y=0; y< extnodes; y++) {
			//printf("path to %d --> %p \n", exts[y]->offset, res);
			int test = tile_test_path_cost(tp, res, exts[x], exts[y]);
			if (test != 0) {
				//printf("Marking path\n");
				tile_mark_path_cost(tp, res, exts[x], exts[y]);
			} 
		}

		cost_rls(res);
	}

	int nkeep=0;
	int rkeep=0;
	for (n=0; n< tp->nnodes; n++) {
		if (tp->nds[n]->keep==1) nkeep++;
	}
	for (r=0; r< tp->nroads; r++) {
		if (tp->rds[r]->keep==1) rkeep++;
	}
	//printf("%d of %d (%d %%) nodes and %d of %d (%d %%) roads kept\n", 
			//nkeep, tp->nnodes, (nkeep*100)/tp->nnodes, 
			//rkeep, tp->nroads, (rkeep*100)/tp->nroads);
	
	tile_prune(tp);
	//printf("Just passed %d\n", tp->num);
	if (exts) free(exts);
}

int dump(int l,node_t *n, road_t *r, void *data)
{
	static char dir[4] = " ><";
	if (r) {
		printf("%d:%d and %d %c\n", n->tile->num, n->offset, r->cost, dir[r->dir]);
	} else {
		printf("%d:%d no road %p\n", n->tile->num, n->offset,r );
	}
	return 0;
}

int checkshortest(int l,node_t *n, road_t *r, void *data)
{
	contract_t *test = (contract_t *)data;

	//if (n->tile == test[0].nd->tile && n->offset== test[0].nd->offset) 
	if (r) {
		//printf("%d and %d += %d \n", test[0].len, test[1].len, r->len);
		test[0].len += r->len;
		test[1].len += r->len;
		//printf("%d and %d += %d \n", test[0].len, test[1].len, r->len);
	}
	if (n == test[0].nd) {
		test[0].score++;
	}
	if (n == test[1].nd) {
		test[1].score++;
	}

	return 0;
}

typedef struct transaction_tag
{
	node_t *nd;
	int score;
	
} transaction_t;


void tile_del_node(tile_t *tp, node_t *nd)
{
	int n;
	//tile_dmp(tp,1);

	//printf("Delete node %d:%d\n", nd->tile->num, nd->offset);
	for(n=0; n< tp->nnodes; n++) {
		if (tp->nds[n] == nd) break;
	}
	if (n==tp->nnodes) printf("Could not find node %d\n", nd->id);

	memmove(&tp->nds[n],&tp->nds[n+1],(tp->nnodes-n-1)*sizeof(node_t *));
	tp->nnodes--;

	//printf("After deletion\n");
	//tile_dmp(tp,1);
}

// del road AND backroad
void tile_del_roads(tile_t *tp, node_t *nd, road_t *rd)
{
	//road_t *backroad = node_back_road(rd->nodeto,nd);

	tile_del_road(tp,rd);
	//tile_del_road(backroad->nodeto->tile, backroad);
}


void tile_del_road(tile_t *tp, road_t *rd)
{
	int r;
	//tile_dmp(tp,1);

	//printf("Deleting road %d -> %d \n", rd->nodefrom->offset, rd->nodeto->offset);
	for(r=0; r< tp->nroads; r++) {
		//printf("Test %d %p against %p\n", r, tp->rds[r], rd);
		if (tp->rds[r] == rd) break;
	}
	if (r==tp->nroads) { 
		printf("Could not find road %d -> %d skipping\n", rd->nodefrom->offset, rd->nodeto->offset);
		tile_dmp(tp,1);
		exit(0);
		//tile_dmp(tp,1);
		//exit(0);
		return;
	}
	memmove(&tp->rds[r],&tp->rds[r+1],(tp->nroads-r-1)*sizeof(road_t *));
	tp->nroads--;

	node_t *from = rd->nodefrom;
	for (r=0; r< from->nroads; r++) {
		if (from->rds[r] == rd) break;
	}
	if (r==from->nroads) { 
		printf("Could not find road %p skipping\n", rd);
		tile_dmp(tp,1);
		exit(0);
		return;
	}
	memmove(&from->rds[r],&from->rds[r+1],(from->nroads-r-1)*sizeof(road_t *));
	from->nroads--;

	//printf("After deletion\n");
	//tile_dmp(tp,1);
}

road_t *road_back_road(road_t *fwd, node_t *orig)
{
	node_t *nodeto = fwd->nodeto;

	int r2;
	for (r2=0; r2<nodeto->nroads; r2++) {
		road_t *rp2 = nodeto->rds[r2];
		//printf("Test %p against %p\n", rp2->nodeto, orig);
		if (rp2->nodeto == orig) { 
			//lprintf(LOG_DEBUG, (char *)"Matched %d %d\n", fwd->len, rp2->len);
			if (rp2->len == fwd->len && is_reverse_dir(rp2,fwd)) 
				return rp2;
		}
	} 
	lprintf(LOG_ERR, (char *)"Could not find back road %d:%d to %d:%d!!!\n", orig->tile->num, fwd->nodefrom->offset,fwd->nodeto->tile->num, fwd->nodeto->offset);
	tile_dmp(orig->tile,1);
	tile_dmp(fwd->nodeto->tile,1);

	tile_renum(orig->tile);
	tile_double_roads(orig->tile);
	tile_dmp(orig->tile,1);
	exit(-1);
	return NULL;
}

road_t *ode_back_road(node_t *nodeto, node_t *orig)
{
	int r2;
	for (r2=0; r2<nodeto->nroads; r2++) {
		road_t *rp2 = nodeto->rds[r2];
		if (rp2->nodeto == orig) { 
			//printf("Setting back road %d:%d\n", rp2->nodeto->tile->num, rp2->nodeto->offset);
			return rp2;
		}
	} 
	printf("Could not find back road !!!\n");
	return NULL;
}
// must be enough ?
#define MAXNN 1000

/*
int node_contract_cost2(tcache_t *tc,node_t *np)
{
	int edge_diff = 0;
	int edges=0;
	int shortcuts=0;
	int r;
	int largest= 0;

	contract_t contracts[MAXNN];
	int nn=0;
	for (r=0; r< np->nroads; r++) {
		road_t *rp = np->rds[r];
		if (rp->keep==0) { 
			//printf("Skipping keep\n");
			continue;
		}
		node_t *nodeto = rp->nodeto;
	
		//printf("Backroad is %p\n", backroad);
		if (nn > MAXNN) {
			printf("Extension needed !!\n");
			printf("By the way, this is more likely to be an error, a node with more than %d roads!!\n", MAXNN);
			exit(0);
		} 
		contracts[nn].to = rp;
		//printf("To node is %d:%d \n", rp->nodeto->tile->num, rp->nodeto->offset);
		contracts[nn].back = road_back_road(rp,np);
		contracts[nn].nd = nodeto;
		if ((rp->len + contracts[nn].back->len) > largest) 
		    largest = rp->len + contracts[nn].back->len;
		
		nn++;
	}
	//printf("Found %d nodes\n", nn);
	edges = nn;
	contract_t testdata[2];
	for (int u=0; u< nn; u++) {
		int32_t res = level_edsger(tc->level, NULL, contracts[u].nd,largest);
		for (int w=0; w< nn; w++) {
			if (w==u) continue;
			int len1 = contracts[u].back->len;
			int len2 = contracts[w].to->len;
			//int32_t res = level_edsger(tc->level, contracts[u].nd, contracts[w].nd,len1+len2);
			testdata[0] = contracts[u];
			testdata[1] = contracts[w];
			testdata[0].score=0;
			testdata[1].score=0;
			int ret = level_traverse_path(tc->level, contracts[w].nd, contracts[u].nd,&checkshortest,&testdata,largest);
			int dir1 = contracts[u].to->dir;
			int dir2 = contracts[w].back->dir;
			if (dir1 != DIR_FROMTO && dir2 != DIR_FROMTO) {
				if (ret == 0 && testdata[0].len != 0 && testdata[0].len >= len1+len2) {
					if (testdata[0].score != 1 || testdata[1].score != 1) {
						printf("Found contraction with score %d %d\n", testdata[0].score, testdata[1].score);
						printf("[%d][%d] is %d vs %d\n", contracts[u].nd->offset,contracts[w].nd->offset, res, len1+len2);
					} 
					//printf("%d,%d No witness found\n", u,w);
					shortcuts++;
				} else {
					//printf("%d,%d Witness found %d >= %d + %d (%d)\n", u,w, testdata[0].len, len1, len2, len1+len2);
				} 
			}
		}
	}
	
	//printf("ED %d %d : %d - %d\n", np->offset, np->id, shortcuts,edges);
	edge_diff = shortcuts-edges;
	
	return edge_diff;
}

int node_contract2(tcache_t *tc,node_t *np,int L)
{
	int r;
	int largest= 0;

	contract_t contracts[MAXNN];
	int nn=0;
	for (r=0; r< np->nroads; r++) {
		road_t *rp = np->rds[r];
		if (rp->keep==0) { 
			//printf("Skipping keep\n");
			continue;
		}
		node_t *nodeto = rp->nodeto;
	
		//printf("Backroad is %p\n", backroad);
		if (nn > MAXNN) {
			printf("Extension needed !!\n");
			printf("By the way, this is more likely to be an error, a node with more than %d roads!!\n", MAXNN);
			exit(0);
		} 
		contracts[nn].to = rp;
		printf("To node is %d:%d len %d\n", rp->nodeto->tile->num, rp->nodeto->offset, rp->len);
		contracts[nn].back = road_back_road(rp,np);
		contracts[nn].nd = nodeto;
		if ((rp->len + contracts[nn].back->len) > largest) 
		    largest = rp->len + contracts[nn].back->len;
		
		nn++;
	}
	//printf("Found %d nodes for %d %d\n", nn, np->offset, np->id);
	contract_t testdata[2];
	if (nn > 1) {
		for (int w=0; w< nn; w++) {
#ifdef VERBOSE
			printf("edsger to %d [%d]------------\n", w, contracts[w].nd->offset);
#endif
			int32_t res = level_edsger(tc->level, NULL, contracts[w].nd,largest);
			for (int u=0; u< nn; u++) {
				if (w==u) continue;
				int len1 = contracts[w].back->len;
				int len2 = contracts[u].to->len;
				//int32_t res = level_edsger(tc->level, contracts[u].nd, contracts[w].nd,len1+len2);
				testdata[0] = contracts[w];
				testdata[1] = contracts[u];
				testdata[0].score=0;
				testdata[1].score=0;
#ifdef VERBOSE
			printf("Traverse from %d [%d]------------\n", u, contracts[u].nd->offset);
#endif
				int ret = level_traverse_path(tc->level, contracts[u].nd, contracts[w].nd,&checkshortest,&testdata,largest);
#ifdef VERBOSE
				printf("After traverse %d %d and %d\n", ret, testdata[0].len, testdata[1].len);
#endif
				// structure : 
				// u ----to---> N ---back---> w  
				// so we need dir from u->to and w->back
				int dir1 = contracts[u].to->dir;
				int dir2 = contracts[w].back->dir;
				printf("dirs %d (%d) and %d (%d)\n", dir1,contracts[u].to->len, dir2,  contracts[w].back->len);
				if (dir1 != DIR_FROMTO && dir2 != DIR_FROMTO) {
					printf("Passed\n");
					if (ret == 0 && (testdata[0].len != 0 && testdata[0].len >= len1+len2)) {
						if (testdata[0].score != 1 || testdata[1].score != 1) {
							printf("Found contraction with score %d %d\n", testdata[0].score, testdata[1].score);
							printf("[%d][%d] is %d vs %d\n", contracts[u].nd->offset,contracts[w].nd->offset, res, len1+len2);
						} 
						int dir = DIR_BOTH;
						// now the most restrictive one counts :
						if (dir1 == dir2) dir = dir1;
						else if (dir1 == DIR_BOTH) dir=dir2;
						else if (dir2 == DIR_BOTH) dir=dir1;
						else {
							printf ("opposite directions\n");
							continue;
						}
						int revdir = opposite_dir(dir);
#ifdef VERBOSE
						printf("dirs %d and %d give %d and %d\n", dir1, dir2, dir, revdir);
						printf("No witness found %d,%d\n",u,w);
#endif
						
						//printf("Bevoor\n");
						//tile_dmp(contracts[u].nd->tile,1);
						// W - ND 
						road_t *shortcut = (road_t *)calloc(sizeof(road_t),1);
						shortcut->nodefrom = contracts[w].nd;
						shortcut->nodeto->tile = contracts[u].nd->tile;
						shortcut->nodeto = contracts[u].nd;
						shortcut->len = len1+len2;
						shortcut->type = contracts[w].back->type;
						shortcut->dir = dir;
						shortcut->rise=1;
						shortcut->keep=1;
						tile_add_road(contracts[w].nd->tile,shortcut);

						// ND -> U
						shortcut = (road_t *)calloc(sizeof(road_t),1);
						shortcut->nodefrom = contracts[u].nd;
						shortcut->nodeto->tile = contracts[w].nd->tile;
						shortcut->nodeto = contracts[w].nd;
						shortcut->len = len1+len2;
						shortcut->type = contracts[w].to->type;
						shortcut->dir = revdir;
						shortcut->rise=1;
						shortcut->keep=1;
						tile_add_road(contracts[u].nd->tile,shortcut);
						//printf("Achter\n");
						//tile_dmp(contracts[u].nd->tile,1);

					} else {
#ifdef VERBOSE
						printf("%d,%d Witness found %d >= %d + %d (%d)\n", u,w, testdata[0].len, len1, len2, len1+len2);
#endif
					} 
				} else {
					//printf("%d,%d direction !?\n", u,w);
				}
			}
		}
	}
	for (r=0; r< np->nroads; r++) {
		road_t *rp = np->rds[r];
		rp->keep=0;
		road_t *back = road_back_road(rp,np);
		back->keep=0;

	}
	np->keep=1;
	np->rank = L;

	for (int u=0; u< nn; u++) {
		contracts[u].nd->contract_cost= node_contract_cost(tc,contracts[u].nd);
	} 
	
	return 0;
}
*/

int node_contract_cost(tcache_t *tcp,node_t *np)
{
	int edge_diff = 0;
	int edges=0;
	int shortcuts=0;
	int r;
	int flargest=0;
	int tlargest=0;
	//int totalcost=0;

	contract_t froms[MAXNN];
	contract_t tos[MAXNN];
	
	int nf=0;
	int nt=0;
	for (r=0; r< np->nroads; r++) {
		road_t *rp = np->rds[r];
		if (rp->keep==0) { 
			//printf("Skipping keep\n");
			continue;
		}
		node_t *nodeto = rp->nodeto;
	
		//printf("Backroad is %p\n", backroad);
		if (nf > MAXNN || nt > MAXNN) {
			printf("Extension needed !!\n");
			printf("By the way, this is more likely to be an error, a node with more than %d roads!!\n", MAXNN);
			exit(0);
		} 

		if (rp->dir == DIR_BOTH || rp->dir == DIR_TOFROM) {
			froms[nf].to = rp;
			//lprintf(LOG_DEBUG, (char *)"From node is %d:%d \n", rp->nodeto->tile->num, rp->nodeto->offset);
			froms[nf].back = road_back_road(rp,np);
			froms[nf].nd = nodeto;
			if ((rp->len) > flargest) 
		    	flargest = rp->len;
			
			nf++;
		}
		if (rp->dir == DIR_BOTH || rp->dir == DIR_FROMTO) {
			tos[nt].to = rp;
			//lprintf(LOG_DEBUG, (char *)"To node is %d:%d \n", rp->nodeto->tile->num, rp->nodeto->offset);
			tos[nt].back = road_back_road(rp,np);
			tos[nt].nd = nodeto;
			if ((rp->len) > tlargest) 
		    	tlargest = rp->len;
			
			nt++;
		}
	}
	//lprintf(LOG_ALOT, (char *)"Found %d,%d nodes\n", nf,nt);
	edges = np->nroads;
	contract_t testdata[2]={{0,0}};
	for (int w=0; w< nt; w++) {
		//printf("edsger to %d [%d]------------\n", w, tos[w].nd->offset);
		level_edsger(tcp->level, NULL, tos[w].nd,flargest+tlargest,MAXHOP);
		for (int u=0; u< nf; u++) {
			int len1 = tos[w].back->len;
			int len2 = froms[u].to->len;
			testdata[0] = tos[w];
			testdata[1] = froms[u];
			testdata[0].score=0;
			testdata[1].score=0;
			testdata[0].len=0;
			testdata[1].len=0;
#ifdef VERBSE
			printf("Traverse from %d [%d]------------\n", u, contracts[u].nd->offset);
#endif
				//int ret = level_traverse_path(tcp->level, froms[u].nd, tos[w].nd,&checkshortest,&testdata,flargest+tlargest);
	//			printf("after traverse %d %d\n", testdata[0].len, froms[u].nd->cost);
#ifdef VERBOSE
		//		printf("After traverse %d %d and %d\n", ret, testdata[0].len, testdata[1].len);
#endif
				// structure : 
				// u ----to---> N ---back---> w  
				// so we need dir from u->to and w->back
				int dir1 = froms[u].to->dir;
				int dir2 = tos[w].back->dir;
#ifdef VERBOSE
				//printf("dirs %d (%d) and %d (%d)\n", dir1,froms[u].to->len, dir2,  tos[w].back->len);
#endif
				if (dir1 != DIR_FROMTO && dir2 != DIR_FROMTO) {
					//printf("Passed\n");
					//if (ret < 1 || (testdata[0].len > 0 && testdata[0].len >= len1+len2)) {
					if ((froms[u].nd->cost < 0 || froms[u].nd->cost >= len1+len2)) {
						if (testdata[0].score != 1 || testdata[1].score != 1) {
							//printf("Found contraction with score %d %d\n", testdata[0].score, testdata[1].score);
							//printf("[%d][%d] is %d vs %d\n", froms[u].nd->offset,tos[w].nd->offset, res, len1+len2);
						} 
						shortcuts++;
					} else {
						//printf("%d,%d Witness found %d >= %d + %d (%d)\n", u,w, testdata[0].len, len1, len2, len1+len2);
					} 
				}
			}
		level_reset_edsger(tcp->level,tos[w].nd);
		//level_check_reset(tcp->level);
	}
	//printf("ED %d %d : %d - %d\n", np->offset, np->id, shortcuts,edges);
	edge_diff = shortcuts-edges;
	//int result = edsger
	return np->a;
}


int node_contract(tcache_t *tc,node_t *np,int L)
{
	int r;
	int flargest= 0;
	int tlargest= 0;

	contract_t froms[MAXNN];
	contract_t tos[MAXNN];
	
	int nf=0;
	int nt=0;
	for (r=0; r< np->nroads; r++) {
		road_t *rp = np->rds[r];
		if (rp->keep==0) { 
			//printf("Skipping keep\n");
			continue;
		}
		node_t *nodeto = rp->nodeto;
		if (nodeto->keep==0) { 
			//printf("Skipping keep\n");
		}
	
		//printf("Backroad is %p\n", backroad);
		if (nf > MAXNN || nt > MAXNN) {
			printf("Extension needed !!\n");
			printf("By the way, this is more likely to be an error, a node with more than %d roads!!\n", MAXNN);
			exit(0);
		} 

		if (rp->dir == DIR_BOTH || rp->dir == DIR_TOFROM) {
			froms[nf].to = rp;
			//lprintf(LOG_ALOT, (char *)"From node is %d:%d \n", rp->nodeto->tile->num, rp->nodeto->offset);
			froms[nf].back = road_back_road(rp,np);
			froms[nf].nd = nodeto;
			if ((rp->len ) > flargest) 
		    	flargest = rp->len;
			
			nf++;
		}
		if (rp->dir == DIR_BOTH || rp->dir == DIR_FROMTO) {
			tos[nt].to = rp;
			//lprintf(LOG_DEBUG, (char *)"To node is %d:%d \n", rp->nodeto->tile->num, rp->nodeto->offset);
			tos[nt].back = road_back_road(rp,np);
			tos[nt].nd = nodeto;
			if ((rp->len ) > tlargest) 
		    	tlargest = rp->len;
			
			nt++;
		}
	}
	//lprintf(LOG_DEBUG, (char *)"Found %d,%d nodes\n", nf,nt);
	//printf((char *)"Found %d,%d nodes\n", nf,nt);
	contract_t testdata[2];
	for (int w=0; w< nt; w++) {
		//lprintf(LOG_DEBUG, (char *)"edsger to %d [%d]------------\n", w, tos[w].nd->offset);
		level_edsger(tc->level, NULL, tos[w].nd,flargest+tlargest,MAXHOP);
		for (int u=0; u< nf; u++) {
			int len1 = tos[w].back->len;
			int len2 = froms[u].to->len;
			//int32_t res = level_edsger(tc->level, contracts[u].nd, contracts[w].nd,len1+len2);
			testdata[0] = tos[w];
			testdata[1] = froms[u];
			testdata[0].score=0;
			testdata[1].score=0;
			testdata[0].len=0;
			testdata[1].len=0;
			//int ret = level_traverse_path(tc->level, froms[u].nd, tos[w].nd,&checkshortest,&testdata,flargest+tlargest);
#ifdef VERBOSE
			printf("After traverse %d and %d\n", testdata[0].len, testdata[1].len);
#endif
			// structure : 
			// u ----to---> N ---back---> w  
			// so we need dir from u->to and w->back
			int dir1 = froms[u].to->dir;
			int dir2 = tos[w].back->dir;
			//lprintf(LOG_DEBUG, (char *)"dirs %d (%d) and %d (%d)\n", dir1,froms[u].to->len, dir2,  tos[w].back->len);
			if (dir1 != DIR_FROMTO && dir2 != DIR_FROMTO) {
				//lprintf(LOG_DEBUG, (char *)"Passed\n");
				if ((froms[u].nd->cost < 0 || froms[u].nd->cost >= len1+len2)) {
				//if (ret <1  || (testdata[0].len > 0 && testdata[0].len >= len1+len2)) {
					if (testdata[0].score != 1 || testdata[1].score != 1) {
						//printf("Found contraction with score %d %d\n", testdata[0].score, testdata[1].score);
						//printf("[%d][%d] is %d vs %d\n", froms[u].nd->offset,tos[w].nd->offset, res, len1+len2);
					} 
					int dir = DIR_BOTH;
					// now the most restrictive one counts :
					if (dir1 == dir2) dir = dir1;
					else if (dir1 == DIR_BOTH) dir=dir2;
					else if (dir2 == DIR_BOTH) dir=dir1;
					else {
						printf ("opposite directions\n");
						continue;
					}
					int revdir = opposite_dir(dir);
					//printf("dirs %d and %d give %d and %d\n", dir1, dir2, dir, revdir);
					//printf("No witness found %d,%d\n",u,w);
					
					//printf("Bevoor\n");
					//tile_dmp(contracts[u].nd->tile,1);
					// W - ND 
					road_t *shortcut = (road_t *)calloc(sizeof(road_t),1);
					shortcut->nodefrom = tos[w].nd;
					shortcut->nodeto = froms[u].nd;
					shortcut->nodeto->tile = froms[u].nd->tile;
					shortcut->len = len1+len2;
					shortcut->type = tos[w].back->type;
					shortcut->dir = dir;
					shortcut->rise=1;
					shortcut->keep=1;
					tile_add_road(tos[w].nd->tile,shortcut);
					tos[w].nd->a++;

					// ND -> U
					shortcut = (road_t *)calloc(sizeof(road_t),1);
					shortcut->nodefrom = froms[u].nd;
					shortcut->nodeto = tos[w].nd;
					shortcut->nodeto->tile = tos[w].nd->tile;
					shortcut->len = len1+len2;
					//if (shortcut->len > newl) shortcut->len = newl;
					shortcut->type = tos[w].to->type;
					shortcut->dir = revdir;
					shortcut->rise=1;
					shortcut->keep=1;
					tile_add_road(froms[u].nd->tile,shortcut);
					froms[u].nd->a++;
					//printf("Achter\n");
					//tile_dmp(tos[u].nd->tile,1);

					//printf("Added len %d in tile %d and %d\n", len1+len2, 
						//froms[w].nd->tile->num, tos[u].nd->tile->num);
				} else {
					//printf("%d,%d Witness found %d >= %d + %d (%d)\n", u,w, testdata[0].len, len1, len2, len1+len2);
				} 
			} else {
				//printf("%d,%d direction !?\n", u,w);
			}
		}
		level_reset_edsger(tc->level,tos[w].nd);
		//level_check_reset(tc->level);
	}
	for (r=0; r< np->nroads; r++) {
		road_t *rp = np->rds[r];
		rp->keep=0;
		road_t *back = road_back_road(rp,np);
		//rp->nodeto->d++;
		back->keep=0;

	}
	np->keep=0;
	np->rank = L;

	for (int u=0; u< nf; u++) {
		froms[u].nd->contract_cost= node_contract_cost(tc,froms[u].nd);
	} 
	for (int w=0; w< nt; w++) {
		tos[w].nd->contract_cost= node_contract_cost(tc,tos[w].nd);
	} 
	
	return 0;
}

/*
int node_contract_cost(tcache_t *tc, node_t *np)
{
	int r;
	//level_reset_keep(tc->level,1);

	int edge_diff = 0;
	int edges=0;
	int shortcuts=0;
	//int uniformity=0;

	contract_t contracts[MAXNN];
	int nn=0;
	for (r=0; r< np->nroads; r++) {
		road_t *rp = np->rds[r];
		node_t *nodeto = rp->nodeto;
	
		//printf("Backroad is %p\n", backroad);
		if (nn > MAXNN) {
			printf("Extension needed !!\n");
			printf("By the way, this is more likely to be an error, a node with more than %d roads!!\n", MAXNN);
			exit(0);
		} 
		contracts[nn].to = rp;
		//printf("To node is %d:%d eval %d\n", tileto->num, rp->nodeto->offset);
		contracts[nn].back = node_back_road(nodeto,np);
		contracts[nn].nd = nodeto;
		
		nn++;
	}
	printf("Found %d nodes\n", nn);
	contract_t testdata[2];
	edges = nn;
	for (int u=0; u< nn; u++) {
		for (int w=0; w< nn; w++) {
			if (w==u) continue;
			int len1 = contracts[u].back->len;
			int len2 = contracts[w].to->len;
			int32_t res = level_edsger(tc->level, contracts[u].nd, contracts[w].nd,len1+len2);
			if (contracts[u].back->dir != DIR_TOFROM && 
				contracts[w].to->dir != DIR_TOFROM) {
				if (res >= len1+len2) {
					testdata[0] = contracts[u];
					testdata[1] = contracts[w];
					testdata[0].score=0;
					testdata[1].score=0;
					level_traverse_path(tc->level, contracts[u].nd, contracts[w].nd,&checkshortest,&testdata);
					if (testdata[0].score != 1 || testdata[1].score != 1) {
						printf("Found contraction with score %d %d\n", testdata[0].score, testdata[1].score);
						printf("[%d][%d] is %d vs %d\n", contracts[u].nd->offset,contracts[w].nd->offset, res, len1+len2);
						exit(-1);
						//tile_dmp(tp, 1);
					} 
					shortcuts++;
				}
			} 
		}
	}
	//printf("%d shortcuts %d edges\n", shortcuts, edges);
	edge_diff = shortcuts-edges;
	
	return edge_diff;
}

void tile_contract(tile_t *tp)
{
	int n, r;
	int r2;
	tile_reset_keep(tp,1);
	contract_t contracts[MAXNN];

	for (n=0; n< tp->nnodes; n++) {
		node_t *np = tp->nds[n];
		int nn=0;
		for (r=0; r< np->nroads; r++) {
			road_t *rp = np->rds[r];
			tile_t *tileto = rp->nodeto->tile;
			node_t *nodeto = rp->nodeto;

			//printf("Backroad is %p\n", backroad);
			if (nn > MAXNN) {
				printf("Extension needed !!\n");
				printf("By the way, this is more likely to be an error, a node with more than %d roads!!\n", MAXNN);
				exit(0);
			} 
			contracts[nn].to = rp;
			printf("To node is %d:%d\n", tileto->num, rp->nodeto->offset);
			contracts[nn].back = NULL;
			for (r2=0; r2<nodeto->nroads; r2++) {
				road_t *rp2 = nodeto->rds[r2];
				if (rp2->nodeto->offset == np->offset && 
					rp2->nodeto->tile == tp) {
					printf("Setting back road %d:%d\n", rp2->nodeto->tile->num, rp2->nodeto->offset);
					contracts[nn].back = rp2;
				}
			} 
			contracts[nn].nd = nodeto;
			
			nn++;
		}
		printf("Found %d nodes\n", nn);
		contract_t testdata[2];
		for (int u=0; u< nn; u++) {
			for (int w=0; w< nn; w++) {
				if (w==u) continue;
				tile_dmp(tp,1);
				int32_t res = level_edsger(tp->level, contracts[u].nd, contracts[w].nd,1000000000);
				int len1 = contracts[u].back->len;
				int len2 = contracts[w].to->len;
				if (contracts[u].back->dir != DIR_TOFROM && 
					contracts[w].to->dir != DIR_TOFROM) {
					//printf("road %d has dir %d\n", contracts[w].to->nodeto,contracts[w].to->dir);
					if (res >= len1+len2) {
						testdata[0] = contracts[u];
						testdata[1] = contracts[w];
						testdata[0].score=0;
						testdata[1].score=0;
						level_traverse_path(tp->level, contracts[u].nd, contracts[w].nd,&checkshortest,&testdata);
						if (testdata[0].score != 1 || testdata[1].score != 1) {
							printf("Found contraction with score %d %d\n", testdata[0].score, testdata[1].score);
							printf("[%d][%d] is %d vs %d\n", contracts[u].nd->offset,contracts[w].nd->offset, res, len1+len2);
							//tile_dmp(tp, 1);
						} 
						//contracts[u].nd->keep=0;

						// u ----> np ----> w 
						// u <---- np <---- w 
						// arrows do not denote oneway streets but structural
						// network direction, this becomes : 
			 			// u ------------> w 
			 			// u <------------ w  
						contracts[u].back->nodeto = contracts[w].nd;

						// and relink
						//tile_del_road(contracts[u].back->nodeto->tile,contracts[u].to);
						//tile_del_road(contracts[u].to->nodeto->tile,contracts[u].back);
					}
				} 
				//tile_del_road(tp,contracts[u].to);
				//tile_del_road(tp,contracts[w].to);
				//tile_del_road(tp,contracts[w].to);
			}
		}
	}
	printf("Contracted %d ----------\n", tp->num);
	tile_dmp(tp,1);
}
*/

void reset_join(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)nodep;

	tile_reset_join(tcep->tile);
}

draw_edge_t gen_get_edge(draw_context_t *dc, int r)
{
	tile_t *tp = (tile_t *)dc->handle;
	road_t *rd = tp->rds[r];
	
	draw_node_t from; 
	draw_node_t to; 

	draw_edge_t nc={ from,to,rd->len, (short) rd->type, (short) rd->dir, 0, NULL } ;

	return nc;
}

draw_node_t gen_get_node(draw_context_t *dc, int n)
{
	tile_t *tp = (tile_t *)dc->handle;
	node_t *nd = tp->nds[n];
	
	draw_node_t nc={ nd->id,NULL, nd->x , nd->y } ;

	return nc;
}

void tile_edsger_radius(tile_t *tp)
{
	int n;

	// set radii
	for (n=0; n< tp->nnodes; n++) { 
		node_t *np = tp->nds[n];
		np->fradius = level_edsger_radius(tp->level, np, H, DIR_FROMTO);
		np->bradius = level_edsger_radius(tp->level, np, H, DIR_TOFROM);
		//if (np->bradius != np->fradius) 
			//printf("%d:%d radii found %d and %d\n", np->tile->num, np->offset, np->fradius, np->bradius);
		//else
			//printf("%d:%d radii found %d\n", np->tile->num, np->offset, np->bradius);
	} 
}

void tile_contract_hh(tile_t *tp)
{
	int n;

	for (n=0; n< tp->nnodes; n++) { 
		node_t *np = tp->nds[n];
		cost_t *cost = level_edsger_cost_HH(tp->level, np, H);
		level_promote(tp->level, cost, np, H);
		cost_rls(cost);
	} 
}

void tcache_contract_hh_1(tcache_t *tc)
{
	int N=0;
	tcarray_t ta = tcache_get_array(tc);
	printf("Contracting %d tiles\n", ta.len);
	for (int t=0; t< ta.len; t++) {
		tile_t *tp = ta.items[t]->tile;
		N+= tp->nnodes;
		tile_edsger_radius(tp);
		printf("calc radius %d/%d                    \r", t+1, ta.len);
		fflush(stdout);
	}

	printf("\n");

	for (int t=0; t< ta.len; t++) {
		tile_t *tp = ta.items[t]->tile;
		N+= tp->nnodes;
		tile_contract_hh(tp);
		printf("contract %d/%d                    \r", t+1, ta.len);
		fflush(stdout);
	}

//	d.Init();
	//d.Fill();
	//d.Run();
	//pqentry_t *pq = (pqentry_t *)calloc(sizeof(pqentry_t),N);
	node_t **pq = (node_t **)calloc(sizeof(node_t *),N);

	if (!pq) { 
		printf("Ayyyyy\n");
		exit(-1);
	}

	//tcache_walk(tc, renum);
}

void tcache_contract_edsger(tcache_t *tc)
{
	int n,N=0;
	tcarray_t ta = tcache_get_array(tc);
	//printf("Contracting %d tiles\n", ta.len);
	for (int t=0; t< ta.len; t++) {
		tile_t *tp = ta.items[t]->tile;
		N+= tp->nnodes;
	}
	printf("Contracting %d nodes\n", N);

	//pqentry_t *pq = (pqentry_t *)calloc(sizeof(pqentry_t),N);
	node_t **pq = (node_t **)calloc(sizeof(node_t *),N);

	if (!pq) { 
		printf("Ayyyyy\n");
		exit(-1);
	}

	N=0;
	for (int t=0; t< ta.len; t++) {
		tile_t *tp = ta.items[t]->tile;
		for (n=0; n< tp->nnodes; n++) {
			pq[N]= tp->nds[n];
			pq[N]->contract_cost= node_contract_cost(tc,pq[N]);
			pq[N]->rank=0;
			N++;
		}
	}
	qsort(pq,N,sizeof(node_t *),contract_cmp);

	n= 0;

	while (n< N) {
		//lprintf(LOG_DEBUG, (char *)"order [%d/%d] %d=%d -> %d and %d\n", n,N, pq[n]->offset,pq[n]->id, pq[n]->contract_cost, pq[n]->rank);
		n++;
	}
	n= 0;
	while (n< N) {
		lprintf(LOG_DEBUG, (char *)"contracting [%d/%d] %d=%d -> %d and %d\n", n,N, pq[n]->offset,pq[n]->id, pq[n]->contract_cost, pq[n]->rank);
		if (n>0) printf("%d/%d %d %%              \r", n,N, (100*n)/N);
		node_contract(tc,pq[n],n);
		// sort rest
		n++;
		qsort(&pq[n],N-n,sizeof(node_t *),contract_cmp);
		printf("Sorted %d items from %d\n", N-n, n);
		for (int n2 = 0; n2< N; n2++) {
			//lprintf(LOG_DEBUG, (char *)"order [%d/%d] %d=%d -> %d and %d\n", n2,N, pq[n2]->offset,pq[n2]->id, pq[n2]->contract_cost, pq[n2]->rank);
		}
	}

	tcache_walk(tc, renum);
//	tcache_dmp(tc);
}

typedef struct simple_tag
{
	int nnodes;
	node_t **nds;
	int *costs;
	node_t *middle;
	road_t *rd;
} simple_t;

typedef struct circle_tag
{
	node_t *nd;
	int cost;
} circle_t;

map<node_t *,circle_t> node_get_circle(node_t *nd, int hops, int dir)
{
	map <node_t *,circle_t>last;
	map <node_t *,circle_t>next;
	map <node_t *,circle_t>total;
	
	circle_t c={0};
	c.nd = nd;
	last[nd] = c;
	for (int h=0;h< hops; h++) {
		for( map<node_t *,circle_t>::iterator ii=last.begin(); ii!=last.end(); ++ii) {
			circle_t circ = (circle_t) ii->second;
			for (int r=0; r< circ.nd->nroads; r++) {
				road_t *rd = circ.nd->rds[r];
				if (rd->dir != dir) continue;
				c.nd = circ.nd->rds[r]->nodeto;
				cout << total[c.nd].cost;
				next[c.nd]=c;
				total[c.nd] = c;
			}
		}
		last = next;
	}

	return total;
}

int32_t node_contract_simple(tcache_t *tc,node_t *np, int simulate)
{
	int edge_diff = 0;
	//int edges=0;
	//int shortcuts=0;
	int r;
	int flargest=0;
	int tlargest=0;

	simple_t froms[MAXNN];
	simple_t tos[MAXNN];
	
	int nf=0;
	int nt=0;
	for (r=0; r< np->nroads; r++) {
		road_t *rp = np->rds[r];
		if (rp->keep==0) { 
			//printf("Skipping keep\n");
			continue;
		}
		node_t *nodeto = rp->nodeto;
	
		//printf("Backroad is %p\n", backroad);
		if (nf > MAXNN || nt > MAXNN) {
			printf("Extension needed !!\n");
			printf("By the way, this is more likely to be an error, a node with more than %d roads!!\n", MAXNN);
			exit(0);
		} 

		if (rp->dir == DIR_BOTH || rp->dir == DIR_TOFROM) {
			lprintf(LOG_DEBUG, (char *)"From node is %d:%d \n", rp->nodeto->tile->num, rp->nodeto->offset);
			froms[nf].rd = road_back_road(rp,np);
			froms[nf].middle = nodeto;
			if ((rp->len) > flargest) 
		    	flargest = rp->len;
			
			nf++;
		}
		if (rp->dir == DIR_BOTH || rp->dir == DIR_FROMTO) {
			tos[nt].rd = rp;
			lprintf(LOG_DEBUG, (char *)"To node is %d:%d \n", rp->nodeto->tile->num, rp->nodeto->offset);
			tos[nt].middle = nodeto;
			if ((rp->len) > tlargest) 
		    	tlargest = rp->len;
			
			nt++;
		}
	}

	lprintf(LOG_ALOT, (char *) "%d and %d nodes found\n", nf, nt);
	if (nf==0 || nt==0) goto end;

	for (int t=0; t< nf; t++) {
		map <node_t*, circle_t>fnodes = node_get_circle(froms[t].middle, MAXHOP, DIR_TOFROM);
		printf("%lu nodes within %d hops\n", fnodes.size(), MAXHOP);
	}

	for (int t=0; t< nt; t++) {
		map <node_t*, circle_t>tnodes = node_get_circle(tos[t].middle, MAXHOP, DIR_FROMTO);
		printf("%lu nodes within %d hops\n", tnodes.size(), MAXHOP);
	}

end:
	return edge_diff;
}

void tcache_contract_simple(tcache_t *tc)
{
	int n,N=0;
	tcarray_t ta = tcache_get_array(tc);
	//printf("Contracting %d tiles\n", ta.len);
	for (int t=0; t< ta.len; t++) {
		tile_t *tp = ta.items[t]->tile;
		N+= tp->nnodes;
	}
	printf("Contracting %d nodes\n", N);

	//pqentry_t *pq = (pqentry_t *)calloc(sizeof(pqentry_t),N);
	node_t **pq = (node_t **)calloc(sizeof(node_t *),N);

	if (!pq) { 
		printf("Ayyyyy\n");
		exit(-1);
	}

	N=0;
	for (int t=0; t< ta.len; t++) {
		tile_t *tp = ta.items[t]->tile;
		for (n=0; n< tp->nnodes; n++) {
			pq[N]= tp->nds[n];
			pq[N]->contract_cost= node_contract_simple(tc,pq[N],true);
			pq[N]->rank=0;
			N++;
		}
	}
	qsort(pq,N,sizeof(node_t *),contract_cmp);

}

void tcache_hh_1(tcache_t *tc)
{

	tcache_walk(tc, reset_join);
	//tcache_dmp(tc);
	tcache_walk(tc, double_roads);

	/*
	tcache_walk(tc, join);
	printf("After join !!\n");
	tcache_walk(tc, double_roads);
	printf("After join !!\n");
	tcache_walk(tc, double_roads);
	*/

	tcache_walk(tc, renum);
	printf("After renum !!\n");
	//tcache_dmp(tc);
	tcache_put_all(tc,114);
	tcache_walk(tc, double_roads);

	//tcache_dmp(tc);
	tcache_reset(tc);
	tcache_check_reset(tc);

	//tcache_dmp(tc);
	tcache_contract_hh_1(tc);
	//tcache_dmp(tc);
}

void tcache_reduce(tcache_t *tc)
{

	tcache_walk(tc, reset_join);
	tcache_dmp(tc);
	tcache_walk(tc, double_roads);

	/*
	tcache_walk(tc, join);
	printf("After join !!\n");
	tcache_walk(tc, double_roads);
	printf("After join !!\n");
	tcache_walk(tc, double_roads);
	*/
	tcache_walk(tc, renum);
	printf("After renum !!\n");
	tcache_dmp(tc);
	tcache_put_all(tc,114);
	tcache_walk(tc, double_roads);

	return;
	//tcache_dmp(tc);
	tcache_reset(tc);
	tcache_check_reset(tc);

	tcache_dmp(tc);
	tcache_contract_edsger(tc);
	tcache_dmp(tc);
}


// renumber nodes and roads after mutations
void tile_renum(tile_t *tp)
{
	int n;

	tile_node_sort(tp);
	for (n=0; n< tp->nnodes; n++) {
		node_t *nd = tp->nds[n];
		nd->offset=n;
	}
	tile_road_sort(tp);
}

void tile_join(tile_t *tp)
{
	int n;

	int p = TESTTILES(tp->num);
	if (p) printf("join %d\n", tp->num);
	// loop backward to ease removal of nodes
	for (n= tp->nnodes-1; n>=0; n--) {
		node_t *nd = tp->nds[n];

	if (p) tile_dmp(tp,1);
		if (nd->join) {
			lprintf(LOG_DEBUG, (char *)"Node %d:%d is joinable\n", nd->tile->num, nd->offset);
			// Nl <--Rl--> N <--Rr--> Nr
			road_t *Rl = nd->rds[0];
			road_t *Rr = nd->rds[1];
			tile_t *Tl = Rl->nodeto->tile;
			tile_t *Tr = Rr->nodeto->tile;

			node_t *Nl = Rl->nodeto;
			node_t *Nr = Rr->nodeto;
			// and back again 
			road_t *Bl = NULL;
			road_t *Br = NULL;
			//printf("Backroad search for node %d:%d\n", nd->tile->num, nd->offset);
			for (int r=0; r< Nl->nroads; r++) {
				if (Nl->rds[r]->nodeto == nd) Bl=Nl->rds[r];
			}
			// orther way round to avoid getting the same backroad 
			// when Nl == Nr (circular reference)
			for (int r= Nr->nroads-1; r>=0; r--) {
				if (Nr->rds[r]->nodeto == nd) Br=Nr->rds[r];
			}
			if (!Bl || !Br) { 
				printf("No backroad found %p %p\n", Bl, Br);
				tile_dmp(tp,1);
			}

			/*
			printf("%d:%d <---- %d <----  %d:%d  <---- %d <---- %d:%d\n", 
				Nl->tile->num, Nl->offset, Rl->len, nd->tile->num, nd->offset, Rr->len, Nr->tile->num, Nr->offset);
			printf("%d:%d ----> %d ----> %d:%d  ----> %d ----> %d:%d\n", 
				Nl->tile->num, Nl->offset, Bl->len, nd->tile->num, nd->offset, Br->len, Nr->tile->num, Nr->offset);
			*/
			int len = Rl->len+Rr->len;

			//printf("Joined %d + %d into %d\n", Rl->len, Rr->len, len);
			// relink the backroads, the ones coming into nd
			Bl->nodeto = Nr;
			Br->nodeto = Nl;
			Bl->nodeto->tile = Tr;
			Br->nodeto->tile = Tl;
			Br->len = len;
			Bl->len = len;

			if (p) tile_dmp(tp,1);

			if (Nl == Nr )  {
				//printf("Circular reference %d:%d and %d:%d\n", Nl->tile->num,
						//Nl->offset, Nr->tile->num, Nr->offset);	
				//if (Nr == nd) 
					//printf("Self reference %d:%d and %d:%d\n", Nl->tile->num,
						//Nl->offset, Nr->tile->num, Nr->offset);	
				tile_del_road(tp,Rl); // Rl == Rn so delete only 1
			} else {
			// now remove nd, Rl and Rr
				tile_del_road(tp,Rl);
				tile_del_road(tp,Rr);
			}

			// now delete node
			tile_del_node(tp,nd);

			/*
			printf("--1--\n");
			for (int r=0; r< nd->nroads; r++) {
				for (int r2=r+1; r2< nd->nroads; r2++) {
					if (nd->rds[r]->nodeto == nd->rds[r2]->nodeto) {
						printf("double %d, %d\n", nd->rds[r]->len, nd->rds[r2]->len);
					}
				}
			}
			printf("--2--\n");
			for (int r=0; r< Nl->nroads; r++) {
				for (int r2=r+1; r2< Nl->nroads; r2++) {
					if (Nl->rds[r]->nodeto == Nl->rds[r2]->nodeto) {
						printf("double %d, %d\n", Nl->rds[r]->len, Nl->rds[r2]->len);
					}
				}
			}
			printf("--3--\n");
			for (int r=0; r< Nr->nroads; r++) {
				for (int r2=r+1; r2< Nr->nroads; r2++) {
					if (Nr->rds[r]->nodeto == Nr->rds[r2]->nodeto) {
						printf("double %d, %d\n", Nr->rds[r]->len, Nr->rds[r2]->len);
					}
				}
			}
			*/

			tile_double_roads(tp);
			if (nd->pnode)
				nd->pnode->pnode=NULL;
			//free(nd);
			//free(Rl);
			//free(Rr);
		}
	}
	if (p) printf("done join %d\n", tp->num);
	if (p) tile_dmp(tp,1);
}

void tile_reduce(tile_t *tp)
{
	//int n;
	tile_reset_keep(tp,1);

	int p = TESTTILES(tp->num);
	printf("Reducing %d\n", tp->num);
	//tile_dmp(tp,1);

	//while (whipe_deadends(tp) == 1) ;

	//printf("whiped\n");
	//tile_dmp(tp,1);

	node_t *nodes[2] ={0};
	node_t *nd=NULL;
	int roads[2] ={0};
	int len;
	while ((len = find_endpoints(tp,nd,nodes,roads)) >= 0) {
		if (len >= 0 && nodes[0] && nodes[1]) {
			if (len == 0) printf("Handle 0 length road !!!\n");

			if (p) {
				printf("Found nodes %d (%d/%dk) and %d (%d/%dk) -> len %d\n", 
				nodes[0]->offset, roads[0], nodes[0]->keep, nodes[1]->offset, roads[1], nodes[1]->keep, len);
			}

			if (nodes[0]==NULL || nodes[1] == NULL || roads[0] < 0 || roads[1] < 0) 
			{
				printf("Could not find nodes and roads %p || %p || %d || %d\n",
					nodes[0], nodes[1], roads[0], roads[1]);
				tile_dmp(tp, 1);
				exit(-1);
			}
			if (nodes[0]->keep && nodes[1]->keep)
			{
				int type1 = tp->rds[roads[0]]->type;
#ifdef USE_TYPES
				int type2 = tp->rds[roads[1]]->type;
				if (type1 != type2) {
					printf("Road type conflict %d and %d\n", type1, type2);	
					printf("Roads %d:%d and %d:%d\n", tp->num, roads[0], tp->num, roads[1]);	
					tile_dmp(tp,1);
				}
#endif
				if (nodes[0]->offset == nodes[1]->offset) {
					//printf("Refusing to make road to self %d:%d\n", 
						//tp->num, nodes[0]->offset);
				} else {
					//printf("Reduce to 1 road ! %d %d\n", tp->rds[roads[0]].dir, tp->rds[roads[1]].dir);
#ifdef USE_TYPES
					if (!is_reverse_dir((tp->rds[roads[0]]), (tp->rds[roads[1]]))) {
		printf("Wrong directions !!!\n");
		tile_dmp(tp,1);
	}
#endif
					if (len==0) len=1;
					tp->rds[roads[0]]->keep = 1;
					tp->rds[roads[0]]->nodeto = nodes[1];
					tp->rds[roads[0]]->nodeto->tile = nodes[1]->tile;
					tp->rds[roads[0]]->len = len;
					tp->rds[roads[0]]->type = type1;
					tp->rds[roads[1]]->keep = 1;
					tp->rds[roads[1]]->nodeto = nodes[0];
					tp->rds[roads[1]]->nodeto->tile = nodes[0]->tile;
					tp->rds[roads[1]]->len = len;
					tp->rds[roads[1]]->type = type1;
					//tile_dmp(tp,1);
				}
			}
		} else {
			//printf("Remove completely\n");
		}
	}
	//printf("ended\n");
	//tile_dmp(tp,1);
	// won't hurt ;)
	while (whipe_deadends(tp) == 1) ;
	//printf("Reduced endpoints %d\n", tp->num);

	//tile_dmp(tp, 1);

	//tile_dmp(tp,1);
	tile_prune(tp);
	//printf("Just pruned %d\n", tp->num);

//tile_dmp(tp,1);

	//exit(0);
}


void tile_double_roads(tile_t *tp)
{

	//tile_sort(tp);
	//printf("tile_double_roads %d\n", tp->num);
	//tile_dmp(tp,1);
	for (int n=0; n< tp->nnodes; n++) {
		node_t *nd = tp->nds[n];
again:
		for (int r1=nd->nroads-1; r1 >=0; r1--) {
			road_t *rd1 = nd->rds[r1];
			for (int r2=r1-1; r2 >=0; r2--) {
				road_t *rd2 = nd->rds[r2];
				if (rd1->nodeto->tile == rd2->nodeto->tile &&
				    rd1->nodeto == rd2->nodeto && rd1->dir == rd2->dir) {
					//printf("Found double road from %d:%d to %d:%d\n", 
						//tp->num, nd->offset, rd1->nodeto->tile->num, rd1->nodeto->offset);
					//printf("len %d vs %d, dir %d vs %d , type %d vs %d\n", 
						//rd1->len, rd2->len, rd1->dir, rd2->dir, rd1->type, rd2->type);

		//tile_dmp(tp,1);
		//exit(0);
					/*
					smallest = 100000000;
					for (int r3 = 0; r3 < rd2->nodeto->nroads; r3++) {
						road_t *opproad = rd2->nodeto->rds[r3];
						if (nd == opproad->nodeto && is_reverse_dir(opproad,rd2)  
							&& opproad->len < rd2->len) {
							smallest = opproad->len; // make sure the very smallest is used
						}
					}
					*/
					if (rd1->type != rd2->type) printf("Warning : type diff %d vs %d\n", rd1->type, rd2->type);
					//if (nd == rd2->nodeto) {
						//tile_del_road(tp,rd2);
					//} 
					if (rd1->dir == rd2->dir) {
						if (rd1->len < rd2->len) {
							rd1->len = rd2->len;
						}
						tile_del_road(tp,rd2);
					}
					goto again;
				}
			}
		}
	} 
}

void tile_double_roads_old(tile_t *tp)
{
	//printf("tile_double_roads %d\n", tp->num);
	//tile_dmp(tp,1);
	int foundlen;
	node_t *to, *from;

	for (int r=0; r< tp->nroads; r++) {
		int fromnr = tp->rds[r]->nodefrom->offset;
		if (tp->rds[r]->nodeto->tile == tp &&
			tp->rds[r]->nodefrom == tp->rds[r]->nodeto) {
			tp->rds[r]->keep=0;
		}

		for (int r2=r+1; r2< tp->nroads && tp->rds[r2]->nodefrom->offset==fromnr; r2++) {
			if (tp->rds[r]->nodefrom == tp->rds[r2]->nodefrom &&
				tp->rds[r]->nodeto->tile == tp->rds[r2]->nodeto->tile &&
				tp->rds[r]->nodeto == tp->rds[r2]->nodeto &&
				tp->rds[r]->keep == 1 && 
				tp->rds[r2]->keep == 1 
				&& tp->rds[r]->dir == tp->rds[r2]->dir 
				//&& tp->rds[r].type == tp->rds[r2].type
				) {
				//printf("Whiping longest double road %d %d:%d %d vs %d\n", r, tp->num, tp->rds[r].nodeto, tp->rds[r].len, tp->rds[r2].len);
				if (ROADCOST(*tp->rds[r]) > ROADCOST(*tp->rds[r2]) || 
				    ( ROADCOST(*tp->rds[r]) == ROADCOST(*tp->rds[r2]) &&
					tp->rds[r]->type < tp->rds[r2]->type ))  {
					tp->rds[r]->keep=0;
					to = tp->rds[r]->nodeto;
					from = tp->rds[r]->nodefrom;
					foundlen = tp->rds[r]->len;
					//printf("Chose from %d with %d\n", to->offset, tp->rds[r].len);
				} else {
					tp->rds[r2]->keep=0;
					to = tp->rds[r2]->nodeto;
					from = tp->rds[r2]->nodefrom;
					foundlen = tp->rds[r2]->len;
			
					//printf("Chose from %d with %d\n", to->offset, tp->rds[r-1].len);
				}
			}
		}
	}
	tile_prune(tp);
}

void tile_node_shrink(tile_t *tp,int offset)
{
	// sort all nodes with the deleted nodes at the end (see node_cmp)
	tile_node_sort(tp);
	// alter the length to the last undeleted node
	tp->nnodes=offset;
	node_t *oldnode = tp->nds[offset];

	// and physically resize : 
	// the next voodoo will realloc correctly for borders n*0,n*1,n*2 etc..
	// hey, we need the memory ;-)
	int resize = ((tp->nnodes + (RALLOCSIZE-1))/RALLOCSIZE) * RALLOCSIZE;

	//printf("Shrinking %d to %d\n", tp->nnodes, resize);
	tp->nds=(node_t **)realloc(tp->nds, sizeof(node_t *)*resize);

	free(oldnode);
}

void tile_sort(tile_t *tp)
{
	int n, r;
	node_t *rev[tp->nnodes];
	
	//printf("in \n");
	//tile_dmp(tp,1);
	qsort(tp->nds,tp->nnodes,sizeof(node_t),linknode_cmp);

	for (n=0; n< tp->nnodes; n++) {
		node_t *nd = tp->nds[n];
		rev[nd->offset] = nd;
	}

	for (n=0; n< tp->nnodes; n++) {
		node_t *nd = tp->nds[n];

		if (nd->keep == 1) {
			noderef_set(nr,nd->tile->num, nd->offset, nd->tile->num, rev[nd->offset]->offset);
		} else {
			printf("Only sort full tiles !!! (fix this !)\n");
			exit(0);
		} 
	}
//	printf("between \n");
	//tile_dmp(tp,1);

	for (r=0; r< tp->nroads; r++) {
		road_t *rd = tp->rds[r];
		if (rd->keep == 0) {
			printf("Only sort full tiles !!! (fix this !)\n");
			exit(0);
		} 

		if (rd->nodefrom->offset < 0 || 
			(rd->nodeto->tile == tp && 
		    rd->nodeto->offset < 0)) { 
			printf("Wrong reference nodefrom %d or nodeto %d error\n", 	rd->nodefrom->offset, rd->nodeto->offset);
			tile_dmp(tp,1);
			exit(-11); // i regard this as fatal error at current
			rd->keep=0;
		}
		//rd->nodefrom = tp->nds[rd->nodefrom].offset;
		rd->nodefrom = rev[rd->nodefrom->offset];
		if (rd->nodeto->tile == tp) 
			//rd->nodeto = tp->nds[rd->nodeto].offset;
			rd->nodeto = rev[rd->nodeto->offset];
		tp->rds[r] = rd;
	}
/*
	for (n=0; n< tp->nnodes; n++) {
		tp->nds[n].offset = n;
	}
*/
	tile_road_sort(tp);
	//printf("indexing \n");
	//tile_dmp(tp,1);
	tile_reindex_roads(tp);

	//printf("reindexed \n");
	//tile_dmp(tp,1);
}

void tile_link_roads(tile_t *tp)
{
	int r;

	for (r=0; r< tp->nroads; r++) {
		road_t *rd = tp->rds[r];
		linkrefelm_t *lre = linkref_get(gen.lr, -1, rd->nodefrom->id);
		if (!lre) {
			printf("Could not find %d:%d\n", tp->num, rd->nodefrom->id);
			linkref_dmp(gen.lr);
		}
		//rd->nodefrom = lre->nd;

		lre = linkref_get(gen.lr, -1 , rd->nodeto->id);
		if (!lre) {
			printf("Could not find %d:%d\n", tp->num, rd->nodeto->id);
			linkref_dmp(gen.lr);
		}
		//printf("Linking %p to %d\n", rd, lre->nd->id);
		//rd->nodeto = lre->nd;
		if (rd->nodeto->rds) 
			rd->nodeto->rds = (road_t **)realloc(rd->nodeto->rds, sizeof(road_t *)*lre->nd->nroads+1);
		else
			rd->nodeto->rds = (road_t **)calloc(sizeof(road_t *),rd->nodeto->nroads+1);
	
		rd->nodeto->rds[rd->nodeto->nroads] = rd;
		rd->nodeto->nroads++;
		//printf("rds %d %p\n", lre->nd->nroads, lre->nd->rds);
	}

}

void tile_node_sort(tile_t *tp)
{
	qsort(tp->nds,tp->nnodes,sizeof(node_t *),node_cmp);
}

void tile_road_sort(tile_t *tp)
{
	qsort(tp->rds,tp->nroads,sizeof(road_t *),road_cmp);
}

/*
tile_t *tiles_add(gen_t *g, tile_t **tiles, int T, int tileid)
{
	int n,r;
	int tilenos[T];
	int t;
	int L;
	int p=0;
	if (TESTTILES(tileid)) p = 1;

	//printf("Tiles add %d --------\n", tileid);

	for (t=0; t< T; t++) {
		if (tiles[t] != NULL) {
			//tile_dmp(tiles[t],1);
			if (p) tile_dmp(tiles[t],1);
			L = levelinfo.level(tiles[t]->num);
			tilenos[t]=tiles[t]->num;
		} else {
			tilenos[t]=0;
		}
	}
	tile_t *comb = tile_ist(tileid);
	int prevnodes=0;

	// add the nodes for all tiles
	for (t=0; t< T; t++) {
		tile_t *tp = tiles[t];
		if (!tp) continue; // may be empty

		// just copy all nodes to the new tile 
		for (n=0;n<tp->nnodes;n++) {
			node_t *nd = (node_t *)calloc(sizeof(node_t),1);
			*nd = *tp->nds[n];
			nd->nroads=0;
			nd->rds = NULL;
			nd->tile= comb;
			nd->pnode = tp->nds[n];
			tp->nds[n]->pnode = nd;
			tile_add_node(comb,nd);
		}
		// then fix the references, some become internal, others 
		// stay external, those are fixed with tile_fix_externals();
		for (r=0;r<tp->nroads;r++) {
			road_t *rd = tp->rds[r];
			road_t *newroad = (road_t *)calloc(sizeof(road_t),1);
			*newroad = *rd;
			int internal=0;
			for (int t2=0; t2< T; t2++) {
				if (rd->nodeto->tile->num == tilenos[t2]) {
					internal=1;
					break;
				}
			}
			newroad->keep=1;
			if (internal==1) { // found,internal node
				newroad->nodeto->tile = comb;
			} else {
				newroad->nodeto->tile = tcache_get(tc[L], levelinfo.parent(rd->nodeto->tile->num));
			}
			//rd.nodefrom += prevnodes;
			//printf("Adding %d %d\n", prevnodes,offset);
			tile_add_road(comb,newroad);
		}
		prevnodes += tp->nnodes;
	}

	comb->dirty=1;
	
	tile_dmp(comb,1);
	if (p) tile_dmp(comb,1);
	return comb;
}
*/

/* add src tile to dst */
/*
void tile_add(gen_t *g,tile_t *dst, tile_t *src)
{
	int n,r;
	int *index=NULL;

	int dstnodes = dst->nnodes;
	// add the nodes in 
	for (n=0;n<src->nnodes;n++) {
		node_t nd = *src->nds[n];
		gen_tile_add_node(g,dst,nd);
	}
	for (r=0;r<src->nroads;r++) {
		road_t *rd = (road_t *)calloc(sizeof(road_t),1);
		*rd = *src->rds[r];
		//if (rd.nodeto->tile != src->num) { // external road 
			// no need to renumber, handle refs later
		//} else {
			rd->nodefrom += dstnodes; 
			rd->nodeto += dstnodes;
			int32_t pnum = levelinfo.parent(rd->nodeto->tile->num);
			rd->nodeto->tile = tcache_get(tc[rd->nodeto->tile->level-1], pnum);
		//}
		tile_add_road(dst,rd);
	}
	//index = (int *)calloc(dst->nnodes,sizeof(int));
	//tile_node_sort(dst);
	//for (r=0;r<src->nroads;r++) {
		//road_t rd = src->rds[r];
		//rd.from = nd
	//}
	
	tile_reindex_roads(dst);
	//tile_dmp(dst,1);

	free(index);
}
*/

int32_t tile_add_node(tile_t *tp, node_t *nd)
{
	int n = tp->nnodes++;
	if ((n % RALLOCSIZE) == 0) {
		if (tp->nds) 
			tp->nds=(node_t **)realloc(tp->nds, sizeof(node_t *)*(tp->nnodes+RALLOCSIZE));
		else
			tp->nds=(node_t **)calloc(sizeof(node_t *),tp->nnodes+RALLOCSIZE);
	}
	nd->tile=tp;
	nd->offset = n;
	tp->nds[n] = nd;
	//printf("Setting %d\n", nd.id);
	return n;
	//printf("Offset now %d\n", tp->nds[n]->offset);
}

int32_t gen_tile_add_node(gen_t *gp,tile_t *tp, node_t nd)
{
	int n = tp->nnodes++;
	if ((n % RALLOCSIZE) == 0) {
		if (tp->nds) 
			tp->nds=(node_t **)realloc(tp->nds, sizeof(node_t *)*(tp->nnodes+RALLOCSIZE));
		else
			tp->nds=(node_t **)calloc(sizeof(node_t *),tp->nnodes+RALLOCSIZE);
	}
	nd.tile=tp;
	nd.offset = n;
	tp->nds[n] = (node_t *)calloc(sizeof(node_t),1);
	*tp->nds[n] = nd;
	linkref_set(gp->lr,nd.tile->num,nd.id,tp->nds[n]);
	//printf("Setting %d\n", nd.id);
	return n;
	//printf("Offset now %d\n", tp->nds[n]->offset);
}

void tile_add_bareroad(gen_t *gp, tile_t *tp, bareroad_t rd, double len)
{
	int r = tp->nroads++;
	//printf("Add road to %d\n", tp->num);
	if ((r % RALLOCSIZE) == 0) {
		if (tp->rds) 
			tp->rds=(road_t **)realloc(tp->rds, sizeof(road_t *)*(tp->nroads+RALLOCSIZE));
		else
			tp->rds=(road_t **)calloc(sizeof(road_t *),tp->nroads+RALLOCSIZE);
	}
	tp->rds[r] = (road_t *)calloc(sizeof(road_t),1);
	tp->rds[r]->keep=1;
	tp->rds[r]->done=0;

	linkrefelm_t *lr = linkref_get(gen.lr, -1, rd.nodeto);
	tp->rds[r]->nodeto = lr->nd;
	tp->rds[r]->nodeto->tile = tcache_get(tc[tp->level], rd.tileto);
	tp->rds[r]->nodeto->keep=1;
	tp->rds[r]->dir = rd.dir;
	tp->rds[r]->type = rd.type;

	lr = linkref_get(gen.lr, -1, rd.from);
	tp->rds[r]->nodefrom = lr->nd;
	tp->rds[r]->nodefrom->keep=1;

	if (lr->nd->rds) 
		lr->nd->rds = (road_t **)realloc(lr->nd->rds,sizeof(road_t *)*(lr->nd->nroads+1));
	else
		lr->nd->rds = (road_t **)calloc(sizeof(road_t *),lr->nd->nroads+1);

	lr->nd->rds[lr->nd->nroads] = tp->rds[r];
	lr->nd->nroads++;

	tp->rds[r]->len = (int) ((len*10.0)+0.5); // meters to decimeters
	if (tp->rds[r]->len==0) tp->rds[r]->len=1; // minimum cost
	//tp->nds[find->offset].lastroad++;
}

int tile_add_road(tile_t *tp, road_t *rd)
{
	//printf("Bevoor\n");
	//tile_dmp(tp,1);
	int r;

	for (r=0; r< rd->nodefrom->nroads; r++) {
		road_t *rd2 = rd->nodefrom->rds[r];
		if (rd2->nodeto == rd->nodeto && rd2->dir==rd->dir) {
			//lprintf(LOG_DEBUG, (char *)"Already a road to %d altering length from %d to %d\n", rd->nodeto->offset, rd2->len, rd->len);
			if (rd2->len > rd->len) {
				rd2->len = rd->len;
			} 
			return rd->len;
		}
	}

	r = tp->nroads++;
	if ((r % RALLOCSIZE) == 0) {
		if (tp->rds) 
			tp->rds=(road_t **)realloc(tp->rds, sizeof(road_t *)*(tp->nroads+RALLOCSIZE));
		else
			tp->rds=(road_t **)calloc(sizeof(road_t *),tp->nroads+RALLOCSIZE);
	}
	tp->rds[r] = rd;

	r= rd->nodefrom->nroads++;
	if ((r % RALLOCSIZE) == 0) {
		if (rd->nodefrom->rds) 
			rd->nodefrom->rds = (road_t **)realloc(rd->nodefrom->rds,sizeof(road_t *)*(rd->nodefrom->nroads+RALLOCSIZE));
		else
			rd->nodefrom->rds = (road_t **)calloc(sizeof(road_t *),rd->nodefrom->nroads+RALLOCSIZE);
	}

	rd->nodefrom->rds[r] = rd;
	//rd->nodefrom->nroads++;
	//printf("Achter\n");
	//tile_dmp(tp,1);

	/*
	if (rd->nodeto->rds) 
		rd->nodeto->rds = (road_t **)realloc(rd->nodeto->rds,sizeof(road_t *)*(rd->nodeto->nroads+1));
	else
		rd->nodeto->rds = (road_t **)calloc(sizeof(road_t *),rd->nodeto->nroads+1);

	rd->nodeto->rds[rd->nodeto->nroads] = rd;
	rd->nodeto->nroads++;
	*/

	//tp->rds[r].keep=1;
	//tp->nds[find->offset].lastroad++;
	return rd->len;
}

tile_t *tile_read(FILE * fp,int tileno)
{
	int t;
	tile_t *tp = tile_ist(tileno);
	//printf("reading tile %d\n", tp->num);
	tp->nroads = io.read_uint32(fp);
	if (tp->nroads) {
		tp->rds = (road_t **)calloc(sizeof(road_t *),tp->nroads);
		for (t=0;t< tp->nroads; t++) {
			tp->rds[t] = read_road(tp,tc[tp->level],fp);
		}
	}
	tp->nnodes = io.read_uint32(fp);
	int count=0;
	if(tp->nnodes) {
		tp->nds = (node_t **)calloc(sizeof(node_t *),tp->nnodes);
		for (t=0;t< tp->nnodes; t++) {
			tp->nds[t] = read_node(tc[tp->level],fp);
			tp->nds[t]->nroads=count;
			//count += tp->nds[t]->lastroad;
			//tp->nds[t]->lastroad=(tp->nds[t]->firstroad+tp->nds[t]->lastroad)-1;
			tp->nds[t]->keep = 1;
		}
	}
	return tp;
}

node_t *tile_to_node(tile_t *tp, road_t rd)
{
	if (rd.nodeto->tile != tp) return NULL;
	return rd.nodeto;
}

node_t *level_to_node(int level, road_t rd)
{
	tile_t *totile = rd.nodeto->tile;

	return tile_to_node(totile,rd);
}

int tile_reverse_road(tile_t *tp, int road)
{
	int r;
	road_t rd = *tp->rds[road];
	node_t fromnode = *rd.nodefrom;
	node_t tonode = *rd.nodeto;

	// only inside tile  !!
	if (rd.nodeto->tile != tp) return -1;

	for (r = 0; r< tonode.nroads; r++) {
		road_t rdback= *tonode.rds[r];
		if (rdback.nodeto->tile!=tp) continue;
		if (rdback.nodeto == rd.nodeto) return r;
	}

	return -1;
}

// get nth road, skipping external references
road_t *node_nth_road(node_t *nd, int count)
{
	if (count >= nd->nroads) return NULL;

	return nd->rds[count];
}

road_t tile_nth_road(tile_t *tp, node_t nd, int count)
{
	road_t rd={0};

	rd.nodefrom= NULL; // error condition
	if (count >= nd.nroads) return rd;

	return *tp->rds[count];
}

int find_endpoints_empty(tile_t *tp, node_t *from, node_t *endnodes[2], int endroads[2])
{
	return 0;
}


/* get the rth road that is marked keep within the node */
road_t *node_get_keep_road(tile_t *tp, node_t *nd,int roadn)
{
	int r;
	int nrds=0;
	for (r=0; r< nd->nroads; r++) {
	printf(": %d %d or %d != %d\n", r, nd->rds[r]->keep==1 , nd->rds[r]->nodeto->tile->num , tp->num);
		if (nd->rds[r]->keep==1 || (nd->rds[r]->nodeto->tile != tp)) {
			if (nrds==roadn) { 
				printf("Ret %d %p\n", r, nd->rds[r]);
				return nd->rds[r];
			}
			nrds++;
		}
	}

	return NULL;
}

road_t *node_get_road(tile_t *tp, node_t *nd,int roadn)
{
	/*
	int r;
	int nrds=0;
	for (r=nd->firstroad; r<= nd->lastroad; r++) {
		if (nrds==roadn) return &tp->rds[r];
		nrds++;
	}

	return NULL;
*/

	return node_get_keep_road(tp,nd,roadn);
}

/* get the number of roads that are marked keep within the node */
int node_get_roads(tile_t *tp, node_t *nd)
{
	/*
	int r;
	int nrds=0;
	for (r=nd->firstroad; r<= nd->lastroad; r++) {
		nrds++;
	}

	return nrds;
*/
	return node_get_keep_roads(tp,nd);
}

int node_get_keep_roads(tile_t *tp, node_t *nd)
{
	int r;
	int nrds=0;
	for (r=0; r< nd->nroads; r++) {
	printf("%d %d or %d != %d\n", r, nd->rds[r]->keep==1 , nd->rds[r]->nodeto->tile->num , tp->num);
		if (nd->rds[r]->keep==1 || (nd->rds[r]->nodeto->tile != tp)) {
				nrds++;
		}
	}

	//printf("%d roads\n", nrds);

	return nrds;
}

int node_test_keep_roads(tile_t *tp, node_t *nd)
{
	int nrds= node_get_keep_roads(tp,nd);

	if (nrds == 2) {
		road_t *a = node_get_keep_road(tp,nd,0);
		road_t *b = node_get_keep_road(tp,nd,1);
		if (!is_reverse_dir(a,b) ||
			a->type != b->type) return 0;
		else 
			return 1;
	}

	return 0;
}

int node_test_roads(tile_t *tp, node_t *nd)
{
	return node_test_keep_roads(tp,nd);

/*
	int nrds= node_get_roads(tp,nd);

	if (nrds == 2) {
		road_t *a = node_get_road(tp,nd,0);
		road_t *b = node_get_road(tp,nd,1);
		if (!is_reverse_dir(a,b) ||
			a->type != b->type) return 0;
		else 
			return 1;
	}

	return 0;
*/
}

/* get all external roads for the given node */
int node_get_external_roads(tile_t *tp, node_t *nd)
{
	int r;
	int nrds=0;
	for (r=0; r< nd->nroads; r++) 
		// needs to be 'keep' or 'external'
		if (nd->rds[r]->nodeto->tile != tp) nrds++;

	return nrds;
}

/* get any loose midpoint, the road it is on should be simplified */
node_t *tile_get_any_midpoint(tile_t *tp)
{
	node_t *startnode;
	int n, nrds, exts;
	for (n=0; n< tp->nnodes; n++) {
		startnode = tp->nds[n];
		if (startnode->keep == 0) continue; 
		nrds= node_get_roads(tp, startnode);
		exts= node_get_external_roads(tp, startnode);
		if (nrds == 2 && exts == 0) {
			road_t *r1 = node_get_road(tp,startnode,0);
			road_t *r2 = node_get_road(tp,startnode,1);
			if ( (r1->nodeto->tile != r2->nodeto->tile || r1->nodeto!=r2->nodeto)
#ifdef USE_TYPES
				&& is_reverse_dir(r1,r2) 
				&& r1->type == r2->type
#endif
				 ) 
			//printf("Start point for join road is %d\n", startnode->offset);
				return startnode;
		}
	}
	return NULL; // none (left)
}

/* get any loose endpoint, the road leading to it should be removed */
node_t *tile_get_any_endpoint(tile_t *tp)
{
	node_t *startnode;
	int n, nrds, exts;
	int p = TESTTILES(tp->num);
	p=1;
	for (n=0; n< tp->nnodes; n++) {
		startnode = tp->nds[n];
		if (startnode->keep == 0) continue; 
		nrds= node_get_keep_roads(tp, startnode);
		exts= node_get_external_roads(tp, startnode);
		if (nrds == 1 && exts == 0) {
			if (p) printf("Start point for delete road is %d\n", startnode->offset);
			return startnode;
		}
	}
	return NULL; // none (left)
}

int whipe_deadends(tile_t *tp)
{
	int r;
	node_t *nd, *lastnode;
	road_t *rd;
	road_t *backroad=NULL;
	road_t *fwdroad=NULL;
	int nrds;
	int exts;

	int p = TESTTILES(tp->num);

	//tile_dmp(tp,1);

	nd = tile_get_any_endpoint(tp);
	if (!nd) return 0;
	lastnode = nd; // to test which way we go
	// for this functions comments : 
	// o is open node, x = delete flagged node , 
	// + is open road, - is deleted
	//         |                  nd,lastnode
	// ....++++o++++++++++o++++++++++o

	nrds = node_get_keep_roads(tp,nd);
	while (1) {  // we already know nd is the endpoint
		nd->keep=0;
		// now find the 'other' road
		for (r=0; r< nrds; r++) { 
			rd = node_get_keep_road(tp,nd,r);
			if (rd->nodeto->tile != tp|| rd->nodeto->offset != lastnode->offset) fwdroad=rd;
			else 
				backroad=rd;
		}
		// step 1
		//         +                    fwdroad   nd,lastnode
		// ....++++o+++++++++++++++o+++++++++++++++x
		// step 2
		//         +   fwdroad  nd,lastnode backroad
		// ....++++o+++++++++++++++x---------------x
		if (backroad) backroad->keep=0; // first time there is no backroad
		rd = fwdroad;
		if(p) printf("Walking to node %d:%d\n", rd->nodeto->tile->num, rd->nodeto->offset);
		lastnode = nd;
		nd = rd->nodeto;
		nrds = node_get_keep_roads(tp,nd);
		exts = node_get_external_roads(tp,nd);
		fwdroad->keep=0;
		// end of road test, either nrds == 1 (deadend, whipe complete road)
		// or nrds > 2, (junction, stop here)
		if (nrds!=2 || exts> 0) break; 
	}
	// handle last node, 
	for (r=0; r< nrds; r++) { 
		rd = node_get_keep_road(tp,nd,r);
		if (rd->nodeto->tile != tp|| rd->nodeto->offset != lastnode->offset) fwdroad=rd;
		else 
			backroad=rd;
	}
	//printf("Node %d:%d (%d) has %d roads and %d exts\n", tp->num, nd->offset, nd->keep, nrds, exts);
	// backroad was not deleted yet , so if this nodes has 1 internal
	// road left, it's should go as well (completely whipe road)
	if (nrds==1 && exts == 0) nd->keep=0;
	if (backroad) backroad->keep=0;
	//printf("Node %d:%d (%d) has %d roads and %d exts\n", tp->num, nd->offset, nd->keep, nrds, exts);
	// mark last point as well
	return 1;
}

/* routing within tile, do not go to external tiles */
int find_endpoints(tile_t *tp, node_t *from, node_t *endnodes[2], int endroads[2])
{
	int len=0;
	int r;
	node_t *nd, *ln; // node, lastnode
	road_t *rd=NULL;
	road_t *lr=NULL; // leftroad
	road_t *rr=NULL; // rightroad
	node_t *sn=NULL; // startnode
	int nrds=0;
	int exts=0;
	endroads[0]= -1;
	endroads[1]= -1;
	int p = TESTTILES(tp->num);

	sn = tile_get_any_midpoint(tp);

	//     |               sn    |        
	// ----o----o-----o-----o----o------- 
	nd = sn;
	if (!nd) return -1;
	//printf("Start point is %d\n", sn->offset);
	ln = nd; // to test which way we go

	nd->keep=0;
	//nrds = node_get_keep_roads(tp,nd);
	nrds = node_get_roads(tp,nd);
	lr = node_get_road(tp,nd,0);
	rr = node_get_road(tp,nd,1);
#ifdef USE_TYPES
	int ldir=lr->dir;
	int rdir=rr->dir;
	int ltype=lr->type;
	int rtype=rr->type;
#endif

#ifdef USE_TYPES
	if ((ldir == DIR_TOFROM && rdir != DIR_FROMTO) 
		|| (ldir == DIR_FROMTO && rdir != DIR_TOFROM)
		|| (ldir == DIR_BOTH && rdir != DIR_BOTH) 
		|| (rdir == DIR_BOTH && ldir != DIR_BOTH) ) {
		printf("Problem dirs %d to %d\n", lr->nodeto->offset, rr->nodeto->offset);
		printf("value %d to %d\n", ldir, rdir);
		exit(0);
	}
	if (ltype != rtype) {
		printf("Problem types %d to %d\n", lr->nodeto->offset, rr->nodeto->offset);
		printf("value %d to %d\n", ltype, rtype);
		exit(0);
	}
#endif

	nd = lr->nodeto;

	//nrds = node_get_keep_roads(tp,nd);
	int valid = node_test_roads(tp,nd);
	exts = node_get_external_roads(tp,nd);
	//printf("Node %d has %d and %d\n", nd->offset, nrds, exts);
	lr->keep=0;
	while (valid && exts==0) {
		len += lr->len;
		nd->keep=0;

		for (r=0; r< 2; r++) { 
			rd = node_get_road(tp,nd,r);
			if (rd && is_backroad(tp,lr,rd,ln)) rr = rd;
			else 
				lr=rd; 
		}
		if (lr==NULL) tile_dmp(tp,1);
#ifdef USE_TYPES
		if (lr->type != rr->type) { 
			//printf("Break on type\n");
			nd->keep=1;
			break;
		}
#endif
		rr->keep=0; 
		lr->keep=0;

#ifdef USE_TYPES
		if (lr->dir != ldir 
			//|| lr->type != ltype
		) {
			//printf("left road changed direction, end here %d != %d\n", lr->dir, ldir);
			break;
		}
#endif
		
		if (p) printf("Walk left to node %d len now %d\n", nd->offset, len);
		rd = lr; 
		ln = nd;  // remember last node
		nd = rd->nodeto;
		//nrds = node_get_keep_roads(tp,nd);
		valid = node_test_roads(tp,nd);
		exts = node_get_external_roads(tp,nd);
	}
	for (r=0; r<nd->nroads; r++) {
		if (nd->rds[r]->nodeto->tile == ln->tile && 
		    nd->rds[r]->nodeto->offset == ln->offset) {
			nd->rds[r]->keep = 0;
			len += nd->rds[r]->len;
			endroads[0]=r;
		}
	}
	endnodes[0] = nd;

	ln = sn;  // reset last node to start node

	nrds = node_get_roads(tp,sn); // should be 1 now,
	if (nrds != 1) //  we removed the other in the first step
	{
		//printf("found circular reference %d \n", nd->offset);
		//printf("delete !!\n");
		nd->keep=0;
		endroads[0] = endroads[1] = -1;
		endnodes[0] = endnodes[1] = NULL;
		return 0;
	}
	rr = node_get_road(tp,sn,0); // so take the first !!
	nd = rr->nodeto;

	//nrds = node_get_keep_roads(tp,nd); 
	valid = node_test_roads(tp,nd);
	exts = node_get_external_roads(tp,nd);
	//printf("Test %d with %d and %d\n", nd->offset, nrds, exts);
	rr->keep=0;
	while (valid && exts==0) {
		len += rr->len;
		nd->keep=0;

		for (r=0; r< 2; r++) { 
			rd = node_get_road(tp,nd,r);
			if (rd && is_backroad(tp,rr,rd,ln)) lr = rd;
			else 
				rr=rd; 
		}
#ifdef USE_TYPES
		if (rr->dir != rdir) {
			//printf("right road changed direction, split here %d != %d\n", rr->dir, rdir);
			break;
		}
		if (rr->type != rtype) {
			//printf("right road changed type, %d %d:%d split here %d != %d\n", rr->nodefrom, rr->nodeto->tile, rr->nodeto, rr->type, rtype);
			//tile_dmp(tp,1);
			//nd->keep=1;
			break;
		}
#endif
		lr->keep=0;
		rr->keep=0;
		
		rd = rr;
		if (p) printf("Walk right to node %d len now %d\n", nd->offset, len);
		ln = nd; // shift last node
		nd = rd->nodeto;
		//nrds = node_get_keep_roads(tp,nd);
		valid = node_test_roads(tp,nd);
		exts = node_get_external_roads(tp,nd);
	}
	//printf("--- testing %d:%d\n", lastnode->tile, lastnode->offset);
	for (r=0; r<nd->nroads; r++) {
		if (nd->rds[r]->nodeto->tile == ln->tile && 
		    nd->rds[r]->nodeto->offset == ln->offset) {
			nd->rds[r]->keep = 0;
			endroads[1]=r;
			len += nd->rds[r]->len;
		}
	}
	endnodes[1] = nd;
	return len;
}


/* routing within tile, do not go to external tiles */
int32_t tile_test_path(tile_t *tp, node_t *from, node_t *to)
{
	int r;
	if (!from || !to) return 0;

	int p=0;
	if (TESTTILES(tp->num)) p = 1;

	//cost_dmp(cost);

	while (from->offset != to->offset) {
		int found=0;
		if (p) printf("to now %d\n", to->offset);
		for (r=0; r< to->nroads; r++) {
			road_t *rd = to->rds[r];
			if (p) printf("trying road %d from %d to %d:%d\n", r, tp->num, rd->nodeto->tile->num, rd->nodeto->offset);
			if (tp!= rd->nodeto->tile) continue;
			//costentry_t *cto = cost_get(cost, to);
			node_t *nd = rd->nodeto;
			//costentry_t *inner = cost_get(cost, nd);
			if (p) printf("costs %d + %d is %d\n", nd->cost, ROADCOST((*rd)), to->cost);
			if (nd->cost+ROADCOST((*rd)) == to->cost) {
				int matchroad=0;
				for (int r2=0; r2 < nd->nroads; r2++) {
					if (p) printf("Testing road %d to %d:%d %d\n", r2, tp->rds[r2]->nodeto->tile->num,tp->rds[r2]->nodeto->offset, tp->num);
					if (nd->rds[r2]->nodeto->tile == tp&&
					    nd->rds[r2]->nodeto->offset == to->offset) {
						if (p) printf("Actually found %d\n", to->offset);
						matchroad=1;
						break;
					}
				}
				if (!matchroad) continue;
				found=1;
				to = nd;
				break;
			}
		}
		if (!found) return 0;
	}
	if (p) printf("Found from node %d == %d\n", to->offset, from->offset);
	return 1;
}

int32_t tile_test_path_cost(tile_t *tp, cost_t *cost, node_t *from, node_t *to)
{
	int r;
	if (!from || !to) return 0;

	//cost_dmp(cost);

	while (from->offset != to->offset) {
		int found=0;
		//printf("to now %d\n", to->offset);
		for (r=0; r< to->nroads; r++) {
			road_t *rd = to->rds[r];
			if (tp!= rd->nodeto->tile) continue;
			costentry_t *cto = cost_get(cost, to);
			node_t *nd = rd->nodeto;
			costentry_t *inner = cost_get(cost, nd);
			if (!cto || !inner) {
				//printf("Failed to find %d\n", from->offset);
				continue;
			}
			else {
				if (inner->cost+ROADCOST((*rd)) == cto->cost) {
					int matchroad=0;
					for (int r2=0; r2 < inner->node->nroads; r2++) {
						//printf("Testing road %d to %d:%d %d\n", r2, tp->rds[r2].nodeto->tile,tp->rds[r2].nodeto, tp->num);
						if (inner->node->rds[r2]->nodeto->tile == tp&&
						    inner->node->rds[r2]->nodeto->offset == to->offset) {
							//printf("Actually found %d\n", to->offset);
							matchroad=1;
							break;
						}
					}
					if (!matchroad) continue;
					found=1;
					to = nd;
					break;
				}
			}
		}
		if (!found) return 0;
	}
	//printf("Found from node %d == %d\n", to->offset, from->offset);
	return 1;
}


int32_t level_traverse_path(int level, node_t *from, node_t *to,path_fnc_t what, void *data,int costlimit)
{
	if (!from || !to) return 0;

	int counter=0;

	node_t *cnode;
	node_t *next;
	node_t *nd;
	road_t *r;

	tile_t *tp = from->tile;
	
	next = tp->nds[from->offset];

	while (next->cost > 0) {
	        counter=0;

		if (!next) {
			return -1;
		}
		if (next->cost > costlimit) {
			return -1;
		}
		// could be run with b==NULL, which means complete the dijkstra
		if (to && next == to) {
			return next->cost;
		}
		cnode=NULL;
	//if (count >= nd->nroads) return NULL;
//
	//return nd->rds[count];
		for (int rr=0; rr< next->nroads; rr++) {
			r = next->rds[rr];
		//for (r = node_nth_road(next,counter); r != NULL; r = node_nth_road(next,counter)) {
#ifdef VERBOSE
			printf("traverse %d:%d\n", r->nodeto->tile->num, r->nodeto->offset);
#endif
			counter++;
			nd = r->nodeto;
            if (r->dir == DIR_TOFROM) continue;
			if (nd->rank >0 && nd->rank > next->rank) continue;
			if (nd->keep==0)  {
#ifdef VERBOSE
				printf("keep is %d\n", nd->keep);
#endif
				continue;
			}
            lprintf(LOG_DEBUG, (char *)"cost is %d = %d + %d (%d)\n", next->cost , nd->cost , ROADCOST(*r), nd->cost + ROADCOST(*r));
            if (next->cost == nd->cost + ROADCOST(*r) && nd->cost != -1 )
            {
            	lprintf(LOG_DEBUG, (char *)"node now %d:%d \n", tp->num, nd->offset);
                cnode = nd;
				//printf("%p\n", &r);
                what(level,next,r,data);
				break; // this will effectively take the first shortest
						// path if there are more equally short paths
			}
		}
		if (!cnode) return -1;
		next = cnode;
		tp = next->tile;
	}
    what(level,next,NULL,data);
	//from->keep= to->keep= 1;

	return 0;
}

/* routing within tile, do not go to external tiles */
int32_t tile_mark_path(tile_t *tp, node_t *from, node_t *to)
{
	int r;
	if (!from || !to) return 0;

	//cost_dmp(cost);
	while (from->offset != to->offset) {
		int found=0;
		//printf("to now %d\n", to->offset);
		for (r=0; r< to->nroads; r++) {
			//printf("r: %d\n", r);
			road_t *rd = to->rds[r];
			if (tp != rd->nodeto->tile) continue;
			//costentry_t *cto = cost_get(cost, to);
			node_t *nd = rd->nodeto;
			//costentry_t *inner = cost_get(cost, nd);
			//printf("cost -get %d is %d\n", to->offset, cto->node->offset);
			//printf("Trying road %d is %d + %d = %d\n", r, inner->cost, rd->len, cto->cost);
			if (nd->cost+ROADCOST((*rd)) == to->cost) {
				//printf("road %d from %d to %d\n", r, to->offset, nd->offset);
				int matchroad=0;
				for (int r2=0; r2 < nd->nroads; r2++) {
					//printf("Testing road %d to %d:%d %d\n", r2, tp->rds[r2].nodeto->tile,tp->rds[r2].nodeto, tp->num);
					if ((nd->rds[r2]->nodeto->tile == tp &&
					    nd->rds[r2]->nodeto->offset == to->offset) ||
						nd->rds[r2]->nodeto->tile != tp) {
						nd->rds[r2]->keep=1;
						//printf("Marking back road %d\n", r2);
						matchroad=1;
						break;
					}
				}
				if (!matchroad) continue;
				found=1;
				nd->keep=1;
				to->keep=1;
				//printf("Marking road %d\n", r);
				//printf("Marking node %d\n", to->offset);
				//printf("Marking node %d\n", nd->offset);
				tp->rds[r]->keep=1;
				// and mark back road !!
				for (int r2=0; r2< nd->nroads; r2++) {
					if ((nd->rds[r2]->nodeto->tile == tp&&
						nd->rds[r2]->nodeto->offset == to->offset) || 
						(nd->rds[r2]->nodeto->tile != tp) ) {
						//printf("Marking back road %d\n", r2);
						tp->rds[r2]->keep=1;
					}
				}
				to = nd;
				break;
			}
		}
		if (!found) break;
	}
	//from->keep= to->keep= 1;

	return 0;
}

int32_t tile_mark_path_cost(tile_t *tp, cost_t *cost, node_t *from, node_t *to)
{
	int r;
	if (!from || !to) return 0;

	//cost_dmp(cost);

	while (from->offset != to->offset) {
		int found=0;
		//printf("to now %d\n", to->offset);
		for (r=0; r< to->nroads; r++) {
			//printf("r: %d\n", r);
			road_t *rd = to->rds[r];
			if (tp != rd->nodeto->tile) continue;
			costentry_t *cto = cost_get(cost, to);
			node_t *nd = rd->nodeto;
			costentry_t *inner = cost_get(cost, nd);
			if (!cto || !inner) {
				//printf("Failed to find %d\n", from->offset);
				continue;
			}
			else {
				//printf("cost -get %d is %d\n", to->offset, cto->node->offset);
				//printf("Trying road %d is %d + %d = %d\n", r, inner->cost, rd->len, cto->cost);
				if (inner->cost+ROADCOST((*rd)) == cto->cost) {
					//printf("road %d from %d to %d\n", r, cto->node->offset, inner->node->offset);
					int matchroad=0;
					for (int r2=0; r2 < inner->node->nroads; r2++) {
						//printf("Testing road %d to %d:%d %d\n", r2, tp->rds[r2].nodeto->tile,tp->rds[r2].nodeto, tp->num);
						if (inner->node->rds[r2]->nodeto->tile == tp &&
						    inner->node->rds[r2]->nodeto->offset == to->offset) {
							inner->node->rds[r2]->keep=1;
							//printf("Marking back road %d\n", r2);
							matchroad=1;
							break;
						}
					}
					if (!matchroad) continue;
					found=1;
					inner->node->keep=1;
					cto->node->keep=1;
					//printf("Marking road %d\n", r);
					//printf("Marking node %d\n", cto->node->offset);
					//printf("Marking node %d\n", inner->node->offset);
					tp->rds[r]->keep=1;
					// and mark back road !!
					for (int r2=0; r2< inner->node->nroads; r2++) {
						if (inner->node->rds[r2]->nodeto->tile == tp &&
							inner->node->rds[r2]->nodeto->offset == cto->node->offset) {
							//printf("Marking back road %d\n", r2);
							inner->node->rds[r2]->keep=1;
						}
					}
					to = nd;
					break;
				}
			}
		}
		if (!found) break;
	}

	return 0;
}

cost_t *level_edsger_cost_fwd(int level, node_t *from,int h)
{
	return level_edsger_cost(level,from,h,DIR_TOFROM);
}

cost_t *level_edsger_cost_bwd(int level, node_t *from,int h)
{	
	return level_edsger_cost(level,from,h,DIR_FROMTO);
}

cost_t *level_edsger_cost(int level, node_t *from,int h, int testdir)
{
	node_t *next;
	node_t *nd;
	road_t r = {0};

	heap_t *heap = (heap_t *)heap_ist(10000, gen_heap_fncs2);
	cost_t *cost = cost_ist(SORT_NODE);

	level_reset2(level);

	node_t *a;
	int p=0;

	// fill in rest of the nodes

	a = from;
	heap->setcost(a,0);
	heap_add(heap, a);
	//heap_dmp(heap);

	while (1) {
		if (heap_empty(heap)) {
			//printf("No endpoint found\n");
			goto cleanup;
		}
		next = (node_t *)heap_get(heap, 0);
		if (p) printf("next is %d\n", next->offset);
		
		if (!next) {
			//cost_dmp(cost);
			goto cleanup;
		}
		if (--h<=0) return cost;

		int counter=0;
		for (int rr=0; rr< next->nroads; rr++) {
			r = *next->rds[rr];
			counter++;
			//printf("[%d] counter is %d %d:%d\n", r.nodefrom, counter, r.nodeto->tile, tp->num);
			nd = r.nodeto;
			//printf("to node is %d, %d %d\n", nd->offset, r.dir, nd->eval2);
			//printf("is %d, %d\n", nd->tile, tp->num);
			if (!nd) continue;
			if (r.dir == testdir) continue; 
			if (nd->eval2) continue; // evaluated
			if (nd->cost2 == HEAP_CLEAR) { // new to the heap
				//printf("cost of %d was %d\n", nd->offset, nd->cost2);
				//nd->cost2 = ROADCOST(r);
				heap->setcost(nd, next->cost2+ROADCOST(r));
				// insert on cost
				//printf("setting %d to %d\n", nd->offset, nd->cost2);
				heap_add(heap, nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost2+ROADCOST(r) < nd->cost2) {
					heap_del(heap,nd);
					//if (p) printf("Deleting %d ---\n", nd->cost2);
					if (p) heap_dmp(heap);
					heap->setcost(nd, next->cost2+ROADCOST(r));
					//if (p) printf("And adding as %d ---\n", nd->cost2);
					if (p) heap_dmp(heap);
					heap_add(heap,nd);
					cost_set(cost,nd,nd->cost2);
				}
			}
		}
		heap_del(heap,next);
		cost_add(cost,next,next->cost2);
		next->eval2 = 1;
	}

cleanup:
	if (heap) heap_rls(heap);
	//cost_dmp(cost);
	return cost; // cost
}

cost_t *tile_edsger_cost(tile_t *tp, node_t *from, node_t *to)
{
	node_t *next;
	node_t *nd;
	road_t r = {0};
	heap_t *heap = (heap_t *)heap_ist(10000, gen_heap_fncs);
	cost_t *cost = cost_ist(SORT_NODE);

	node_t *a;
	node_t *b;
	int p=0;

	if (TESTTILES(tp->num)) p = 1;
	if (p) {
		printf("Edsger on tile %d\n", tp->num);
		tile_dmp(tp,1);
	}

	// fill in rest of the nodes
	a = tp->nds[from->offset];
	// NO: does not have to be a real node !!
	b = to;

	heap->setcost(a, 0);
	heap_add(heap, a);
	//heap_dmp(heap);

	while (1) {
		if (heap_empty(heap)) {
			//printf("No endpoint found\n");
			goto cleanup;
		}
		next = (node_t *)heap_get(heap, 0);
		if (p) printf("next is %d\n", next->offset);
		
		if (!next) {
			//cost_dmp(cost);
			goto cleanup;
		}
		// could be run with b==NULL, which means complete the dijkstra
		if (b && next->tile == b->tile && 
		    next->offset == b->offset) {
			//printf("Found to node (%d) at cost %d\n", b->offset, b->cost);
			cost_add(cost,b,b->cost);
			//heap_dmp(heap);
			//cost_dmp(cost);
			goto cleanup;
		}

		int counter=0;
		for (r = tile_nth_road(tp,*next,counter); r.nodefrom != NULL; r = tile_nth_road(tp,*next,counter))
		{
			counter++;
			//printf("[%d] counter is %d %d:%d\n", r.nodefrom, counter, r.nodeto->tile, tp->num);
			if (r.nodeto->tile != tp) continue;
			//printf("[%d] found road %d to %d:%d %d long\n", counter, r.nodefrom,r.nodeto->tile,r.nodeto,r.len);
			nd = tile_to_node(tp,r);
			//printf("to node is %d, %d %d\n", nd->offset, r.dir, nd->eval);
			//printf("is %d, %d\n", nd->tile, tp->num);
			if (!nd) continue;
			if (r.dir == DIR_TOFROM) continue; 
			if (nd->eval) continue; // evaluated
			if (nd->tile!= tp) continue;
			if (nd->cost == HEAP_CLEAR) { // new to the heap
				//printf("cost of %d was %d\n", nd->offset, nd->cost);
				//nd->cost = ROADCOST(r);
				heap->setcost(nd, next->cost+ROADCOST(r));
				// insert on cost
				//printf("setting %d to %d\n", nd->offset, nd->cost);
				heap_add(heap, nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost+ROADCOST(r) < nd->cost) {
					heap_del(heap,nd);
					if (p) printf("Deleting %d ---\n", nd->cost);
					if (p) heap_dmp(heap);
					heap->setcost(nd, next->cost+ROADCOST(r));
					if (p) printf("And adding as %d ---\n", nd->cost);
					if (p) heap_dmp(heap);
					heap_add(heap,nd);
					cost_set(cost,nd,nd->cost);
				}
			}
		}
		heap_del(heap,next);
		cost_add(cost,next,next->cost);
		next->eval = 1;
	}

cleanup:
	if (heap) heap_rls(heap);
	//cost_dmp(cost);
	return cost; // cost
}

/*
int32_t net_edsger_flat(int level, node_t *from, node_t *to)
{
	node_t *next;
	node_t *nd;
	//road_t r = clearroad;
	road_t *r;
	heap_t *heap = (heap_t *)heap_ist(10000, gen_heap_fncs);
	tile_t *tpa = tilenode_get_tile(np,to);
	tile_t *tpb = tilenode_get_tile(np,from);

	int32_t res;

	node_t *a = &tpa->nds[to->offset];
	node_t *b = &tpb->nds[from->offset];

	a->cost = 0;
	heap->add(a);
	//heap->dmp();

	while (1) {
		if (heap->empty()) {
			printf("No endpoint found\n");
			goto cleanup;
		}
		next = heap->get();
		tpa = node_get_tile(np,next);
		//printf("next is %d:%d %d\n", next->tilenode->tp->num, next->tilenode->offset, next->eval);
		
		if (!next) {
			//cost->dmp();
			delete heap;
			goto cleanup;
		}
		// could be run with b==NULL, which means complete the dijkstra
		if (b && next->tilenode->tp == b->tilenode->tp && 
		    next->tilenode->offset == b->tilenode->offset) {
			//printf("Found to node (%d) a cost %d (%d) minutes\n", b->tilenode->offset, next->cost, next->cost / 60000);
			globcost = next->cost;
			//cost->add(next);
			//heap->dmp();
			//cost->dmp();
			res.dm = next->cost;
			goto cleanup;
		}

		int counter=0;
		for (r = node_nth_road(np,tpa,next,counter); r!= NULL; r = node_nth_road(np,tpa,next,counter))
		{
			counter++;
			//printf("[%d] counter is %d %d:%d\n", r.nodefrom, counter, r.nodeto->tile, np->num);
			nd = road_to_node(np,r);
			//printf("to node is %d:%d, %d %d\n", nd->tilenode->tp->num, nd->tilenode->offset, r.dir, nd->eval);
			//printf("is %d, %d\n", nd->tile, np->num);
			if (nd->eval==1) continue;
			if (r->dir == DIR_FROMTO) continue; 
			if (nd->cost == HEAP_CLEAR) { // new to the heap
				nd->cost= next->cost+ROADCOST((*r));
				heap->add(nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost+ROADCOST(*r) < nd->cost) {
					heap->del(nd);
					nd->cost = next->cost+ROADCOST(*r);
					heap->add(nd);
					//cost->set(nd);
				}
			}
		}
		heap->del(next);
		//cost->add(next);
		//cost->dmp();
		next->eval = 1;
		//heap->dmp();
		//printf("Setting %d to %d\n", next->tilenode->offset, next->eval);
	}

cleanup:
	if (heap) delete heap;
	heap=NULL;
	//cost->dmp();
	return res; // cost
}
*/

#define WORKSIZE 100
static node_t *workarray[WORKSIZE];
int last;

int32_t level_reset_edsger(int level, node_t *to)
{
	node_t *next;
	node_t *nd;
	road_t *r;

	next = to;

	int size=0;

	int count=0;
	
	//vector<node_t *> vec;
	//vec.push_back(to);

	// workarray works by just adding to the and of the array, 
	// first the startnode is added : [start]
	// then all it's outgoing nodes are added overwriting the current slot : 
	// [start] becomes (if it has 3 outging roads) : 
	// [node1][node2][node3] 
	// size is now 3, so size-1 gives you the last element, and that gets overwritten
	// again, evaluated nodes are not added again so after growing it will shrink again
	// it's a simple heap implementation with no sorting
	workarray[size++] = to;

	while (1) {
		if (size == 0) {
			 goto done;
		}
		/*
		if (size > WORKSIZE) {
			printf("Insufficient workspace\n");
			exit(0);
		}
		*/
		next = workarray[size-1];
		//lprintf(LOG_DEBUG, (char *)"next is %d\n", next->offset);
		for (int rr=0; rr< next->nroads; rr++) {
			r = next->rds[rr];
			nd = r->nodeto;
			if (nd->cost != HEAP_CLEAR || nd->eval==1) {
				workarray[size-1] = nd; 	// overwrite last (is current next)
				size++;
			}
			nd->cost = HEAP_CLEAR; 
			nd->eval = 0;
			nd->hop = 0;
			//nd->d = 0;
			//nd->a = 0;
			nd->depth = 0;
		}
		size--;
		count++;
		
		//next->eval=0;
		//next->hop = 0;
		//next->del = 0;
		next->cost=HEAP_CLEAR;
	}
done:
	//printf("Reset %d nodes\n", count);
	return 0;
}

void countactive(const void *data, void *udata)
{
	node_t *nd = (node_t *)data;
	int *active = (int *)udata;
	
	//printf("C: %d:%d active is %d\n", nd->tile->num, nd->offset, nd->active);
	if (nd->active) *active=1;
}

void dumpparents(const void *data, void *udata)
{
	node_t *nd = (node_t *)data;
	printf("P: %d:%d active is %d\n", nd->tile->num, nd->offset, nd->active);
}

int test_active(node_t *nd)
{
	int active=0;

	//dump_parents(nd);

	treemap_walk(nd->parents, countactive, &active);
	return active;
}

void add_parent(node_t *a, node_t *b)
{
	if (a->parents==NULL) a->parents= treemap_ist(gen_nodecmp);
	treemap_add(a->parents, b);
/*
	if (a->nparents++) 
		a->parents = (node_t **)realloc(a->parents,sizeof(node_t *)*a->nparents);
	else
		a->parents = (node_t **)calloc(sizeof(node_t *),a->nparents);

	a->parents[a->nparents-1] = b;
*/
	b->p=1;

	//printf("Added : now %d\n", a->nparents);
}

int samenode(node_t *a, node_t *b)
{
	if (a->tile->num != b->tile->num || 
		a->offset != b->offset) return 0;
	return 1;
}

cost_t *FwdN(int level,node_t *nd)
{
	cost_t *cost = level_edsger_cost_fwd(level,nd,H);

	return cost;
}

cost_t *BwdN(int level,node_t *nd)
{
	cost_t *cost = level_edsger_cost_bwd(level,nd,H);

	return cost;
}

void path_stretch(path_t *P)
{
	P->nelms++;

	if (P->nelms==1) P->elms = (pathelm_t *)calloc(sizeof(pathelm_t),P->nelms);
	else P->elms = (pathelm_t *)realloc(P->elms, sizeof(pathelm_t)*P->nelms);
}

void path_dmp(path_t *P)
{
	int n;

	for (n=0; n< P->nelms; n++) {
		printf("[%d] %d:%d (%d)\n", n, P->elms[n].n->tile->num, 
			P->elms[n].n->offset, (P->elms[n].r==NULL) ? -1 : P->elms[n].r->cost);
	}
}

void wlk_paths(int level,node_t *p,node_t *nd, path_t *P)
{
	int r;
	node_t *s0, *s1;

	for (r=0; r< nd->nroads; r++) {
		road_t *rd= nd->rds[r];
		if (rd->dir == DIR_FROMTO) continue;
		node_t *from =rd->nodeto;
		if (nd->cost == from->cost + rd->cost) {
			//printf("YEP %d is %d + %d !!\n", from->cost , nd->cost , rd->cost);
			path_stretch(P);
			P->elms[P->nelms-1].n = from;
			P->elms[P->nelms-1].r = rd;

			wlk_paths(level,nd,from,P);
			if (from->cost == 0) { // endpoint found
				s0 = from;
				s1 = nd;
				/*
				printf(" done s0=%d:%d, s1=%d:%d and p is %d:%d\n", 
					s0->tile->num, s0->offset,
					s1->tile->num, s1->offset,
					p->tile->num, p->offset
					);
				*/
				// is s1 coming before P (just s1 != p)
				if (samenode(p,s1)) {
					//printf("Failed S1 ≺ P\n");
					continue;
				} 
				// is p not in the (fwd) neighbourhood of s1
				cost_t *Nfs1 = FwdN(level,s1);
				//cost_dmp(Nfs1);
				if (cost_find(Nfs1, p) != NULL) {
					//printf("Failed p ∉ N→(s1)\n");
					continue;
				} 
				// is p not in the (fwd) neighbourhood of s1
				cost_t *Nbp = BwdN(level,p);
				//cost_dmp(Nbp);
				if (cost_find(Nbp, s0) != NULL) {
					//printf("Failed s0 ∉ N←(p)\n");
					continue;
				} 

				if (overlap(P, Nfs1, Nbp) <= 1) {
					//printf("Failed P ⋂ N→(s1) ⋂ N←(p)\n");
					continue;
				} 

				printf("Passive !!\n");
				p->active=0;
				//path_dmp(P);
			} 
		} 
	}
}

void dump_parents(node_t *nd)
{
	treemap_walk(nd->parents, dumpparents, NULL);

	//for (t=0; t< nd->nparents; t++) { 
		//node_t *p = nd->parents[t];
		//printf("parent %d:%d, active is %d\n", p->tile->num, p->offset, p->active);
	//} 
}

int settle_node(int level, node_t *s0, node_t *nd)
{
	//printf("%d:%d -> Settling node %d:%d -- Path back : \n", s0->tile->num, s0->offset, nd->tile->num, nd->offset);

	//path_t p={0};
	int b=0;
	int a=0;

	//path_stretch(&p);
	//p.elms[p.nelms-1].n = nd;
	//p.elms[p.nelms-1].r = NULL;

	// b' :
	if (s0 == nd) { 
		//printf("This is s0, setting b to 0 and a to ∞\n");
		nd->b=0;
		nd->a=INT_MAX;
	} 
	//dump_parents(nd);
	if (treemap_get(nd->parents, s0)!= NULL) { 
		//printf("s0 is parent (so this is s1) %d + %d !!\n", nd->cost, nd->fradius);
		b = nd->cost + nd->fradius;
	} else 
		b = 0;

	node_t **nds = (node_t **)treemap_array(nd->parents);
	for (int t=0; t< treemap_len(nd->parents); t++) {
		if (b < nds[t]->b) b = nds[t]->b;
		if (a < nds[t]->a) a = nds[t]->a;
		//printf("a now %d\n", a);
		//printf("b now %d\n", b);
	}
	nd->b = b;
	for (int t=0; t< treemap_len(nd->parents); t++) {
		if (a == INT_MAX && (nd->cost > nd->b)) { 
			a = 0;
			node_t **subs = (node_t **)treemap_array(nds[t]->parents);
			for (int s=0; s< treemap_len(nds[t]->parents); s++) { 
				if (a < subs[s]->cost) a = subs[s]->cost;
			}
			free(subs);
			//printf("Outside neighbourhood------%d--------\n", a);
			nd->a = a;
		} else {
			nd->a=a;
		} 
	}
	free(nds);

	printf("Distance is %d\n", nd->a);
	printf("abort : (%d + %d) < %d\n", nd->a, nd->bradius, nd->cost);
	if (nd->a != INT_MAX && (nd->a + nd->bradius) <  nd->cost) return 1;

	return 0;
	//wlk_paths(level,nd,nd,&p);
	//path_dmp(&p);
}

/* routing within level, calcuate neighbourhood radius for node from */
int32_t level_edsger_radius(int level, node_t *from, int h, int dir)
{
	node_t *next;
	node_t *nd;
	road_t *r;
	heap_t *heap = (heap_t *)heap_ist(1000, gen_heap_fncs);

	node_t *a;
	
	// needed 
	level_reset(level);
	//level_check_reset(level);
	// fill in rest of the nodes
	a = from;
	a->active=1;
	a->b = 0;
	a->a = INT_MAX;

	// swap test direction
	int testdir = dir==DIR_FROMTO ? DIR_TOFROM : DIR_FROMTO;

	heap->setcost(a,0);
	heap_add(heap, a);

	//d.AddNode(a->x, a->y, a);

	while (1) {
		if (heap_empty(heap)) {
			 goto error;
		}
		next = (node_t *)heap_get(heap, 0);
	//	lprintf(LOG_DEBUG, (char *)"next is %d\n", next->offset);
	
		if (!next) {
			//cost_dmp(cost);
			goto error;
		}
		if (--h<0) {
			return heap->getcost(next);
		}

		for (int rr=0; rr< next->nroads; rr++) {
			r = next->rds[rr];
#ifdef VERBOSE
			printf("found road from %d to %d:%d %d long\n", r->nodefrom->offset,r->nodeto->tile->num,r->nodeto->offset,r->len);
#endif
			nd = r->nodeto;
			//d.AddEdge(next->x, next->y,nd->x, nd->y, r);
			//printf("node is %p\n", nd);
			if (!nd) continue;
#ifdef VERBOSE
			//printf("dir is %d\n", r->dir);
#endif
			if (r->dir == testdir) continue;  
			if (nd->eval) continue; // evaluated
			if (nd->keep==0)  {
#ifdef VERBOSE
				//printf("keep is %d\n", nd->keep);
#endif
				continue;
			}
			if (heap->getcost(nd) == HEAP_CLEAR) { // new to the heap
				heap->setcost(nd, heap->getcost(next)+ROADCOST(*r));
				heap_add(heap, nd);
				nd->tile->dirty=1;
			} else { // already in the heap
				// only set cost if lower
				if (heap->getcost(next) +ROADCOST(*r) < heap->getcost(nd)) {
					heap_del(heap,nd);
					heap->setcost(nd, heap->getcost(next)+ROADCOST(*r));
					nd->tile->dirty=1;
					heap_add(heap,nd);
				}
			}
		}
		heap_del(heap,next);
		next->eval = 1;
		next->tile->dirty=1;
	}

	if (heap) heap_rls(heap);
	//cost_dmp(cost);
	return heap->getcost(next) ; // cost
error:
	//printf("node not found, network completely evaluated\n");
	if (heap) heap_rls(heap);

	return -1;
}

void wlkfnc(const void *a, void *udata)
{
	node_t *n = *(node_t **) a;
	node_t *s0 = (node_t *)udata;
	
	// inside S0 neighbourhood
	if (n->cost < s0->fradius) return;

	int stop=0;

	printf("%d:%d (a %d) (e %d) cost %d, tslack %d stop is %d fradius is %d\n", n->tile->num, n->offset, n->active, n->eval, n->cost, n->bradius, stop, s0->fradius);
	node_t **parents = (node_t **)treemap_array(n->parents);
	if (n->slack < 0) 
		n->slack = n->bradius;
	int t;
	for (t=0; t< treemap_len(n->parents); t++) {
		road_t *rd;
		node_t *p = parents[t];
		//printf("Parent is %d\n", p->offset);
		for (int r =0; r< p->nroads; r++) { 
			if (p->rds[r]->nodeto == n) {
				//printf("Road %d long\n", p->rds[r]->len);
				rd = p->rds[r];
			} 
		} 
		int deltau = n->slack - rd->len;
		//printf("Slack is %d - %d = %d\n", n->slack, len, deltau);
		if (deltau < 0) rd->rise=1;
		else p->slack = deltau;
	} 
	treemap_array_rls(n->parents);
	//dump_parents(n);
}

int32_t level_promote(int level, cost_t *cost, node_t *from, int h)
{
	//cost_dmp(cost);
	cost_walk(cost,wlkfnc, from);
	return 0;
}

cost_t *level_edsger_cost_HH(int level, node_t *from, int h)
{
	node_t *next;
	node_t *nd;
	road_t *r;
	heap_t *heap = (heap_t *)heap_ist(1000, gen_heap_fncs);
	cost_t *cost = cost_ist(SORT_COST);

	node_t *a;

	// needed 
	level_reset_keep(level, 1);
	// fill in rest of the nodes
	a = from;
	a->active=1;
	a->b = 0; 
	a->a = INT_MAX;

	heap->setcost(a,0);
	heap_add(heap, a);

	//d.AddNode(a->x, a->y, a);

	while (1) {
		if (heap_empty(heap)) {
			 goto error;
		}
		next = (node_t *)heap_get(heap, 0);
		//lprintf(LOG_DEBUG, (char *)"next is %d (active %d)\n", next->offset, next->active);
	
		if (!next) {
			cost_dmp(cost);
			goto error;
		}
		//if (h--<0) {
			//return heap->getcost(a);
		//}
		int abort = settle_node(level,from,next);
		if (abort == 1) next->active=0;
		for (int rr=0; rr< next->nroads; rr++) {
			r = next->rds[rr];
#ifdef VERBOSE
			printf("found road from %d to %d:%d %d long\n", r->nodefrom->offset,r->nodeto->tile->num,r->nodeto->offset,r->len);
#endif
			nd = r->nodeto;
			//d.AddEdge(next->x, next->y,nd->x, nd->y, r->len, r);
			//printf("node is %p\n", nd);
			if (!nd) continue;
#ifdef VERBOSE
			//printf("dir is %d\n", r->dir);
#endif
			//printf("active %d for node %d:%d\n", next->active, next->tile->num, next->offset);
			if (r->dir == DIR_TOFROM) continue;  
			if (nd->eval) continue; // evaluated
			if (nd->keep==0)  {
#ifdef VERBOSE
				//printf("keep is %d\n", nd->keep);
#endif
				continue;
			}
			if (heap->getcost(nd) == HEAP_CLEAR) { // new to the heap
				heap->setcost(nd, heap->getcost(next)+ROADCOST(*r));
				add_parent(nd,next);
				nd->active = test_active(nd);
				if (nd->active) {
					heap_add(heap, nd);
					//d.AddNode(nd->x, nd->y, nd);
				}
			} else { // already in the heap
				// only set cost if lower
				if (heap->getcost(next) +ROADCOST(*r) < heap->getcost(nd)) {
					heap_del(heap,nd);
				//	printf("Deleting %d ---\n", nd->cost);
					heap->setcost(nd, heap->getcost(next)+ROADCOST(*r));
					nd->tile->dirty=1;
					add_parent(nd,next);
					nd->active = test_active(nd);
					if (nd->active) 
						heap_add(heap,nd);
				}
			}
		}
		heap_del(heap,next);
		cost_add(cost,next,next->cost);
		next->eval = 1;
		//printf("Next is settled\n");
		
		next->tile->dirty=1;
	}

	if (heap) heap_rls(heap);
	//cost_dmp(cost);
	return cost;
error:
	//printf("node not found, network completely evaluated\n");
	if (heap) heap_rls(heap);

	return cost;
}

int32_t level_edsger(int level, node_t *from, node_t *to,int costlimit, int hoplimit)
{
	node_t *next;
	node_t *nd;
	road_t *r;
	heap_t *heap = (heap_t *)heap_ist(1000, gen_heap_fncs);

	node_t *a;
	node_t *b;

	// fill in rest of the nodes
	a = to;
	b = from;


	//if (from) 
		//lprintf(LOG_DEBUG, (char *)"Dijkstra from %d:%d to %d:%d\n", from->tile->num, from->offset, to->tile->num, to->offset); 
	//else 
		//lprintf(LOG_DEBUG, (char *)"Dijkstra to %d:%d limit %d\n", to->tile->num, to->offset, costlimit); 

	heap->setcost(a, 0);
	heap_add(heap, a);

	while (1) {
		if (heap_empty(heap)) {
			 goto error;
		}
		next = (node_t *)heap_get(heap, 0);
		//lprintf(LOG_DEBUG, (char *)"next is %d\n", next->offset);
	
		if (!next) {
			//cost_dmp(cost);
			goto error;
		}
		if (next->hop > hoplimit) {
			//lprintf(LOG_DEBUG, (char *)"Stopped on hop limit %d > %d\n", next->hop, hoplimit);
			goto error;
		}
		if (next->cost >= costlimit) {
			//lprintf(LOG_DEBUG, (char *)"Stopped on cost limit %d > %d\n", next->cost, costlimit);
			goto error;
		}
		// could be run with b==NULL, which means complete the dijkstra
		if (b && next == b) {
			//printf("Found to node (%d) at cost %d\n", b->offset, b->cost);
			//cost_add(cost,b,b->cost);
			//cost_dmp(cost);
			if (heap) heap_rls(heap);
			return b->cost;
		}

		for (int rr=0; rr< next->nroads; rr++) {
			r = next->rds[rr];
#ifdef VERBOSE
			printf("found road from %d to %d:%d %d long\n", r->nodefrom->offset,r->nodeto->tile->num,r->nodeto->offset,r->len);
#endif
			nd = r->nodeto;
			//printf("node is %p\n", nd);
			if (!nd) continue;
#ifdef VERBOSE
			printf("dir is %d\n", r->dir);
#endif
			if (r->dir == DIR_FROMTO) continue;  // we are routing back!!
			//if (nd->rank >0 && nd->rank < next->rank) continue;
			if (nd->rank >0 && nd->rank < next->rank) continue;
#ifdef VERBOSE
			printf("eval is %d\n", nd->eval);
#endif
			if (nd->eval) continue; // evaluated
			if (nd->keep==0)  {
#ifdef VERBOSE
				printf("keep is %d\n", nd->keep);
#endif
				continue;
			}
			//printf("tile is %d %d\n", nd->tile, tpa->num);
			//if (nd->tile != tp->num) tp=tcache_get(tc[level],nd->tile);
			if (nd->cost == HEAP_CLEAR) { // new to the heap
				//nd->cost = ROADCOST(r);
				heap->setcost(nd, next->cost+ROADCOST(*r));
				nd->tile->dirty=1;
				//printf("road is %d type is %d %d\n", r.len, r.type, speeds[r.type]);
				//printf("setting %d to %d next cost was %d\n", nd->offset, nd->cost, next->cost);
				nd->hop = next->hop+1;
				heap_add(heap, nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost+ROADCOST(*r) < nd->cost) {
					heap_del(heap,nd);
				//	printf("Deleting %d ---\n", nd->cost);
					heap->setcost(nd, next->cost+ROADCOST(*r));
					nd->tile->dirty=1;
					heap_add(heap,nd);
					//cost_set(cost,nd,nd->cost);
				}
			}
		}
		heap_del(heap,next);
		//cost_add(cost,next,next->cost);
		next->eval = 1;
		next->tile->dirty=1;
	}

	if (heap) heap_rls(heap);
	//cost_dmp(cost);
	return next->cost ; // cost
error:
	//printf("node not found, network completely evaluated\n");
	if (heap) heap_rls(heap);
	return -1;
}


/* routing within tile, do not go to external tiles */
int32_t tile_edsger(tile_t *tp, node_t *from, node_t *to)
{
	node_t *next;
	node_t *nd;
	road_t r = {0};
	heap_t *heap = (heap_t *)heap_ist(10000, gen_heap_fncs);

	node_t *a;
	node_t *b;

	int p= TESTTILES(tp->num);

	if (p) {
		printf("Edsger on tile %d\n", tp->num);
		tile_dmp(tp,1);
	}

	// fill in rest of the nodes
	a = tp->nds[from->offset];
	// NO: does not have to be a real node !!
	b = to;

	tile_reset(tp);
	//tile_dmp(tp,1);

	heap->setcost(a,0);
	heap_add(heap, a);
	//heap_dmp(heap);

	while (1) {
		if (heap_empty(heap)) {
			 goto error;
		}
		next = (node_t *)heap_get(heap, 0);
		//if(p) printf("next is %d\n", next->offset);
		
		if (!next) {
			//cost_dmp(cost);
			goto error;
		}
		// could be run with b==NULL, which means complete the dijkstra
		if (b && next->tile == b->tile && 
		    next->offset == b->offset) {
			//printf("Found to node (%d) at cost %d\n", b->offset, b->cost);
			//cost_add(cost,b,b->cost);
			//cost_dmp(cost);
			if (heap) heap_rls(heap);
			return b->cost;
		}

		int counter=0;
		for (r = tile_nth_road(tp,*next,counter); r.nodefrom != NULL; r = tile_nth_road(tp,*next,counter))
		{
			counter++;
			if (r.nodeto->tile != tp) continue;
			if(p)printf("[%d] found road %d to %d:%d %d long\n", counter, r.nodefrom->offset,r.nodeto->tile->num,r.nodeto->offset,r.len);
			nd = tile_to_node(tp,r);
			if(p) printf("node is %p\n", nd);
			if (!nd) continue;
			if(p) printf("dir is %d\n", r.dir);
			if (r.dir == DIR_TOFROM) continue; 
			if(p) printf("eval is %d\n", nd->eval);
			if (nd->eval) continue; // evaluated
			if(p) printf("tile is %d %d\n", nd->tile->num, tp->num);
			if (nd->tile!= tp) continue;
			if (nd->cost == HEAP_CLEAR) { // new to the heap
				//nd->cost = ROADCOST(r);
				heap->setcost(nd, next->cost+ROADCOST(r));
				// insert on cost
				if (p) printf("road is %d type is %d %d\n", r.len, r.type, speeds[r.type]);
				if (p) printf("setting %d to %d next cost was %d\n", nd->offset, nd->cost, next->cost);
				heap_add(heap, nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost+ROADCOST(r) < nd->cost) {
					heap_del(heap,nd);
					if(p) printf("Deleting %d ---\n", nd->cost);
				if(p) heap_dmp(heap);
					heap->setcost(nd, next->cost+ROADCOST(r));
					if(p) printf("And adding as %d ---\n", nd->cost);
					heap_add(heap,nd);
				if(p) heap_dmp(heap);
					//cost_set(cost,nd,nd->cost);
				}
			}
		}
		heap_del(heap,next);
		//cost_add(cost,next,next->cost);
		next->eval = 1;
	}

	if (heap) heap_rls(heap);
	//cost_dmp(cost);
	return next->cost ; // cost
error:
	//printf("node not found, network completely evaluated\n");
	if (heap) heap_rls(heap);
	return -1;
}

/* reindex the roads within a tile, make sure that firstroad and
	lastroad in each node point to the correct road offsets in the tile
*/
void tile_reindex_roads(tile_t *tp)
{
    int r;
    road_t *rp;

    int last=-1;
    int locount=0;
    int hicount=0;

    node_t *np=NULL;

    //printf("Doing tile %d\n", tp->num);
    for (r=0; r< tp->nroads; r++) {
        rp = tp->rds[r];

        //printf("Doing x road %d %d \n", r, rp->nodefrom);
        if (rp->nodefrom->offset != last) {
            if (np) {
                //np->firstroad = locount;
                //np->lastroad = hicount;
				np->nroads = hicount-locount;
				locount++;
				hicount++;
				//printf("Set %d %d\n", locount, hicount);
            }
			if (rp->nodefrom < 0) {
				printf("Bad nodefrom \n");
				exit(0);
			}
            np = rp->nodefrom;
            locount=hicount;
            last = rp->nodefrom->offset;
        } else {
            hicount++;
        }
        //printf("count %d,%d\n", locount,hicount);

    }
    if (np) {
        //np->firstroad = locount;
        //np->lastroad = hicount;
		np->nroads = hicount-locount;
    }
	//tile_dmp(tp,1);
}


/*
int32_t tile_edsger_(tile_t *tp, node_t *from, node_t *to)
{
	cost_t *cost = cost_ist();

	tile_edsger_cost(tp,cost,from,to);

	cost_rls(cost);
}
*/
