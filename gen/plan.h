typedef struct location_tag
{	
	char pc[10];
	double lat;
	double lon;
} location_t;

typedef struct driver_tag
{
	char name[100];
	char telno[20];
} driver_t;

typedef struct truck_tag
{
	char id[100];
	char code[100];
	location_t base;
	//driver_t driver;
} truck_t;

typedef struct order_tag {
	int id;
	int client;
	int size;
	double pLat;
	double pLon;
	double dLat;
	double dLon;
} order_t;

typedef struct stop_tag
{
	int id;
} stop_t;

typedef struct trip_tag
{
	int oid;
	int tripnr;
	int seqnr;
	int dur;
	time_t when;
} trip_t;

order_t *order_find(char *id);
