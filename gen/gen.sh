#!/bin/bash

echo getting $1
t1=`date +%s`
declare -i t1
d1=`date`
#wget http://download.geofabrik.de/osm/europe/$1.osm.pbf &> $1.dmp

echo extracting
t2=`date +%s`
declare -i t2
d2=`date`
/home/kees/projects/3pty/osrm/osrm-extract $1.osm.pbf &>> $1.dmp

echo preparing
t3=`date +%s`
declare -i t3
d3=`date`
#/home/kees/projects/3pty/osrm/osrm-prepare $1.osrm $1.osrm.restrictions &>> $1.dmp

echo timings
t4=`date +%s`
declare -i t4
d4=`date`
let dt=$t2-$t1
let et=$t3-$t2
let pt=$t4-$t3
let tt=$t4-$t1
echo "Started : " $d1
echo "Finished : " $d4
echo "Download time    : " $dt
echo "Extraction time  : " $et
echo "Preparation time : " $pt
echo "Total time       : " $tt
