#include <gen.h>
#include <level.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/* hash table based heap :
	part is the number of hash indices
 */
heap_t *heap_ist(int part, heapfnc_t fncs)
{
	heap_t *hp = (heap_t *)calloc(sizeof(heap_t), 1);

	hp->part=part;
	hp->bot=0;
	hp->top=0;
	hp->cmp = fncs.cmp;
	hp->setcost = fncs.setcost;
	hp->getcost = fncs.getcost;
	hp->setflag = fncs.setflag;
	hp->getflag = fncs.getflag;
	
	if (hp->part < 1) hp->part=1;
	// initial calloc, so we can realloc later
	hp->phys=1; 
	hp->data = (heapentry_t **)calloc(hp->phys , sizeof(heapentry_t *));

	return hp;
}

int bigdiff=0;

// NOTE : find the index on cost, but the node on tile/node !!!
void heap_del(heap_t *hp, void *np)
{
	heapentry_t *ptr;
	heapentry_t *old;
	int cost = hp->getcost(np);
	int offset = (cost / hp->part) - hp->bot;

	if (offset<0) {
		printf("Heap (Del) failure !!, (cost/part) - bottom -> (%d/%d) - %d = %d\n", 
		cost, hp->part, hp->bot, offset);
		assert(1==0);
	} 
	//printf("D: %d\n", hp->getcost(np));
	//printf("offset %d\n", offset);
	//heap_dmp(hp);

	for (ptr = hp->data[offset]; ptr != NULL; ptr = ptr->next)
	{
		//printf("Testing %d\n", ptr->node->cost);
		if (ptr->next) {
			//printf("tiles %d and %d\n", np->tile, ptr->next->node->tile);
			//printf("offsets %d and %d\n", np->offset, ptr->next->node->offset);
			//printf("cost %d \n", ptr->next->node->cost);
		} else {
			//printf("First ? %d\n", ptr->node->cost);
		}
		if (ptr->next && 
			hp->cmp(np,ptr->next->node) == 0) {
			//np->tile == ptr->next->node->tile && np->offset == ptr->next->node->offset) {
			// delete next
			old = ptr->next;
			ptr->next = ptr->next->next;
			//printf("Yep deleting %d now\n", old->node->cost);
			free(old);
			break;
		}
		if (hp->cmp(np, ptr->node) == 0) {
	//	if (np->tile == ptr->node->tile && np->offset == ptr->node->offset) {
			// delete first
			//printf("Deleting first\n");
			old = hp->data[offset];
			hp->data[offset] = hp->data[offset]->next;
			free(old);
			break;
		}
	}
	if (hp->top-hp->bot > bigdiff) {
		bigdiff = hp->top-hp->bot;
		//printf("%d         \r", bigdiff);
		//fflush(stdout);
	}
	//heap_dmp(hp);
}

void heap_dmp(heap_t *hp)
{
	int y;
	heapentry_t *ptr;
	int sz = hp->top - hp->bot;

	printf("offset [%d:%d], phys %d\n", hp->bot, hp->top, hp->phys);

	for (y=0; y< sz; y++) {
		printf("\n[%d] (%d):", y, y+hp->bot);
		for (ptr = hp->data[y]; ptr != NULL; ptr = ptr->next)
		{
			int cost = hp->getcost(ptr->node);
			printf("[%p,%d]", ptr->node, cost);
		}
	}
	printf("\n");
}

void heap_add(heap_t *hp, void *nd)
{
	int oldsz;

	int cost = hp->getcost(nd);
	int index = (cost / hp->part) - hp->bot;
	heapentry_t *ptr=NULL;

	if (index<0) {
		printf("Heap (Add) failure !!, (cost/part) - bottom -> (%d/%d) - %d = %d\n", 
		cost, hp->part, hp->bot, index);
		assert(1==0);
	} 
	//printf("A: %d\n", hp->getcost(nd));
	//printf("offset: %d\n", index);
	//heap_dmp(hp);

	if (index+1 > hp->top-hp->bot) {
		oldsz = hp->phys ;
		if (index >= hp->phys) {
			hp->phys=index+1;
			//printf("resizing from %d to %d\n", oldsz, hp->phys);
		}
		hp->data = (heapentry_t **)realloc(hp->data, hp->phys * sizeof(heapentry_t *));
		int t;
		for (t=oldsz; t< hp->phys; t++)  {	
			//printf("setting %d to null\n", t);
			hp->data[t] = NULL;
		}
		hp->top= hp->bot+index+1;
	}
	heapentry_t *hnp = (heapentry_t *)calloc(sizeof(heapentry_t), 1);
	//printf("Creating %p \n", hnp);
	for (ptr = hp->data[index]; ptr != NULL; ptr = ptr->next)
	{
		if (hp->getcost(nd) < hp->getcost(ptr->node)) {
			// default will handle this
			break;
		}
		if ( !ptr->next || hp->getcost(nd) < hp->getcost(ptr->next->node) ) { 
			// insert here
			hnp->next = ptr->next;
			hnp->node = nd;
			ptr->next = hnp;
			//heap_dmp(hp);
			return;
		}
	}
	// insert as only 
	hnp->next = ptr;
	hnp->node = nd;
	hp->data[index] = hnp;
	//heap_dmp(hp);
}

// remove all empty slots at the beginning
void heap_prune(heap_t *hp)
{
	int y;
	int sz = hp->top - hp->bot;
	int offset=0;

	for (y=0; y< sz; y++) {
		//printf("%d is %p\n", y, hp->data[y]);
		if (hp->data[y] != NULL) 
			break;
		offset++;
	}
	if (offset==0) return;
	//printf("Movin %d to %d , %d big\n", offset, 0, hp->phys);
	memmove(&hp->data[0], &hp->data[offset], (hp->phys-offset)*sizeof(heapentry_t *));
	//printf("resetting %d to %d \n", hp->phys, offset);
	memset(&hp->data[hp->phys-offset], '\0', offset*sizeof(heapentry_t *));
	hp->bot += offset;
}

void *heap_get(heap_t *hp, int num)
{
    heap_prune(hp);
    if (hp->top-hp->bot < 1 || !hp->data[0] ) return NULL;
    return hp->data[0]->node;
}

int heap_empty(heap_t *hp)
{
    return (heap_get(hp, 0) == NULL);
}

void heap_rls(heap_t *hp)
{
	int y;
	heapentry_t *ptr;
	heapentry_t *old;
	int sz = hp->top - hp->bot;

	for (y=0; y< sz; y++) {
		ptr = hp->data[y]; 
		while(ptr) {
			old = ptr;
			ptr=ptr->next;
			//printf("Freeing %p\n", old);
			free(old);
		}
	}
	//printf("Freeing %p\n", hp->data);
	free(hp->data);
	free(hp);
}

#ifdef TESTING
int main(int argc, char **argv)
{
	heap_t *hp = heap_ist(10);
	node_t *np;

	np = newnode(22);
	np->tile=0;
	np->node = 263;

	heap_add(hp, np);
	heap_dmp(hp);

	np = newnode(128);
	np->tile=0;
	np->node = 4228;
	heap_add(hp, np);
	heap_dmp(hp);

	np = newnode(122);
	np->tile=0;
	np->node = 4222;

	heap_add(hp, np);
	heap_dmp(hp);

	np = heap_get(hp,0);
	printf("Smallest node %d\n", np->cost);
	heap_del(hp,np);

	heap_dmp(hp);
	return 0;
}
#endif
