#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libxml/parser.h>
#include <rds.h>

#include <ftw.h>
#define MAXSIZE 1000000

enum objtype { TYPE_VERBLIJF, TYPE_LIGPLAATS, TYPE_STANDPLAATS} ;

typedef struct coord_tag
{
	double lat;
	double lng;
} coord_t;

typedef struct Nummeraanduiding_tag
{
	char identificatie[100];
	char aanduidingRecordInactief[10];
	char officieel[100];
	char huisnummer[100];
	char huisletter[100];
	char huisnummertoevoeging[100];
	char postcode[100];
	char gerelateerdeOpenbareRuimte[100];
} Nummeraanduiding;

typedef struct geo_tag
{
	long long id;
	int type;
	int gcount;
	coord_t *geometry;
} geo_t;

typedef struct index_tag
{
	long long geo_id;
	long long ref_id;
} index_t;

typedef struct hnr_tag
{
	long long id;
	int huisnummer;
	char *huisletter;
	char *huisnummertoevoeging;
	char *postcode;
	long long gerelateerdeOpenbareRuimte;
} hnr_t;

typedef struct Pand_tag
{
	char identificatie[100];
	char aanduidingRecordInactief[100];
	char officieel[100];
	int gcount;
	char *geometry;
	char bouwjaar[100];
	char pandstatus[100];
} Pand;

typedef struct OpenbareRuimte_tag
{
	char identificatie[100];
	char aanduidingRecordInactief[100];
	char officieel[100];
	char openbareRuimteNaam[4000];
	char openbareRuimteType[1000];
	char openbareRuimteStatus[100];
	char gerelateerdeWoonplaats[100];
} OpenbareRuimte;

typedef struct orr_tag
{
	long long id;
	char *naam;
	char *type;
	long long wpl_id;
} orr_t;

typedef struct StandPlaats_tag
{
	char identificatie[100];
	char aanduidingRecordInactief[100];
	char officieel[100];
	int gcount;
	char *geometry;
	char hoofdadres[100];
	int nadr;
	char nevenadres[4000]; // multiple possible
	char standplaatsStatus[100];
} StandPlaats;

typedef struct LigPlaats_tag
{
	char identificatie[100];
	char aanduidingRecordInactief[100];
	char officieel[100];
	int gcount;
	char *geometry;
	char hoofdadres[100];
	int nadr;
	char nevenadres[4000]; // multiple possible
	char status[100];
} LigPlaats;

typedef struct lp_tag
{
	long long id;
	int gcount;
	coord_t *geometry;
	long long hoofdadres;
	int nadr;
	long long *nevenadres; // multiple possible
} lp_t;

typedef struct sp_tag
{
	long long id;
	int gcount;
	coord_t *geometry;
	long long hoofdadres;
	int nadr;
	long long *nevenadres; // multiple possible
} sp_t;

typedef struct WoonPlaats_tag
{
	char identificatie[100];
	char aanduidingRecordInactief[100];
	char officieel[100];
	int gcount;
	char *geometry;
	char woonplaatsNaam[100];
	char woonplaatsStatus[100];
} WoonPlaats;

typedef struct wpl_tag
{
	long long id;
	char *naam;
} wpl_t;

typedef struct VerblijfsObject_tag
{
	char identificatie[100];
	char aanduidingRecordInactief[100];
	char officieel[100];
	int gcount;
	char *geometry;
	char hoofdadres[100];
	int nadr;
	char nevenadres[4000]; // multiple possible
	char verblijfsobjectStatus[100];
	char gebruiksdoelVerblijfsobject[100];
	char oppervlakteVerblijfsobject[100];
	char gerelateerdPand[100];
} Verblijfsobject;

typedef struct vob_tag
{
	long long id;
	int gcount;
	coord_t *geometry;
	long long hoofdadres;
	int nadr;
	long long *nevenadres; // multiple possible
	char *functie;
	int oppervlakte;
	long long gerelateerdPand;
} vob_t;

char *conv_coord(char *input,int fields,int *count)
{
	static char base[400000]={0};

	char *tok;
	RDSPoint rp;
	GPSPoint gp;

	base[0]='\0';
	char piece[100];

	if (count) *count=0;

	tok = strtok(input," ");
	while (tok) {
		rp.x = atof(tok);
		tok = strtok(NULL, " ");
		rp.y = atof(tok);
		tok = strtok(NULL, " ");
		gp = makeGPSPoint(rp);

		if (fields == 3) tok = strtok(NULL, " "); // skip height
		sprintf(piece,"%f %f", gp.lat,gp.lng);

		strcat(&base[0],piece);
		strcat(&base[0]," ");
		if (count) (*count)++;
	}

	return base;
}

void parseLigplaats (xmlDocPtr doc, xmlNodePtr cur) 
{
	LigPlaats obj;
	memset(&obj, '\0', sizeof(LigPlaats));

	while (cur != NULL) {

		xmlChar *key;
		key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"identificatie"))) {
			sprintf(obj.identificatie,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"aanduidingRecordInactief"))) {
			sprintf(obj.aanduidingRecordInactief,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"officieel"))) {
			sprintf(obj.officieel,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"ligplaatsStatus"))) {
			sprintf(obj.status,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"gerelateerdeAdressen"))) {
			xmlNodePtr sub1 = cur->xmlChildrenNode;
			int hadr=0;
			while (sub1 != NULL) {
				key = xmlNodeListGetString(doc, sub1->xmlChildrenNode, 1);
	    		if ((!xmlStrcmp(sub1->name, (const xmlChar *)"hoofdadres"))) {
					xmlNodePtr sub2 = sub1->xmlChildrenNode;
					hadr++;
					if (hadr >1) printf("Handle more than 1 hoofd addres\n");
					while (sub2 != NULL) {
						key = xmlNodeListGetString(doc, sub2->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(sub2->name, (const xmlChar *)"identificatie"))) {
							sprintf(obj.hoofdadres,"%s" ,(key));
						} else 
	    				if ((xmlStrcmp(sub2->name, (const xmlChar *)"text"))) {
							printf("Handle Other label %s\n", sub2->name);
						} 
						sub2 = sub2->next;
					} 
				} else 
	    		if ((!xmlStrcmp(sub1->name, (const xmlChar *)"nevenadres"))) {
					xmlNodePtr sub2 = sub1->xmlChildrenNode;
					obj.nadr++;
					while (sub2 != NULL) {
						key = xmlNodeListGetString(doc, sub2->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(sub2->name, (const xmlChar *)"identificatie"))) {
							sprintf(obj.nevenadres,"%s %s",obj.nevenadres ,(key));
						} else 
	    				if ((xmlStrcmp(sub2->name, (const xmlChar *)"text"))) {
							printf("Handle Other label %s\n", sub2->name);
						} 
						sub2 = sub2->next;
					} 
				} else 
	    		if ((xmlStrcmp(sub1->name, (const xmlChar *)"text"))) {
					printf("Handle Other kind of adres %s\n", sub1->name);
				} 
				sub1 = sub1->next;
			} 
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"ligplaatsGeometrie"))) {
			xmlNodePtr polgon = cur->xmlChildrenNode;
			while (polgon != NULL) {
				key = xmlNodeListGetString(doc, polgon->xmlChildrenNode, 1);
	    		if ((!xmlStrcmp(polgon->name, (const xmlChar *)"Polygon"))) {
					xmlNodePtr exterior = polgon->xmlChildrenNode;
					while (exterior != NULL) {
						key = xmlNodeListGetString(doc, exterior->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(exterior->name, (const xmlChar *)"exterior"))) {
							xmlNodePtr linearring = exterior->xmlChildrenNode;
							while (linearring != NULL) {
								key = xmlNodeListGetString(doc, linearring->xmlChildrenNode, 1);
	    						if ((!xmlStrcmp(linearring->name, (const xmlChar *)"LinearRing"))) {
									xmlNodePtr poslist = linearring->xmlChildrenNode;
									while (poslist != NULL) {
										key = xmlNodeListGetString(doc, poslist->xmlChildrenNode, 1);
	    								if ((!xmlStrcmp(poslist->name, (const xmlChar *)"posList"))) {
											obj.geometry = conv_coord((char *)key,3,&obj.gcount);
										} 
										poslist = poslist->next;
									} 
								}
								linearring = linearring->next;
							} 
						}
						exterior = exterior->next;
					} 
				}
				polgon = polgon->next;
			} 
		} 
	    xmlFree(key);
		cur = cur->next;
	}

	printf("%s\t%s\t%s\t%s\t%s\t%d\t%s\t%d\t%s\n" 
			,obj.identificatie
			,obj.aanduidingRecordInactief
			,obj.officieel
			,obj.hoofdadres
			,obj.status
			,obj.nadr
			,obj.nevenadres
			,obj.gcount
			,obj.geometry
			);
}

void parseStandplaats (xmlDocPtr doc, xmlNodePtr cur) 
{
	StandPlaats obj;
	memset(&obj, '\0', sizeof(StandPlaats));

	while (cur != NULL) {

		xmlChar *key;
		key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"identificatie"))) {
			sprintf(obj.identificatie,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"aanduidingRecordInactief"))) {
			sprintf(obj.aanduidingRecordInactief,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"officieel"))) {
			sprintf(obj.officieel,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"standplaatsStatus"))) {
			sprintf(obj.standplaatsStatus,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"gerelateerdeAdressen"))) {
			xmlNodePtr sub1 = cur->xmlChildrenNode;
			int hadr=0;
			while (sub1 != NULL) {
				key = xmlNodeListGetString(doc, sub1->xmlChildrenNode, 1);
	    		if ((!xmlStrcmp(sub1->name, (const xmlChar *)"hoofdadres"))) {
					xmlNodePtr sub2 = sub1->xmlChildrenNode;
					hadr++;
					if (hadr >1) printf("Handle more than 1 hoofd addres\n");
					while (sub2 != NULL) {
						key = xmlNodeListGetString(doc, sub2->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(sub2->name, (const xmlChar *)"identificatie"))) {
							sprintf(obj.hoofdadres,"%s" ,(key));
						} else 
	    				if ((xmlStrcmp(sub2->name, (const xmlChar *)"text"))) {
							printf("Handle Other label %s\n", sub2->name);
						} 
						sub2 = sub2->next;
					} 
				} else 
	    		if ((!xmlStrcmp(sub1->name, (const xmlChar *)"nevenadres"))) {
					xmlNodePtr sub2 = sub1->xmlChildrenNode;
					obj.nadr++;
					while (sub2 != NULL) {
						key = xmlNodeListGetString(doc, sub2->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(sub2->name, (const xmlChar *)"identificatie"))) {
							sprintf(obj.nevenadres,"%s %s",obj.nevenadres ,(key));
						} else 
	    				if ((xmlStrcmp(sub2->name, (const xmlChar *)"text"))) {
							printf("Handle Other label %s\n", sub2->name);
						} 
						sub2 = sub2->next;
					} 
				} else 
	    		if ((xmlStrcmp(sub1->name, (const xmlChar *)"text"))) {
					printf("Handle Other kind of adres %s\n", sub1->name);
				} 
				sub1 = sub1->next;
			} 
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"standplaatsGeometrie"))) {
			xmlNodePtr polgon = cur->xmlChildrenNode;
			while (polgon != NULL) {
				key = xmlNodeListGetString(doc, polgon->xmlChildrenNode, 1);
	    		if ((!xmlStrcmp(polgon->name, (const xmlChar *)"Polygon"))) {
					xmlNodePtr exterior = polgon->xmlChildrenNode;
					while (exterior != NULL) {
						key = xmlNodeListGetString(doc, exterior->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(exterior->name, (const xmlChar *)"exterior"))) {
							xmlNodePtr linearring = exterior->xmlChildrenNode;
							while (linearring != NULL) {
								key = xmlNodeListGetString(doc, linearring->xmlChildrenNode, 1);
	    						if ((!xmlStrcmp(linearring->name, (const xmlChar *)"LinearRing"))) {
									xmlNodePtr poslist = linearring->xmlChildrenNode;
									while (poslist != NULL) {
										key = xmlNodeListGetString(doc, poslist->xmlChildrenNode, 1);
	    								if ((!xmlStrcmp(poslist->name, (const xmlChar *)"posList"))) {
											obj.geometry = conv_coord((char *)key,3,&obj.gcount);
										} 
										poslist = poslist->next;
									} 
								}
								linearring = linearring->next;
							} 
						}
						exterior = exterior->next;
					} 
				}
				polgon = polgon->next;
			} 
		} 
	    xmlFree(key);
		cur = cur->next;
	}

	printf("%s\t%s\t%s\t%s\t%s\t%d\t%s\t%d\t%s\n" 
			,obj.identificatie
			,obj.aanduidingRecordInactief
			,obj.officieel
			,obj.hoofdadres
			,obj.standplaatsStatus
			,obj.nadr
			,obj.nevenadres
			,obj.gcount
			,obj.geometry
			);
}

void parseWoonplaats (xmlDocPtr doc, xmlNodePtr cur) 
{
	WoonPlaats obj;
	memset(&obj, '\0', sizeof(WoonPlaats));

	while (cur != NULL) {

		xmlChar *key;
		key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"identificatie"))) {
			sprintf(obj.identificatie,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"aanduidingRecordInactief"))) {
			sprintf(obj.aanduidingRecordInactief,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"officieel"))) {
			sprintf(obj.officieel,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"woonplaatsStatus"))) {
			sprintf(obj.woonplaatsStatus,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"woonplaatsNaam"))) {
			sprintf(obj.woonplaatsNaam,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"woonplaatsGeometrie"))) {
			xmlNodePtr polgon = cur->xmlChildrenNode;
			while (polgon != NULL) {
				key = xmlNodeListGetString(doc, polgon->xmlChildrenNode, 1);
	    		if ((!xmlStrcmp(polgon->name, (const xmlChar *)"Polygon"))) {
					xmlNodePtr exterior = polgon->xmlChildrenNode;
					while (exterior != NULL) {
						key = xmlNodeListGetString(doc, exterior->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(exterior->name, (const xmlChar *)"exterior"))) {
							xmlNodePtr linearring = exterior->xmlChildrenNode;
							while (linearring != NULL) {
								key = xmlNodeListGetString(doc, linearring->xmlChildrenNode, 1);
	    						if ((!xmlStrcmp(linearring->name, (const xmlChar *)"LinearRing"))) {
									xmlNodePtr poslist = linearring->xmlChildrenNode;
									while (poslist != NULL) {
										key = xmlNodeListGetString(doc, poslist->xmlChildrenNode, 1);
	    								if ((!xmlStrcmp(poslist->name, (const xmlChar *)"posList"))) {
											obj.geometry = conv_coord((char *)key,2,&obj.gcount);
										} 
										poslist = poslist->next;
									} 
								}
								linearring = linearring->next;
							} 
						}
						exterior = exterior->next;
					} 
				}
				polgon = polgon->next;
			} 
		} 
	    xmlFree(key);
		cur = cur->next;
	}

	printf("%s\t%s\t%s\t%s\t%s\t%d\t%s\n" 
			,obj.identificatie
			,obj.aanduidingRecordInactief
			,obj.officieel
			,obj.woonplaatsNaam
			,obj.woonplaatsStatus
			,obj.gcount
			,obj.geometry
			);
}

void parseVerblijfsObject (xmlDocPtr doc, xmlNodePtr cur) 
{
	Verblijfsobject obj;
	memset(&obj, '\0', sizeof(Verblijfsobject));

	while (cur != NULL) {

		xmlChar *key;
		key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"identificatie"))) {
			sprintf(obj.identificatie,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"aanduidingRecordInactief"))) {
			sprintf(obj.aanduidingRecordInactief,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"officieel"))) {
			sprintf(obj.officieel,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"verblijfsobjectStatus"))) {
			sprintf(obj.verblijfsobjectStatus,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"gebruiksdoelVerblijfsobject"))) {
			sprintf(obj.gebruiksdoelVerblijfsobject,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"oppervlakteVerblijfsobject"))) {
			sprintf(obj.oppervlakteVerblijfsobject,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"gerelateerdeAdressen"))) {
			xmlNodePtr sub1 = cur->xmlChildrenNode;
			int hadr=0;
			while (sub1 != NULL) {
				key = xmlNodeListGetString(doc, sub1->xmlChildrenNode, 1);
	    		if ((!xmlStrcmp(sub1->name, (const xmlChar *)"hoofdadres"))) {
					xmlNodePtr sub2 = sub1->xmlChildrenNode;
					hadr++;
					if (hadr >1) printf("Handle more than 1 hoofd addres\n");
					while (sub2 != NULL) {
						key = xmlNodeListGetString(doc, sub2->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(sub2->name, (const xmlChar *)"identificatie"))) {
							sprintf(obj.hoofdadres,"%s" ,(key));
						} else 
	    				if ((xmlStrcmp(sub2->name, (const xmlChar *)"text"))) {
							printf("Handle Other label %s\n", sub2->name);
						} 
						sub2 = sub2->next;
					} 
				} else 
	    		if ((!xmlStrcmp(sub1->name, (const xmlChar *)"nevenadres"))) {
					xmlNodePtr sub2 = sub1->xmlChildrenNode;
					obj.nadr++;
					while (sub2 != NULL) {
						key = xmlNodeListGetString(doc, sub2->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(sub2->name, (const xmlChar *)"identificatie"))) {
							sprintf(obj.nevenadres,"%s %s",obj.nevenadres ,(key));
						} else 
	    				if ((xmlStrcmp(sub2->name, (const xmlChar *)"text"))) {
							printf("Handle Other label %s\n", sub2->name);
						} 
						sub2 = sub2->next;
					} 
				} else 
	    		if ((xmlStrcmp(sub1->name, (const xmlChar *)"text"))) {
					printf("Handle Other kind of adres %s\n", sub1->name);
				} 
				sub1 = sub1->next;
			} 
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"gerelateerdPand"))) {
			xmlNodePtr sub1 = cur->xmlChildrenNode;
			int hadr=0;
			while (sub1 != NULL) {
				key = xmlNodeListGetString(doc, sub1->xmlChildrenNode, 1);
	    		if ((!xmlStrcmp(sub1->name, (const xmlChar *)"identificatie"))) {
					hadr++;
					if (hadr >1) printf("Handle more than 1 gerelateerd pand addres\n");
					sprintf(obj.gerelateerdPand,"%s" ,(key));
				} else 
	    		if ((xmlStrcmp(sub1->name, (const xmlChar *)"text"))) {
					printf("Handle Other kind of adres %s\n", sub1->name);
				} 
				sub1 = sub1->next;
			} 
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"verblijfsobjectGeometrie"))) {
			xmlNodePtr polgon = cur->xmlChildrenNode;
			while (polgon != NULL) {
	    		if ((!xmlStrcmp(polgon->name, (const xmlChar *)"Polygon"))) {
					xmlNodePtr exterior = polgon->xmlChildrenNode;
					while (exterior != NULL) {
	    				if ((!xmlStrcmp(exterior->name, (const xmlChar *)"exterior"))) {
							xmlNodePtr linearring = exterior->xmlChildrenNode;
							while (linearring != NULL) {
	    						if ((!xmlStrcmp(linearring->name, (const xmlChar *)"LinearRing"))) {
									xmlNodePtr poslist = linearring->xmlChildrenNode;
									while (poslist != NULL) {
										key = xmlNodeListGetString(doc, poslist->xmlChildrenNode, 1);
	    								if ((!xmlStrcmp(poslist->name, (const xmlChar *)"posList"))) {
											obj.geometry = conv_coord((char *)key,3,&obj.gcount);
										} 
										poslist = poslist->next;
									} 
								}
								linearring = linearring->next;
							} 
						}
						exterior = exterior->next;
					} 
				} else 
	    		if ((!xmlStrcmp(polgon->name, (const xmlChar *)"Point"))) {
					xmlNodePtr sub2 = polgon->xmlChildrenNode;
					while (sub2 != NULL) {
						key = xmlNodeListGetString(doc, sub2->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(sub2->name, (const xmlChar *)"pos"))) {
							obj.geometry = conv_coord((char *)key,3,&obj.gcount);
						} 
						sub2 = sub2->next;
					} 
				} 
				polgon = polgon->next;
			} 
		} 
	    xmlFree(key);
		cur = cur->next;
	}

	printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%d\t%s\t%d\t%s\n" 
			,obj.identificatie
			,obj.aanduidingRecordInactief
			,obj.officieel
			,obj.hoofdadres
			,obj.gebruiksdoelVerblijfsobject
			,obj.verblijfsobjectStatus
			,obj.oppervlakteVerblijfsobject
			,obj.gerelateerdPand
			,obj.nadr
			,obj.nevenadres
			,obj.gcount
			,obj.geometry
			);
}

void parsePand (xmlDocPtr doc, xmlNodePtr cur) 
{
	Pand obj;
	memset(&obj, '\0', sizeof(Pand));

	while (cur != NULL) {

		xmlChar *key;
		key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"identificatie"))) {
			sprintf(obj.identificatie,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"aanduidingRecordInactief"))) {
			sprintf(obj.aanduidingRecordInactief,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"officieel"))) {
			sprintf(obj.officieel,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"bouwjaar"))) {
			sprintf(obj.bouwjaar,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"pandstatus"))) {
			sprintf(obj.pandstatus,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"pandGeometrie"))) {
			xmlNodePtr polgon = cur->xmlChildrenNode;
			while (polgon != NULL) {
				key = xmlNodeListGetString(doc, polgon->xmlChildrenNode, 1);
	    		if ((!xmlStrcmp(polgon->name, (const xmlChar *)"Polygon"))) {
					xmlNodePtr exterior = polgon->xmlChildrenNode;
					while (exterior != NULL) {
						key = xmlNodeListGetString(doc, exterior->xmlChildrenNode, 1);
	    				if ((!xmlStrcmp(exterior->name, (const xmlChar *)"exterior"))) {
							xmlNodePtr linearring = exterior->xmlChildrenNode;
							while (linearring != NULL) {
								key = xmlNodeListGetString(doc, linearring->xmlChildrenNode, 1);
	    						if ((!xmlStrcmp(linearring->name, (const xmlChar *)"LinearRing"))) {
									xmlNodePtr poslist = linearring->xmlChildrenNode;
									while (poslist != NULL) {
										key = xmlNodeListGetString(doc, poslist->xmlChildrenNode, 1);
	    								if ((!xmlStrcmp(poslist->name, (const xmlChar *)"posList"))) {
											obj.geometry = conv_coord((char *)key,3,&obj.gcount);
										} 
										poslist = poslist->next;
									} 
								}
								linearring = linearring->next;
							} 
						}
						exterior = exterior->next;
					} 
				}
				polgon = polgon->next;
			} 
		} 
	    xmlFree(key);
		cur = cur->next;
	}

	printf("%s\t%s\t%s\t%s\t%s\t%d\t%s\n" 
			,obj.identificatie
			,obj.aanduidingRecordInactief
			,obj.officieel
			,obj.bouwjaar
			,obj.pandstatus
			,obj.gcount
			,obj.geometry
			);
}

void parseOpenbareRuimte (xmlDocPtr doc, xmlNodePtr cur) 
{
	OpenbareRuimte obj;
	memset(&obj, '\0', sizeof(OpenbareRuimte));

	while (cur != NULL) {
		xmlChar *key;
		key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"identificatie"))) {
			sprintf(obj.identificatie,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"aanduidingRecordInactief"))) {
			sprintf(obj.aanduidingRecordInactief,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"officieel"))) {
			sprintf(obj.officieel,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"openbareRuimteNaam"))) {
			sprintf(obj.openbareRuimteNaam,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"openbareRuimteType"))) {
			sprintf(obj.openbareRuimteType,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"openbareRuimteStatus"))) {
			sprintf(obj.openbareRuimteStatus,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"gerelateerdeWoonplaats"))) {
			xmlNodePtr deeper = cur->xmlChildrenNode;
			while (deeper != NULL) {
				key = xmlNodeListGetString(doc, deeper->xmlChildrenNode, 1);
	    		if ((!xmlStrcmp(deeper->name, (const xmlChar *)"identificatie"))) {
					sprintf(obj.gerelateerdeWoonplaats,"%s" ,(key));
				}
				deeper = deeper->next;
			} 
		} 
	    xmlFree(key);
		cur = cur->next;
	}

	printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\n" ,obj.identificatie
			,obj.aanduidingRecordInactief
			,obj.officieel
			,obj.openbareRuimteNaam
			,obj.openbareRuimteType
			,obj.openbareRuimteStatus
			,obj.gerelateerdeWoonplaats);
}

void parseNummeraanduiding (xmlDocPtr doc, xmlNodePtr cur) 
{
	Nummeraanduiding obj;
	memset(&obj, '\0', sizeof(Nummeraanduiding));

	while (cur != NULL) {
		xmlChar *key;
		key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"identificatie"))) {
			sprintf(obj.identificatie,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"aanduidingRecordInactief"))) {
			sprintf(obj.aanduidingRecordInactief,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"officieel"))) {
			sprintf(obj.officieel,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"huisnummer"))) {
			sprintf(obj.huisnummer,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"huisletter"))) {
			sprintf(obj.huisletter,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"huisnummertoevoeging"))) {
			sprintf(obj.huisnummertoevoeging,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"postcode"))) {
			sprintf(obj.postcode,"%s" ,(key));
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"gerelateerdeOpenbareRuimte"))) {
			xmlNodePtr deeper = cur->xmlChildrenNode;
			while (deeper != NULL) {
				key = xmlNodeListGetString(doc, deeper->xmlChildrenNode, 1);
	    		if ((!xmlStrcmp(deeper->name, (const xmlChar *)"identificatie"))) {
					sprintf(obj.gerelateerdeOpenbareRuimte,"%s" ,(key));
				}
				deeper = deeper->next;
			} 
		} 
	    xmlFree(key);
		cur = cur->next;
	}

	printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" ,obj.identificatie
			,obj.aanduidingRecordInactief
			,obj.officieel
			,obj.huisnummer
			,obj.huisletter
			,obj.huisnummertoevoeging
			,obj.postcode
			,obj.gerelateerdeOpenbareRuimte);
}

void descend (xmlDocPtr doc, xmlNodePtr cur,int level) {

	xmlNodePtr deeper=NULL;

	while (cur != NULL) {
		xmlChar *key;
		key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"Nummeraanduiding"))) {
			parseNummeraanduiding(doc,cur->xmlChildrenNode);
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"Pand"))) {
			parsePand(doc,cur->xmlChildrenNode);
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"OpenbareRuimte"))) {
			parseOpenbareRuimte(doc,cur->xmlChildrenNode);
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"Standplaats"))) {
			parseStandplaats(doc,cur->xmlChildrenNode);
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"Woonplaats"))) {
			parseWoonplaats(doc,cur->xmlChildrenNode);
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"Verblijfsobject"))) {
			parseVerblijfsObject(doc,cur->xmlChildrenNode);
		} 
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"Ligplaats"))) {
			parseLigplaats(doc,cur->xmlChildrenNode);
		} 
		deeper = cur->xmlChildrenNode;
		if (deeper) descend(doc,deeper,level+1);
	    xmlFree(key);
		cur = cur->next;
	}
    return;
}

int read_all(const char *name, const struct stat *st, int flag)
{
	xmlDocPtr doc;
	xmlNodePtr cur;

	if (flag) return 0;
	doc = xmlParseFile(name);
	if (doc == NULL ) {
		fprintf(stderr,"Document not parsed successfully. \n");
		return -1;
	}
	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		fprintf(stderr,"empty document\n");
		xmlFreeDoc(doc);
		return -1;
	}
	descend (doc,cur,0);
	xmlFreeDoc(doc);

	return 0;
}

int dir_num()
{
#define NUMDIR "../bag/9999NUM08012013"
	ftw(NUMDIR, read_all, FTW_D);
	return 0;
}

int dir_sta()
{
#define STADIR "../bag/9999STA08012013"
	ftw(STADIR, read_all, FTW_D);
	//read_all("test.xml",NULL,0);
	return 0;
}

int dir_pnd()
{
#define PNDDIR "../bag/9999PND08012013"
	ftw(PNDDIR, read_all, FTW_D);
	//read_all("test.xml",NULL,0);
	return 0;
}

int dir_opr()
{
#define OPRDIR "../bag/9999OPR08012013"
	ftw(OPRDIR, read_all, FTW_D);
	//read_all("test.xml",NULL,0);
	return 0;
}

int dir_vbo()
{
#define VBODIR "../bag/9999VBO08012013"
	ftw(VBODIR, read_all, FTW_D);
	//read_all("test.xml",NULL,0);
	return 0;
}

int dir_wpl()
{
#define WPLDIR "../bag/9999WPL08012013"
	ftw(WPLDIR, read_all, FTW_D);
	//read_all("test.xml",NULL,0);
	return 0;
}

int file_lpl()
{
#define LPLFILE "../bag/ligplaatsen.xml"
	read_all(LPLFILE,NULL,0);
	return 0;
}

int sp_cmp(const void *a, const void *b)
{
	sp_t *A = (sp_t *)a;
	sp_t *B = (sp_t *)b;

	//printf("%lld against %lld\n", A->hoofdadres, B->hoofdadres);
	if (A->hoofdadres < B->hoofdadres) return -1;
	if (A->hoofdadres > B->hoofdadres) return  1;
	return 0;
}

int lp_cmp(const void *a, const void *b)
{
	lp_t *A = (lp_t *)a;
	lp_t *B = (lp_t *)b;

	//printf("%lld against %lld\n", A->hoofdadres, B->hoofdadres);
	if (A->hoofdadres < B->hoofdadres) return -1;
	if (A->hoofdadres > B->hoofdadres) return  1;
	return 0;
}

int vob_cmp(const void *a, const void *b)
{
	vob_t *A = (vob_t *)a;
	vob_t *B = (vob_t *)b;

	//printf("%lld against %lld\n", A->hoofdadres, B->hoofdadres);
	if (A->hoofdadres < B->hoofdadres) return -1;
	if (A->hoofdadres > B->hoofdadres) return  1;
	return 0;
}

int geo_cmp(const void *a, const void *b)
{
	geo_t *A = (geo_t *)a;
	geo_t *B = (geo_t *)b;

	//printf("%lld against %lld\n", A->hoofdadres, B->hoofdadres);
	if (A->id < B->id) return -1;
	if (A->id > B->id) return  1;
	return 0;
}

int orr_cmp(const void *a, const void *b)
{
	orr_t *A = (orr_t *)a;
	orr_t *B = (orr_t *)b;

	//printf("%lld against %lld\n", A->hoofdadres, B->hoofdadres);
	if (A->id < B->id) return -1;
	if (A->id > B->id) return  1;
	return 0;
}

int wpl_cmp(const void *a, const void *b)
{
	wpl_t *A = (wpl_t *)a;
	wpl_t *B = (wpl_t *)b;

	//printf("%lld against %lld\n", A->hoofdadres, B->hoofdadres);
	if (A->id < B->id) return -1;
	if (A->id > B->id) return  1;
	return 0;
}

int idx_cmp(const void *a, const void *b)
{
	index_t *A = (index_t *)a;
	index_t *B = (index_t *)b;

	//printf("%lld against %lld\n", A->hoofdadres, B->hoofdadres);
	if (A->ref_id < B->ref_id) return -1;
	if (A->ref_id > B->ref_id) return  1;
	return 0;
}

int combine_files()
{
#define HNCSV "huisnummer.csv"
#define VBCSV "verblijfsobject.csv"
#define LPCSV "ligplaatsen.csv"
#define SPCSV "standplaats.csv"
#define ORCSV "openbareruimte.csv"
#define WPCSV "woonplaats.csv"

	FILE *hnp = fopen(HNCSV, "r");
	FILE *vop = fopen(VBCSV, "r");
	FILE *lpp = fopen(LPCSV, "r");
	FILE *spp = fopen(SPCSV, "r");
	FILE *orp = fopen(ORCSV, "r");
	FILE *wpp = fopen(WPCSV, "r");

static char buf[MAXSIZE];

	if (!hnp) { printf("Could not open %s\n", HNCSV); exit(0); } 
	if (!vop) { printf("Could not open %s\n", VBCSV); exit(0); } 
	if (!lpp) { printf("Could not open %s\n", LPCSV); exit(0); } 
	if (!spp) { printf("Could not open %s\n", SPCSV); exit(0); } 
	if (!orp) { printf("Could not open %s\n", ORCSV); exit(0); } 
	if (!wpp) { printf("Could not open %s\n", WPCSV); exit(0); } 

	char *line;
	int hnrcount = 0;
	while ((line = fgets(buf,MAXSIZE,hnp)) != NULL) hnrcount++;
	hnr_t *huisnrs = (hnr_t *)calloc(sizeof(hnr_t),hnrcount);
	fseek(hnp,0,SEEK_SET);
	hnrcount=0;
	while ((line = fgets(buf,MAXSIZE,hnp)) != NULL) {
		hnr_t record;
		char *tok = strsep(&line,"\t");
		record.id = atoll(tok);
		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		record.huisnummer = atol(tok);
		tok = strsep(&line,"\t");
		record.huisletter = strdup(tok);
		tok = strsep(&line,"\t");
		record.huisnummertoevoeging = strdup(tok);
		tok = strsep(&line,"\t");
		record.postcode = strdup(tok);
		tok = strsep(&line,"\t");
		record.gerelateerdeOpenbareRuimte = atoll(tok);
	
		huisnrs[hnrcount] = record;
		hnrcount++;
	}

	int wplcount = 0;
	while ((line = fgets(buf,MAXSIZE,wpp)) != NULL) wplcount++;
	wpl_t *wpls = (wpl_t *)calloc(sizeof(wpl_t),wplcount);
	fseek(wpp,0,SEEK_SET);
	wplcount=0;
	while ((line = fgets(buf,MAXSIZE,wpp)) != NULL) {
		wpl_t record;
		char *tok = strsep(&line,"\t");
		record.id = atoll(tok);
		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		record.naam = strdup(tok);
		tok = strsep(&line,"\t");

		wpls[wplcount] = record;
		wplcount++;
	}

	//printf("Sorting %d or \n", wplcount);
	qsort(wpls,wplcount,sizeof(wpl_t),wpl_cmp);

	int orrcount = 0;
	while ((line = fgets(buf,MAXSIZE,orp)) != NULL) orrcount++;
	orr_t *oruimtes = (orr_t *)calloc(sizeof(orr_t),orrcount);
	fseek(orp,0,SEEK_SET);
	orrcount=0;
	while ((line = fgets(buf,MAXSIZE,orp)) != NULL) {
		orr_t record;

		char *tok = strsep(&line,"\t");
		record.id = atoll(tok);
		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		record.naam = strdup(tok);
// WARN : the data file 9999OPR08012013-000011.xml comes with a bug 
// its says Peppelpad\nPeppelpad that breaks this code, fix it bij hand 
// when you have new data files !! 
		tok = strsep(&line,"\t");
		record.type = strdup(tok);
		tok = strsep(&line,"\t"); // status
		tok = strsep(&line,"\t"); 
		record.wpl_id = atoll(tok);
	
		oruimtes[orrcount] = record;
		orrcount++;
	}

	//printf("Sorting %d or \n", orrcount);
	qsort(oruimtes,orrcount,sizeof(orr_t),orr_cmp);

#define RALLOCSIZE 100000

	int coordcount=0;
	int indexcount=0;
	geo_t *geos = (geo_t *)calloc(sizeof(geo_t),RALLOCSIZE);
	index_t *refs = (index_t *)calloc(sizeof(index_t),RALLOCSIZE);
	long long hoofdadres;
	long long nevenadres;

	geo_t record;
	index_t ref;

	while ((line = fgets(buf,MAXSIZE,vop)) != NULL)
	{

		char *tok = strsep(&line,"\t");
		record.id = atoll(tok);
		record.type = TYPE_VERBLIJF;
		ref.geo_id = record.id;

		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");

		hoofdadres = atoll(tok);

		ref.ref_id = hoofdadres;
		refs[indexcount] = ref;
		indexcount++; 
		if (indexcount % RALLOCSIZE == 0) 
			refs = (index_t *)realloc(refs,sizeof(index_t)*(indexcount+RALLOCSIZE));
		tok = strsep(&line,"\t");
		//record.functie = strdup(tok);
		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t"); 
		tok = strsep(&line,"\t");
		//record.gerelateerdPand = atoll(tok);
		tok = strsep(&line,"\t");
		int neven= atol(tok);
		int n;
		tok = strsep(&line,"\t"); 	// neven adresses ignored
		char *subtok = strtok(tok," ");
		for (n=0; n< neven; n++) {
			nevenadres = atoll(subtok);
			ref.ref_id = nevenadres;
			//printf("Adding %d of %d %lld -> %lld\n", n, neven, ref.ref_id, ref.geo_id);
			refs[indexcount] = ref;
			subtok = strtok(NULL," ");
		}
	
		tok = strsep(&line,"\t");
		record.gcount = atol(tok);
		tok = strsep(&line,"\t");
		record.geometry = (coord_t *)calloc(sizeof(coord_t),record.gcount);
		int c;
		subtok = strtok(tok," ");
		for (c=0; c< record.gcount; c++) {
			record.geometry[c].lat = atof(subtok);
			subtok = strtok(NULL," ");
			record.geometry[c].lng = atof(subtok);
			subtok = strtok(NULL," ");
		}
	
		geos[coordcount] = record;
		coordcount++;
		if (coordcount % RALLOCSIZE == 1) 
			geos = (geo_t *)realloc(geos,sizeof(geo_t)*(coordcount+RALLOCSIZE));
	}

	while ((line = fgets(buf,MAXSIZE,lpp)) != NULL)
	{
		char *tok = strsep(&line,"\t");
		record.id = atoll(tok);
		record.type = TYPE_LIGPLAATS;
		ref.geo_id = record.id;

		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		hoofdadres = atoll(tok);
		ref.ref_id = hoofdadres;
		refs[indexcount] = ref;
		indexcount++; 
		if (indexcount % RALLOCSIZE == 0) 
			refs = (index_t *)realloc(refs,sizeof(index_t)*(indexcount+RALLOCSIZE));
		tok = strsep(&line,"\t");
		//if (record.id != record.hoofdadres) printf("It happens !!\n");
		tok = strsep(&line,"\t");
		int neven= atol(tok);
		int n;
		tok = strsep(&line,"\t"); 	// neven adresses ignored
		char *subtok = strtok(tok," ");
		for (n=0; n< neven; n++) {
			nevenadres = atoll(subtok);
			ref.ref_id = nevenadres;
			refs[indexcount] = ref;
			indexcount++; 
			if (indexcount % RALLOCSIZE == 0) 
				refs = (index_t *)realloc(refs,sizeof(index_t)*(indexcount+RALLOCSIZE));
			subtok = strtok(NULL," ");
		}
		tok = strsep(&line,"\t");
		record.gcount = atol(tok);
		tok = strsep(&line,"\t");
		record.geometry = (coord_t *)calloc(sizeof(coord_t),record.gcount);
		int c;
		subtok = strtok(tok," ");
		for (c=0; c< record.gcount; c++) {
			record.geometry[c].lat = atof(subtok);
			subtok = strtok(NULL," ");
			record.geometry[c].lng = atof(subtok);
			subtok = strtok(NULL," ");
		}
	
		geos[coordcount] = record;
		coordcount++;
		if (coordcount % RALLOCSIZE == 1) 
			geos = (geo_t *)realloc(geos,sizeof(geo_t)*(coordcount+RALLOCSIZE));
	}

	while ((line = fgets(buf,MAXSIZE,spp)) != NULL)
	{
		geo_t record;
		index_t ref;

		char *tok = strsep(&line,"\t");
		record.id = atoll(tok);
		record.type = TYPE_LIGPLAATS;
		ref.geo_id = record.id;

		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		tok = strsep(&line,"\t");
		hoofdadres = atoll(tok);
		ref.ref_id = hoofdadres;
		refs[indexcount] = ref;
		indexcount++; 
		if (indexcount % RALLOCSIZE == 0) 
			refs = (index_t *)realloc(refs,sizeof(index_t)*(indexcount+RALLOCSIZE));
		tok = strsep(&line,"\t");
		//if (record.id != record.hoofdadres) printf("It happens !!\n");
		tok = strsep(&line,"\t");
		int neven= atol(tok);
		int n;
		tok = strsep(&line,"\t"); 	// neven adresses ignored
		char *subtok = strtok(tok," ");
		for (n=0; n< neven; n++) {
			nevenadres = atoll(subtok);
			ref.ref_id = nevenadres;
			refs[indexcount] = ref;
			indexcount++; 
			if (indexcount % RALLOCSIZE == 0) 
				refs = (index_t *)realloc(refs,sizeof(index_t)*(indexcount+RALLOCSIZE));
			subtok = strtok(NULL," ");
		}
		tok = strsep(&line,"\t");
		record.gcount = atol(tok);
		tok = strsep(&line,"\t");
		record.geometry = (coord_t *)calloc(sizeof(coord_t),record.gcount);
		int c;
		subtok = strtok(tok," ");
		for (c=0; c< record.gcount; c++) {
			record.geometry[c].lat = atof(subtok);
			subtok = strtok(NULL," ");
			record.geometry[c].lng = atof(subtok);
			subtok = strtok(NULL," ");
		}
	
		geos[coordcount] = record;
		coordcount++;
		if (coordcount % RALLOCSIZE == 1) 
			geos = (geo_t *)realloc(geos,sizeof(geo_t)*(coordcount+RALLOCSIZE));
	}

	qsort(geos,coordcount,sizeof(geo_t),geo_cmp);
	qsort(refs,indexcount,sizeof(index_t),idx_cmp);

	int i;
	for (i=0; i< hnrcount; i++) {
		geo_t *geop,geo;
		index_t *indexp,index;
		orr_t  *ortp, ort;
		wpl_t  *wplp, wpl;
		geo.id = huisnrs[i].id;
		geop = (geo_t *)bsearch(&geo,geos,coordcount,sizeof(geo_t),geo_cmp);

		ort.id = huisnrs[i].gerelateerdeOpenbareRuimte;
		ortp = (orr_t *)bsearch(&ort,oruimtes,orrcount,sizeof(orr_t), orr_cmp);
		char * orname="";
		if (ortp) orname = ortp->naam;
		else printf("Could not find openbare ruimte %lld\n", huisnrs[i].gerelateerdeOpenbareRuimte);

		char *wname="";
		wpl.id =  ortp->wpl_id;
		wplp = (wpl_t *)bsearch(&wpl,wpls,wplcount,sizeof(wpl_t), wpl_cmp);
		if (wplp) wname = wplp->naam;
		else printf("Could not find woonplaats %lld for %lld\n", ortp->wpl_id, ort.id);

		if (!geop) {
			index.ref_id = huisnrs[i].id;
			indexp = (index_t *)bsearch(&index,refs,indexcount,sizeof(index_t),idx_cmp);
			if (!indexp) ; //printf("Could not find reference %lld\n", index.ref_id);
			else {
				geo.id = indexp->geo_id;
				geop = (geo_t *)bsearch(&geo,geos,coordcount,sizeof(geo_t),geo_cmp);
			}
		} 

		if (!geop) ;//printf("Could not find geometry object for %lld\n", huisnrs[i].id);

		else printf("%lld\t%s\t%s\t%d\t%s\t%s\t%s\t%lld\t%f\t%f\n", huisnrs[i].id,huisnrs[i].postcode,orname,huisnrs[i].huisnummer,huisnrs[i].huisletter,huisnrs[i].huisnummertoevoeging,wname,huisnrs[i].gerelateerdeOpenbareRuimte,geop->geometry[0].lat, geop->geometry[0].lng);
	}

	fclose(hnp);
	fclose(vop);
	fclose(lpp);
	fclose(spp);
	fclose(orp);
	fclose(wpp);

	return 0;
}

/* maybe needed some day, so keep ?  */
int combine_files_complete()
{
#define HNCSV "huisnummer.csv"
#define VBCSV "verblijfsobject.csv"
#define LPCSV "ligplaatsen.csv"
#define SPCSV "standplaats.csv"
#define ORCSV "openbareruimte.csv"

	FILE *hnp = fopen(HNCSV, "r");
	FILE *vop = fopen(VBCSV, "r");
	FILE *lpp = fopen(LPCSV, "r");
	FILE *spp = fopen(SPCSV, "r");
	FILE *orp = fopen(ORCSV, "r");

static char buf[MAXSIZE];

	if (!hnp) { printf("Could not open %s\n", HNCSV); exit(0); } 
	if (!vop) { printf("Could not open %s\n", VBCSV); exit(0); } 
	if (!lpp) { printf("Could not open %s\n", LPCSV); exit(0); } 
	if (!spp) { printf("Could not open %s\n", SPCSV); exit(0); } 
	if (!orp) { printf("Could not open %s\n", ORCSV); exit(0); } 

	char *line;
	int hnrcount = 0;
	while ((line = fgets(buf,MAXSIZE,hnp)) != NULL) hnrcount++;
	hnr_t *huisnrs = (hnr_t *)calloc(sizeof(hnr_t),hnrcount);
	fseek(hnp,0,SEEK_SET);
	hnrcount=0;
	while ((line = fgets(buf,MAXSIZE,hnp)) != NULL) {
		hnr_t record;
		char *tok = strsep(&line,",");
		record.id = atoll(tok);
		tok = strsep(&line,",");
		tok = strsep(&line,",");
		tok = strsep(&line,",");
		record.huisnummer = atol(tok);
		tok = strsep(&line,",");
		record.huisletter = strdup(tok);
		tok = strsep(&line,",");
		record.huisnummertoevoeging = strdup(tok);
		tok = strsep(&line,",");
		record.postcode = strdup(tok);
		tok = strsep(&line,",");
		record.gerelateerdeOpenbareRuimte = atoll(tok);
	
		huisnrs[hnrcount] = record;
		hnrcount++;
	}


	int vobcount=0;
	while ((line = fgets(buf,MAXSIZE,vop)) != NULL) vobcount++; 
	fseek(vop,0,SEEK_SET);
	vob_t *vobs = (vob_t *)calloc(sizeof(vob_t),vobcount);
	vobcount=0;
	while ((line = fgets(buf,MAXSIZE,vop)) != NULL)
	{
		vob_t record;
		char *tok = strsep(&line,",");
		record.id = atoll(tok);
		tok = strsep(&line,",");
		tok = strsep(&line,",");
		tok = strsep(&line,",");
		record.hoofdadres = atoll(tok);
		tok = strsep(&line,",");
		//if (record.id != record.hoofdadres) printf("It happens !!\n");
		record.functie = strdup(tok);
		tok = strsep(&line,",");
		tok = strsep(&line,","); 
		record.oppervlakte = atol(tok);
		tok = strsep(&line,",");
		record.gerelateerdPand = atoll(tok);
		tok = strsep(&line,","); 	// neven adresses (count) ignored
		tok = strsep(&line,","); 	// neven adresses ignored
		tok = strsep(&line,",");
		record.gcount = atol(tok);
		tok = strsep(&line,",");
		record.geometry = (coord_t *)calloc(sizeof(coord_t),record.gcount);
		int c;
		char *subtok = strtok(tok," ");
		for (c=0; c< record.gcount; c++) {
			record.geometry[c].lat = atof(subtok);
			subtok = strtok(NULL," ");
			record.geometry[c].lng = atof(subtok);
			subtok = strtok(NULL," ");
		}
	
		vobs[vobcount] = record;
		vobcount++;
	}

	qsort(vobs,vobcount,sizeof(vob_t),vob_cmp);

	int lpcount=0;
	while ((line = fgets(buf,MAXSIZE,lpp)) != NULL) lpcount++; 
	fseek(lpp,0,SEEK_SET);
	lp_t *lps = (lp_t *)calloc(sizeof(lp_t),lpcount);
	lpcount=0;
	while ((line = fgets(buf,MAXSIZE,lpp)) != NULL)
	{
		lp_t record;
		char *tok = strsep(&line,",");
		record.id = atoll(tok);
		tok = strsep(&line,",");
		tok = strsep(&line,",");
		tok = strsep(&line,",");
		record.hoofdadres = atoll(tok);
		tok = strsep(&line,",");
		//if (record.id != record.hoofdadres) printf("It happens !!\n");
		tok = strsep(&line,","); 	// neven adresses (count) ignored
		tok = strsep(&line,","); 	// neven adresses ignored
		tok = strsep(&line,",");
		record.gcount = atol(tok);
		tok = strsep(&line,",");
		record.geometry = (coord_t *)calloc(sizeof(coord_t),record.gcount);
		int c;
		char *subtok = strtok(tok," ");
		for (c=0; c< record.gcount; c++) {
			record.geometry[c].lat = atof(subtok);
			subtok = strtok(NULL," ");
			record.geometry[c].lng = atof(subtok);
			subtok = strtok(NULL," ");
		}
	
		lps[lpcount] = record;
		lpcount++;
	}

	qsort(lps,lpcount,sizeof(lp_t),lp_cmp);

	int spcount=0;
	while ((line = fgets(buf,MAXSIZE,spp)) != NULL) spcount++; 
	fseek(spp,0,SEEK_SET);
	sp_t *sps = (sp_t *)calloc(sizeof(sp_t),spcount);
	spcount=0;
	while ((line = fgets(buf,MAXSIZE,spp)) != NULL)
	{
		sp_t record;
		char *tok = strsep(&line,",");
		record.id = atoll(tok);
		tok = strsep(&line,",");
		tok = strsep(&line,",");
		tok = strsep(&line,",");
		record.hoofdadres = atoll(tok);
		tok = strsep(&line,",");
		//if (record.id != record.hoofdadres) printf("It happens !!\n");
		tok = strsep(&line,","); 	// neven adresses (count) ignored
		tok = strsep(&line,","); 	// neven adresses ignored
		tok = strsep(&line,",");
		record.gcount = atol(tok);
		tok = strsep(&line,",");
		record.geometry = (coord_t *)calloc(sizeof(coord_t),record.gcount);
		int c;
		char *subtok = strtok(tok," ");
		for (c=0; c< record.gcount; c++) {
			record.geometry[c].lat = atof(subtok);
			subtok = strtok(NULL," ");
			record.geometry[c].lng = atof(subtok);
			subtok = strtok(NULL," ");
		}
	
		sps[spcount] = record;
		spcount++;
	}

	qsort(sps,spcount,sizeof(sp_t),sp_cmp);

	int i;
	for (i=0; i< hnrcount; i++) {
		vob_t srcvob;
		lp_t srclp;
		sp_t srcsp;
		srcvob.hoofdadres = huisnrs[i].id;
		srclp.hoofdadres = huisnrs[i].id;
		srcsp.hoofdadres = huisnrs[i].id;
		vob_t *vop = (vob_t *)bsearch(&srcvob,vobs,vobcount,sizeof(vob_t),vob_cmp);
		lp_t *lp = (lp_t *)bsearch(&srclp,lps,lpcount,sizeof(lp_t),lp_cmp);
		sp_t *sp = (sp_t *)bsearch(&srcsp,sps,spcount,sizeof(sp_t),sp_cmp);
		if (!vop && !lp && !sp) printf("Could not find vob for %lld\n", huisnrs[i].id);
		//else printf("Read %lld,%s,%d,%lld,%f,%f\n", huisnrs[i].id, huisnrs[i].postcode,huisnrs[i].huisnummer,huisnrs[i].gerelateerdeOpenbareRuimte,vop->geometry[0].lat, vop->geometry[0].lng);
	}
	for (i=0; i< 10; i++) {
		printf("Read %lld,%s,%lld,%f,%f\n", vobs[i].id, vobs[i].functie,vobs[i].hoofdadres, vobs[i].geometry[0].lat,vobs[i].geometry[0].lng);
	}

	fclose(hnp);
	fclose(vop);
	fclose(lpp);
	fclose(spp);

	return 0;
}

int main()
{
	//dir_num();
	//dir_opr();
	//dir_sta();
	//dir_pnd();
	//dir_vbo();
	//dir_wpl();
	//file_lpl();

	combine_files();

	return 0;
}
