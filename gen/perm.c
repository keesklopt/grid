#include <stdio.h>
#include <stdlib.h>

char *input="abcdefghijklmnopqrstuvw";

static int number_recurse(int k, int n, int d, int *arr, int (perform)(int *, int, void *), void *x)
{
	int a;
	if (d==0) return perform(arr,n,x);

	for (a=0; a< k; a++) {
		arr[n-d]=a;
		number_recurse(k,n,d-1,arr,perform,x);
	}
	return 0;
}

int number(int k, int n, int (perform)(int *, int, void *), void *x)
{
	int *arr=calloc(sizeof(int),n);

	number_recurse(k,n,n,arr,perform,x);

	free(arr);
	return 0;
}

static int permute_recurse(int k, int n, int d, int *arr, int (perform)(int *, int, void *), void *x)
{
	int a;
	if (d==0) return perform(arr,n,x);

	int *swap;
	int tmp;
	for (a=n-d; a< k; a++) {
		tmp = arr[n-d];
		arr[n-d] = arr[a];
		arr[a] = tmp;
		permute_recurse(k,n,d-1,arr,perform,x);
		arr[a] = arr[n-d];
		arr[n-d] = tmp;
	}
	return 0;
}

int permute(int k, int n, int (perform)(int *, int, void *), void *x)
{
	int *arr=calloc(sizeof(int),k); // TOTAL length, only use the first n 
	int a;

	for (a=0; a< k; a++) arr[a]=a;
	permute_recurse(k,n,n,arr,perform,x);

	free(arr);
	return 0;
}

int exjun(int *base, int n, void *xtra)
{
	int t;
	for (t=0; t< n; t++) {
		//printf("[%d]", base[t]);
		printf("%c", input[base[t]]);
	}
	printf("\n");
	return 0;
}

int main()
{
	//number(2,4,exjun,NULL);
	permute(11,11,exjun,NULL);
	return 0;
}
