#include <net.h>
#include <heap.h>
#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <signal.h>
#include <float.h>
#include <string.h>
#include <map>

FILE *dmpfile;

IO io;
LevelSquare levelinfo;
Haversine haversine;

int globcost=0;

Cost *a, *aa;
Cost *b, *bb;

road_t road_dontmatch={NULL,1};
node_t node_dontmatch={NULL,-1};
node_t clearnode= { 0,0,0,0,HEAP_CLEAR,0};
road_t clearroad= { NULL,0};

double searchdist[16];

void fill_searchdist(void)
{
	int t;
	searchdist[15] = SEARCHDIST;
	for (t=14; t>=0; t--) 
		searchdist[t] = searchdist[t+1]*2;
}

void draw_open(void)
{
	dmpfile = fopen("grid_draw", "w");
}

void draw_close(void)
{
	if (dmpfile) fclose(dmpfile);
}

/*
const float speeds[16] = {
	90/360.0, //#define AS_MOTORWAY         0
	75/360.0, //#define AS_MOTORWAY_LINK  1
	85/360.0, //#define AS_TRUNK            2
	70/360.0, //#define AS_TRUNK_LINK       3
	65/360.0, //#define AS_PRIMARY          4
	60/360.0, //#define AS_PRIMARY_LINK     5
	55/360.0, //#define AS_SECONDARY        6
	50/360.0, //#define AS_SECONDARY_LINK   7
	40/360.0, //#define AS_TERTIARY       8
	30/360.0, //#define AS_TERTIARY_LINK  9
	25/360.0, //#define AS_UNCLASSIFIED   10 
	25/360.0, //#define AS_RESIDENTIAL    11
	10/360.0, //#define AS_LIVINGSTREET     12
	15/360.0, //#define AS_SERVICE      13
	5/360.0,  //#define AS_FERRY            14
	50/360.0  //#define AS_DEFAULT      15
};
*/

int speeds[16] = { 
90, //#define AS_MOTORWAY     	0
75, //#define AS_MOTORWAY_LINK  1
85, //#define AS_TRUNK			2
70, //#define AS_TRUNK_LINK		3
65, //#define AS_PRIMARY      	4
60, //#define AS_PRIMARY_LINK  	5
55, //#define AS_SECONDARY    	6
50, //#define AS_SECONDARY_LINK	7
40, //#define AS_TERTIARY       8
30, //#define AS_TERTIARY_LINK  9
25, //#define AS_UNCLASSIFIED   10 
25, //#define AS_RESIDENTIAL    11
10, //#define AS_LIVINGSTREET 	12
15, //#define AS_SERVICE	 	13
5,  //#define AS_FERRY        	14
50  //#define AS_DEFAULT		15
};

int dump_node(node_t* nd,int extra)
{
	int o = OUT_BINARY;
	//tile_t *tileto = rd->tileto->tile;
	//node_t *nodeto = &tileto->nds[rd->nodeto];

#ifdef VERBOSE
	printf("node %d:%d cost %d (%d)\n", nd->tilenode->tp->tileno, nd->tilenode->offset, nd->cost, globcost-nd->cost);
#endif
	io.write_uint8(o, dmpfile,'n');
	io.write_comment(o,dmpfile,",");
	io.write_int32(o, dmpfile,nd->tilenode->tp->tileno);
	io.write_comment(o,dmpfile,",");
	io.write_int32(o, dmpfile,nd->tilenode->offset);
	io.write_comment(o,dmpfile,",");
	//printf("writing %d\n", extra);
	io.write_uint8(o, dmpfile,extra);
	io.write_comment(o,dmpfile,"\n");

	return 0;
}

int dump_road(tile_t *tp, road_t *rd,int extra)
{
	int o = OUT_BINARY;

	int64_t r = ((int64_t)rd - (int64_t)tp->rds);
	r /= sizeof(road_t);

	io.write_uint8(o, dmpfile,'r');
	io.write_comment(o,dmpfile,",");
	io.write_uint32(o, dmpfile,tp->num);
	io.write_comment(o,dmpfile,",");
	io.write_uint32(o, dmpfile,r);
	io.write_comment(o,dmpfile,",");
	io.write_uint8(o, dmpfile,extra);
	io.write_comment(o,dmpfile,"\n");
	//printf("n %d:%d\n", nd->tilenode->tp->num, nd->tilenode->offset);
	//printf("r %d:%d\n", nd->tilenode->tp->num, rd->nodeto);
	return 0;
}

// net_traverse delivers the path IN REVERSE : and the last element
// will be the FROM node and NULL as road, so catch rd==NULL
// 0<-a-1<-b-2<-c-3 comes in as :
// node 3, road c
// node 2, road b
// node 1, road a
// node 0, NULL
int dump(net_t *np, node_t* nd, road_t *rd,void *data)
{
	dump_node(nd,11);

	if (rd==NULL) return 0;
	dump_road(nd->tilenode->tp->tile, rd, 11);

	return 0;
}

road_t read_road(tcache_t *tc, FILE *fp)
{	
	road_t r;
	int32_t nodefrom;
	int32_t tileto;

	nodefrom = io.read_uint32(fp);
	tileto = io.read_uint32(fp);
	tcentry_t *tcep = tcache_find(tc,tileto);
	r.tileto = tcep;
	//printf("Set tile to %p\n", r.tileto);
	r.nodeto = io.read_uint32(fp);
	r.len = io.read_uint32(fp);
	r.dir = io.read_uint8(fp);
	r.type = io.read_uint8(fp);

	return r;
}

node_t read_node(tcache_t *tc,tile_t *tp, FILE *fp, FILE *fp2)
{	
	node_t n;
	n.lastroad = io.read_uint8(fp);
	n.rank = io.read_int32(fp);
	//printf("Read rank %d \n", n.rank);
	int32_t link;

	link = io.read_int32(fp2);
	if (link <0) n.link=NULL;
	else {
		int parent = levelinfo.parent(tp->num);
		if (tc->net->tcs[tp->level-1] == NULL) {
			n.link=NULL;
		}  else {
			tile_t *p = tcache_get(tc->net->tcs[tp->level-1], parent);
			if(p && p->nnodes) 
				n.link = p->nds[link].tilenode;
			else
				n.link = NULL;
		}
	}

	return n;
}

node_coord_t read_coord(FILE *fp)
{	
	node_coord_t n;
	n.tile = io.read_uint32(fp);
	n.node = io.read_uint32(fp);
	n.x = io.read_uint32(fp);
	n.y = io.read_uint32(fp);
//printf("coord read %d,%d \n", n.x,n.y);

	return n;
}

int roadcost(road_t road)
{
   	//int cost = (road.len)/speeds[(road.type)];
	//int cost = (road.len*360)/speeds[(road.type)];
	int cost = road.len;

   	//printf("cost : %d and %f is %d\n", road.road->len, speeds[road.road->type], cost);
   	if (cost < 1) cost = 1;
   	return cost;
}

static uint32_t *steps=NULL;
static uint32_t *parts=NULL;

void lprintf(const char *format, ...)
{
}

uint32_t *calcsteps(int depth)
{
	uint32_t step=0;
	int d;
	uint32_t quad=1;
	uint32_t wide=1;
	steps= (uint32_t *)calloc(sizeof(uint32_t), depth);
	parts= (uint32_t *)calloc(sizeof(uint32_t), depth);

	for (d=0; d< depth; d++) 
	{
		step += quad;
		steps[d]=step;
		parts[d]=wide;
		wide *=2;
		quad *=4;
		//printf("%u,%u,%u,%u\n", d,parts[d], parts[d]*parts[d]-1,steps[d]);
	}
	
	return steps;
}

// returns cm
double CalculateDistance( double nLat1, double nLon1, double nLat2, double nLon2 )
{
	return haversine.distance(nLat1,nLon1,nLat2,nLon2);
}

void net_reset(net_t *np,int lo, int hi)
{
	for (int t=lo; t<=hi; t++) 
		if (np->tcs[t]) 
			tcache_reset(np->tcs[t]);
}

void net_check_reset(net_t *np,int lo,int hi)
{
	for (int t=lo; t<=hi; t++) 
		if (np->tcs[t]) 
			tcache_check_reset(np->tcs[t]);
}

/*
int ccc_cmp_verbose(const void *a, const void *b)
{
	ccc_t *A = (ccc_t *)a;
	ccc_t *B = (ccc_t *)b;

	printf("%s against %s\n", A->carcode, B->carcode);
	return strcmp(A->carcode, B->carcode);
}

uint32_t readuint32(FILE *fp)
{
	uint32_t swapped;
	fread(&swapped,sizeof(uint32_t),1,fp);
	return ntohl(swapped);
}


slice_t tile_2_xy(TileAlgo *ta, unsigned int n, int depth)
{
	int l;
	unsigned int qn;
	int order=ORDER_TOPDOWN;
	int32_t d=depth-2;
	slice_t s={0};

	if (ta) order = ta->order;

	s.level=0;
	l=d;
	
	// heap has all toplevels first so :
	if (order == ORDER_HEAP) {
		for(l=0; l< depth; l++) {
			if (n >= steps[l]) {
				s.level++;
			}
		}
		n -= steps[s.level-1];
		l = s.level;
	}
	// now find quadrant 
	while (l-- >= 0) {
		if (order==ORDER_HEAP) {
			qn = parts[l];
			qn *= qn;
		} else {
			n--;
			if (n<0) break;
			qn = steps[l+1];
			s.level++;
		} 
		if (n >= qn*2) {
			s.x |= 1;
			n -= qn*2;
		}
		if (n >= qn) {
			s.y |= 1;
			n -= qn;
		}
		if (n<=0) break;
		//printf("%d,%d,%d,%d\n", n, qn, s.x, s.y);
		s.x <<=1;
		s.y <<=1;
	}

	return s;
}
*/

#define TEST_BIT_32(data,pos) (data & (1<<(pos)))
#define TEST_BIT_16(data,pos) (data & (1<<(pos)))

void net_dmp(net_t *np)
{
	if (!np) {
		printf("NULL net\n");
	}
	printf("tiles : %d\n", np->stat.ntiles);
	printf("nodes : %d\n", np->stat.nnodes);
	printf("roads : %d\n", np->stat.nroads);
	printf("min x : %d\n", np->stat.minx);
	printf("min y : %d\n", np->stat.miny);
	printf("max x : %d\n", np->stat.maxx);
	printf("max y : %d\n", np->stat.maxy);
	printf("max roads : %d\n", np->stat.maxroad);
	printf("max nodes : %d\n", np->stat.maxnode);
	printf("max forward : %d\n", np->stat.maxforward);
}

void net_statistics(net_t *np)
{
	net_dmp(np);

	np->stat.reset();

	for (np->nlevels=15; np->nlevels>0; np->nlevels--) {
		if (np->tcs[np->nlevels] == NULL) {
			break;
		}
		np->stat.add(&np->tcs[np->nlevels]->stat);
		np->tcs[np->nlevels]->stat.dump(false);
	}
	np->stat.dump(true);
	np->nlevels++;
}

int net_test(net_t *np)
{
	//int t;
	//int n;
	//int r;
	int l;


	for (l=0; l<16; l++) { 
		if (np->tcs[l]->fps) 
			//tcache_find_all(np->tcs[l]);
			tcache_get_all(np->tcs[l]);
	}
	
	return 0;
}

/*
static int net_read_params(net_t *np, FILE *fp)
{
	int8_t test[5]={0};
	//test[0] = readint8(fp);
	//test[1] = readint8(fp);
	//test[2] = readint8(fp);
	//test[3] = readint8(fp);
	//test[4] = '\0';
	
	//if (!strcmp((char *)test, "deal")) {
		//printf("Correct signature found\n");
	//} else 
	if (!strcmp((char *)test, "para")) {
		printf("This is a text format file\n");
		exit(0);
	//} else  {
		//printf("Unkown file format\n");
		//exit(0);
	}
	np->ntiles = readint32(fp);
	np->nnodes = readint32(fp);
	np->nroads = readint32(fp);
	np->min_x = readint32(fp);
	np->min_y = readint32(fp);
	np->max_x = readint32(fp);
	np->max_y = readint32(fp);
	np->nnames = readint32(fp);
	np->npostcodes = readint32(fp);
	np->ncountries = readint32(fp);

	return 0;
}
*/

/*
tile_t *net_read_tile(net_t *np, int32_t tnr)
{
	tile_t *tile= tile_ist(tnr);
	int l = levelinfo->level(tnr);
	node_t node;
	road_t road;
	int t;
	//sprintf(name,"%s/tile_%d", np->dir, tnr);
	//FILE *fp = fopen(name,"r");

	int nnodes=0;
	int nroads=0;
	
	//fseek(np->datp, offset, SEEK_SET);
	tile->num = tnr;
	nroads = io->readint32(np->levels[l].fps[ORD_INDEX]);

	for (t=0; t< nroads; t++) {
		//road.nodefrom = io->readint32(np->levels[l].fps[ORD_ROADS]);
		road.tileto = io->readint32(np->levels[l].fps[ORD_ROADS]);
		road.nodeto = io->readint32(np->levels[l].fps[ORD_ROADS]);
		road.len= io->readint32(np->levels[l].fps[ORD_ROADS]);
		road.dir= io->readint8(np->levels[l].fps[ORD_ROADS]);
		road.type= io->readint8(np->levels[l].fps[ORD_ROADS]);
		//printf("%d dir %d\n", t, tile.rds[t].type);

		//if (np->method == METHOD_LENGTH) {
			road.cost=road.len;
		//} else { // for now: METHOD_SPEED

		tile->rds[t] = road;
		//tile_add_road(tile,road);
	}

	nnodes = io->readint32(np->levels[l].fps[ORD_NODES]);
	int count = 0;
	int span;
	//printf("eeeuugg %d heeft %d nodes \n", tnr, tile.nnodes);
	for (t=0; t< nnodes; t++) {
		//node.x = readint32(fp);
		//node.y = readint32(fp);
		span = io->readint8(np->levels[l].fps[ORD_NODES]);
		node.firstroad= count;
		count += span;
		node.lastroad= count;
		node.tile= tnr;
		node.offset= t; // neede for node comparison
		node.cost = HEAP_CLEAR;

		//printf("%d len %d\n", t, node.firstroad);
		//printf("%d len %d\n", t, node.lastroad);
		//tile_add_node(tile,node);
	}
	
	return tile;
}
*/

tile_t *net_get(net_t *np, int32_t tnr)
{
	return tcache_get(np->tcs[15],tnr);
}

node_t *road_to_node(net_t *np, road_t *rd)
{
	//node_t *wn = (node_t *)malloc(sizeof(node_t));
	node_t *wn;

	tcache_get(rd->tileto->tc,rd->tileto->tileno);
	//if (!rd->tileto->tile) tcache_get(rd->tileto->tc,rd->tileto->tileno);
	wn= &rd->tileto->tile->nds[rd->nodeto];

	return wn;
}

tile_t *net_get_tile(net_t *np, int tileno)
{
	int level = levelinfo.level(tileno);
	tile_t *tp = tcache_get(np->tcs[level],tileno);

	return tp;
}

node_t *tilenode_get_node(net_t *np, tilenode_t *tn)
{
	tile_t *tp = tilenode_get_tile(np, tn);
	return &tp->nds[tn->offset];
}

tilenode_t *net_get_tilenode(net_t *np, int tileno, int offset)
{
	tile_t *tp = net_get_tile(np,tileno);
	return tp->nds[offset].tilenode;
}

tile_t *tilenode_get_tile(net_t *np, tilenode_t *nd)
{
	if (nd->tp != NULL) return nd->tp->tile;

	return tcache_get(np->tcs[nd->tp->level],nd->tp->tileno);
}

tile_t *node_get_tile(net_t *np, node_t *nd)
{
	if (nd->tilenode->tp != NULL) return nd->tilenode->tp->tile;
	return tcache_get(np->tcs[nd->tilenode->tp->level],nd->tilenode->tp->tileno);
}

road_t *node_nth_road(net_t *np, tile_t *tp, node_t *nd, int count)
{
	int offset;
	road_t *rd;
	//tcentry_t *other;

	//rd.nodeto = -1; // error condition

	offset = nd->firstroad + count;
	if (offset > nd->lastroad) return NULL;

	rd= &tp->rds[offset];
	
	//other = rd.tileto;
	//other->tile = tcache_get(np->tcs[level],other->tileno);
	//rd.nodeto = &other->tile->nds[rd.nodeto];

	return rd;
}

/*
int ll_cmp(const void *left, const void *right, void *nix)
{
	ll_t l = *(ll_t *)left;
	ll_t r = *(ll_t *)right;

	//printf("uueeh, %d and %d\n", l.lat, r.lat);
	if (l.lat < r.lat) return -1;
	if (l.lat > r.lat) return  1;
	// x doesn't really matter in this case, but do it anyway
	// to avoid mistakes when its use for other purposes
	if (l.lon < r.lon) return -1;
	if (l.lon > r.lon) return  1;
	return 0;
}

int pc_cmp(const void *left, const void *right)
{
	pc_t l = *(pc_t *)left;
	pc_t r = *(pc_t *)right;

	if (l.country < r.country) return -1;
	if (l.country > r.country) return  1;
	return strcmp(l.name, r.name);
}

int pc_cmp_verbose(const void *left, const void *right)
{
	pc_t *l = (pc_t *)left;
	pc_t *r = (pc_t *)right;

	printf("%u, %s against ", l->country, l->name);
	printf("%u, %s\n", r->country, r->name);

	if (l->country < r->country) return -1;
	if (l->country > r->country) return  1;
	return strcmp(l->name, r->name);
}

void pc_dmp(intptr_t wid, const void *data, void *xtra)
{
	//pc_t *pc = (pc_t *)data;

	//printf("%d:%d is %s\n", pc->tile, pc->offset, pc->name);
}

static int net_read_lls(net_t *np, FILE *fp)
{
	size_t l=0;
	ll_t  pc;

	//np->pcs = da_ist(sizeof(pc_t), np->npostcodes);
	np->lls = calloc(sizeof(ll_t), np->nnodes);

	while(l < np->nnodes) {
		pc.lat = readuint32(np->crdp);
		pc.lon = readuint32(np->crdp);
		pc.tile= readint32(np->crdp);
		pc.offset = readint32(np->crdp);
		np->lls[l] = pc;
		l++;
	}
	// should be sorted
	//qsort(np->lls, np->nnodes, sizeof(pc_t),ll_cmp);

	return 0;
}
*/

tilenode_t *net_get_node_by_ll(net_t *np, int32_t lat, int32_t lon, int *displacement)
{
	tilenode_t *wn = NULL;
	printf("Getting %d,%d in tilecache %p\n", lat, lon, np->tcs[15]);

	wn = tcache_get_node_by_ll(np->tcs[15],lat,lon,displacement);

	return wn;
}

tilenode_t *net_get_node_by_pc(net_t *np, uint16_t country, char *code)
{
	return NULL;
}

net_t *net_open(char *dir, char *network)
{
	net_t *n = (net_t *)calloc(sizeof(net_t),1);
	int l;

	n->dir= strdup(dir);
	n->ibase= strdup(network);

	fill_searchdist();

	for (l=0; l<= 15; l++) {
	/*
		sprintf(fname, "%s/%s/grid_%d_index",n->dir,n->ibase,l);
		n->levels[l].fps[ORD_INDEX] = fopen(fname,"r");
		sprintf(fname, "%s/%s/grid_%d_nodes",n->dir,n->ibase,l);
		n->levels[l].fps[ORD_NODES] = fopen(fname,"r");
		sprintf(fname, "%s/%s/grid_%d_roads",n->dir,n->ibase,l);
		n->levels[l].fps[ORD_ROADS] = fopen(fname,"r");
		sprintf(fname, "%s/%s/grid_%d_coord",n->dir,n->ibase,l);
		// open it, but don't use ?
		n->levels[l].fps[ORD_COORDS] = fopen(fname,"r");
	*/
		n->tcs[l] = tcache_ist(n,l,100000000);
	}
	if (n->tcs[15] == NULL) {
		// safe to say this network won't do anything
		goto error;
	}

	//printf("wat %d\n", wat);
	return n;

error:
	// cleanup !
	net_close(n);
	return NULL;
}

void hp_dmp(intptr_t wid, const void *data, void *udata)
{
	node_t *l=(node_t *)data;

	printf("%d\n", l->cost);
}

int heapcost1_cmp(const void *left, const void *right, void *data)
{
	node_t *l=(node_t *)left;
	node_t *r=(node_t *)right;

	return l->cost - r->cost;
}

int heapcost_cmp(const void *left, const void *right, void *data)
{
	node_t *l=(node_t *)left;
	node_t *r=(node_t *)right;

	return l->cost - r->cost;
}

int heapnode_cmp(const void *left, const void *right, void *data)
{
	node_t *l=(node_t *)left;
	node_t *r=(node_t *)right;

	if (l==r) return 0;
	return -1;
}

int heapold_cmp(const void *left, const void *right, void *data)
{
	node_t *l=*(node_t **)left;
	node_t *r=*(node_t **)right;

	return l->cost - r->cost;
}

Cost *net_edsger_single_old(net_t *np, Heap *heap, int level, tilenode_t *start,tilenode_t *from, int dir)
{
	node_t *next;
	node_t *nd;
	road_t *r=NULL;
	int counter;

	Cost *cost = new Cost();

	node_t *a;
	node_t *center;
	tile_t *tp;

	tp = tilenode_get_tile(np,from);
	center = &tp->nds[from->offset];

	if (start) {
		tp = tilenode_get_tile(np,start);
		a = &tp->nds[start->offset];

		a->cost = 0;
		heap->add(a);
	}
	//heap->dmp();

	while (1) {
		if (heap->empty()) {
			//printf("No endpoint found\n");
			goto cleanup;
		}
		next = heap->get();
		tp = node_get_tile(np,next);
		//tp = next->tilenode->tp;
		//printf("next is %d\n", next->tilenode->offset);
		
		if (!next) {
			//cost->dmp();
			delete heap;
			goto cleanup;
		}
		if (next->cost > MAX_COST) {
			printf("Reached closure\n");
			//cost->dmp();
			goto cleanup;
		}

		counter=0;
		for (r = node_nth_road(np,tp,next,counter); r != NULL; r = node_nth_road(np,tp,next,counter))
		{
			counter++;
			//printf("[%d] counter is %d:%d\n", counter, r->tileto->tileno, r->nodeto);

			//printf("Road goes to %d (%d)\n", r->tileto->tileno, r->tileto->tile->internal);
	//		if (!r->tileto->tile->internal) continue;
			nd = road_to_node(np,r);
			//printf("to node is %d, %d %d\n", nd->tilenode->offset, r->dir, nd->eval);
			//printf("is %d, %d\n", nd->tilenode->tile, np->num);
			if (r->dir == dir) continue; 
			if (nd->eval==1) continue;
			if (nd->cost == HEAP_CLEAR) { // new to the heap
				nd->cost= next->cost+ROADCOST((*r));
				//if (nd->link != NULL) { 
				double dist = haversine.distance(center->y,center->x,nd->y,nd->x);
				//printf("d : %f against %f\n", dist, searchdist[level]);
				if (dist > searchdist[level]) {
					//printf("Setting max to 1 for node %d:%d on %f\n", next->tilenode->tp->num, next->tilenode->offset,dist);
					nd->max=1;
				}
				heap->add(nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost+ROADCOST(*r) < nd->cost) {
					heap->del(nd);
					double dist = haversine.distance(center->y,center->x,nd->y,nd->x);
					nd->cost = next->cost+ROADCOST(*r);
					if (dist > searchdist[level]) {
				//		printf("Setting max to 1 for node %d:%d\n", next->tilenode->tp->num, next->tilenode->offset);
						nd->max=1;
					}
					heap->add(nd);
					//cost->set(nd);
				}
			}
		}
		heap->del(next);
		cost->add(next);
		//cost->dmp();
		next->eval = 1;
	}

cleanup:
	return cost; // cost
}

Cost *net_edsger_single(net_t *np, Heap *heap, int level, tilenode_t *from, int dir)
{
	node_t *next;
	node_t *nd;
	road_t *r=NULL;
	int counter;

	Cost *cost = new Cost();

	node_t *center;
	tile_t *tp;

	tp = tilenode_get_tile(np,from);
	center = &tp->nds[from->offset];

	//heap->dmp();

	while (1) {
		if (heap->empty()) {
			//printf("No endpoint found\n");
			goto cleanup;
		}
		next = heap->get();
		tp = node_get_tile(np,next);
#ifdef VERBOSE
	printf("next is %d:%d\n", tp->num, next->tilenode->offset);
#endif
		
		if (!next) {
			//cost->dmp();
			delete heap;
			goto cleanup;
		}

		counter=0;
		for (r = node_nth_road(np,tp,next,counter); r != NULL; r = node_nth_road(np,tp,next,counter))
		{
			counter++;
			//printf("[%d] counter is %d:%d\n", counter, r->tileto->tileno, r->nodeto);

	//		if (!r->tileto->tile->internal) continue;
			nd = road_to_node(np,r);
#ifdef VERBOSE
			printf("Road goes to %d (%d)\n", r->tileto->tileno, r->nodeto);
			printf("to node is %d, %d %d\n", nd->tilenode->offset, r->dir, nd->eval);
#endif
			//printf("is %d, %d\n", nd->tilenode->tile, np->num);
			if (r->dir == dir) continue; 
			if (nd->eval==1) continue;
			//printf("%d rank %d\n", next->rank, nd->rank);
			// GOT IT
			if (nd->rank < next->rank) continue;
			//printf("passsed cost is %d\n", next->cost);
			if (nd->cost == HEAP_CLEAR) { // new to the heap
				nd->cost= next->cost+ROADCOST((*r));
				heap->add(nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost+ROADCOST(*r) < nd->cost) {
					heap->del(nd);
					nd->cost = next->cost+ROADCOST(*r);
					heap->add(nd);
					//cost->set(nd);
				}
			}
		}
		heap->del(next);
		cost->add(next);
		//cost->dmp();
		next->eval = 1;
	}

cleanup:
	return cost; // cost
}

Cost *net_edsger_single_n(net_t *np, Heap *heap, int level, tilenode_t *node,int dir)
{
	node_t *next;
	node_t *nd;
	road_t *r=NULL;

	Cost *cost = new Cost();

	node_t *a;
	tile_t *tp;

	if (node) {
		tp = tilenode_get_tile(np,node);
		a = &tp->nds[node->offset];

		a->cost = 0;
		heap->add(a);
	}
	//heap->dmp();

	while (1) {
		if (heap->empty()) {
			//printf("No endpoint found\n");
			goto cleanup;
		}
		next = heap->get();
		tp = node_get_tile(np,next);
		//tp = next->tilenode->tp;
		//printf("next is %d\n", next->tilenode->offset);
		
		if (!next) {
			//cost->dmp();
			delete heap;
			goto cleanup;
		}
		if (next->cost > MAX_COST) {
			printf("Reached closure\n");
			//cost->dmp();
			goto cleanup;
		}

		int counter=0;
		for (r = node_nth_road(np,tp,next,counter); r != NULL; r = node_nth_road(np,tp,next,counter))
		{
			counter++;
			//printf("[%d] counter is %d:%d\n", counter, r->tileto->tileno, r->nodeto);

			//printf("Road goes to %d (%d)\n", r->tileto->tileno, r->tileto->tile->internal);
	//		if (!r->tileto->tile->internal) continue;
			nd = road_to_node(np,r);
			//printf("to node is %d, %d %d\n", nd->tilenode->offset, r->dir, nd->eval);
			//printf("is %d, %d\n", nd->tilenode->tile, np->num);
			if (r->dir == dir) continue; 
			if (nd->eval==1) continue;
			if (nd->cost == HEAP_CLEAR) { // new to the heap
				nd->cost= next->cost+ROADCOST((*r));
				if (nd->link != NULL) { 
					//printf("Setting max to 1 for node %d:%d\n", nd->tilenode->tp->tileno, nd->tilenode->offset);
					nd->max=1;
				}
				heap->add(nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost+ROADCOST(*r) < nd->cost) {
					heap->del(nd);
					nd->cost = next->cost+ROADCOST(*r);
					if (nd->link != NULL) {
						//printf("Setting max to 1 for node %d:%d\n", nd->tilenode->tp->tileno, nd->tilenode->offset);
						nd->max=1;
					}
					heap->add(nd);
					//cost->set(nd);
				}
			}
		}
		heap->del(next);
		cost->add(next);
		//cost->dmp();
		next->eval = 1;
	}

cleanup:
	return cost; // cost
}

Cost *net_edsger_single_9(net_t *np, Heap *heap, int level, tilenode_t *node,int dir)
{
	node_t *next;
	node_t *nd;
	road_t *r=NULL;

	Cost *cost = new Cost();

	node_t *a;
	tile_t *tp;

	if (node) {
		tp = tilenode_get_tile(np,node);
		a = &tp->nds[node->offset];

		a->cost = 0;
		heap->add(a);
	}
	//heap->dmp();

	while (1) {
		if (heap->empty()) {
			//printf("No endpoint found\n");
			goto cleanup;
		}
		next = heap->get();
		tp = node_get_tile(np,next);
		//tp = next->tilenode->tp;
		//printf("next is %d\n", next->tilenode->offset);
		
		if (!next) {
			//cost->dmp();
			delete heap;
			goto cleanup;
		}
		if (next->cost > MAX_COST) {
			printf("Reached closure\n");
			//cost->dmp();
			goto cleanup;
		}

		int counter=0;
		for (r = node_nth_road(np,tp,next,counter); r != NULL; r = node_nth_road(np,tp,next,counter))
		{
			counter++;
			//printf("[%d] counter is %d:%d\n", counter, r->tileto->tileno, r->nodeto);

			//printf("Road goes to %d (%d)\n", r->tileto->tileno, r->tileto->tile->internal);
			if (level > BREAK && !r->tileto->tile->internal) continue;
			nd = road_to_node(np,r);
			//printf("to node is %d, %d %d\n", nd->tilenode->offset, r->dir, nd->eval);
			//printf("is %d, %d\n", nd->tilenode->tile, np->num);
			if (r->dir == dir) continue; 
			if (nd->eval==1) continue;
			if (nd->cost == HEAP_CLEAR) { // new to the heap
				nd->cost= next->cost+ROADCOST((*r));
				if (nd->link != NULL) { 
					//printf("Setting max to 1 for node %d:%d\n", next->tilenode->tp->num, next->tilenode->offset);
					//nd->max=1;
				}
				heap->add(nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost+ROADCOST(*r) < nd->cost) {
					heap->del(nd);
					nd->cost = next->cost+ROADCOST(*r);
					if (nd->link != NULL) {
				//		printf("Setting max to 1 for node %d:%d\n", next->tilenode->tp->num, next->tilenode->offset);
						//nd->max=1;
					}
					heap->add(nd);
					//cost->set(nd);
				}
			}
		}
		heap->del(next);
		cost->add(next);
		//cost->dmp();
		next->eval = 1;
	}

cleanup:
	//heap->dmp();
	// TODO : probably here put all heap content into cost !!
	//heap->reset();
	//if (heap) delete heap;
	//heap=NULL;
	//cost->dmp();
	return cost; // cost
}

Cost *net_edsger_single_4(net_t *np, Heap *heap, int level, tilenode_t *node,int dir)
{
	node_t *next;
	node_t *nd;
	road_t *r=NULL;

	Cost *cost = new Cost();

	node_t *a;
	tile_t *tp;

	if (node) {
		tp = tilenode_get_tile(np,node);
		a = &tp->nds[node->offset];

		a->cost = 0;
		heap->add(a);
	}

	while (1) {
		if (heap->empty()) {
			//printf("No endpoint found\n");
			goto cleanup;
		}
		next = heap->get();
		tp = node_get_tile(np,next);
		//tp = next->tilenode->tp;
		//printf("next is %d\n", next->tilenode->offset);
		
		if (!next) {
			//cost->dmp();
			delete heap;
			goto cleanup;
		}
		if (next->cost > MAX_COST) {
			printf("Reached closure\n");
			//cost->dmp();
			goto cleanup;
		}

		int counter=0;
		for (r = node_nth_road(np,tp,next,counter); r != NULL; r = node_nth_road(np,tp,next,counter))
		{
			counter++;
			//printf("[%d] counter is %d:%d\n", counter, r->tileto->tileno, r->nodeto);

			//printf("Road goes to %d (%d)\n", r->tileto->tileno, r->tileto->tile->internal);
			if (!r->tileto->tile->internal) continue;
			nd = road_to_node(np,r);
			//printf("to node is %d, %d %d\n", nd->tilenode->offset, r->dir, nd->eval);
			//printf("is %d, %d\n", nd->tilenode->tile, np->num);
			if (r->dir == dir) continue; 
			if (nd->eval==1) continue;
			if (nd->cost == HEAP_CLEAR) { // new to the heap
				nd->cost= next->cost+ROADCOST((*r));
				if (nd->link != NULL) { 
					//printf("Setting max to 1 for node %d:%d\n", next->tilenode->tp->num, next->tilenode->offset);
					//nd->max=1;
				}
				heap->add(nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost+ROADCOST(*r) < nd->cost) {
					heap->del(nd);
					nd->cost = next->cost+ROADCOST(*r);
					if (nd->link != NULL) {
				//		printf("Setting max to 1 for node %d:%d\n", next->tilenode->tp->num, next->tilenode->offset);
						//nd->max=1;
					}
					heap->add(nd);
					//cost->set(nd);
				}
			}
		}
		heap->del(next);
		cost->add(next);
		//cost->dmp();
		next->eval = 1;
	}

cleanup:
	//heap->dmp();
	// TODO : probably here put all heap content into cost !!
	//heap->reset();
	//if (heap) delete heap;
	//heap=NULL;
	//cost->dmp();
	return cost; // cost
}


/* _n means neighbours, so this is the one not using elevate()
	but stops at (or over) the tile border
	networks generated with _9 function !!
 */
Cost *net_edsger_forward_9(net_t *np, Heap *h, int level,tilenode_t *from)
{
	return net_edsger_single_9(np,h,level,from,DIR_TOFROM);
}

Cost *net_edsger_backward_9(net_t *np, Heap *h,int level,tilenode_t *to)
{
	return net_edsger_single_9(np,h,level,to,DIR_FROMTO);
}

Cost *net_edsger_forward_n(net_t *np, Heap *h, int level,tilenode_t *from)
{
	return net_edsger_single_n(np,h,level,from,DIR_TOFROM);
}

Cost *net_edsger_backward_n(net_t *np, Heap *h,int level,tilenode_t *to)
{
	return net_edsger_single_n(np,h,level,to,DIR_FROMTO);
}

Cost *net_edsger_forward_4(net_t *np, Heap *h, int level,tilenode_t *from)
{
	return net_edsger_single_4(np,h,level,from,DIR_TOFROM);
}

Cost *net_edsger_backward_4(net_t *np, Heap *h,int level,tilenode_t *to)
{
	return net_edsger_single_4(np,h,level,to,DIR_FROMTO);
}

Cost *net_edsger_forward_old(net_t *np, Heap *h, int level,tilenode_t *start, tilenode_t *from)
{
	return net_edsger_single_old(np,h,level,start,from,DIR_TOFROM);
}

Cost *net_edsger_backward_old(net_t *np, Heap *h,int level,tilenode_t *start, tilenode_t *to)
{
	return net_edsger_single_old(np,h,level,start,to,DIR_FROMTO);
}

Cost *net_edsger_forward(net_t *np, Heap *h, int level,tilenode_t *from)
{
	return net_edsger_single(np,h,level,from,DIR_TOFROM);
}

Cost *net_edsger_backward(net_t *np, Heap *h,int level,tilenode_t *to)
{
	return net_edsger_single(np,h,level,to,DIR_FROMTO);
}

// to be clear, this is the one for 4 tile reduction, 
// _n is the one with elevate
result_t net_edsger_4(net_t *np, tilenode_t *from, tilenode_t *to)
{
	int l=15;
	result_t r= {11,22};
	Heap *heapa = new Heap(10000);
	Heap *heapb = new Heap(10000);

	costnode_t **a_arr;
	costnode_t **b_arr;
	//net_reset(np);

	aa = new Cost();

	int testtile = from->tp->tileno;

	while (l>0) {
		tile_t *tp = tcache_get(np->tcs[l], testtile);
		printf("New set is %d\n", tp->num);
		if (tp) tp->internal = 1;
		a = net_edsger_forward_4(np,heapa,l,from);

		a->draw(15-l);
		//a->draw(6);

		a_arr = a->get_array();
		aa->add(a_arr);
		delete(heapa);
		heapa = new Heap(100);
		for (costnode_t **ap = a_arr; *ap != NULL; (ap)++) {
			testtile = levelinfo.parent((*ap)->node->tilenode->tp->tileno);

			if ((*ap)->node->link) {
				tile_t *p = tilenode_get_tile(np,(*ap)->node->link);
				node_t *peer = &p->nds[(*ap)->node->link->offset];
				peer->cost = (*ap)->node->cost;
				peer->max = 0;
				peer->eval = 0;
				heapa->add(peer);
			}
		}
		//heapa->dmp();
		//a->dmp();
		a->reset();
		//net_reset(np,l,l);
		//net_check_reset(np,l,l);

		from=NULL;
		l--;
		tp->internal=0;
		if (np->tcs[l] == NULL) break;
	}

	l = 15;

	a->reset();

	bb = new Cost();
	testtile = to->tp->tileno;

	while (l>0) {
		tile_t *tp = tcache_get(np->tcs[l], testtile);
		if (tp) tp->internal = 1;
		b = net_edsger_backward_4(np,heapb,l,to);

		b->draw(15-l);
		//b->dmp();
		//b->draw(7);
		b_arr = b->get_array();
		bb->add(b_arr);
		delete(heapb);
		heapb = new Heap(100);
		for (costnode_t **ap = b_arr; *ap != NULL; (ap)++) {
			testtile = levelinfo.parent((*ap)->node->tilenode->tp->tileno);

			if ((*ap)->node->link) {
				tile_t *p = tilenode_get_tile(np, (*ap)->node->link);
				node_t *peer = &p->nds[(*ap)->node->link->offset];
				peer->cost = (*ap)->node->cost;
				peer->max = 0;
				peer->eval = 0;
				heapb->add(peer);
			}
		}
		//heapb->dmp();

		b->reset();
		//net_check_reset(np,l,l);

		to=NULL;
		l--;
		tp->internal=0;
		if (np->tcs[l] == NULL) break;
	}

	//printf("Now dumping aa\n");
	//aa->dmp();
	//printf("Now dumping bb\n");
	//bb->dmp();
	a_arr = aa->get_array();
	b_arr = bb->get_array();
	//printf("Done\n");
	//node_t *n;

	r = a->overlap(a_arr,b_arr);
	//if ( r.join != NULL)  {
		//printf("Got it %d:%d!!|\n", r.join->tilenode->tp->tileno, r.join->tilenode->offset );
		//dump_node(r.join,10);
		//net_traverse_cost(np,bb,n->tilenode,dump,NULL);
	//}

	//printf("Level is now %d\n", l);

	//node_t *meet = a->meet(b);

	return r;
}

result_t net_edsger(net_t *np, tilenode_t *from, tilenode_t *to)
{
	int l=15;
	//result_t r= {11,22};
	Heap *heapa = new Heap(10000);
	Heap *heapb = new Heap(10000);

	costnode_t **a_arr;
	costnode_t **b_arr;

	aa = new Cost();
	
	a = net_edsger_forward_n(np,heapa,l,from);
	a->draw(15-l);
	//a->dmp();
	//heapa->dmp();
	a_arr = a->get_array();
	aa->add(a_arr);

	heapa->elevate(np,a_arr);
	a->reset();

	l--;
	a = net_edsger_forward(np, heapa, l, from);
	a->draw(4);
	a_arr = a->get_array();
	aa->add(a_arr);
	a->reset();

	l = 15;
	bb = new Cost();
	b = net_edsger_backward_n(np,heapb,l,to);
	b->draw(15-l);
	b_arr = b->get_array();
	bb->add(b_arr);
	//bb->dmp();
	heapb->elevate(np,b_arr);
	b->reset();
	l--;
	b = net_edsger_backward(np, heapb, l, to);
	//printf("dump b\n");
	//b->dmp();
	b_arr = b->get_array();
	bb->add(b_arr);
	b->draw(5);
	b->reset();

//	printf("Dumping aa\n");
	//aa->dmp();
	a_arr = aa->get_array();
	b_arr = bb->get_array();
	result_t res;
	res = a->overlap(a_arr,b_arr);
	if ( res.join != NULL)  {
		//printf("Got it %d:%d!!|\n", res.join->tilenode->tp->tileno, res.join->tilenode->offset );
		dump_node(res.join,10);
		//net_traverse_cost(np,bb,n->tilenode,dump,NULL);
	}

	return res;
}

result_t net_edsger_old(net_t *np, tilenode_t *from, tilenode_t *to)
{
	int l=15;
	//result_t r= {11,22};
	Heap *heapa = new Heap(10000);
	Heap *heapb = new Heap(10000);

	costnode_t **a_arr;
	costnode_t **b_arr;

	tilenode_t *tnf=from;
	tilenode_t *tnt=to;

	//net_check_reset(np,l,l);

	aa = new Cost();

	while (l>1) {
		a = net_edsger_forward_old(np,heapa,l,from,tnf);
		a->draw(15-l);
		//a->dmp();
		a_arr = a->get_array();
		aa->add(a_arr);
		heapa->elevate(np,a_arr);
		//net_reset(np,l,l);
		//heapa->dmp();
		a->reset();
		//net_check_reset(np,l,l);

		from=NULL;
		l--;
		if (np->tcs[l] == NULL) break;
	}

	a->reset();

	l = 15;

	//net_reset(np);
	bb = new Cost();

	while (l>1) {
		b = net_edsger_backward_old(np,heapb,l,to,tnt);
		b->draw(15-l);
		//b->dmp();
		b_arr = b->get_array();
		bb->add(b_arr);
		//bb->dmp();
		heapb->elevate(np,b_arr);
		//net_reset(np,l,l);
		//heapb->dmp();

		b->reset();
		//net_check_reset(np,l,l);

		to=NULL;
		l--;
		if (np->tcs[l] == NULL) break;
	}

	//printf("Now dumping aa\n");
	//aa->dmp();
	a_arr = aa->get_array();
	b_arr = bb->get_array();
	result_t res;
	res = a->overlap(a_arr,b_arr);
	if ( res.join != NULL)  {
		//printf("Got it %d:%d!!|\n", res.join->tilenode->tp->num, res.join->tilenode->offset );
		//dump_node(res.join,10);
		//net_traverse_cost(np,bb,n->tilenode,dump,NULL);
	}

	return res;
}

result_t net_edsger_9(net_t *np, tilenode_t *from, tilenode_t *to)
{
	int l=15;
	result_t r= {11,22};
	Heap *heapa = new Heap(10000);
	Heap *heapb = new Heap(10000);

	costnode_t **a_arr;
	costnode_t **b_arr;
	//net_reset(np);

	aa = new Cost();

	map<int,int> internals;
	int *neighbours = levelinfo.neighbours(from->tp->tileno);

	for (int t=0;t< 9; t++) {
		if (neighbours[t]) {
			internals[neighbours[t]] = neighbours[t];
		}
	}
	internals[from->tp->tileno] = from->tp->tileno;

	while (l>= BREAK) {
		//printf("Level %d\n", l);
		//net_check_reset(np,l,l);

		map<int,int>::const_iterator key, end;
		end = internals.end();

		for (key=internals.begin(); key != end; key++) {
			//printf("New set is %d\n", key->first);
			tile_t *tp = tcache_get(np->tcs[l], key->first);
			if (tp) tp->internal = 1;
		}
		a = net_edsger_forward_9(np,heapa,l,from);

		//printf("Drawing\n");
		a->draw(15-l);

		a_arr = a->get_array();
		aa->add(a_arr);
		//printf("Dumping\n");
		//aa->dmp();
		internals.clear();
		delete(heapa);
		heapa = new Heap(100);
		for (costnode_t **ap = a_arr; *ap != NULL; (ap)++) {
			int testtile = levelinfo.parent((*ap)->node->tilenode->tp->tileno);
			int *neighbours = levelinfo.neighbours(testtile);
	
			for (int t=0;t< 9; t++) {
				if (neighbours[t]) {
					internals[neighbours[t]] = neighbours[t];
				}
			}
			internals[testtile] = testtile;

			if ((*ap)->node->link) {
				tile_t *p = tilenode_get_tile(np,(*ap)->node->link);
				node_t *peer = &p->nds[(*ap)->node->link->offset];
				peer->cost = (*ap)->node->cost;
				peer->max = 0;
				peer->eval = 0;
				heapa->add(peer);
			}
		}
		//heapa->dmp();

		a->reset();
		//net_reset(np);
		//net_check_reset(np,l,l);

		from=NULL;
		l--;
		if (np->tcs[l] == NULL) break;
	}

	l = 15;

	a->reset();
	//net_reset(np);

	bb = new Cost();
	neighbours = levelinfo.neighbours(to->tp->tileno);
	internals.clear();
	for (int t=0;t< 9; t++) {
		if (neighbours[t]) {
			internals[neighbours[t]] = neighbours[t];
		}
	}
	internals[to->tp->tileno] = to->tp->tileno;

	while (l>=BREAK) {
		//printf("Level %d\n", l);

		map<int,int>::const_iterator key, end;
		end = internals.end();

		for (key=internals.begin(); key != end; key++) {
			//printf("New set is %d\n", key->first);
			tile_t *tp = tcache_get(np->tcs[l], key->first);
			if (tp) tp->internal = 1;
		}
		b = net_edsger_backward_9(np,heapb,l,to);

		b->draw(15-l);
		b_arr = b->get_array();
		bb->add(b_arr);
		internals.clear();
		delete(heapb);
		heapb = new Heap(100);
		for (costnode_t **ap = b_arr; *ap != NULL; (ap)++) {
			int testtile = levelinfo.parent((*ap)->node->tilenode->tp->tileno);
			int *neighbours = levelinfo.neighbours(testtile);
	
			for (int t=0;t< 9; t++) {
				if (neighbours[t]) {
					internals[neighbours[t]] = neighbours[t];
				}
			}
			internals[testtile] = testtile;

			if ((*ap)->node->link) {
				tile_t *p = tilenode_get_tile(np,(*ap)->node->link);
				node_t *peer = &p->nds[(*ap)->node->link->offset];
				peer->cost = (*ap)->node->cost;
				peer->max = 0;
				peer->eval = 0;
				//printf("Copying %d from %d:%d to %d:%d\n", peer->cost, 
					//(*ap)->node->tilenode->tp->num, (*ap)->node->tilenode->offset,
					//peer->tilenode->tp->num, peer->tilenode->offset);
				heapb->add(peer);
			}
		}
		//heapb->dmp();

		b->reset();
		//net_check_reset(np,l,l);

		to=NULL;
		l--;
		if (np->tcs[l] == NULL) break;
	}
	//b = net_edsger_backward(np,heapb,l,to);

	//printf("Now dumping aa\n");
	//aa->dmp();
	//printf("Now dumping bb\n");
	//bb->dmp();
	a_arr = aa->get_array();
	b_arr = bb->get_array();
	//printf("Done\n");
	r = a->overlap(a_arr,b_arr);
	if ( r.join != NULL)  {
		//printf("Got it %d:%d!!|\n", r.join->tilenode->tp->tileno, r.join->tilenode->offset );
		//dump_node(r.join,10);
		//net_traverse_cost(np,bb,n->tilenode,dump,NULL);
	}

	//printf("Level is now %d\n", l);

	//node_t *meet = a->meet(b);

	return r;
}

result_t net_edsger_n(net_t *np, tilenode_t *from, tilenode_t *to)
{
	int l=15;
	result_t r= {11,22};
	Heap *heapa = new Heap(10000);
	Heap *heapb = new Heap(10000);

	costnode_t **a_arr;
	costnode_t **b_arr;
	//net_reset(np);

	aa = new Cost();

	while (l>1) {
		a = net_edsger_forward_n(np,heapa,l,from);
		a->draw(15-l);
		//a->dmp();
		a_arr = a->get_array();
		aa->add(a_arr);
		heapa->elevate(np,a_arr);
		//net_reset(np,l,l);
		//heapa->dmp();
		a->reset();
		net_check_reset(np,l,l);

		from=NULL;
		l--;
		if (np->tcs[l] == NULL) break;
	}

	a->reset();

	l = 15;

	//net_reset(np);
	bb = new Cost();

	while (l>1) {
		b = net_edsger_backward_n(np,heapb,l,to);
		b->draw(15-l);
		//b->dmp();
		b_arr = b->get_array();
		bb->add(b_arr);
		//bb->dmp();
		heapb->elevate(np,a_arr);
		//net_reset(np,l,l);
		//heapb->dmp();

		b->reset();

		to=NULL;
		l--;
		if (np->tcs[l] == NULL) break;
	}

	//printf("Now dumping aa\n");
	//aa->dmp();
	a_arr = aa->get_array();
	b_arr = bb->get_array();
	result_t res;
	res = a->overlap(a_arr,b_arr);
	if ( res.join != NULL)  {
		//printf("Got it %d:%d!!|\n", res.join->tilenode->tp->num, res.join->tilenode->offset );
		//dump_node(res.join,10);
		//net_traverse_cost(np,bb,n->tilenode,dump,NULL);
	}

	return r;
}

// don't leave level
// Note: edsgers are done backwards with direction check reverse, just so 
// the resulting path will be in the correct from->to order
result_t net_edsger_flat(net_t *np, int level, tilenode_t *from, tilenode_t *to)
{
	node_t *next;
	node_t *nd;
	//road_t r = clearroad;
	road_t *r;
	Heap *heap = new Heap(100);
	tile_t *tpa = tilenode_get_tile(np,to);
	tile_t *tpb = tilenode_get_tile(np,from);

	result_t res={0};

	node_t *a = &tpa->nds[to->offset];
	node_t *b = &tpb->nds[from->offset];

	a->cost = 0;
	heap->add(a);
	//heap->dmp();

	while (1) {
		if (heap->empty()) {
			//printf("No endpoint found\n");
			goto cleanup;
		}
		next = heap->get();
		tpa = node_get_tile(np,next);
		//printf("next is %d:%d %d\n", next->tilenode->tp->num, next->tilenode->offset, next->eval);
		
		if (!next) {
			//cost->dmp();
			delete heap;
			goto cleanup;
		}
		// could be run with b==NULL, which means complete the dijkstra
		if (b && next->tilenode->tp == b->tilenode->tp && 
		    next->tilenode->offset == b->tilenode->offset) {
			//printf("Found to node (%d) a cost %d (%d) minutes\n", b->tilenode->offset, next->cost, next->cost / 60000);
			globcost = next->cost;
			//cost->add(next);
			//heap->dmp();
			//cost->dmp();
			res.dm = next->cost;
			goto cleanup;
		}

		int counter=0;
		for (r = node_nth_road(np,tpa,next,counter); r!= NULL; r = node_nth_road(np,tpa,next,counter))
		{
			counter++;
			//printf("[%d] counter is %d %d:%d\n", r.nodefrom, counter, r.tileto, np->num);
			nd = road_to_node(np,r);
			//printf("to node is %d:%d, %d %d\n", nd->tilenode->tp->num, nd->tilenode->offset, r.dir, nd->eval);
			//printf("is %d, %d\n", nd->tile, np->num);
			if (nd->eval==1) continue;
			if (r->dir == DIR_FROMTO) continue; 
			if (nd->cost == HEAP_CLEAR) { // new to the heap
				nd->cost= next->cost+ROADCOST((*r));
				heap->add(nd);
			} else { // already in the heap
				// only set cost if lower
				if (next->cost+ROADCOST(*r) < nd->cost) {
					heap->del(nd);
					nd->cost = next->cost+ROADCOST(*r);
					heap->add(nd);
					//cost->set(nd);
				}
			}
		}
		heap->del(next);
		//cost->add(next);
		//cost->dmp();
		next->eval = 1;
		//heap->dmp();
		//printf("Setting %d to %d\n", next->tilenode->offset, next->eval);
	}

cleanup:
	if (heap) delete heap;
	heap=NULL;
	//cost->dmp();
	return res; // cost
}

int count_fnc (net_t *netp, node_t *np, road_t *rp,void *udata)
{
	int *c = (int *) udata;
	(*c)++;
	//printf("das %d\n", *c);
	return 0;
}

int time_fnc (net_t *netp, node_t *np, road_t *rp,void *udata)
{
	int *c = (int *) udata;

	(*c) += ROADCOST(*rp);
	//printf("das %d (%d,%d)\n", *c, ROADCOST(*rp), ROADCOST(*rp));
	return 0;
}

int dist_fnc (net_t *netp, node_t *np, road_t *rp,void *udata)
{
	int *c = (int *) udata;

	(*c)+= rp->len;
	//printf("das %d (%d,%d)\n", *c, nl, rl);
	return 0;
}

void net_traverse_cost(net_t *np, Cost *cst, tilenode_t *from, path_fnc_t what, void *data)
{
	int counter=0;
	//int level=1000;
	//int nl;

	node_t *cnode=NULL;
	node_t *next;
	node_t *nd;
	costnode_t *cnd;
	costnode_t *cnext;
	road_t *r;

	tile_t *tp = tilenode_get_tile(np,from);

	next = &tp->nds[from->offset];
	cnext = cst->get(next);
	while (cnext->cost > 0) { // from node has cost 0
		counter=0;
		// current level first
		printf("Trying %d:%d\n", next->tilenode->tp->tileno, next->tilenode->offset);
		for (r = node_nth_road(np,tp,next,counter); r!= NULL; r = node_nth_road(np,tp,next,counter))
		{
			printf("road to %d:%d\n", r->tileto->tileno, r->nodeto);
			counter++;
			nd = road_to_node(np,r);
			cnd = cst->get(nd);
			if (!cnd) continue;
			//nl = levelinfo.level(ndtest->tile->num);
			//if (nl < level) continue;
			if (r->dir == DIR_TOFROM) continue;
			printf("%d == %d + %d\n", cnext->cost , cnd->cost , ROADCOST(*r) );
			if (cnext->cost == cnd->cost + ROADCOST(*r) )  
			{
				cnode = nd;
				what(np,nd,r,data);
				break;
			}
		}
		next = cnode;
		tp = node_get_tile(np,next);
	}
}

/*
int net_seconds_cost(net_t *np, Cost *cst, node_t to)
{
	int c=0;
	net_traverse_cost(np, cst, &to, time_fnc, &c);
	return c;
}

int net_meters_cost(net_t *np, Cost *cst, node_t to)
{
	int c=0;
	net_traverse_cost(np, cst, &to, dist_fnc, &c);
	return c;
}
*/

void net_traverse(net_t *np, tilenode_t *to, path_fnc_t what, void *data)
{
	int counter=0;

	node_t *cnode;
	node_t *next;
	node_t *nd;
	road_t *r;

	tile_t *tp = tilenode_get_tile(np,to);

	next = &tp->nds[to->offset];
	while (next->cost > 0) { // from node has cost 0
		counter=0;
		// current level first
		for (r = node_nth_road(np,tp,next,counter); r != NULL; r = node_nth_road(np,tp,next,counter))
		{
			counter++;
			//node_t *was = nd;
			nd = road_to_node(np,r);
			if (r->dir == DIR_TOFROM) continue;
			//printf("cost is %d = %d + %d\n", 
			//next->cost , nd->cost , ROADCOST(*r));
			if (next->cost == nd->cost + ROADCOST(*r) )  
			{
			//printf("node now %d:%d from road %d\n", tp->num, nd->tilenode->offset, was->firstroad+counter);
				cnode = nd;
				what(np,next,r,data);
				break; // for multiple shortest paths
			}
		}
		//proceed:
		next = cnode;
		tp = node_get_tile(np,next);
	}
	what(np,next,NULL,data);
}

/*
path_t *net_path_flat(net_t *np, tilenode_t *from, tilenode_t *to, int *pathlen)
{
	path_t *path;
	int counter=0;
	result_t tst = net_edsger(np,15,from,to);

	//if (tst < 0) return NULL;
	
	node_t *cnode;
	node_t *next;
	node_t *nd;
	road_t r = {0};
	int len=0;

	next = &to;
	while ( (next->tilenode->tp != from->tilenode->tile) && 
			(next->tilenode->offset != from->tilenode->offset)) {
		counter=0;
		for (r = node_nth_road(np,next,counter); r.nodeto != NULL; r = node_nth_road(np,next,counter))
		{
			counter++;
			nd = road_to_node(np,r);
			if (r.road->dir == DIR_FROMTO) continue;
			//printf("testing %d=%d+%d \n", next->cost, nd->cost, ROADCOST(r));
			if (next->cost == nd->cost + ROADCOST(r) )  
			{
				cnode = nd;
				//printf("found road %d to %d:%d %d long\n", r.nodefrom,r.tileto,r.nodeto,r.len);
				len++;
			}
		}
		next = cnode;
	}
	path = (path_t *)calloc(sizeof(path_t), len+1);
	path[len].r = road_dontmatch ; // one more node then roads
	path[len].n = to;

	if (pathlen) *pathlen=len+1;
	next = &to;
	while (next->node != from.node) {
		counter=0;
		for (r = node_nth_road(np,next,counter); r.nodeto != NULL; r = node_nth_road(np,next,counter))
		{
			counter++;
			nd = road_to_node(np,r);
			if (r.road->dir == DIR_FROMTO) continue;
			if (next->cost == nd->cost + ROADCOST(r) )  
			{
				cnode = nd;
				len--;
				path[len].r=r;
				path[len].n=*nd;
			}
		}
		next = cnode;
	}
	return path;
}
*/

void net_close(net_t *np)
{
	if (!np) return;

	for (int l=0; l<= 15; l++) {
		if (np->tcs[l]) tcache_rls(np->tcs[l]);
	}
	free(np);
}
