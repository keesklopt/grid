#include <net.h>

tile_t *tile_ist(int32_t num)
{
	tile_t *tp = (tile_t *)calloc(sizeof(tile_t),1);

    tp->nds= NULL;
    tp->rds= NULL;
    tp->num = num;
    tp->level = levelinfo.level(num);
    tp->parent = levelinfo.parent(num);

    return tp;
}

void tile_rls(tile_t *tp)
{
    if (!tp) return;
    //printf("Deleting tile %d with %p %p %p %p\n", tp->num, tp, tp->nds, tp->rds, tp);
    if (tp->nds) free(tp->nds);
    if (tp->rds) free(tp->rds);
    free(tp);
}

void tile_check_reset(tile_t *tp)
{
	int r;
	int n;

	if (!tp) return;
	
	for (r=0; r< tp->nroads; r++) {
	//	tp->rds[r].keep=1;
	//	tp->rds[r].cost=roadcost(tp->rds[r]);
//#define ROADCOST(road) ((road.len)/speeds[road.type])
	}
	for (n=0; n< tp->nnodes; n++) {
		//tp->nds[n].keep=1;
		if ( tp->nds[n].cost!=HEAP_CLEAR) 
			printf("Nope %d:%d !! cost is wrong %d\n", tp->num, n, tp->nds[n].cost);
		if (tp->nds[n].eval!=0)
			printf("Nope %d:%d !! eval is wrong\n", tp->num, n);
		if (tp->nds[n].max!=0)
			printf("Nope %d:%d !! max is wrong\n", tp->num, n);
	}
	tp->offset = 0;
}

void tile_reset(tile_t *tp)
{
	int r;
	int n;

	if (!tp) return;

	tp->internal=0;
	
	for (r=0; r< tp->nroads; r++) {
	//	tp->rds[r].keep=1;
	//	tp->rds[r].cost=roadcost(tp->rds[r]);
//#define ROADCOST(road) ((road.len)/speeds[road.type])
	}
	for (n=0; n< tp->nnodes; n++) {
		//tp->nds[n].keep=1;
		tp->nds[n].cost=HEAP_CLEAR;
		tp->nds[n].max=0;
		tp->nds[n].eval=0;
	}
	tp->offset = 0;
}

void tile_dmp(tile_t *tp, int verbose)
{
	int t;
	printf("Tile dmp %d: (%d nodes/%d roads)\n", tp->num, tp->nnodes, tp->nroads);
	if (!verbose) return;
	for (t=0; t< tp->nnodes; t++) {
		node_t nd = tp->nds[t];
		printf("[%d] %d:%d [%d-%d], (%d,%d) \n", t, tp->num, nd.tilenode->offset, nd.firstroad, nd.lastroad, nd.x, nd.y);
	}
	printf("(%d roads)\n", tp->nroads);
	for (t=0; t< tp->nroads; t++) {
		road_t rd = tp->rds[t];
		const char *dir = "<->";
		if (rd.dir == DIR_FROMTO) dir=" ->";
		if (rd.dir == DIR_TOFROM) dir="<- ";
		printf("[%d] %s %d:%d, len %d\n", t, dir, rd.tileto->tileno,rd.nodeto, rd.len);
	}
}

