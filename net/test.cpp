#include <net.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <float.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <ftw.h>
#include <dirent.h>
#include <signal.h>

int nnodes;

tcache_t *tc[16];
typedef struct clt_tag
{
	string code;
	double lat;
	double lon;
} clt_t;

clt_t rit1[] = {
 { "ORIG",52.2560000,4.7960000},
 { "09LO",52.2927377,5.2324384},
 { "10ZZ",52.3020494,5.2551977},
 { "12HU",52.3672309,5.2094591},
 { "29XU",52.371353,5.222124},
 { "12CC",52.3138844,5.0347502},
 { "14KM",52.52302,5.487309},
 { "16WP",52.5029964,5.4969436},
 { "12IX",52.4562372,5.6910016},
 { "11OD",52.5207617,5.7263693},
 { "11ZL",52.5119006,5.7230311},
 { "23UT",52.667,5.6039307},
};

static int net_intrand(int lo, int hi)
{
  double diff = (hi - lo)+1;

  return lo + (int)(diff * rand() / (RAND_MAX + 1.0));
}

/*rit_t din1[] = {
 20OJ,52.2219963,5.9514701
 06ZL,51.9254038,6.6028777
 17MZ,52.2151307,5.9715468
 17FK,52.2317436,5.9952825
 17CJ,52.2239121,6.0033479
 17YJ,52.193795,6.0149098
 17IW,52.1980287,5.9854375
 17TW,52.1980287,5.9854375
 17UB,52.2042246,5.9936398
 17SP,52.201914,5.9710893
 17RQ,52.1919695,5.9510022
 17OM,52.2548012,5.9629144
 05MG,52.1636913,5.9842315
 04UV,52.1226743,6.0211526
 17ZQ0,52.1158193,6.0205881
}
*/

typedef struct td_cache_tag
{
	int a;
	int b;
	int dist;
} td_cache_t;

/*
static int int_cmp(const void *a, const void *b)
{
    const int A=*(const int *)a;
    const int B=*(const int *)b;

    //printf("Testing %d and %d\n", A, B);
    if (A< B) return -1;
    if (A> B) return  1;
    return 0;
}
*/

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
    printf("Caught segfault at address %p\n", si->si_addr);
    exit(0);
}

/* this will help complete the output of redirection 
 when encountering a sigfault */
void setup_sig(void)
{
	struct sigaction sa;

	memset(&sa, 0, sizeof(struct sigaction));
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = segfault_sigaction;
    sa.sa_flags   = SA_SIGINFO;

	sigaction(SIGSEGV, &sa, NULL);
}

int ptv()
{
	//char *url = "http://10.2.2.20/";
	return 0;
}

int main()
{
	net_t *np;
	tilenode_t *from, *to;
	//path_t *path;

	printf("-- Opening network -- \n");

	//for (int t=0; t< 100; t++) {
		//printf("%d\n", intrand(3,3));
	//}
	//exit(0);

	setup_sig();

	np = net_open((char *)DAT_INDIR, (char *)DEFAULTNETWORK);
	net_test(np);

	//test_lux(np);

	//exit(0);

	if (!np) {
		printf("could not open network %s\n", DEFAULTNETWORK);
		return -1;
	}

	net_statistics(np);

    //from = net_get_node_by_ll(np, 519753640, 41345390, NULL);
    //to = net_get_node_by_ll(np, 523918920, 46493630, NULL);

	//from = (tilenode_t *)calloc(sizeof(tilenode_t,1));
	//to = (tilenode_t *)calloc(sizeof(tilenode_t,1));

	srand(0);

	//from = net_get_tilenode(np, 1299649719,109);
	//to = net_get_tilenode(np, 1299757420,76);

	//printf("From is %d:%d\n", from->tp->tileno, from->offset);
	//printf("To is %d:%d\n", to->tp->tileno, to->offset);


	//Cost *cst;
	result_t res1,res2;
	//net_reset(np,0,15);
	
	struct timeval start, end;

#define TESTING 1
#define STARTLOOP 1
#define STOPLOOP 110000

//#define EXHAUSTIVE 1
//#define ONE_STEP 1

#define X 0
#define Y 0

/*
0->78 : Fail from 1299649681:0 to 1299649684:21 (5922 vs 0)
0->82 : Fail from 1299649681:0 to 1299649684:25 (6190 vs 0)
0->86 : Fail from 1299649681:0 to 1299649684:29 (6679 vs 0)
0->90 : Fail from 1299649681:0 to 1299649684:33 (7159 vs 0)
0->91 : Fail from 1299649681:0 to 1299649684:34 (7483 vs 0)
0->92 : Fail from 1299649681:0 to 1299649684:35 (7628 vs 0)
0->93 : Fail from 1299649681:0 to 1299649684:36 (7782 vs 0)
0->94 : Fail from 1299649681:0 to 1299649684:37 (7957 vs 0)
0->95 : Fail from 129964
*/

	tcarray_t choices = tcache_get_array(np->tcs[15]);
	int nnds=0;

	for (int x=0; x< choices.len; x++) {
		tcentry_t *tc = choices.items[x];
		int nnodes = tc->tile->nnodes;
		for (int y=0; y< nnodes; y++) {
			nnds++;
		}
	}
	
	tilenode_t **tnds= (tilenode_t **)calloc(sizeof(tilenode_t *),nnds);
	nnds=0;
	for (int x=0; x< choices.len; x++) {
		tcentry_t *tc = choices.items[x];
		int nnodes = tc->tile->nnodes;
		for (int y=0; y< nnodes; y++) {
			tnds[nnds] = tc->tile->nds[y].tilenode;
			nnds++;
		}
	}
		
	printf("%d choices\n", nnds);
	net_reset(np,0,15);

	draw_open();
	gettimeofday(&start,NULL);

#ifdef EXHAUSTIVE
	for (int f=X; f< nnds; f++) {
		printf("%d/%d          \n", f, nnds);
	for (int t=Y; t< nnds; t++) {
#else
	for (int x=0; x<= STOPLOOP; x++) {
		int f = net_intrand(0, choices.len-1);
		int t = net_intrand(0, choices.len-1);
		tcentry_t *ftc = choices.items[f];
		tcentry_t *ttc = choices.items[t];
		int fn = net_intrand(0,ftc->tile->nnodes-1);
		int tn = net_intrand(0,ttc->tile->nnodes-1);
		from = ftc->tile->nds[fn].tilenode;
		to = ttc->tile->nds[tn].tilenode;
#endif
		from = tnds[f];
		to = tnds[t];

		printf("%d:%d to %d:%d\n", from->tp->tileno, from->offset, to->tp->tileno, to->offset);
		//if (x< STARTLOOP) continue; // already tested

	// problem nodes : 
	//from = net_get_tilenode(np, 1299971045,7);
	//to = net_get_tilenode(np, 1299660393,76);

#ifdef TESTING
		res1 = net_edsger_flat(np,15,from,to);
		//printf("Result is %d decimeters (%f km)\n", res1.dm, (double)res1.dm/10000.0);
		net_traverse(np,from,dump,NULL);
		node_t *fnode = tilenode_get_node(np,from);
		node_t *tnode = tilenode_get_node(np,to);
		dump_node(fnode,12);
		dump_node(tnode,13);
		net_reset(np,0,15);
#endif

		res2 = net_edsger(np, from, to);
#ifdef TESTING
		if (res1.dm != res2.dm) {
			printf("%d->%d : Fail from %d:%d to %d:%d (%d vs %d)\n", f,t, from->tp->tileno, from->offset, to->tp->tileno, to->offset, res1.dm,res2.dm);
		//exit(0);
		}
#endif
		//printf("Cst %d\n", res2.dm);
		//net_reset(np,0,15);

#ifdef ONE_STEP
break;
#endif
	}
#ifdef ONE_STEP
break;
#endif
#ifdef EXHAUSTIVE
	}
#endif
	gettimeofday(&end,NULL);
	draw_close();

	double diff = ((end.tv_sec * 1000000.0 + end.tv_usec) - (start.tv_sec * 1000000.0 + start.tv_usec)) / 1000000.0;

	printf("%f sec %f per dijkstra\n", diff, diff/(STOPLOOP-STARTLOOP+1));

	/*
	net_reset(np);
	res = net_edsger(np,from,to);
	printf("result %d,%d\n", res.dm,res.ms);
	*/

	exit(0);

	//worknode_t one, two;
	// one way test :
	//s:5090,357913942:5084,106,0
	//s:5090,357913942:5093,79,2
	//s:5093,357913942:5090,79,1
	//one.tile= 357913942;
	//one.offset= 5090;
	//two.tile= 357913942;
	//two.offset= 5093;

	//meters = net_shortest(np, &one, &two);
	//printf("Thats' %d m\n", meters);
	//meters = net_shortest(np, &two, &one);
	//printf("Thats' %d m\n", meters);
}

