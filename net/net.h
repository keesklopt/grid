#include <grid.h>
#include <level.h>

#define BREAK 1
//#define VERBOSE

#define DAT_INDIR "../dat"
//#define DEFAULTNETWORK "n9"
//#define DEFAULTNETWORK "netherlands"
//#define DEFAULTNETWORK "europe"
//#define DEFAULTNETWORK "luxembourg"
#define DEFAULTNETWORK "andorra"
//#define DEFAULTNETWORK "hoek"
//#define DEFAULTNETWORK "small"
//#define DEFAULTNETWORK "haarlem"

//#define DAT_INDIR ".."
//#define DEFAULTNETWORK "vis"

#define MAX_COST 500000000

// level 15 is roughly 1223 meters wide 610 high so we take the largest
// earth = 40075.16 km along the equator, 1223 * 2^15 is 40.075.264
// earth = 40008.00 km along the poles, 611 * 2^16 is 40.042.496
// thats slightly bigger so it will cover the search area
// remember as the earth unfolds, the whole equator is stretched but
// only length from north to south pole is used for height 
#define CELLWIDTH 1223
#define CELLHIGHT 611
// the largest possible search dist in 9 cells is 
// square root of (3*1223)^2 + (3*611)^2 = 4101.396
//#define SEARCHDIST 4102
//#define SEARCHDIST 3800 // ok for netherlands : 100
#define SEARCHDIST 3800
// possibly this can be smaller ? try out

/* europe counts : 
ntiles : 8086058
nnodes : 128073269	// these do contains duplicates
nroads : 544784348	// these do contains duplicates
miny : 326341735	// boundbox of europe
maxy : 711681329
minx : -312643345
maxx : 461237286
minlen : 1			// no 0 lengths, good
maxlen : 2951926	// maxlen stays below ROAD_MAX 8388607
maxroad : 4061 		// can we use int16 for node and road offsets ??
maxnode : 1949		// biggest count in all europe 
maxforward : 16		// never more than 16 ways out, 256 allowed
*/

/*  netherlands
ntiles : 90277
nnodes : 2915076
nroads : 12888964
miny : 507510732
maxy : 534972100
minx : 33588772
maxx : 72278926
minlen : 1
maxlen : 238132
maxroad : 1110
maxnode : 498
maxforward : 8
*/

//#define ROADCOST(rd) rd.road->len


typedef struct tcentry_tag tcentry_t;
typedef struct tile_tag tile_t;
typedef struct tilenode_tag tilenode_t;

class Cost;

#define ROADCOST(rd) roadcost(rd)

/* struct used to uniquely identify a node */
typedef struct node_tag
{
	//int32_t offset; // node index 
	tilenode_t *tilenode;
	int32_t firstroad;
	int32_t lastroad;
	tilenode_t *link;
	int32_t rank;
	int32_t cost : 30;
	uint32_t eval : 1;
	uint32_t max : 1; //
#ifndef SKIP_COORDS
	int32_t x;
	int32_t y;
#endif
} node_t;

struct tilenode_tag
{
	//tile_t *tp;
	tcentry_t *tp;
	int32_t offset;
};

typedef struct costnode_tag
{
//	tile_t *tile;
	node_t *node;
	int32_t cost;
} costnode_t;

typedef struct result_tag
{
	int dm;
	int ms;
	node_t *join;
} result_t;

typedef struct road_tag
{
	tcentry_t *tileto;
	int32_t nodeto;
	uint32_t len: 23; // in dm length max is 8388607 (838 km)
	int32_t type: 5;
	uint32_t dir: 2;
} road_t;

#define ROAD_MAX 8388607
// europe's biggest road november 2012 : 2951926
/*
typedef struct workroad_tag
{
	road_t *road;
	node_t *nodeto;
	int32_t cost;
} road_t;
*/

typedef struct path_tag {
	node_t n;
	road_t r;
} path_t;

extern road_t road_dontmatch;
extern node_t node_dontmatch;
extern node_t clearnode;
extern road_t clearroad;

struct tile_tag
{
	int8_t  level;
	int8_t  done;
	int8_t  internal;
	int32_t num;
	int32_t parent;
	int32_t nnodes;
	int32_t nroads;
	node_t *nds;
	road_t *rds;
	int32_t offset; // for renumbering on higher level
};

typedef struct tcache_tag tcache_t;

struct tcentry_tag
{
	tcache_t *tc;
	int32_t tileno;
	tile_t *tile;
	int8_t  level;

	uint32_t stamp : 29;
	uint32_t clean : 1;
	uint32_t done : 1;
	uint32_t incore : 1;
} ;

typedef struct tcarray_tag
{
    int len;
    tcentry_t **items;
} tcarray_t;

typedef struct net_tag
{
	Stats stat;

	char *dir; 
	char *ibase; 

	//ll_t       *lls;
	//pc_t       *pcs;
	//ccc_t      *ccs;
	tcache_t   *tcs[16];
	int         nlevels;
} net_t;

struct tcache_tag 
{
	int max;
	int nindices;
	int ntiles;
	int incore;
	int stamp; // 'time' stamp for the tiles
	void *tcs; // gnu binary tree (tiles)
	void *idx; // gnu binary tree (index)
	int nmarks;
	int level;
    tcentry_t **marks; // seperately mark tiles, for deletion etc
    tcarray_t arr;
	net_t *net;
	node_coord_t *coords;
	void *hook; // general purpose

	// level files 
	int noffset;
	int roffset;
	FILE* fps[7]; // index, nodes,roads,coords

	Stats stat;
};

typedef struct grid_entry_tag
{
	int32_t t;
	int32_t d;
} grid_entry_t;

typedef struct grid_tag
{
	int32_t nnodes;
	int32_t output; // OUT_DEC for readable text
	grid_entry_t *grid;
	node_t *nodes;
} grid_t;

typedef struct tsp_tag {
	int numorders;
	node_t *base;
	grid_t *grid; 	// time distance grid to use
} tsp_t;

typedef int (* path_fnc_t)(net_t *, node_t*, road_t *,void *);


/* grid.cpp */
node_coord_t read_coord(FILE *fp);
road_t read_road(tcache_t *tc, FILE *fp);
node_t read_node(tcache_t *tc,tile_t *tp, FILE*, FILE *fp);
grid_t *grid_ist(node_t **nds);
void grid_resize(grid_t *gp, int n);
int grid_set(grid_t *grid, int pos, node_t nd);
void grid_calc_complete(grid_t *gp, net_t *np);
int grid_write(grid_t *gp, string file);
grid_t *grid_read(net_t *np, char *file);
void grid_dmp(grid_t *gp);

void draw_open(void);
void draw_close(void);
int dump_node(node_t* nd,int);
int dump_road(tile_t *tp, road_t *rd,int);
int dump(net_t *np, node_t* nd, road_t *rd,void *data);
int roadcost(road_t road);
void net_reset(net_t *np,int lo,int hi);
result_t net_edsger(net_t *np, tilenode_t *from, tilenode_t *to);
result_t net_edsger_4(net_t *np, tilenode_t *from, tilenode_t *to);
result_t net_edsger_9(net_t *np, tilenode_t *from, tilenode_t *to);
result_t net_edsger_n(net_t *np, tilenode_t *from, tilenode_t *to);
result_t net_edsger_flat(net_t *tp, int, tilenode_t *from, tilenode_t *to);
path_t *net_path(net_t *np, tilenode_t from, tilenode_t to, int *pathlen);
path_t *net_path_flat(net_t *np, tilenode_t *from, tilenode_t *to, int *pathlen);
void net_traverse(net_t *np, tilenode_t *to, path_fnc_t what, void *data);
void net_traverse_cost(net_t *np, Cost *cst, tilenode_t *to, path_fnc_t what, void *data);
node_t *road_to_node(net_t *np, road_t *rd);
road_t *node_nth_road(net_t *np, tile_t *tp, node_t *nd, int count);
void net_init();
node_t *net_get_node(net_t *np, int level, int32_t tile, int32_t node);
tilenode_t *net_get_node_by_ll(net_t *np, int32_t lat, int32_t lon, int *displacement);
tilenode_t *net_get_node_by_pc(net_t *np, uint16_t country, char *code);
int net_seconds_cost(net_t *np, Cost *cst, node_t to);
int net_meters(net_t *np, node_t to);
int net_meters_cost(net_t *np, Cost *cst, node_t to);
net_t *net_open(char *dir, char *network);
int net_test(net_t *np);
void net_close(net_t *np);
void net_statistics(net_t *tp);
void net_check_reset(net_t *np,int lo,int hi);
tile_t *net_get_tile(net_t *np, int tileno);
tile_t *tilenode_get_tile(net_t *np, tilenode_t *nd);
node_t *tilenode_get_node(net_t *np,tilenode_t *nd);

/* tcache.cpp */
void tcache_dmp(tcache_t *tc);
tile_t *tcache_get_random_tile(tcache_t *tc);
tilenode_t *net_get_random_tilenode(net_t *np);
void count_down(const void *nodep, const VISIT which, const int depth);
void tcache_get_all(tcache_t *tc);
void tcache_find_all(tcache_t *tc);
void tcache_mark_internal(tcache_t *tc);
void tcache_read_all(tcache_t *tc);
tcarray_t tcache_get_array(tcache_t *tc);
tcentry_t *tcache_get_entry(tcache_t *tc, uint32_t tileno);
void tcache_rls(tcache_t *tc);
tilenode_t *tcache_get_node_by_ll(tcache_t *tc, int32_t lat, int32_t lon, int *displacement);
tcentry_t *tcache_find(tcache_t *tc, uint32_t tileno);
tile_t *tcache_get(tcache_t *tc, uint32_t tileno);
void tcache_reset(tcache_t *tc);
void tcache_check_reset(tcache_t *tc);
tcache_t *tcache_ist(net_t *net,int level, int max);
void tcache_add_offset(tcache_t *tc, offsets_t *te);
int tcache_init_level_read(tcache_t *tc,int level);
void tcache_add(tcache_t *tc, tcentry_t *te);
void tcache_del(tcache_t *tc, tcentry_t *te);
int *csearch(const void *item, const void *array, int nitems, int size, int (*compar)(const void *, const void *));

/* tile.cpp */
void tile_dmp(tile_t *tp, int verbose);
void tile_reset_cost(tile_t *tp,int32_t method);
tile_t *tile_ist(int32_t num);
void tile_rls(tile_t *tp);
void tile_reset(tile_t *tp);
void tile_check_reset(tile_t *tp);
void tile_reset_keep(tile_t *tp,int keep);

/* net.cpp */
tilenode_t *net_get_random_node(net_t *np);
tile_t *node_get_tile(net_t *np, node_t *nd);
tilenode_t *net_get_tilenode(net_t *np, int tileno, int offset);
double CalculateDistance( double nLat1, double nLon1, double nLat2, double nLon2 );
