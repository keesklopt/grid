#include <net.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <limits>

struct hook_tag { 
	uint32_t tileno;
	uint32_t count;
};

extern IO io;

tcache_t *tcache_ist(net_t *net,int level, int max)
{
	tcache_t *tc = (tcache_t *)calloc(sizeof(tcache_t),1);
	if (!tc) return NULL;

	tc->max=max;
	// we need at least 5 tiles for the aggregation of 4 tiles into 1
	if (tc->max < 5) tc->max=5; 
	tc->ntiles=0;
	tc->level = level;
	tc->stamp=0;
	tc->tcs = NULL;
	tc->idx = NULL;
	tc->net = net;
	tc->coords= NULL;
	if (tcache_init_level_read(tc,level) != 0) {
		free (tc);
		tc=NULL;
	}

	return tc;
}

void tcache_walk(tcache_t *tc, void (*fnc)(const void *,const VISIT,const int))
{
	if (!tc) return;
	twalk(tc->tcs,fnc);
}

void tce_get(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)nodep;

	tcache_get(tcep->tc,tcep->tileno);

	//printf("Releasing tile\n");
	//free(tcep);
}

void count_down(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)nodep;
	tcache_t *tc = tcep->tc;
	struct hook_tag *hook = (struct hook_tag *) tc->hook;

	hook->count--;
	if (hook->count==0) hook->tileno = tcep->tileno;
}

void tce_find(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)nodep;

	tcache_find(tcep->tc,tcep->tileno);
}

void tce_rls(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)nodep;
	tile_rls(tcep->tile);

	//printf("Releasing tile\n");
	//free(tcep);
}

/* walk according to index, not loaded tiles */
void tcache_walk_index(tcache_t *tc, void (*fnc)(const void *,const VISIT,const int))
{
	if (!tc) return;
	twalk(tc->idx,fnc);
}

void tcache_find_all(tcache_t *tc)
{
	tcache_walk(tc,tce_find);
}

void tcache_get_all(tcache_t *tc)
{
	tcache_walk(tc,tce_get);
}

static void reset(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tile_reset(tcep->tile);
}

static void mark_internal(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tcep->tile->internal=1;
}

static void check_reset(const void *elm, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tcep = *(tcentry_t **)elm;
	tile_check_reset(tcep->tile);
}

void tcache_mark_internal(tcache_t *tc)
{
	tcache_walk(tc,mark_internal);
}

void tcache_reset(tcache_t *tc)
{
	tcache_walk(tc,reset);
}

void tcache_check_reset(tcache_t *tc)
{
	tcache_walk(tc,check_reset);
}

void freeit(void *d)
{
	free(d);
}

void tcache_rls(tcache_t *tc)
{
	if (!tc) return;

	twalk(tc->tcs,tce_rls);

	if (tc->tcs) tdestroy(tc->tcs,free);
	if (tc->marks) free(tc->marks);
	//if (tc->tcs) free(tc->tcs);
	if (tc->idx) tdestroy(tc->idx,freeit);
	free(tc);
}

void markoldest(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	tcache_t *tc=tp->tc;

	tcentry_t *tcep = tc->marks[0];

	// mark oldest timestamp that is in-core
	if (tp->incore && (!tcep || tp->stamp < tcep->stamp)) {
		//printf("Oldest is now %d\n", tp->stamp);
		tc->marks[0] = tp;
	} 
}

int op_cmp(const void *a, const void *b)
{
	const offsets_t *A=(const offsets_t *)a;
	const offsets_t *B=(const offsets_t *)b;

	//printf("Testing %d and %d\n", A->tileno, B->tileno);
	if (A->tile < B->tile) return -1;
	if (A->tile > B->tile) return  1;
	return 0;
}

int tc_cmp(const void *a, const void *b)
{
	const tcentry_t *A=(const tcentry_t *)a;
	const tcentry_t *B=(const tcentry_t *)b;

	//printf("Testing %d and %d\n", A->tileno, B->tileno);
	if (A->tileno < B->tileno) return -1;
	if (A->tileno > B->tileno) return  1;
	return 0;
}

int tcache_len(tcache_t *tc)
{
	if (!tc || !tc->tcs) return 0;
	return tc->ntiles;
}

void tcache_mark(tcache_t *tc, tcentry_t *tcep)
{
	tc->nmarks++;
	if (tc->marks==NULL) 
		tc->marks = (tcentry_t **)calloc(1,sizeof(tcentry_t *));
	else
		tc->marks = (tcentry_t **)realloc(tc->marks,sizeof(tcentry_t *)*tc->nmarks);

	tc->marks[tc->nmarks-1] = tcep;
}

void tce_fill(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	tcache_t *tc=tp->tc;

	if (tp->done) return;
	int curitem = tc->arr.len++;
	tc->arr.items= (tcentry_t **)realloc(tc->arr.items, tc->arr.len * sizeof(tcentry_t *));
	tc->arr.items[curitem] = tp;
}

/* allocates !!, clean the data up by just 'free'ing */
tcarray_t tcache_get_array(tcache_t *tc)
{
	//tc->worklevel=level;
	// allocate one item just to be able to always realloc, 
	tc->arr.items = (tcentry_t **)calloc(1,sizeof(tcentry_t *));
	tc->arr.len=0;

	twalk(tc->tcs,tce_fill);
	return tc->arr;
}

offsets_t *tcache_get_offsets(tcache_t *tc, uint32_t tileno)
{
	offsets_t tce={0};
	offsets_t *tcep;
	offsets_t *thelper;

	tce.tile=tileno;
	thelper = &tce;
	tcep = (offsets_t *)tfind((void *)thelper,(void * const *)&tc->idx,op_cmp);
	if (tcep) tcep = *(offsets_t **)tcep;

	return tcep;
}

tile_t *tile_read_parts(tcache_t *tc,tcentry_t *tcep)
{
	int t;

	offsets_t *op = tcache_get_offsets(tc,tcep->tileno);

	if (!op) {
		//printf("Tile %d not found at level %d\n", tileno, tc->level);
		return NULL;
	} 

	tcep->tile->nroads = op->nroads;

	uint64_t seek = (uint64_t)op->roffset * 18ULL;

	fseeko(tc->fps[ORD_ROADS],seek,SEEK_SET);
	if (tcep->tile->nroads) {
		tcep->tile->rds = (road_t *)calloc(sizeof(road_t),tcep->tile->nroads);
		for (t=0;t< tcep->tile->nroads; t++) {
			tcep->tile->rds[t] = read_road(tc,tc->fps[ORD_ROADS]);
		}
	}

    seek = (uint64_t)op->noffset * 5ULL;
	fseeko(tc->fps[ORD_NODES],seek,SEEK_SET);
    seek = (uint64_t)op->noffset * 4ULL;
	fseeko(tc->fps[ORD_UPLINKS],seek,SEEK_SET);
    seek = (uint64_t)op->noffset * 8ULL;
	fseeko(tc->fps[ORD_COORDS],seek,SEEK_SET);
	tcep->tile->nnodes = op->nnodes;
	int count=0;
	if(tcep->tile->nnodes) {
		// allocate one extra because lastroad = node[n+1].firstroad-1
		tcep->tile->nds = (node_t *)calloc(sizeof(node_t),tcep->tile->nnodes+1); 
		for (t=0;t< tcep->tile->nnodes; t++) {
			tcep->tile->nds[t] = read_node(tc,tcep->tile,tc->fps[ORD_NODES], tc->fps[ORD_UPLINKS]);
	//printf("node was %d:%d \n", tp->num, t);
			tcep->tile->nds[t].tilenode = (tilenode_t *)calloc(1,sizeof(tilenode_t));
			tcep->tile->nds[t].tilenode->tp = tcep;
			tcep->tile->nds[t].tilenode->offset = t;
			//tp->nds[t].offset=t;
#ifndef SKIP_COORDS
			tcep->tile->nds[t].x = io.read_uint32(tc->fps[ORD_COORDS]);
			tcep->tile->nds[t].y = io.read_uint32(tc->fps[ORD_COORDS]);
#endif
#ifdef NOLASTROAD
			count += tcep->tile->nds[t].firstroad;
			tcep->tile->nds[t].firstroad=count;
#else
            tcep->tile->nds[t].firstroad=count;
            count += tcep->tile->nds[t].lastroad;
            tcep->tile->nds[t].lastroad=(tcep->tile->nds[t].firstroad+tcep->tile->nds[t].lastroad)-1;
#endif

		}
	}
	if (count != tcep->tile->nroads) {
		// can occur when reading stage 3 so quit this
		//printf("No : %d %d\n", count, tp->nroads);
	}
	//tile_reset(tcep->tile);
	return tcep->tile;
}

void tcache_read_parms(tcache_t *tc)
{
	tc->stat.ntiles = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.nnodes = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.nroads = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.miny = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.maxy = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.minx = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.maxx = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.minlen = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.maxlen = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.maxnode = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.maxroad = io.read_int32(tc->fps[ORD_PARMS]);
	tc->stat.maxforward = io.read_int32(tc->fps[ORD_PARMS]);
}

// find or install the tile, but do NOT read the data yet
tcentry_t *tcache_find(tcache_t *tc, uint32_t tileno)
{
	tcentry_t tce={0};
	tcentry_t *tcep;
	tcentry_t *thelper;

	tce.tileno=tileno;
	thelper = &tce;
	tcep = (tcentry_t *)tfind((void *)thelper,(void * const *)&tc->tcs,tc_cmp);

	if (tcep) { // was in cache
		tcep = *(tcentry_t **)tcep;
		if (tcep->incore) 
			return tcep;
		return tcep;
	} else {
		tcep = (tcentry_t *)calloc(sizeof(tcentry_t),1);
		tcep->incore=0;
		tcep->done=0;
		tcep->tile = tile_ist(tileno);
		//printf("Created %p\n", tcep);
		tcep->tileno = tileno;
		tcache_add(tc, tcep);
	}

	return tcep;
}

tcentry_t *tcache_get_entry(tcache_t *tc, uint32_t tileno)
{
	tcentry_t *tcep = tcache_find(tc,tileno);

	if (tcep->incore) 
		return tcep;
	// else read from disk
	tcep->incore=1;
	tile_read_parts(tc,tcep);
	tcep->level = tcep->tile->level;
	//printf("In cored tile %d\n", tileno);
	//tile_dmp(tcep->tile,1);
	return tcep;
}

tile_t *tcache_get(tcache_t *tc, uint32_t tileno)
{
	tcentry_t *tcep = tcache_find(tc,tileno);

	if (tcep->incore) 
		return tcep->tile;
	// else read from disk
	tcep->incore=1;
	tile_read_parts(tc,tcep);
	tcep->level = tcep->tile->level;
	//printf("In cored tile %d\n", tileno);
	//tile_dmp(tcep->tile,1);
	return tcep->tile;
}

void tce_dmp(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;
	printf("Tile : %d stamp %d (%s)\n", tp->tileno, tp->stamp, 
			tp->incore ? "in mem": "on disk");
	//wlk_tile_dmp(0, tp->tile,NULL);
}

void tce_prune(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	if (tp->tile->nnodes == 0 || tp->tile->nroads==0) {
		tcache_mark(tp->tc,tp); // for deletion
	}
}

void tce_all(const void *nodep, const VISIT which, const int depth)
{
	if (which == preorder || which == endorder) return;
	tcentry_t *tp = *(tcentry_t **)nodep;

	tcache_mark(tp->tc,tp); // for deletion
}

void tcache_clear_marks(tcache_t *tc)
{
	if (tc->marks) free(tc->marks);
	tc->marks=NULL;
	tc->nmarks=0;
}

/* unused since we have a cache per level
void tcache_del_level(tcache_t *tc)
{
	int t;
	tcache_clear_marks(tc); // just in case
	tc->worklevel=level;
	twalk(tc->tcs,tce_prunelevel);
	for (t =0; t< tc->nmarks; t++) {
		tcache_del(tc,tc->marks[t]);
	}
	tcache_clear_marks(tc); // and again
}
*/

void tcache_prune(tcache_t *tc)
{
	int t;
	tcache_clear_marks(tc); // just in case
	twalk(tc->tcs,tce_prune);
	for (t =0; t< tc->nmarks; t++) {
		tcache_del(tc,tc->marks[t]);
	}
	tcache_clear_marks(tc); // and again
}

void tcache_exit_level(tcache_t *tc)
{
	int t;
	for (t=0; t< 7; t++) {
		if (tc->fps[t]) fclose(tc->fps[t]);
		tc->fps[t]=NULL;
	}
	tc->noffset=0;
	tc->roffset=0;
}

int tcache_init_level_read(tcache_t *tc,int level)
{
	char fname[1024];
	int nitems=0;
	int no=0;
	int ro=0;

	tcache_exit_level(tc);
	sprintf(fname, "%s/%s/grid_%d_index",tc->net->dir,tc->net->ibase,level);
	if ((tc->fps[ORD_INDEX] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_nodes",tc->net->dir,tc->net->ibase,level);
	if ((tc->fps[ORD_NODES] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_roads",tc->net->dir,tc->net->ibase,level);
	if ((tc->fps[ORD_ROADS] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_coord",tc->net->dir,tc->net->ibase,level);
	if ((tc->fps[ORD_COORDS] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_drooc",tc->net->dir,tc->net->ibase,level);
	if ((tc->fps[ORD_REVCOORDS] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_parms",tc->net->dir,tc->net->ibase,level);
	if ((tc->fps[ORD_PARMS] = fopen(fname,"r"))==NULL) goto error;
	sprintf(fname, "%s/%s/grid_%d_up",tc->net->dir,tc->net->ibase,level);
	if ((tc->fps[ORD_UPLINKS] = fopen(fname,"r"))==NULL) goto error;

	tcache_read_parms(tc);
	tc->nindices=0;
	while (1) {
		offsets_t *op = (offsets_t *)calloc(sizeof(offsets_t),1);
		//printf("alloc %p\n", op);
		// fread needed to detect EOF
		nitems = fread(&op->tile,sizeof(uint32_t),1,tc->fps[ORD_INDEX]);
		op->tile = ntohl(op->tile);
		if (nitems != 1) { 
			free(op);
			break;
		}
		op->nnodes = io.read_uint32(tc->fps[ORD_INDEX]);
		op->nroads = io.read_uint32(tc->fps[ORD_INDEX]);
		op->noffset=no;
		op->roffset=ro;
		no += op->nnodes;
		ro += op->nroads;
		tcache_add_offset(tc,op); 
		// install the cache with empty tiles
		// so it can be tcache_walk() 'd
		tcache_find(tc,op->tile); 
	}
	//printf("Tiles %d indices %d\n", tc->stat.ntiles, tc->nindices);

	return 0;
error:
	for (int t=0; t< 7; t++) 
		if (tc->fps[t]) fclose(tc->fps[t]);
	return -1;
}

void tcache_empty(tcache_t *tc)
{
	int t;
	tcache_clear_marks(tc); // just in case
	twalk(tc->tcs,tce_all);
	for (t =0; t< tc->nmarks; t++) {
		tcache_del(tc,tc->marks[t]);
	}
	tcache_clear_marks(tc); // and again

	//printf("%d tiles left in cache\n", tc->ntiles);
}

void tcache_dmp(tcache_t *tc)
{
	twalk(tc->tcs,tce_dmp);
}

void tcache_del(tcache_t *tc, tcentry_t *te)
{
	tcentry_t *tep;
	//printf("Bye %d\n", te->tile->num);
	tile_rls(te->tile);
	tep = (tcentry_t *)tdelete((void *)te,&tc->tcs,tc_cmp);
	free(te);
	tc->ntiles--;

	//tcache_dmp(tc);
}

void tcache_add_offset(tcache_t *tc, offsets_t *te)
{
	// Sorted !
	offsets_t *op;
	op = *(offsets_t **)tsearch((void *)te,&tc->idx,op_cmp);

	//printf("Added tile %d\n", te->tile);

	tc->nindices++;
}

void tcache_add(tcache_t *tc, tcentry_t *te)
{
	// Sorted !
	tcentry_t *tcep;
	tcep = *(tcentry_t **)tsearch((void *)te,&tc->tcs,tc_cmp);
	tcep->tc=tc;
	tcep->stamp=tc->stamp++;

	if (tc->incore > tc->max) {
		tcache_clear_marks(tc);
		tcache_mark(tc,NULL); // mark one item for keeping the oldest
		twalk(tc->tcs,markoldest);
		tcep = tc->marks[0];
		//printf("Deleting one tile %d\n",tcep->tile->num);
		if (tcep) tcache_del(tc,tcep);
	}
	//printf("Adding one tile\n");
	tc->ntiles++;
	//tc->incore++;
	tcep->incore=0;

	//printf("Added --------- %d\n", te->tileno);
	//tcache_dmp(tc);
	//printf("Done --------- %d\n", te->tileno);
}

void tcache_add_tile(tcache_t *tc, tile_t *tp)
{
	tcentry_t *tcep = (tcentry_t *)calloc(1,sizeof(tcentry_t));

	tcep->tile= tp;
	tcep->tileno= tp->num;
	tcache_add(tc, tcep);
}

void tcache_read_coords(tcache_t *tc)
{
	int t;
	printf("Reading %d coords \n", tc->stat.nnodes);
	tc->coords = (node_coord_t *)calloc(tc->stat.nnodes,sizeof(node_coord_t));
	for (t=0; t< tc->stat.nnodes; t++) {
		tc->coords[t] = read_coord(tc->fps[ORD_REVCOORDS]);
	}
}

static int rev_cmp(const void *a, const void *b)
{
    const node_coord_t *A=(const node_coord_t *)a;
    const node_coord_t *B=(const node_coord_t *)b;

    //printf("Testing %d and %d\n", A->y, B->y);
    if (A->y < B->y) return -1;
    if (A->y > B->y) return  1;
    if (A->x < B->x) return -1;
    if (A->x > B->x) return  1;
    return 0;
}

int *csearch(const void *item, const void *array, int nitems, int size, int (*compar)(const void *, const void *))
{
	int lo=0,hi=nitems-1;
	char *workarr = (char *)array;
	int mid;
	int cmp;
	static int between[2];

	while (hi>=lo) {
		mid = (hi+lo)/2;
		cmp = compar(item,workarr+(mid*size));
		//printf("Cmp is %d %d-%d-%d\n", cmp, lo,mid,hi);
		if (cmp == 0) {
			between[0] = mid;
			between[1] = mid;
			return between;
		} 
		if (cmp > 0) lo=mid+1;
		if (cmp < 0) hi=mid-1;
	} 
	between[0] = lo-1;
	between[1] = hi+1;
	return between;
}

tilenode_t *tcache_get_node_by_ll(tcache_t *tc, int32_t lat, int32_t lon, int *displacement) {
	tilenode_t *wn= (tilenode_t *)calloc(1,sizeof(tilenode_t));
	node_coord_t nc, *ncp;

	nc.x = lon;
	nc.y = lat;
	if (tc->coords == NULL) tcache_read_coords(tc);

	int found;
	int *offsets = csearch(&nc,tc->coords,tc->stat.nnodes,sizeof(node_coord_t),rev_cmp);
	ncp = &tc->coords[offsets[0]];
	if (offsets[0] == offsets[1]) {
		printf("Found exact coordinate on %d:%d\n", ncp->tile, ncp->node);
	} else {
		int down=offsets[0];
		int up=offsets[1];
		found=0;
		double distmax = numeric_limits<double>::max( );
		while (down >=0 || up < tc->stat.nnodes) {
			double limitdown = haversine.distance(lat,lon,tc->coords[down].y,lon);
			double limitup = haversine.distance(lat,lon,tc->coords[up].y,lon);
			double distdown = haversine.distance(lat,lon,tc->coords[down].y,tc->coords[down].x);
			double distup = haversine.distance(lat,lon,tc->coords[up].y,tc->coords[up].x);
			//printf("Starting at %d and %d distances %f and %f\n", down,up,distup,distdown);
			if (distdown < distmax) {
				found= down;
				distmax = distdown;
			}
			if (distup < distmax) {
				found= up;
				distmax = distup;
			}

			if (distup > distdown) {
				if (down > 0) 
					down--;
			} else { 
				if (up < tc->stat.nnodes-1) 
					up++;
			}
			if (distmax < limitup && distmax < limitdown) break;
		}
		printf("Found %f location %d\n", distmax, found);
		ncp = &tc->coords[found];
	} 
	printf("Closest on y coordinate is %d:%d %d,%d\n", ncp->tile, ncp->node, ncp->y, ncp->x);

#ifdef DOUBLE_CHECK
	// double check TODO : delete when this never fires !!
	double distmax = numeric_limits<double>::max( );
	int check;
	for (int t=0; t< tc->stat.nnodes; t++) {
		double test = haversine->distance(lat,lon,tc->coords[t].y,tc->coords[t].x);
		if (test < distmax) { 
			distmax = test;
			check = t;
		}
	}
	if (check != found) {
		printf("Other result !!! %d != %d %f inshort : FIX !!\n", found, check, distmax);
		exit(0);
	}
#endif
	
	wn->tp = tcache_get_entry(tc,ncp->tile);
	wn->offset = ncp->node;

	return wn;
}
