#!/usr/bin/python

import sys;
import os;
import urllib;
import json;
import time;

print("Place finder");

#url="http://maps.googleapis.com/maps/api/geocode/json?address=Thorbeckestraat+2+Huizen&sensor=false&region=nl";
url='http://maps.googleapis.com/maps/api/geocode/json?sensor=false&region=nl';

if (len(sys.argv) < 2):
    print ("Usage :" + sys.argv[0] + " <importfile>");
    exit

try:
    f = open(sys.argv[1])
except:
    print "Sorry:", sys.exc_type, ":", sys.exc_value
    exit

lines = f.readlines()
for l in range(0, len(lines)):
 fields = lines[l].split(';');
 route = fields[0].strip()
 ritnaam = fields[1].strip()
 adress = fields[5].strip()
 nummer = fields[6].strip()
 plaats = fields[8].strip()

 get_url = url + '&address=' + adress + '+' + nummer + '+' + plaats;

 #print (get_url);
 #time.sleep(1);
 response = urllib.urlopen(get_url)
 html = response.read()
 obj=json.loads(html)

 #print line[l];
 stat = obj["status"];
 if (stat == "OK"):
    location = obj["results"][0]["geometry"]["location"];
    y = location["lat"];
    x = location["lng"];
    for f in range(0, len(fields)-1):
      sys.stdout.write (fields[f] + ";");
    sys.stdout.write(';' + str(y) + ';' + str(x));
    print("");
 else:
    print("Failed line" + get_url);

