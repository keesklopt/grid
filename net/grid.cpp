#include <net.h>


grid_t *grid_ist(tilenode_t **nds)
{
	int n;

	grid_t *gp = (grid_t *)calloc(sizeof(grid_t), 1);
	gp->nnodes=0;
	gp->output=OUT_BINARY; // NO dec of hex for now !

	if (nds == NULL || nds[0] == NULL) return gp;
	for (n=0; nds[n] != NULL; n++) {
		grid_set(gp, n, *nds[n]);
	}

	return gp;
}


void grid_resize(grid_t *gp, int n)
{
	if (n < gp->nnodes) return;

	if (gp->nnodes==0) {
		gp->nodes = (node_entry_t *)calloc(sizeof(node_entry_t), n);
		gp->grid = (grid_entry_t *)calloc(sizeof(grid_entry_t), n*n);
	} else {
		gp->nodes = (node_entry_t *)realloc(gp->nodes, sizeof(node_entry_t)* n);
		gp->grid = (grid_entry_t *)realloc(gp->grid, sizeof(grid_entry_t)* n*n);
	}
	gp->nnodes=n;
}

int grid_set(grid_t *grid, int pos, worknode_t nd)
{
	if (pos < 0) return -2;
	if (pos >= grid->nnodes) grid_resize(grid, pos+1);
	
	grid->nodes[pos].nd = nd;
	return 0;
}

void grid_calc_complete(grid_t *gp, net_t *np)
{
/*
	int x;
	int y;
	Cost *cst;

	for (y=0; y< gp->nnodes; y++) {
		cst = net_edsger_cost(np, &gp->nodes[y].nd, &node_dontmatch);
		for (x=0; x< gp->nnodes; x++) {
			gp->grid[(y*gp->nnodes+x)].d=
				net_meters_cost(np, cst, gp->nodes[x].nd);
		}
	}
*/
}

int grid_write(grid_t *gp, string file)
{
	FILE *fp = fopen(file.c_str(), "w");
	int t;
	int32_t n = gp->nnodes;

	if (!fp) {	
		printf("Could not open file %s for writing\n", file.c_str());
		return -1;
	}
	io->write_int32(gp->output,fp, gp->nnodes);
	for (t=0; t< gp->nnodes; t++){
		io->write_int32(gp->output,fp,gp->nodes[t].nd.tile->num);
		io->write_int32(gp->output,fp,gp->nodes[t].nd.node->offset);
	}
	fwrite(gp->grid, sizeof(grid_entry_t), n*n, fp);
	fclose(fp);

	return 0;
}

grid_t *grid_read(net_t *np, char *file)
{
	grid_t *gp = grid_ist(NULL);
	FILE *fp = fopen(file, "r");
	int t;
	int n;
	int32_t tile, node;

	if (!fp) {	
		printf("Could not open file %s for reading\n", file);
		return NULL;
	}
	n = gp->nnodes = io->read_int32(fp);
	grid_resize(gp, n);

	for (t=0; t< gp->nnodes; t++){
		tile = io->read_int32(fp);
		node = io->read_int32(fp);
		//gp->nodes[t].nd = net_get_node_by_ll(np, y,x, NULL);
	}
	fread(gp->grid, sizeof(grid_entry_t), n*n, fp);
	
	fclose(fp);

	return gp;
}

void grid_dmp(grid_t *gp)
{
	int x;
	int y;
	int n;

	for (n=0; n< gp->nnodes; n++) {
		printf("[%d] = %d,%d\n", n, gp->nodes[n].nd.tile->num, gp->nodes[n].nd.node->offset);
	}
	for (y=0; y< gp->nnodes; y++) {
		printf("\n");
		for (x=0; x< gp->nnodes; x++) {
			printf("[%5.5d]", gp->grid[(y*gp->nnodes+x)].d);
		}
	}
}
