#include <search.h>
#include <stdlib.h>
#include <string.h>

#define LOWERCOST(a,b) (((a)->max && !(b)->max) || (((a)->max == (b->max) && (a)->cost < (a)->cost)))

int narr;
int extra;
costnode_t **array;

void costnode_add(const void *nodep, const VISIT which, const int depth)
{
   	if (which == preorder || which == endorder) return;
	//printf("Walking dead %d arr\n", narr);
   	//costnode_t *tp = *(costnode_t **)nodep;
	//if (tp->node->link != NULL) 
		array[narr++] = *(costnode_t **)nodep;
}

void costnode_draw(const void *nodep, const VISIT which, const int depth)
{
   	if (which == preorder || which == endorder) return;
   	costnode_t *tp = *(costnode_t **)nodep;
	//if (tp->node->link != NULL) 
		dump_node(tp->node,extra);
}

void costnode_dmp(const void *nodep, const VISIT which, const int depth)
{
   	if (which == preorder || which == endorder) return;
   	costnode_t *tp = *(costnode_t **)nodep;
	//if (tp->node->link != NULL) 
   	printf("cost [%d] %d:%d (%p) node is %p\n", tp->cost, tp->node->tilenode->tp->tile->num, tp->node->tilenode->offset,tp->node->link, tp->node);
   	//wlk_tile_dmp(0, tp->node,NULL);
}

void resetcost(const void *nodep, const VISIT which, const int depth)
{
   	if (which == preorder || which == endorder) return;
   	costnode_t *tp = *(costnode_t **)nodep;
	//printf("Reset %d:%d\n", tp->node->tilenode->tp->tileno, tp->node->tilenode->offset);
	tp->node->cost=HEAP_CLEAR;
	tp->node->eval=0;
	tp->node->max=0;
}

int costnode_cmp(const void *a, const void *b)
{
    const costnode_t *A=(const costnode_t *)a;
    const costnode_t *B=(const costnode_t *)b;

    //printf("Testing %p and %p\n", A->node, B->node);
    if (A->node < B->node) return -1;
    if (A->node > B->node) return  1;
    //if (A->node->tilenode->tp->num < B->node->tilenode->tp->num) return -1;
    //if (A->node->tilenode->tp->num > B->node->tilenode->tp->num) return  1;
    //if (A->node->tilenode->offset < B->node->tilenode->offset) return -1;
    //if (A->node->tilenode->offset > B->node->tilenode->offset) return  1;
    return 0;
}

class Cost
{
	public:
	int nelm;
	void *elms; 

	void reset()
	{
    	twalk(this->elms,resetcost);
	}

	void dmp()
	{
    	twalk(this->elms,costnode_dmp);
	}

	void draw(int xtr)
	{
		extra = xtr;
    	twalk(this->elms,costnode_draw);
	}

	void set(node_t *nd)
	{
    	// Sorted !
		costnode_t *test;
    	costnode_t *tcep = (costnode_t *)calloc(1,sizeof(costnode_t));
		tcep->node=nd;
		tcep->cost=nd->cost;

    	test = *(costnode_t **)tsearch((void *)tcep,&elms,costnode_cmp);
		if (test == NULL) {
			printf("tree failure !\n");
			exit(EXIT_FAILURE);
		} else if (test != tcep)
			free(tcep);

        nelm++;
	}

	void add(costnode_t **b_arr)
	{
		//printf("Before %d nodes\n", nelm);
		while (*b_arr) {
			add((*b_arr)->node);
			//printf("Add\n");
			(b_arr)++;
		}

		//printf("Now %d nodes\n", nelm);
	}

	costnode_t *get(node_t *nd)
	{
    	costnode_t *tcep;
		costnode_t tce;

		tce.node = nd;
    	tcep = (costnode_t *)tfind((void *)&tce,&elms,costnode_cmp);
		if (tcep) tcep = *(costnode_t **)tcep;
		return tcep;
	}

	result_t overlap(costnode_t **a_arr, costnode_t **b_arr) {
	//	node_t *found = NULL;
		result_t r = {0};
		int foundcost=MAX_COST;
		while (*a_arr && *b_arr) {
				//printf("%d:%d vs %d:%d\n", (*a_arr)->node->tilenode->tp->tile->num, (*a_arr)->node->tilenode->offset, (*b_arr)->node->tilenode->tp->tile->num, (*b_arr)->node->tilenode->offset);
			if ((*a_arr)->node == (*b_arr)->node) {
				//printf("node %d:%d with %d and %d\n", (*a_arr)->node->tilenode->tp->tile->num,(*a_arr)->node->tilenode->offset, (*a_arr)->cost, (*b_arr)->cost);

				if ( (*a_arr)->cost != HEAP_CLEAR && 
					(*b_arr)->cost != HEAP_CLEAR &&
					(r.join==NULL || (*a_arr)->cost+(*b_arr)->cost <= foundcost )) { 
					//printf("Better node %d:%d with %d and %d\n", (*a_arr)->node->tilenode->tp->tile->num,(*a_arr)->node->tilenode->offset, (*a_arr)->cost, (*b_arr)->cost);
					r.join = (*a_arr)->node;
					foundcost = (*a_arr)->cost + (*b_arr)->cost;
					r.dm = foundcost;
				}
				(a_arr)++;
				(b_arr)++;
			} else 
			if ((*a_arr)->node < (*b_arr)->node) {
				(a_arr)++;
			} else {
				(b_arr)++;
			} 
		}
		return r;
	}

	result_t overlap(Cost *b) {
		//node_t *rval=NULL;
		result_t rval = {0};
		costnode_t **a_arr = this->get_array();
		costnode_t **b_arr = b->get_array();

		rval = overlap(a_arr,b_arr);

		free(a_arr);
		free(b_arr);
		return rval;
	}

	costnode_t **get_array(void)
	{
		array = (costnode_t **)calloc(this->nelm+1, sizeof(costnode_t *));
		narr=0;
    	twalk(this->elms,costnode_add);
		array[narr++] = NULL;
		return array;
	}

	void add(node_t *nd)
	{
    	return set(nd);
	}

};

class HeapNode
{
	public:
	node_t *node;
	//int32_t cost;
    HeapNode *next;
};

/* hash table based heap :
	part is the number of hash indices
 */
class Heap
{
	public:
	int bigdiff;
	int32_t part;
    int32_t bot;
    int32_t top;
    int32_t phys;
    HeapNode **data;
    HeapNode *maxline;

	Heap(int part)
	{
		this->part=part;
		this->bot=0;
		this->top=0;
		this->bigdiff=0;
		if (this->part < 1) this->part=1;
		// initial calloc, so we can realloc later
		this->phys=1; 
		this->data = (HeapNode **)calloc(this->phys , sizeof(HeapNode *));
		this->maxline = NULL;
	}

	void elevate(net_t *np, costnode_t **cn)
	{
		HeapNode *ptr;
		this->bot=0;
		this->top=0;
		this->bigdiff=0;
		if (this->part < 1) this->part=1;
		// initial calloc, so we can realloc later
		this->phys=1; 
		free(this->data);
		this->data = (HeapNode **)calloc(this->phys , sizeof(HeapNode *));

		//dmp();
		ptr= this->maxline;
		for (; ptr != NULL; ptr = ptr->next)
		{
			//int parent = levelinfo->parent(ptr->node->tilenode->tp->tile->num);
			//printf ("Found cost %d for node %d:%d\n", ptr->node->cost, ptr->node->tilenode->tp->tileno, ptr->node->tilenode->offset);
			if (ptr->node->link!=NULL) {
				tile_t *p = tilenode_get_tile(np,ptr->node->link);
				node_t *peer = &p->nds[ptr->node->link->offset];
				//printf ("Peer would be node %d:%d cost %d\n", parent, ptr->node->link->offset, peer->cost);
				peer->cost = ptr->node->cost;
				peer->max = 0;
				peer->eval = 0;
				add(peer);
			}

			// reset old node
			ptr->node->eval=0;
			ptr->node->cost=HEAP_CLEAR;
			ptr->node->max=0;

		}

		while (*cn) {
			if ((*cn)->node->link) {
				//int parent = levelinfo->parent(ptr->node->tilenode->tp->tile->num);
				//printf ("Found cost %d for node %d:%d\n", ptr->node->cost, ptr->node->tilenode->tp->tileno, ptr->node->tilenode->offset);
				tile_t *p = tilenode_get_tile(np,(*cn)->node->link);
				node_t *peer = &p->nds[(*cn)->node->link->offset];
				//printf ("Peer would be node %d:%d cost %d\n", parent, ptr->node->link->offset, peer->cost);
				peer->cost = (*cn)->node->cost;
				peer->max = 0;
				peer->eval = 0;

				// reset old node
				(*cn)->node->eval=0;
				(*cn)->node->cost=HEAP_CLEAR;
				(*cn)->node->max=0;
	
				add(peer);
			} else {	
				// reset old node
				(*cn)->node->eval=0;
				(*cn)->node->cost=HEAP_CLEAR;
				(*cn)->node->max=0;
	
			}
			(cn)++;
		}

		// now delete all maxline entries
		ptr = this->maxline;
		while (ptr != NULL) {
			HeapNode *bye = ptr;
			ptr= ptr->next;
			free(bye);
		}
		this->maxline = NULL;
	}

	// NOTE : find the index on cost, but the node on tile/node !!!
	void del(node_t *np)
	{
		HeapNode *ptr;
		HeapNode **root;
		HeapNode *old;
		int offset = (np->cost / this->part) - this->bot;
		int found=0;
		//printf("D: %d\n", np->cost);
		//printf("offset %d\n", offset);
		//printf("max %d\n", np->max);
		//dmp();
		if (np->max) root = &this->maxline;
		else root = &this->data[offset];

	
		ptr = *root;

		for (; ptr != NULL; ptr = ptr->next)
		{
			//printf("Testing %d\n", ptr->node->cost);
			if (ptr->next) {
				//printf("tiles %d and %d\n", np->tile, *ptr->next->node->tile);
				//printf("offsets %d and %d\n", np->offset, *ptr->next->node->offset);
				//printf("cost %d \n", *ptr->next->node->cost);
			} else {
				//printf("First ? %d\n", *ptr->node->cost);
			}
			if (ptr->next && 
				np == ptr->next->node) {
				// delete next
				old = ptr->next;
				ptr->next = ptr->next->next;
				//printf("Yep deleting %d now\n", old->node->cost);
				free(old);
				found=1;
				break;
			}
			if (np == ptr->node) {
				// delete first
				//printf("Deleting first\n");
				//old = this->data[offset];
				old = *root;
				(*root) = (*root)->next;
				//this->data[offset] = this->data[offset]->next;
				free(old);
				found=1;
				break;
			}
		}
		if (!found) {
			printf("Did not find %d \n", np->cost);
		printf("offset %d\n", offset);
			dmp();
			exit(-1);
		}
		if (this->top-this->bot > this->bigdiff) {
			bigdiff = this->top-this->bot;
			//printf("%d         \r", bigdiff);
			//fflush(stdout);
		}
		//heap_dmp(this);
	}
	
	void reset()
	{
		int y;
		HeapNode *ptr;
		int sz = this->top - this->bot;
	
		for (y=0; y<= sz; y++) {
			ptr = this->data[y]; 
			if (y==sz) ptr = this->maxline;
			for (; ptr != NULL; ptr = ptr->next)
			{
				ptr->node->cost=HEAP_CLEAR;
				ptr->node->eval=0;
				ptr->node->max=0;
			}
		}
	}
	
	void dmp()
	{
		int y;
		HeapNode *ptr;
		int sz = this->top - this->bot;
	
		printf("offset [%d:%d], phys %d\n", this->bot, this->top, this->phys);
	
		for (y=0; y< sz; y++) {
			printf("\n[%d] (%d):", y, y+this->bot);
			for (ptr = this->data[y]; ptr != NULL; ptr = ptr->next)
			{
				printf("%d:%d cost %d max is %d ", ptr->node->tilenode->tp->tileno, 
			ptr->node->tilenode->offset, ptr->node->cost, ptr->node->max);
			}
		}
			printf("\n[MAX] :");
		for (ptr = this->maxline; ptr != NULL; ptr = ptr->next)
		{
			printf("maxline %d:%d cost %d max %d ln %p ", ptr->node->tilenode->tp->tileno, 
		ptr->node->tilenode->offset, ptr->node->cost, ptr->node->max, ptr->node->link);
		}
		printf("\n");
	}
	
	void add(node_t *nd)
	{
		int oldsz;
		int index = (nd->cost / this->part) - this->bot;
		HeapNode *ptr=NULL;
		//printf("A: %d\n", nd->cost);
		//printf("offset: %d %d\n", index, nd->max);
		//dmp();

		if (nd->max == 0) {
			if (index+1 > this->top-this->bot) {
				oldsz = this->phys ;
				if (index >= this->phys) {
					this->phys=index+1;
					//printf("resizing from %d to %d\n", oldsz, this->phys);
				}
				this->data = (HeapNode **)realloc(this->data, this->phys * sizeof(HeapNode *));
				int t;
				for (t=oldsz; t< this->phys; t++)  {	
					//printf("setting %d to null\n", t);
					this->data[t] = NULL;
				}
				this->top= this->bot+index+1;
			}
			ptr = this->data[index];
		} else 
			ptr = this->maxline;

		HeapNode *hnp = (HeapNode *)calloc(sizeof(HeapNode), 1);
		//printf("Creating %p \n", hnp);

		for (; ptr != NULL; ptr = ptr->next)
		{
			if (nd->cost < ptr->node->cost) {
				// default will handle this
				break;
			}
			if ( (!ptr->next || nd->cost < ptr->next->node->cost) ) { 
				//printf("Inserting!?!\n");
				// insert here
				hnp->next = ptr->next;
				hnp->node = nd;
				ptr->next = hnp;
				return;
			}
		}
		// insert as only 
		hnp->next = ptr;
		hnp->node = nd;
		if (nd->max) 
			this->maxline = hnp;
		else
			this->data[index] = hnp;
		//dmp();
	}
	
	node_t *newnode(int cost)
	{
		node_t *nn = (node_t *)calloc(sizeof(node_t), 1);
	
		nn->cost = cost;
		return nn;
	}
	
	// remove all empty slots at the beginning
	void prune()
	{
		int y;
		int sz = this->top - this->bot;
		int offset=0;
	
		for (y=0; y< sz; y++) {
			//printf("%d is %p\n", y, this->data[y]);
			if (this->data[y] != NULL) 
				break;
			offset++;
		}
		if (offset==0) return;
		//printf("Movin %d to %d , %d big\n", offset, 0, this->phys);
		memmove(&this->data[0], &this->data[offset], (this->phys-offset)*sizeof(HeapNode *));
		//printf("resetting %d to %d \n", this->phys, offset);
		memset(&this->data[this->phys-offset], '\0', offset*sizeof(HeapNode *));
		this->bot += offset;
	}
	
	node_t *get()
	{
		prune();
		if (this->top-this->bot < 1 || !this->data[0] ) return NULL;
		return this->data[0]->node;
	}

	void clear()
	{
		int y;
		int sz = this->top - this->bot;
		HeapNode *ptr;
		HeapNode *old;
		for (y=0; y< sz; y++) {
			ptr = this->data[y]; 
			while(ptr) {
				old = ptr;
				ptr=ptr->next;
				//printf("Freeing %p\n", old);
				free(old);
			}
		}
		//printf("Freeing %p\n", this->data);
		//free(this->data);
	}

	int empty()
	{
		return (get() == NULL);
	}
	
	~Heap()
	{
	
		clear();
		//free(this);
	}
};

/* net.cpp */
Cost *net_edsger_forward(net_t *np, Heap *h, int level,tilenode_t *from);
Cost *net_edsger_backward(net_t *np, Heap *h,int level,tilenode_t *to);
Cost *net_edsger_single(net_t *np, Heap *heap, int level, tilenode_t *node,int dir);
